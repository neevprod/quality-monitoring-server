package util;

import com.pearson.itautomation.oldthrashbarg.OldThrashbarg;
import com.typesafe.config.Config;
import org.jasypt.util.text.BasicTextEncryptor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class EncryptorDecryptorTest {
    private final String decryptedString = "topSecretPassword";
    private final String encryptedString = "+H23ryvqZCdFgd9WMzbIvdEWdaF1urS7xD8VewYU2iI=";
    private EncryptorDecryptor encryptorDecryptor;

    @Before
    public void initialize() {
        encryptorDecryptor = new EncryptorDecryptor();
    }

    @Test
    public void convertToDatabaseColumn_returnEncryptedValue() {
        // When
        String result = encryptorDecryptor.convertToDatabaseColumn(decryptedString);

        // Then
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPasswordCharArray(OldThrashbarg.KEY);
        assertThat("The string should be encrypted correctly", encryptor.decrypt(result), equalTo(decryptedString));
    }

    @Test
    public void convertToEntityAttribute_returnDecryptedValue() {
        // When
        String result = encryptorDecryptor.convertToEntityAttribute(encryptedString);

        // Then
        assertThat("The string should be decrypted correctly", result, equalTo(decryptedString));
    }

    @Test
    public void getDecryptedConfigurationString_returnDecryptedValueFromConfiguration() {
        // Given
        Config configuration = Mockito.mock(Config.class);
        String key = "application.topSecretPassword";
        Mockito.when(configuration.getString(key)).thenReturn(encryptedString);

        // When
        String result = EncryptorDecryptor.getDecryptedConfigurationString(configuration, key);

        // Then
        assertThat("The string from the configuration should be decrypted correctly", result, equalTo(decryptedString));
    }
}
