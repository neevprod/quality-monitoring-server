package util;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.SecureController;
import global.TestWithApplication;
import models.PermissionSettingsValue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import play.db.jpa.JPAApi;
import play.libs.Json;
import services.PermissionSettingsValueService;

import java.util.HashMap;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class SessionDataTest extends TestWithApplication {
    private final String moduleLevelModuleName = "moduleLevelModule";
    private final String tenantLevelModuleName = "tenantLevelModule";
    private final String environmentLevelModuleName = "environmentLevelModule";

    private JPAApi jpaApi;
    private PermissionSettingsValueService mockValueService;
    private ObjectNode sessionJson;
    private ObjectNode environmentLevelPermissions;
    private SessionData sessionData;

    @Before
    public void setUp() {
        sessionJson = Json.newObject();
        sessionJson.put("userId", 123L);
        sessionJson.put("userEmail", "tester@test.com");
        sessionJson.put("userName", "Tester Testington");
        sessionJson.put("apiUser", false);

        ObjectNode userPermissions = sessionJson.putObject("userPermissions");
        userPermissions.put("admin", false);
        ArrayNode modulePermissions = userPermissions.putArray("modulePermissions");

        ObjectNode moduleLevelModule = modulePermissions.addObject();
        moduleLevelModule.put("permissionType", "ModuleLevelPermission");
        moduleLevelModule.put("moduleName", moduleLevelModuleName);
        moduleLevelModule.put("permissions", 3L);

        ObjectNode tenantLevelModule = modulePermissions.addObject();
        tenantLevelModule.put("permissionType", "TenantLevelPermission");
        tenantLevelModule.put("moduleName", tenantLevelModuleName);
        ObjectNode tenantLevelPermissions = tenantLevelModule.putObject("permissions");

        ObjectNode previewer1Permissions = tenantLevelPermissions.putObject("1");
        previewer1Permissions.put("1", 3L);
        previewer1Permissions.put("2", 6L);

        ObjectNode previewer2Permissions = tenantLevelPermissions.putObject("2");
        previewer2Permissions.put("3", 2L);
        previewer2Permissions.put("5", 8L);

        ObjectNode previewer3Permissions = tenantLevelPermissions.putObject("3");
        previewer3Permissions.put("*", 4L);
        previewer3Permissions.put("8", 3L);

        ObjectNode previewer4Permissions = tenantLevelPermissions.putObject("4");
        previewer4Permissions.put("*", 4L);

        ObjectNode environmentLevelModule = modulePermissions.addObject();
        environmentLevelModule.put("permissionType", "EnvironmentLevelPermission");
        environmentLevelModule.put("moduleName", environmentLevelModuleName);
        environmentLevelPermissions = Json.newObject();
        environmentLevelModule.set("permissions", environmentLevelPermissions);

        environmentLevelPermissions.put("1", 3L);
        environmentLevelPermissions.put("2", 2L);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        mockValueService = Mockito.mock(PermissionSettingsValueService.class);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);
    }

    @Test
    public void getUserId_returnUserId() {
        // When
        Long userId = sessionData.getUserId();

        // Then
        assertThat("User ID should be correct", userId, equalTo(123L));
    }

    @Test
    public void getUserEmail_emailExists_returnOptionalWithUserEmail() {
        // When
        Optional<String> userEmailOptional = sessionData.getUserEmail();

        // Then
        assertThat("User email should be correct", userEmailOptional.get(), equalTo("tester@test.com"));
    }

    @Test
    public void getUserEmail_noEmailExists_returnEmptyOptional() {
        // Given
        sessionJson.remove("userEmail");
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        Optional<String> userEmailOptional = sessionData.getUserEmail();

        // Then
        assertThat("Optional should be empty", userEmailOptional.isPresent(), equalTo(false));
    }

    @Test
    public void getUserName_returnUserName() {
        // When
        String userName = sessionData.getUserName();

        // Then
        assertThat("Name of the user should be correct", userName, equalTo("Tester Testington"));
    }

    @Test
    public void isAdmin_returnIsAdmin() {
        // When
        boolean isAdmin = sessionData.isAdmin();

        // Then
        assertThat("IsAdmin value should be correct", isAdmin, equalTo(false));
    }

    @Test
    public void isApiUser_returnIsApiUser() {
        // When
        boolean isApiUser = sessionData.isApiUser();

        // Then
        assertThat("IsApiUser value should be correct", isApiUser, equalTo(false));
    }

    @Test
    public void hasModuleLevelPermission_moduleIsWrongType_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasModuleLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasModuleLevelPermission_moduleDoesNotExist_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasModuleLevelPermission("feature1", SecureController.AccessType.READ,
                "someRandomModule");

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasModuleLevelPermission_userHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasModuleLevelPermission("feature1", SecureController.AccessType.READ,
                moduleLevelModuleName);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasModuleLevelPermission_featureNotListedInSettings_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasModuleLevelPermission("feature2", SecureController.AccessType.READ,
                moduleLevelModuleName);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasModuleLevelPermission_noPermissionForAccessType_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(true);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(true);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasModuleLevelPermission("feature1", SecureController.AccessType.UPDATE,
                moduleLevelModuleName);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasModuleLevelPermission_adminUser_returnTrue() {
        // Given
        ((ObjectNode) sessionJson.get("userPermissions")).put("admin", true);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        boolean hasPermission = sessionData.hasModuleLevelPermission("feature1", SecureController.AccessType.READ,
                moduleLevelModuleName);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasWildcardAccessToPreviewer_moduleIsWrongType_returnFalse() {
        // When
        boolean hasAccess = sessionData.hasWildcardAccessToPreviewer(moduleLevelModuleName, 3);

        // Then
        assertThat("The return value should be false", hasAccess, equalTo(false));
    }

    @Test
    public void hasWildcardAccessToPreviewer_moduleDoesNotExist_returnFalse() {
        // When
        boolean hasAccess = sessionData.hasWildcardAccessToPreviewer("someRandomModule", 3);

        // Then
        assertThat("The return value should be false", hasAccess, equalTo(false));
    }

    @Test
    public void hasWildcardAccessToPreviewer_hasWildcardAccess_returnTrue() {
        // When
        boolean hasWildcardAccess = sessionData.hasWildcardAccessToPreviewer(tenantLevelModuleName, 3);

        // Then
        assertThat("The returned value should be true", hasWildcardAccess, equalTo(true));
    }

    @Test
    public void hasWildcardAccessToPreviewer_noWildcardAccess_returnFalse() {
        // When
        boolean hasWildcardAccess = sessionData.hasWildcardAccessToPreviewer(tenantLevelModuleName, 1);

        // Then
        assertThat("The returned value should be false", hasWildcardAccess, equalTo(false));
    }

    @Test
    public void hasWildcardAccessToPreviewer_noAccessToPreviewer_returnFalse() {
        // When
        boolean hasWildcardAccess = sessionData.hasWildcardAccessToPreviewer(tenantLevelModuleName, 9);

        // Then
        assertThat("The returned value should be false", hasWildcardAccess, equalTo(false));
    }

    @Test
    public void getAllowedPreviewerIds_moduleIsWrongType_returnEmptySet() {
        // When
        Set<Integer> resultSet = sessionData.getAllowedPreviewerIds(moduleLevelModuleName);

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedPreviewerIds_moduleDoesNotExist_returnEmptySet() {
        // When
        Set<Integer> resultSet = sessionData.getAllowedPreviewerIds("someRandomModule");

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedPreviewerIds_userHasAccessToPreviewers_returnSetOfIds() {
        // When
        Set<Integer> resultSet = sessionData.getAllowedPreviewerIds(tenantLevelModuleName);

        // Then
        assertThat("The set should have a size of 4", resultSet.size(), equalTo(4));
        assertThat("The set should contain the value 1", resultSet.contains(1), equalTo(true));
        assertThat("The set should contain the value 2", resultSet.contains(2), equalTo(true));
        assertThat("The set should contain the value 3", resultSet.contains(3), equalTo(true));
        assertThat("The set should contain the value 4", resultSet.contains(4), equalTo(true));
    }

    @Test
    public void getAllowedPreviewerIds_userHasNoAccessToPreviewers_returnEmptySet() {
        // Given
        ((ObjectNode) sessionJson.get("userPermissions")).putArray("modulePermissions");
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        Set<Integer> resultSet = sessionData.getAllowedPreviewerIds(tenantLevelModuleName);

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedTenantIds_moduleIsWrongType_returnEmptySet() {
        // When
        Set<Long> resultSet = sessionData.getAllowedTenantIds(moduleLevelModuleName, 1);

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedTenantIds_moduleDoesNotExist_returnEmptySet() {
        // When
        Set<Long> resultSet = sessionData.getAllowedTenantIds("someRandomModule", 1);

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedTenantIds_userHasAccessToTenants_returnSetOfIds() {
        // When
        Set<Long> resultSet = sessionData.getAllowedTenantIds(tenantLevelModuleName, 1);

        // Then
        assertThat("The set should have a size of 2", resultSet.size(), equalTo(2));
        assertThat("The set should contain the value 1", resultSet.contains(1L), equalTo(true));
        assertThat("The set should contain the value 2", resultSet.contains(2L), equalTo(true));
    }

    @Test
    public void getAllowedTenantIds_userHasNoAccessToTenants_returnEmptySet() {
        // When
        Set<Long> resultSet = sessionData.getAllowedTenantIds(tenantLevelModuleName, 4);

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedTenantIds_userHasWildcardPermissions_filterOutWildcards() {
        // When
        Set<Long> resultSet = sessionData.getAllowedTenantIds(tenantLevelModuleName, 3);

        // Then
        assertThat("The set should have a size of 1", resultSet.size(), equalTo(1));
        assertThat("The set should contain the value 8", resultSet.contains(8L), equalTo(true));
    }

    @Test
    public void hasTenantLevelPermission_moduleIsWrongType_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                moduleLevelModuleName, 1, 1L);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasTenantLevelPermission_moduleDoesNotExist_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                "someRandomModule", 1, 1L);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasTenantLevelPermission_userHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1, 1L);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasTenantLevelPermission_noPermissionForPreviewer_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 3, 1L);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasTenantLevelPermission_noPermissionForTenant_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1, 3L);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasTenantLevelPermission_featureNotListedInSettings_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature2", SecureController.AccessType.READ,
                tenantLevelModuleName, 1, 1L);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasTenantLevelPermission_noPermissionForAccessType_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(true);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(true);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.UPDATE,
                tenantLevelModuleName, 1, 1L);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasTenantLevelPermission_hasPermissionForWildcardButNotTenant_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> tenantValueMap = new HashMap<>();
        tenantValueMap.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(4L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(tenantValueMap);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 3, 8L);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasTenantLevelPermission_hasPermissionForTenantButNotWildcard_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(false);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(true);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> tenantValueMap = new HashMap<>();
        tenantValueMap.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(4L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(tenantValueMap);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 3, 8L);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasTenantLevelPermission_hasPermissionForWildcardWithTenantPermissionNotPresent_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(4L)).thenReturn(wildcardValueMap);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 3, 1L);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasTenantLevelPermission_noPermissionForTenantNorWildcard_returnFalse() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(false);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> tenantValueMap = new HashMap<>();
        tenantValueMap.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(4L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(tenantValueMap);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 3, 8L);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(false));
    }

    @Test
    public void hasTenantLevelPermission_adminUser_returnTrue() {
        // Given
        ((ObjectNode) sessionJson.get("userPermissions")).put("admin", true);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        boolean hasPermission = sessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 3, 1L);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyTenantLevelPermission_moduleIsWrongType_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                moduleLevelModuleName, 1);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyTenantLevelPermission_moduleDoesNotExist_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                "someRandomModule", 1);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyTenantLevelPermission_firstTenantHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap1 = new HashMap<>();
        valueMap1.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap2 = new HashMap<>();
        valueMap2.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap1);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(valueMap2);

        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyTenantLevelPermission_lastTenantHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(false);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap1 = new HashMap<>();
        valueMap1.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(true);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap2 = new HashMap<>();
        valueMap2.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap1);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(valueMap2);

        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyTenantLevelPermission_WildcardHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> tenantValueMap = new HashMap<>();
        tenantValueMap.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(4L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(tenantValueMap);

        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 3);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyTenantLevelPermission_WildcardHasPermissionAndNoTenantSettingsPresent_returnTrue() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(4L)).thenReturn(wildcardValueMap);

        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 4);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyTenantLevelPermission_noPermissionForPreviewer_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyTenantLevelPermission_featureNotListedForAnyTenant_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature2", SecureController.AccessType.READ,
                tenantLevelModuleName, 1);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyTenantLevelPermission_noPermissionForAccessTypeInAnyTenant_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(true);
        value.setReadAccess(false);
        value.setUpdateAccess(true);
        value.setDeleteAccess(true);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyTenantLevelPermission_adminUser_returnTrue() {
        // Given
        ((ObjectNode) sessionJson.get("userPermissions")).put("admin", true);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        boolean hasPermission = sessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void getAllowedEnvironmentIds_moduleIsWrongType_returnEmptySet() {
        // When
        Set<Integer> resultSet = sessionData.getAllowedEnvironmentIds(tenantLevelModuleName);

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedEnvironmentIds_moduleDoesNotExist_returnEmptySet() {
        // When
        Set<Integer> resultSet = sessionData.getAllowedEnvironmentIds("someRandomModule");

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedEnvironmentIds_userHasAccessToEnvironments_returnSetOfIds() {
        // When
        Set<Integer> resultSet = sessionData.getAllowedEnvironmentIds(environmentLevelModuleName);

        // Then
        assertThat("The set should have a size of 2", resultSet.size(), equalTo(2));
        assertThat("The set should contain the value 1", resultSet.contains(1), equalTo(true));
        assertThat("The set should contain the value 2", resultSet.contains(2), equalTo(true));
    }

    @Test
    public void getAllowedEnvironmentIds_userHasNoAccessToEnvironments_returnEmptySet() {
        // Given
        environmentLevelPermissions.removeAll();
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        Set<Integer> resultSet = sessionData.getAllowedEnvironmentIds(environmentLevelModuleName);

        // Then
        assertThat("The set should be empty", resultSet.size(), equalTo(0));
    }

    @Test
    public void getAllowedEnvironmentIds_userHasWildcardPermissions_filterOutWildcards() {
        // Given
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        Set<Integer> resultSet = sessionData.getAllowedEnvironmentIds(environmentLevelModuleName);

        // Then
        assertThat("The set should have a size of 2", resultSet.size(), equalTo(2));
        assertThat("The set should contain the value 1", resultSet.contains(1), equalTo(true));
        assertThat("The set should contain the value 2", resultSet.contains(2), equalTo(true));
    }

    @Test
    public void hasWildcardAccessToEnvironments_moduleIsWrongType_returnFalse() {
        // When
        boolean hasAccess = sessionData.hasWildcardAccessToEnvironments(tenantLevelModuleName);

        // Then
        assertThat("The returned value should be false", hasAccess, equalTo(false));
    }

    @Test
    public void hasWildcardAccessToEnvironments_moduleDoesNotExist_returnFalse() {
        // When
        boolean hasAccess = sessionData.hasWildcardAccessToEnvironments("someRandomModule");

        // Then
        assertThat("The returned value should be false", hasAccess, equalTo(false));
    }

    @Test
    public void hasWildcardAccessToEnvironments_hasWildcardAccess_returnTrue() {
        // Given
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        boolean hasWildcardAccess = sessionData.hasWildcardAccessToEnvironments(environmentLevelModuleName);

        // Then
        assertThat("The returned value should be true", hasWildcardAccess, equalTo(true));
    }

    @Test
    public void hasWildcardAccessToEnvironments_noWildcardAccess_returnFalse() {
        // When
        boolean hasWildcardAccess = sessionData.hasWildcardAccessToEnvironments(environmentLevelModuleName);

        // Then
        assertThat("The returned value should be false", hasWildcardAccess, equalTo(false));
    }

    @Test
    public void hasEnvironmentLevelPemission_moduleIsWrongType_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                tenantLevelModuleName, 1);

        // Then
        assertThat("The returned value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasEnvironmentLevelPemission_moduleDoesNotExist_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                "someRandomModule", 1);

        // Then
        assertThat("The returned value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasEnvironmentLevelPermission_userHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                environmentLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasEnvironmentLevelPermission_noPermissionForEnvironment_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                environmentLevelModuleName, 4);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasEnvironmentLevelPermission_featureNotListedInSettings_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature2", SecureController.AccessType.READ,
                environmentLevelModuleName, 1);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasEnvironmentLevelPermission_noPermissionForAccessType_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(true);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(true);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.UPDATE,
                environmentLevelModuleName, 1);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasEnvironmentLevelPermission_hasPermissionForWildcardButNotEnvironment_returnTrue() {
        // Given
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> environmentValueMap = new HashMap<>();
        environmentValueMap.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(environmentValueMap);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                environmentLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasEnvironmentLevelPermission_hasPermissionForEnvironmentButNotWildcard_returnTrue() {
        // Given
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(false);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(true);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> environmentValueMap = new HashMap<>();
        environmentValueMap.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(environmentValueMap);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                environmentLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasEnvironmentLevelPermission_hasPermissionForWildcardWithEnvironmentPermissionNotPresent_returnTrue() {
        // Given
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(wildcardValueMap);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                environmentLevelModuleName, 4);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasEnvironmentLevelPermission_noPermissionForEnvironmentNorWildcard_returnFalse() {
        // Given
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(false);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> environmentValueMap = new HashMap<>();
        environmentValueMap.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(environmentValueMap);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                environmentLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(false));
    }

    @Test
    public void hasEnvironmentLevelPermission_adminUser_returnTrue() {
        // Given
        ((ObjectNode) sessionJson.get("userPermissions")).put("admin", true);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        boolean hasPermission = sessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                environmentLevelModuleName, 1);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyEnvironmentLevelPemission_moduleIsWrongType_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, tenantLevelModuleName);

        // Then
        assertThat("The returned value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyEnvironmentLevelPemission_moduleDoesNotExist_returnFalse() {
        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, "someRandomModule");

        // Then
        assertThat("The returned value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyEnvironmentLevelPermission_firstEnvironmentHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap1 = new HashMap<>();
        valueMap1.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap2 = new HashMap<>();
        valueMap2.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap1);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(2L)).thenReturn(valueMap2);

        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, environmentLevelModuleName);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyEnvironmentLevelPermission_lastEnvironmentHasPermission_returnTrue() {
        // Given
        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(false);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap1 = new HashMap<>();
        valueMap1.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(true);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap2 = new HashMap<>();
        valueMap2.put("feature1", value2);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap1);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(2L)).thenReturn(valueMap2);

        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, environmentLevelModuleName);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyEnvironmentLevelPermission_WildcardHasPermission_returnTrue() {
        // Given
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        PermissionSettingsValue value1 = new PermissionSettingsValue();
        value1.setCreateAccess(false);
        value1.setReadAccess(true);
        value1.setUpdateAccess(false);
        value1.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value1);

        PermissionSettingsValue value2 = new PermissionSettingsValue();
        value2.setCreateAccess(false);
        value2.setReadAccess(false);
        value2.setUpdateAccess(false);
        value2.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> environment1ValueMap = new HashMap<>();
        environment1ValueMap.put("feature1", value2);

        PermissionSettingsValue value3 = new PermissionSettingsValue();
        value3.setCreateAccess(false);
        value3.setReadAccess(false);
        value3.setUpdateAccess(false);
        value3.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> environment2ValueMap = new HashMap<>();
        environment2ValueMap.put("feature1", value3);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(wildcardValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(environment1ValueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(2L)).thenReturn(environment2ValueMap);

        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, environmentLevelModuleName);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyEnvironmentLevelPermission_WildcardHasPermissionAndNoEnvironmentSettingsPresent_returnTrue() {
        // Given
        environmentLevelPermissions.removeAll();
        environmentLevelPermissions.put("*", 6L);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> wildcardValueMap = new HashMap<>();
        wildcardValueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(6L)).thenReturn(wildcardValueMap);

        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, environmentLevelModuleName);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }

    @Test
    public void hasAnyEnvironmentLevelPermission_featureNotListedForAnyEnvironment_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(false);
        value.setReadAccess(true);
        value.setUpdateAccess(false);
        value.setDeleteAccess(false);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(2L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature2",
                SecureController.AccessType.READ, environmentLevelModuleName);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyEnvironmentLevelPermission_noPermissionForAccessTypeInAnyEnvironment_returnFalse() {
        // Given
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setCreateAccess(true);
        value.setReadAccess(false);
        value.setUpdateAccess(true);
        value.setDeleteAccess(true);

        HashMap<String, PermissionSettingsValue> valueMap = new HashMap<>();
        valueMap.put("feature1", value);

        Mockito.when(mockValueService.findAllByPermissionSettingsId(3L)).thenReturn(valueMap);
        Mockito.when(mockValueService.findAllByPermissionSettingsId(2L)).thenReturn(valueMap);

        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, environmentLevelModuleName);

        // Then
        assertThat("The return value should be false", hasPermission, equalTo(false));
    }

    @Test
    public void hasAnyEnvironmentLevelPermission_adminUser_returnTrue() {
        // Given
        ((ObjectNode) sessionJson.get("userPermissions")).put("admin", true);
        sessionData = new SessionData(jpaApi, mockValueService, sessionJson);

        // When
        boolean hasPermission = sessionData.hasAnyEnvironmentLevelPermission("feature1",
                SecureController.AccessType.READ, environmentLevelModuleName);

        // Then
        assertThat("The return value should be true", hasPermission, equalTo(true));
    }
}
