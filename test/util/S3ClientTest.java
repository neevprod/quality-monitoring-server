package util;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest({S3Client.class})
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class S3ClientTest {
    private static final String bucketName = "product-validation-math-scoring-manifest";
    private static final String key = "product_validation_manifest_41.json";
    private static final S3Object s3Object = PowerMockito.mock(S3Object.class);
    private static S3Client client;

    @BeforeClass
    public static void init() throws Exception {

        AmazonS3Client s3Client = PowerMockito.mock(AmazonS3Client.class);
        PowerMockito.when(s3Client.getObject(bucketName, key)).thenReturn(s3Object);
        PowerMockito.whenNew(AmazonS3Client.class).withAnyArguments().thenReturn(s3Client);

        client = new S3Client("us-east-1");
        client.setAmazonS3Client(s3Client);
    }

    @Test
    public void downloadFileFromS3_non_empty_content() throws AmazonClientException, IOException {
        //Given
        String dummyText = "I am dummy!!!";
        S3ObjectInputStream mockInputStream = new S3ObjectInputStream(new ByteArrayInputStream(dummyText.getBytes()), null);
        Mockito.doReturn(mockInputStream).when(s3Object).getObjectContent();

        //When
        int totalLines = 0;
        try (BufferedReader br = client.downFileFromS3(bucketName, key)) {
            while (br.readLine() != null) {
                totalLines++;
            }
        }

        //Then
        assertThat("The count of lines should be one.", totalLines, equalTo(1));
    }

    public void downloadFileFromS3_empty_content() throws AmazonClientException, IOException {
        //Given
        String dummyText = "";
        S3ObjectInputStream mockInputStream = new S3ObjectInputStream(new ByteArrayInputStream(dummyText.getBytes()), null);
        Mockito.doReturn(mockInputStream).when(s3Object).getObjectContent();

        //When
        int totalLines = 0;
        try (BufferedReader br = client.downFileFromS3(bucketName, key)) {
            while (br.readLine() != null) {
                totalLines++;
            }
        }

        //Then
        assertThat("The count of lines should be zero.", totalLines, equalTo(0));
    }
}
