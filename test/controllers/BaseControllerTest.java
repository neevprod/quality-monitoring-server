package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BaseControllerTest {

    private class BaseControllerImpl extends BaseController { }

    private BaseControllerImpl baseController;

    @Before
    public void initialize() {
        baseController = new BaseControllerImpl();
    }

    @Test
    public void successResult_noData_correctResultReturned() throws Exception {
        // When
        Result result = baseController.successResult();

        // Then
        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void successResult_withDataAsJson_correctResultReturned() throws Exception {
        // Given
        ObjectNode data = Json.newObject();
        data.put("something", "value");

        // When
        Result result = baseController.successResult(data);

        // Then
        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        assertThat("Result should contain correct data", resultJson.get("data").get("something").asText(),
                equalTo(data.get("something").asText()));
    }

    @Test
    public void successResult_withDataAsJsonAndErrors_correctResultReturned() throws Exception {
        // Given
        ObjectNode data = Json.newObject();
        data.put("something", "value");
        List<String> messages = new ArrayList<>();
        messages.add("error 1");
        messages.add("error 2");

        // When
        Result result = baseController.successResult(data, messages);

        // Then
        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        assertThat("Result should contain correct data", resultJson.get("data").get("something").asText(),
                equalTo(data.get("something").asText()));
        assertThat("Result should contain first message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo(messages.get(0)));
        assertThat("Result should contain second message", resultJson.get("errors").get(1).get("message").asText(),
                equalTo(messages.get(1)));
    }

    @Test
    public void successResult_withDataAsObject_correctResultReturned() throws Exception {
        // Given
        ArrayList<String> data = new ArrayList<>();
        data.add("One");
        data.add("Two");
        data.add("Three");
        String dataName = "name";

        // When
        Result result = baseController.successResult(data, dataName);

        // Then
        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        assertThat("Result should contain correct data", resultJson.get("data").get(dataName),
                equalTo(Json.toJson(data)));
    }

    @Test
    public void failResult_noData_correctResultReturned() throws Exception {
        // Given
        int statusCode = Http.Status.BAD_REQUEST;
        String message = "Some error occurred.";
        ObjectNode data = Json.newObject();
        data.put("something", "value");

        // When
        Result result = baseController.failResult(statusCode, message);

        // Then
        assertThat("Result should have the specified status", result.status(), equalTo(statusCode));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain correct message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo(message));
    }

    @Test
    public void failResult_noDataWithMultipleErrors_correctResultReturned() throws Exception {
        // Given
        int statusCode = Http.Status.BAD_REQUEST;
        List<String> messages = new ArrayList<>();
        messages.add("error 1");
        messages.add("error 2");
        ObjectNode data = Json.newObject();
        data.put("something", "value");

        // When
        Result result = baseController.failResult(statusCode, messages);

        // Then
        assertThat("Result should have the specified status", result.status(), equalTo(statusCode));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain first message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo(messages.get(0)));
        assertThat("Result should contain second message", resultJson.get("errors").get(1).get("message").asText(),
                equalTo(messages.get(1)));
    }

    @Test
    public void failResult_withDataAsJson_correctResultReturned() throws Exception {
        // Given
        int statusCode = Http.Status.UNAUTHORIZED;
        String message = "Some error occurred.";
        ObjectNode data = Json.newObject();
        data.put("something", "value");

        // When
        Result result = baseController.failResult(statusCode, message, data);

        // Then
        assertThat("Result should have the specified status", result.status(), equalTo(statusCode));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain correct message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo(message));
        assertThat("Result should contain correct data", resultJson.get("data").get("something").asText(),
                equalTo(data.get("something").asText()));
    }

    @Test
    public void failResult_withDataAsJsonAndMultipleErrors_correctResultReturned() throws Exception {
        // Given
        int statusCode = Http.Status.UNAUTHORIZED;
        List<String> messages = new ArrayList<>();
        messages.add("error 1");
        messages.add("error 2");
        ObjectNode data = Json.newObject();
        data.put("something", "value");

        // When
        Result result = baseController.failResult(statusCode, messages, data);

        // Then
        assertThat("Result should have the specified status", result.status(), equalTo(statusCode));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain the first message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo(messages.get(0)));
        assertThat("Result should contain the second message", resultJson.get("errors").get(1).get("message").asText(),
                equalTo(messages.get(1)));
        assertThat("Result should contain correct data", resultJson.get("data").get("something").asText(),
                equalTo(data.get("something").asText()));
    }

    @Test
    public void failResult_withDataAsObject_correctResultReturned() throws Exception {
        // Given
        int statusCode = Http.Status.BAD_REQUEST;
        String message = "Some error occurred.";
        ArrayList<String> data = new ArrayList<>();
        data.add("One");
        data.add("Two");
        data.add("Three");
        String dataName = "name";

        // When
        Result result = baseController.failResult(statusCode, message, data, dataName);

        // Then
        assertThat("Result should have the specified status", result.status(), equalTo(statusCode));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain correct message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo(message));
        assertThat("Result should contain correct data", resultJson.get("data").get(dataName),
                equalTo(Json.toJson(data)));
    }

    @Test
    public void failResult_withDataAsObjectAndMultipleErrors_correctResultReturned() throws Exception {
        // Given
        int statusCode = Http.Status.BAD_REQUEST;
        List<String> messages = new ArrayList<>();
        messages.add("error 1");
        messages.add("error 2");
        ArrayList<String> data = new ArrayList<>();
        data.add("One");
        data.add("Two");
        data.add("Three");
        String dataName = "name";

        // When
        Result result = baseController.failResult(statusCode, messages, data, dataName);

        // Then
        assertThat("Result should have the specified status", result.status(), equalTo(statusCode));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain the first message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo(messages.get(0)));
        assertThat("Result should contain the second message", resultJson.get("errors").get(1).get("message").asText(),
                equalTo(messages.get(1)));
        assertThat("Result should contain correct data", resultJson.get("data").get(dataName),
                equalTo(Json.toJson(data)));
    }

    @Test
    public void accessDeniedResult_correctResultReturned() throws Exception {
        // When
        Result result = baseController.accessDeniedResult();

        // Then
        assertThat("Result should have a FORBIDDEN status", result.status(), equalTo(Http.Status.FORBIDDEN));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain correct message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo("You do not have sufficient permission to perform this request."));
    }

    @Test
    public void jsonResult_correctResultReturned() throws Exception {
        // Given
        int statusCode = Http.Status.OK;

        ObjectNode content = Json.newObject();
        content.put("success", "true");
        ObjectNode data = content.putObject("data");
        data.put("field1", 123);
        data.put("field2", 345);

        // When
        Result result = baseController.jsonResult(statusCode, content);

        // Then
        assertThat("Result should have the specified status", result.status(), equalTo(statusCode));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.stringify(content), Json.stringify(resultJson), true);
    }

    @Test
    public void jobStartResult_correctResultReturned() throws Exception {
        // Given
        Object jobStartResponse = "jobId_1";

        // When
        Result result = baseController.jobStartResult(jobStartResponse);

        // Then
        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        assertThat("Result should contain job ID", resultJson.get("data").get("jobId").asText(),
                equalTo(jobStartResponse));
    }

    @Test
    public void jobCheckResult_jobExists_successResultReturned() throws Exception {
        // Given
        ObjectNode jobCheckResponse = Json.newObject();
        jobCheckResponse.put("isRunning", false);
        jobCheckResponse.put("success", true);

        // When
        Result result = baseController.jobCheckResult(jobCheckResponse);

        // Then
        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        assertThat("Result should contain is running status", resultJson.get("data").get("isRunning").asBoolean(),
                equalTo(false));
        assertThat("Result should contain job success status", resultJson.get("data").get("success").asBoolean(),
                equalTo(true));
    }

    @Test
    public void jobStartResult_jobDoesNotExist_failResultReturned() throws Exception {
        // When
        Result result = baseController.jobCheckResult(false);

        // Then
        assertThat("Result should have the correct status", result.status(), equalTo(Http.Status.BAD_REQUEST));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result should contain correct message", resultJson.get("errors").get(0).get("message").asText(),
                equalTo("The given job does not exist."));
    }

}
