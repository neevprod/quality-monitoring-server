package controllers;

import com.auth0.jwt.JWTVerifier;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import global.TestWithApplication;
import models.*;
import models.qtivalidation.PreviewerTenant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.ApiUserService;
import services.usermanagement.HumanUserService;
import services.usermanagement.UserPermissionsService;
import util.ActiveDirectoryAuthenticator;

import javax.inject.Provider;
import java.time.Clock;
import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JWTVerifier.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class LoginControllerTest extends TestWithApplication {
    private User user;
    private HumanUser humanUser;
    private LoginUser loginUser;
    private ApiUser apiUser;
    private HumanUserService humanUserService;
    private ApiUserService apiUserService;
    private UserPermissionsService userPermissionsService;
    private List<UserPermissions> userPermissionsList;
    private Clock clock;
    private LoginController loginController;

    private ActiveDirectoryAuthenticator mockedAdAuth;
    private Provider<ActiveDirectoryAuthenticator> mockedAdAuthProvider;

    @Mock
    private JavaContextComponents contextComponents;

    private JsonNode getJsonFromToken(String token) throws Exception {
        String[] parts = token.split("\\.");
        if (parts.length < 2) {
            throw new IllegalArgumentException("The given token is not in the proper format.");
        }
        String jsonString = new String(org.apache.commons.codec.binary.Base64.decodeBase64(parts[1]), "UTF-8");
        return Json.mapper().readValue(jsonString, JsonNode.class);
    }

    @Before
    public void setUp() {
        user = mock(User.class);
        when(user.getId()).thenReturn(1L);
        when(user.getFullname()).thenReturn("Tester Testington");
        when(user.getIsAdmin()).thenReturn(false);

        humanUser = mock(HumanUser.class);
        when(humanUser.getNetworkId()).thenReturn("tester");
        when(humanUser.getEmail()).thenReturn("tester@pearson.com");
        when(humanUser.getUser()).thenReturn(user);

        apiUser = mock(ApiUser.class);
        when(apiUser.getApiKey()).thenReturn("12345");
        when(apiUser.getUser()).thenReturn(user);

        humanUserService = mock(HumanUserService.class);
        when(humanUserService.findByNetworkId("tester")).thenReturn(Optional.of(humanUser));
        when(humanUserService.findByEmail("tester@pearson.com")).thenReturn(Optional.of(humanUser));

        apiUserService = mock(ApiUserService.class);
        when(apiUserService.findByApiKeyName("apiUser")).thenReturn(Optional.of(apiUser));

        userPermissionsList = new ArrayList<>();
        userPermissionsService = mock(UserPermissionsService.class);
        when(userPermissionsService.findAllByUserId(1L)).thenReturn(userPermissionsList);

        mockedAdAuth = Mockito.mock(ActiveDirectoryAuthenticator.class);
        mockedAdAuthProvider = Mockito.mock(Provider.class);
        Mockito.when(mockedAdAuthProvider.get()).thenReturn(mockedAdAuth);

        when(mockedAdAuth.authenticate("tester", "test")).thenReturn(true);

        clock = app.injector().instanceOf(Clock.class);

        // Unfortunately, we have to do this because the JWT library doesn't allow you to provide an evaluation time.
        PowerMockito.mockStatic(System.class);
        PowerMockito.when(System.currentTimeMillis()).thenReturn(clock.millis());

        loginController = new LoginController(app.config(), clock, humanUserService, apiUserService,
                userPermissionsService, app.injector().instanceOf(FormFactory.class), mockedAdAuthProvider);
    }

    @Test
    public void login_correctCredentials_returnValidToken() throws Exception {
        // Given
        when(user.getIsAdmin()).thenReturn(true);

        ObjectNode requestJson = Json.newObject();
        requestJson.put("networkId", "tester");
        requestJson.put("password", "test");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        String jwt = resultJson.get("data").get("token").asText();

        JWTVerifier jwtVerifier = new JWTVerifier(app.config().getString("play.http.secret.key"));
        jwtVerifier.verify(jwt);
        JsonNode jsonClaims = getJsonFromToken(jwt);

        assertThat("Session should contain a JWT ID", jsonClaims.get("jti").asText(), notNullValue());
        assertThat("Session should contain correct user ID", jsonClaims.get("userId").asLong(), equalTo(1L));
        assertThat("Session should contain correct user email", jsonClaims.get("userEmail").asText(),
                equalTo("tester@pearson.com"));
        assertThat("Session should contain correct username", jsonClaims.get("userName").asText(),
                equalTo("Tester Testington"));
        assertThat("Session should contain correct apiUser value", jsonClaims.get("apiUser").asBoolean(),
                equalTo(false));
        assertThat("Session should contain correct admin value",
                jsonClaims.get("userPermissions").get("admin").asBoolean(), equalTo(true));
        assertThat("Session should contain correct issued at time", jsonClaims.get("iat").asLong(),
                equalTo(clock.millis() / 1000L));
        assertThat("Session should contain correct expiration time", jsonClaims.get("exp").asLong(),
                equalTo(clock.millis() / 1000L + app.config().getDuration("session.timeout").getSeconds()));
    }

    @Test
    public void login_correctCredentialsApiUser_returnValidToken() throws Exception {
        // Given
        when(user.getIsAdmin()).thenReturn(true);

        ObjectNode requestJson = Json.newObject();
        requestJson.put("apiKeyName", "apiUser");
        requestJson.put("apiKey", "12345");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        String jwt = resultJson.get("data").get("token").asText();
        JWTVerifier jwtVerifier = new JWTVerifier(app.config().getString("play.http.secret.key"));
        jwtVerifier.verify(jwt);
        JsonNode jsonClaims = getJsonFromToken(jwt);

        assertThat("Session should contain a JWT ID", jsonClaims.get("jti").asText(), notNullValue());
        assertThat("Session should contain correct user ID", jsonClaims.get("userId").asLong(), equalTo(1L));
        assertThat("Session should not contain a user email", jsonClaims.get("userEmail"), equalTo(null));
        assertThat("Session should contain correct username", jsonClaims.get("userName").asText(),
                equalTo("Tester Testington"));
        assertThat("Session should contain correct apiUser value", jsonClaims.get("apiUser").asBoolean(),
                equalTo(true));
        assertThat("Session should contain correct admin value",
                jsonClaims.get("userPermissions").get("admin").asBoolean(), equalTo(true));
        assertThat("Session should contain correct issued at time", jsonClaims.get("iat").asLong(),
                equalTo(clock.millis() / 1000L));
        assertThat("Session should contain correct expiration time", jsonClaims.get("exp").asLong(),
                equalTo(clock.millis() / 1000L + app.config().getDuration("session.timeout").getSeconds()));
    }

    @Test
    public void login_correctCredentialsNonAdminUser_tokenHasPermissions() throws Exception {
        // Given
        PermissionType permissionType = new PermissionType();
        permissionType.setPermissionTypeName("TenantLevelPermission");
        ApplicationModule applicationModule = new ApplicationModule();
        applicationModule.setApplicationModuleName("module1");
        applicationModule.setPermissionType(permissionType);

        PermissionOption option = new PermissionOption();
        option.setName("feature1");
        PermissionSettingsValue value = new PermissionSettingsValue();
        value.setPermissionOption(option);
        Set<PermissionSettingsValue> values = new HashSet<>();
        values.add(value);

        UserPermissions userPermissions1 = new UserPermissions(1, 3L, 4L);
        userPermissions1.setApplicationModule(applicationModule);
        userPermissions1.setIsWildcard(false);
        userPermissions1.setPermissionSettings(new PermissionSettings());
        userPermissions1.getPermissionSettings().setPermissionSettingsValues(values);
        UserPermissions userPermissions2 = new UserPermissions(2, 6L, 3L);
        userPermissions2.setApplicationModule(applicationModule);
        userPermissions2.setIsWildcard(false);
        userPermissions2.setPermissionSettings(new PermissionSettings());
        userPermissions2.getPermissionSettings().setPermissionSettingsValues(values);
        UserPermissions userPermissions3 = new UserPermissions();
        userPermissions3.setApplicationModule(applicationModule);
        userPermissions3.setPreviewerId(2);
        userPermissions3.setPermissionSettingsId(2L);
        userPermissions3.setIsWildcard(true);
        userPermissions3.setPermissionSettings(new PermissionSettings());
        userPermissions3.getPermissionSettings().setPermissionSettingsValues(values);
        userPermissionsList.add(userPermissions1);
        userPermissionsList.add(userPermissions2);
        userPermissionsList.add(userPermissions3);

        ObjectNode requestJson = Json.newObject();
        requestJson.put("networkId", "tester");
        requestJson.put("password", "test");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        String jwt = resultJson.get("data").get("token").asText();
        JWTVerifier jwtVerifier = new JWTVerifier(app.config().getString("play.http.secret.key"));
        jwtVerifier.verify(jwt);
        JsonNode jsonClaims = getJsonFromToken(jwt);
        JsonNode userPermissionsJson = jsonClaims.get("userPermissions").get("modulePermissions").get(0)
                .get("permissions");

        assertThat("Claims should contain first tenant permission", userPermissionsJson.get("1").get("3").longValue(),
                equalTo(4L));
        assertThat("Claims should contain second tenant permission", userPermissionsJson.get("2").get("6").longValue(),
                equalTo(3L));
        assertThat("Claims should contain third tenant permission", userPermissionsJson.get("2").get("*").longValue(),
                equalTo(2L));
    }

    @Test
    public void login_correctCredentialsNonAdminUser_returnCorrectUserPermissions() throws Exception {
        // Given
        PermissionType tenantPermissionType = new PermissionType();
        tenantPermissionType.setPermissionTypeName("TenantLevelPermission");
        ApplicationModule tenantApplicationModule = new ApplicationModule();
        tenantApplicationModule.setApplicationModuleName("tenantModule");
        tenantApplicationModule.setPermissionType(tenantPermissionType);

        PermissionOption tenantOption = new PermissionOption();
        tenantOption.setName("feature1");
        PermissionSettingsValue tenantValue = new PermissionSettingsValue();
        tenantValue.setPermissionOption(tenantOption);
        tenantValue.setCreateAccess(true);
        tenantValue.setUpdateAccess(true);
        Set<PermissionSettingsValue> tenantValues = new HashSet<>();
        tenantValues.add(tenantValue);

        PreviewerTenant tenant1 = new PreviewerTenant();
        tenant1.setPreviewerTenantId(1L);
        tenant1.setPreviewerId(1);
        tenant1.setTenantId(3L);
        PreviewerTenant tenant2 = new PreviewerTenant();
        tenant2.setPreviewerTenantId(2L);
        tenant2.setPreviewerId(2);
        tenant2.setTenantId(6L);

        UserPermissions tenantUserPermissions1 = new UserPermissions(1, 3L, 4L);
        tenantUserPermissions1.setApplicationModule(tenantApplicationModule);
        tenantUserPermissions1.setIsWildcard(false);
        tenantUserPermissions1.setPermissionSettings(new PermissionSettings());
        tenantUserPermissions1.getPermissionSettings().setPermissionSettingsValues(tenantValues);
        UserPermissions tenantUserPermissions2 = new UserPermissions(2, 6L, 3L);
        tenantUserPermissions2.setApplicationModule(tenantApplicationModule);
        tenantUserPermissions2.setIsWildcard(false);
        tenantUserPermissions2.setPermissionSettings(new PermissionSettings());
        tenantUserPermissions2.getPermissionSettings().setPermissionSettingsValues(tenantValues);
        UserPermissions tenantUserPermissions3 = new UserPermissions();
        tenantUserPermissions3.setApplicationModule(tenantApplicationModule);
        tenantUserPermissions3.setPreviewerId(3);
        tenantUserPermissions3.setPermissionSettingsId(4L);
        tenantUserPermissions3.setIsWildcard(true);
        tenantUserPermissions3.setPermissionSettings(new PermissionSettings());
        tenantUserPermissions3.getPermissionSettings().setPermissionSettingsValues(tenantValues);
        userPermissionsList.add(tenantUserPermissions1);
        userPermissionsList.add(tenantUserPermissions2);
        userPermissionsList.add(tenantUserPermissions3);

        PermissionType modulePermissionType = new PermissionType();
        modulePermissionType.setPermissionTypeName("ModuleLevelPermission");
        ApplicationModule moduleApplicationModule = new ApplicationModule();
        moduleApplicationModule.setApplicationModuleName("moduleModule");
        moduleApplicationModule.setPermissionType(modulePermissionType);

        PermissionOption moduleOption = new PermissionOption();
        moduleOption.setName("feature2");
        PermissionSettingsValue moduleValue = new PermissionSettingsValue();
        moduleValue.setPermissionOption(moduleOption);
        moduleValue.setReadAccess(true);
        Set<PermissionSettingsValue> moduleValues = new HashSet<>();
        moduleValues.add(moduleValue);

        UserPermissions moduleUserPermissions = new UserPermissions();
        moduleUserPermissions.setPermissionSettingsId(5L);
        moduleUserPermissions.setApplicationModule(moduleApplicationModule);
        moduleUserPermissions.setIsWildcard(false);
        moduleUserPermissions.setPermissionSettings(new PermissionSettings());
        moduleUserPermissions.getPermissionSettings().setPermissionSettingsValues(moduleValues);
        userPermissionsList.add(moduleUserPermissions);

        PermissionType environmentPermissionType = new PermissionType();
        environmentPermissionType.setPermissionTypeName("EnvironmentLevelPermission");
        ApplicationModule environmentApplicationModule = new ApplicationModule();
        environmentApplicationModule.setApplicationModuleName("environmentModule");
        environmentApplicationModule.setPermissionType(environmentPermissionType);

        PermissionOption environmentOption = new PermissionOption();
        environmentOption.setName("feature3");
        PermissionSettingsValue environmentValue = new PermissionSettingsValue();
        environmentValue.setPermissionOption(environmentOption);
        environmentValue.setUpdateAccess(true);
        environmentValue.setDeleteAccess(true);
        Set<PermissionSettingsValue> environmentValues = new HashSet<>();
        environmentValues.add(environmentValue);

        UserPermissions environmentUserPermissions = new UserPermissions();
        environmentUserPermissions.setPermissionSettingsId(6L);
        environmentUserPermissions.setApplicationModule(environmentApplicationModule);
        environmentUserPermissions.setIsWildcard(false);
        environmentUserPermissions.setEnvironmentId(3);
        environmentUserPermissions.setPermissionSettings(new PermissionSettings());
        environmentUserPermissions.getPermissionSettings().setPermissionSettingsValues(environmentValues);
        userPermissionsList.add(environmentUserPermissions);

        ObjectNode requestJson = Json.newObject();
        requestJson.put("networkId", "tester");
        requestJson.put("password", "test");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode userPermissions = resultJson.get("data").get("userPermissions");

        assertThat("isAdmin should be set to false in user permissions", userPermissions.get("admin").asBoolean(),
                equalTo(false));

        String expectedPermissionsList = "[{\n" + "    \"permissionType\": \"ModuleLevelPermission\",\n"
                + "    \"moduleName\": \"moduleModule\",\n" + "    \"permissions\": [{\n"
                + "        \"name\": \"feature2\",\n" + "        \"createAccess\": false,\n"
                + "        \"readAccess\": true,\n" + "        \"updateAccess\": false,\n"
                + "        \"deleteAccess\": false\n" + "    }]\n" + "}, {\n"
                + "    \"permissionType\": \"EnvironmentLevelPermission\",\n"
                + "    \"moduleName\": \"environmentModule\",\n" + "    \"environmentPermissions\": [{\n"
                + "        \"environmentId\": 3,\n" + "        \"permissions\": [{\n"
                + "            \"name\": \"feature3\",\n" + "            \"createAccess\": false,\n"
                + "            \"readAccess\": false,\n" + "            \"updateAccess\": true,\n"
                + "            \"deleteAccess\": true\n" + "        }]\n" + "    }]\n" + "}, {\n"
                + "    \"permissionType\": \"TenantLevelPermission\",\n" + "    \"moduleName\": \"tenantModule\",\n"
                + "    \"previewerPermissions\": [{\n" + "        \"previewerId\": 1,\n"
                + "        \"tenantPermissions\": [{\n" + "            \"tenantId\": 3,\n"
                + "            \"permissions\": [{\n" + "                \"name\": \"feature1\",\n"
                + "                \"createAccess\": true,\n" + "                \"readAccess\": false,\n"
                + "                \"updateAccess\": true,\n" + "                \"deleteAccess\": false\n"
                + "            }]\n" + "        }]\n" + "    }, {\n" + "        \"previewerId\": 2,\n"
                + "        \"tenantPermissions\": [{\n" + "            \"tenantId\": 6,\n"
                + "            \"permissions\": [{\n" + "                \"name\": \"feature1\",\n"
                + "                \"createAccess\": true,\n" + "                \"readAccess\": false,\n"
                + "                \"updateAccess\": true,\n" + "                \"deleteAccess\": false\n"
                + "            }]\n" + "        }]\n" + "    }, {\n" + "        \"previewerId\": 3,\n"
                + "        \"tenantPermissions\": [{\n" + "            \"tenantId\": \"*\",\n"
                + "            \"permissions\": [{\n" + "                \"name\": \"feature1\",\n"
                + "                \"createAccess\": true,\n" + "                \"readAccess\": false,\n"
                + "                \"updateAccess\": true,\n" + "                \"deleteAccess\": false\n"
                + "            }]\n" + "        }]\n" + "    }]\n" + "}]";
        JSONAssert.assertEquals(expectedPermissionsList, userPermissions.get("modulePermissions").toString(), false);
    }

    @Test
    public void login_correctCredentialsAdminUser_tokenHasNoPermissions() throws Exception {
        // Given
        when(user.getIsAdmin()).thenReturn(true);

        userPermissionsList.add(new UserPermissions(1, 3L, 4L));
        userPermissionsList.add(new UserPermissions(2, 6L, 3L));
        when(userPermissionsService.findAllByUserId(1L)).thenReturn(userPermissionsList);

        ObjectNode requestJson = Json.newObject();
        requestJson.put("networkId", "tester");
        requestJson.put("password", "test");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        String jwt = resultJson.get("data").get("token").asText();
        JWTVerifier jwtVerifier = new JWTVerifier(app.config().getString("play.http.secret.key"));
        jwtVerifier.verify(jwt);
        JsonNode jsonClaims = getJsonFromToken(jwt);

        assertThat("Claims should not contain permissions",
                jsonClaims.get("userPermissions").get("modulePermissions").size(), equalTo(0));
    }

    @Test
    public void login_correctCredentialsAdminUser_returnCorrectUserPermissions() throws Exception {
        // Given
        when(user.getIsAdmin()).thenReturn(true);

        userPermissionsList.add(new UserPermissions(1, 3L, 4L));
        userPermissionsList.add(new UserPermissions(2, 6L, 3L));
        when(userPermissionsService.findAllByUserId(1L)).thenReturn(userPermissionsList);

        ObjectNode requestJson = Json.newObject();
        requestJson.put("networkId", "tester");
        requestJson.put("password", "test");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode userPermissions = resultJson.get("data").get("userPermissions");

        assertThat("isAdmin should be set to false in user permissions", userPermissions.get("admin").asBoolean(),
                equalTo(true));
        assertThat("permissionsList should not be present in user permissions",
                userPermissions.get("modulePermissions"), equalTo(null));
    }

    @Test
    public void login_incorrectUsername_returnError() throws Exception {
        // Given
        when(humanUserService.findByNetworkId("tester")).thenReturn(Optional.empty());

        loginController = Mockito.spy(new LoginController(app.config(), clock, humanUserService, apiUserService,
                userPermissionsService, app.injector().instanceOf(FormFactory.class), mockedAdAuthProvider));

        ObjectNode requestJson = Json.newObject();
        requestJson.put("networkId", "tester");
        requestJson.put("password", "test");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        Mockito.verify(loginController).failResult(Http.Status.UNAUTHORIZED,
                "Invalid networkId and password combination.");
    }

    @Test
    public void login_noUsername_returnError() throws Exception {
        // Given
        ObjectNode requestJson = Json.newObject();
        requestJson.put("password", "test");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);
        loginController = Mockito.spy(new LoginController(app.config(), clock, humanUserService, apiUserService,
                userPermissionsService, app.injector().instanceOf(FormFactory.class), mockedAdAuthProvider));

        // When
        Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        Mockito.verify(loginController).failResult(Http.Status.BAD_REQUEST, "NetworkId or API key name is required.");
    }

    @Test
    public void login_noPassword_returnError() throws Exception {
        // Given
        ObjectNode requestJson = Json.newObject();
        requestJson.put("networkId", "tester");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);
        loginController = Mockito.spy(new LoginController(app.config(), clock, humanUserService, apiUserService,
                userPermissionsService, app.injector().instanceOf(FormFactory.class), mockedAdAuthProvider));

        // When
        Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        Mockito.verify(loginController).failResult(Http.Status.BAD_REQUEST, "NetworkId and password are required.");
    }

    @Test
    public void login_incorrectApiKeyName_returnError() throws Exception {
        // Given
        when(apiUserService.findByApiKeyName("apiUser")).thenReturn(Optional.empty());

        loginController = Mockito.spy(new LoginController(app.config(), clock, humanUserService, apiUserService,
                userPermissionsService, app.injector().instanceOf(FormFactory.class), mockedAdAuthProvider));

        ObjectNode requestJson = Json.newObject();
        requestJson.put("apiKeyName", "apiUser");
        requestJson.put("apiKey", "12345");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        Mockito.verify(loginController).failResult(Http.Status.UNAUTHORIZED,
                "Invalid API key name and API key combination.");
    }

    @Test
    public void login_incorrectApiKey_returnError() throws Exception {
        // Given
        loginController = Mockito.spy(new LoginController(app.config(), clock, humanUserService, apiUserService,
                userPermissionsService, app.injector().instanceOf(FormFactory.class), mockedAdAuthProvider));

        ObjectNode requestJson = Json.newObject();
        requestJson.put("apiKeyName", "apiUser");
        requestJson.put("apiKey", "1234");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        Mockito.verify(loginController).failResult(Http.Status.UNAUTHORIZED,
                "Invalid API key name and API key combination.");
    }

    @Test
    public void login_noApiKey_returnError() throws Exception {
        // Given
        loginController = Mockito.spy(new LoginController(app.config(), clock, humanUserService, apiUserService,
                userPermissionsService, app.injector().instanceOf(FormFactory.class), mockedAdAuthProvider));

        ObjectNode requestJson = Json.newObject();
        requestJson.put("apiKeyName", "apiUser");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);

        // When
        Helpers.invokeWithContext(requestBuilder, contextComponents, () -> loginController.login("v1"));

        // Then
        Mockito.verify(loginController).failResult(Http.Status.BAD_REQUEST, "API key name and API key are required.");
    }

}