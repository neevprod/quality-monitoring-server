package controllers.systemvalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import global.TestWithApplication;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.systemvalidation.ViewStatsFailureService;
import util.SessionData;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ViewStatsFailureControllerTest extends TestWithApplication {
    private SessionData sessionData;
    private ViewStatsFailureService viewStatsFailureService;
    private ViewStatsFailureController viewStatsFailureController;
    private Http.RequestBuilder requestBuilder;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() {
        sessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(sessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        viewStatsFailureService = Mockito.mock(ViewStatsFailureService.class);

        requestBuilder = new Http.RequestBuilder();

        viewStatsFailureController = Mockito.spy(new ViewStatsFailureController(sessionDataFactory,
                viewStatsFailureService));
    }

    @Test
    public void getStatsFailureReport_getAllStatsFailure() throws Exception {
        // Given
        requestBuilder.uri("?selectedDivJobIds=7");
        List<String> selectedDivJobIds = new ArrayList<>();
        selectedDivJobIds.add("7");
        List<Object[]> failedStats = new ArrayList<>();
        Object[] object = new Object[] {
                7,
                null,
                14,
                "REF081501",
                "65d645c8-3793-456f-8d6d-543402cf4685",
                "{\"studentTestComparisons\":[{\"mismatchedFields\":[{\"fieldName\":\"Item REF0815_0005\",\"itemStatus\":\"FIELD_TEST\",\"expectedValue\":\"0.0\",\"result\":\"NOTCOMPARED\"}]}]}" };
        failedStats.add(object);
        Mockito.doReturn(failedStats).when(viewStatsFailureService).findStatsFailureReport(selectedDivJobIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return viewStatsFailureController.getStatsFailureReport("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

}