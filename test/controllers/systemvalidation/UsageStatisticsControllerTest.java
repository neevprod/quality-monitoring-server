package controllers.systemvalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.SecureController;
import global.TestWithApplication;
import models.systemvalidation.Environment;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.systemvalidation.EnvironmentService;
import services.systemvalidation.UsageStatisticsService;
import util.SessionData;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class UsageStatisticsControllerTest extends TestWithApplication {
    private SessionData sessionData;
    private UsageStatisticsService usageStatisticsService;
    private UsageStatisticsController usageStatisticsController;
    private Http.RequestBuilder requestBuilder;
    private EnvironmentService environmentService;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() {
        sessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(sessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        usageStatisticsService = Mockito.mock(UsageStatisticsService.class);
        environmentService = Mockito.mock(EnvironmentService.class);

        requestBuilder = new Http.RequestBuilder();

        usageStatisticsController = Mockito.spy(new UsageStatisticsController(sessionDataFactory,
                usageStatisticsService, environmentService));
    }

    @Test
    public void getTestAttempts() throws Exception {
        // Given
        Integer environmentId = 2;
        String scopeTreePath = "All";
        String sDate = "1490466600000";
        String eDate = "1492799340000";
        Timestamp sTime = new Timestamp(Long.parseLong(sDate));
        Timestamp eTime = new Timestamp(Long.parseLong(eDate));
        List<Object[]> failedTestAttemptsForState = new ArrayList<>();
        List<Object[]> testAttemptsForForm = new ArrayList<>();
        Object[] object = new Object[] { "IRIS_PROCESSING_VALIDATOR", 2 };
        failedTestAttemptsForState.add(object);
        object = new Object[] { "REF081501", 2 };
        testAttemptsForForm.add(object);
        Mockito.doReturn(2).when(usageStatisticsService)
                .findAllTestAttempts(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(2).when(usageStatisticsService)
                .findAllPassedTestAttempts(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(2).when(usageStatisticsService)
                .findAllFailedTestAttempts(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(2).when(usageStatisticsService)
                .findAllPassedResumedTestAttempts(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(2).when(usageStatisticsService)
                .findAllFailedResumedTestAttempts(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(2).when(usageStatisticsService).findAllUniqueUsers(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(2).when(usageStatisticsService).findAllUniqueForms(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(failedTestAttemptsForState).when(usageStatisticsService)
                .findFailedTestAttemptsForState(environmentId, scopeTreePath, sTime, eTime);
        Mockito.doReturn(testAttemptsForForm).when(usageStatisticsService)
                .findFailedTestAttemptsForState(environmentId, scopeTreePath, sTime, eTime);

        Mockito.doReturn(true)
                .when(sessionData)
                .hasEnvironmentLevelPermission("usageStatistics", SecureController.AccessType.READ,
                        SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return usageStatisticsController.getTestAttempts("v1", 2, "All", sDate, eDate);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void getEnvironments_returnAllEnvironments() throws Exception {
        // Given
        List<Environment> environments = new ArrayList<>();
        Environment environment = new Environment();
        environment.setName("INT_REF_4.1");
        environment.setEnvironmentId(2);
        environment.setArchived(false);

        environments.add(environment);

        Mockito.doReturn(environments).when(environmentService).findAllEnvironmentsWithDivJob();

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return usageStatisticsController.getEnvironments("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("environments");

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(environments).toString(), resultData.toString(), true);
    }

    @Test
    public void getScopeTreePaths_returnAllScopeTreePaths() throws Exception {
        // Given
        Integer environmentId = 2;
        List<Object[]> scopeTreePaths = new ArrayList<>();
        Object[] object = new Object[] { "/ref/2015-16/refspr16" };
        scopeTreePaths.add(object);

        Mockito.doReturn(scopeTreePaths).when(usageStatisticsService).findAllScopeTreePath(environmentId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return usageStatisticsController.getScopeTreePaths("v1", 2);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("scopeTreePaths");

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(scopeTreePaths).toString(), resultData.toString(), true);
    }

    @Test
    public void getEnvironment_returnEnvironment() throws Exception {
        // Given
        Integer environmentId = 2;
        Environment environment = new Environment();
        environment.setEnvironmentId(2);
        environment.setName("INT_REF_4.1");

        ObjectNode node = Json.newObject();
        node.put("name", "INT_REF_4.1");
        node.put("environmentId", 2);

        Mockito.doReturn(Optional.of(environment)).when(environmentService).findByIdWithDivJob(environmentId);
        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return usageStatisticsController.getEnvironment("v1", 2);
        });
        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(node).toString(), resultJson.get("data").get("environment").toString(),
                true);
    }
}