package controllers.systemvalidation;

import akka.actor.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.SecureController;
import global.TestWithApplication;
import info.solidsoft.mockito.java8.LambdaMatcher;
import jobs.JobRunnerActor;
import models.qtivalidation.Previewer;
import models.qtivalidation.PreviewerTenant;
import models.systemvalidation.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.systemvalidation.*;
import util.ConnectionPoolManager;
import util.SessionData;

import javax.persistence.Tuple;
import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class DivJobControllerTest extends TestWithApplication {
    private SessionData sessionData;
    private DivJobExecService divJobExecService;
    private DivJobExecStatusService divJobExecStatusService;
    private StudentScenarioService studentScenarioService;
    private StudentScenarioResultService studentScenarioResultService;

    private StudentScenarioResultCommentService studentScenarioResultCommentService;
    private StudentScenarioResultCommentCategoryService studentScenarioResultCommentCategoryService;
    private ExternalDefectRepositoryService externalDefectRepositoryService;
    private StudentScenarioStatusService studentScenarioStatusService;
    private StateService stateService;
    private ConnectionPoolManager connectionPoolManager;

    private ScenarioService scenarioService;
    private EnvironmentService environmentService;
    private Http.RequestBuilder requestBuilder;
    private DivJobController divJobController;
    private DbConnectionService dbConnectionService;
    private ApiConnectionService apiConnectionService;
    private Set<String> queuedJobs;

    @Mock
    private JavaContextComponents contextComponents;

    private static class MockJobRunnerActor extends UntypedAbstractActor {
        private Set<String> queuedJobs;

        public MockJobRunnerActor(Set<String> queuedJobs) {
            this.queuedJobs = queuedJobs;
        }

        @Override
        public void onReceive(Object message) throws Exception {
            if (message instanceof String && JobRunnerActor.GET_RUNNING_JOBS_MESSAGE.equals(message)) {
                Set<String> runningJobs = new HashSet<>();

                ObjectNode returnData = Json.newObject();
                returnData.set("runningJobs", Json.toJson(runningJobs));
                returnData.set("queuedJobs", Json.toJson(queuedJobs));

                getSender().tell(returnData, getSelf());
            }
        }
    }

    @Before
    public void setUp() {
        sessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(sessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        divJobExecService = Mockito.mock(DivJobExecService.class);
        divJobExecStatusService = Mockito.mock(DivJobExecStatusService.class);
        studentScenarioService = Mockito.mock(StudentScenarioService.class);
        studentScenarioResultService = Mockito.mock(StudentScenarioResultService.class);
        scenarioService = Mockito.mock(ScenarioService.class);
        dbConnectionService = Mockito.mock(DbConnectionService.class);
        apiConnectionService = Mockito.mock(ApiConnectionService.class);
        environmentService = Mockito.mock(EnvironmentService.class);

        studentScenarioResultCommentService = Mockito.mock(StudentScenarioResultCommentService.class);
        studentScenarioResultCommentCategoryService = Mockito.mock(StudentScenarioResultCommentCategoryService.class);
        externalDefectRepositoryService = Mockito.mock(ExternalDefectRepositoryService.class);
        studentScenarioStatusService = Mockito.mock(StudentScenarioStatusService.class);
        stateService = Mockito.mock(StateService.class);
        connectionPoolManager = Mockito.mock(ConnectionPoolManager.class);

        queuedJobs = new HashSet<>();

        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        ActorRef jobRunnerActor = actorSystem.actorOf(Props.create(MockJobRunnerActor.class, queuedJobs));

        requestBuilder = new Http.RequestBuilder();

        divJobController = Mockito.spy(new DivJobController(sessionDataFactory, divJobExecService, divJobExecStatusService,
                studentScenarioService, studentScenarioResultService, scenarioService, environmentService,
                dbConnectionService, studentScenarioResultCommentService, apiConnectionService,
                studentScenarioResultCommentCategoryService, externalDefectRepositoryService,
                studentScenarioStatusService, stateService, jobRunnerActor,
                app.config(), connectionPoolManager));
    }

    @Test
    public void getEnvironments_adminUser_returnAllEnvironments() throws Exception {
        // Given
        List<Environment> environments = new ArrayList<>();

        Environment environment1 = new Environment();
        environment1.setEnvironmentId(1);
        environment1.setName("environment1");
        Environment environment2 = new Environment();
        environment2.setName("environment2");
        environment1.setEnvironmentId(2);

        environments.add(environment1);
        environments.add(environment2);

        Mockito.doReturn(environments).when(environmentService).findAll();
        Mockito.doReturn(true).when(sessionData).isAdmin();
        Mockito.doReturn(false).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironments("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("environments");

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(environments).toString(), resultData.toString(), true);
    }

    @Test
    public void getEnvironments_hasWildcardPermission_returnAllEnvironments() throws Exception {
        // Given
        List<Environment> environments = new ArrayList<>();

        Environment environment1 = new Environment();
        environment1.setEnvironmentId(1);
        environment1.setName("environment1");
        Environment environment2 = new Environment();
        environment2.setName("environment2");
        environment1.setEnvironmentId(2);

        environments.add(environment1);
        environments.add(environment2);

        Mockito.doReturn(environments).when(environmentService).findAll();
        Mockito.doReturn(false).when(sessionData).isAdmin();
        Mockito.doReturn(true).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironments("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("environments");

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(environments).toString(), resultData.toString(), true);
    }

    @Test
    public void getEnvironments_noAdminOrWildcardPermission_returnAllowedEnvironments() throws Exception {
        // Given
        List<Environment> environments = new ArrayList<>();

        Environment environment1 = new Environment();
        environment1.setEnvironmentId(1);
        environment1.setName("environment1");
        Environment environment2 = new Environment();
        environment2.setName("environment2");
        environment1.setEnvironmentId(2);

        environments.add(environment1);
        environments.add(environment2);

        Set<Integer> environmentIds = new HashSet<>();
        environmentIds.add(1);
        environmentIds.add(2);

        Mockito.doReturn(environments).when(environmentService).findAll(environmentIds);
        Mockito.doReturn(false).when(sessionData).isAdmin();
        Mockito.doReturn(false).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(environmentIds).when(sessionData)
                .getAllowedEnvironmentIds(SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironments("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("environments");

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(environments).toString(), resultData.toString(), true);
    }

    @Test
    public void getEnvironmentById_validIdWithAdminUser_returnEnvironment() throws Exception {
        // Given
        Integer environmentId = 1;

        Environment environment = new Environment();
        environment.setEnvironmentId(1);
        environment.setName("environment1");

        Mockito.doReturn(true).when(sessionData).isAdmin();
        Mockito.doReturn(false).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(environment)).when(environmentService).findById(environmentId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironmentById("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(environment).toString(),
                resultJson.get("data").get("environment").toString(), true);
    }

    @Test
    public void getEnvironmentById_invalidIdWithAdminUser_returnNotFoundMessage() throws Exception {
        // Given
        Integer environmentId = 1;

        Mockito.doReturn(true).when(sessionData).isAdmin();
        Mockito.doReturn(false).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.empty()).when(environmentService).findById(environmentId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironmentById("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("The correct error message should be returned",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find an environment with the given ID."));
    }

    @Test
    public void getEnvironmentById_validIdWithWildcardPermission_returnEnvironment() throws Exception {
        // Given
        Integer environmentId = 1;

        Environment environment = new Environment();
        environment.setEnvironmentId(1);
        environment.setName("environment1");

        Mockito.doReturn(false).when(sessionData).isAdmin();
        Mockito.doReturn(true).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(environment)).when(environmentService).findById(environmentId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironmentById("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(environment).toString(),
                resultJson.get("data").get("environment").toString(), true);
    }

    @Test
    public void getEnvironmentById_invalidIdWithWildcardPermission_returnNotFoundMessage() throws Exception {
        // Given
        Integer environmentId = 1;

        Mockito.doReturn(false).when(sessionData).isAdmin();
        Mockito.doReturn(true).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.empty()).when(environmentService).findById(environmentId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironmentById("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("The correct error message should be returned",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find an environment with the given ID."));
    }

    @Test
    public void getEnvironmentById_validIdWithRegularPermission_returnEnvironment() throws Exception {
        // Given
        Integer environmentId = 1;

        Environment environment = new Environment();
        environment.setEnvironmentId(1);
        environment.setName("environment1");

        Set<Integer> environmentIds = new HashSet<>();
        environmentIds.add(1);
        environmentIds.add(2);

        Mockito.doReturn(false).when(sessionData).isAdmin();
        Mockito.doReturn(false).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(environment)).when(environmentService).findById(environmentId);
        Mockito.doReturn(environmentIds).when(sessionData)
                .getAllowedEnvironmentIds(SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironmentById("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(environment).toString(),
                resultJson.get("data").get("environment").toString(), true);
    }

    @Test
    public void getEnvironmentById_validIdWithNoPermission_returnNotFoundMessage() throws Exception {
        // Given
        Integer environmentId = 1;

        Environment environment = new Environment();
        environment.setEnvironmentId(1);
        environment.setName("environment1");

        Set<Integer> environmentIds = new HashSet<>();
        environmentIds.add(2);
        environmentIds.add(3);

        Mockito.doReturn(false).when(sessionData).isAdmin();
        Mockito.doReturn(false).when(sessionData)
                .hasWildcardAccessToEnvironments(SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(environment)).when(environmentService).findById(environmentId);
        Mockito.doReturn(environmentIds).when(sessionData)
                .getAllowedEnvironmentIds(SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEnvironmentById("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("The correct error message should be returned",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find an environment with the given ID."));
    }

    @Test
    public void getDivJobExecutions_hasPermission_returnExecutions() throws Exception {
        // Given
        Integer environmentId = 1;

        Environment environment = new Environment();
        environment.setEnvironmentId(environmentId);
        environment.setName("environment1");

        DivJobExecStatus status = new DivJobExecStatus();
        status.setDivJobExecStatusId(1);
        status.setStatus("Not Started");

        PathState ps = new PathState();
        State s = new State();
        s.setStateName("STATS");
        s.setStateId(6);
        ps.setState(s);
        Path p = new Path();
        p.setPathStates(Collections.singletonList(ps));

        DivJobExec divJobExec1 = new DivJobExec();
        divJobExec1.setEnvironment(environment);
        divJobExec1.setDivJobExecId(1L);
        divJobExec1.setDivJobExecStatus(status);

        divJobExec1.setNumInProgress(1);
        divJobExec1.setNumNotStarted(1);
        divJobExec1.setNumFailed(1);
        divJobExec1.setNumCompleted(1);
        divJobExec1.setNumStudentScenarios(4);
        divJobExec1.setTestType("standard");
        divJobExec1.setPath(p);

        DivJobExec divJobExec2 = new DivJobExec();
        divJobExec2.setEnvironment(environment);
        divJobExec2.setDivJobExecId(2L);
        divJobExec2.setDivJobExecStatus(status);

        divJobExec2.setNumInProgress(1);
        divJobExec2.setNumNotStarted(1);
        divJobExec2.setNumFailed(1);
        divJobExec2.setNumCompleted(1);
        divJobExec2.setNumStudentScenarios(4);
        divJobExec2.setTestType("standard");
        divJobExec2.setPath(p);

        DivJobExec divJobExec3 = new DivJobExec();
        divJobExec3.setEnvironment(environment);
        divJobExec3.setDivJobExecId(3L);
        divJobExec3.setDivJobExecStatus(status);

        divJobExec3.setNumInProgress(1);
        divJobExec3.setNumNotStarted(1);
        divJobExec3.setNumFailed(1);
        divJobExec3.setNumCompleted(1);
        divJobExec3.setNumStudentScenarios(4);
        divJobExec3.setTestType("standard");
        divJobExec3.setPath(p);

        List<DivJobExec> divJobExecs = new ArrayList<>();
        divJobExecs.add(divJobExec1);
        divJobExecs.add(divJobExec2);
        divJobExecs.add(divJobExec3);

        List<Long> divJobExecIds = Arrays.asList(1L, 2L, 3L);

        queuedJobs.add("dataIntegrityValidation_2");

        Mockito.doReturn(divJobExecs).when(divJobExecService).findByEnvironmentId(environmentId);

        Mockito.doReturn(new ArrayList<Tuple>()).when(divJobExecService).getDivJobStudentScenarioStatusCounts(divJobExecIds);
        Mockito.doReturn(true).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDivJobsByEnvironmentId("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));

        JsonNode expectedJson = Json.toJson(divJobExecs);
        ObjectNode queuedJobStatus = (ObjectNode) expectedJson.get(1).get("divJobExecStatus");
        queuedJobStatus.put("divJobExecStatusId", 0);
        queuedJobStatus.put("status", "Queued");
        JSONAssert.assertEquals(expectedJson.toString(), resultJson.get("data").get("divJobExecutions").toString(),
                true);
    }

    @Test
    public void getDivJobExecutions_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Integer environmentId = 1;

        Environment environment = new Environment();
        environment.setEnvironmentId(environmentId);
        environment.setName("environment1");

        DivJobExec divJobExec1 = new DivJobExec();
        divJobExec1.setEnvironment(environment);
        divJobExec1.setDivJobExecId(1L);
        DivJobExec divJobExec2 = new DivJobExec();
        divJobExec2.setEnvironment(environment);
        divJobExec2.setDivJobExecId(2L);
        DivJobExec divJobExec3 = new DivJobExec();
        divJobExec3.setEnvironment(environment);
        divJobExec3.setDivJobExecId(3L);

        List<DivJobExec> divJobExecs = new ArrayList<>();
        divJobExecs.add(divJobExec1);
        divJobExecs.add(divJobExec2);
        divJobExecs.add(divJobExec3);

        Mockito.doReturn(divJobExecs).when(divJobExecService).findByEnvironmentId(environmentId);

        Mockito.doReturn(false).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDivJobsByEnvironmentId("v1", environmentId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void saveEnvironment_withPermission_callSaveMethodInEnvironmentService() throws Exception {
        // Given
        Environment environment = new Environment();
        environment.setName("newEnvironment");

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.CREATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(environment));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.saveEnvironment("v1");
        });

        // Then
        Mockito.verify(environmentService)
                .save(LambdaMatcher.argLambda((e) -> e.getName().equals(environment.getName())));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void saveEnvironment_noPermission_returnForbiddenMessage() throws Exception {
        // Given
        Environment environment = new Environment();
        environment.setName("newEnvironment");

        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.CREATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(environment));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.saveEnvironment("v1");
        });

        // Then
        Mockito.verifyZeroInteractions(environmentService);

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void updateEnvironment_withPermission_updateEnvironmentInDatabase() throws Exception {
        // Given
        Integer environmentId = 3;

        Environment environment = new Environment();
        environment.setEnvironmentId(environmentId);
        environment.setName("new");
        environment.setFeUrl("new");
        environment.setEpenUrl("new");

        Environment databaseEnvironment = new Environment();
        databaseEnvironment.setEnvironmentId(environmentId);
        databaseEnvironment.setName("old");
        databaseEnvironment.setFeUrl("old");
        databaseEnvironment.setEpenUrl("old");

        Mockito.doReturn(true).when(sessionData).hasEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.of(databaseEnvironment)).when(environmentService).findById(environmentId);

        requestBuilder.bodyJson(Json.toJson(environment));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.updateEnvironment("v1", environmentId);
        });

        // Then
        assertThat("The environment name should be updated", databaseEnvironment.getName(),
                equalTo(environment.getName()));
        assertThat("The Frontend URL should be updated", databaseEnvironment.getFeUrl(),
                equalTo(environment.getFeUrl()));
        assertThat("The ePEN URL should be updated", databaseEnvironment.getEpenUrl(),
                equalTo(environment.getEpenUrl()));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void updateEnvironment_noPermission_returnForbiddenMessage() throws Exception {
        // Given
        Integer environmentId = 3;

        Environment environment = new Environment();
        environment.setEnvironmentId(environmentId);
        environment.setName("environment");

        Mockito.doReturn(false).when(sessionData).hasEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);

        requestBuilder.bodyJson(Json.toJson(environment));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.updateEnvironment("v1", environmentId);
        });

        // Then
        Mockito.verifyZeroInteractions(environmentService);

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getDbConnectionById_validIdWithPermission_returnDbConnection() throws Exception {
        // Given
        Integer dbConnectionId = 1;

        DbConnection dbConnection = new DbConnection();
        dbConnection.setDbConnectionId(dbConnectionId);
        dbConnection.setHost("localhost");

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(dbConnection)).when(dbConnectionService).findByDbConnectionId(dbConnectionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDbConnectionById("v1", dbConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(dbConnection).toString(),
                resultJson.get("data").get("dbConnection").toString(), true);
    }

    @Test
    public void getDbConnectionById_invalidIdWithPermission_returnNotFoundMessage() throws Exception {
        // Given
        Integer dbConnectionId = 1;

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.empty()).when(dbConnectionService).findByDbConnectionId(dbConnectionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDbConnectionById("v1", dbConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("The correct error message should be returned",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Database connection not exist in database for dbConnectionId = 1"));
    }

    @Test
    public void getDbConnectionById_noPermission_returnForbiddenMessage() throws Exception {
        // Given
        Integer dbConnectionId = 1;

        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDbConnectionById("v1", dbConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getDbConnections_hasPermission_returnDbConnections() throws Exception {
        // Given
        List<DbConnection> dbConnections = new ArrayList<>();
        DbConnection dbConnection = new DbConnection();
        dbConnection.setDbConnectionId(1);
        dbConnection.setHost("localhost");
        dbConnections.add(dbConnection);

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(dbConnections).when(dbConnectionService).findAll();

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDbConnections("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(dbConnections).toString(),
                resultJson.get("data").get("dbConnections").toString(), true);
    }

    @Test
    public void getDbConnections_noPermission_returnForbiddenMessage() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDbConnections("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void saveDbConnection_hasPermission_callSaveMethodInDbConnectionService() throws Exception {
        // Given
        DbConnectionDto dbConnection = new DbConnectionDto();
        dbConnection.setDbName("newDb");
        dbConnection.setHost("host");
        dbConnection.setDbUser("user");
        dbConnection.setDbPass("password");
        dbConnection.setPort(123);

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.CREATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(dbConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.saveDbConnection("v1");
        });

        // Then
        Mockito.verify(dbConnectionService)
                .save(LambdaMatcher.argLambda((dbc) -> dbc.getDbName().equals(dbConnection.getDbName())
                        && dbc.getHost().equals(dbConnection.getHost())
                        && dbc.getDbUser().equals(dbConnection.getDbUser())
                        && dbc.getDbPass().equals(dbConnection.getDbPass())
                        && dbc.getPort().equals(dbConnection.getPort())));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void saveDbConnection_noPermission_returnForbiddenMessage() throws Exception {
        // Given
        DbConnectionDto dbConnection = new DbConnectionDto();
        dbConnection.setDbName("newDb");
        dbConnection.setHost("host");
        dbConnection.setDbUser("user");
        dbConnection.setDbPass("password");
        dbConnection.setPort(123);

        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.CREATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(dbConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.saveDbConnection("v1");
        });

        // Then
        Mockito.verifyZeroInteractions(dbConnectionService);

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void updateDbConnection_hasPermission_updateDbConnectionInDatabase() throws Exception {
        // Given
        Integer dbConnectionId = 5;

        DbConnectionDto dbConnection = new DbConnectionDto();
        dbConnection.setDbConnectionId(dbConnectionId);
        dbConnection.setDbName("new");
        dbConnection.setHost("new");
        dbConnection.setDbUser("new");
        dbConnection.setDbPass("new");
        dbConnection.setPort(123);

        DbConnection databaseDbConnection = new DbConnection();
        databaseDbConnection.setDbConnectionId(dbConnectionId);
        databaseDbConnection.setDbName("old");
        databaseDbConnection.setHost("old");
        databaseDbConnection.setDbUser("old");
        databaseDbConnection.setDbPass("old");
        databaseDbConnection.setPort(321);

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(databaseDbConnection)).when(dbConnectionService)
                .findByDbConnectionId(dbConnectionId);

        requestBuilder.bodyJson(Json.toJson(dbConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.updateDbConnection("v1", dbConnectionId);
        });

        // Then
        assertThat("The DB name should be updated", databaseDbConnection.getDbName(),
                equalTo(dbConnection.getDbName()));
        assertThat("The DB host should be updated", databaseDbConnection.getHost(), equalTo(dbConnection.getHost()));
        assertThat("The DB user should be updated", databaseDbConnection.getDbUser(),
                equalTo(dbConnection.getDbUser()));
        assertThat("The DB password should be updated", databaseDbConnection.getDbPass(),
                equalTo(dbConnection.getDbPass()));
        assertThat("The port should be updated", databaseDbConnection.getPort(), equalTo(dbConnection.getPort()));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void updateDbConnection_noPermission_returnForbiddenResponse() throws Exception {
        // Given
        Integer dbConnectionId = 5;

        DbConnectionDto dbConnection = new DbConnectionDto();
        dbConnection.setDbConnectionId(dbConnectionId);
        dbConnection.setDbName("new");
        dbConnection.setHost("new");
        dbConnection.setDbUser("new");
        dbConnection.setDbPass("new");
        dbConnection.setPort(123);

        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(dbConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.updateDbConnection("v1", dbConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getApiConnections_hasPermission_returnApiConnections() throws Exception {
        // Given
        List<ApiConnection> apiConnections = new ArrayList<>();
        ApiConnection apiConnection = new ApiConnection();
        apiConnection.setApiConnectionId(1);
        apiConnection.setApiUrl("localhost");
        apiConnections.add(apiConnection);

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(apiConnections).when(apiConnectionService).findAll();

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getApiConnections("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(apiConnections).toString(),
                resultJson.get("data").get("apiConnections").toString(), true);
    }

    @Test
    public void getApiConnections_noPermission_returnForbiddenMessage() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getApiConnections("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getApiConnectionById_validIdWithPermission_returnApiConnection() throws Exception {
        // Given
        Integer apiConnectionId = 1;

        ApiConnection apiConnection = new ApiConnection();
        apiConnection.setApiConnectionId(apiConnectionId);
        apiConnection.setApiUrl("localhost");
        apiConnection.setToken("Test");
        apiConnection.setSecret("Test");

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(apiConnection)).when(apiConnectionService).findByApiConnectionId(apiConnectionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getApiConnectionById("v1", apiConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(apiConnection).toString(),
                resultJson.get("data").get("apiConnection").toString(), true);
    }

    @Test
    public void getApiConnectionById_invalidIdWithPermission_returnNotFoundMessage() throws Exception {
        // Given
        Integer apiConnectionId = 1;

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.empty()).when(apiConnectionService).findByApiConnectionId(apiConnectionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getApiConnectionById("v1", apiConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getApiConnectionById_noPermission_returnForbiddenMessage() throws Exception {
        // Given
        Integer apiConnectionId = 1;

        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getApiConnectionById("v1", apiConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void saveApiConnection_withPermission_callSaveMethodOfApiConnectionService() throws Exception {
        // Given
        ApiConnection apiConnection = new ApiConnection();
        apiConnection.setApiUrl("apiUrl");
        apiConnection.setToken("token");
        apiConnection.setSecret("secret");

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.CREATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(apiConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.saveApiConnection("v1");
        });

        // Then
        Mockito.verify(apiConnectionService)
                .save(LambdaMatcher.argLambda((ac) -> ac.getApiUrl().equals(apiConnection.getApiUrl())
                        && ac.getToken().equals(apiConnection.getToken())));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void saveApiConnection_noPermission_returnForbiddenResult() throws Exception {
        // Given
        ApiConnection apiConnection = new ApiConnection();
        apiConnection.setApiUrl("apiUrl");
        apiConnection.setToken("token");
        apiConnection.setSecret("secret");

        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.CREATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(apiConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.saveApiConnection("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void updateApiConnection_withPermission_updateApiConnectionInDatabase() throws Exception {
        // Given
        Integer apiConnectionId = 7;

        ApiConnection apiConnection = new ApiConnection();
        apiConnection.setApiConnectionId(apiConnectionId);
        apiConnection.setApiUrl("newUrl");
        apiConnection.setToken("newToken");
        apiConnection.setSecret("newSecret");

        ApiConnection databaseApiConnection = new ApiConnection();
        databaseApiConnection.setApiConnectionId(apiConnectionId);
        databaseApiConnection.setApiUrl("oldUrl");
        databaseApiConnection.setToken("oldToken");
        databaseApiConnection.setSecret("oldSecret");

        Mockito.doReturn(true).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());
        Mockito.doReturn(Optional.of(databaseApiConnection)).when(apiConnectionService)
                .findByApiConnectionId(apiConnectionId);

        requestBuilder.bodyJson(Json.toJson(apiConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.updateApiConnection("v1", apiConnectionId);
        });

        // Then
        assertThat("The URL should be updated", databaseApiConnection.getApiUrl(), equalTo(apiConnection.getApiUrl()));
        assertThat("The token should be updated", databaseApiConnection.getToken(), equalTo(apiConnection.getToken()));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void updateApiConnection_noPermission_returnForbiddenResult() throws Exception {
        // Given
        Integer apiConnectionId = 7;

        ApiConnection apiConnection = new ApiConnection();
        apiConnection.setApiConnectionId(apiConnectionId);
        apiConnection.setApiUrl("newUrl");
        apiConnection.setToken("newToken");
        apiConnection.setSecret("newSecret");

        Mockito.doReturn(false).when(sessionData).hasAnyEnvironmentLevelPermission("environments",
                SecureController.AccessType.UPDATE, SecureController.Module.SYSTEM_VALIDATION.getName());

        requestBuilder.bodyJson(Json.toJson(apiConnection));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.updateApiConnection("v1", apiConnectionId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getTestNavScenario_resultFound_returnScenarioInformation() throws Exception {
        // Given
        int environmentId = 1;
        long divJobExecId = 3;
        long studentScenarioId = 5;

        ObjectNode scenarioContent = Json.newObject();
        scenarioContent.put("scenarioName", "SCENARIO_CLASS");
        scenarioContent.put("testNumber", "ABC123");
        scenarioContent.put("formNumber", "XYZ789");
        scenarioContent.putArray("itemResponses");

        int testMapId = 342;
        String testMapName = "SOME_TEST_MAP";
        int testMapVersion = 3;
        String previewerUrl = "previewer.pearson.com";
        long tenantId = 39;

        TestmapDetail testmapDetail = new TestmapDetail();
        testmapDetail.setTestmapId(testMapId);
        testmapDetail.setTestmapName(testMapName);
        testmapDetail.setTestmapVersion(testMapVersion);

        Previewer previewer = new Previewer();
        previewer.setUrl(previewerUrl);

        PreviewerTenant previewerTenant = new PreviewerTenant();
        previewerTenant.setTenantId(tenantId);
        previewerTenant.setPreviewer(previewer);

        Scenario scenario = new Scenario();
        scenario.setScenario(scenarioContent.toString());
        scenario.setTestmapDetail(testmapDetail);
        scenario.setPreviewerTenant(previewerTenant);

        ObjectNode modifiedScenarioContent = Json.newObject();
        modifiedScenarioContent.put("scenarioName", "SCENARIO_CLASS");
        modifiedScenarioContent.put("testMapId", testMapId);
        modifiedScenarioContent.put("testMapName", testMapName);
        modifiedScenarioContent.put("testMapVersion", testMapVersion);
        modifiedScenarioContent.put("previewerUrl", previewerUrl);
        modifiedScenarioContent.put("tenantId", tenantId);
        modifiedScenarioContent.put("testNumber", "ABC123");
        modifiedScenarioContent.put("formNumber", "XYZ789");
        modifiedScenarioContent.putArray("itemResponses");

        ObjectNode expectedScenarioJson = (ObjectNode) Json.toJson(scenario);
        expectedScenarioJson.set("scenarioContent", modifiedScenarioContent);

        Mockito.doReturn(true).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.of(scenario)).when(scenarioService)
                .findTestNavScenarioByStudentScenarioId(environmentId, divJobExecId, studentScenarioId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getTestNavScenario("v1", environmentId, divJobExecId, studentScenarioId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(expectedScenarioJson.toString(), resultJson.get("data").get("scenario").toString(),
                true);
    }

    @Test
    public void getTestNavScenario_noResultFound_returnNotFound() throws Exception {
        // Given
        int environmentId = 1;
        long divJobExecId = 3;
        long studentScenarioId = 5;

        Mockito.doReturn(true).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.empty()).when(scenarioService).findTestNavScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getTestNavScenario("v1", environmentId, divJobExecId, studentScenarioId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getTestNavScenario_hasNoPermission_returnAccessDenied() throws Exception {
        // Given
        int environmentId = 1;
        long divJobExecId = 3;
        long studentScenarioId = 5;

        Mockito.doReturn(false).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.empty()).when(scenarioService).findTestNavScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getTestNavScenario("v1", environmentId, divJobExecId, studentScenarioId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getEpenScenario_resultFound_returnScenarioInformation() throws Exception {
        // Given
        int environmentId = 1;
        long divJobExecId = 3;
        long studentScenarioId = 5;

        ObjectNode scenarioContent = Json.newObject();
        scenarioContent.put("uin", (String) null);
        scenarioContent.put("scenarioName", "SCENARIO_CLASS");
        ArrayNode scenarioData = scenarioContent.putArray("data");
        scenarioContent.put("uuid", (String) null);

        ObjectNode scenarioDataObject = scenarioData.addObject();
        scenarioDataObject.put("itemName", "item");
        scenarioDataObject.put("responseState", (String) null);

        Scenario scenario = new Scenario();
        scenario.setScenario(scenarioContent.toString());

        ObjectNode modifiedScenarioContent = (ObjectNode) Json.parse(scenarioContent.toString());
        modifiedScenarioContent.remove("uin");
        ((ObjectNode) modifiedScenarioContent.get("data").get(0)).remove("responseState");
        modifiedScenarioContent.remove("uuid");

        ObjectNode expectedScenarioJson = (ObjectNode) Json.toJson(scenario);
        expectedScenarioJson.set("scenarioContent", modifiedScenarioContent);

        Mockito.doReturn(true).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.of(scenario)).when(scenarioService).findEpenScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEpenScenario("v1", environmentId, divJobExecId, studentScenarioId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(expectedScenarioJson.toString(), resultJson.get("data").get("scenario").toString(),
                true);
    }

    @Test
    public void getEpenScenario_resultFoundWithExtraInformation_returnScenarioInformationWithoutExtraInformation()
            throws Exception {
        // Given
        int environmentId = 1;
        long divJobExecId = 3;
        long studentScenarioId = 5;

        ObjectNode scenarioContent = Json.newObject();
        scenarioContent.putObject("environmentDetails");
        scenarioContent.putArray("scorerAccounts");
        ArrayNode inputData = scenarioContent.putArray("inputData");

        ObjectNode inputDataObject = inputData.addObject();
        inputDataObject.put("uin", (String) null);
        inputDataObject.put("scenarioName", "SCENARIO_CLASS");
        ArrayNode scenarioData = inputDataObject.putArray("data");
        inputDataObject.put("uuid", (String) null);

        ObjectNode scenarioDataObject = scenarioData.addObject();
        scenarioDataObject.put("itemName", "item");
        scenarioDataObject.put("responseState", (String) null);

        Scenario scenario = new Scenario();
        scenario.setScenario(scenarioContent.toString());

        ObjectNode modifiedScenarioContent = (ObjectNode) Json
                .parse(scenarioContent.get("inputData").get(0).toString());
        modifiedScenarioContent.remove("uin");
        ((ObjectNode) modifiedScenarioContent.get("data").get(0)).remove("responseState");
        modifiedScenarioContent.remove("uuid");

        ObjectNode expectedScenarioJson = (ObjectNode) Json.toJson(scenario);
        expectedScenarioJson.set("scenarioContent", modifiedScenarioContent);

        Mockito.doReturn(true).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.of(scenario)).when(scenarioService).findEpenScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEpenScenario("v1", environmentId, divJobExecId, studentScenarioId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(expectedScenarioJson.toString(), resultJson.get("data").get("scenario").toString(),
                true);
    }

    @Test
    public void getEpenScenario_noResultFound_returnNotFound() throws Exception {
        // Given
        int environmentId = 1;
        long divJobExecId = 3;
        long studentScenarioId = 5;

        Mockito.doReturn(true).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.empty()).when(scenarioService).findEpenScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEpenScenario("v1", environmentId, divJobExecId, studentScenarioId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getEpenScenario_hasNoPermission_returnAccessDenied() throws Exception {
        // Given
        int environmentId = 1;
        long divJobExecId = 3;
        long studentScenarioId = 5;

        Mockito.doReturn(false).when(sessionData).hasEnvironmentLevelPermission("divJobExecutions",
                SecureController.AccessType.READ, SecureController.Module.SYSTEM_VALIDATION.getName(), environmentId);
        Mockito.doReturn(Optional.empty()).when(scenarioService).findEpenScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getEpenScenario("v1", environmentId, divJobExecId, studentScenarioId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getDivJobStatus_resultFound_returnDivJobStatus() throws Exception {
        // Given
        long divJobExecId = 3l;
        DivJobExecStatus divJobExecStatus = new DivJobExecStatus();
        divJobExecStatus.setStatus("inProgress");
        Environment environment = new Environment();
        environment.setName("INT_REF_4.2");
        DivJobExec divJobExec = new DivJobExec();
        divJobExec.setDivJobExecStatus(divJobExecStatus);
        divJobExec.setEnvironment(environment);

        Mockito.doReturn(Optional.of(divJobExec)).when(divJobExecService).findStatusByDivJobExecId(divJobExecId);

        JsonNode divJobExecJson = Json.toJson(divJobExec.getDivJobExecStatus().getStatus());
        JsonNode environmentExecJson = Json.toJson(divJobExec.getEnvironment().getName());
        ObjectNode data = Json.newObject();
        data.set("status", divJobExecJson);
        data.set("environment", environmentExecJson);
        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDivJobStatus("v1", divJobExecId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(divJobExecJson.toString(), resultJson.get("data").get("status").toString(), true);
        JSONAssert.assertEquals(environmentExecJson.toString(), resultJson.get("data").get("environment").toString(),
                true);
    }

    @Test
    public void getDivJobStatus_resultNotFound_returnDivJobStatus() throws Exception {
        // Given
        long divJobExecId = 0l;

        Mockito.doReturn(Optional.empty()).when(divJobExecService).findStatusByDivJobExecId(divJobExecId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return divJobController.getDivJobStatus("v1", divJobExecId);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));

    }

}
