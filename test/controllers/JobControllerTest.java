package controllers;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import global.TestWithApplication;
import jobs.JobProtocol;
import jobs.JobRunnerActor;
import jobs.NightlyJobStarter;
import jobs.qtivalidation.QtiScoringValidationJobStarter;
import jobs.systemvalidation.DivJobStarter;
import models.systemvalidation.DivJobExec;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import play.core.j.JavaContextComponents;
import play.db.jpa.JPAApi;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.systemvalidation.DivJobExecService;
import services.systemvalidation.StudentScenarioService;
import util.SessionData;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletionStage;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class JobControllerTest extends TestWithApplication {
    private static final String QTI_SCV_JOB = "qtiScvJob";
    private static final String DIV_JOB = "divJob";

    private SessionData sessionData;
    private DivJobExecService divJobExecService;
    private StudentScenarioService studentScenarioService;
    private Http.RequestBuilder requestBuilder;
    private JobController jobController;
    private JPAApi jpaApi;

    @Mock
    private JavaContextComponents contextComponents;
    @Before
    public void setUp() throws Exception {
        sessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(sessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        ActorRef jobRunnerActor = actorSystem.actorOf(Props.create(MockJobRunnerActor.class));

        NightlyJobStarter nightlyJobStarter = app.injector().instanceOf(NightlyJobStarter.class);
        QtiScoringValidationJobStarter.Factory qtiScvJobStarterFactory =
                app.injector().instanceOf(QtiScoringValidationJobStarter.Factory.class);
        DivJobStarter.Factory divJobStarterFactory = app.injector().instanceOf(DivJobStarter.Factory.class);

        divJobExecService = Mockito.mock(DivJobExecService.class);
        studentScenarioService = Mockito.mock(StudentScenarioService.class);
        requestBuilder = new Http.RequestBuilder();
        jpaApi = Mockito.mock(JPAApi.class);
        jobController = Mockito.spy(new JobController(sessionDataFactory, jobRunnerActor, nightlyJobStarter,
                qtiScvJobStarterFactory, divJobStarterFactory, divJobExecService, studentScenarioService, jpaApi, app.config()));
    }

    @Test
    public void getRunningJobs_isAdmin_returnSuccessResult() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData).isAdmin();

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.getJobs("v1"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));

        assertThat("Result should contain an array of running jobs",
                resultJson.get("data").get("runningJobs").isArray(), equalTo(true));
        assertThat("Running jobs array should contain 1 running job",
                resultJson.get("data").get("runningJobs").size(), equalTo(1));
        assertThat("Running jobs array should contain job1",
                resultJson.get("data").get("runningJobs").get(0).asText(), equalTo("job1"));

        assertThat("Result should contain an array of queued jobs",
                resultJson.get("data").get("queuedJobs").isArray(), equalTo(true));
        assertThat("Queued jobs array should contain 1 queued job",
                resultJson.get("data").get("queuedJobs").size(), equalTo(1));
        assertThat("Queued jobs array should contain job1",
                resultJson.get("data").get("queuedJobs").get(0).asText(), equalTo("job2"));
    }

    @Test
    public void getRunningJobs_isNotAdmin_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData).isAdmin();

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.getJobs("v1"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void startNightlyJob_isAdmin_returnSuccessResult() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData).isAdmin();

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.startNightlyJob("v1"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void startNightlyJob_isNotAdmin_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData).isAdmin();

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.startNightlyJob("v1"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void checkNightlyJobStatus_isAdmin_returnSuccessResult() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData).isAdmin();

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.checkNightlyJobStatus("v1"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void checkNightlyJobStatus_isNotAdmin_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData).isAdmin();

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.checkNightlyJobStatus("v1"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void startQtiScoringValidationJob_hasPermission_returnSuccessResultWithJobId() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(QTI_SCV_JOB, SecureController.AccessType.CREATE, SecureController.Module
                        .QTI_VALIDATION.getName(), 1, 2L);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.startQtiScoringValidationJob("v1", 1, 2L));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        assertThat("Result JSON should have the correct job ID",
                resultJson.get("data").get("jobId").asText(), equalTo("qtiScoringValidation_1_2"));
    }

    @Test
    public void startQtiScoringValidationJob_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData)
                .hasTenantLevelPermission(QTI_SCV_JOB, SecureController.AccessType.CREATE, SecureController.Module
                        .QTI_VALIDATION.getName(), 1, 2L);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.startQtiScoringValidationJob("v1", 1, 2L));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void checkQtiScoringValidationJobStatus_hasPermission_returnSuccessResult() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(QTI_SCV_JOB, SecureController.AccessType.READ, SecureController.Module
                        .QTI_VALIDATION.getName(), 1, 2L);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.checkQtiScoringValidationJobStatus("v1", 1, 2L));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void checkQtiScoringValidationJobStatus_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData)
                .hasTenantLevelPermission(QTI_SCV_JOB, SecureController.AccessType.READ, SecureController.Module
                        .QTI_VALIDATION.getName(), 1, 2L);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.checkQtiScoringValidationJobStatus("v1", 1, 2L));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void startDivJobExecution_hasPermissionAndDivJobExecExists_returnSuccessResultWithJobId() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasEnvironmentLevelPermission(DIV_JOB, SecureController.AccessType.CREATE, SecureController.Module
                        .SYSTEM_VALIDATION.getName(), 1);
        Mockito.doReturn(Optional.of(Mockito.mock(DivJobExec.class))).when(divJobExecService).find(1, 12L);
        Mockito.doReturn(Optional.empty()).when(divJobExecService).findByDivJobExecId(12L);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.startDivJobExecution("v1", 1, 12L));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        assertThat("Result JSON should have the correct job ID",
                resultJson.get("data").get("jobId").asText(), equalTo("dataIntegrityValidation_12"));
    }

    @Test
    public void startDivJobExecution_hasPermissionButDivJobExecDoesNotExists_returnError() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasEnvironmentLevelPermission(DIV_JOB, SecureController.AccessType.CREATE, SecureController.Module
                        .SYSTEM_VALIDATION.getName(), 1);

        Mockito.doReturn(Optional.empty()).when(divJobExecService).findByDivJobExecId(12L);
        Mockito.doReturn(Optional.empty()).when(divJobExecService).find(1, 12L);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.startDivJobExecution("v1", 1, 12L));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct error message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find the given DIV job execution."));
    }

    @Test
    public void startDivJobExecution_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData)
                .hasEnvironmentLevelPermission(DIV_JOB, SecureController.AccessType.CREATE, SecureController.Module
                        .SYSTEM_VALIDATION.getName(), 1);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> jobController.startDivJobExecution("v1", 1, 12L));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    public static class MockJobRunnerActor extends UntypedAbstractActor {
        @Override
        public void onReceive(Object message) throws Exception {
            if (message instanceof JobProtocol.JobStartMessage) {
                getSender().tell(((JobProtocol.JobStartMessage) message).getJobStarter().getJobId(), getSelf());
            } else if (message instanceof JobProtocol.JobCheckMessage) {
                ObjectNode returnJson = Json.newObject();
                returnJson.put("isRunning", true);
                getSender().tell(returnJson, getSelf());
            } else if (message instanceof String && JobRunnerActor.GET_RUNNING_JOBS_MESSAGE.equals(message)) {
                Set<String> runningJobs = new HashSet<>();
                runningJobs.add("job1");
                Set<String> queuedJobs = new HashSet<>();
                queuedJobs.add("job2");

                ObjectNode returnData = Json.newObject();
                returnData.set("runningJobs", Json.toJson(runningJobs));
                returnData.set("queuedJobs", Json.toJson(queuedJobs));

                getSender().tell(returnData, getSelf());
            }
        }
    }

}
