package controllers.qtirespgen;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.SecureController;
import global.TestWithApplication;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import util.SessionData;
import util.qtirespgen.QtiResponseGenerator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class QtiRespGenControllerTest extends TestWithApplication {
    private SessionData mockedSessionData;
    private QtiResponseGenerator qtiResponseGenerator;
    private Http.RequestBuilder requestBuilder;
    private QtiRespGenController qtiRespGenController;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setup() {
        mockedSessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));
        qtiResponseGenerator = Mockito.mock(QtiResponseGenerator.class);
        requestBuilder = new Http.RequestBuilder();
        qtiRespGenController = Mockito.spy(new QtiRespGenController(sessionDataFactory, qtiResponseGenerator));
    }

    @Test
    public void getResponseOutcome_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).hasModuleLevelPermission("responses",
                SecureController.AccessType.READ, SecureController.Module.QTI_RESP_GEN.getName());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("responses", Json.newObject());
            return qtiRespGenController.getResponseOutcome("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
    }

    @Test
    public void getResponseOutcome_hasPermission_returnOutcomes() throws Exception {
        // Given
        ObjectNode request = getFakeRequest();
        QtiResponseGenerator.ResponseOutcomeResult fakeResponse = getFakeResponseOutcomeResult();
        Mockito.doReturn(true).when(mockedSessionData).hasModuleLevelPermission("responses",
                SecureController.AccessType.READ, SecureController.Module.QTI_RESP_GEN.getName());
        Mockito.doReturn(fakeResponse).when(qtiResponseGenerator).getResponseOutcomes(request.get("itemXml").asText(),
                (ArrayNode) request.get("responses"));

        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(request);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("outcomes", Json.newObject());
            return qtiRespGenController.getResponseOutcome("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(getFakeResponseOutcomeResult()).toString(),
                resultJson.toString(), true);

    }

    private QtiResponseGenerator.ResponseOutcomeResult getFakeResponseOutcomeResult() {
        ObjectNode dataNode = Json.newObject();
        ArrayNode responseOutcomes = dataNode.putArray("responseOutcomes");
        ObjectNode responseOutcome = responseOutcomes.addObject();
        responseOutcome.put("scorable", true);
        ArrayNode outcomes = responseOutcome.putArray("outcomes");
        ObjectNode outcome = outcomes.addObject();
        outcome.put("identifier", "SCORE");
        outcome.put("value", "2");
        return new QtiResponseGenerator.ResponseOutcomeResult(true, dataNode, null);
    }

    private ObjectNode getFakeRequest() {
        ObjectNode body = Json.newObject();
        body.put("itemXml", "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?> "
                + "<assessmentItem xmlns=\"http://www.imsglobal.org/xsd/imsqti_v2p1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" adaptive=\"false\" identifier=\"AUTO0001\" timeDependent=\"false\" title=\"Multiple inlineChoices\" xsi:schemaLocation=\"http://www.imsglobal.org/xsd/imsqti_v2p1 http://www.imsglobal.org/xsd/imsqti_v2p1.xsd\"> <responseDeclaration baseType=\"identifier\" cardinality=\"single\" identifier=\"RESPONSE1\"> <correctResponse> <value>choice_2</value> </correctResponse> </responseDeclaration> <responseDeclaration baseType=\"identifier\" cardinality=\"single\" identifier=\"RESPONSE2\"> <correctResponse> <value>choice_7</value> </correctResponse> </responseDeclaration> <responseDeclaration baseType=\"identifier\" cardinality=\"single\" identifier=\"RESPONSE3\"> <correctResponse> <value>choice_11</value> </correctResponse> </responseDeclaration> <responseDeclaration baseType=\"identifier\" cardinality=\"single\" identifier=\"RESPONSE4\"> <correctResponse> <value>choice_15</value> </correctResponse> </responseDeclaration> <outcomeDeclaration baseType=\"float\" cardinality=\"single\" identifier=\"SCORE\"/> <itemBody> <div> <h3> <object data=\"images/Airplane3.PNG\" type=\"image/png\"/> </h3> <h3>First Airplane Trip</h3> <p/> <p> <strong>Select the correct word from the drop down menu</strong> </p>Jake is<inlineChoiceInteraction responseIdentifier=\"RESPONSE1\" shuffle=\"false\"> <inlineChoice identifier=\"choice_1\">gone</inlineChoice> <inlineChoice identifier=\"choice_2\">going</inlineChoice> <inlineChoice identifier=\"choice_3\">went</inlineChoice> <inlineChoice identifier=\"choice_4\">walked</inlineChoice> </inlineChoiceInteraction>on a trip. He and Mom take a a taxi to the airport. \"It's my first plane trip,\" he <inlineChoiceInteraction responseIdentifier=\"RESPONSE2\" shuffle=\"false\"> <inlineChoice identifier=\"choice_5\">told</inlineChoice> <inlineChoice identifier=\"choice_6\">yelled</inlineChoice> <inlineChoice identifier=\"choice_7\">tells</inlineChoice> <inlineChoice identifier=\"choice_8\">asked</inlineChoice> <inlineChoice identifier=\"choice_16\">option5</inlineChoice> <inlineChoice identifier=\"choice_17\">option6</inlineChoice> <inlineChoice identifier=\"choice_18\">option7</inlineChoice> <inlineChoice identifier=\"choice_19\">optionnnnnnnn8</inlineChoice> <inlineChoice identifier=\"choice_20\">option 9</inlineChoice> <inlineChoice identifier=\"choice_21\">option 10</inlineChoice> <inlineChoice identifier=\"choice_22\">option 11</inlineChoice> <inlineChoice identifier=\"choice_23\">option 12</inlineChoice> <inlineChoice identifier=\"choice_24\">option 13</inlineChoice> <inlineChoice identifier=\"choice_25\">option 14</inlineChoice> <inlineChoice identifier=\"choice_26\">option 15</inlineChoice> <inlineChoice identifier=\"choice_27\">option 16</inlineChoice> <inlineChoice identifier=\"choice_28\">option 17</inlineChoice> <inlineChoice identifier=\"choice_29\">option 18</inlineChoice> <inlineChoice identifier=\"choice_30\">option 19</inlineChoice> <inlineChoice identifier=\"choice_31\">option 20</inlineChoice> <inlineChoice identifier=\"choice_32\">option 21</inlineChoice> <inlineChoice identifier=\"choice_33\">option 22</inlineChoice> <inlineChoice identifier=\"choice_34\">option 23</inlineChoice> <inlineChoice identifier=\"choice_35\">option 24</inlineChoice> </inlineChoiceInteraction>the taxi driver.That's great, the taxi driver says. Jake rolls his suitcase onto the plane. \"It's my first plane trip,\" he tells the pilot. \"Welcome aboard\", the pilot says. Jake <inlineChoiceInteraction responseIdentifier=\"RESPONSE3\" shuffle=\"false\"> <inlineChoice identifier=\"choice_9\">fastened</inlineChoice> <inlineChoice identifier=\"choice_10\">fasten</inlineChoice> <inlineChoice identifier=\"choice_11\">fastens</inlineChoice> <inlineChoice identifier=\"choice_12\">fastening</inlineChoice> </inlineChoiceInteraction>his seatbelt. The plane's engine rumble and roar. Jake opens his backpack and pulls out Panda. \"It's my first plane trip,\" he whispers. He <inlineChoiceInteraction responseIdentifier=\"RESPONSE4\" shuffle=\"false\"> <inlineChoice identifier=\"choice_13\">held</inlineChoice> <inlineChoice identifier=\"choice_14\">hold </inlineChoice> <inlineChoice identifier=\"choice_15\">holds</inlineChoice> </inlineChoiceInteraction>Panda's paw. The plane moves faster and faster. Then Whoosh! On the ground, cars and houses look like toys. Jake smiles. \"Guess what, Panda?\" he says. \"Flying is fun!\" </div> </itemBody> <responseProcessing> <responseCondition> <responseIf> <match> <variable identifier=\"RESPONSE1\"/> <correct identifier=\"RESPONSE1\"/> </match> <setOutcomeValue identifier=\"SCORE\"> <sum> <variable identifier=\"SCORE\"/> <baseValue baseType=\"integer\">1</baseValue> </sum> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <match> <variable identifier=\"RESPONSE2\"/> <correct identifier=\"RESPONSE2\"/> </match> <setOutcomeValue identifier=\"SCORE\"> <sum> <variable identifier=\"SCORE\"/> <baseValue baseType=\"integer\">1</baseValue> </sum> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <match> <variable identifier=\"RESPONSE3\"/> <correct identifier=\"RESPONSE3\"/> </match> <setOutcomeValue identifier=\"SCORE\"> <sum> <variable identifier=\"SCORE\"/> <baseValue baseType=\"integer\">1</baseValue> </sum> </setOutcomeValue> </responseIf> </responseCondition> <responseCondition> <responseIf> <match> <variable identifier=\"RESPONSE4\"/> <correct identifier=\"RESPONSE4\"/> </match> <setOutcomeValue identifier=\"SCORE\"> <sum> <variable identifier=\"SCORE\"/> <baseValue baseType=\"integer\">1</baseValue> </sum> </setOutcomeValue> </responseIf> </responseCondition> </responseProcessing> </assessmentItem>");

        ArrayNode responses = body.putArray("responses");
        ObjectNode response = responses.addObject();
        ArrayNode mods = response.putArray("mods");
        ObjectNode mod = mods.addObject();
        mod.put("did", "RESPONSE4");
        mod.put("r", "choice_15");
        return body;
    }
}
