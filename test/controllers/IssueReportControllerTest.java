package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import global.TestWithApplication;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import play.api.libs.mailer.MailerClient;
import play.core.j.JavaContextComponents;
import play.data.FormFactory;
import play.libs.mailer.Email;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import javax.inject.Provider;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class IssueReportControllerTest extends TestWithApplication {
    private Email mockedEmail;
    private MailerClient mockedMailerClient;
    private IssueReportController issueReportController;
    private String fromAddress;
    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() throws Exception {
        mockedEmail = Mockito.mock(Email.class);
        @SuppressWarnings("unchecked") Provider<Email> mockedEmailProvider = Mockito.mock(Provider.class);
        Mockito.when(mockedEmailProvider.get()).thenReturn(mockedEmail);
        mockedMailerClient = Mockito.mock(MailerClient.class);
        fromAddress = app.config().getString("application.email.fromAddress");

        issueReportController = new IssueReportController(app.injector().instanceOf(FormFactory.class),
                mockedEmailProvider, mockedMailerClient, app.config());
    }

    @Test
    public void reportIssue_emailAddressAndDescriptionPopulated_sendEmail() {
        // Given
        String email = "tester@pearson.com";
        String description = "The app broke.";
        String errorId = "ABC123456";
        HashMap<String, String> formMap = new HashMap<>();
        formMap.put("email", email);
        formMap.put("description", description);
        formMap.put("errorId", errorId);
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .bodyForm(formMap);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> issueReportController.reportIssue("v1"));

        // Then
        Mockito.verify(mockedEmail).setSubject(Mockito.contains(errorId));
        Mockito.verify(mockedEmail).setFrom(fromAddress);
        Mockito.verify(mockedEmail).setBodyHtml(Mockito.contains(email));
        Mockito.verify(mockedEmail).setBodyHtml(Mockito.contains(description));
        List<String> recipientList = app.config().getStringList("issue.report.recipients");
        recipientList.stream().forEach(recipient -> Mockito.verify(mockedEmail).addTo(recipient));
        Mockito.verify(mockedMailerClient).send(mockedEmail);

        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));
    }

    @Test
    public void reportIssue_onlyDescriptionPopulated_sendEmail() {
        // Given
        String description = "The app broke.";
        String errorId = "ABC123456";
        HashMap<String, String> formMap = new HashMap<>();
        formMap.put("description", description);
        formMap.put("errorId", errorId);
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .bodyForm(formMap);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> issueReportController.reportIssue("v1"));

        // Then
        Mockito.verify(mockedEmail).setSubject(Mockito.contains(errorId));
        Mockito.verify(mockedEmail).setFrom(fromAddress);
        Mockito.verify(mockedEmail).setBodyHtml(Mockito.contains(description));
        List<String> recipientList = app.config().getStringList("issue.report.recipients");
        recipientList.stream().forEach(recipient -> Mockito.verify(mockedEmail).addTo(recipient));
        Mockito.verify(mockedMailerClient).send(mockedEmail);

        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));
    }

    @Test
    public void reportIssue_noDescription_returnError() throws Exception {
        // Given
        String email = "tester@pearson.com";
        String errorId = "ABC123456";
        HashMap<String, String> formMap = new HashMap<>();
        formMap.put("email", email);
        formMap.put("errorId", errorId);
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .bodyForm(formMap);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> issueReportController.reportIssue("v1"));

        // Then
        assertThat("Result should have a BAD_REQUEST status", result.status(), equalTo(Http.Status.BAD_REQUEST));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result should contain the correct error message", resultJson.get("errors").get(0)
                .get("message").asText(), equalTo("Please include a description of the issue."));
    }

    @Test
    public void reportIssue_invalidEmail_returnError() throws Exception {
        // Given
        String email = "tester@@pearson.com";
        String description = "The app broke.";
        String errorId = "ABC123456";
        HashMap<String, String> formMap = new HashMap<>();
        formMap.put("email", email);
        formMap.put("description", description);
        formMap.put("errorId", errorId);
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .bodyForm(formMap);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> issueReportController.reportIssue("v1"));

        // Then
        assertThat("Result should have a BAD_REQUEST status", result.status(), equalTo(Http.Status.BAD_REQUEST));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result should contain success value of false", resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result should contain the correct error message", resultJson.get("errors").get(0)
                .get("message").asText(), equalTo("Please enter a valid email address."));
    }

}
