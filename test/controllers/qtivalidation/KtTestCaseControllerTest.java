package controllers.qtivalidation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.JobControllerTest;
import controllers.SecureController;
import global.TestWithApplication;
import jobs.qtivalidation.KtTestCaseDownloadJobStarter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import play.core.j.JavaContextComponents;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import util.S3Client;
import util.SessionData;
import util.qtivalidation.S3ClientFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.CompletionStage;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class KtTestCaseControllerTest extends TestWithApplication {
    private static final String BUCKET = "product-validation-math-scoring-manifest";
    private static final String MANIFEST_FILE = "manifest.json";
    private Http.RequestBuilder requestBuilder;
    private KtTestCaseController ktTestCaseController;
    private String testCasesPermission;
    private String moduleName;
    private SecureController.AccessType accessType;
    @Mock
    private SessionData mockedSessionData;
    @Mock
    private S3ClientFactory s3ClientFactory;
    @Mock
    private JavaContextComponents contextComponents;
    @Mock
    private S3Client s3Client;

    @Before
    public void setUp() {
        requestBuilder = new Http.RequestBuilder();
        testCasesPermission = "responses";
        accessType = SecureController.AccessType.READ;
        moduleName = SecureController.Module.QTI_RESP_GEN.getName();
        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        ActorRef jobRunnerActor = actorSystem.actorOf(Props.create(JobControllerTest.MockJobRunnerActor.class));
        KtTestCaseDownloadJobStarter.Factory ktTestCaseDownloadJobStarterFactor =
                app.injector().instanceOf(KtTestCaseDownloadJobStarter.Factory.class);
        ktTestCaseController = Mockito.spy(
                new KtTestCaseController(sessionDataFactory, s3ClientFactory,
                        jobRunnerActor, ktTestCaseDownloadJobStarterFactor,
                        app.config()));
    }

    @Test
    public void updateKtTestCases_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData)
                .hasModuleLevelPermission(testCasesPermission, accessType, moduleName);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> ktTestCaseController.updateKtTestCases("v1", "manifest.json"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void updateKtTestCasesTest_with_successResult() throws Exception {
        //Given
        Mockito.doReturn(true).when(mockedSessionData)
                .hasModuleLevelPermission(testCasesPermission, accessType, moduleName);

        Mockito.doReturn(s3Client).when(s3ClientFactory).create();
        Mockito.doReturn(getDummyTestCases()).when(s3Client).downFileFromS3(BUCKET, MANIFEST_FILE);

        // When
        CompletionStage<Result> resultCompletionStage = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> ktTestCaseController.updateKtTestCases("v1", "manifest.json"));
        Result result = resultCompletionStage.toCompletableFuture().get();

        // Then
        assertThat("Status of result should be ok", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
    }

    @Test
    public void updateKtTestCasesTest_with_unprocessableEntityResult() throws Exception {
        //Given
        Mockito.doReturn(true).when(mockedSessionData)
                .hasModuleLevelPermission(testCasesPermission, accessType, moduleName);

        Mockito.doReturn(s3Client).when(s3ClientFactory).create();
        Mockito.doReturn(getKtTestcaseJsonObject()).when(s3Client).downFileFromS3(BUCKET, MANIFEST_FILE);

        // When
        CompletionStage<Result> completionStageResult = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> ktTestCaseController.updateKtTestCases("v1", "manifest.json"));
        Result result = completionStageResult.toCompletableFuture().get();
        // Then
        assertThat("Status of result should be UNPROCESSABLE_ENTITY", result.status(), equalTo(Http.Status.UNPROCESSABLE_ENTITY));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void updateKtTestCasesTest_with_badRequestResult() throws Exception {
        //Given
        Mockito.doReturn(true).when(mockedSessionData)
                .hasModuleLevelPermission(testCasesPermission, accessType, moduleName);

        Mockito.doReturn(s3Client).when(s3ClientFactory).create();
        Mockito.doReturn(getBadRequestTestCases()).when(s3Client).downFileFromS3(BUCKET, MANIFEST_FILE);

        // When
        CompletionStage<Result> completionStageResult = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> ktTestCaseController.updateKtTestCases("v1", "manifest.json"));
        Result result = completionStageResult.toCompletableFuture().get();
        // Then
        assertThat("Status of result should be BAD_REQUEST", result.status(), equalTo(Http.Status.BAD_REQUEST));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    private BufferedReader getBadRequestTestCases() {
        String testCaseJson = "This is bad request.";
        return stringToBufferedReader(testCaseJson);
    }


    private BufferedReader getDummyTestCases() {
        String testCaseJson = "[ { \"_id\": \"0052-M00313#RESPONSE_B\", \"data\": [ { \"responseId\": \"5d2fe38c-735f-41ac-be20-968e384eb10d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><divide/><cn>1</cn><cn>12</cn></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mfrac><mn>1</mn><mn>12</mn></mfrac></math>\" } }, { \"responseId\": \"e9195fb3-36c8-4790-9812-e3ce030fd4fd\", \"savedScore\": 0, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><plus/><apply><divide/><cn>3</cn><cn>4</cn></apply><apply><minus/><apply><divide/><cn>1</cn><cn>3</cn></apply></apply></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mrow><mfrac><mn>3</mn><mn>4</mn></mfrac><mrow><mo form=\\\"infix\\\">-</mo><mfrac><mn>1</mn><mn>3</mn></mfrac></mrow></mrow></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"0052-M00313\", \"responseIdentifier\": \"RESPONSE_B\" }, { \"_id\": \"02119ed1-0631-4db5-aaa6-e1da2ad2474d\", \"data\": [ { \"responseId\": \"defe2c08-9085-4b5e-a64a-8850b615403d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"0 2 1 0\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><eq/><apply><times class=\\\"sign\\\"/><apply><divide/><cn>32</cn><cn>5</cn></apply><apply><divide/><cn>2</cn><cn>4</cn></apply></apply><apply><divide/><cn>64</cn><cn>5</cn></apply></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mrow><mrow><mfrac><mn>32</mn><mn>5</mn></mfrac><mo class=\\\"sign\\\">×</mo><mfrac><mn>2</mn><mn>4</mn></mfrac></mrow><mo>=</mo><mfrac><mn>64</mn><mn>5</mn></mfrac></mrow></math>\" } }, { \"responseId\": \"7a9941db-4a8e-46f6-b8d7-f130bc975384\", \"savedScore\": 0, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"0\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><csymbol cd=\\\"basic\\\">box</csymbol></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mstyle style=\\\"border: 1px dotted black\\\"><mphantom><mi mathvariant=\\\"normal\\\">x</mi></mphantom></mstyle></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"VH262599\", \"responseIdentifier\": \"RESPONSE_B\" } ]";
        return stringToBufferedReader(testCaseJson);
    }

    private BufferedReader getKtTestcaseJsonObject() {
        String testCaseJson = "{ \"_id\": \"0052-M00313#RESPONSE_B\", \"data\": [ { \"responseId\": \"5d2fe38c-735f-41ac-be20-968e384eb10d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><divide/><cn>1</cn><cn>12</cn></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mfrac><mn>1</mn><mn>12</mn></mfrac></math>\" } }, { \"responseId\": \"e9195fb3-36c8-4790-9812-e3ce030fd4fd\", \"savedScore\": 0, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><plus/><apply><divide/><cn>3</cn><cn>4</cn></apply><apply><minus/><apply><divide/><cn>1</cn><cn>3</cn></apply></apply></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mrow><mfrac><mn>3</mn><mn>4</mn></mfrac><mrow><mo form=\\\"infix\\\">-</mo><mfrac><mn>1</mn><mn>3</mn></mfrac></mrow></mrow></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"0052-M00313\", \"responseIdentifier\": \"RESPONSE_B\" }";
        return stringToBufferedReader(testCaseJson);
    }

    private BufferedReader stringToBufferedReader(String testCaseJson) {
        InputStream is = new ByteArrayInputStream(testCaseJson.getBytes());
        return new BufferedReader(new InputStreamReader(is));
    }
}
