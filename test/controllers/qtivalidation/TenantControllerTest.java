package controllers.qtivalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.SecureController;
import global.TestWithApplication;
import models.qtivalidation.PreviewerTenant;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.qtivalidation.PreviewerTenantService;
import services.qtivalidation.UserTenantJobSubscriptionService;
import util.SessionData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class TenantControllerTest extends TestWithApplication {
    private SessionData mockedSessionData;
    private PreviewerTenantService mockedPreviewerTenantService;
    private UserTenantJobSubscriptionService mockedUserTenantJobSubscriptionService;
    private Http.RequestBuilder requestBuilder;
    private TenantController tenantController;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() {
        mockedSessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        mockedPreviewerTenantService = Mockito.mock(PreviewerTenantService.class);
        requestBuilder = new Http.RequestBuilder();
        mockedUserTenantJobSubscriptionService = Mockito.mock(UserTenantJobSubscriptionService.class);
        tenantController = Mockito.spy(new TenantController(sessionDataFactory, mockedPreviewerTenantService,
                mockedUserTenantJobSubscriptionService));
    }

    @Test
    public void getTenants_validIdWithAdminUser_returnTenantsFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        PreviewerTenant tenant1 = new PreviewerTenant();
        tenant1.setPreviewerTenantId(1L);
        PreviewerTenant tenant2 = new PreviewerTenant();
        tenant2.setPreviewerTenantId(2L);

        ArrayList<PreviewerTenant> tenants = new ArrayList<>();
        tenants.add(tenant1);
        tenants.add(tenant2);
        Mockito.doReturn(tenants).when(mockedPreviewerTenantService).findAllActive(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenants("v1", 1);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenants).toString(), resultJson.get("data").get("tenants").toString(),
                true);
    }

    @Test
    public void getTenants_invalidIdWithAdminUser_returnEmptyList() throws Exception {
        // Given
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        ArrayList<PreviewerTenant> tenants = new ArrayList<>();
        Mockito.doReturn(tenants).when(mockedPreviewerTenantService).findAllActive(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenants("v1", 1);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenants).toString(), resultJson.get("data").get("tenants").toString(),
                true);
    }

    @Test
    public void getTenants_validIdWithWildcardAccess_returnTenantsFromService() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();
        Mockito.doReturn(true).when(mockedSessionData)
                .hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION.getName(), 1);

        PreviewerTenant tenant1 = new PreviewerTenant();
        tenant1.setPreviewerTenantId(1L);
        PreviewerTenant tenant2 = new PreviewerTenant();
        tenant2.setPreviewerTenantId(2L);

        ArrayList<PreviewerTenant> tenants = new ArrayList<>();
        tenants.add(tenant1);
        tenants.add(tenant2);
        Mockito.doReturn(tenants).when(mockedPreviewerTenantService).findAllActive(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenants("v1", 1);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenants).toString(), resultJson.get("data").get("tenants").toString(),
                true);
    }

    @Test
    public void getTenants_invalidIdWithWildcardAccess_returnEmptyList() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();
        Mockito.doReturn(true).when(mockedSessionData)
                .hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION.getName(), 1);

        ArrayList<PreviewerTenant> tenants = new ArrayList<>();
        Mockito.doReturn(tenants).when(mockedPreviewerTenantService).findAllActive(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenants("v1", 1);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenants).toString(), resultJson.get("data").get("tenants").toString(),
                true);
    }

    @Test
    public void getTenants_validIdWithNonAdminUser_returnTenantsFromService() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();

        Set<Long> allowedIds = new HashSet<>();
        allowedIds.add(1L);
        allowedIds.add(2L);
        Mockito.doReturn(allowedIds).when(mockedSessionData)
                .getAllowedTenantIds(SecureController.Module.QTI_VALIDATION.getName(), 1);

        PreviewerTenant tenant1 = new PreviewerTenant();
        tenant1.setPreviewerTenantId(1L);
        tenant1.setTenantId(1L);
        PreviewerTenant tenant2 = new PreviewerTenant();
        tenant2.setPreviewerTenantId(2L);
        tenant2.setTenantId(2L);

        ArrayList<PreviewerTenant> tenants = new ArrayList<>();
        tenants.add(tenant1);
        tenants.add(tenant2);
        Mockito.doReturn(tenants).when(mockedPreviewerTenantService).findAllActive(1, allowedIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenants("v1", 1);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenants).toString(), resultJson.get("data").get("tenants").toString(),
                true);
    }

    @Test
    public void getTenants_invalidIdWithNonAdminUser_returnEmptyList() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();

        Set<Long> allowedIds = new HashSet<>();
        Mockito.doReturn(allowedIds).when(mockedSessionData)
                .getAllowedTenantIds(SecureController.Module.QTI_VALIDATION.getName(), 2);

        ArrayList<PreviewerTenant> tenants = new ArrayList<>();
        Mockito.doReturn(tenants).when(mockedPreviewerTenantService).findAllActive(2, allowedIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenants("v1", 2);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenants).toString(), resultJson.get("data").get("tenants").toString(),
                true);
    }

    @Test
    public void getTenant_validIdWithAdminUser_returnPreviewerFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        PreviewerTenant tenant = new PreviewerTenant();
        tenant.setPreviewerTenantId(1L);
        tenant.setTenantId(1L);
        Mockito.doReturn(Optional.of(tenant)).when(mockedPreviewerTenantService).findActive(1, 1L);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenant("v1", 1, 1L);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenant).toString(), resultJson.get("data").get("tenant").toString(), false);
    }

    @Test
    public void getTenant_invalidIdWithAdminUser_returnNotFoundMessage() throws Exception {
        // Given
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        Mockito.doReturn(Optional.empty()).when(mockedPreviewerTenantService).findActive(1, 1L);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenant("v1", 1, 1L);
        });

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Error message should be correct", resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find the requested tenant."));
    }

    @Test
    public void getTenant_validIdWithWildcardAccess_returnPreviewerFromService() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();
        Mockito.doReturn(true).when(mockedSessionData)
                .hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION.getName(), 1);

        PreviewerTenant tenant = new PreviewerTenant();
        tenant.setPreviewerTenantId(1L);
        tenant.setTenantId(1L);
        Mockito.doReturn(Optional.of(tenant)).when(mockedPreviewerTenantService).findActive(1, 1L);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenant("v1", 1, 1L);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenant).toString(), resultJson.get("data").get("tenant").toString(), false);
    }

    @Test
    public void getTenant_invalidIdWithWildcardAccess_returnNotFoundMessage() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();
        Mockito.doReturn(true).when(mockedSessionData)
                .hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION.getName(), 1);

        Mockito.doReturn(Optional.empty()).when(mockedPreviewerTenantService).findActive(1, 1L);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenant("v1", 1, 1L);
        });

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Error message should be correct", resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find the requested tenant."));
    }

    @Test
    public void getTenant_validIdWithNonAdminUser_returnPreviewerFromService() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();

        Set<Long> allowedIds = new HashSet<>();
        allowedIds.add(1L);
        allowedIds.add(2L);
        Mockito.doReturn(allowedIds).when(mockedSessionData)
                .getAllowedTenantIds(SecureController.Module.QTI_VALIDATION.getName(), 1);

        PreviewerTenant tenant = new PreviewerTenant();
        tenant.setPreviewerTenantId(1L);
        tenant.setTenantId(1L);
        Mockito.doReturn(Optional.of(tenant)).when(mockedPreviewerTenantService).findActive(1, 1L);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenant("v1", 1, 1L);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(tenant).toString(), resultJson.get("data").get("tenant").toString(), false);
    }

    @Test
    public void getTenant_invalidIdWithNonAdminUser_returnNotFoundMessage() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();

        Set<Long> allowedIds = new HashSet<>();
        allowedIds.add(2L);
        Mockito.doReturn(allowedIds).when(mockedSessionData)
                .getAllowedTenantIds(SecureController.Module.QTI_VALIDATION.getName(), 1);

        PreviewerTenant tenant = new PreviewerTenant();
        tenant.setPreviewerTenantId(1L);
        tenant.setTenantId(1L);
        Mockito.doReturn(Optional.of(tenant)).when(mockedPreviewerTenantService).findActive(1, 1L);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return tenantController.getTenant("v1", 1, 1L);
        });

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Error message should be correct", resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find the requested tenant."));
    }

}
