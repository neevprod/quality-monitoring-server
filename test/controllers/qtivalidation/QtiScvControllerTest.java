package controllers.qtivalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.SecureController;
import global.TestWithApplication;
import models.HumanUser;
import models.JobType;
import models.qtivalidation.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.JobTypeService;
import services.qtivalidation.*;
import services.usermanagement.HumanUserService;
import util.SessionData;

import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class QtiScvControllerTest extends TestWithApplication {
    private final static String qtiScvExecutionsPermission = "qtiScvExecutions";
    private final static int previewerId = 1;
    private final static long tenantId = 3L;

    private SessionData sessionData;
    private HistExecService histExecService;
    private HistTenantService histTenantService;
    private HistItemService histItemService;
    private HistItemExceptionService histItemExceptionService;
    private HistTestCaseService histTestCaseService;
    private JobTypeService jobTypeService;
    private PreviewerTenantService previewerTenantService;
    private UserTenantJobSubscriptionService userTenantJobSubscriptionService;
    private HumanUserService humanUserService;
    private Http.RequestBuilder requestBuilder;
    private QtiScvController qtiScvController;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() {
        sessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(sessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        histExecService = Mockito.mock(HistExecService.class);
        histTenantService = Mockito.mock(HistTenantService.class);
        histItemService = Mockito.mock(HistItemService.class);
        histItemExceptionService = Mockito.mock(HistItemExceptionService.class);
        histTestCaseService = Mockito.mock(HistTestCaseService.class);
        jobTypeService = Mockito.mock(JobTypeService.class);
        previewerTenantService = Mockito.mock(PreviewerTenantService.class);
        userTenantJobSubscriptionService = Mockito.mock(UserTenantJobSubscriptionService.class);
        humanUserService = Mockito.mock(HumanUserService.class);
        requestBuilder = new Http.RequestBuilder();

        qtiScvController = Mockito.spy(new QtiScvController(sessionDataFactory, histExecService, histTenantService,
                histItemService, histItemExceptionService, histTestCaseService, jobTypeService, previewerTenantService,
                userTenantJobSubscriptionService, humanUserService));
    }

    @Test
    public void getQtiScvExecutions_adminUser_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).isAdmin();

        HistExec histExec1 = new HistExec();
        histExec1.setHistExecId(3);
        HistExec histExec2 = new HistExec();
        histExec2.setHistExecId(5);
        List<HistExec> histExecs = new ArrayList<>();
        histExecs.add(histExec1);
        histExecs.add(histExec2);
        Mockito.doReturn(histExecs).when(histExecService).findAllByPreviewerId(previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvExecutions("v1", previewerId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histExecs).toString(),
                resultJson.get("data").get("qtiScvExecutions").toString(), true);
    }

    @Test
    public void getQtiScvExecutions_withWildcardAccess_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);

        HistExec histExec1 = new HistExec();
        histExec1.setHistExecId(3);
        HistExec histExec2 = new HistExec();
        histExec2.setHistExecId(5);
        List<HistExec> histExecs = new ArrayList<>();
        histExecs.add(histExec1);
        histExecs.add(histExec2);
        Mockito.doReturn(histExecs).when(histExecService).findAllByPreviewerId(previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvExecutions("v1", previewerId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histExecs).toString(),
                resultJson.get("data").get("qtiScvExecutions").toString(), true);
    }

    @Test
    public void getQtiScvExecutions_withPermission_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);

        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);
        tenantIds.add(4L);
        Mockito.doReturn(tenantIds).when(sessionData).getAllowedTenantIds(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);

        HistExec histExec1 = new HistExec();
        histExec1.setHistExecId(3);
        HistExec histExec2 = new HistExec();
        histExec2.setHistExecId(5);
        List<HistExec> histExecs = new ArrayList<>();
        histExecs.add(histExec1);
        histExecs.add(histExec2);
        Mockito.doReturn(histExecs).when(histExecService).findAllByPreviewerId(previewerId, tenantIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvExecutions("v1", previewerId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histExecs).toString(),
                resultJson.get("data").get("qtiScvExecutions").toString(), true);
    }

    @Test
    public void getQtiScvExecutions_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvExecutions("v1", previewerId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getQtiScvTenantExecutions_adminUser_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).isAdmin();

        int qtiScvExecutionId = 21;

        PreviewerTenant previewerTenant1 = new PreviewerTenant();
        previewerTenant1.setPreviewerId(previewerId);
        previewerTenant1.setTenantId(2L);
        HistTenant histTenant1 = new HistTenant();
        histTenant1.setHistTenantId(2);
        histTenant1.setPreviewerTenant(previewerTenant1);

        PreviewerTenant previewerTenant2 = new PreviewerTenant();
        previewerTenant2.setPreviewerId(previewerId);
        previewerTenant2.setTenantId(4L);
        HistTenant histTenant2 = new HistTenant();
        histTenant2.setHistTenantId(8);
        histTenant2.setPreviewerTenant(previewerTenant2);

        List<HistTenant> histTenants = new ArrayList<>();
        histTenants.add(histTenant1);
        histTenants.add(histTenant2);
        Mockito.doReturn(histTenants).when(histTenantService).findAllByHistExecId(qtiScvExecutionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecutions("v1", previewerId, qtiScvExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histTenants).toString(),
                resultJson.get("data").get("qtiScvTenantExecutions").toString(), true);
    }

    @Test
    public void getQtiScvTenantExecutions_withWildcardQtiScvExecutionAccess_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                        1L);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                        2L);

        int qtiScvExecutionId = 21;

        PreviewerTenant previewerTenant1 = new PreviewerTenant();
        previewerTenant1.setPreviewerId(previewerId);
        previewerTenant1.setTenantId(1L);
        HistTenant histTenant1 = new HistTenant();
        histTenant1.setHistTenantId(2);
        histTenant1.setPreviewerTenant(previewerTenant1);

        PreviewerTenant previewerTenant2 = new PreviewerTenant();
        previewerTenant2.setPreviewerId(previewerId);
        previewerTenant2.setTenantId(2L);
        HistTenant histTenant2 = new HistTenant();
        histTenant2.setHistTenantId(8);
        histTenant2.setPreviewerTenant(previewerTenant2);

        List<HistTenant> histTenants = new ArrayList<>();
        histTenants.add(histTenant1);
        histTenants.add(histTenant2);
        Mockito.doReturn(histTenants).when(histTenantService).findAllByHistExecId(qtiScvExecutionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecutions("v1", previewerId, qtiScvExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histTenants).toString(),
                resultJson.get("data").get("qtiScvTenantExecutions").toString(), true);
    }

    @Test
    public void
    getQtiScvTenantExecutions_withWildcardAccessButNotWildcardQtiScvExecutionAccess_returnAllowedDataFromService()
            throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 1L);
        Mockito.doReturn(false).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 2L);

        int qtiScvExecutionId = 21;

        PreviewerTenant previewerTenant1 = new PreviewerTenant();
        previewerTenant1.setPreviewerId(previewerId);
        previewerTenant1.setTenantId(1L);
        HistTenant histTenant1 = new HistTenant();
        histTenant1.setHistTenantId(2);
        histTenant1.setPreviewerTenant(previewerTenant1);

        PreviewerTenant previewerTenant2 = new PreviewerTenant();
        previewerTenant2.setPreviewerId(previewerId);
        previewerTenant2.setTenantId(2L);
        HistTenant histTenant2 = new HistTenant();
        histTenant2.setHistTenantId(8);
        histTenant2.setPreviewerTenant(previewerTenant2);

        List<HistTenant> histTenants = new ArrayList<>();
        histTenants.add(histTenant1);
        histTenants.add(histTenant2);
        Mockito.doReturn(histTenants).when(histTenantService).findAllByHistExecId(qtiScvExecutionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecutions("v1", previewerId, qtiScvExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));

        List<HistTenant> expectedHistTenants = new ArrayList<>();
        expectedHistTenants.add(histTenant1);
        JSONAssert.assertEquals(Json.toJson(expectedHistTenants).toString(),
                resultJson.get("data").get("qtiScvTenantExecutions").toString(), true);
    }

    @Test
    public void getQtiScvTenantExecutions_withPermission_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 2L);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 4L);

        int qtiScvExecutionId = 21;

        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);
        tenantIds.add(4L);
        Mockito.doReturn(tenantIds).when(sessionData).getAllowedTenantIds(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);

        PreviewerTenant previewerTenant1 = new PreviewerTenant();
        previewerTenant1.setPreviewerId(previewerId);
        previewerTenant1.setTenantId(2L);
        HistTenant histTenant1 = new HistTenant();
        histTenant1.setHistTenantId(2);
        histTenant1.setPreviewerTenant(previewerTenant1);

        PreviewerTenant previewerTenant2 = new PreviewerTenant();
        previewerTenant2.setPreviewerId(previewerId);
        previewerTenant2.setTenantId(4L);
        HistTenant histTenant2 = new HistTenant();
        histTenant2.setHistTenantId(8);
        histTenant2.setPreviewerTenant(previewerTenant2);

        List<HistTenant> histTenants = new ArrayList<>();
        histTenants.add(histTenant1);
        histTenants.add(histTenant2);
        Mockito.doReturn(histTenants).when(histTenantService)
                .findAllByHistExecId(qtiScvExecutionId, previewerId, tenantIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecutions("v1", previewerId, qtiScvExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histTenants).toString(),
                resultJson.get("data").get("qtiScvTenantExecutions").toString(), true);
    }

    @Test
    public void getQtiScvTenantExecutions_withQtiScvExecutionPermissionForOneTenant_returnAllowedDataFromService()
            throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 2L);
        Mockito.doReturn(false).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 4L);

        int qtiScvExecutionId = 21;

        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);
        tenantIds.add(4L);
        Mockito.doReturn(tenantIds).when(sessionData).getAllowedTenantIds(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);

        PreviewerTenant previewerTenant1 = new PreviewerTenant();
        previewerTenant1.setPreviewerId(previewerId);
        previewerTenant1.setTenantId(2L);
        HistTenant histTenant1 = new HistTenant();
        histTenant1.setHistTenantId(2);
        histTenant1.setPreviewerTenant(previewerTenant1);

        PreviewerTenant previewerTenant2 = new PreviewerTenant();
        previewerTenant2.setPreviewerId(previewerId);
        previewerTenant2.setTenantId(4L);
        HistTenant histTenant2 = new HistTenant();
        histTenant2.setHistTenantId(8);
        histTenant2.setPreviewerTenant(previewerTenant2);

        List<HistTenant> histTenants = new ArrayList<>();
        histTenants.add(histTenant1);
        histTenants.add(histTenant2);
        Mockito.doReturn(histTenants).when(histTenantService)
                .findAllByHistExecId(qtiScvExecutionId, previewerId, tenantIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecutions("v1", previewerId, qtiScvExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));

        List<HistTenant> expectedHistTenants = new ArrayList<>();
        expectedHistTenants.add(histTenant1);
        JSONAssert.assertEquals(Json.toJson(expectedHistTenants).toString(),
                resultJson.get("data").get("qtiScvTenantExecutions").toString(), true);
    }

    @Test
    public void getQtiScvTenantExecutions_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecutions("v1", previewerId, 21));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getQtiScvTenantExecution_adminUser_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).isAdmin();
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 1L);

        int qtiScvExecutionId = 21;
        int qtiScvTenantExecutionId = 12;

        PreviewerTenant previewerTenant = new PreviewerTenant();
        previewerTenant.setPreviewerId(previewerId);
        previewerTenant.setTenantId(1L);
        HistTenant histTenant = new HistTenant();
        histTenant.setHistTenantId(2);
        histTenant.setPreviewerTenant(previewerTenant);
        Optional<HistTenant> histTenantOptional = Optional.of(histTenant);
        Mockito.doReturn(histTenantOptional).when(histTenantService).find(qtiScvTenantExecutionId, qtiScvExecutionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecution("v1", previewerId, qtiScvExecutionId,
                        qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histTenant).toString(),
                resultJson.get("data").get("qtiScvTenantExecution").toString(), true);
    }

    @Test
    public void getQtiScvTenantExecution_withWildcardAccess_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 1L);

        int qtiScvExecutionId = 21;
        int qtiScvTenantExecutionId = 12;

        PreviewerTenant previewerTenant = new PreviewerTenant();
        previewerTenant.setPreviewerId(previewerId);
        previewerTenant.setTenantId(1L);
        HistTenant histTenant = new HistTenant();
        histTenant.setHistTenantId(2);
        histTenant.setPreviewerTenant(previewerTenant);
        Optional<HistTenant> histTenantOptional = Optional.of(histTenant);
        Mockito.doReturn(histTenantOptional).when(histTenantService).find(qtiScvTenantExecutionId, qtiScvExecutionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecution("v1", previewerId, qtiScvExecutionId,
                        qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histTenant).toString(),
                resultJson.get("data").get("qtiScvTenantExecution").toString(), true);
    }

    @Test
    public void getQtiScvTenantExecution_withWildcardAccessButNoQtiScvExecutionPermission_returnAccessDeniedResult()
            throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData).hasWildcardAccessToPreviewer(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);
        Mockito.doReturn(false).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 1L);

        int qtiScvExecutionId = 21;
        int qtiScvTenantExecutionId = 12;

        PreviewerTenant previewerTenant = new PreviewerTenant();
        previewerTenant.setPreviewerId(previewerId);
        previewerTenant.setTenantId(1L);
        HistTenant histTenant = new HistTenant();
        histTenant.setHistTenantId(2);
        histTenant.setPreviewerTenant(previewerTenant);
        Optional<HistTenant> histTenantOptional = Optional.of(histTenant);
        Mockito.doReturn(histTenantOptional).when(histTenantService).find(qtiScvTenantExecutionId, qtiScvExecutionId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecution("v1", previewerId, qtiScvExecutionId,
                        qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getQtiScvTenantExecution_withPermission_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 2L);

        int qtiScvExecutionId = 21;
        int qtiScvTenantExecutionId = 12;

        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);
        tenantIds.add(4L);
        Mockito.doReturn(tenantIds).when(sessionData).getAllowedTenantIds(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);

        PreviewerTenant previewerTenant = new PreviewerTenant();
        previewerTenant.setPreviewerId(previewerId);
        previewerTenant.setTenantId(2L);
        HistTenant histTenant = new HistTenant();
        histTenant.setHistTenantId(2);
        histTenant.setPreviewerTenant(previewerTenant);
        Optional<HistTenant> histTenantOptional = Optional.of(histTenant);
        Mockito.doReturn(histTenantOptional).when(histTenantService)
                .find(qtiScvTenantExecutionId, qtiScvExecutionId, previewerId, tenantIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecution("v1", previewerId, qtiScvExecutionId,
                        qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histTenant).toString(),
                resultJson.get("data").get("qtiScvTenantExecution").toString(), true);
    }

    @Test
    public void getQtiScvTenantExecution_noQtiScvExecutionPermissionForTenant_returnAccessDeniedResult()
            throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);
        Mockito.doReturn(false).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, 2L);

        int qtiScvExecutionId = 21;
        int qtiScvTenantExecutionId = 12;

        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);
        tenantIds.add(4L);
        Mockito.doReturn(tenantIds).when(sessionData).getAllowedTenantIds(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);

        PreviewerTenant previewerTenant = new PreviewerTenant();
        previewerTenant.setPreviewerId(previewerId);
        previewerTenant.setTenantId(2L);
        HistTenant histTenant = new HistTenant();
        histTenant.setHistTenantId(2);
        histTenant.setPreviewerTenant(previewerTenant);
        Optional<HistTenant> histTenantOptional = Optional.of(histTenant);
        Mockito.doReturn(histTenantOptional).when(histTenantService)
                .find(qtiScvTenantExecutionId, qtiScvExecutionId, previewerId, tenantIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecution("v1", previewerId, qtiScvExecutionId,
                        qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getQtiScvTenantExecution_withPermissionButInvalidId_returnNotFoundResult() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);

        int qtiScvExecutionId = 21;
        int qtiScvTenantExecutionId = 12;

        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);
        tenantIds.add(4L);
        Mockito.doReturn(tenantIds).when(sessionData).getAllowedTenantIds(SecureController.Module.QTI_VALIDATION
                .getName(), previewerId);

        Optional<HistTenant> histTenantOptional = Optional.empty();
        Mockito.doReturn(histTenantOptional).when(histTenantService)
                .find(qtiScvTenantExecutionId, qtiScvExecutionId, previewerId, tenantIds);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecution("v1", previewerId, qtiScvExecutionId,
                        qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getQtiScvTenantExecution_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData)
                .hasAnyTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId);

        int qtiScvExecutionId = 21;
        int qtiScvTenantExecutionId = 12;

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getQtiScvTenantExecution("v1", previewerId, qtiScvExecutionId,
                        qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getValidatedItems_withPermission_returnDataFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, tenantId);

        int qtiScvTenantExecutionId = 12;

        HistItem histItem1 = new HistItem();
        histItem1.setHistItemId(1);
        HistItem histItem2 = new HistItem();
        histItem1.setHistItemId(2);
        List<HistItem> histItems = new ArrayList<>();
        histItems.add(histItem1);
        histItems.add(histItem2);
        Mockito.doReturn(histItems).when(histItemService)
                .findAllByHistTenantId(qtiScvTenantExecutionId, previewerId, tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getValidatedItems("v1", previewerId, tenantId, qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(histItems).toString(),
                resultJson.get("data").get("validatedItems").toString(), true);
    }

    @Test
    public void getValidatedItems_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData)
                .hasTenantLevelPermission(qtiScvExecutionsPermission, SecureController.AccessType.READ,
                        SecureController.Module.QTI_VALIDATION.getName(), previewerId, tenantId);

        int qtiScvTenantExecutionId = 12;

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> qtiScvController.getValidatedItems("v1", previewerId, tenantId, qtiScvTenantExecutionId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
    }

    @Test
    public void createQtiScvTenantJobSubscriptions_withSuccessResult() throws  Exception{
        // Given
        Mockito.doReturn(true).when(sessionData).hasTenantLevelPermission("qtiScvTenantJobSubscriptions",
                SecureController.AccessType.CREATE, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                tenantId);

        JobType jobType = new JobType();
        jobType.setCode("SCV");
        jobType.setName("Test");
        jobType.setJobTypeId(0);

        Previewer previewer = new Previewer();
        previewer.setPreviewerId(previewerId);
        PreviewerTenant previewerTenant = new PreviewerTenant(previewer, tenantId, null, true);
        previewerTenant.setPreviewerTenantId(0L);

        HumanUser humanUser = new HumanUser();
        humanUser.setHumanUserId(0L);
        humanUser.setUserId(0L);

        Mockito.doReturn(Optional.of(humanUser)).when(humanUserService).findByUserId(0L);
        Mockito.doReturn(Optional.of(jobType)).when(jobTypeService).findByName("Score Consistency Validation");
        Mockito.doReturn(Optional.of(previewerTenant)).when(previewerTenantService)
                .findActiveWithNoJobSubscription(previewerId, tenantId, 0L, 0);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return qtiScvController.createQtiScvTenantJobSubscriptions("v1", previewerId,tenantId);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
    }

    @Test
    public void createQtiScvTenantJobSubscriptions_noPermission_withoutSuccessResult() throws  Exception{
        // Given
        Mockito.doReturn(false).when(sessionData).hasTenantLevelPermission("qtiScvTenantJobSubscriptions",
                SecureController.AccessType.CREATE, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return qtiScvController.createQtiScvTenantJobSubscriptions("v1", previewerId, tenantId);
        });

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
    }

    @Test
    public void removeQtiScvTenantJobSubscription_withSuccessResult() throws  Exception{
        // Given
        Mockito.doReturn(true).when(sessionData).hasTenantLevelPermission("qtiScvTenantJobSubscriptions",
                SecureController.AccessType.DELETE, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                tenantId);

        JobType jobType = new JobType();
        jobType.setCode("SCV");
        jobType.setName("Test");
        jobType.setJobTypeId(0);

        HumanUser humanUser = new HumanUser();
        humanUser.setHumanUserId(0L);
        humanUser.setUserId(0L);

        UserTenantJobSubscription subscription=Mockito.mock(UserTenantJobSubscription.class);

        Mockito.doReturn(Optional.of(humanUser)).when(humanUserService).findByUserId(0L);
        Mockito.doReturn(Optional.of(jobType)).when(jobTypeService).findByName("Score Consistency Validation");
        Mockito.doReturn(Optional.of(subscription)).when(userTenantJobSubscriptionService)
                .findByPreviewerIdTenantIdUserIdAndJobTypeId(previewerId, tenantId, 0L, 0);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return qtiScvController.removeQtiScvTenantJobSubscription("v1", previewerId,tenantId);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        Mockito.verify(userTenantJobSubscriptionService).delete(subscription);
    }

    @Test
    public void removeQtiScvTenantJobSubscription__noPermission_withoutSuccessResult() throws  Exception{
        // Given
        Mockito.doReturn(false).when(sessionData).hasTenantLevelPermission("qtiScvTenantJobSubscriptions",
                SecureController.AccessType.DELETE, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return qtiScvController.removeQtiScvTenantJobSubscription("v1", previewerId,tenantId);
        });

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
    }
}
