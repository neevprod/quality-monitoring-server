package controllers.qtivalidation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pearson.itautomation.tn8.previewer.api.GetItemScoringFingerprintResult;
import com.pearson.itautomation.tn8.previewer.api.GetItemScoringTestCasesResult;
import com.pearson.itautomation.tn8.previewer.api.GetTenantItemsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import controllers.JobControllerTest;
import controllers.SecureController;
import global.TestWithApplication;
import info.solidsoft.mockito.java8.LambdaMatcher;
import jobs.qtivalidation.TestCaseUploadJobStarter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import util.SessionData;
import util.qtivalidation.ItemResponsePoolFactory;
import util.qtivalidation.TestCaseCoverageFactory;
import util.qtivalidation.TestNav8APIFactory;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TestNav8API.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class TestCaseControllerTest extends TestWithApplication {
    private int previewerId;
    private long tenantId;
    private int itemId;
    private String itemIdentifier;
    private String testCasesPermission;
    private String Items;
    private SessionData mockedSessionData;
    private TestNav8API mockedApi;
    private TestNav8APIFactory mockedApiFactory;
    private Http.RequestBuilder requestBuilder;
    private TestCaseController testCaseController;
    private ItemResponsePoolFactory itemResponsePoolFactory;
    private TestCaseCoverageFactory testCaseCoverageFactory;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() throws Exception {
        previewerId = 3;
        tenantId = 2;
        itemId = 1;
        itemIdentifier = "item1";
        testCasesPermission = "testCases";
        Items="items";
        mockedSessionData = Mockito.mock(SessionData.class);
        Mockito.doReturn(true).when(mockedSessionData)
                .hasTenantLevelPermission(testCasesPermission, SecureController.AccessType.READ, SecureController
                        .Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        Mockito.doReturn(true).when(mockedSessionData)
        .hasTenantLevelPermission(testCasesPermission, SecureController.AccessType.DELETE, SecureController
                .Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        Mockito.doReturn(true).when(mockedSessionData)
        .hasTenantLevelPermission(Items, SecureController.AccessType.READ, SecureController
                .Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        mockedApi = Mockito.mock(TestNav8API.class);
        mockedApiFactory = Mockito.mock(TestNav8APIFactory.class);
        Mockito.doReturn(Optional.of(mockedApi)).when(mockedApiFactory).create(previewerId);

        requestBuilder = new Http.RequestBuilder();
        itemResponsePoolFactory = Mockito.mock(ItemResponsePoolFactory.class);
        testCaseCoverageFactory = Mockito.mock(TestCaseCoverageFactory.class);
        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        ActorRef jobRunnerActor = actorSystem.actorOf(Props.create(JobControllerTest.MockJobRunnerActor.class));
        TestCaseUploadJobStarter.Factory testCaseUploadJobStarterFactory =
                app.injector().instanceOf(TestCaseUploadJobStarter.Factory.class);

        testCaseController = Mockito.spy(new TestCaseController(sessionDataFactory, mockedApiFactory,
                itemResponsePoolFactory, testCaseCoverageFactory, jobRunnerActor, testCaseUploadJobStarterFactory,
                app.config()));
    }

    @Test
    public void getTestCases_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData)
                .hasTenantLevelPermission(testCasesPermission, SecureController.AccessType.READ, SecureController
                        .Module.QTI_VALIDATION.getName(), previewerId, tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseController.getTestCases("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getTestCases_previewerNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doReturn(Optional.empty()).when(mockedApiFactory).create(previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseController.getTestCases("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a previewer with the given ID."));
    }

    @Test
    public void getTestCases_tenantNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doThrow(new NullPointerException()).when(mockedApi).getTenantItems(Matchers.anyInt(), Matchers.any());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseController.getTestCases("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find an item with the given ID."));
    }

    @Test
    public void getTestCases_withPermission_returnDataFromApi() throws Exception {
        // Given
        String testCasesJson = "{\n" +
                "    \"api\": \"getItemScoringTestcases\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"testcases\": [{\n" +
                "            \"id\": \"36\",\n" +
                "            \"tenant_id\": \"1\",\n" +
                "            \"item_identifier\": \"item1\",\n" +
                "            \"mods\": [{\n" +
                "                \"did\": \"RESPONSE\",\n" +
                "                \"r\": [\"C R\"]\n" +
                "            }],\n" +
                "            \"outcomes\": [{\n" +
                "                \"identifier\": \"SCORE\",\n" +
                "                \"value\": \"1\"\n" +
                "            }, {\n" +
                "                \"identifier\": \"scoreSystem\",\n" +
                "                \"value\": \"machine\"\n" +
                "            }]\n" +
                "        }, {\n" +
                "            \"id\": \"37\",\n" +
                "            \"tenant_id\": \"1\",\n" +
                "            \"item_identifier\": \"item1\",\n" +
                "            \"mods\": [{\n" +
                "                \"did\": \"RESPONSE\",\n" +
                "                \"r\": [\"D M\", \"C R\"]\n" +
                "            }],\n" +
                "            \"outcomes\": [{\n" +
                "                \"identifier\": \"SCORE\",\n" +
                "                \"value\": \"1\"\n" +
                "            }]\n" +
                "        }]\n" +
                "    }\n" +
                "}";
        Mockito.doReturn(new GetItemScoringTestCasesResult(testCasesJson))
                .when(mockedApi).getItemScoringTestCases((int) tenantId, itemIdentifier);

        String itemJson = "{\n" +
                "    \"api\": \"getItemsForTenant\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"tenant\": \"1\",\n" +
                "        \"nTotal\": \"4272\",\n" +
                "        \"nTotalAfterFilter\": \"4272\",\n" +
                "        \"nTotalInArray\": \"2\",\n" +
                "        \"items\": [\n" +
                "            {\n" +
                "                \"id\": \"1\",\n" +
                "                \"identifier\": \"item1\",\n" +
                "                \"nTestCases\": \"7\",\n" +
                "                \"hasFingerprint\": \"yes\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}\n";
        Mockito.doReturn(new GetTenantItemsResult(itemJson))
                .when(mockedApi).getTenantItems(Matchers.eq((int) tenantId),
                        LambdaMatcher.argLambda((map) -> String.valueOf(itemId)
                                .equals(map.get(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS))));

        String fingerprintJson = "{\n" +
                "    \"api\": \"getItemScoringFingerprint\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"input\": \"{\\\"items\\\":[{\\\"itemRef\\\":{\\\"externalItemId\\\":\\\"item1\\\",\\\"id\\\":\\\"1\\\"},\\\"state\\\":{\\\"mods\\\": [{\\\"did\\\":\\\"RESPONSE\\\",\\\"r\\\":[\\\"C R\\\"]}]}}]}\",\n" +
                "        \"output\": \"{\\\"api\\\":\\\"scoreBulkItemsByIdentifierTN8\\\",\\\"success\\\":\\\"yes\\\",\\\"msg\\\":{\\\"scores\\\":[{\\\"itemIdentifier\\\":\\\"item1\\\",\\\"itemType\\\":\\\"Multiple Choice\\\",\\\"SCORE\\\":\\\"0\\\",\\\"MAXSCORE\\\":\\\"1.0\\\",\\\"success\\\":\\\"yes\\\",\\\"msg\\\":\\\"success\\\"}]}}\",\n" +
                "        \"modifyDate\": \"2015-02-18 23:57:02.0\"\n" +
                "    }\n" +
                "}\n";
        Mockito.doReturn(new GetItemScoringFingerprintResult(fingerprintJson))
                .when(mockedApi).getItemScoringFingerprint((int) tenantId, itemIdentifier);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseController.getTestCases("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals("[{\n" +
                        "    \"id\": 36,\n" +
                        "    \"tenant_id\": 1,\n" +
                        "    \"fingerprinted\": true,\n" +
                        "    \"item_identifier\": \"item1\",\n" +
                        "    \"mods\": [{\n" +
                        "        \"did\": \"RESPONSE\",\n" +
                        "        \"r\": [\"C R\"]\n" +
                        "    }],\n" +
                        "    \"outcomes\": [{\n" +
                        "        \"identifier\": \"SCORE\",\n" +
                        "        \"value\": \"1\"\n" +
                        "    }, {\n" +
                        "        \"identifier\": \"scoreSystem\",\n" +
                        "        \"value\": \"machine\"\n" +
                        "    }]\n" +
                        "}, {\n" +
                        "    \"id\": 37,\n" +
                        "    \"tenant_id\": 1,\n" +
                        "    \"fingerprinted\": false,\n" +
                        "    \"item_identifier\": \"item1\",\n" +
                        "    \"mods\": [{\n" +
                        "        \"did\": \"RESPONSE\",\n" +
                        "        \"r\": [\"D M\", \"C R\"]\n" +
                        "    }],\n" +
                        "    \"outcomes\": [{\n" +
                        "        \"identifier\": \"SCORE\",\n" +
                        "        \"value\": \"1\"\n" +
                        "    }]\n" +
                        "}]",
                resultJson.get("data").get("testCases").toString(), true);
    }

    @Test
    public void getTestCases_itemNotFound_returnNotFound() throws Exception {
        // Given
        String itemJson = "{\n" +
                "    \"api\": \"getTenants\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"tenant\": \"1\",\n" +
                "        \"nTotal\": \"4329\",\n" +
                "        \"nTotalAfterFilter\": \"0\",\n" +
                "        \"nTotalInArray\": \"0\",\n" +
                "        \"items\": []\n" +
                "    }\n" +
                "}\n";
        Mockito.doReturn(new GetTenantItemsResult(itemJson))
                .when(mockedApi).getTenantItems(Matchers.eq((int) tenantId),
                        LambdaMatcher.argLambda((map) -> String.valueOf(itemId)
                                .equals(map.get(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS))));

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseController.getTestCases("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find an item with the given ID."));
    }
    
    @Test
    public void deleteTestCases_itemNotFound_returnNotFound() throws Exception {
        // Given
        requestBuilder.uri("?parsedCsv=%5B%22111111%22,%22VJ-orderspinner%22%5D");
        String testCasesJson = "{\n" +
                "    \"api\": \"getItemScoringTestcases\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"testcases\": [{\n" +
                "            \"id\": \"36\",\n" +
                "            \"tenant_id\": \"1\",\n" +
                "            \"item_identifier\": \"item1\",\n" +
                "            \"mods\": [{\n" +
                "                \"did\": \"RESPONSE\",\n" +
                "                \"r\": [\"C R\"]\n" +
                "            }],\n" +
                "            \"outcomes\": [{\n" +
                "                \"identifier\": \"SCORE\",\n" +
                "                \"value\": \"1\"\n" +
                "            }, {\n" +
                "                \"identifier\": \"scoreSystem\",\n" +
                "                \"value\": \"machine\"\n" +
                "            }]\n" +
                "        }, {\n" +
                "            \"id\": \"37\",\n" +
                "            \"tenant_id\": \"1\",\n" +
                "            \"item_identifier\": \"item1\",\n" +
                "            \"mods\": [{\n" +
                "                \"did\": \"RESPONSE\",\n" +
                "                \"r\": [\"D M\", \"C R\"]\n" +
                "            }],\n" +
                "            \"outcomes\": [{\n" +
                "                \"identifier\": \"SCORE\",\n" +
                "                \"value\": \"1\"\n" +
                "            }]\n" +
                "        }]\n" +
                "    }\n" +
                "}";
        Mockito.doReturn(new GetItemScoringTestCasesResult(testCasesJson))
                .when(mockedApi).getItemScoringTestCases((int) tenantId, itemIdentifier);

        String itemJson = "{\n" +
                "    \"api\": \"getItemsForTenant\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"tenant\": \"1\",\n" +
                "        \"nTotal\": \"4272\",\n" +
                "        \"nTotalAfterFilter\": \"4272\",\n" +
                "        \"nTotalInArray\": \"2\",\n" +
                "        \"items\": [\n" +
                "            {\n" +
                "                \"id\": \"1\",\n" +
                "                \"identifier\": \"item1\",\n" +
                "                \"nTestCases\": \"7\",\n" +
                "                \"hasFingerprint\": \"yes\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}\n";
        Mockito.doReturn(new GetTenantItemsResult(itemJson))
                .when(mockedApi).getTenantItems(Matchers.eq((int) tenantId),
                        LambdaMatcher.argLambda((map) -> String.valueOf(111111)
                                .equals(map.get(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS))));
        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseController.deleteTestCases("v1", previewerId, tenantId));
     
        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find the items"+" [111111]"));
    }

    @Test
    public void deleteTestCases_itemTestCaseDeleted_returnSuccess() throws Exception {
        // Given
    	requestBuilder.uri("?parsedCsv=%5B%2266238%22,%22AUTO0007%22%5D");
    	 String testCasesJson = "{\n" +
                 "    \"api\": \"getItemScoringTestcases\",\n" +
                 "    \"success\": \"yes\",\n" +
                 "    \"msg\": {\n" +
                 "        \"testcases\": [{\n" +
                 "            \"id\": \"36\",\n" +
                 "            \"tenant_id\": \"1\",\n" +
                 "            \"item_identifier\": \"item1\",\n" +
                 "            \"mods\": [{\n" +
                 "                \"did\": \"RESPONSE\",\n" +
                 "                \"r\": [\"C R\"]\n" +
                 "            }],\n" +
                 "            \"outcomes\": [{\n" +
                 "                \"identifier\": \"SCORE\",\n" +
                 "                \"value\": \"1\"\n" +
                 "            }, {\n" +
                 "                \"identifier\": \"scoreSystem\",\n" +
                 "                \"value\": \"machine\"\n" +
                 "            }]\n" +
                 "        }, {\n" +
                 "            \"id\": \"37\",\n" +
                 "            \"tenant_id\": \"1\",\n" +
                 "            \"item_identifier\": \"item1\",\n" +
                 "            \"mods\": [{\n" +
                 "                \"did\": \"RESPONSE\",\n" +
                 "                \"r\": [\"D M\", \"C R\"]\n" +
                 "            }],\n" +
                 "            \"outcomes\": [{\n" +
                 "                \"identifier\": \"SCORE\",\n" +
                 "                \"value\": \"1\"\n" +
                 "            }]\n" +
                 "        }]\n" +
                 "    }\n" +
                 "}";
         Mockito.doReturn(new GetItemScoringTestCasesResult(testCasesJson))
                 .when(mockedApi).getItemScoringTestCases((int) tenantId, "AUTO0007");

         String itemJson = "{\n" +
                 "    \"api\": \"getItemsForTenant\",\n" +
                 "    \"success\": \"yes\",\n" +
                 "    \"msg\": {\n" +
                 "        \"tenant\": \"1\",\n" +
                 "        \"nTotal\": \"4272\",\n" +
                 "        \"nTotalAfterFilter\": \"4272\",\n" +
                 "        \"nTotalInArray\": \"2\",\n" +
                 "        \"items\": [\n" +
                 "            {\n" +
                 "                \"id\": \"66238\",\n" +
                 "                \"identifier\": \"AUTO0007\",\n" +
                 "                \"nTestCases\": \"2\",\n" +
                 "                \"hasFingerprint\": \"yes\"\n" +
                 "            }\n" +
                 "        ]\n" +
                 "    }\n" +
                 "}\n";
         Mockito.doReturn(new GetTenantItemsResult(itemJson))
                 .when(mockedApi).getTenantItems(Matchers.eq((int) tenantId),
                         LambdaMatcher.argLambda((map) -> String.valueOf(66238)
                                 .equals(map.get(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS))));
         
         String fingerprintJson = "{\n" +
                 "    \"api\": \"getItemScoringFingerprint\",\n" +
                 "    \"success\": \"yes\",\n" +
                 "    \"msg\": {\n" +
                 "        \"input\": \"{\\\"items\\\":[{\\\"itemRef\\\":{\\\"externalItemId\\\":\\\"item1\\\",\\\"id\\\":\\\"1\\\"},\\\"state\\\":{\\\"mods\\\": [{\\\"did\\\":\\\"RESPONSE\\\",\\\"r\\\":[\\\"C R\\\"]}]}}]}\",\n" +
                 "        \"output\": \"{\\\"api\\\":\\\"scoreBulkItemsByIdentifierTN8\\\",\\\"success\\\":\\\"yes\\\",\\\"msg\\\":{\\\"scores\\\":[{\\\"itemIdentifier\\\":\\\"item1\\\",\\\"itemType\\\":\\\"Multiple Choice\\\",\\\"SCORE\\\":\\\"0\\\",\\\"MAXSCORE\\\":\\\"1.0\\\",\\\"success\\\":\\\"yes\\\",\\\"msg\\\":\\\"success\\\"}]}}\",\n" +
                 "        \"modifyDate\": \"2015-02-18 23:57:02.0\"\n" +
                 "    }\n" +
                 "}\n";
         Mockito.doReturn(new GetItemScoringFingerprintResult(fingerprintJson))
                 .when(mockedApi).getItemScoringFingerprint((int) tenantId, "AUTO0007");
        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseController.deleteTestCases("v1", previewerId, tenantId));
     
        Mockito.verify(mockedApi).deleteItemScoringTestCase((int)tenantId, 36);
        Mockito.verify(mockedApi).deleteItemScoringTestCase((int)tenantId, 37);	
        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(true));
    }

}
