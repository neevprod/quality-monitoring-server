package controllers.qtivalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import controllers.SecureController;
import global.TestWithApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import util.SessionData;
import util.qtivalidation.TestNav8APIFactory;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TestNav8API.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class TestMapControllerTest extends TestWithApplication {
    private static final String TEST_MAPS = "testMaps";
    private int previewerId;
    private int tenantId;
    private String testMapsPermission;

    private SessionData mockedSessionData;
    private TestNav8API mockedApi;
    private TestNav8APIFactory mockedApiFactory;
    private Http.RequestBuilder requestBuilder;
    private TestMapController testMapController;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() throws Exception {
        previewerId = 1;
        tenantId = 1;
        testMapsPermission = TEST_MAPS;

        mockedSessionData = Mockito.mock(SessionData.class);
        Mockito.doReturn(true).when(mockedSessionData).hasTenantLevelPermission(testMapsPermission,
                SecureController.AccessType.READ, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                (long) tenantId);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        mockedApi = Mockito.mock(TestNav8API.class);
        mockedApiFactory = Mockito.mock(TestNav8APIFactory.class);
        Mockito.doReturn(Optional.of(mockedApi)).when(mockedApiFactory).create(previewerId);

        requestBuilder = new Http.RequestBuilder();
        testMapController = Mockito.spy(new TestMapController(sessionDataFactory, mockedApiFactory));
    }

    @Test
    public void getTestMaps_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).hasTenantLevelPermission(testMapsPermission,
                SecureController.AccessType.READ, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                (long) tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testMapController.getTestMaps("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
    }

    @Test
    public void getTestMaps_previewerNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doReturn(Optional.empty()).when(mockedApiFactory).create(previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testMapController.getTestMaps("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a previewer with the given ID."));
    }

    @Test
    public void getTestMaps_tenantNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doThrow(new NullPointerException()).when(mockedApi).getTestMaps(Matchers.anyInt());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testMapController.getTestMaps("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a tenant with the given ID."));
    }

    @Test
    public void getTestMaps_withPermission_returnDataFromApi() throws Exception {
        // Mock the value returned by TN8 API
        // Given
        String testMapsJson = "{\"api\":\"getTestmaps\",\"success\":\"yes\","
                + "\"msg\":{\"testmaps\":[{\"id\":\"84\",\"identifier\":\"aitup5ms1\","
                + "\"formId\":\"aitup5ms1\",\"formName\":\"Preloading form-multiple section\","
                + "\"title\":\"VJ-Preloading form\",\"tenantId\":\"6\",\"isSecure\":\"true\","
                + "\"dateCreated\":\"2014-08-04 18:03:21.0\",\"grade\":\"\",\"subject\":\"\",\"itemCount\":\"97\"},"
                + "{\"id\":\"145\",\"identifier\":\"aitup1ms1\",\"formId\":\"aitup1ms1\","
                + "\"formName\":\"8.1 Rel-Preloading Form-Multiple Section\","
                + "\"title\":\"8.1 Rel-Preloading Form-Multiple Section\",\"tenantId\":\"6\","
                + "\"isSecure\":\"true\",\"dateCreated\":\"2014-09-12 18:31:44.0\","
                + "\"grade\":\"\",\"subject\":\"\",\"itemCount\":\"103\"}]}}";

        Mockito.doReturn(new GetTestMapsResult(testMapsJson)).when(mockedApi).getTestMaps(tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testMapController.getTestMaps("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        assertThat("Number of total results should be 2.", resultJson.get("data").get("totalResults").asInt(),
                equalTo(2));
        String expectedJson = "[\n" + "{\n" + "\"id\": 84,\n" + "\"identifier\": \"aitup5ms1\",\n"
                + "\"formId\": \"aitup5ms1\",\n" + "\"formName\": \"Preloading form-multiple section\",\n"
                + "\"title\": \"VJ-Preloading form\",\n" + "\"tenantId\": 6,\n"
                + "\"dateCreated\": \"2014-08-04 18:03:21.0\",\n" + "\"itemCount\": 97,\n" + "\"secure\": true\n"
                + "},\n"

                + "{\n" + "\"id\": 145,\n" + "\"identifier\": \"aitup1ms1\",\n" + "\"formId\": \"aitup1ms1\",\n"
                + "\"formName\": \"8.1 Rel-Preloading Form-Multiple Section\",\n"
                + "\"title\": \"8.1 Rel-Preloading Form-Multiple Section\",\n" + "\"tenantId\": 6,\n"
                + "\"dateCreated\": \"2014-09-12 18:31:44.0\",\n" + "\"itemCount\": 103,\n" + "\"secure\": true\n"
                + "},\n" + "]";
        JSONAssert.assertEquals(expectedJson, resultJson.get("data").get("testMaps").toString(), true);
    }
}
