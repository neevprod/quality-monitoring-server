package controllers.qtivalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import controllers.SecureController;
import global.TestWithApplication;
import models.qtivalidation.Previewer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.qtivalidation.PreviewerService;
import util.SessionData;

import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
public class PreviewerControllerTest extends TestWithApplication {
    private SessionData mockedSessionData;
    private PreviewerService mockedPreviewerService;
    private List<Previewer> previewerList;
    private Http.RequestBuilder requestBuilder;
    private PreviewerController previewerController;
    private Config configuration;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() {
        mockedSessionData = Mockito.mock(SessionData.class);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        previewerList = new ArrayList<>();
        mockedPreviewerService = Mockito.mock(PreviewerService.class);
        requestBuilder = new Http.RequestBuilder();
        configuration = ConfigFactory.load(ConfigFactory.defaultApplication());

        previewerController = Mockito.spy(new PreviewerController(sessionDataFactory, mockedPreviewerService, configuration));
    }

    @Test
    public void getPreviewers_adminUser_returnAllPreviewers() throws Exception {
        // Given
        Mockito.doReturn(previewerList).when(mockedPreviewerService).findAll();
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        Previewer previewer1 = new Previewer();
        previewer1.setPreviewerId(1);
        previewer1.setName("previewer1");
        previewer1.setUrl("url1");
        previewer1.setApiUrl("apiUrl1");
        previewer1.setProtocol("https://");
        Previewer previewer2 = new Previewer();
        previewer2.setPreviewerId(2);
        previewer2.setName("previewer2");
        previewer2.setUrl("url2");
        previewer2.setApiUrl("apiUrl2");
        previewer2.setProtocol("https://");

        previewerList.add(previewer1);
        previewerList.add(previewer2);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return previewerController.getPreviewers("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("previewers");

        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(previewerList).toString(), resultData.toString(), true);
    }

    @Test
    public void getPreviewers_noPreviewers_returnEmptyList() throws Exception {
        // Given
        Mockito.doReturn(previewerList).when(mockedPreviewerService).findAll();
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return previewerController.getPreviewers("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("previewers");

        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals("[]", resultData.toString(), true);
    }

    @Test
    public void getPreviewers_nonAdminUser_returnAllowedPreviewers() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();

        Set<Integer> allowedPreviewerIds = new HashSet<>();
        allowedPreviewerIds.add(1);
        allowedPreviewerIds.add(2);
        Mockito.doReturn(allowedPreviewerIds).when(mockedSessionData).getAllowedPreviewerIds(SecureController.Module
                .QTI_VALIDATION.getName());

        Mockito.doReturn(previewerList).when(mockedPreviewerService).findAll(allowedPreviewerIds);

        Previewer previewer1 = new Previewer();
        previewer1.setPreviewerId(1);
        previewer1.setName("previewer1");
        previewer1.setUrl("url1");
        previewer1.setApiUrl("apiUrl1");
        previewer1.setProtocol("https://");
        Previewer previewer2 = new Previewer();
        previewer2.setPreviewerId(2);
        previewer2.setName("previewer2");
        previewer2.setUrl("url2");
        previewer2.setApiUrl("apiUrl2");
        previewer2.setProtocol("https://");

        previewerList.add(previewer1);
        previewerList.add(previewer2);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return previewerController.getPreviewers("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JsonNode resultData = resultJson.get("data").get("previewers");

        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals(Json.toJson(previewerList).toString(), resultData.toString(), true);
    }

    @Test
    public void getPreviewer_validIdWithAdminUser_returnPreviewerFromService() throws Exception {
        // Given
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        Previewer previewer1 = new Previewer();
        previewer1.setPreviewerId(1);
        Mockito.doReturn(Optional.of(previewer1)).when(mockedPreviewerService).find(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return previewerController.getPreviewer("v1", 1);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(previewer1).toString(),
                resultJson.get("data").get("previewer").toString(), false);
    }

    @Test
    public void getPreviewer_invalidIdWithAdminUser_returnNotFoundMessage() throws Exception {
        // Given
        Mockito.doReturn(true).when(mockedSessionData).isAdmin();

        Mockito.doReturn(Optional.empty()).when(mockedPreviewerService).find(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return previewerController.getPreviewer("v1", 1);
        });

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Error message should be correct", resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a previewer with the given ID."));
    }

    @Test
    public void getPreviewer_validIdWithNonAdminUser_returnPreviewerFromService() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();

        Set<Integer> allowedPreviewerIds = new HashSet<>();
        allowedPreviewerIds.add(1);
        allowedPreviewerIds.add(2);
        Mockito.doReturn(allowedPreviewerIds).when(mockedSessionData).getAllowedPreviewerIds(SecureController.Module
                .QTI_VALIDATION.getName());

        Previewer previewer1 = new Previewer();
        previewer1.setPreviewerId(1);
        Mockito.doReturn(Optional.of(previewer1)).when(mockedPreviewerService).find(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return previewerController.getPreviewer("v1", 1);
        });

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        JSONAssert.assertEquals(Json.toJson(previewer1).toString(),
                resultJson.get("data").get("previewer").toString(), false);
    }

    @Test
    public void getPreviewer_invalidIdWithNonAdminUser_returnNotFoundMessage() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).isAdmin();

        Set<Integer> allowedPreviewerIds = new HashSet<>();
        allowedPreviewerIds.add(2);
        Mockito.doReturn(allowedPreviewerIds).when(mockedSessionData).getAllowedPreviewerIds(SecureController.Module
                .QTI_VALIDATION.getName());

        Previewer previewer1 = new Previewer();
        previewer1.setPreviewerId(1);
        Mockito.doReturn(Optional.of(previewer1)).when(mockedPreviewerService).find(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents, () -> {
            Http.Context.current().args.put("session", Json.newObject());
            return previewerController.getPreviewer("v1", 1);
        });

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));

        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Error message should be correct", resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a previewer with the given ID."));
    }

}
