package controllers.qtivalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pearson.itautomation.tn8.previewer.api.GetTenantItemsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import controllers.SecureController;
import global.TestWithApplication;
import info.solidsoft.mockito.java8.LambdaMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import util.SessionData;
import util.qtivalidation.TestNav8APIFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TestNav8API.class, GetTenantItemsResult.class})
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class ItemControllerTest extends TestWithApplication {
    private SessionData mockedSessionData;
    private TestNav8API mockedApi;
    private TestNav8APIFactory mockedApiFactory;
    private Http.RequestBuilder requestBuilder;
    private ItemController itemController;

    private String itemsJsonWithoutItems;
    private String itemsJsonWithItems;

    private int previewerId;
    private long tenantId;
    private int itemId;
    private String itemsPermission;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() throws Exception {
        previewerId = 1;
        tenantId = 2;
        itemId = 3;
        itemsPermission = "items";

        itemsJsonWithoutItems = "{\n" +
                "    \"api\": \"getItemsForTenant\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"tenant\": \"1\",\n" +
                "        \"nTotal\": \"4272\",\n" +
                "        \"nTotalAfterFilter\": \"0\",\n" +
                "        \"nTotalInArray\": \"0\",\n" +
                "        \"items\": []\n" +
                "    }\n" +
                "}\n";
        itemsJsonWithItems = "{\n" +
                "    \"api\": \"getItemsForTenant\",\n" +
                "    \"success\": \"yes\",\n" +
                "    \"msg\": {\n" +
                "        \"tenant\": \"1\",\n" +
                "        \"nTotal\": \"4272\",\n" +
                "        \"nTotalAfterFilter\": \"4272\",\n" +
                "        \"nTotalInArray\": \"2\",\n" +
                "        \"items\": [\n" +
                "            {\n" +
                "                \"id\": \"1\",\n" +
                "                \"identifier\": \"item1\",\n" +
                "                \"nTestCases\": \"7\",\n" +
                "                \"hasFingerprint\": \"yes\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"id\": \"2\",\n" +
                "                \"identifier\": \"item2\",\n" +
                "                \"nTestCases\": \"0\",\n" +
                "                \"hasFingerprint\": \"no\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}\n";

        mockedSessionData = Mockito.mock(SessionData.class);
        Mockito.doReturn(true).when(mockedSessionData)
                .hasTenantLevelPermission(itemsPermission, SecureController.AccessType.READ, SecureController.Module
                        .QTI_VALIDATION.getName(), previewerId, tenantId);

        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        GetTenantItemsResult mockedItemsResult = Mockito.mock(GetTenantItemsResult.class);
        mockedApi = Mockito.mock(TestNav8API.class);
        Mockito.doReturn(mockedItemsResult).when(mockedApi).getTenantItems(Matchers.anyInt(), Matchers.any());
        mockedApiFactory = Mockito.mock(TestNav8APIFactory.class);
        Mockito.doReturn(Optional.of(mockedApi)).when(mockedApiFactory).create(previewerId);

        requestBuilder = new Http.RequestBuilder();
        itemController = Mockito.spy(new ItemController(sessionDataFactory, mockedApiFactory));
    }

    @Test
    public void getItems_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData)
                .hasTenantLevelPermission(itemsPermission, SecureController.AccessType.READ, SecureController.Module
                        .QTI_VALIDATION.getName(), previewerId, tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getItems_previewerNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doReturn(Optional.empty()).when(mockedApiFactory).create(previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a previewer with the given ID."));
    }

    @Test
    public void getItems_tenantNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doThrow(new NullPointerException()).when(mockedApi).getTenantItems(Matchers.anyInt(), Matchers.any());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a tenant with the given ID."));
    }

    @Test
    public void getItems_withPermission_returnDataFromApi() throws Exception {
        // Given
        GetTenantItemsResult itemsResult = new GetTenantItemsResult(itemsJsonWithItems);
        Mockito.doReturn(itemsResult).when(mockedApi).getTenantItems(Matchers.anyInt(), Matchers.any());
        List<GetTenantItemsResult.Item> items = new ArrayList<>();

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        assertThat("Result JSON should contain total number of items",
                resultJson.get("data").get("totalResults").asInt(), equalTo(4272));
        JSONAssert.assertEquals("[\n" +
                "            {\n" +
                "                \"id\": 1,\n" +
                "                \"identifier\": \"item1\",\n" +
                "                \"numTestCases\": 7,\n" +
                "                \"fingerprinted\": true\n" +
                "            },\n" +
                "            {\n" +
                "                \"id\": 2,\n" +
                "                \"identifier\": \"item2\",\n" +
                "                \"numTestCases\": 0,\n" +
                "                \"fingerprinted\": false\n" +
                "            }\n" +
                "        ]\n",
                resultJson.get("data").get("items").toString(), true);
    }

    @Test
    public void getItems_noFilters_callApiWithoutFilters() {
        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(Map::isEmpty,
                "Param map should be empty"));
    }

    @Test
    public void getItems_includeStartIndex_callApiWithDisplayStart() {
        // Given
        requestBuilder.uri("?startIndex=10");

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(
                (m) -> m.get(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_START).equals("10"),
                "Param map should include the correct DISPLAY_START param"));
    }

    @Test
    public void getItems_sortColumnAsc_callApiWithOrderByClauseAsc() {
        // Given
        requestBuilder.uri("?sortColumn=id");

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(
                (m) -> m.get(GetTenantItemsResult.GetTenantItemsParam.ORDER_BY_CLAUSE).equals("id asc"),
                "Param map should include the correct ORDER_BY_CLAUSE param"));
    }

    @Test
    public void getItems_sortColumnDesc_callApiWithOrderByClauseDesc() {
        // Given
        requestBuilder.uri("?sortColumn=id&sortReversed=true");

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(
                (m) -> m.get(GetTenantItemsResult.GetTenantItemsParam.ORDER_BY_CLAUSE).equals("id desc"),
                "Param map should include the correct ORDER_BY_CLAUSE param"));
    }

    @Test
    public void getItems_includeSearch_callApiWithSearch() {
        // Given
        requestBuilder.uri("?search=query");

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(
                (m) -> m.get(GetTenantItemsResult.GetTenantItemsParam.SEARCH).equals("query"),
                "Param map should include the correct SEARCH param"));
    }

    @Test
    public void getItems_includeTestCaseFilter_callApiWithTestCaseFilter() {
        // Given
        requestBuilder.uri("?testCaseFilter=1");

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(
                (m) -> m.get(GetTenantItemsResult.GetTenantItemsParam.TESTCASE_FILTER).equals("1"),
                "Param map should include the correct TESTCASE_FILTER param"));
    }

    @Test
    public void getItems_includeFingerprintFilter_callApiWithFingerprintFilter() {
        // Given
        requestBuilder.uri("?fingerprintFilter=-1");

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(
                (m) -> m.get(GetTenantItemsResult.GetTenantItemsParam.FINGERPRINT_FILTER).equals("-1"),
                "Param map should include the correct FINGERPRINT_FILTER param"));
    }

    @Test
    public void getItems_includeNumberToShow_callApiWithDisplayLength() {
        // Given
        requestBuilder.uri("?numberToShow=50");

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItems("v1", previewerId, tenantId));

        // Then
        Mockito.verify(mockedApi).getTenantItems(Matchers.eq((int) tenantId), LambdaMatcher.argLambda(
                (m) -> m.get(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_LENGTH).equals("50"),
                "Param map should include the correct DISPLAY_LENGTH param"));
    }

    @Test
    public void getItem_noPermission_returnAccessDenied() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData)
                .hasTenantLevelPermission(itemsPermission, SecureController.AccessType.READ, SecureController.Module
                        .QTI_VALIDATION.getName(), previewerId, tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItem("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getItem_previewerNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doReturn(Optional.empty()).when(mockedApiFactory).create(previewerId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItem("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a previewer with the given ID."));
    }

    @Test
    public void getItem_tenantNotFound_returnNotFound() throws Exception {
        // Given
        Mockito.doThrow(new NullPointerException()).when(mockedApi).getTenantItems(Matchers.anyInt(), Matchers.any());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItem("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Result JSON should have the correct message",
                resultJson.get("errors").get(0).get("message").asText(),
                equalTo("Unable to find a tenant with the given ID."));
    }

    @Test
    public void getItem_itemNotFound_returnNotFound() throws Exception {
        // Given
        GetTenantItemsResult itemsResult = new GetTenantItemsResult(itemsJsonWithoutItems);
        Mockito.doReturn(itemsResult).when(mockedApi).getTenantItems(Matchers.anyInt(), Matchers.any());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItem("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be NOT_FOUND", result.status(), equalTo(Http.Status.NOT_FOUND));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false",
                resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getItem_hasPermissionAndItemFound_returnItem() throws Exception {
        // Given
        GetTenantItemsResult itemsResult = new GetTenantItemsResult(itemsJsonWithItems);
        Mockito.doReturn(itemsResult).when(mockedApi).getTenantItems(Matchers.anyInt(), Matchers.any());

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> itemController.getItem("v1", previewerId, tenantId, itemId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true",
                resultJson.get("success").asBoolean(), equalTo(true));
        JSONAssert.assertEquals("{\n" +
                        "            \"id\": 1,\n" +
                        "            \"identifier\": \"item1\",\n" +
                        "            \"numTestCases\": 7,\n" +
                        "            \"fingerprinted\": true\n" +
                        "        }",
                resultJson.get("data").get("item").toString(), true);
    }
}
