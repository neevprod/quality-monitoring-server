package controllers.qtivalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.SecureController;
import global.TestWithApplication;
import models.User;
import models.qtivalidation.TestCaseUpload;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import play.core.j.JavaContextComponents;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.qtivalidation.TestCaseUploadService;
import util.SessionData;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({ "javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*" })
public class TestCaseUploadControllerTest extends TestWithApplication {
    private String apiVersion;
    private int previewerId;
    private long tenantId;
    private String testCasesPermission;
    private Http.RequestBuilder requestBuilder;
    private SessionData mockedSessionData;
    private TestCaseUploadService mockedTestCaseUploadService;
    private TestCaseUploadHistoryController testCaseUploadController;

    @Mock
    private JavaContextComponents contextComponents;

    @Before
    public void setUp() throws Exception {
        previewerId = 3;
        tenantId = 2;
        apiVersion = "v1";
        testCasesPermission = "testCases";
        requestBuilder = new Http.RequestBuilder();

        mockedSessionData = Mockito.mock(SessionData.class);
        Mockito.doReturn(true).when(mockedSessionData).hasTenantLevelPermission(testCasesPermission,
                SecureController.AccessType.READ, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                tenantId);
        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(mockedSessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        mockedTestCaseUploadService = Mockito.mock(TestCaseUploadService.class);

        testCaseUploadController = Mockito
                .spy(new TestCaseUploadHistoryController(sessionDataFactory, mockedTestCaseUploadService));
    }

    @Test
    public void getTestCaseUploadHistory_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(mockedSessionData).hasTenantLevelPermission(testCasesPermission,
                SecureController.AccessType.READ, SecureController.Module.QTI_VALIDATION.getName(), previewerId,
                tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseUploadController.getTestCaseUploadHistory(apiVersion, previewerId, tenantId));

        // Then
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of false", resultJson.get("success").asBoolean(),
                equalTo(false));
    }

    @Test
    public void getTestCaseUploadHistory_withPermission_returnData() throws Exception {
        Timestamp time = Timestamp.from(Instant.now());

        // Given
        List<TestCaseUpload> uploads = new ArrayList<TestCaseUpload>();
        TestCaseUpload upload = new TestCaseUpload();
        User user = new User();
        user.setId(1L);
        user.setFullname("user1");
        user.setIsAdmin(true);
        user.setIsApiUser(false);

        upload.setPreviewerId(3);
        upload.setTenantId(2L);
        upload.setTestCaseUploadsId(1L);
        upload.setItemsCount(1);
        upload.setTestCasesCount(5);
        upload.setFailedTestCasesCount(1);
        upload.setMismatchedTestCasesCount(0);
        upload.setProcessedDate(time);
        upload.setUploadJsonResult("");
        upload.setUser(user);
        uploads.add(upload);

        Mockito.doReturn(uploads).when(mockedTestCaseUploadService).findAllByTenantId(previewerId, tenantId);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, contextComponents,
                () -> testCaseUploadController.getTestCaseUploadHistory("v1", previewerId, tenantId));

        // Then
        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Result JSON should have success value of true", resultJson.get("success").asBoolean(),
                equalTo(true));
        final String expected = "{\"testCaseUploadsId\":1,\"previewerId\":3,\"tenantId\":2,\"user\":{\"id\":1,"
                + "\"fullname\":\"user1\",\"isAdmin\":true},\"itemsCount\":1,\"testCasesCount\":5,"
                + "\"failedTestCasesCount\":1,\"mismatchedTestCasesCount\":0,\"processedDate\":" + time.getTime() + "}";
        JSONAssert.assertEquals(expected, resultJson.get("data").get("testCaseUploads").get(0).toString(), true);
    }
}
