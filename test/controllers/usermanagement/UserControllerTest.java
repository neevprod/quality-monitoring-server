package controllers.usermanagement;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.typesafe.config.Config;
import controllers.SecureController;
import global.TestWithApplication;
import models.HumanUser;
import models.UserPermissions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import play.cache.AsyncCacheApi;
import play.core.j.JavaContextComponents;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.PermissionSettingsValueService;
import services.qtivalidation.PreviewerService;
import services.qtivalidation.PreviewerTenantService;
import services.systemvalidation.EnvironmentService;
import services.usermanagement.*;
import util.ActiveDirectoryAuthenticator;
import util.SessionData;

import javax.inject.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by VJOSHNA on 12/1/2016.
 */
public class UserControllerTest extends TestWithApplication {
    private SessionData sessionData;
    private HumanUserService humanUserService;
    private PermissionSettingsService permissionSettingsService;

    private EnvironmentService environmentService;
    private PreviewerService previewerService;
    private PreviewerTenantService tenantService;
    private UserPermissionsService userPermissionsService;
    private ApplicationModulesService applicationModulesService;
    private PermissionSettingsValueService permissionSettingsValueService;
    private PermissionOptionService permissionOptionService;
    private AsyncCacheApi cache;
    private Config config;

    private UserController userController;
    private Http.RequestBuilder requestBuilder;

    private ActiveDirectoryAuthenticator mockedAdAuth;
    private Provider<ActiveDirectoryAuthenticator> mockedAdAuthProvider;
    @Mock
    JavaContextComponents mMockContext;
    @Before
    public void setUp() {
        sessionData = Mockito.mock(SessionData.class);
        SessionData.Factory sessionDataFactory = Mockito.mock(SessionData.Factory.class);
        Mockito.doReturn(sessionData).when(sessionDataFactory).create(Matchers.any(JsonNode.class));

        humanUserService = Mockito.mock(HumanUserService.class);
        permissionSettingsService = Mockito.mock(PermissionSettingsService.class);
        environmentService = Mockito.mock(EnvironmentService.class);
        previewerService = Mockito.mock(PreviewerService.class);
        tenantService = Mockito.mock(PreviewerTenantService.class);
        userPermissionsService = Mockito.mock(UserPermissionsService.class);
        applicationModulesService = Mockito.mock(ApplicationModulesService.class);
        permissionSettingsValueService = Mockito.mock(PermissionSettingsValueService.class);
        permissionOptionService = Mockito.mock(PermissionOptionService.class);
        config = Mockito.mock(Config.class);
        cache = Mockito.mock(AsyncCacheApi.class);
        mockedAdAuthProvider = Mockito.mock(Provider.class);

        requestBuilder = new Http.RequestBuilder();

        mockedAdAuth = Mockito.mock(ActiveDirectoryAuthenticator.class);
        Mockito.when(mockedAdAuthProvider.get()).thenReturn(mockedAdAuth);

        userController = Mockito.spy(new UserController(sessionDataFactory, humanUserService, permissionSettingsService,
                environmentService, previewerService, tenantService, userPermissionsService, applicationModulesService,
                permissionSettingsValueService, permissionOptionService, config, cache, mockedAdAuthProvider));
    }

    @Test
    public void getUsers_hasPermission_returnUsers() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData).hasModuleLevelPermission("user", SecureController.AccessType.READ,
                SecureController.Module.USER_MANAGEMENT.getName());
        Mockito.doReturn(getFakeHumanUsers(5)).when(humanUserService).findAll();

        // When
        Result result = Helpers.invokeWithContext(requestBuilder,mMockContext, () -> {
            Http.Context.current().args.put("users", Json.newObject());
            return userController.getUsers("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));

        JSONAssert.assertEquals(Json.toJson(getFakeHumanUsers(5)).toString(),
                resultJson.get("data").get("users").toString(), true);
    }

    @Test
    public void getUsers_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData).hasModuleLevelPermission("user", SecureController.AccessType.READ,
                SecureController.Module.USER_MANAGEMENT.getName());
        Mockito.doReturn(getFakeHumanUsers(2)).when(humanUserService).findAll();

        // When
        Result result = Helpers.invokeWithContext(requestBuilder,mMockContext, () -> {
            Http.Context.current().args.put("users", Json.newObject());
            return userController.getUsers("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));
    }

    @Test
    public void getHumanUser_hasPermission_returnUser() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData).hasModuleLevelPermission("user", SecureController.AccessType.READ,
                SecureController.Module.USER_MANAGEMENT.getName());
        Mockito.doReturn(getFakeHumanUser()).when(humanUserService).findByHumanUserId(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder, mMockContext,() -> {
            Http.Context.current().args.put("humanUser", Json.newObject());
            return userController.getHumanUser("v1", 1);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));

        JSONAssert.assertEquals(Json.toJson(getFakeHumanUser().get()).toString(),
                resultJson.get("data").get("humanUser").toString(), true);

    }

    @Test
    public void getHumanUser_noPermission_returnAccessDeniedResult() throws Exception {
        // Given
        Mockito.doReturn(false).when(sessionData).hasModuleLevelPermission("user", SecureController.AccessType.READ,
                SecureController.Module.USER_MANAGEMENT.getName());
        Mockito.doReturn(getFakeHumanUser()).when(humanUserService).findByHumanUserId(1);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder,mMockContext, () -> {
            Http.Context.current().args.put("humanUser", Json.newObject());
            return userController.getHumanUser("v1", 1);
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Status of result should be FORBIDDEN", result.status(), equalTo(Http.Status.FORBIDDEN));
        assertThat("Value of success in result should be false", resultJson.get("success").asBoolean(), equalTo(false));

    }

    @Test
    public void addUser_hasPermission_returnNewUser() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData).hasModuleLevelPermission("user", SecureController.AccessType.CREATE,
                SecureController.Module.USER_MANAGEMENT.getName());
        Mockito.doReturn(Optional.empty()).when(humanUserService).findByEmail("nand.joshi@pearson.com");

        Mockito.doReturn(getFakeHumanUser().get()).when(mockedAdAuth).findUser(null, null, "nand.joshi@pearson.com");

        ObjectNode requestJson = Json.newObject();
        requestJson.put("email", "nand.joshi@pearson.com");
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder().bodyJson(requestJson);
        // When
        Result result = Helpers.invokeWithContext(requestBuilder,mMockContext, () -> {
            Http.Context.current().args.put("humanUser", Json.newObject());
            return userController.addUser("v1");
        });

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);

        assertThat("Status of result should be OK", result.status(), equalTo(Http.Status.OK));
        assertThat("Value of success in result should be true", resultJson.get("success").asBoolean(), equalTo(true));

        JSONAssert.assertEquals(Json.toJson(getFakeHumanUser().get()).toString(),
                resultJson.get("data").get("user").toString(), true);

    }

    @Test
    public void deleteUserPermission_hasPermission() throws Exception {
        // Given
        Mockito.doReturn(true).when(sessionData).hasModuleLevelPermission("user", SecureController.AccessType.DELETE,
                SecureController.Module.USER_MANAGEMENT.getName());

        Optional<UserPermissions> mockedUserPermissions = Optional.of(Mockito.mock(UserPermissions.class));
        Mockito.doReturn(mockedUserPermissions).when(userPermissionsService).findByUserPermissionId(1l);

        // When
        Result result = Helpers.invokeWithContext(requestBuilder,mMockContext, () -> {
            Http.Context.current().args.put("humanUser", Json.newObject());
            return userController.deleteUserPermission("v1", 1l);
        });

        // Then
        Mockito.verify(userPermissionsService).delete(mockedUserPermissions.get());
        assertThat("Result should have an OK status", result.status(), equalTo(Http.Status.OK));

    }

    private List<HumanUser> getFakeHumanUsers(int numberOfUsers) {
        List<HumanUser> users = new ArrayList<>();
        for (long i = 0; i < numberOfUsers; i++) {
            HumanUser user = new HumanUser();
            user.setHumanUserId(i);
            users.add(user);
        }
        return users;
    }

    private Optional<HumanUser> getFakeHumanUser() {
        String userJson = "{ \"humanUserId\": 2, \"userId\": 2, \"email\": \"nand.joshi@pearson.com\", \"networkId\": "
                + "\"vjoshna\", \"user\": { \"id\": 2, \"fullname\": \"Nand Joshi\", \"isAdmin\": false } }";

        Gson gson = new Gson();
        HumanUser user = gson.fromJson(userJson, HumanUser.class);
        return Optional.of(user);
    }
}
