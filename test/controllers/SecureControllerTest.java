package controllers;

import global.exceptions.QaApplicationPermissionException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import util.SessionData;

public class SecureControllerTest {

    private class SecureControllerImpl extends SecureController {
        SecureControllerImpl(SessionData.Factory sessionDataFactory) {
            super(sessionDataFactory);
        }
    }

    private SecureControllerImpl secureController;
    private SessionData mockSessionData;

    @Before
    public void initialize() {
        secureController = Mockito.spy(new SecureControllerImpl(null));

        mockSessionData = Mockito.mock(SessionData.class);
        Mockito.doReturn(mockSessionData).when(secureController).sessionData();
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void enforceModuleLevelPermission_userHasPermission_noExceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasModuleLevelPermission("feature1", SecureController.AccessType.READ,
                "module1")).thenReturn(true);

        // When
        secureController.enforceModuleLevelPermission("feature1", SecureController.AccessType.READ, "module1");
    }

    @Test
    public void enforceModuleLevelPermission_userDoesNotHavePermission_exceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasModuleLevelPermission("feature1", SecureController.AccessType.READ,
                "module1")).thenReturn(false);

        // Then
        exception.expect(QaApplicationPermissionException.class);

        // When
        secureController.enforceModuleLevelPermission("feature1", SecureController.AccessType.READ, "module1");
    }

    @Test
    public void enforceTenantLevelPermission_userHasPermission_noExceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                "module1", 1, 2L)).thenReturn(true);

        // When
        secureController.enforceTenantLevelPermission("feature1", SecureController.AccessType.READ, "module1", 1, 2L);
    }

    @Test
    public void enforceTenantLevelPermission_userDoesNotHavePermission_exceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasTenantLevelPermission("feature1", SecureController.AccessType.READ,
                "module1", 1, 2L)).thenReturn(false);

        // Then
        exception.expect(QaApplicationPermissionException.class);

        // When
        secureController.enforceTenantLevelPermission("feature1", SecureController.AccessType.READ, "module1", 1, 2L);
    }

    @Test
    public void enforceTenantLevelPermission_noTenantGivenAndUserHasPermission_noExceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                "module1", 1)).thenReturn(true);

        // When
        secureController.enforceTenantLevelPermission("feature1", SecureController.AccessType.READ, "module1", 1);
    }

    @Test
    public void enforceTenantLevelPermission_noTenantGivenAndUserDoesNotHavePermission_exceptionThrown() throws
            Exception {
        // Given
        Mockito.when(mockSessionData.hasAnyTenantLevelPermission("feature1", SecureController.AccessType.READ,
                "module1", 1)).thenReturn(false);

        // Then
        exception.expect(QaApplicationPermissionException.class);

        // When
        secureController.enforceTenantLevelPermission("feature1", SecureController.AccessType.READ, "module1", 1);
    }

    @Test
    public void enforceEnvironmentLevelPermission_userHasPermission_noExceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                "module1", 1)).thenReturn(true);

        // When
        secureController.enforceEnvironmentLevelPermission("feature1", SecureController.AccessType.READ, "module1", 1);
    }

    @Test
    public void enforceEnvironmentLevelPermission_userDoesNotHavePermission_exceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                "module1", 1)).thenReturn(false);

        // Then
        exception.expect(QaApplicationPermissionException.class);

        // When
        secureController.enforceEnvironmentLevelPermission("feature1", SecureController.AccessType.READ, "module1", 1);
    }

    @Test
    public void enforceEnvironmentLevelPermission_noEnvironmentGivenAndUserHasPermission_noExceptionThrown() throws Exception {
        // Given
        Mockito.when(mockSessionData.hasAnyEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                "module1")).thenReturn(true);

        // When
        secureController.enforceEnvironmentLevelPermission("feature1", SecureController.AccessType.READ, "module1");
    }

    @Test
    public void enforceEnvironmentLevelPermission_noEnvironmentGivenAndUserDoesNotHavePermission_exceptionThrown() throws
            Exception {
        // Given
        Mockito.when(mockSessionData.hasAnyEnvironmentLevelPermission("feature1", SecureController.AccessType.READ,
                "module1")).thenReturn(false);

        // Then
        exception.expect(QaApplicationPermissionException.class);

        // When
        secureController.enforceEnvironmentLevelPermission("feature1", SecureController.AccessType.READ, "module1");
    }

}
