package global;

import com.typesafe.config.ConfigFactory;
import org.junit.Before;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.Helpers;

public class TestWithApplication {
    protected static Application app;

    @Before
    public void startApplicationIfNecessary() {
        if (app == null) {
            app = new GuiceApplicationBuilder()
                    .loadConfig(ConfigFactory.load(ConfigFactory.load("application-test.conf")))
                    .build();
            Helpers.start(app);
        }
    }

}
