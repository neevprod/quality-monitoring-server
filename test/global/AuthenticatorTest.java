package global;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.core.j.JavaContextComponents;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;

import java.time.Clock;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JWTVerifier.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class AuthenticatorTest extends TestWithApplication {
    private Clock clock;
    @Mock
    private JavaContextComponents contextComponents;

    private String getToken(String userName, long issuedAtMillis) {
        Map<String, Object> claims = new HashMap<>();
        String jwtId = UUID.randomUUID().toString();
        long currentTimeSeconds = issuedAtMillis / 1000L;
        claims.put("jti", jwtId);
        claims.put("iat", currentTimeSeconds);
        claims.put("exp", currentTimeSeconds + app.config().getDuration("session.timeout").getSeconds());
        claims.put("userName", userName);

        JWTSigner jwtSigner = new JWTSigner(app.config().getString("play.http.secret.key"));
        return jwtSigner.sign(claims);
    }

    private Map<String, Object> getClaims(String token) throws Exception {
        JWTVerifier jwtVerifier = new JWTVerifier(app.config().getString("play.http.secret.key"));
        return jwtVerifier.verify(token);
    }

    @Before
    public void setUp() {
        clock = app.injector().instanceOf(Clock.class);

        // Unfortunately, we have to do this because the JWT library doesn't allow you to provide an evaluation time.
        PowerMockito.mockStatic(System.class);
        PowerMockito.when(System.currentTimeMillis()).thenReturn(clock.millis());
    }

    @Test
    public void getUsername_validTokenWithoutCookie_returnUserName() throws Exception {
        // Given
        String userName = "testUser";
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", getToken(userName, clock.millis()));
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        assertThat("Username should be returned", result, equalTo(userName));
    }

    @Test
    public void getUsername_validTokenWithoutCookie_putSessionInfoInContext() throws Exception {
        // Given
        String userName = "testUser";
        String token = getToken(userName, clock.millis());
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", token);
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        JsonNode sessionJson = (JsonNode) context.args.get("session");
        Map<String, Object> jwtClaims = getClaims(token);
        assertThat("Session ID should be in context", sessionJson.get("jti").asText(), equalTo(jwtClaims.get("jti")));
        assertThat("Expiration time should be in context", sessionJson.get("exp").asLong(),
                equalTo(((Number) jwtClaims.get("exp")).longValue()));
        assertThat("Issued at time should be in context", sessionJson.get("iat").asLong(),
                equalTo(((Number) jwtClaims.get("iat")).longValue()));
        assertThat("Username should be in context", sessionJson.get("userName").asText(),
                equalTo(jwtClaims.get("userName")));
    }

    @Test
    public void getUsername_validTokenWithoutCookie_cookieNotSet() throws Exception {
        // Given
        String userName = "testUser";
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", getToken(userName, clock.millis()));
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        assertThat("No cookies should not be set", context.response().cookies().iterator().hasNext(), equalTo(false));
    }

    @Test
    public void getUsername_validTokenWithCookie_returnUserName() throws Exception {
        // Given
        String userName = "testUser";
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", getToken(userName, clock.millis()))
                .cookie(Http.Cookie.builder(app.config().getString("session.cookie.name"), "asdf")
                        .withPath("/").withSecure(false).withHttpOnly(false).build());
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        assertThat("Username should be returned", result, equalTo(userName));
    }

    @Test
    public void getUsername_validTokenWithCookie_putSessionInfoInContext() throws Exception {
        // Given
        String userName = "testUser";
        String token = getToken(userName, clock.millis());
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", token)
                .cookie(Http.Cookie.builder(app.config().getString("session.cookie.name"), "asdf")
                        .withPath("/").withSecure(false).withHttpOnly(false).build());
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        JsonNode sessionJson = (JsonNode) context.args.get("session");
        Map<String, Object> jwtClaims = getClaims(token);
        assertThat("Session ID should be in context", sessionJson.get("jti").asText(), equalTo(jwtClaims.get("jti")));
        assertThat("Expiration time should be in context", sessionJson.get("exp").asLong(),
                equalTo(((Number) jwtClaims.get("exp")).longValue()));
        assertThat("Issued at time should be in context", sessionJson.get("iat").asLong(),
                equalTo(((Number) jwtClaims.get("iat")).longValue()));
        assertThat("Username should be in context", sessionJson.get("userName").asText(),
                equalTo(jwtClaims.get("userName")));
    }

    @Test
    public void getUsername_validTokenWithCookie_updateCookie() throws Exception {
        // Given
        String userName = "testUser";
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", getToken(userName, clock.millis() - 5000))
                .cookie(Http.Cookie.builder(app.config().getString("session.cookie.name"), "asdf")
                        .withPath("/").withSecure(false).withHttpOnly(false).build());

        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        Iterator<Http.Cookie> iterator = context.response().cookies().iterator();
        Http.Cookie sessionCookie = null;
        while (iterator.hasNext()) {
            Http.Cookie cookie = iterator.next();
            if (cookie.name().equals(app.config().getString("session.cookie.name"))) {
                sessionCookie = cookie;
            }
        }
        assertThat("Session cookie should be in response", sessionCookie, notNullValue());

        Map<String, Object> claims = getClaims(sessionCookie.value());
        assertThat("Token in cookie should contain correct username", claims.get("userName"),
                equalTo(userName));
        assertThat("Token in cookie should contain correct issued at time", ((Number) claims.get("iat")).longValue(),
                equalTo(clock.millis() / 1000L));
        assertThat("Token in cookie should contain correct expiration time", ((Number) claims.get("exp")).longValue(),
                equalTo(clock.millis() / 1000L + app.config().getDuration("session.timeout").getSeconds()));
    }

    @Test
    public void getUsername_tokenExpired_returnNull() throws Exception {
        // Given
        String userName = "testUser";
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", getToken(userName, clock.millis() - 11000));
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        assertThat("Username should be null", result, equalTo(null));
    }

    @Test
    public void getUsername_noToken_returnNull() throws Exception {
        // Given
        String userName = "testUser";
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder();
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        assertThat("Username should be null", result, equalTo(null));
    }

    @Test
    public void getUsername_invalidToken_returnNull() throws Exception {
        // Given
        String userName = "testUser";
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder()
                .header("X-Auth-Token", getToken(userName, clock.millis()).concat("a"));
        Http.Context context = new Http.Context(requestBuilder, contextComponents);

        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        String result = authenticator.getUsername(context);

        // Then
        assertThat("Username should be null", result, equalTo(null));
    }

    @Test
    public void onUnauthorized_returnProperResponse() throws Exception {
        // Given
        Http.RequestBuilder requestBuilder = new Http.RequestBuilder();
        Http.Context context = new Http.Context(requestBuilder, contextComponents);
        Authenticator authenticator = app.injector().instanceOf(Authenticator.class);

        // When
        Result result = authenticator.onUnauthorized(context);

        // Then
        JsonNode resultJson = new ObjectMapper().readValue(Helpers.contentAsString(result), JsonNode.class);
        assertThat("Response status should be unauthorized", result.status(), equalTo(Http.Status.UNAUTHORIZED));
        assertThat("Response should contain success value of false", resultJson.get("success").asBoolean(), equalTo(false));
        assertThat("Response should contain the proper error message", resultJson.get("errors").get(0)
                .get("message").asText(), equalTo("Access not authorized. No valid session."));
    }
}