package services;

import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.operation.Operation;

public class CommonDbOperations {

    public static final Operation DELETE_ALL = Operations.deleteAllFrom("user_permissions", "permission_settings_value",
            "permission_settings", "permission_option", "user_tenant_job_subscription", "job_type", "test_case_uploads",
            "status", "api_user", "human_user", "hist_test_case", "hist_item", "hist_item_exception",
            "hist_tenant_exception", "hist_previewer_exception", "hist_tenant", "hist_exec", "hist_status",
            "previewer_tenant_item_test_cases", "previewer_tenant_item_fingerprint", "previewer_tenant_item_xml",
            "previewer_tenant_item", "previewer_tenant", "previewer", "application_module", "permission_type",
            "student_scenario_result_comment", "student_scenario_result_comment_category", "external_defect_repository",
            "student_scenario_result", "student_scenario", "scenario", "scenario_type", "scenario_class",
            "testmap_detail", "test_session", "div_job_exec", "div_job_exec_type", "path_state", "state", "path",
            "environment", "db_connection", "api_connection", "div_job_exec_status", "user", "student_scenario_status",
            "application_setting", "epen_user_reservation", "test_type", "kt_test_case");
}