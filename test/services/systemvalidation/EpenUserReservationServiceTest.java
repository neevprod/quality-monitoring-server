package services.systemvalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.systemvalidation.EpenUserReservation;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by SELIGE on 6/8/2017.
 */
public class EpenUserReservationServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private EpenUserReservationService epenUserReservationService;
    private Timestamp timestamp;
    private Timestamp expiredLockTimestamp;

    @Before
    public void setUp() throws Exception {
        timestamp = Timestamp.from(Instant.now());
        expiredLockTimestamp = Timestamp.from(Instant.now().minus(1500, ChronoUnit.MINUTES));

        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,
                Operations.insertInto("epen_user_reservation").columns("id", "locked", "locked_timestamp")
                        .values(1, 1, timestamp).values(2, 1, expiredLockTimestamp).values(3, 0, timestamp).build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        epenUserReservationService = app.injector().instanceOf(EpenUserReservationService.class);

    }

    @Test
    public void find_next_available_returnIdForUserWithExpiredLock() {

        Optional<EpenUserReservation> epenUserReservation = jpaApi
                .withTransaction(() -> epenUserReservationService.findNextAvailable());

        assertThat("The returned id should match the first record with an expired lock",
                epenUserReservation.get().getId(), equalTo(2));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_next_available_returnIdForUserWithoutLock() {

        Optional<EpenUserReservation> epenUserReservation=jpaApi.withTransaction(() -> epenUserReservationService.findNextAvailable());

        epenUserReservation = jpaApi.withTransaction(() -> epenUserReservationService.findNextAvailable());

        assertThat("The returned id should match the first record which is not locked",
                epenUserReservation.get().getId(), equalTo(3));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_next_available_returnIdForUnlockedUser(){

        jpaApi.withTransaction(() -> epenUserReservationService.unlock(1));

        Optional<EpenUserReservation> epenUserReservation = jpaApi
                .withTransaction(() -> epenUserReservationService.findNextAvailable());

        assertThat("The returned id should match the record that was unlocked", epenUserReservation.get().getId(),
                equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_next_available_returnNewIdWhenAllAreLocked() {

        Optional<EpenUserReservation> epenUserReservation = jpaApi
                .withTransaction(() -> epenUserReservationService.findNextAvailable());

        epenUserReservation = jpaApi.withTransaction(() -> epenUserReservationService.findNextAvailable());

        epenUserReservation = jpaApi.withTransaction(() -> epenUserReservationService.findNextAvailable());

        assertThat("The returned id should match a newly created user record", epenUserReservation.get().getId(),
                equalTo(4));

        dbSetupTracker.skipNextLaunch();
    }

}