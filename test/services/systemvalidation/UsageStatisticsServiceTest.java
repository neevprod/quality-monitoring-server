package services.systemvalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Tarun Gupta
 */
public class UsageStatisticsServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private UsageStatisticsService usageStatisticsService;
    private Timestamp timestamp;

    @Before
    public void setUp() {
        timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,

                Operations.insertInto("div_job_exec_status").columns("div_job_exec_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("user").columns("id", "fullname", "isAdmin").values(1, "Tester Testington", 0)
                        .build(),
                Operations.insertInto("api_connection").columns("api_connection_id", "token", "secret", "api_url")
                        .values(1, "fe_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://fe_api_connection_url")
                        .values(2, "be_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://be_api_connection_url")
                        .values(3, "epen2_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://epen2_api_connection_url")
                        .values(4, "iris_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://iris_api_connection_url")
                        .values(5, "ice_bridge_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://ice_bridge_api_connection_url")
                        .values(6, "data_warehouse_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://data_warehouse_api_connection_url")
                        .build(),
                Operations.insertInto("db_connection")
                        .columns("db_connection_id", "host", "port", "db_name", "db_user", "db_pass")
                        .values(1, "fe_mysql_connection_host", 3306, "fe_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(2, "be_mysql_connection_host", 3306, "be_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(3, "epen2_mysql_connection_host", 3306, "epen2_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(4, "iris_mysql_connection_host", 3306, "iris_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(5, "ice_bridge_oracle_connection_host", 3306, "ice_bridge_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(6, "data_warehouse_mongo_connection_host", 27017, "data_warehouse_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(7, "wf_db_connection_host", 3306, "wf_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(8, "epen2_mongo_connection_id", 27017, "epen2_mongo_connection_id", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .build(),
                Operations.insertInto("environment")
                        .columns("environment_id", "name", "fe_url", "epen_url", "fe_mysql_connection_id",
                                "be_mysql_connection_id", "epen2_mysql_connection_id", "iris_mysql_connection_id",
                                "ice_bridge_oracle_connection_id", "data_warehouse_mongo_connection_id",
                                "fe_api_connection_id", "be_api_connection_id", "epen2_api_connection_id",
                                "iris_api_connection_id", "ice_bridge_api_connection_id",
                                "data_warehouse_api_connection_id", "epen2_mongo_connection_id")
                        .values(1, "TestEnv", "http://pan-tst-env.pearsondev.com/",
                                "https://epen2-int-ref-3-0.pearsondev.com", 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 8)
                        .build(),
                Operations.insertInto("path").columns("path_id", "path_name", "path_description")
                        .values(1, "DefaultPath", "This is the default path").build(),
                Operations.insertInto("state").columns("state_id", "state_name", "state_description", "auto_retries")
                        .values(1, "FE_STUDENT_SESSION_DETAIL_FETCHER", "state_description", 0)
                        .values(2, "TEST_SCENARIO_CREATOR", "state_description", 0)
                        .values(3, "TN8_API_DATA_LOADER", "state_description", 0)
                        .values(4, "IRIS_PROCESSING_VALIDATOR", "state_description", 0)
                        .values(5, "BE_PATH_PROCESSING_VALIDATOR", "state_description", 0)
                        .values(6, "EPEN2_SCORING_SCENARIO_CREATOR", "state_description", 0)
                        .values(7, "EPEN2_BULK_SCORER", "state_description", 0)
                        .values(8, "EXPECTED_SCORE_GENERATOR", "state_description", 0)
                        .values(9, "DATAWAREHOUSE_EXTRACTOR", "state_description", 0)
                        .values(10, "EXPECTED_TO_ACTUAL_SCORE_COMPARER", "state_description", 0).build(),
                Operations.insertInto("path_state").columns("path_state_id", "path_id", "state_id").values(1, 1, 1)
                        .values(2, 1, 2).values(3, 1, 3).values(4, 1, 4).values(5, 1, 5).values(6, 1, 6).values(7, 1, 7)
                        .values(8, 1, 8).values(9, 1, 9).values(10, 1, 10).build(),
                Operations.insertInto("div_job_exec_type").columns("div_job_exec_type_id", "div_job_exec_type")
                        .values(1, "PANEXT").build(),
                Operations.insertInto("test_type").columns("test_type_id", "name")
                        .values(1, "Standard").values(2, "Battery").build(),
                Operations.insertInto("div_job_exec")
                        .columns("div_job_exec_id", "environment_id", "path_id", "completed", "submit_time_stamp",
                                "complete_time_stamp", "submit_user_id", "priority", "div_job_exec_status_id",
                                "div_job_exec_type_id", "scope_tree_path", "test_type_id")
                        .values(1, 1, 1, 0, timestamp, timestamp, 1, 1, 1, 1, "/ref/2015-16/refspr16", 1)
                        .values(2, 1, 1, 0, timestamp, timestamp, 1, 1, 2, 1, "/ref/2015-16/refspr16", 1).build(),
                Operations.insertInto("scenario_type").columns("scenario_type_id", "scenario_type").values(1, "TN8")
                        .values(2, "EPEN2").build(),
                Operations.insertInto("scenario_class").columns("scenario_class_id", "scenario_class")
                        .values(1, "minPoints").values(2, "maxPoints").build(),
                Operations.insertInto("testmap_detail")
                        .columns("testmap_detail_id", "testmap_id", "testmap_name", "testmap_publish_format",
                                "testmap_version")
                        .values(1, 1234, "TestMap1", "E", 1).build(),
                Operations.insertInto("scenario")
                        .columns("scenario_id", "scenario_type_id", "scenario", "scenario_name", "test_Code",
                                "form_code", "scenario_class_id", "testmap_detail_id")
                        .values(1, 1, "7468697320697320612074657374", "minPoints1", "tc1", "fc1", 1, 1)
                        .values(2, 2, "7468697320697320612074657374", "maxPoints1", "tc1", "fc1", 2, 1).build(),
                Operations.insertInto("test_session")
                        .columns("test_session_id", "testnav_application_url", "testnav_webservice_url",
                                "testnav_client_secret", "testnav_client_identifier", "testnav_version",
                                "testnav_customer_code", "scope_code", "session_seal_codes", "session_name",
                                "pan_test_session_id", "environment_id")
                        .values(1, "http://www.testnav.com", "http://www.testnav-api.com", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "identifier", "8.2", "pcc", "standard", "sc1,sc2", "testSession1", "1", 1)
                        .build(),
                Operations.insertInto("student_scenario_status").columns("student_scenario_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("student_scenario")
                        .columns("student_scenario_id", "uuid", "testnav_login_name", "testnav_login_password",
                                "wf_db_connection_id", "div_job_exec_id", "testnav_scenario_id", "epen_scenario_id",
                                "last_completed_state_id", "student_scenario_status_id", "test_session_id")
                        .values(1, "uuid1", "tnLogin1", "tnPass1", 7, 1, 1, 2, 6, 4, 1)
                        .values(2, "uuid2", "tnLogin2", "tnPass2", 7, 1, 1, 2, 6, 4, 1)
                        .values(3, "uuid3", "tnLogin3", "tnPass3", 7, 1, 1, 2, 6, 3, 1)
                        .values(4, "uuid4", "tnLogin4", "tnPass4", 7, 1, 1, 2, 6, 3, 1).build(),
                Operations.insertInto("student_scenario_result")
                        .columns("student_scenario_result_id", "student_scenario_id", "state_id", "result_details",
                                "timestamp", "success", "version")
                        .values(1, 1, 6, "contentKey", timestamp, 1, 1).values(2, 2, 6, "contentKey", timestamp, 0, 1)
                        .values(3, 2, 6, "contentKey", timestamp, 1, 2).values(4, 3, 6, "contentKey", timestamp, 0, 1)
                        .values(5, 4, 6, "contentKey", timestamp, 0, 1).values(6, 4, 6, "contentKey", timestamp, 0, 2)
                        .build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        usageStatisticsService = app.injector().instanceOf(UsageStatisticsService.class);
    }

    @Test
    public void findAllTestAttempts_singleEnvironment_returnAllTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        Integer count = jpaApi.withTransaction(
                () -> usageStatisticsService.findAllTestAttempts(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 4", count, equalTo(4));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllTestAttempts_allEnvironment_returnAllTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        Integer count = jpaApi.withTransaction(
                () -> usageStatisticsService.findAllTestAttempts(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 4", count, equalTo(4));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllUniqueUsers_singleEnvironment_returnAllUniqueUsers() throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        Integer count = jpaApi.withTransaction(
                () -> usageStatisticsService.findAllUniqueUsers(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllUniqueUsers_allEnvironment_returnAllUniqueUsers() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        Integer count = jpaApi.withTransaction(
                () -> usageStatisticsService.findAllUniqueUsers(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllUniqueForms_singleEnvironment_returnAllUniqueForms() throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        Integer count = jpaApi.withTransaction(
                () -> usageStatisticsService.findAllUniqueForms(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllUniqueForms_allEnvironment_returnAllUniqueForms() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        Integer count = jpaApi.withTransaction(
                () -> usageStatisticsService.findAllUniqueForms(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findFailedTestAttemptsForState_singleEnvironment_returnFailedTestAttemptsForState() throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        java.util.List<Object[]> list = jpaApi.withTransaction(() -> usageStatisticsService
                .findFailedTestAttemptsForState(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", list.size(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findFailedTestAttemptsForState_allEnvironment_returnFailedTestAttemptsForState() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        java.util.List<Object[]> list = jpaApi.withTransaction(() -> usageStatisticsService
                .findFailedTestAttemptsForState(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", list.size(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findTestAttemptsForForm_singleEnvironment_returnTestAttemptsForForm() throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        java.util.List<Object[]> list = jpaApi.withTransaction(() -> usageStatisticsService
                .findTestAttemptsForForm(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", list.size(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findTestAttemptsForForm_allEnvironment_returnTestAttemptsForForm() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        java.util.List<Object[]> list = jpaApi.withTransaction(() -> usageStatisticsService
                .findTestAttemptsForForm(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", list.size(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllScopeTreePath_singleEnvironment_returnAllScopeTreePath() throws Throwable {
        // Given
        Integer environmentId = 1;

        // When
        java.util.List<Object[]> list = jpaApi
                .withTransaction(() -> usageStatisticsService.findAllScopeTreePath(environmentId));

        // Then
        assertThat("The returned list should have a length of 1", list.size(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllScopeTreePath_allEnvironment_returnAllScopeTreePath() throws Throwable {
        // Given
        Integer environmentId = 0;

        // When
        java.util.List<Object[]> list = jpaApi
                .withTransaction(() -> usageStatisticsService.findAllScopeTreePath(environmentId));

        // Then
        assertThat("The returned list should have a length of 1", list.size(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllPassedTestAttempts_singleEnvironment_returnAllPassedTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService.findAllPassedTestAttempts(environmentId,
                scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 2", count, equalTo(2));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllPassedTestAttempts_allEnvironment_returnAllPassedTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService.findAllPassedTestAttempts(environmentId,
                scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 2", count, equalTo(2));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllFailedResumedTestAttempts_singleEnvironment_returnAllFailedResumedTestAttempts()
            throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService
                .findAllFailedResumedTestAttempts(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllFailedResumedTestAttempts_allEnvironment_returnAllFailedResumedTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService
                .findAllFailedResumedTestAttempts(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllPassedResumedTestAttempts_singleEnvironment_returnAllPassedResumedTestAttempts()
            throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService
                .findAllPassedResumedTestAttempts(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllPassedResumedTestAttempts_allEnvironment_returnAllPassedResumedTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService
                .findAllPassedResumedTestAttempts(environmentId, scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 1", count, equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllFailedTestAttempts_singleEnvironment_returnAllFailedTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 1;
        String scopeTreePath = "/ref/2015-16/refspr16";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService.findAllFailedTestAttempts(environmentId,
                scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 2", count, equalTo(2));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllFailedTestAttempts_allEnvironment_returnAllFailedTestAttempts() throws Throwable {
        // Given
        Integer environmentId = 0;
        String scopeTreePath = "All";

        // When
        Integer count = jpaApi.withTransaction(() -> usageStatisticsService.findAllFailedTestAttempts(environmentId,
                scopeTreePath, timestamp, timestamp));

        // Then
        assertThat("The returned list should have a length of 2", count, equalTo(2));

        dbSetupTracker.skipNextLaunch();
    }
}
