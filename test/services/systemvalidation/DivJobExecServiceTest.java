package services.systemvalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.systemvalidation.DivJobExec;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Gert Selis
 */
public class DivJobExecServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private DivJobExecService divJobExecService;
    private Timestamp timestamp;

    @Before
    public void setUp() {
        timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,

                Operations.insertInto("div_job_exec_status").columns("div_job_exec_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("user").columns("id", "fullname", "isAdmin").values(1, "Tester Testington", 0)
                        .build(),
                Operations.insertInto("api_connection").columns("api_connection_id", "token", "secret", "api_url")
                        .values(1, "fe_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://fe_api_connection_url")
                        .values(2, "be_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://be_api_connection_url")
                        .values(3, "epen2_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://epen2_api_connection_url")
                        .values(4, "iris_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://iris_api_connection_url")
                        .values(5, "ice_bridge_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://ice_bridge_api_connection_url")
                        .values(6, "data_warehouse_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://data_warehouse_api_connection_url")
                        .build(),
                Operations.insertInto("db_connection")
                        .columns("db_connection_id", "host", "port", "db_name", "db_user", "db_pass")
                        .values(1, "fe_mysql_connection_host", 3306, "fe_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(2, "be_mysql_connection_host", 3306, "be_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(3, "epen2_mysql_connection_host", 3306, "epen2_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(4, "iris_mysql_connection_host", 3306, "iris_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(5, "ice_bridge_oracle_connection_host", 3306, "ice_bridge_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(6, "data_warehouse_mongo_connection_host", 27017, "data_warehouse_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(7, "wf_db_connection_host", 3306, "wf_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(8, "epen2_mongo_connection_id", 27017, "epen2_mongo_connection_id", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .build(),
                Operations.insertInto("environment")
                        .columns("environment_id", "name", "fe_url", "epen_url", "fe_mysql_connection_id",
                                "be_mysql_connection_id", "epen2_mysql_connection_id", "iris_mysql_connection_id",
                                "ice_bridge_oracle_connection_id", "data_warehouse_mongo_connection_id",
                                "fe_api_connection_id", "be_api_connection_id", "epen2_api_connection_id",
                                "iris_api_connection_id", "ice_bridge_api_connection_id",
                                "data_warehouse_api_connection_id", "epen2_mongo_connection_id")
                        .values(1, "TestEnv", "http://pan-tst-env.pearsondev.com/",
                                "https://epen2-int-ref-3-0.pearsondev.com", 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 8)
                        .build(),
                Operations.insertInto("path").columns("path_id", "path_name", "path_description")
                        .values(1, "DefaultPath", "This is the default path").build(),
                Operations.insertInto("state").columns("state_id", "state_name", "state_description", "auto_retries")
                        .values(1, "FE_STUDENT_SESSION_DETAIL_FETCHER", "state_description", 0)
                        .values(2, "TEST_SCENARIO_CREATOR", "state_description", 0)
                        .values(3, "TN8_API_DATA_LOADER", "state_description", 0)
                        .values(4, "IRIS_PROCESSING_VALIDATOR", "state_description", 0)
                        .values(5, "BE_PATH_PROCESSING_VALIDATOR", "state_description", 0)
                        .values(6, "EPEN2_SCORING_SCENARIO_CREATOR", "state_description", 0)
                        .values(7, "EPEN2_BULK_SCORER", "state_description", 0)
                        .values(8, "EXPECTED_SCORE_GENERATOR", "state_description", 0)
                        .values(9, "DATAWAREHOUSE_EXTRACTOR", "state_description", 0)
                        .values(10, "EXPECTED_TO_ACTUAL_SCORE_COMPARER", "state_description", 0).build(),
                Operations.insertInto("path_state").columns("path_state_id", "path_id", "state_id").values(1, 1, 1)
                        .values(2, 1, 2).values(3, 1, 3).values(4, 1, 4).values(5, 1, 5).values(6, 1, 6).values(7, 1, 7)
                        .values(8, 1, 8).values(9, 1, 9).values(10, 1, 10).build(),
                Operations.insertInto("div_job_exec_type").columns("div_job_exec_type_id", "div_job_exec_type")
                        .values(1, "PANEXT").build(),
                Operations.insertInto("test_type").columns("test_type_id", "name")
                        .values(1, "Standard").values(2, "Battery").build(),
                Operations.insertInto("div_job_exec")
                        .columns("div_job_exec_id", "environment_id", "path_id", "completed", "submit_time_stamp",
                                "complete_time_stamp", "submit_user_id", "priority", "div_job_exec_status_id",
                                "div_job_exec_type_id", "test_type_id")
                        .values(1, 1, 1, 0, timestamp, timestamp, 1, 1, 1, 1, 1)
                        .values(2, 1, 1, 0, timestamp, timestamp, 1, 1, 2, 1, 1).build(),
                Operations.insertInto("scenario_type").columns("scenario_type_id", "scenario_type").values(1, "TN8")
                        .values(2, "EPEN2").build(),
                Operations.insertInto("scenario_class").columns("scenario_class_id", "scenario_class")
                        .values(1, "minPoints").values(2, "maxPoints").build(),
                Operations.insertInto("testmap_detail")
                        .columns("testmap_detail_id", "testmap_id", "testmap_name", "testmap_publish_format",
                                "testmap_version")
                        .values(1, 1234, "TestMap1", "E", 1).build(),
                Operations.insertInto("scenario")
                        .columns("scenario_id", "scenario_type_id", "scenario", "scenario_name", "test_Code",
                                "form_code", "scenario_class_id", "testmap_detail_id")
                        .values(1, 1, "7468697320697320612074657374", "minPoints1", "tc1", "fc1", 1, 1)
                        .values(2, 2, "7468697320697320612074657374", "maxPoints1", "tc1", "fc1", 2, 1).build(),
                Operations.insertInto("test_session")
                        .columns("test_session_id", "testnav_application_url", "testnav_webservice_url",
                                "testnav_client_secret", "testnav_client_identifier", "testnav_version",
                                "testnav_customer_code", "scope_code", "session_seal_codes", "session_name",
                                "pan_test_session_id", "environment_id")
                        .values(1, "http://www.testnav.com", "http://www.testnav-api.com", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "identifier", "8.2", "pcc", "standard", "sc1,sc2", "testSession1", "1", 1)
                        .build(),
                Operations.insertInto("student_scenario_status").columns("student_scenario_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("student_scenario")
                        .columns("student_scenario_id", "uuid", "testnav_login_name", "testnav_login_password",
                                "wf_db_connection_id", "div_job_exec_id", "testnav_scenario_id", "epen_scenario_id",
                                "last_completed_state_id", "student_scenario_status_id", "test_session_id")
                        .values(1, "a1f27d1b-3b5a-48a3-a0cb-ce9e728d37f5", "4520549168", "b371b5", 7, 1, 1, 2, 1, 1, 1)
                        .build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        divJobExecService = app.injector().instanceOf(DivJobExecService.class);
    }

    @Test
    public void findAll_divJobExecsExist_returnAll() throws Throwable {
        // When
        List<DivJobExec> divJobExecs = jpaApi.withTransaction(divJobExecService::findAll);

        // Then
        assertThat("The returned list should have a length of 2", divJobExecs.size(), equalTo(2));

        assertThat("The divJobExecId of the first item should be correct", divJobExecs.get(0).getDivJobExecId(),
                equalTo(1L));
        assertThat("The submitTimeStamp of the first item should be correct", divJobExecs.get(0).getSubmitTimeStamp(),
                equalTo(timestamp));
        assertThat("The user name of the first item should be correct",
                divJobExecs.get(0).getSubmitUser().getFullname(), equalTo("Tester Testington"));
        assertThat("The status of the first item should be correct",
                divJobExecs.get(0).getDivJobExecStatus().getStatus(), equalTo("Not Started"));
        assertThat("The environment name of the first item should be correct",
                divJobExecs.get(0).getEnvironment().getName(), equalTo("TestEnv"));

        assertThat("The divJobExecId of the second item should be correct", divJobExecs.get(1).getDivJobExecId(),
                equalTo(2L));
        assertThat("The submitTimeStamp of the second item should be correct", divJobExecs.get(1).getSubmitTimeStamp(),
                equalTo(timestamp));
        assertThat("The user name of the second item should be correct",
                divJobExecs.get(1).getSubmitUser().getFullname(), equalTo("Tester Testington"));
        assertThat("The status of the second item should be correct",
                divJobExecs.get(1).getDivJobExecStatus().getStatus(), equalTo("In Progress"));
        assertThat("The environment name of the second item should be correct",
                divJobExecs.get(1).getEnvironment().getName(), equalTo("TestEnv"));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAll_noDivJobExecsExist_returnEmptyList() throws Throwable {
        // Given
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL);
        Database db = app.injector().instanceOf(Database.class);
        new DbSetup(new DataSourceDestination(db.getDataSource()), operation).launch();

        // When
        List<DivJobExec> divJobExecs = jpaApi.withTransaction(divJobExecService::findAll);

        // Then
        assertThat("The returned list should be empty", divJobExecs.isEmpty(), equalTo(true));
    }

    @Test
    public void findByDivJobExecId_givenDivJobExecIdExists_returnCorrectDivJobExec() throws Throwable {
        // When
        Optional<DivJobExec> optionalDivJobExec = jpaApi
                .withTransaction(() -> divJobExecService.findByDivJobExecId(1L));

        // Then
        assertThat("The div_job_exec_id of the returned divJobExec should be as expected",
                optionalDivJobExec.get().getDivJobExecId(), equalTo(1L));

        assertThat("The environment_id of the returned divJobExec should be as expected",
                optionalDivJobExec.get().getEnvironment().getEnvironmentId(), equalTo(1));

        assertThat("The path_id  of the returned divJobExec should be as expected",
                optionalDivJobExec.get().getPath().getPathId(), equalTo(1));

        assertThat("The 'completed' field of the returned divJobExec should be as expected",
                optionalDivJobExec.get().isCompleted(), equalTo(false));

        assertThat("The submit_user_id of the returned divJobExec should be as expected",
                optionalDivJobExec.get().getSubmitUserId().intValue(), equalTo(1));

        assertThat("The priority of the returned divJobExec should be as expected",
                optionalDivJobExec.get().getPriority(), equalTo(1));

        assertThat("The div_job_exec_status_id of the returned divJobExec should be as expected",
                optionalDivJobExec.get().getDivJobExecStatusId(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByDivJobExecId_givenDivJobExecIdDoesNotExist_returnNull() throws Throwable {
        // When
        Optional<DivJobExec> optionalDivJobExec = jpaApi
                .withTransaction(() -> divJobExecService.findByDivJobExecId(0L));

        // Then
        assertThat("The result should be empty", optionalDivJobExec.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_validEnvironmentIdAndDivJobIdGiven_returnOptionalWithDivJobExec() throws Throwable {
        // When
        Optional<DivJobExec> divJobExecOptional = jpaApi.withTransaction(() -> divJobExecService.find(1, 1L));

        // Then
        assertThat("The div_job_exec_id of the returned divJobExec should be as expected",
                divJobExecOptional.get().getDivJobExecId(), equalTo(1L));
        assertThat("The 'completed' field of the returned divJobExec should be as expected",
                divJobExecOptional.get().isCompleted(), equalTo(false));
        assertThat("The submit_user_id of the returned divJobExec should be as expected",
                divJobExecOptional.get().getSubmitUserId().intValue(), equalTo(1));
        assertThat("The priority of the returned divJobExec should be as expected",
                divJobExecOptional.get().getPriority(), equalTo(1));
        assertThat("The div_job_exec_status_id of the returned divJobExec should be as expected",
                divJobExecOptional.get().getDivJobExecStatusId(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_invalidEnvironmentIdIdGiven_returnEmptyOptional() throws Throwable {
        // When
        Optional<DivJobExec> divJobExecOptional = jpaApi.withTransaction(() -> divJobExecService.find(2, 1L));

        // Then
        assertThat("The optional should be empty", divJobExecOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_invalidDivJobExecIdIdGiven_returnEmptyOptional() throws Throwable {
        // When
        Optional<DivJobExec> divJobExecOptional = jpaApi.withTransaction(() -> divJobExecService.find(1, 5L));

        // Then
        assertThat("The optional should be empty", divJobExecOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findStatusByDivJobExecId_invalidDivJobExecIdIdGiven_returnEmptyOptional() throws Throwable {
        // When
        Optional<DivJobExec> divJobExecOptional = jpaApi.withTransaction(() -> divJobExecService
                .findStatusByDivJobExecId(0l));

        // Then
        assertThat("The optional should be empty", divJobExecOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findStatusByDivJobExecId_validDivJobExecIdIdGiven_returnOptionalWithDivJobExec() throws Throwable {
        // When
        Optional<DivJobExec> divJobExecOptional = jpaApi.withTransaction(() -> divJobExecService
                .findStatusByDivJobExecId(1l));

        // Then
        assertThat("The div_job_exec_id of the returned divJobExec should be as expected", divJobExecOptional.get()
                .getDivJobExecId(), equalTo(1L));
        assertThat("The 'status' field of the returned divJobExec should be as expected", divJobExecOptional.get()
                .getDivJobExecStatus().getStatus(), equalTo("Not Started"));
        assertThat("The environment name of the returned divJobExec should be as expected", divJobExecOptional.get()
                .getEnvironment().getName(), equalTo("TestEnv"));
        dbSetupTracker.skipNextLaunch();
    }
}