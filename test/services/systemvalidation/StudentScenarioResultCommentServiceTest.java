package services.systemvalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.systemvalidation.StudentScenarioResultComment;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Gert Selis
 */
public class StudentScenarioResultCommentServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private Timestamp timestamp;

    @Before
    public void setUp() {
        timestamp = Timestamp.from(Instant.now());

        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,

                Operations.insertInto("db_connection")
                        .columns("db_connection_id", "host", "port", "db_name", "db_user", "db_pass")
                        .values(1, "testHost", "1234", "testDatabase", "testUser", "testPassword").build(),
                Operations.insertInto("api_connection").columns("api_connection_id", "token", "secret", "api_url")
                        .values("1", "testToken", "testSecret", "testApi_url").build(),
                Operations.insertInto("environment")
                        .columns("environment_id", "name", "fe_url", "fe_mysql_connection_id", "be_mysql_connection_id",
                                "epen2_mysql_connection_id", "iris_mysql_connection_id",
                                "ice_bridge_oracle_connection_id", "data_warehouse_mongo_connection_id",
                                "fe_api_connection_id", "be_api_connection_id", "epen2_api_connection_id",
                                "iris_api_connection_id", "ice_bridge_api_connection_id",
                                "data_warehouse_api_connection_id", "epen_url", "epen2_mongo_connection_id")
                        .values(1, "testEnvironment", "feTestUrl", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, "epenTestUrl", 1)
                        .build(),
                Operations.insertInto("path").columns("path_id", "path_name", "path_description")
                        .values(1, "testPath", "testPathDescription").build(),
                Operations.insertInto("user").columns("id", "fullname", "isAdmin", "is_api_user")
                        .values(1, "testUser", 1, 0).build(),
                Operations.insertInto("div_job_exec_status").columns("div_job_exec_status_id", "status")
                        .values(1, "Completed").build(),
                Operations.insertInto("div_job_exec_type").columns("div_job_exec_type_id", "div_job_exec_type")
                        .values(1, "PANEXT").build(),
                Operations.insertInto("test_type").columns("test_type_id", "name")
                        .values(1, "Standard").values(2, "Battery").build(),
                Operations.insertInto("div_job_exec")
                        .columns("div_job_exec_id", "environment_id", "path_id", "completed", "submit_time_stamp",
                                "complete_time_stamp", "submit_user_id", "priority", "div_job_exec_status_id",
                                "div_job_exec_type_id", "scope_tree_path", "test_type_id")
                        .values(1, 1, 1, 1, timestamp, timestamp, 1, 1, 1, 1, "testScopeTreePath", 1).build(),
                Operations.insertInto("state").columns("state_id", "state_name", "state_description")
                        .values(1, "testState", "testStateDescription").build(),
                Operations.insertInto("scenario_class")
                        .columns("scenario_class_id", "scenario_class", "enabled_for_tn8", "enabled_for_epen")
                        .values(1, "maxPoints", 1, 1).build(),
                Operations.insertInto("scenario_type").columns("scenario_type_id", "scenario_type").values("1", "TN8")
                        .build(),
                Operations.insertInto("testmap_detail")
                        .columns("testmap_detail_id", "testmap_id", "testmap_name", "testmap_version",
                                "testmap_publish_format")
                        .values(1, "12345", "testTestMap", 1, "E").build(),
                Operations.insertInto("scenario")
                        .columns("scenario_id", "scenario_type_id", "scenario_class_id", "scenario", "test_code",
                                "form_code", "testmap_detail_id", "scenario_name", "previewer_tenant_id")
                        .values(1, 1, 1, "ABCD1234", "testTestCode", "testFormCode", 1, "testScenarioName", 1).build(),
                Operations.insertInto("test_session")
                        .columns("test_session_id", "testnav_application_url", "testnav_webservice_url",
                                "testnav_client_secret", "testnav_client_identifier", "testnav_version",
                                "testnav_customer_code", "scope_code", "session_name", "pan_test_session_id",
                                "environment_id")
                        .values(1, "http://test.testnav.com", "http://test-api.testnav.com", "12345secret",
                                "67890identifier", "8.0", "tst", "tst15", "testSession", 1, 1)
                        .build(),
                Operations.insertInto("student_scenario_status").columns("student_scenario_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("student_scenario")
                        .columns("student_scenario_id", "uuid", "testnav_login_name", "testnav_login_password",
                                "div_job_exec_id", "testnav_scenario_id", "last_completed_state_id",
                                "student_scenario_status_id", "student_code", "last_name", "first_name",
                                "student_group_name", "student_test_status", "session_assign_status", "export_type",
                                "test_session_id", "wf_db_connection_id", "epen_scenario_id")
                        .values("1", "testUuid", "testLoginName", "testPassword", "1", "1", "1", "1", "testStudentCode",
                                "testLastName", "testFirstName", "testGroupName", "testTestStatus", "ready",
                                "testExportType", "1", "1", "1")
                        .build(),
                Operations
                        .insertInto("student_scenario_result").columns("student_scenario_result_id",
                        "student_scenario_id", "state_id", "result_details", "success", "version")
                        .values(1, 1, 1, "ABC123", 1, 1).build(),

                Operations.insertInto("student_scenario_result_comment_category")
                        .columns("student_scenario_result_comment_category_id", "name", "description")
                        .values("1", "testCategoryName", "testCategoryDescription").build(),
                Operations.insertInto("external_defect_repository").columns("external_defect_repository_id", "name")
                        .values(1, "testDefectRepoName").build(),
                Operations.insertInto("student_scenario_result_comment")
                        .columns("student_scenario_result_comment_id", "student_scenario_result_id",
                                "student_scenario_result_comment_category_id", "result_comment", "timestamp", "user_id",
                                "external_defect_repository_id", "external_defect_id")
                        .values(1, 1, 1, "Comment 1", timestamp, 1, 1, "Defect 1")
                        .values(2, 1, 1, "Comment 2", timestamp, 1, 1, "Defect 2")
                        .values(3, 1, 1, "Comment 3", timestamp, 1, 1, "Defect 3").build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
    }

    @Test
    public void findAll() {
        // Given
        StudentScenarioResultCommentService ssrcs = app.injector()
                .instanceOf(StudentScenarioResultCommentService.class);

        // When
        List<StudentScenarioResultComment> ssrComments = jpaApi.withTransaction(() -> ssrcs.findAll(1, 1L));

        // Then
        assertThat("The size of the returned list should be 3", ssrComments.size(), equalTo(3));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByStudentScenarioResultCommentId() {
        // Given
        StudentScenarioResultCommentService ssrcs = app.injector()
                .instanceOf(StudentScenarioResultCommentService.class);

        // When
        Optional<StudentScenarioResultComment> studentScenarioResultComment = jpaApi
                .withTransaction(() -> ssrcs.findByStudentScenarioResultCommentId(1, 1L));

        // Then
        assertThat(
                "The student_scenario_result_comment_id of the returned student scenario result comment should be as expected",
                studentScenarioResultComment.get().getResultCommentId(), equalTo(1L));

        assertThat("The result_comment of the returned student scenario result comment should be as expected",
                studentScenarioResultComment.get().getResultComment(), equalTo("Comment 1"));

        assertThat(
                "The external_defect_repository name of the returned student scenario result comment should be as expected",
                studentScenarioResultComment.get().getExternalDefectRepository().getName(),
                equalTo("testDefectRepoName"));

        assertThat("The external_defect_id of the returned student scenario result comment should be as expected",
                studentScenarioResultComment.get().getExternalDefectId(), equalTo("Defect 1"));

        assertThat(
                "The student_scenario_result_comment_category name of the returned student scenario result comment should be as expected",
                studentScenarioResultComment.get().getStudentScenarioResultCommentCategory().getName(),
                equalTo("testCategoryName"));

        assertThat("The student scenario id of the returned student scenario result comment should be as expected",
                studentScenarioResultComment.get().getStudentScenarioResult().getStudentScenarioResultId(),
                equalTo(1L));

        assertThat("The full name for the user of the returned student scenario result comment should be as expected",
                studentScenarioResultComment.get().getUser().getFullname(), equalTo("testUser"));

        dbSetupTracker.skipNextLaunch();
    }
}