/**
 * 
 */
package services.systemvalidation;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import global.TestWithApplication;
import models.systemvalidation.StudentScenarioResultCommentCategory;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

/**
 * @author Gert Selis
 *
 */
public class StudentScenarioResultCommentCategoryServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;

    @Before
    public void setUp() {
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,

                Operations.insertInto("student_scenario_result_comment_category")
                        .columns("student_scenario_result_comment_category_id", "name", "description")
                        .values(1, "Category 1", "Category 1 description")
                        .values(2, "Category 2", "Category 2 description")
                        .values(3, "Category 3", "Category 3 description").build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
    }

    @Test
    public void findAll() {
        // Given
        StudentScenarioResultCommentCategoryService ssrccs = app.injector()
                .instanceOf(StudentScenarioResultCommentCategoryService.class);

        // When
        List<StudentScenarioResultCommentCategory> ssrcCategories = jpaApi.withTransaction(() -> ssrccs.findAll());

        // Then
        assertThat("The size of the returned list should be 3", ssrcCategories.size(), equalTo(3));

        dbSetupTracker.skipNextLaunch();

    }

}
