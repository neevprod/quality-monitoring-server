package services.systemvalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.systemvalidation.Environment;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Gert Selis
 */
public class EnvironmentServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private Timestamp timestamp;

    @Before
    public void setUp() {
        timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,

                Operations.insertInto("api_connection").columns("api_connection_id", "token", "secret", "api_url")
                        .values(1, "fe_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://fe_api_connection_url")
                        .values(2, "be_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://be_api_connection_url")
                        .values(3, "epen2_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://epen2_api_connection_url")
                        .values(4, "iris_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://iris_api_connection_url")
                        .values(5, "ice_bridge_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://ice_bridge_api_connection_url")
                        .values(6, "data_warehouse_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://data_warehouse_api_connection_url")
                        .build(),

                Operations.insertInto("db_connection")
                        .columns("db_connection_id", "host", "port", "db_name", "db_user", "db_pass")
                        .values(1, "fe_mysql_connection_host", 3306, "fe_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(2, "be_mysql_connection_host", 3306, "be_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(3, "epen2_mysql_connection_host", 3306, "epen2_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(4, "iris_mysql_connection_host", 3306, "iris_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(5, "ice_bridge_oracle_connection_host", 3306, "ice_bridge_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(6, "data_warehouse_mongo_connection_host", 27017, "data_warehouse_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(7, "wf_db_connection_host", 3306, "wf_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(8, "epen2_mongo_connection_id", 27017, "epen2_mongo_connection_id", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .build(),

                Operations.insertInto("environment")
                        .columns("environment_id", "name", "fe_url", "epen_url", "fe_mysql_connection_id",
                                "be_mysql_connection_id", "epen2_mysql_connection_id", "iris_mysql_connection_id",
                                "ice_bridge_oracle_connection_id", "data_warehouse_mongo_connection_id",
                                "fe_api_connection_id", "be_api_connection_id", "epen2_api_connection_id",
                                "iris_api_connection_id", "ice_bridge_api_connection_id",
                                "data_warehouse_api_connection_id", "epen2_mongo_connection_id")
                        .values(1, "TestEnv", "https://fe1.pearsondev.com", "https://epen1.pearsondev.com", 1, 2, 3, 4,
                                5, 6, 1, 2, 3, 4, 5, 6, 8)
                        .values(2, "TestEnv2", "https://fe2.pearsondev.com", "https://epen2.pearsondev.com", 1, 2, 3, 4,
                                5, 6, 1, 2, 3, 4, 5, 6, 8)
                        .values(3, "TestEnv3", "https://fe3.pearsondev.com", "https://epen3.pearsondev.com", 1, 2, 3, 4,
                                5, 6, 1, 2, 3, 4, 5, 6, 8)
                        .build(),
                Operations.insertInto("div_job_exec_status").columns("div_job_exec_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("div_job_exec_type").columns("div_job_exec_type_id", "div_job_exec_type")
                        .values(1, "PANEXT").build(),
                Operations.insertInto("path").columns("path_id", "path_name", "path_description")
                        .values(1, "DefaultPath", "This is the default path").build(),
                Operations.insertInto("user").columns("id", "fullname", "isAdmin", "is_api_user")
                        .values(1, "Tarun Gupta", 1, 0).values(2, "Nand Joshi", 0, 0).build(),
                Operations.insertInto("test_type").columns("test_type_id", "name")
                        .values(1, "Standard").values(2, "Battery").build(),
                Operations.insertInto("div_job_exec")
                        .columns("div_job_exec_id", "complete_time_stamp", "completed", "priority", "submit_time_stamp",
                                "submit_user_id", "div_job_exec_status_id", "div_job_exec_type_id", "environment_id",
                                "path_id", "scope_tree_path", "test_type_id")
                        .values(1, timestamp, 1, 1, timestamp, 1, 3, 1, 2, 1, "/ref/2015-16/refspr16", 1).build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
    }

    @Test
    public void findById_givenEnvironmentIdExists_returnCorrectEnvironment() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);

        // When
        Optional<Environment> optionalEnvironment = jpaApi.withTransaction(() -> environmentService.findById(1));

        // Then
        assertThat("The environment_id of the returned environment should be as expected",
                optionalEnvironment.get().getEnvironmentId(), equalTo(1));

        assertThat("The name of the returned environment should be as expected", optionalEnvironment.get().getName(),
                equalTo("TestEnv"));

        assertThat("The fe_url of the returned environment should be as expected", optionalEnvironment.get().getFeUrl(),
                equalTo("https://fe1.pearsondev.com"));

        assertThat("The epen_url of the returned environment should be as expected",
                optionalEnvironment.get().getEpenUrl(), equalTo("https://epen1.pearsondev.com"));

        assertThat("The fe_mysql_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getFeMysqlConnection().getDbConnectionId(), equalTo(1));

        assertThat("The be_mysql_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getBeMysqlConnection().getDbConnectionId(), equalTo(2));

        assertThat("The epen2_mysql_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getEpen2MysqlConnection().getDbConnectionId(), equalTo(3));

        assertThat("The iris_mysql_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getIrisMysqlConnection().getDbConnectionId(), equalTo(4));

        assertThat("The ice_bridge_oracle_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getIceBridgeOracleConnection().getDbConnectionId(), equalTo(5));

        assertThat("The data_warehouse_mongo_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getDataWarehouseMongoConnection().getDbConnectionId(), equalTo(6));

        assertThat("The fe_api_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getFeApiConnection().getApiConnectionId(), equalTo(1));

        assertThat("The be_api_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getBeApiConnection().getApiConnectionId(), equalTo(2));

        assertThat("The epen2_api_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getEpen2ApiConnection().getApiConnectionId(), equalTo(3));

        assertThat("The iris_api_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getIrisApiConnection().getApiConnectionId(), equalTo(4));

        assertThat("The ice_bridge_api_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getIceBridgeApiConnection().getApiConnectionId(), equalTo(5));

        assertThat("The data_warehouse_api_connection_id of the returned environment should be as expected",
                optionalEnvironment.get().getDataWarehouseApiConnection().getApiConnectionId(), equalTo(6));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findById_givenEnvironmentIdDoesNotExist_returnNull() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);

        // When
        Optional<Environment> optionalEnvironment = jpaApi.withTransaction(() -> environmentService.findById(0));

        // Then
        assertThat("The result should be empty", optionalEnvironment.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAll_noIdsGiven_returnListOfAllEnvironments() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);

        // When
        List<Environment> environments = jpaApi.withTransaction(() -> environmentService.findAll());

        // Then
        assertThat("The size of the returned list should be 3", environments.size(), equalTo(3));

        assertThat("The environment_id of the first environment should be as expected",
                environments.get(0).getEnvironmentId(), equalTo(1));
        assertThat("The name of the first environment should be as expected", environments.get(0).getName(),
                equalTo("TestEnv"));
        assertThat("The fe_url of the first environment should be as expected", environments.get(0).getFeUrl(),
                equalTo("https://fe1.pearsondev.com"));
        assertThat("The epen_url of the returned environment should be as expected", environments.get(0).getEpenUrl(),
                equalTo("https://epen1.pearsondev.com"));
        assertThat("The fe_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getFeMysqlConnection().getDbConnectionId(), equalTo(1));
        assertThat("The be_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getBeMysqlConnection().getDbConnectionId(), equalTo(2));
        assertThat("The epen2_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getEpen2MysqlConnection().getDbConnectionId(), equalTo(3));
        assertThat("The iris_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getIrisMysqlConnection().getDbConnectionId(), equalTo(4));
        assertThat("The ice_bridge_oracle_connection_id of the first environment should be as expected",
                environments.get(0).getIceBridgeOracleConnection().getDbConnectionId(), equalTo(5));
        assertThat("The data_warehouse_mongo_connection_id of the first environment should be as expected",
                environments.get(0).getDataWarehouseMongoConnection().getDbConnectionId(), equalTo(6));
        assertThat("The fe_api_connection_id of the first environment should be as expected",
                environments.get(0).getFeApiConnection().getApiConnectionId(), equalTo(1));
        assertThat("The be_api_connection_id of the first environment should be as expected",
                environments.get(0).getBeApiConnection().getApiConnectionId(), equalTo(2));
        assertThat("The epen2_api_connection_id of the first environment should be as expected",
                environments.get(0).getEpen2ApiConnection().getApiConnectionId(), equalTo(3));
        assertThat("The iris_api_connection_id of the first environment should be as expected",
                environments.get(0).getIrisApiConnection().getApiConnectionId(), equalTo(4));
        assertThat("The ice_bridge_api_connection_id of the first environment should be as expected",
                environments.get(0).getIceBridgeApiConnection().getApiConnectionId(), equalTo(5));
        assertThat("The data_warehouse_api_connection_id of the first environment should be as expected",
                environments.get(0).getDataWarehouseApiConnection().getApiConnectionId(), equalTo(6));

        assertThat("The environment_id of the second environment should be as expected",
                environments.get(1).getEnvironmentId(), equalTo(2));
        assertThat("The name of the second environment should be as expected", environments.get(1).getName(),
                equalTo("TestEnv2"));
        assertThat("The fe_url of the second environment should be as expected", environments.get(1).getFeUrl(),
                equalTo("https://fe2.pearsondev.com"));
        assertThat("The epen_url of the returned environment should be as expected", environments.get(1).getEpenUrl(),
                equalTo("https://epen2.pearsondev.com"));
        assertThat("The fe_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getFeMysqlConnection().getDbConnectionId(), equalTo(1));
        assertThat("The be_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getBeMysqlConnection().getDbConnectionId(), equalTo(2));
        assertThat("The epen2_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getEpen2MysqlConnection().getDbConnectionId(), equalTo(3));
        assertThat("The iris_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getIrisMysqlConnection().getDbConnectionId(), equalTo(4));
        assertThat("The ice_bridge_oracle_connection_id of the second environment should be as expected",
                environments.get(1).getIceBridgeOracleConnection().getDbConnectionId(), equalTo(5));
        assertThat("The data_warehouse_mongo_connection_id of the second environment should be as expected",
                environments.get(1).getDataWarehouseMongoConnection().getDbConnectionId(), equalTo(6));
        assertThat("The fe_api_connection_id of the second environment should be as expected",
                environments.get(1).getFeApiConnection().getApiConnectionId(), equalTo(1));
        assertThat("The be_api_connection_id of the second environment should be as expected",
                environments.get(1).getBeApiConnection().getApiConnectionId(), equalTo(2));
        assertThat("The epen2_api_connection_id of the second environment should be as expected",
                environments.get(1).getEpen2ApiConnection().getApiConnectionId(), equalTo(3));
        assertThat("The iris_api_connection_id of the second environment should be as expected",
                environments.get(1).getIrisApiConnection().getApiConnectionId(), equalTo(4));
        assertThat("The ice_bridge_api_connection_id of the second environment should be as expected",
                environments.get(1).getIceBridgeApiConnection().getApiConnectionId(), equalTo(5));
        assertThat("The data_warehouse_api_connection_id of the second environment should be as expected",
                environments.get(1).getDataWarehouseApiConnection().getApiConnectionId(), equalTo(6));

        assertThat("The environment_id of the third environment should be as expected",
                environments.get(2).getEnvironmentId(), equalTo(3));
        assertThat("The name of the third environment should be as expected", environments.get(2).getName(),
                equalTo("TestEnv3"));
        assertThat("The fe_url of the third environment should be as expected", environments.get(2).getFeUrl(),
                equalTo("https://fe3.pearsondev.com"));
        assertThat("The epen_url of the returned environment should be as expected", environments.get(2).getEpenUrl(),
                equalTo("https://epen3.pearsondev.com"));
        assertThat("The fe_mysql_connection_id of the third environment should be as expected",
                environments.get(2).getFeMysqlConnection().getDbConnectionId(), equalTo(1));
        assertThat("The be_mysql_connection_id of the third environment should be as expected",
                environments.get(2).getBeMysqlConnection().getDbConnectionId(), equalTo(2));
        assertThat("The epen2_mysql_connection_id of the third environment should be as expected",
                environments.get(2).getEpen2MysqlConnection().getDbConnectionId(), equalTo(3));
        assertThat("The iris_mysql_connection_id of the third environment should be as expected",
                environments.get(2).getIrisMysqlConnection().getDbConnectionId(), equalTo(4));
        assertThat("The ice_bridge_oracle_connection_id of the third environment should be as expected",
                environments.get(2).getIceBridgeOracleConnection().getDbConnectionId(), equalTo(5));
        assertThat("The data_warehouse_mongo_connection_id of the third environment should be as expected",
                environments.get(2).getDataWarehouseMongoConnection().getDbConnectionId(), equalTo(6));
        assertThat("The fe_api_connection_id of the third environment should be as expected",
                environments.get(2).getFeApiConnection().getApiConnectionId(), equalTo(1));
        assertThat("The be_api_connection_id of the third environment should be as expected",
                environments.get(2).getBeApiConnection().getApiConnectionId(), equalTo(2));
        assertThat("The epen2_api_connection_id of the third environment should be as expected",
                environments.get(2).getEpen2ApiConnection().getApiConnectionId(), equalTo(3));
        assertThat("The iris_api_connection_id of the third environment should be as expected",
                environments.get(2).getIrisApiConnection().getApiConnectionId(), equalTo(4));
        assertThat("The ice_bridge_api_connection_id of the third environment should be as expected",
                environments.get(2).getIceBridgeApiConnection().getApiConnectionId(), equalTo(5));
        assertThat("The data_warehouse_api_connection_id of the third environment should be as expected",
                environments.get(2).getDataWarehouseApiConnection().getApiConnectionId(), equalTo(6));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAll_idsGiven_returnListOfAllEnvironmentsFilteredByGivenIds() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);
        Set<Integer> environmentIds = new HashSet<>();
        environmentIds.add(1);
        environmentIds.add(3);

        // When
        List<Environment> environments = jpaApi.withTransaction(() -> environmentService.findAll(environmentIds));

        // Then
        assertThat("The size of the returned list should be 2", environments.size(), equalTo(2));

        assertThat("The environment_id of the first environment should be as expected",
                environments.get(0).getEnvironmentId(), equalTo(1));
        assertThat("The name of the first environment should be as expected", environments.get(0).getName(),
                equalTo("TestEnv"));
        assertThat("The fe_url of the first environment should be as expected", environments.get(0).getFeUrl(),
                equalTo("https://fe1.pearsondev.com"));
        assertThat("The epen_url of the returned environment should be as expected", environments.get(0).getEpenUrl(),
                equalTo("https://epen1.pearsondev.com"));
        assertThat("The fe_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getFeMysqlConnection().getDbConnectionId(), equalTo(1));
        assertThat("The be_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getBeMysqlConnection().getDbConnectionId(), equalTo(2));
        assertThat("The epen2_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getEpen2MysqlConnection().getDbConnectionId(), equalTo(3));
        assertThat("The iris_mysql_connection_id of the first environment should be as expected",
                environments.get(0).getIrisMysqlConnection().getDbConnectionId(), equalTo(4));
        assertThat("The ice_bridge_oracle_connection_id of the first environment should be as expected",
                environments.get(0).getIceBridgeOracleConnection().getDbConnectionId(), equalTo(5));
        assertThat("The data_warehouse_mongo_connection_id of the first environment should be as expected",
                environments.get(0).getDataWarehouseMongoConnection().getDbConnectionId(), equalTo(6));
        assertThat("The fe_api_connection_id of the first environment should be as expected",
                environments.get(0).getFeApiConnection().getApiConnectionId(), equalTo(1));
        assertThat("The be_api_connection_id of the first environment should be as expected",
                environments.get(0).getBeApiConnection().getApiConnectionId(), equalTo(2));
        assertThat("The epen2_api_connection_id of the first environment should be as expected",
                environments.get(0).getEpen2ApiConnection().getApiConnectionId(), equalTo(3));
        assertThat("The iris_api_connection_id of the first environment should be as expected",
                environments.get(0).getIrisApiConnection().getApiConnectionId(), equalTo(4));
        assertThat("The ice_bridge_api_connection_id of the first environment should be as expected",
                environments.get(0).getIceBridgeApiConnection().getApiConnectionId(), equalTo(5));
        assertThat("The data_warehouse_api_connection_id of the first environment should be as expected",
                environments.get(0).getDataWarehouseApiConnection().getApiConnectionId(), equalTo(6));

        assertThat("The environment_id of the second environment should be as expected",
                environments.get(1).getEnvironmentId(), equalTo(3));
        assertThat("The name of the second environment should be as expected", environments.get(1).getName(),
                equalTo("TestEnv3"));
        assertThat("The fe_url of the second environment should be as expected", environments.get(1).getFeUrl(),
                equalTo("https://fe3.pearsondev.com"));
        assertThat("The epen_url of the returned environment should be as expected", environments.get(1).getEpenUrl(),
                equalTo("https://epen3.pearsondev.com"));
        assertThat("The fe_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getFeMysqlConnection().getDbConnectionId(), equalTo(1));
        assertThat("The be_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getBeMysqlConnection().getDbConnectionId(), equalTo(2));
        assertThat("The epen2_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getEpen2MysqlConnection().getDbConnectionId(), equalTo(3));
        assertThat("The iris_mysql_connection_id of the second environment should be as expected",
                environments.get(1).getIrisMysqlConnection().getDbConnectionId(), equalTo(4));
        assertThat("The ice_bridge_oracle_connection_id of the second environment should be as expected",
                environments.get(1).getIceBridgeOracleConnection().getDbConnectionId(), equalTo(5));
        assertThat("The data_warehouse_mongo_connection_id of the second environment should be as expected",
                environments.get(1).getDataWarehouseMongoConnection().getDbConnectionId(), equalTo(6));
        assertThat("The fe_api_connection_id of the second environment should be as expected",
                environments.get(1).getFeApiConnection().getApiConnectionId(), equalTo(1));
        assertThat("The be_api_connection_id of the second environment should be as expected",
                environments.get(1).getBeApiConnection().getApiConnectionId(), equalTo(2));
        assertThat("The epen2_api_connection_id of the second environment should be as expected",
                environments.get(1).getEpen2ApiConnection().getApiConnectionId(), equalTo(3));
        assertThat("The iris_api_connection_id of the second environment should be as expected",
                environments.get(1).getIrisApiConnection().getApiConnectionId(), equalTo(4));
        assertThat("The ice_bridge_api_connection_id of the second environment should be as expected",
                environments.get(1).getIceBridgeApiConnection().getApiConnectionId(), equalTo(5));
        assertThat("The data_warehouse_api_connection_id of the second environment should be as expected",
                environments.get(1).getDataWarehouseApiConnection().getApiConnectionId(), equalTo(6));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAll_emptySetOfIdsGiven_returnEmptyList() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);
        Set<Integer> environmentIds = new HashSet<>();

        // When
        List<Environment> environments = jpaApi.withTransaction(() -> environmentService.findAll(environmentIds));

        // Then
        assertThat("The size of the returned list should be 0", environments.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllEnvironmentsWithDivJob__noIdsGiven_returnAllEnvironmentsWithDivJob() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);

        // When
        List<Environment> environments = jpaApi
                .withTransaction(() -> environmentService.findAllEnvironmentsWithDivJob());

        // Then
        assertThat("The size of the returned list should be 1", environments.size(), equalTo(1));

        assertThat("The environment_id of the first environment should be as expected",
                environments.get(0).getEnvironmentId(), equalTo(2));
        assertThat("The name of the first environment should be as expected", environments.get(0).getName(),
                equalTo("TestEnv2"));
        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByIdWithDivJob_givenEnvironmentIdExists_returnCorrectEnvironmentWithDivJob() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);

        // When
        Optional<Environment> optionalEnvironment = jpaApi
                .withTransaction(() -> environmentService.findByIdWithDivJob(2));

        // Then
        assertThat("The environment_id of the returned environment should be as expected",
                optionalEnvironment.get().getEnvironmentId(), equalTo(2));

        assertThat("The name of the returned environment should be as expected", optionalEnvironment.get().getName(),
                equalTo("TestEnv2"));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByIdWithDivJob_givenEnvironmentIdDoesNotExist_returnNullWithDivJob() throws Throwable {
        // Given
        EnvironmentService environmentService = app.injector().instanceOf(EnvironmentService.class);

        // When
        Optional<Environment> optionalEnvironment = jpaApi
                .withTransaction(() -> environmentService.findByIdWithDivJob(0));

        // Then
        assertThat("The result should be empty", optionalEnvironment.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }
}
