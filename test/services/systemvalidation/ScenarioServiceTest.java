package services.systemvalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.systemvalidation.Scenario;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ScenarioServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private ScenarioService scenarioService;
    private Timestamp timestamp;

    @Before
    public void setUp() {
        timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,

                Operations.insertInto("div_job_exec_status").columns("div_job_exec_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("user").columns("id", "fullname", "isAdmin").values(1, "Tester Testington", 0)
                        .build(),
                Operations.insertInto("api_connection").columns("api_connection_id", "token", "secret", "api_url")
                        .values(1, "fe_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://fe_api_connection_url")
                        .values(2, "be_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://be_api_connection_url")
                        .values(3, "epen2_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://epen2_api_connection_url")
                        .values(4, "iris_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://iris_api_connection_url")
                        .values(5, "ice_bridge_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://ice_bridge_api_connection_url")
                        .values(6, "data_warehouse_api_connection_token", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "http://data_warehouse_api_connection_url")
                        .build(),
                Operations.insertInto("db_connection")
                        .columns("db_connection_id", "host", "port", "db_name", "db_user", "db_pass")
                        .values(1, "fe_mysql_connection_host", 3306, "fe_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(2, "be_mysql_connection_host", 3306, "be_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(3, "epen2_mysql_connection_host", 3306, "epen2_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(4, "iris_mysql_connection_host", 3306, "iris_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(5, "ice_bridge_oracle_connection_host", 3306, "ice_bridge_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(6, "data_warehouse_mongo_connection_host", 27017, "data_warehouse_db", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(7, "wf_db_connection_host", 3306, "wf_db", "user1", "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .values(8, "epen2_mongo_connection_id", 27017, "epen2_mongo_connection_id", "user1",
                                "vm6TTJHmq+wZ9PXQuhFO+w==")
                        .build(),
                Operations.insertInto("environment")
                        .columns("environment_id", "name", "fe_url", "epen_url", "fe_mysql_connection_id",
                                "be_mysql_connection_id", "epen2_mysql_connection_id", "iris_mysql_connection_id",
                                "ice_bridge_oracle_connection_id", "data_warehouse_mongo_connection_id",
                                "fe_api_connection_id", "be_api_connection_id", "epen2_api_connection_id",
                                "iris_api_connection_id", "ice_bridge_api_connection_id",
                                "data_warehouse_api_connection_id", "epen2_mongo_connection_id")
                        .values(1, "TestEnv", "http://pan-tst-env.pearsondev.com/",
                                "https://epen2-int-ref-3-0.pearsondev.com", 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6, 8)
                        .build(),
                Operations.insertInto("path")
                        .columns("path_id", "path_name",
                                "path_description")
                        .values(1, "DefaultPath",
                                "This is the default path")
                        .build(),
                Operations.insertInto("state").columns("state_id", "state_name", "state_description", "auto_retries")
                        .values(1, "FE_STUDENT_SESSION_DETAIL_FETCHER", "state_description", 0)
                        .values(2, "TEST_SCENARIO_CREATOR", "state_description", 0)
                        .values(3, "TN8_API_DATA_LOADER", "state_description", 0)
                        .values(4, "IRIS_PROCESSING_VALIDATOR", "state_description", 0)
                        .values(5, "BE_PATH_PROCESSING_VALIDATOR", "state_description",
                                0)
                        .values(6, "EPEN2_SCORING_SCENARIO_CREATOR", "state_description", 0)
                        .values(7, "EPEN2_BULK_SCORER", "state_description", 0)
                        .values(8, "EXPECTED_SCORE_GENERATOR", "state_description", 0)
                        .values(9, "DATAWAREHOUSE_EXTRACTOR", "state_description", 0)
                        .values(10, "EXPECTED_TO_ACTUAL_SCORE_COMPARER", "state_description", 0).build(),
                Operations.insertInto("path_state").columns("path_state_id", "path_id", "state_id").values(1, 1, 1)
                        .values(2, 1, 2).values(3, 1, 3).values(4, 1, 4).values(5, 1, 5).values(6, 1, 6).values(7, 1, 7)
                        .values(8, 1, 8).values(9, 1, 9).values(10, 1, 10).build(),
                Operations.insertInto("div_job_exec_type").columns("div_job_exec_type_id", "div_job_exec_type")
                        .values(1, "PANEXT").build(),
                Operations.insertInto("test_type").columns("test_type_id", "name")
                        .values(1, "Standard").values(2, "Battery").build(),
                Operations.insertInto("div_job_exec")
                        .columns("div_job_exec_id", "environment_id", "path_id", "completed", "submit_time_stamp",
                                "complete_time_stamp", "submit_user_id", "priority", "div_job_exec_status_id",
                                "div_job_exec_type_id", "test_type_id")
                        .values(1, 1, 1, 0, timestamp, timestamp, 1, 1, 1, 1, 1)
                        .values(2, 1, 1, 0, timestamp, timestamp, 1, 1, 2, 1, 1).build(),
                Operations.insertInto("scenario_type").columns("scenario_type_id", "scenario_type").values(1, "TN8")
                        .values(2, "EPEN2").build(),
                Operations.insertInto("scenario_class").columns("scenario_class_id", "scenario_class")
                        .values(1, "minPoints").values(2, "maxPoints").build(),
                Operations.insertInto("testmap_detail")
                        .columns("testmap_detail_id", "testmap_id", "testmap_name", "testmap_publish_format",
                                "testmap_version")
                        .values(1, 1234, "TestMap1", "E", 1).build(),
                Operations.insertInto("scenario")
                        .columns("scenario_id", "scenario_type_id", "scenario", "scenario_name", "test_Code",
                                "form_code", "scenario_class_id", "testmap_detail_id", "previewer_tenant_id")
                        .values(1, 1, "{\"scenario\": 1}", "minPoints1", "tc1", "fc1", 1, 1, 1)
                        .values(2, 2, "{\"scenario\": 2}", "maxPoints1", "tc1", "fc1", 2, 1, null)
                        .values(3, 1, "{\"scenario\": 2}", "scenario1", "tc1", "fc1", 1, 1, 1)
                        .values(4, 1, "{\"scenario\": 1}", "scenario1", "tc1", "fc1", 1, 1, 1)
                        .values(5, 2, "{\"scenario\": 2}", "scenario2", "tc2", "fc2", 2, 1, null)
                        .values(6, 2, "{\"scenario\": 1}", "scenario2", "tc2", "fc2", 2, 1, null)
                        .build(),
                Operations.insertInto("test_session")
                        .columns("test_session_id", "testnav_application_url", "testnav_webservice_url",
                                "testnav_client_secret", "testnav_client_identifier", "testnav_version",
                                "testnav_customer_code", "scope_code", "session_seal_codes", "session_name",
                                "pan_test_session_id", "environment_id")
                        .values(1, "http://www.testnav.com", "http://www.testnav-api.com", "vm6TTJHmq+wZ9PXQuhFO+w==",
                                "identifier", "8.2", "pcc", "standard", "sc1,sc2", "testSession1", "1", 1)
                        .build(),
                Operations.insertInto("previewer")
                        .columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true)
                        .build(),
                Operations.insertInto("student_scenario_status").columns("student_scenario_status_id", "status")
                        .values(1, "Not Started").values(2, "In Progress").values(3, "Failed").values(4, "Completed")
                        .build(),
                Operations.insertInto("student_scenario")
                        .columns("student_scenario_id", "uuid", "testnav_login_name", "testnav_login_password",
                                "wf_db_connection_id", "div_job_exec_id", "testnav_scenario_id", "epen_scenario_id",
                                "last_completed_state_id", "student_scenario_status_id", "test_session_id")
                        .values(1, "a1f27d1b-3b5a-48a3-a0cb-ce9e728d37f5", "4520549168", "b371b5", 7, 1, 1, 2, 1, 1, 1)
                        .build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        scenarioService = app.injector().instanceOf(ScenarioService.class);
    }

    @Test
    public void findTestNavScenarioByStudentScenarioId_scenarioExists_returnScenario() throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findTestNavScenarioByStudentScenarioId(1, 1, 1));

        // Then
        Scenario scenario = scenarioOptional.get();
        assertThat("The scenario should have the correct ID", scenario.getScenarioId(), equalTo(1));
        assertThat("The scenario should have the correct scenario contents", scenario.getScenario(),
                equalTo("{\"scenario\": 1}"));
        assertThat("The scenario should have the correct name", scenario.getScenarioName(), equalTo("minPoints1"));
        assertThat("The scenario should have the correct test code", scenario.getTestCode(), equalTo("tc1"));
        assertThat("The scenario should have the correct form code", scenario.getFormCode(), equalTo("fc1"));
        assertThat("The scenario should have the correct test map", scenario.getTestmapDetail().getTestmapId(),
                equalTo(1234));
        assertThat("The scenario should have the correct previewer url",
                scenario.getPreviewerTenant().getPreviewer().getUrl(), equalTo("previewer1.com"));
        assertThat("The scenario should have the correct tenant ID", scenario.getPreviewerTenant().getTenantId(),
                equalTo(1L));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findTestNavScenarioByStudentScenarioId_incorrectEnvironmentId_returnEmptyOptional() throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findTestNavScenarioByStudentScenarioId(2, 1, 1));

        // Then
        assertThat("The optional should be empty", scenarioOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findTestNavScenarioByStudentScenarioId_incorrectDivJobExecId_returnEmptyOptional() throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findTestNavScenarioByStudentScenarioId(1, 2, 1));

        // Then
        assertThat("The optional should be empty", scenarioOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findTestNavScenarioByStudentScenarioId_incorrectStudentScenarioId_returnEmptyOptional()
            throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findTestNavScenarioByStudentScenarioId(1, 1, 2));

        // Then
        assertThat("The optional should be empty", scenarioOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findEpenScenarioByStudentScenarioId_scenarioExists_returnScenario() throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findEpenScenarioByStudentScenarioId(1, 1, 1));

        // Then
        Scenario scenario = scenarioOptional.get();
        assertThat("The scenario should have the correct ID", scenario.getScenarioId(), equalTo(2));
        assertThat("The scenario should have the correct scenario contents", scenario.getScenario(),
                equalTo("{\"scenario\": 2}"));
        assertThat("The scenario should have the correct name", scenario.getScenarioName(), equalTo("maxPoints1"));
        assertThat("The scenario should have the correct test code", scenario.getTestCode(), equalTo("tc1"));
        assertThat("The scenario should have the correct form code", scenario.getFormCode(), equalTo("fc1"));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findEpenScenarioByStudentScenarioId_incorrectEnvironmentId_returnEmptyOptional() throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findEpenScenarioByStudentScenarioId(2, 1, 1));

        // Then
        assertThat("The optional should be empty", scenarioOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findEpenScenarioByStudentScenarioId_incorrectDivJobExecId_returnEmptyOptional() throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findEpenScenarioByStudentScenarioId(1, 2, 1));

        // Then
        assertThat("The optional should be empty", scenarioOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findEpenScenarioByStudentScenarioId_incorrectStudentScenarioId_returnEmptyOptional()
            throws Throwable {
        // When
        Optional<Scenario> scenarioOptional = jpaApi
                .withTransaction(() -> scenarioService.findEpenScenarioByStudentScenarioId(1, 1, 2));

        // Then
        assertThat("The optional should be empty", scenarioOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_isActiveWithNoPreviousVersions_returnTrue() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 1,
                "minPoints1", 1234, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be true", result, equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_isActiveWithPreviousVersions_returnTrue() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 1,
                "scenario1", 1234, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be true", result, equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_scenarioStringOfActiveDoesNotMatch_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 1,
                "scenario1", 1234, 1, "{\"scenario\": 2}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_previewerTenantIdOfActiveDoesNotMatch_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 1,
                "scenario1", 1234, 1, "{\"scenario\": 1}", 2L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_testMapIdOfActiveDoesNotMatch_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 1,
                "scenario1", 1235, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_testMapVersionOfActiveDoesNotMatch_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 1,
                "scenario1", 1234, 2, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_wrongScenarioTypeId_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 2, 1,
                "scenario1", 1234, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_wrongScenarioClassId_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 2,
                "scenario1", 1234, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_wrongScenarioName_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc1", 1, 1,
                "scenario2", 1234, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_wrongTestCode_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc2", "fc1", 1, 1,
                "scenario1", 1234, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveTestNavScenario_wrongFormCode_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario("tc1", "fc2", 1, 1,
                "scenario1", 1234, 1, "{\"scenario\": 1}", 1L));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_isActiveWithNoPreviousVersions_returnTrue() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc1", "fc1", 2, 2, "maxPoints1", "{\"scenario\": 2}"));

        // Then
        assertThat("The result should be true", result, equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_isActiveWithPreviousVersions_returnTrue() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc2", "fc2", 2, 2, "scenario2", "{\"scenario\": 1}"));

        // Then
        assertThat("The result should be true", result, equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_scenarioStringOfActiveDoesNotMatch_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc2", "fc2", 2, 2, "scenario2", "{\"scenario\": 2}"));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_wrongScenarioTypeId_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc2", "fc2", 1, 2, "scenario2", "{\"scenario\": 1}"));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_wrongScenarioClassId_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc2", "fc2", 2, 1, "scenario2", "{\"scenario\": 1}"));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_wrongScenarioName_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc2", "fc2", 2, 2, "scenario1", "{\"scenario\": 1}"));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_wrongTestCode_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc1", "fc2", 2, 2, "scenario2", "{\"scenario\": 1}"));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void isActiveScenario_wrongFormCode_returnFalse() throws Throwable {
        // When
        boolean result = jpaApi.withTransaction(
                () -> scenarioService.isActiveScenario("tc2", "fc1", 2, 2, "scenario2", "{\"scenario\": 1}"));

        // Then
        assertThat("The result should be false", result, equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }
}
