/**
 * 
 */
package services.systemvalidation;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import global.TestWithApplication;
import models.systemvalidation.ExternalDefectRepository;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

/**
 * @author Gert Selis
 *
 */
public class ExternalDefectRepositoryServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;

    @Before
    public void setUp() {
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,

                Operations.insertInto("external_defect_repository").columns("external_defect_repository_id", "name")
                        .values(1, "Repository 1").values(2, "Repository 2").values(3, "Repository 3").build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
    }

    @Test
    public void findAll() {
        // Given
        ExternalDefectRepositoryService edrs = app.injector().instanceOf(ExternalDefectRepositoryService.class);

        // When
        List<ExternalDefectRepository> defectRepos = jpaApi.withTransaction(() -> edrs.findAll());

        // Then
        assertThat("The size of the returned list should be 3", defectRepos.size(), equalTo(3));

        dbSetupTracker.skipNextLaunch();

    }
}
