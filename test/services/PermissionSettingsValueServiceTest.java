package services;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.PermissionSettingsValue;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;

import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PermissionSettingsValueServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;

    @Before
    public void setUp() throws Exception {
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("permission_type")
                        .columns("permission_type_id", "permission_type_name")
                        .values(1, "permissionType1")
                        .build(),
                Operations.insertInto("application_module")
                        .columns("application_module_id", "application_module_name", "permission_type_id")
                        .values(1, "module1", 1)
                        .build(),
                Operations.insertInto("permission_option")
                        .columns("permission_option_id", "name", "application_module_id", "create_access", "read_access", "update_access", "delete_access")
                        .values(1, "feature1", 1, 1, 1, 1, 1)
                        .values(2, "feature2", 1, 1, 1, 1, 1)
                        .build(),
                Operations.insertInto("permission_settings")
                        .columns("permission_settings_id", "name", "application_module_id")
                        .values(1, "permissions1", 1)
                        .values(2, "permissions2", 1)
                        .build(),
                Operations.insertInto("permission_settings_value")
                        .columns("permission_settings_value_id", "permission_settings_id", "permission_option_id", "create_access", "read_access", "update_access", "delete_access")
                        .values(1, 1, 1, 1, 1, 0, 0)
                        .values(2, 1, 2, 0, 0, 1, 1)
                        .values(3, 2, 1, 0, 1, 0, 0)
                        .values(4, 2, 2, 0, 1, 0, 0)
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
    }

    @Test
    public void findAllByPermissionSettingsId_valuesExistForId_returnMapOfValues() throws Throwable {
        // Given
        PermissionSettingsValueService permissionSettingsValueService = app.injector()
                .instanceOf(PermissionSettingsValueService.class);

        // When
        Map<String, PermissionSettingsValue> valueMap = jpaApi.withTransaction(() ->
                permissionSettingsValueService.findAllByPermissionSettingsId(1L));

        // Then
        assertThat("The returned map should have a size of 2", valueMap.size(), equalTo(2));
        assertThat("The value for feature1 in the map should have create access",
                valueMap.get("feature1").getCreateAccess(), equalTo(true));
        assertThat("The value for feature1 in the map should have read access",
                valueMap.get("feature1").getReadAccess(), equalTo(true));
        assertThat("The value for feature1 in the map should not have update access",
                valueMap.get("feature1").getUpdateAccess(), equalTo(false));
        assertThat("The value for feature1 in the map should not have delete access",
                valueMap.get("feature1").getDeleteAccess(), equalTo(false));
        assertThat("The value for feature2 in the map should not have create access",
                valueMap.get("feature2").getCreateAccess(), equalTo(false));
        assertThat("The value for feature2 in the map should not have read access",
                valueMap.get("feature2").getReadAccess(), equalTo(false));
        assertThat("The value for feature2 in the map should have update access",
                valueMap.get("feature2").getUpdateAccess(), equalTo(true));
        assertThat("The value for feature2 in the map should have delete access",
                valueMap.get("feature2").getDeleteAccess(), equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByPermissionSettingsId_noValuesExistForId_returnEmptyMap() throws Throwable {
        // Given
        PermissionSettingsValueService permissionSettingsValueService = app.injector()
                .instanceOf(PermissionSettingsValueService.class);

        // When
        Map<String, PermissionSettingsValue> valueMap = jpaApi.withTransaction(() ->
                permissionSettingsValueService.findAllByPermissionSettingsId(3L));

        // Then
        assertThat("The returned map should have a size of 2", valueMap.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }
}
