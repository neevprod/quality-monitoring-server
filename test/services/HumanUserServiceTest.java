package services;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import global.TestWithApplication;
import models.HumanUser;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.usermanagement.HumanUserService;

public class HumanUserServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;

    @Before
    public void setUp() {
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("user")
                        .columns("id", "fullname", "isAdmin", "is_api_user")
                        .values(1, "Tester Testington", 0, 0)
                        .values(2, "John Doe", 1, 0)
                        .build(),
                Operations.insertInto("human_user")
                        .columns("human_user_id", "user_id", "email", "network_id")
                        .values(1, 1, "tester@test.com", "tester")
                        .values(2, 2, "someone@test.com", "someone")
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
    }

    @Test
    public void findByEmail_givenEmailExists_returnCorrectUser() throws Throwable {
        // Given
        HumanUserService userService = app.injector().instanceOf(HumanUserService.class);

        // When
        Optional<HumanUser> humanUserOptional =
                jpaApi.withTransaction(() -> userService.findByNetworkId("tester"));

        // Then
        assertThat("The ID of the returned human user should be as expected",
                humanUserOptional.get().getHumanUserId(), equalTo(1L));
        assertThat("The email of the returned human user should be as expected",
                humanUserOptional.get().getEmail(), equalTo("tester@test.com"));
        assertThat("The name of the returned user should be as expected",
                humanUserOptional.get().getUser().getFullname(), equalTo("Tester Testington"));
        assertThat("The 'is admin' field of the returned user should be as expected",
                humanUserOptional.get().getUser().getIsAdmin(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByEmail_givenEmailDoesNotExist_returnNull() throws Throwable {
        // Given
        HumanUserService userService = app.injector().instanceOf(HumanUserService.class);

        // When
        Optional<HumanUser> humanUserOptional =
                jpaApi.withTransaction(() -> userService.findByNetworkId("invalidUser"));

        // Then
        assertThat("The result should be empty", humanUserOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

}