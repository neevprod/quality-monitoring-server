package services;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import global.TestWithApplication;
import models.ApplicationSetting;
import play.db.Database;
import play.db.jpa.JPAApi;

public class ApplicationSettingServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private ApplicationSettingService applicationSettingService;

    @Before
    public void setUp() {
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("application_setting")
                        .columns("application_setting_id", "application_setting_name", "application_setting_value")
                        .values(1, "setting_1", "123")
                        .values(2, "setting_2", "something")
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
        applicationSettingService = app.injector().instanceOf(ApplicationSettingService.class);
    }

    @Test
    public void findByApplicationSettingName_settingExists_returnSetting() {
        // When
        Optional<ApplicationSetting> applicationSettingOptional =
                jpaApi.withTransaction(() -> applicationSettingService.findByApplicationSettingName("setting_1"));

        // Then
        assertThat("The ID of the returned application setting should be as expected",
                applicationSettingOptional.get().getApplicationSettingId(), equalTo(1));
        assertThat("The name of the returned application setting should be as expected",
                applicationSettingOptional.get().getApplicationSettingName(), equalTo("setting_1"));
        assertThat("The value of the returned application setting should be as expected",
                applicationSettingOptional.get().getApplicationSettingValue(), equalTo("123"));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByApplicationSettingName_settingDoesNotExist_returnEmptyOptional() {
        // When
        Optional<ApplicationSetting> applicationSettingOptional =
                jpaApi.withTransaction(() -> applicationSettingService.findByApplicationSettingName("setting_3"));

        // Then
        assertThat("The returned Optional should be empty",
                applicationSettingOptional.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }
}
