package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.HistItem;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class HistItemServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private HistItemService histItemService;

    @Before
    public void setUp() throws Exception {
        Timestamp timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("previewer")
                        .columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true)
                        .values(2, 1, 2, "tenant2", true)
                        .build(),
                Operations.insertInto("hist_exec")
                        .columns("hist_exec_id", "previewer_id", "exec_start", "exec_stop", "exec_completed",
                                "tenant_level_exec")
                        .values(1, 1, timestamp, timestamp, 1, 0)
                        .values(2, 1, timestamp, timestamp, 0, 1)
                        .build(),
                Operations.insertInto("hist_status")
                        .columns("hist_status_id", "name")
                        .values(4, "REACTIVATED")
                        .build(),
                Operations.insertInto("hist_tenant")
                        .columns("hist_tenant_id", "hist_exec_id", "previewer_tenant_id", "exec_start", "exec_stop",
                                "exec_completed", "hist_status_id", "items_validated", "items_with_failed_tcs",
                                "items_with_pending_tcs", "deactivated_items", "item_exceptions")
                        .values(1, 1, 1, timestamp, timestamp, 0, 4, 3, 0, 3, 0, 0)
                        .values(2, 2, 2, timestamp, timestamp, 1, 4, 6, 2, 1, 5, 0)
                        .build(),
                Operations.insertInto("previewer_tenant_item")
                        .columns("previewer_tenant_item_id", "previewer_tenant_id", "item_id", "item_identifier",
                                "active", "last_update")
                        .values(1, 1, 1, "item1", true, timestamp)
                        .values(2, 1, 2, "item2", true, timestamp)
                        .values(3, 2, 3, "item3", true, timestamp)
                        .values(4, 2, 4, "item4", true, timestamp)
                        .build(),
                Operations.insertInto("previewer_tenant_item_xml")
                        .columns("previewer_tenant_item_xml_id", "previewer_tenant_item_id", "is_valid",
                                "item_xml_version", "last_update")
                        .values(1, 1, true, 0, timestamp)
                        .values(2, 2, true, 0, timestamp)
                        .values(3, 3, true, 0, timestamp)
                        .values(4, 4, true, 0, timestamp)
                        .build(),
                Operations.insertInto("hist_item")
                        .columns("hist_item_id", "hist_tenant_id", "previewer_tenant_item_id",
                                "previewer_tenant_item_xml_id", "hist_status_id", "test_cases", "failed_test_cases",
                                "pending_test_cases")
                        .values(1, 1, 1, 1, 4, 3, 5, 7)
                        .values(2, 1, 2, 2, 4, 1, 8, 1)
                        .values(3, 2, 3, 3, 4, 2, 2, 3)
                        .values(4, 2, 4, 4, 4, 0, 3, 2)
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        histItemService = app.injector().instanceOf(HistItemService.class);
    }

    @Test
    public void findAllByHistTenantId_validIdGiven_returnHistItems() throws Throwable {
        // When
        List<HistItem> resultList = jpaApi.withTransaction(() -> histItemService.findAllByHistTenantId(1));

        // Then
        assertThat("The returned list should have a length of 1", resultList.size(), equalTo(2));

        assertThat("The ID of the first HistItem in the list should be as expected",
                resultList.get(0).getHistItemId(), equalTo(1));
        assertThat("The HistTenant ID of the first HistItem in the list should be as expected",
                resultList.get(0).getHistTenant().getHistTenantId(), equalTo(1));
        assertThat("The PreviewerTenant Item ID of the first HistItem in the list should be as expected",
                resultList.get(0).getPreviewerTenantItem().getItemId(), equalTo(1L));
        assertThat("The number of test cases for the first HistItem in the list should be as expected",
                resultList.get(0).getTestCases(), equalTo(3));
        assertThat("The number of failed test cases for the first HistItem in the list should be as expected",
                resultList.get(0).getFailedTestCases(), equalTo(5));
        assertThat("The number of pending test cases for the first HistItem in the list should be as expected",
                resultList.get(0).getPendingTestCases(), equalTo(7));

        assertThat("The ID of the second HistItem in the list should be as expected",
                resultList.get(1).getHistItemId(), equalTo(2));
        assertThat("The HistTenant ID of the second HistItem in the list should be as expected",
                resultList.get(1).getHistTenant().getHistTenantId(), equalTo(1));
        assertThat("The PreviewerTenant Item ID of the second HistItem in the list should be as expected",
                resultList.get(1).getPreviewerTenantItem().getItemId(), equalTo(2L));
        assertThat("The number of test cases for the second HistItem in the list should be as expected",
                resultList.get(1).getTestCases(), equalTo(1));
        assertThat("The number of failed test cases for the second HistItem in the list should be as expected",
                resultList.get(1).getFailedTestCases(), equalTo(8));
        assertThat("The number of pending test cases for the second HistItem in the list should be as expected",
                resultList.get(1).getPendingTestCases(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistTenantId_invalidIdGiven_returnEmptyList() throws Throwable {
        // When
        List<HistItem> resultList = jpaApi.withTransaction(() -> histItemService.findAllByHistTenantId(3));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistTenantId_validIdGivenWithSameTenant_returnHistItems() throws Throwable {
        // When
        List<HistItem> resultList = jpaApi.withTransaction(() -> histItemService.findAllByHistTenantId(1, 1, 1L));

        // Then
        assertThat("The returned list should have a length of 1", resultList.size(), equalTo(2));

        assertThat("The ID of the first HistItem in the list should be as expected",
                resultList.get(0).getHistItemId(), equalTo(1));
        assertThat("The HistTenant ID of the first HistItem in the list should be as expected",
                resultList.get(0).getHistTenant().getHistTenantId(), equalTo(1));
        assertThat("The PreviewerTenant Item ID of the first HistItem in the list should be as expected",
                resultList.get(0).getPreviewerTenantItem().getItemId(), equalTo(1L));
        assertThat("The number of test cases for the first HistItem in the list should be as expected",
                resultList.get(0).getTestCases(), equalTo(3));
        assertThat("The number of failed test cases for the first HistItem in the list should be as expected",
                resultList.get(0).getFailedTestCases(), equalTo(5));
        assertThat("The number of pending test cases for the first HistItem in the list should be as expected",
                resultList.get(0).getPendingTestCases(), equalTo(7));

        assertThat("The ID of the second HistItem in the list should be as expected",
                resultList.get(1).getHistItemId(), equalTo(2));
        assertThat("The HistTenant ID of the second HistItem in the list should be as expected",
                resultList.get(1).getHistTenant().getHistTenantId(), equalTo(1));
        assertThat("The PreviewerTenant Item ID of the second HistItem in the list should be as expected",
                resultList.get(1).getPreviewerTenantItem().getItemId(), equalTo(2L));
        assertThat("The number of test cases for the second HistItem in the list should be as expected",
                resultList.get(1).getTestCases(), equalTo(1));
        assertThat("The number of failed test cases for the second HistItem in the list should be as expected",
                resultList.get(1).getFailedTestCases(), equalTo(8));
        assertThat("The number of pending test cases for the second HistItem in the list should be as expected",
                resultList.get(1).getPendingTestCases(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistTenantId_validIdGivenWithDifferentTenant_returnEmptyList() throws Throwable {
        // When
        List<HistItem> resultList = jpaApi.withTransaction(() -> histItemService.findAllByHistTenantId(1, 1, 2L));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistTenantId_invalidIdGivenWithTenant_returnEmptyList() throws Throwable {
        // When
        List<HistItem> resultList = jpaApi.withTransaction(() -> histItemService.findAllByHistTenantId(3, 1, 1L));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

}
