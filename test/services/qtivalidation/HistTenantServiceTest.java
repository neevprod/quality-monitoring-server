package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.HistTenant;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class HistTenantServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private HistTenantService histTenantService;

    @Before
    public void setUp() throws Exception {
        Timestamp timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("previewer")
                        .columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true)
                        .values(2, 1, 2, "tenant2", true)
                        .build(),
                Operations.insertInto("hist_exec")
                        .columns("hist_exec_id", "previewer_id", "exec_start", "exec_stop", "exec_completed",
                                "tenant_level_exec")
                        .values(1, 1, timestamp, timestamp, 1, 0)
                        .values(2, 1, timestamp, timestamp, 0, 1)
                        .values(3, 1, timestamp, timestamp, 1, 0)
                        .values(4, 1, timestamp, timestamp, 1, 1)
                        .values(5, 1, timestamp, timestamp, 0, 0)
                        .build(),
                Operations.insertInto("hist_status")
                        .columns("hist_status_id", "name")
                        .values(4, "REACTIVATED")
                        .build(),
                Operations.insertInto("hist_tenant")
                        .columns("hist_tenant_id", "hist_exec_id", "previewer_tenant_id", "exec_start", "exec_stop",
                                "exec_completed", "hist_status_id", "items_validated", "items_with_failed_tcs",
                                "items_with_pending_tcs", "deactivated_items", "item_exceptions")
                        .values(1, 1, 2, timestamp, timestamp, 1, 4, 6, 2, 1, 5, 0)
                        .values(2, 3, 1, timestamp, timestamp, 0, 4, 3, 0, 3, 0, 0)
                        .values(3, 2, 2, timestamp, timestamp, 1, 4, 4, 1, 0, 0, 3)
                        .values(4, 4, 1, timestamp, timestamp, 1, 4, 2, 8, 3, 3, 0)
                        .values(5, 1, 1, timestamp, timestamp, 1, 4, 9, 7, 2, 2, 1)
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        histTenantService = app.injector().instanceOf(HistTenantService.class);
    }

    @Test
    public void find_validIdsGiven_returnHistTenantOptional() throws Throwable {
        // When
        Optional<HistTenant> resultOptional = jpaApi.withTransaction(() -> histTenantService.find(2, 3));

        // Then
        HistTenant result = resultOptional.get();
        assertThat("The ID of the returned HistTenant should be as expected",
                result.getHistTenantId(), equalTo(2));
        assertThat("The HistExec ID of the returned HistTenant should be as expected",
                result.getHistExec().getHistExecId(), equalTo(3));
        assertThat("The PreviewerTenant ID of the returned HistTenant should be as expected",
                result.getPreviewerTenant().getPreviewerTenantId(), equalTo(1L));
        assertThat("The ExecCompleted value of the returned HistTenant should be as expected",
                result.getExecCompleted(), equalTo(false));
        assertThat("The items validated value of the returned HistTenant should be as expected",
                result.getItemsValidated(), equalTo(3));
        assertThat("The items with failed test cases value of the returned HistTenant should be as expected",
                result.getItemsWithFailedTcs(), equalTo(0));
        assertThat("The items with pending test cases value of the returned HistTenant should be as expected",
                result.getItemsWithPendingTcs(), equalTo(3));
        assertThat("The deactivated items value of the returned HistTenant should be as expected",
                result.getDeactivatedItems(), equalTo(0));
        assertThat("The item exceptions value of the returned HistTenant should be as expected",
                result.getItemExceptions(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_invalidHistTenantIdGiven_returnEmptyOptional() throws Throwable {
        // When
        Optional<HistTenant> resultOptional = jpaApi.withTransaction(() -> histTenantService.find(6, 3));

        // Then
        assertThat("The returned Optional should be empty", resultOptional, equalTo(Optional.empty()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_invalidHistExecIdGiven_returnEmptyOptional() throws Throwable {
        // When
        Optional<HistTenant> resultOptional = jpaApi.withTransaction(() -> histTenantService.find(2, 1));

        // Then
        assertThat("The returned Optional should be empty", resultOptional, equalTo(Optional.empty()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_validIdsGivenWithMatchingTenant_returnHistTenantOptional() throws Throwable {
        // Given
        int previewerId = 1;
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(1L);

        // When
        Optional<HistTenant> resultOptional =
                jpaApi.withTransaction(() -> histTenantService.find(2, 3, previewerId, tenantIds));

        // Then
        HistTenant result = resultOptional.get();
        assertThat("The ID of the returned HistTenant should be as expected",
                result.getHistTenantId(), equalTo(2));
        assertThat("The HistExec ID of the returned HistTenant should be as expected",
                result.getHistExec().getHistExecId(), equalTo(3));
        assertThat("The PreviewerTenant ID of the returned HistTenant should be as expected",
                result.getPreviewerTenant().getPreviewerTenantId(), equalTo(1L));
        assertThat("The ExecCompleted value of the returned HistTenant should be as expected",
                result.getExecCompleted(), equalTo(false));
        assertThat("The items validated value of the returned HistTenant should be as expected",
                result.getItemsValidated(), equalTo(3));
        assertThat("The items with failed test cases value of the returned HistTenant should be as expected",
                result.getItemsWithFailedTcs(), equalTo(0));
        assertThat("The items with pending test cases value of the returned HistTenant should be as expected",
                result.getItemsWithPendingTcs(), equalTo(3));
        assertThat("The deactivated items value of the returned HistTenant should be as expected",
                result.getDeactivatedItems(), equalTo(0));
        assertThat("The item exceptions value of the returned HistTenant should be as expected",
                result.getItemExceptions(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_validIdsGivenWithNonMatchingTenants_returnEmptyOptional() throws Throwable {
        // Given
        int previewerId = 1;
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);
        tenantIds.add(3L);

        // When
        Optional<HistTenant> resultOptional =
                jpaApi.withTransaction(() -> histTenantService.find(2, 3, previewerId, tenantIds));

        // Then
        assertThat("The returned Optional should be empty", resultOptional, equalTo(Optional.empty()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_invalidHistTenantIdGivenWithTenants_returnEmptyOptional() throws Throwable {
        // Given
        int previewerId = 1;
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(1L);
        tenantIds.add(2L);

        // When
        Optional<HistTenant> resultOptional =
                jpaApi.withTransaction(() -> histTenantService.find(6, 3, previewerId, tenantIds));

        // Then
        assertThat("The returned Optional should be empty", resultOptional, equalTo(Optional.empty()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_invalidHistExecIdGivenWithMatchingTenant_returnEmptyOptional() throws Throwable {
        // Given
        int previewerId = 1;
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(1L);

        // When
        Optional<HistTenant> resultOptional =
                jpaApi.withTransaction(() -> histTenantService.find(2, 1, previewerId, tenantIds));

        // Then
        assertThat("The returned Optional should be empty", resultOptional, equalTo(Optional.empty()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_validIdGiven_returnHistTenants() throws Throwable {
        // When
        List<HistTenant> resultList = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(2));

        // Then
        assertThat("The returned list should have a length of 1", resultList.size(), equalTo(1));
        assertThat("The ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistTenantId(), equalTo(3));
        assertThat("The HistExec ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistExec().getHistExecId(), equalTo(2));
        assertThat("The PreviewerTenant ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getPreviewerTenant().getPreviewerTenantId(), equalTo(2L));
        assertThat("The ExecCompleted value of the first HistTenant in the list should be as expected",
                resultList.get(0).getExecCompleted(), equalTo(true));
        assertThat("The items validated value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsValidated(), equalTo(4));
        assertThat("The items with failed test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithFailedTcs(), equalTo(1));
        assertThat("The items with pending test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithPendingTcs(), equalTo(0));
        assertThat("The deactivated items value of the first HistTenant in the list should be as expected",
                resultList.get(0).getDeactivatedItems(), equalTo(0));
        assertThat("The item exceptions value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemExceptions(), equalTo(3));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_invalidIdGiven_returnEmptyList() throws Throwable {
        // When
        List<HistTenant> resultList = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(7));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_validIdGivenWithMatchingTenants_returnHistTenants() throws Throwable {
        //Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(1L);
        tenantIds.add(2L);

        // When
        List<HistTenant> resultList =
                jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(1, 1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistTenantId(), equalTo(1));
        assertThat("The HistExec ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistExec().getHistExecId(), equalTo(1));
        assertThat("The PreviewerTenant ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getPreviewerTenant().getPreviewerTenantId(), equalTo(2L));
        assertThat("The ExecCompleted value of the first HistTenant in the list should be as expected",
                resultList.get(0).getExecCompleted(), equalTo(true));
        assertThat("The items validated value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsValidated(), equalTo(6));
        assertThat("The items with failed test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithFailedTcs(), equalTo(2));
        assertThat("The items with pending test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithPendingTcs(), equalTo(1));
        assertThat("The deactivated items value of the first HistTenant in the list should be as expected",
                resultList.get(0).getDeactivatedItems(), equalTo(5));
        assertThat("The item exceptions value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemExceptions(), equalTo(0));

        assertThat("The ID of the second HistTenant in the list should be as expected",
                resultList.get(1).getHistTenantId(), equalTo(5));
        assertThat("The HistExec ID of the second HistTenant in the list should be as expected",
                resultList.get(1).getHistExec().getHistExecId(), equalTo(1));
        assertThat("The PreviewerTenant ID of the second HistTenant in the list should be as expected",
                resultList.get(1).getPreviewerTenant().getPreviewerTenantId(), equalTo(1L));
        assertThat("The ExecCompleted value of the second HistTenant in the list should be as expected",
                resultList.get(1).getExecCompleted(), equalTo(true));
        assertThat("The items validated value of the second HistTenant in the list should be as expected",
                resultList.get(1).getItemsValidated(), equalTo(9));
        assertThat("The items with failed test cases value of the second HistTenant in the list should be as expected",
                resultList.get(1).getItemsWithFailedTcs(), equalTo(7));
        assertThat("The items with pending test cases value of the second HistTenant in the list should be as expected",
                resultList.get(1).getItemsWithPendingTcs(), equalTo(2));
        assertThat("The deactivated items value of the second HistTenant in the list should be as expected",
                resultList.get(1).getDeactivatedItems(), equalTo(2));
        assertThat("The item exceptions value of the second HistTenant in the list should be as expected",
                resultList.get(1).getItemExceptions(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_validIdGivenWithSomeMatchingTenants_returnHistTenantsThatMatchTenants()
            throws Throwable {
        //Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(2L);

        // When
        List<HistTenant> resultList =
                jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(1, 1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 1", resultList.size(), equalTo(1));

        assertThat("The ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistTenantId(), equalTo(1));
        assertThat("The HistExec ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistExec().getHistExecId(), equalTo(1));
        assertThat("The PreviewerTenant ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getPreviewerTenant().getPreviewerTenantId(), equalTo(2L));
        assertThat("The ExecCompleted value of the first HistTenant in the list should be as expected",
                resultList.get(0).getExecCompleted(), equalTo(true));
        assertThat("The items validated value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsValidated(), equalTo(6));
        assertThat("The items with failed test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithFailedTcs(), equalTo(2));
        assertThat("The items with pending test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithPendingTcs(), equalTo(1));
        assertThat("The deactivated items value of the first HistTenant in the list should be as expected",
                resultList.get(0).getDeactivatedItems(), equalTo(5));
        assertThat("The item exceptions value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemExceptions(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_validIdGivenWithNonMatchingTenants_returnEmptyList() throws Throwable {
        //Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(3L);
        tenantIds.add(4L);

        // When
        List<HistTenant> resultList =
                jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(1, 1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_validIdAndTenantsGivenWithWrongPreviewer_returnEmptyList() throws Throwable {
        //Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(1L);
        tenantIds.add(2L);

        // When
        List<HistTenant> resultList =
                jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(1, 2, tenantIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_invalidIdGivenWithTenants_returnEmptyList() throws Throwable {
        //Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(1L);
        tenantIds.add(2L);

        // When
        List<HistTenant> resultList =
                jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(7, 1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecId_emptyTenantsSet_returnEmptyList() throws Throwable {
        //Given
        Set<Long> tenantIds = new HashSet<>();

        // When
        List<HistTenant> resultList =
                jpaApi.withTransaction(() -> histTenantService.findAllByHistExecId(1, 1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecIds_singleIdGiven_returnHistTenants() throws Throwable {
        // Given
        Set<Integer> histExecIds = new HashSet<>();
        histExecIds.add(2);

        // When
        List<HistTenant> resultList = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecIds(histExecIds));

        // Then
        assertThat("The returned list should have a length of 1", resultList.size(), equalTo(1));
        assertThat("The ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistTenantId(), equalTo(3));
        assertThat("The HistExec ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistExec().getHistExecId(), equalTo(2));
        assertThat("The PreviewerTenant ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getPreviewerTenant().getPreviewerTenantId(), equalTo(2L));
        assertThat("The ExecCompleted value of the first HistTenant in the list should be as expected",
                resultList.get(0).getExecCompleted(), equalTo(true));
        assertThat("The items validated value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsValidated(), equalTo(4));
        assertThat("The items with failed test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithFailedTcs(), equalTo(1));
        assertThat("The items with pending test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithPendingTcs(), equalTo(0));
        assertThat("The deactivated items value of the first HistTenant in the list should be as expected",
                resultList.get(0).getDeactivatedItems(), equalTo(0));
        assertThat("The item exceptions value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemExceptions(), equalTo(3));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecIds_multipleIdsGiven_returnHistTenants() throws Throwable {
        // Given
        Set<Integer> histExecIds = new HashSet<>();
        histExecIds.add(1);
        histExecIds.add(3);

        // When
        List<HistTenant> resultList = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecIds(histExecIds));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistTenantId(), equalTo(1));
        assertThat("The HistExec ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getHistExec().getHistExecId(), equalTo(1));
        assertThat("The PreviewerTenant ID of the first HistTenant in the list should be as expected",
                resultList.get(0).getPreviewerTenant().getPreviewerTenantId(), equalTo(2L));
        assertThat("The ExecCompleted value of the first HistTenant in the list should be as expected",
                resultList.get(0).getExecCompleted(), equalTo(true));
        assertThat("The items validated value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsValidated(), equalTo(6));
        assertThat("The items with failed test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithFailedTcs(), equalTo(2));
        assertThat("The items with pending test cases value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemsWithPendingTcs(), equalTo(1));
        assertThat("The deactivated items value of the first HistTenant in the list should be as expected",
                resultList.get(0).getDeactivatedItems(), equalTo(5));
        assertThat("The item exceptions value of the first HistTenant in the list should be as expected",
                resultList.get(0).getItemExceptions(), equalTo(0));

        assertThat("The ID of the third HistTenant in the list should be as expected",
                resultList.get(1).getHistTenantId(), equalTo(5));
        assertThat("The HistExec ID of the third HistTenant in the list should be as expected",
                resultList.get(1).getHistExec().getHistExecId(), equalTo(1));
        assertThat("The PreviewerTenant ID of the third HistTenant in the list should be as expected",
                resultList.get(1).getPreviewerTenant().getPreviewerTenantId(), equalTo(1L));
        assertThat("The ExecCompleted value of the third HistTenant in the list should be as expected",
                resultList.get(1).getExecCompleted(), equalTo(true));
        assertThat("The items validated value of the third HistTenant in the list should be as expected",
                resultList.get(1).getItemsValidated(), equalTo(9));
        assertThat("The items with failed test cases value of the third HistTenant in the list should be as expected",
                resultList.get(1).getItemsWithFailedTcs(), equalTo(7));
        assertThat("The items with pending test cases value of the third HistTenant in the list should be as expected",
                resultList.get(1).getItemsWithPendingTcs(), equalTo(2));
        assertThat("The deactivated items value of the third HistTenant in the list should be as expected",
                resultList.get(1).getDeactivatedItems(), equalTo(2));
        assertThat("The item exceptions value of the third HistTenant in the list should be as expected",
                resultList.get(1).getItemExceptions(), equalTo(1));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecIds_singleIdWithNoMatchesGiven_returnEmptyList() throws Throwable {
        // Given
        Set<Integer> histExecIds = new HashSet<>();
        histExecIds.add(5);

        // When
        List<HistTenant> resultList = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecIds(histExecIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecIds_multipleIdsWithNoMatchesGiven_returnEmptyList() throws Throwable {
        // Given
        Set<Integer> histExecIds = new HashSet<>();
        histExecIds.add(5);
        histExecIds.add(7);

        // When
        List<HistTenant> resultList = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecIds(histExecIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByHistExecIds_noIdsGiven_returnEmptyList() throws Throwable {
        // Given
        Set<Integer> histExecIds = new HashSet<>();

        // When
        List<HistTenant> resultList = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecIds(histExecIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

}
