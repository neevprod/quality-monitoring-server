package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.UserTenantJobSubscription;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class UserTenantJobSubscriptionServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private Timestamp timestamp;
    private JPAApi jpaApi;
    private UserTenantJobSubscriptionService userTenantJobSubscriptionService;

    @Before
    public void setUp() throws Exception {
        timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,
                Operations.insertInto("user").columns("id", "fullname", "isAdmin", "is_api_user")
                        .values(1, "User 1", 1, 0).values(2, "User 2", 0, 0).values(3, "User 3", 0, 0).build(),
                Operations.insertInto("human_user").columns("human_user_id", "user_id", "email", "network_id")
                        .values(1, 1, "user1@pearson.com", "user1").values(2, 2, "user2@pearson.com", "user2")
                        .values(3, 3, "user3@pearson.com", "user3").build(),
                Operations.insertInto("previewer").columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://").build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true).values(2, 1, 2, "tenant2", true)
                        .values(3, 1, 3, "tenant3", true).build(),
                Operations.insertInto("job_type").columns("job_type_id", "code", "name").values(1, "J1", "Job 1")
                        .values(2, "J2", "Job 2").values(3, "J3", "Job 3").values(4, "J4", "Job 4").build(),
                Operations.insertInto("user_tenant_job_subscription")
                        .columns("user_tenant_job_subscription_id", "human_user_id", "previewer_tenant_id",
                                "job_type_id", "last_update")
                        .values(1, 3, 1, 1, timestamp).values(2, 1, 2, 3, timestamp).values(3, 2, 1, 2, timestamp)
                        .values(4, 2, 3, 1, timestamp).values(5, 1, 1, 3, timestamp).build());
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        userTenantJobSubscriptionService = app.injector().instanceOf(UserTenantJobSubscriptionService.class);
    }

    @Test
    public void fetchByJobTypeName_nameWithMatches_returnMatches() throws Throwable {
        // Given
        String jobName = "Job 1";

        // When
        List<UserTenantJobSubscription> resultList = jpaApi
                .withTransaction(() -> userTenantJobSubscriptionService.fetchByJobTypeName(jobName));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the first subscription in the list should be as expected",
                resultList.get(0).getUserTenantJobSubscriptionId(), equalTo(1L));
        assertThat("The human user ID of the first subscription in the list should be as expected",
                resultList.get(0).getHumanUser().getHumanUserId(), equalTo(3L));
        assertThat("The PreviewerTenant ID of the first subscription in the list should be as expected",
                resultList.get(0).getPreviewerTenantId(), equalTo(1L));
        assertThat("The job type ID of the first subscription in the list should be as expected",
                resultList.get(0).getJobType().getJobTypeId(), equalTo(1));
        assertThat("The last updated time of the first subscription in the list should be as expected",
                resultList.get(0).getLastUpdate().getTime(), equalTo(timestamp.getTime()));

        assertThat("The ID of the second subscription in the list should be as expected",
                resultList.get(1).getUserTenantJobSubscriptionId(), equalTo(4L));
        assertThat("The human user ID of the second subscription in the list should be as expected",
                resultList.get(1).getHumanUser().getHumanUserId(), equalTo(2L));
        assertThat("The PreviewerTenant ID of the second subscription in the list should be as expected",
                resultList.get(1).getPreviewerTenantId(), equalTo(3L));
        assertThat("The job type ID of the second subscription in the list should be as expected",
                resultList.get(1).getJobType().getJobTypeId(), equalTo(1));
        assertThat("The last updated time of the second subscription in the list should be as expected",
                resultList.get(1).getLastUpdate().getTime(), equalTo(timestamp.getTime()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void fetchByJobTypeName_existingJobNameWithoutMatches_returnEmptyList() throws Throwable {
        // Given
        String jobName = "Job 4";

        // When
        List<UserTenantJobSubscription> resultList = jpaApi
                .withTransaction(() -> userTenantJobSubscriptionService.fetchByJobTypeName(jobName));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void fetchByJobTypeName_nonexistentJobName_returnEmptyList() throws Throwable {
        // Given
        String jobName = "Job 425";

        // When
        List<UserTenantJobSubscription> resultList = jpaApi
                .withTransaction(() -> userTenantJobSubscriptionService.fetchByJobTypeName(jobName));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void fetchByJobTypeName_emptyString_returnEmptyList() throws Throwable {
        // Given
        String jobName = "";

        // When
        List<UserTenantJobSubscription> resultList = jpaApi
                .withTransaction(() -> userTenantJobSubscriptionService.fetchByJobTypeName(jobName));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByUserIdAndPreviewerId_userIdAndPreviewerIdMatches_returnMatches() throws Throwable {
        // Given
        final Long userId = 1L;
        final Integer previewerId = 1;

        // When
        List<UserTenantJobSubscription> resultList = jpaApi.withTransaction(
                () -> userTenantJobSubscriptionService.findByUserIdAndPreviewerId(userId, previewerId));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the first subscription in the list should be as expected",
                resultList.get(0).getUserTenantJobSubscriptionId(), equalTo(5L));
        assertThat("The human user ID of the first subscription in the list should be as expected",
                resultList.get(0).getHumanUser().getHumanUserId(), equalTo(1L));
        assertThat("The PreviewerTenant ID of the first subscription in the list should be as expected",
                resultList.get(0).getPreviewerTenantId(), equalTo(1L));
        assertThat("The job type ID of the first subscription in the list should be as expected",
                resultList.get(0).getJobType().getJobTypeId(), equalTo(3));
        assertThat("The last updated time of the first subscription in the list should be as expected",
                resultList.get(0).getLastUpdate().getTime(), equalTo(timestamp.getTime()));

        assertThat("The ID of the second subscription in the list should be as expected",
                resultList.get(1).getUserTenantJobSubscriptionId(), equalTo(2L));
        assertThat("The human user ID of the second subscription in the list should be as expected",
                resultList.get(1).getHumanUser().getHumanUserId(), equalTo(1L));
        assertThat("The PreviewerTenant ID of the second subscription in the list should be as expected",
                resultList.get(1).getPreviewerTenantId(), equalTo(2L));
        assertThat("The job type ID of the second subscription in the list should be as expected",
                resultList.get(1).getJobType().getJobTypeId(), equalTo(3));
        assertThat("The last updated time of the second subscription in the list should be as expected",
                resultList.get(1).getLastUpdate().getTime(), equalTo(timestamp.getTime()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByUserIdAndPreviewerId_userIdAndPreviewerIdWithoutMatches_returnEmptyList() throws Throwable {
        // Given
        final Long userId = 5L;
        final Integer previewerId = 10;

        // When
        List<UserTenantJobSubscription> resultList = jpaApi.withTransaction(
                () -> userTenantJobSubscriptionService.findByUserIdAndPreviewerId(userId, previewerId));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByPreviewerIdTenantIdUserIdAndJobTypeId_userIdPreviewerIdTenantIdMatches_returnMatches()
            throws Throwable {

        // Given
        final Long userId = 1L;
        final Integer previewerId = 1;
        final Long tenantId = 1L;
        final int jobTypeId = 3;

        // When
        Optional<UserTenantJobSubscription> result = jpaApi.withTransaction(() -> userTenantJobSubscriptionService
                .findByPreviewerIdTenantIdUserIdAndJobTypeId(previewerId, tenantId, userId, jobTypeId));

        // Then
        assertThat("The result should should present", result.isPresent(), equalTo(true));

        assertThat("The ID of the first subscription in the list should be as expected",
                result.get().getUserTenantJobSubscriptionId(), equalTo(5L));
        assertThat("The human user ID of the first subscription in the list should be as expected",
                result.get().getHumanUser().getHumanUserId(), equalTo(1L));
        assertThat("The PreviewerTenant ID of the first subscription in the list should be as expected",
                result.get().getPreviewerTenantId(), equalTo(1L));
        assertThat("The job type ID of the first subscription in the list should be as expected",
                result.get().getJobType().getJobTypeId(), equalTo(3));
        assertThat("The last updated time of the first subscription in the list should be as expected",
                result.get().getLastUpdate().getTime(), equalTo(timestamp.getTime()));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findByPreviewerIdTenantIdUserIdAndJobTypeId_userIdPreviewerIdTenantIdWithoutMatches_returnEmpty()
            throws Throwable {

        // Given
        final Long userId = 5L;
        final Integer previewerId = 1;
        final Long tenantId = 8L;
        final int jobTypeId = 3;

        // When
        Optional<UserTenantJobSubscription> result = jpaApi.withTransaction(() -> userTenantJobSubscriptionService
                .findByPreviewerIdTenantIdUserIdAndJobTypeId(previewerId, tenantId, userId, jobTypeId));

        // Then
        assertThat("The result should not present ", result.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }
}
