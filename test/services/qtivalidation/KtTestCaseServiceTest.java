package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.KtTestCase;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class KtTestCaseServiceTest extends TestWithApplication {
    private static final DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private KtTestCaseService ktTestCaseService;

    @Before
    public void setUp() {
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,
                Operations.insertInto("kt_test_case").columns("kt_test_case_id", "kt_id", "kt_candidate_response",
                        "kt_response_id", "kt_saved_score", "kt_saved_score_state", "kt_include_in_build",
                        "item_identifier", "response_identifier")
                        .values(1, "kt-id-000000000000", "{\"content\":\"test1\",\"location\":\"0 1 2\",\"presentation\":\"test2\"}",
                                "kt-response-id-000000000000", "1.0", "\"savedScoreState_accepted\"", 1,
                                "item1", "response_A")
                        .values(2, "kt-id-000000000001", "{\"content\":\"test3\",\"location\":\"0 1 2\",\"presentation\":\"test4\"}",
                                "kt-response-id-000000000001", "0.0", "\"savedScoreState_accepted\"", 1,
                                "item1", "response_A")
                        .build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        ktTestCaseService = app.injector().instanceOf(KtTestCaseService.class);
    }

    @Test
    public void findByIdentifiers_validIdentifiersGiven() {
        //When
        List<KtTestCase> resultList = jpaApi.withTransaction(() -> ktTestCaseService.findByIdentifiers("item1"));

        //Then
        assertThat("The returned list should have a size of 2", resultList.size(), equalTo(2));

        assertThat("The kt_test_case_id of the first record in the list should be as expected", resultList.get(0).getKtTestCaseId(), equalTo(1));
        assertThat("The kt_id of the first record in the list should be as expected", resultList.get(0).getKtId(), equalTo("kt-id-000000000000"));
        assertThat("The kt_candidate_response of the first record in the list should be as expected", resultList.get(0).getKtCandidateResponse(), equalTo("{\"content\":\"test1\",\"location\":\"0 1 2\",\"presentation\":\"test2\"}"));
        assertThat("The kt_response_id of the first record in the list should be as expected", resultList.get(0).getKtResponseId(), equalTo("kt-response-id-000000000000"));
        assertThat("The kt_saved_score of the first record in the list should be as expected", resultList.get(0).getKtSavedScore(), equalTo("1.0"));
        assertThat("The kt_saved_score_state of the first record in the list should be as expected", resultList.get(0).getKtSavedScoreState(), equalTo("\"savedScoreState_accepted\""));
        assertThat("The kt_include_in_build of the first record in the list should be as expected", resultList.get(0).getKtIncludeInBuild(), equalTo(true));
//        assertThat("The kt_working_id of the first record in the list should be as expected", resultList.get(0).getKtWorkingId(), equalTo("kt-working-id-000000000000"));
        assertThat("The item_identifier of the first record in the list should be as expected", resultList.get(0).getItemIdentifier(), equalTo("item1"));
        assertThat("The response_identifier of the first record in the list should be as expected", resultList.get(0).getResponseIdentifier(), equalTo("response_A"));

        assertThat("The kt_test_case_id of the second record in the list should be as expected", resultList.get(1).getKtTestCaseId(), equalTo(2));
        assertThat("The kt_id of the second record in the list should be as expected", resultList.get(1).getKtId(), equalTo("kt-id-000000000001"));
        assertThat("The kt_candidate_response of the second record in the list should be as expected", resultList.get(1).getKtCandidateResponse(), equalTo("{\"content\":\"test3\",\"location\":\"0 1 2\",\"presentation\":\"test4\"}"));
        assertThat("The kt_response_id of the second record in the list should be as expected", resultList.get(1).getKtResponseId(), equalTo("kt-response-id-000000000001"));
        assertThat("The kt_saved_score of the second record in the list should be as expected", resultList.get(1).getKtSavedScore(), equalTo("0.0"));
        assertThat("The kt_saved_score_state of the second record in the list should be as expected", resultList.get(1).getKtSavedScoreState(), equalTo("\"savedScoreState_accepted\""));
        assertThat("The kt_include_in_build of the second record in the list should be as expected", resultList.get(1).getKtIncludeInBuild(), equalTo(true));
//        assertThat("The kt_working_id of the second record in the list should be as expected", resultList.get(1).getKtWorkingId(), equalTo("kt-working-id-000000000001"));
        assertThat("The item_identifier of the second record in the list should be as expected", resultList.get(1).getItemIdentifier(), equalTo("item1"));
        assertThat("The response_identifier of the second record in the list should be as expected", resultList.get(1).getResponseIdentifier(), equalTo("response_A"));
    }

    @Test
    public void findByIdentifiers_invalidIdentifiersGiven() {
        //When
        List<KtTestCase> resultList = jpaApi.withTransaction(() -> ktTestCaseService.findByIdentifiers("item2"));

        //Then
        assertThat("The returned list should have a size of 0", resultList.size(), equalTo(0));
    }

    @Test
    public void createKtTestcase() {
        //When
        KtTestCase ktTestCase = KtTestCase.createKtTestCaseObject("kt-id-000000000002",
                "{\"content\":\"test5\",\"location\":\"0 1 2\",\"presentation\":\"test6\"}",
                "kt-response-id-000000000002", "0.0", "\"savedScoreState_accepted\"", true,
                "item2", "response_B");

        jpaApi.withTransaction(() -> ktTestCaseService.create(ktTestCase));
        List<KtTestCase> resultList = jpaApi.withTransaction(() -> ktTestCaseService.findByIdentifiers("item2"));

        //Then
        assertThat("The returned list should have a size of 1", resultList.size(), equalTo(1));
    }

    @Test
    public void deleteKtTestCases() {
        //When
        jpaApi.withTransaction(() -> ktTestCaseService.deleteByIdentifiers("item1"));
        List<KtTestCase> resultList = jpaApi.withTransaction(() -> ktTestCaseService.findByIdentifiers("item1"));

        //Then
        assertThat("The returned list should have a size of 0", resultList.size(), equalTo(0));
    }

}
