package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.Previewer;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PreviewerServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private static JPAApi jpaApi;
    private PreviewerService previewerService;

    @Before
    public void setUp() throws Exception {
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("previewer")
                        .columns("previewer_id", "name", "url", "api_url", "protocol", "api_protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://", "https://")
                        .values(2, "previewer2", "previewer2.com", "api.previewer2.com", "https://", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true)
                        .values(2, 1, 2, "tenant2", true)
                        .values(3, 2, 1, "tenant3", true)
                        .values(4, 2, 2, "tenant4", true)
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        previewerService = app.injector().instanceOf(PreviewerService.class);
    }

    @Test
    public void findAll_previewersExist_returnListOfPreviewers() throws Throwable {
        // When
        List<Previewer> resultList = jpaApi.withTransaction((Supplier<List<Previewer>>) previewerService::findAll);

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));
        assertThat("The ID of the first previewer in the list should be as expected", resultList.get(0).getPreviewerId(), equalTo(1));
        assertThat("The name of the first previewer in the list should be as expected", resultList.get(0).getName(), equalTo("previewer1"));
        assertThat("The ID of the second previewer in the list should be as expected", resultList.get(1).getPreviewerId(), equalTo(2));
        assertThat("The name of the second previewer in the list should be as expected", resultList.get(1).getName(), equalTo("previewer2"));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAll_noPreviewersExist_returnEmptyList() throws Throwable {
        // Given
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL);
        Database db = app.injector().instanceOf(Database.class);
        new DbSetup(new DataSourceDestination(db.getDataSource()), operation).launch();

        // When
        List<Previewer> resultList = jpaApi.withTransaction((Supplier<List<Previewer>>) previewerService::findAll);

        // Then
        assertThat("The returned list should be empty", resultList.size(), equalTo(0));
    }

    @Test
    public void findAll_validSetGiven_returnListOfPreviewers() throws Throwable {
        // Given
        Set<Integer> previewerIds = new HashSet<>();
        previewerIds.add(1);
        previewerIds.add(2);

        // When
        List<Previewer> resultList = jpaApi.withTransaction(() -> previewerService.findAll(previewerIds));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the first previewer in the list should be as expected", resultList.get(0).getPreviewerId(), equalTo(1));
        assertThat("The name of the first previewer in the list should be as expected", resultList.get(0).getName(), equalTo("previewer1"));

        assertThat("The ID of the second previewer in the list should be as expected", resultList.get(1).getPreviewerId(), equalTo(2));
        assertThat("The name of the second previewer in the list should be as expected", resultList.get(1).getName(), equalTo("previewer2"));
    }

    @Test
    public void findAll_partiallyValidSetGiven_returnListOfValidPreviewers() throws Throwable {
        // Given
        Set<Integer> previewerIds = new HashSet<>();
        previewerIds.add(1);
        previewerIds.add(3);

        // When
        List<Previewer> resultList = jpaApi.withTransaction(() -> previewerService.findAll(previewerIds));

        // Then
        assertThat("The returned list should have a length of 1", resultList.size(), equalTo(1));

        assertThat("The ID of the first previewer in the list should be as expected", resultList.get(0).getPreviewerId(), equalTo(1));
        assertThat("The name of the first previewer in the list should be as expected", resultList.get(0).getName(), equalTo("previewer1"));
    }

    @Test
    public void findAll_emptyListGiven_returnEmptyList() throws Throwable {
        // Given
        Set<Integer> previewerIds = new HashSet<>();

        // When
        List<Previewer> resultList = jpaApi.withTransaction(() -> previewerService.findAll(previewerIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));
    }

    @Test
    public void find_validIdGiven_returnPreviewer() throws Throwable {
        // When
        Optional<Previewer> optionalPreviewer = jpaApi.withTransaction(() -> previewerService.find(1));

        // Then
        assertThat("The ID of the previewer should be correct", optionalPreviewer.get().getPreviewerId(),
                equalTo(1));
        assertThat("The name of the previewer should be correct", optionalPreviewer.get().getName(),
                equalTo("previewer1"));
        assertThat("The URL of the previewer should be correct", optionalPreviewer.get().getUrl(),
                equalTo("previewer1.com"));
        assertThat("The API URL of the previewer should be correct", optionalPreviewer.get().getApiUrl(),
                equalTo("api.previewer1.com"));
        assertThat("The protocol of the previewer should be correct", optionalPreviewer.get().getProtocol(),
                equalTo("https://"));
        assertThat("The API protocol of the previewer should be correct", optionalPreviewer.get().getApiProtocol(),
                equalTo("https://"));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void find_invalidIdGiven_returnNull() throws Throwable {
        // When
        Optional<Previewer> optionalPreviewer = jpaApi.withTransaction(() -> previewerService.find(3));

        // Then
        assertThat("The result should be empty", optionalPreviewer.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

}