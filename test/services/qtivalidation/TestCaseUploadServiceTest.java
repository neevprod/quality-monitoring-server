package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.TestCaseUpload;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestCaseUploadServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private TestCaseUploadService testCaseUploadService;

    @Before
    public void setUp() throws Exception {
        Timestamp timestamp = Timestamp.from(Instant.now());
        Operation operation = Operations.sequenceOf(CommonDbOperations.DELETE_ALL,
                Operations.insertInto("previewer").columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true).values(2, 1, 2, "tenant2", true)
                        .build(),
                Operations.insertInto("user").columns("id", "fullname", "isAdmin", "is_api_user")
                        .values(1, "user1", 1, 0)
                        .build(),
                Operations.insertInto("test_case_uploads")
                        .columns("test_case_uploads_id", "previewer_id", "tenant_id", "user_id", "items_count",
                                "test_cases_count", "failed_test_cases_count", "mismatched_test_cases_count",
                                "processed_date")
                        .values(1, 1, 1, 1, 1, 10, 5, 0, timestamp)
                        .values(2, 1, 1, 1, 2, 20, 0, 5, timestamp)
                        .build());

        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        testCaseUploadService = app.injector().instanceOf(TestCaseUploadService.class);
    }

    @Test
    public void findAllByTenantId_validIdGiven_returnHistory() throws Throwable {
        // When
        List<TestCaseUpload> resultList = jpaApi.withTransaction(() -> testCaseUploadService.findAllByTenantId(1, 1));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the second Upload in the list should be as expected",
                resultList.get(0).getTestCaseUploadsId(), equalTo(2L));
        assertThat("The Items Count of the second Upload in the list should be as expected",
                resultList.get(0).getItemsCount(), equalTo(2));
        assertThat("The Test Cases Count of the second Upload in the list should be as expected",
                resultList.get(0).getTestCasesCount(), equalTo(20));
        assertThat("The Failed Test Cases Count of the second Upload in the list should be as expected",
                resultList.get(0).getFailedTestCasesCount(), equalTo(0));
        assertThat("The Mismatched Test Cases Count of the second Upload in the list should be as expected",
                resultList.get(0).getMismatchedTestCasesCount(), equalTo(5));
        assertThat("The User Name of the first Upload in the list should be as expected",
                resultList.get(0).getUser().getFullname(), equalTo("user1"));

        assertThat("The ID of the first Upload in the list should be as expected",
                resultList.get(1).getTestCaseUploadsId(), equalTo(1L));
        assertThat("The Items Count of the first Upload in the list should be as expected",
                resultList.get(1).getItemsCount(), equalTo(1));
        assertThat("The Test Cases Count of the first Upload in the list should be as expected",
                resultList.get(1).getTestCasesCount(), equalTo(10));
        assertThat("The Failed Test Cases Count of the first Upload in the list should be as expected",
                resultList.get(1).getFailedTestCasesCount(), equalTo(5));
        assertThat("The Mismatched Test Cases Count of the first Upload in the list should be as expected",
                resultList.get(1).getMismatchedTestCasesCount(), equalTo(0));
        assertThat("The User Name of the first Upload in the list should be as expected",
                resultList.get(1).getUser().getFullname(), equalTo("user1"));
    }

    @Test
    public void findAllByTenantId_invalidIdGiven_returnEmptyList() throws Throwable {
        // When
        List<TestCaseUpload> resultList = jpaApi.withTransaction(() -> testCaseUploadService.findAllByTenantId(3, 3));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }
}
