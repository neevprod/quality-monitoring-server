package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.PreviewerTenant;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PreviewerTenantServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;
    private PreviewerTenantService previewerTenantService;

    @Before
    public void setUp() throws Exception {
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("previewer")
                        .columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://")
                        .values(2, "previewer2", "previewer2.com", "api.previewer2.com", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true)
                        .values(2, 1, 2, "tenant2", true)
                        .values(3, 1, 3, "tenant3", false)
                        .values(4, 2, 1, "tenant4", true)
                        .values(5, 2, 2, "tenant5", true)
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        previewerTenantService = app.injector().instanceOf(PreviewerTenantService.class);
    }

    @Test
    public void findAll_validIdGiven_returnTenants() throws Throwable {
        // When
        List<PreviewerTenant> tenants = jpaApi.withTransaction(() -> previewerTenantService.findAll(1));

        // Then
        assertThat("Size of the list should be 3", tenants.size(), equalTo(3));

        assertThat("PreviewerTenant ID of the first tenant should be correct", tenants.get(0).getPreviewerTenantId(),
                equalTo(1L));
        assertThat("Tenant ID of the first tenant should be correct", tenants.get(0).getTenantId(), equalTo(1L));
        assertThat("Name of the first tenant should be correct", tenants.get(0).getName(), equalTo("tenant1"));
        assertThat("Is active of the first tenant should be correct", tenants.get(0).getActive(), equalTo(true));

        assertThat("PreviewerTenant ID of the second tenant should be correct", tenants.get(1).getPreviewerTenantId(),
                equalTo(2L));
        assertThat("Tenant ID of the second tenant should be correct", tenants.get(1).getTenantId(), equalTo(2L));
        assertThat("Name of the second tenant should be correct", tenants.get(1).getName(), equalTo("tenant2"));
        assertThat("Is active of the second tenant should be correct", tenants.get(1).getActive(), equalTo(true));

        assertThat("PreviewerTenant ID of the third tenant should be correct", tenants.get(2).getPreviewerTenantId(),
                equalTo(3L));
        assertThat("Tenant ID of the third tenant should be correct", tenants.get(2).getTenantId(), equalTo(3L));
        assertThat("Name of the third tenant should be correct", tenants.get(2).getName(), equalTo("tenant3"));
        assertThat("Is active of the third tenant should be correct", tenants.get(2).getActive(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAll_invalidIdGiven_returnEmptyList() throws Throwable {
        // When
        List<PreviewerTenant> tenants = jpaApi.withTransaction(() -> previewerTenantService.findAll(3));

        // Then
        assertThat("The list should be empty", tenants.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllActive_validIdGiven_returnTenants() throws Throwable {
        // When
        List<PreviewerTenant> tenants = jpaApi.withTransaction(() -> previewerTenantService.findAllActive(1));

        // Then
        assertThat("Size of the list should be 2", tenants.size(), equalTo(2));

        assertThat("PreviewerTenant ID of the first tenant should be correct", tenants.get(0).getPreviewerTenantId(),
                equalTo(1L));
        assertThat("Tenant ID of the first tenant should be correct", tenants.get(0).getTenantId(), equalTo(1L));
        assertThat("Name of the first tenant should be correct", tenants.get(0).getName(), equalTo("tenant1"));
        assertThat("Is active of the first tenant should be correct", tenants.get(0).getActive(), equalTo(true));

        assertThat("PreviewerTenant ID of the second tenant should be correct", tenants.get(1).getPreviewerTenantId(),
                equalTo(2L));
        assertThat("Tenant ID of the second tenant should be correct", tenants.get(1).getTenantId(), equalTo(2L));
        assertThat("Name of the second tenant should be correct", tenants.get(1).getName(), equalTo("tenant2"));
        assertThat("Is active of the second tenant should be correct", tenants.get(1).getActive(), equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllActive_invalidIdGiven_returnEmptyList() throws Throwable {
        // When
        List<PreviewerTenant> tenants = jpaApi.withTransaction(() -> previewerTenantService.findAllActive(3));

        // Then
        assertThat("The list should be empty", tenants.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllActive_validIdGivenContainingAllowedTenants_returnAllowedTenants() throws Throwable {
        // Given
        Set<Long> allowedIds = new HashSet<>();
        allowedIds.add(1L);
        allowedIds.add(2L);
        allowedIds.add(3L);

        // When
        List<PreviewerTenant> tenants =
                jpaApi.withTransaction(() -> previewerTenantService.findAllActive(1, allowedIds));

        // Then
        assertThat("Size of the list should be 2", tenants.size(), equalTo(2));

        assertThat("PreviewerTenant ID of the first tenant should be correct", tenants.get(0).getPreviewerTenantId(),
                equalTo(1L));
        assertThat("Tenant ID of the first tenant should be correct", tenants.get(0).getTenantId(), equalTo(1L));
        assertThat("Name of the first tenant should be correct", tenants.get(0).getName(), equalTo("tenant1"));
        assertThat("Is active of the first tenant should be correct", tenants.get(0).getActive(), equalTo(true));

        assertThat("PreviewerTenant ID of the second tenant should be correct", tenants.get(1).getPreviewerTenantId(),
                equalTo(2L));
        assertThat("Tenant ID of the second tenant should be correct", tenants.get(1).getTenantId(), equalTo(2L));
        assertThat("Name of the second tenant should be correct", tenants.get(1).getName(), equalTo("tenant2"));
        assertThat("Is active of the second tenant should be correct", tenants.get(1).getActive(), equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllActive_validIdGivenContainingOneAllowedTenant_returnAllowedTenant() throws Throwable {
        // Given
        Set<Long> allowedIds = new HashSet<>();
        allowedIds.add(1L);

        // When
        List<PreviewerTenant> tenants =
                jpaApi.withTransaction(() -> previewerTenantService.findAllActive(1, allowedIds));

        // Then
        assertThat("Size of the list should be 1", tenants.size(), equalTo(1));

        assertThat("PreviewerTenant ID of the first tenant should be correct", tenants.get(0).getPreviewerTenantId(),
                equalTo(1L));
        assertThat("Tenant ID of the first tenant should be correct", tenants.get(0).getTenantId(), equalTo(1L));
        assertThat("Name of the first tenant should be correct", tenants.get(0).getName(), equalTo("tenant1"));
        assertThat("Is active of the first tenant should be correct", tenants.get(0).getActive(), equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllActive_validIdGivenContainingNoAllowedTenants_returnEmptyList() throws Throwable {
        // Given
        Set<Long> allowedIds = new HashSet<>();

        // When
        List<PreviewerTenant> tenants =
                jpaApi.withTransaction(() -> previewerTenantService.findAllActive(2, allowedIds));

        // Then
        assertThat("The list should be empty", tenants.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllActive_invalidIdGivenContainingNoAllowedTenants_returnEmptyList() throws Throwable {
        // Given
        Set<Long> allowedIds = new HashSet<>();

        // When
        List<PreviewerTenant> tenants =
                jpaApi.withTransaction(() -> previewerTenantService.findAllActive(3, allowedIds));

        // Then
        assertThat("The list should be empty", tenants.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findActive_validIdsGiven_returnPreviewer() throws Throwable {
        // When
        Optional<PreviewerTenant> optionalTenant =
                jpaApi.withTransaction(() -> previewerTenantService.findActive(1, 1L));

        // Then
        assertThat("PreviewerTenant ID of the tenant should be correct", optionalTenant.get().getPreviewerTenantId(),
                equalTo(1L));
        assertThat("Tenant ID of the tenant should be correct", optionalTenant.get().getTenantId(), equalTo(1L));
        assertThat("Name of the tenant should be correct", optionalTenant.get().getName(), equalTo("tenant1"));
        assertThat("Is active of the tenant should be correct", optionalTenant.get().getActive(), equalTo(true));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findActive_idOfInactiveTenantGiven_returnEmpty() throws Throwable {
        // When
        Optional<PreviewerTenant> optionalTenant =
                jpaApi.withTransaction(() -> previewerTenantService.findActive(1, 3L));

        // Then
        assertThat("The result should by empty", optionalTenant.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findActive_invalidPreviewerIdGiven_returnEmpty() throws Throwable {
        // When
        Optional<PreviewerTenant> optionalTenant =
                jpaApi.withTransaction(() -> previewerTenantService.findActive(3, 1L));

        // Then
        assertThat("The result should by empty", optionalTenant.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findActive_invalidTenantIdGiven_returnEmpty() throws Throwable {
        // When
        Optional<PreviewerTenant> optionalTenant =
                jpaApi.withTransaction(() -> previewerTenantService.findActive(1, 4L));

        // Then
        assertThat("The result should by empty", optionalTenant.isPresent(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

}
