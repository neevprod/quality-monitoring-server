package services.qtivalidation;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.qtivalidation.HistExec;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.CommonDbOperations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class HistExecServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private static final Timestamp timestamp = Timestamp.from(Instant.now());
    private JPAApi jpaApi;
    private HistExecService histExecService;

    @Before
    public void setUp() throws Exception {
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("previewer")
                        .columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://")
                        .values(2, "previewer2", "previewer2.com", "api.previewer2.com", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", true)
                        .values(2, 1, 2, "tenant2", true)
                        .values(3, 2, 3, "tenant3", true)
                        .values(4, 2, 4, "tenant4", true)
                        .build(),
                Operations.insertInto("hist_exec")
                        .columns("hist_exec_id", "previewer_id", "exec_start", "exec_stop", "exec_completed",
                                "tenant_level_exec")
                        .values(1, 2, timestamp, timestamp, 1, 1)
                        .values(2, 1, timestamp, timestamp, 1, 0)
                        .values(3, 2, timestamp, timestamp, 1, 1)
                        .values(4, 2, timestamp, timestamp, 1, 1)
                        .values(5, 1, timestamp, timestamp, 0, 0)
                        .values(6, 1, timestamp, timestamp, 0, 1)
                        .build(),
                Operations.insertInto("hist_status")
                        .columns("hist_status_id", "name")
                        .values(4, "REACTIVATED")
                        .build(),
                Operations.insertInto("hist_tenant")
                        .columns("hist_tenant_id", "hist_exec_id", "previewer_tenant_id", "exec_start", "exec_stop",
                                "exec_completed", "hist_status_id", "items_validated", "items_with_failed_tcs",
                                "items_with_pending_tcs", "deactivated_items", "item_exceptions")
                        .values(1, 1, 3, timestamp, timestamp, 1, 4, 6, 2, 1, 5, 0)
                        .values(2, 2, 1, timestamp, timestamp, 0, 4, 3, 0, 3, 0, 0)
                        .values(3, 3, 4, timestamp, timestamp, 1, 4, 4, 1, 0, 0, 3)
                        .values(4, 4, 3, timestamp, timestamp, 1, 4, 2, 8, 3, 3, 0)
                        .values(5, 5, 1, timestamp, timestamp, 1, 4, 9, 7, 2, 2, 1)
                        .values(6, 5, 2, timestamp, timestamp, 1, 4, 2, 8, 3, 3, 0)
                        .values(7, 6, 4, timestamp, timestamp, 1, 4, 2, 8, 3, 3, 0)
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);

        jpaApi = app.injector().instanceOf(JPAApi.class);
        histExecService = app.injector().instanceOf(HistExecService.class);
    }

    @Test
    public void findAllByPreviewerId_previewerHasHistExecs_returnHistExecsNotAtTenantLevel() throws Throwable {
        // When
        List<HistExec> resultList = jpaApi.withTransaction(() -> histExecService.findAllByPreviewerId(1));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the first HistExec in the list should be as expected",
                resultList.get(0).getHistExecId(), equalTo(5));
        assertThat("The previewer ID of the first HistExec in the list should be as expected",
                resultList.get(0).getPreviewer().getPreviewerId(), equalTo(1));
        assertThat("The start time of the first HistExec in the list should be as expected",
                resultList.get(0).getExecStart(), equalTo(timestamp));
        assertThat("The end time of the first HistExec in the list should be as expected",
                resultList.get(0).getExecStop(), equalTo(timestamp));
        assertThat("The completed value of the first HistExec in the list should be as expected",
                resultList.get(0).getExecCompleted(), equalTo(false));
        assertThat("The tenant level value of the first HistExec in the list should be as expected",
                resultList.get(0).getTenantLevelExec(), equalTo(false));

        assertThat("The ID of the second HistExec in the list should be as expected",
                resultList.get(1).getHistExecId(), equalTo(2));
        assertThat("The previewer ID of the second HistExec in the list should be as expected",
                resultList.get(1).getPreviewer().getPreviewerId(), equalTo(1));
        assertThat("The start time of the second HistExec in the list should be as expected",
                resultList.get(1).getExecStart(), equalTo(timestamp));
        assertThat("The end time of the second HistExec in the list should be as expected",
                resultList.get(1).getExecStop(), equalTo(timestamp));
        assertThat("The completed value of the second HistExec in the list should be as expected",
                resultList.get(1).getExecCompleted(), equalTo(true));
        assertThat("The tenant level value of the second HistExec in the list should be as expected",
                resultList.get(1).getTenantLevelExec(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByPreviewerId_previewerHasNoHistExecs_returnEmptyList() throws Throwable {
        // When
        List<HistExec> resultList = jpaApi.withTransaction(() -> histExecService.findAllByPreviewerId(6));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByPreviewerId_previewerHasOnlyTenantLevelHistExecs_returnEmptyList() throws Throwable {
        // When
        List<HistExec> resultList = jpaApi.withTransaction(() -> histExecService.findAllByPreviewerId(2));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByPreviewerId_previewerHasHistExecsWithGivenTenants_returnHistExecsNotAtTenantLevel()
            throws Throwable {
        // Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(1L);
        tenantIds.add(2L);
        tenantIds.add(3L);

        // When
        List<HistExec> resultList = jpaApi.withTransaction(() -> histExecService.findAllByPreviewerId(1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The ID of the first HistExec in the list should be as expected",
                resultList.get(0).getHistExecId(), equalTo(5));
        assertThat("The previewer ID of the first HistExec in the list should be as expected",
                resultList.get(0).getPreviewer().getPreviewerId(), equalTo(1));
        assertThat("The start time of the first HistExec in the list should be as expected",
                resultList.get(0).getExecStart(), equalTo(timestamp));
        assertThat("The end time of the first HistExec in the list should be as expected",
                resultList.get(0).getExecStop(), equalTo(timestamp));
        assertThat("The completed value of the first HistExec in the list should be as expected",
                resultList.get(0).getExecCompleted(), equalTo(false));
        assertThat("The tenant level value of the first HistExec in the list should be as expected",
                resultList.get(0).getTenantLevelExec(), equalTo(false));

        assertThat("The ID of the second HistExec in the list should be as expected",
                resultList.get(1).getHistExecId(), equalTo(2));
        assertThat("The previewer ID of the second HistExec in the list should be as expected",
                resultList.get(1).getPreviewer().getPreviewerId(), equalTo(1));
        assertThat("The start time of the second HistExec in the list should be as expected",
                resultList.get(1).getExecStart(), equalTo(timestamp));
        assertThat("The end time of the second HistExec in the list should be as expected",
                resultList.get(1).getExecStop(), equalTo(timestamp));
        assertThat("The completed value of the second HistExec in the list should be as expected",
                resultList.get(1).getExecCompleted(), equalTo(true));
        assertThat("The tenant level value of the second HistExec in the list should be as expected",
                resultList.get(1).getTenantLevelExec(), equalTo(false));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByPreviewerId_previewerHasNoHistExecsWithGivenTenants_returnEmptyList() throws Throwable {
        // Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(3L);

        // When
        List<HistExec> resultList = jpaApi.withTransaction(() -> histExecService.findAllByPreviewerId(1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByPreviewerId_previewerHasOnlyTenantLevelHistExecsWithGivenTenants_returnEmptyList()
            throws Throwable {
        // Given
        Set<Long> tenantIds = new HashSet<>();
        tenantIds.add(3L);
        tenantIds.add(4L);

        // When
        List<HistExec> resultList = jpaApi.withTransaction(() -> histExecService.findAllByPreviewerId(2, tenantIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByPreviewerId_emptySetOfTenantsGiven_returnEmptyList() throws Throwable {
        // Given
        Set<Long> tenantIds = new HashSet<>();

        // When
        List<HistExec> resultList = jpaApi.withTransaction(() -> histExecService.findAllByPreviewerId(1, tenantIds));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

}
