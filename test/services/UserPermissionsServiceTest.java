package services;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.DbSetupTracker;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import global.TestWithApplication;
import models.PermissionSettingsValue;
import models.UserPermissions;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.jpa.JPAApi;
import services.usermanagement.UserPermissionsService;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class UserPermissionsServiceTest extends TestWithApplication {
    private static DbSetupTracker dbSetupTracker = new DbSetupTracker();
    private JPAApi jpaApi;

    @Before
    public void setUp() throws Exception {
        Operation operation = Operations.sequenceOf(
                CommonDbOperations.DELETE_ALL,
                Operations.insertInto("user")
                        .columns("id", "fullname", "isadmin")
                        .values(1L, "User 1", 0)
                        .values(2L, "User 2", 0)
                        .build(),
                Operations.insertInto("previewer")
                        .columns("previewer_id", "name", "url", "api_url", "protocol")
                        .values(1, "previewer1", "previewer1.com", "api.previewer1.com", "https://")
                        .build(),
                Operations.insertInto("previewer_tenant")
                        .columns("previewer_tenant_id", "previewer_id", "tenant_id", "name", "active")
                        .values(1, 1, 1, "tenant1", 1)
                        .values(2, 1, 2, "tenant2", 1)
                        .build(),
                Operations.insertInto("permission_type")
                        .columns("permission_type_id", "permission_type_name")
                        .values(1, "permissionType1")
                        .values(2, "permissionType2")
                        .build(),
                Operations.insertInto("application_module")
                        .columns("application_module_id", "application_module_name", "permission_type_id")
                        .values(1, "module1", 1)
                        .values(2, "module2", 2)
                        .build(),
                Operations.insertInto("permission_option")
                        .columns("permission_option_id", "name", "application_module_id", "create_access", "read_access", "update_access", "delete_access")
                        .values(1, "feature1", 1, 1, 1, 1, 1)
                        .values(2, "feature2", 1, 1, 1, 1, 1)
                        .build(),
                Operations.insertInto("permission_settings")
                        .columns("permission_settings_id", "name", "application_module_id")
                        .values(1, "permissions1", 1)
                        .values(2, "permissions2", 1)
                        .values(3, "permissions3", 1)
                        .build(),
                Operations.insertInto("permission_settings_value")
                        .columns("permission_settings_value_id", "permission_settings_id", "permission_option_id", "create_access", "read_access", "update_access", "delete_access")
                        .values(1, 1, 1, 1, 1, 0, 0)
                        .values(2, 1, 2, 0, 0, 1, 1)
                        .values(3, 2, 1, 0, 1, 0, 0)
                        .values(4, 2, 2, 0, 1, 0, 0)
                        .values(5, 3, 1, 0, 0, 1, 0)
                        .values(6, 3, 2, 0, 0, 1, 0)
                        .build(),
                Operations.insertInto("user_permissions")
                        .columns("user_permission_id", "user_id", "application_module_id", "previewer_id", "tenant_id", "permission_settings_id")
                        .values(1, 1, 2, 1, 1, 3)
                        .values(2, 2, 1, 1, 2, 1)
                        .values(3, 1, 1, 1, 2, 2)
                        .build()
        );
        Database db = app.injector().instanceOf(Database.class);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(db.getDataSource()), operation);
        dbSetupTracker.launchIfNecessary(dbSetup);
        jpaApi = app.injector().instanceOf(JPAApi.class);
    }

    @Test
    public void findAllByUserId_permissionsExist_returnListOfPermissions() throws Throwable {
        // Given
        UserPermissionsService userPermissionsService = app.injector().instanceOf(UserPermissionsService.class);

        // When
        List<UserPermissions> resultList = jpaApi.withTransaction(() -> userPermissionsService.findAllByUserId(1L));

        // Then
        assertThat("The returned list should have a length of 2", resultList.size(), equalTo(2));

        assertThat("The first entry in the list should have the correct previewer ID",
                resultList.get(0).getPreviewerId(), equalTo(1));
        assertThat("The first entry in the list should have the correct tenant ID",
                resultList.get(0).getTenantId(), equalTo(1L));
        assertThat("The first entry in the list should have the correct settings ID",
                resultList.get(0).getPermissionSettingsId(), equalTo(3L));
        assertThat("The first entry in the list should have the correct application module ID",
                resultList.get(0).getApplicationModule().getApplicationModuleId(), equalTo(2));
        assertThat("The first entry in the list should have the correct permission type ID",
                resultList.get(0).getApplicationModule().getPermissionType().getPermissionTypeId(), equalTo(2));

        assertThat("The second entry in the list should have the correct previewer ID",
                resultList.get(1).getPreviewerId(), equalTo(1));
        assertThat("The second entry in the list should have the correct tenant ID",
                resultList.get(1).getTenantId(), equalTo(2L));
        assertThat("The second entry in the list should have the correct settings ID",
                resultList.get(1).getPermissionSettingsId(), equalTo(2L));
        assertThat("The second entry in the list should have the correct application module ID",
                resultList.get(1).getApplicationModule().getApplicationModuleId(), equalTo(1));
        assertThat("The second entry in the list should have the correct permission type ID",
                resultList.get(1).getApplicationModule().getPermissionType().getPermissionTypeId(), equalTo(1));

        Set<PermissionSettingsValue> values = resultList.get(1).getPermissionSettings()
                .getPermissionSettingsValues();
        for(PermissionSettingsValue value: values) {
            assertThat("The create access in the value should be set to false", value.getCreateAccess(), equalTo(false));
            assertThat("The read access in the value should be set to true", value.getReadAccess(), equalTo(true));
            assertThat("The update access in the value should be set to false", value.getUpdateAccess(), equalTo(false));
            assertThat("The delete access in the value should be set to false", value.getDeleteAccess(), equalTo(false));
        }

        dbSetupTracker.skipNextLaunch();
    }

    @Test
    public void findAllByUserId_noPermissionsExistForUser_returnEmptyList() throws Throwable {
        // Given
        UserPermissionsService userPermissionsService = app.injector().instanceOf(UserPermissionsService.class);

        // When
        List<UserPermissions> resultList = jpaApi.withTransaction(() -> userPermissionsService.findAllByUserId(3L));

        // Then
        assertThat("The returned list should have a length of 0", resultList.size(), equalTo(0));

        dbSetupTracker.skipNextLaunch();
    }

}
