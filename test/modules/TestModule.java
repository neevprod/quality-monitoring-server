package modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.pearson.itautomation.qtiscv.QtiScv;
import controllers.systemvalidation.EpenScenarioCreator;
import global.CustomJsonMapper;
import jobs.JobRunnerActor;
import jobs.NightlyJobActor;
import jobs.UpdateTenantsActor;
import jobs.UpdateTenantsChildActor;
import jobs.qtivalidation.*;
import jobs.systemvalidation.*;
import org.mockito.Mockito;
import play.libs.akka.AkkaGuiceSupport;
import play.libs.mailer.Email;
import util.ActiveDirectoryAuthenticator;
import util.ConnectionPoolManager;
import util.SessionData;
import util.systemvalidation.BackendData;

import java.time.Clock;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TestModule extends AbstractModule implements AkkaGuiceSupport {

    @Override
    protected void configure() {
        bind(CustomJsonMapper.class).asEagerSingleton();

        // Set the clock time to one year before now to make it more clear if we have timing issues in tests.
        // (If we set it to the current time, and some tests don't set a fixed time, they might coincidentally succeed.)
        ZonedDateTime lastYear = ZonedDateTime.now().minusYears(1);
        bind(Clock.class).toInstance(Clock.fixed(lastYear.toInstant(), ZoneId.systemDefault()));

        install(new FactoryModuleBuilder().build(SessionData.Factory.class));
        install(new FactoryModuleBuilder().build(BackendData.Factory.class));
        bind(QtiScv.class).toInstance(Mockito.mock(QtiScv.class));

        install(new FactoryModuleBuilder().build(QtiScoringValidationJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(DivJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(TestCaseUploadJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(DivJobSetupJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(KtTestCaseDownloadJobStarter.Factory.class));

        bindActor(JobRunnerActor.class, "jobRunnerActor");
        bindActorFactory(NightlyJobActor.class, NightlyJobActor.Factory.class);
        bindActorFactory(UpdateTenantsActor.class, UpdateTenantsActor.Factory.class);
        bindActorFactory(UpdateTenantsChildActor.class, UpdateTenantsChildActor.Factory.class);
        bindActorFactory(QtiScoringValidationActor.class, QtiScoringValidationActor.Factory.class);
        bindActorFactory(DataIntegrityValidationActor.class, DataIntegrityValidationActor.Factory.class);
        bindActorFactory(DataIntegrityValidationActorTest.MockActor.class, Tn8ApiDataLoaderActor.Factory.class);
        bindActorFactory(DataIntegrityValidationActorTest.MockActor.class, IrisValidationActor.Factory.class);
        bindActorFactory(TestCaseUploadActor.class, TestCaseUploadActor.Factory.class);
        bindActorFactory(DataIntegrityValidationActorTest.MockActor.class, BackendValidationActor.Factory.class);
        bindActorFactory(DataIntegrityValidationActorTest.MockActor.class, Epen2ValidationActor.Factory.class);
        bindActorFactory(DataIntegrityValidationActorTest.MockActor.class, StatsValidationActor.Factory.class);
        bindActorFactory(DivJobSetupActor.class, DivJobSetupActor.Factory.class);
        bindActorFactory(BatteryStatsValidationActor.class, BatteryStatsValidationActor.Factory.class);
        bindActorFactory(KtTestCaseDownloadActor.class, KtTestCaseDownloadActor.Factory.class);
    }

    @Provides
    Email provideEmail() {
        return new Email();
    }

    @Provides
    ActiveDirectoryAuthenticator adAuth() {
        return Mockito.mock(ActiveDirectoryAuthenticator.class);
    }

    @Provides
    EpenScenarioCreator provideEpenScenarioCreator(ConnectionPoolManager connectionPoolManager) {
        return new EpenScenarioCreator(connectionPoolManager);
    }

}
