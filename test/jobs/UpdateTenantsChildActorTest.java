package jobs;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import com.pearson.itautomation.tn8.previewer.api.GetTenantsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import global.TestWithApplication;
import info.solidsoft.mockito.java8.LambdaMatcher;
import models.qtivalidation.Previewer;
import models.qtivalidation.PreviewerTenant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.db.jpa.JPAApi;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;
import services.qtivalidation.PreviewerService;
import services.qtivalidation.PreviewerTenantService;
import util.qtivalidation.TestNav8APIFactory;

import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TestNav8API.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class UpdateTenantsChildActorTest extends TestWithApplication {
    private ActorRef actor;
    TestNav8APIFactory apiFactory;
    private PreviewerService previewerService;
    private PreviewerTenantService previewerTenantService;
    private TestNav8API previewer1Api;
    private TestNav8API previewer2Api;
    private Previewer previewer1;
    private Previewer previewer2;

    @Before
    public void setUp() throws Exception {
        apiFactory = Mockito.mock(TestNav8APIFactory.class);
        previewer1Api = Mockito.mock(TestNav8API.class);
        previewer2Api = Mockito.mock(TestNav8API.class);

        previewerService = Mockito.mock(PreviewerService.class);
        previewerTenantService = Mockito.mock(PreviewerTenantService.class);

        previewer1 = new Previewer();
        previewer1.setPreviewerId(1);
        previewer1.setApiProtocol("https://");
        previewer1.setApiUrl("previewer1.com");
        previewer2 = new Previewer();
        previewer2.setPreviewerId(2);
        previewer2.setApiProtocol("https://");
        previewer2.setApiUrl("previewer2.com");

        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        actor = actorSystem.actorOf(Props.create(UpdateTenantsChildActor.class, app.injector().instanceOf(JPAApi.class),
                apiFactory, previewerService, previewerTenantService));
    }

    @Test
    public void onReceive_updateMessageWithNewTenantAdded_addNewTenant() throws Exception {
        // Given
        Set<Integer> previewerIds = new HashSet<>();
        previewerIds.add(1);

        GetTenantsResult getTenantsResult = new GetTenantsResult("{" +
                "    \"api\": \"getTenants\"," +
                "    \"success\": \"yes\"," +
                "    \"msg\": {" +
                "        \"tenants\": [{" +
                "            \"id\": \"1\"," +
                "            \"name\": \"tenant1\"" +
                "        }]" +
                "    }" +
                "}");
        Mockito.doReturn(Optional.of(previewer1Api)).when(apiFactory).create(1);
        Mockito.doReturn(getTenantsResult).when(previewer1Api).getTenants();

        List<Previewer> previewers = new ArrayList<>();
        previewers.add(previewer1);
        Mockito.doReturn(previewers).when(previewerService).findAll(previewerIds);

        // When
        Future<Object> future = Patterns.ask(actor, new UpdateTenantsChildActor.UpdateMessage(previewerIds), 3000);
        String result = (String) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Finished message should be returned", result, equalTo(UpdateTenantsActor.FINISHED_MESSAGE));
        Mockito.verify(previewerTenantService, Mockito.times(1)).create(Mockito.any());
        Mockito.verify(previewerTenantService).create(
                LambdaMatcher.argLambda((tenant) -> tenant.getTenantId() == 1L,
                        "The created tenant should have the correct tenant ID"));
        Mockito.verify(previewerTenantService).create(
                LambdaMatcher.argLambda((tenant) -> tenant.getName().equals("tenant1"),
                        "The created tenant should have the correct name"));
        Mockito.verify(previewerTenantService).create(
                LambdaMatcher.argLambda(PreviewerTenant::getActive,
                        "The created tenant should be active"));
        Mockito.verify(previewerTenantService).create(
                LambdaMatcher.argLambda((tenant) -> tenant.getPreviewer().getPreviewerId() == 1,
                        "The created tenant should be associated with the correct previewer"));
    }

    @Test
    public void onReceive_updateMessageWithTenantRenamed_renameTenant() throws Exception {
        // Given
        Set<Integer> previewerIds = new HashSet<>();
        previewerIds.add(1);

        GetTenantsResult getTenantsResult = new GetTenantsResult("{" +
                "    \"api\": \"getTenants\"," +
                "    \"success\": \"yes\"," +
                "    \"msg\": {" +
                "        \"tenants\": [{" +
                "            \"id\": \"1\"," +
                "            \"name\": \"newName\"" +
                "        }]" +
                "    }" +
                "}");
        Mockito.doReturn(Optional.of(previewer1Api)).when(apiFactory).create(1);
        Mockito.doReturn(getTenantsResult).when(previewer1Api).getTenants();

        List<Previewer> previewers = new ArrayList<>();
        previewers.add(previewer1);
        Mockito.doReturn(previewers).when(previewerService).findAll(previewerIds);

        PreviewerTenant tenant = Mockito.spy(new PreviewerTenant(previewer1, 1L, "tenant1", true));
        List<PreviewerTenant> tenants = new ArrayList<>();
        tenants.add(tenant);
        Mockito.doReturn(tenants).when(previewerTenantService).findAll(previewer1.getPreviewerId());

        // When
        Future<Object> future = Patterns.ask(actor, new UpdateTenantsChildActor.UpdateMessage(previewerIds), 3000);
        String result = (String) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Finished message should be returned", result, equalTo(UpdateTenantsActor.FINISHED_MESSAGE));
        Mockito.verify(tenant).setName("newName");
    }

    @Test
    public void onReceive_updateMessageWithTenantRemoved_deactivateTenant() throws Exception {
        // Given
        Set<Integer> previewerIds = new HashSet<>();
        previewerIds.add(1);

        GetTenantsResult getTenantsResult = new GetTenantsResult("{" +
                "    \"api\": \"getTenants\"," +
                "    \"success\": \"yes\"," +
                "    \"msg\": {" +
                "        \"tenants\": []" +
                "    }" +
                "}");
        Mockito.doReturn(Optional.of(previewer1Api)).when(apiFactory).create(1);
        Mockito.doReturn(getTenantsResult).when(previewer1Api).getTenants();

        List<Previewer> previewers = new ArrayList<>();
        previewers.add(previewer1);
        Mockito.doReturn(previewers).when(previewerService).findAll(previewerIds);

        PreviewerTenant tenant = Mockito.spy(new PreviewerTenant(previewer1, 1L, "tenant1", true));
        List<PreviewerTenant> tenants = new ArrayList<>();
        tenants.add(tenant);
        Mockito.doReturn(tenants).when(previewerTenantService).findAll(previewer1.getPreviewerId());

        // When
        Future<Object> future = Patterns.ask(actor, new UpdateTenantsChildActor.UpdateMessage(previewerIds), 3000);
        String result = (String) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Finished message should be returned", result, equalTo(UpdateTenantsActor.FINISHED_MESSAGE));
        Mockito.verify(tenant).setActive(false);
    }

    @Test
    public void onReceive_updateMessageWithTenantReadded_reactivateTenant() throws Exception {
        // Given
        Set<Integer> previewerIds = new HashSet<>();
        previewerIds.add(1);

        GetTenantsResult getTenantsResult = new GetTenantsResult("{" +
                "    \"api\": \"getTenants\"," +
                "    \"success\": \"yes\"," +
                "    \"msg\": {" +
                "        \"tenants\": [{" +
                "            \"id\": \"1\"," +
                "            \"name\": \"previewer1\"" +
                "        }]" +
                "    }" +
                "}");
        Mockito.doReturn(Optional.of(previewer1Api)).when(apiFactory).create(1);
        Mockito.doReturn(getTenantsResult).when(previewer1Api).getTenants();

        List<Previewer> previewers = new ArrayList<>();
        previewers.add(previewer1);
        Mockito.doReturn(previewers).when(previewerService).findAll(previewerIds);

        PreviewerTenant tenant = Mockito.spy(new PreviewerTenant(previewer1, 1L, "tenant1", false));
        List<PreviewerTenant> tenants = new ArrayList<>();
        tenants.add(tenant);
        Mockito.doReturn(tenants).when(previewerTenantService).findAll(previewer1.getPreviewerId());

        // When
        Future<Object> future = Patterns.ask(actor, new UpdateTenantsChildActor.UpdateMessage(previewerIds), 3000);
        String result = (String) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Finished message should be returned", result, equalTo(UpdateTenantsActor.FINISHED_MESSAGE));
        Mockito.verify(tenant).setActive(true);
    }

    @Test
    public void onReceive_updateMessageWithTenantsUpdatedInMultiplePreviewers_updateTenants() throws Exception {
        // Given
        Set<Integer> previewerIds = new HashSet<>();
        previewerIds.add(1);
        previewerIds.add(2);

        GetTenantsResult getTenantsResult1 = new GetTenantsResult("{" +
                "    \"api\": \"getTenants\"," +
                "    \"success\": \"yes\"," +
                "    \"msg\": {" +
                "        \"tenants\": [{" +
                "            \"id\": \"1\"," +
                "            \"name\": \"newName1\"" +
                "        }]" +
                "    }" +
                "}");
        Mockito.doReturn(Optional.of(previewer1Api)).when(apiFactory).create(1);
        Mockito.doReturn(getTenantsResult1).when(previewer1Api).getTenants();

        GetTenantsResult getTenantsResult2 = new GetTenantsResult("{" +
                "    \"api\": \"getTenants\"," +
                "    \"success\": \"yes\"," +
                "    \"msg\": {" +
                "        \"tenants\": [{" +
                "            \"id\": \"2\"," +
                "            \"name\": \"newName2\"" +
                "        }]" +
                "    }" +
                "}");
        Mockito.doReturn(Optional.of(previewer2Api)).when(apiFactory).create(2);
        Mockito.doReturn(getTenantsResult2).when(previewer2Api).getTenants();

        List<Previewer> previewers = new ArrayList<>();
        previewers.add(previewer1);
        previewers.add(previewer2);
        Mockito.doReturn(previewers).when(previewerService).findAll(previewerIds);

        PreviewerTenant tenant1 = Mockito.spy(new PreviewerTenant(previewer1, 1L, "tenant1", true));
        List<PreviewerTenant> tenants1 = new ArrayList<>();
        tenants1.add(tenant1);
        Mockito.doReturn(tenants1).when(previewerTenantService).findAll(previewer1.getPreviewerId());

        PreviewerTenant tenant2 = Mockito.spy(new PreviewerTenant(previewer2, 2L, "tenant2", true));
        List<PreviewerTenant> tenants2 = new ArrayList<>();
        tenants2.add(tenant2);
        Mockito.doReturn(tenants2).when(previewerTenantService).findAll(previewer2.getPreviewerId());

        // When
        Future<Object> future = Patterns.ask(actor, new UpdateTenantsChildActor.UpdateMessage(previewerIds), 3000);
        String result = (String) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Finished message should be returned", result, equalTo(UpdateTenantsActor.FINISHED_MESSAGE));
        Mockito.verify(tenant1).setName("newName1");
        Mockito.verify(tenant2).setName("newName2");
    }

}
