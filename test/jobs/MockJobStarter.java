package jobs;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;

import javax.inject.Inject;
import java.time.Clock;

public class MockJobStarter extends JobStarter {
    private String jobType = "mockJob";
    private int maxCurrentInstances = 1;

    public static class MockActor extends UntypedAbstractActor {
        @Override
        public void onReceive(Object message) throws Exception {
        }
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public void setMaxCurrentInstances(int maxCurrentInstances) {
        this.maxCurrentInstances = maxCurrentInstances;
    }

    @Inject
    protected MockJobStarter(Clock clock) {
        super(clock);
    }

    @Override
    public String getJobType() {
        return jobType;
    }

    @Override
    public int getMaxConcurrentInstances() {
        return maxCurrentInstances;
    }

    @Override
    public String getJobId() {
        return String.valueOf(getInitializationTime().toEpochMilli());
    }

    @Override
    public Actor createJobActor() {
        return new MockActor();
    }

    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        if (jobActor == null || sender == null) {
            throw new NullPointerException();
        }
    }

}
