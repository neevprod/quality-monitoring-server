package jobs.qtivalidation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import com.pearson.itautomation.qtiscv.QtiScv;
import global.TestWithApplication;
import jobs.JobProtocol;
import models.HumanUser;
import models.qtivalidation.HistTenant;
import models.qtivalidation.Previewer;
import models.qtivalidation.PreviewerTenant;
import models.qtivalidation.UserTenantJobSubscription;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.api.libs.mailer.MailerClient;
import play.db.jpa.JPAApi;
import play.libs.mailer.Email;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;
import services.qtivalidation.HistTenantService;
import services.qtivalidation.UserTenantJobSubscriptionService;

import javax.inject.Provider;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(QtiScv.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class QtiScoringValidationActorTest extends TestWithApplication {
    private ActorRef actor;
    private QtiScv qtiScv;
    private HistTenantService histTenantService;
    private UserTenantJobSubscriptionService userTenantJobSubscriptionService;
    private Email email1;
    private Email email2;
    private MailerClient mailerClient;

    @Before
    public void setUp() throws Exception {
        qtiScv = PowerMockito.mock(QtiScv.class);
        List<Integer> histExecIds = new ArrayList<>();
        Mockito.doReturn(histExecIds).when(qtiScv).execute();

        histTenantService = Mockito.mock(HistTenantService.class);
        userTenantJobSubscriptionService = Mockito.mock(UserTenantJobSubscriptionService.class);

        email1 = Mockito.mock(Email.class);
        email2 = Mockito.mock(Email.class);
        @SuppressWarnings("unchecked") Provider<Email> emailProvider = Mockito.mock(Provider.class);
        Mockito.when(emailProvider.get())
                .thenReturn(email1)
                .thenReturn(email2);

        mailerClient = Mockito.mock(MailerClient.class);

        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        actor = actorSystem.actorOf(Props.create(QtiScoringValidationActor.class, qtiScv,
                app.injector().instanceOf(JPAApi.class), histTenantService, userTenantJobSubscriptionService,
                emailProvider, mailerClient, app.config()));
    }

    @Test
    public void onReceive_runAllMessage_executeScvLibraryForSubscribedTenants() throws Exception {
        // Given
        String jobId = "qtiScoringValidation";

        // When
        Future<Object> future = Patterns.ask(actor, new QtiScoringValidationActor.RunAllMessage(jobId), 3000);
        FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        Mockito.verify(qtiScv).execute();
    }

    @Test
    public void onReceive_runAllMessage_emailSubscribersWithResults() throws Exception {
        // Given
        List<Integer> histExecIds = new ArrayList<>();
        histExecIds.add(1);
        histExecIds.add(2);

        Mockito.doReturn(histExecIds).when(qtiScv).execute();
        Mockito.doReturn(getHistTenants()).when(histTenantService)
                .findAllByHistExecIds(histExecIds.stream().collect(Collectors.toSet()));
        Mockito.doReturn(getSubscriptions()).when(userTenantJobSubscriptionService)
                .fetchByJobTypeName("Score Consistency Validation");

        String jobId = "qtiScoringValidation";

        // When
        Future<Object> future = Patterns.ask(actor, new QtiScoringValidationActor.RunAllMessage(jobId), 3000);
        FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        String eol = System.getProperty("line.separator");

        Mockito.verify(email1).setSubject("previewer1 SCV Job for Tenant 1 Completed with Failures");
        Mockito.verify(email1).setFrom(app.config().getString("application.email.fromAddress"));
        Mockito.verify(email1).addBcc("user1@pearson.com");
        Mockito.verify(email1).setBodyText(
                "Previewer Name: previewer1" + eol +
                "Tenant Id: 1" + eol +
                "Tenant Name: tenant1" + eol +
                "Execution Date: 09/02/2015" + eol +
                "Execution Duration: 00:00:10" + eol +
                "Number of Items Validated: 10" + eol +
                "Number of Items with Failed Test Cases: 11" + eol +
                "Number of Items with Pending Test Cases: 12" + eol +
                "Number of Deactivated Items: 13" + eol +
                "Number of Item Exceptions: 14" + eol +
                "Execution Completed: true"
        );
        Mockito.verify(mailerClient).send(email1);

        Mockito.verify(email2).setSubject("previewer2 SCV Job for Tenant 2 Completed with No Failures");
        Mockito.verify(email2).setFrom(app.config().getString("application.email.fromAddress"));
        Mockito.verify(email2).addBcc("user2@pearson.com");
        Mockito.verify(email2).addBcc("user3@pearson.com");
        Mockito.verify(email2).setBodyText(
                "Previewer Name: previewer2" + eol +
                "Tenant Id: 2" + eol +
                "Tenant Name: tenant2" + eol +
                "Execution Date: 09/02/2015" + eol +
                "Execution Duration: 00:00:20" + eol +
                "Number of Items Validated: 20" + eol +
                "Number of Items with Failed Test Cases: 0" + eol +
                "Number of Items with Pending Test Cases: 22" + eol +
                "Number of Deactivated Items: 23" + eol +
                "Number of Item Exceptions: 0" + eol +
                "Execution Completed: true"
        );
        Mockito.verify(mailerClient).send(email2);
    }

    @Test
    public void onReceive_runAllMessageWithSuccessfulRun_sendFinishMessage() throws Exception {
        // Given
        String jobId = "qtiScoringValidation";

        // When
        Future<Object> future = Patterns.ask(actor, new QtiScoringValidationActor.RunAllMessage(jobId), 3000);
        JobProtocol.JobFinishMessage result = (JobProtocol.JobFinishMessage) FutureConverters.toJava(future)
                .toCompletableFuture().get();

        // Then
        assertThat("The returned JobFinishMessage should have the right job ID", result.getJobId(),
                equalTo(jobId));
        assertThat("The returned JobFinishMessage should have success set to true", result.isSuccess(),
                equalTo(true));
    }

    @Test
    public void onReceive_runAllMessageWithFailedRun_sendFailMessage() throws Exception {
        // Given
        Mockito.doThrow(new RuntimeException()).when(qtiScv).execute();
        String jobId = "qtiScoringValidation";

        // When
        Future<Object> future = Patterns.ask(actor, new QtiScoringValidationActor.RunAllMessage(jobId), 3000);
        JobProtocol.JobFinishMessage result = (JobProtocol.JobFinishMessage) FutureConverters.toJava(future)
                .toCompletableFuture().get();

        // Then
        assertThat("The returned JobFinishMessage should have the right job ID", result.getJobId(),
                equalTo(jobId));
        assertThat("The returned JobFinishMessage should have success set to false", result.isSuccess(),
                equalTo(false));
    }

    @Test
    public void onReceive_runMessage_executeScvLibraryForSingleTenant() throws Exception {
        // Given
        String jobId = "qtiScoringValidation_2_5";
        QtiScoringValidationActor.RunMessage runMessage = new QtiScoringValidationActor.RunMessage(jobId, 2, 5);

        // When
        Future<Object> future = Patterns.ask(actor, runMessage, 3000);
        FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        Mockito.verify(qtiScv).executeForTenant(2, 5);
    }

    @Test
    public void onReceive_runMessageWithSuccessfulRun_sendFinishMessage() throws Exception {
        // Given
        String jobId = "qtiScoringValidation_2_5";
        QtiScoringValidationActor.RunMessage runMessage = new QtiScoringValidationActor.RunMessage(jobId, 2, 5L);

        // When
        Future<Object> future = Patterns.ask(actor, runMessage, 3000);
        JobProtocol.JobFinishMessage result = (JobProtocol.JobFinishMessage) FutureConverters.toJava(future)
                .toCompletableFuture().get();

        // Then
        assertThat("The returned JobFinishMessage should have the right job ID", result.getJobId(),
                equalTo(jobId));
        assertThat("The returned JobFinishMessage should have success set to true", result.isSuccess(),
                equalTo(true));
    }

    @Test
    public void onReceive_runMessageWithFailedRun_sendFailMessage() throws Exception {
        // Given
        String jobId = "qtiScoringValidation_2_5";
        QtiScoringValidationActor.RunMessage runMessage = new QtiScoringValidationActor.RunMessage(jobId, 2, 5L);
        Mockito.doThrow(new RuntimeException()).when(qtiScv).executeForTenant(2, 5);

        // When
        Future<Object> future = Patterns.ask(actor, runMessage, 3000);
        JobProtocol.JobFinishMessage result = (JobProtocol.JobFinishMessage) FutureConverters.toJava(future)
                .toCompletableFuture().get();

        // Then
        assertThat("The returned JobFinishMessage should have the right job ID", result.getJobId(),
                equalTo(jobId));
        assertThat("The returned JobFinishMessage should have success set to false", result.isSuccess(),
                equalTo(false));
    }

    /**
     * Get a list of HistTenants.
     *
     * @return A list of HistTenants.
     */
    private List<HistTenant> getHistTenants() {
        Previewer previewer1 = new Previewer();
        previewer1.setPreviewerId(1);
        previewer1.setName("previewer1");
        PreviewerTenant tenant1 = new PreviewerTenant();
        tenant1.setPreviewerTenantId(1L);
        tenant1.setPreviewer(previewer1);
        tenant1.setTenantId(1L);
        tenant1.setName("tenant1");
        Previewer previewer2 = new Previewer();
        previewer2.setPreviewerId(2);
        previewer2.setName("previewer2");
        PreviewerTenant tenant2 = new PreviewerTenant();
        tenant2.setPreviewerTenantId(2L);
        tenant2.setPreviewer(previewer2);
        tenant2.setTenantId(2L);
        tenant2.setName("tenant2");

        // This epoch time represents 9/2/2015 at 2:16 PM Central.
        Instant instant = Instant.ofEpochMilli(1441221406000L);

        HistTenant histTenant1 = new HistTenant();
        histTenant1.setHistTenantId(1);
        histTenant1.setPreviewerTenant(tenant1);
        histTenant1.setExecStart(Timestamp.from(instant.minusSeconds(10)));
        histTenant1.setExecStop(Timestamp.from(instant));
        histTenant1.setExecCompleted(true);
        histTenant1.setItemsValidated(10);
        histTenant1.setItemsWithFailedTcs(11);
        histTenant1.setItemsWithPendingTcs(12);
        histTenant1.setDeactivatedItems(13);
        histTenant1.setItemExceptions(14);

        HistTenant histTenant2 = new HistTenant();
        histTenant2.setHistTenantId(2);
        histTenant2.setPreviewerTenant(tenant2);
        histTenant2.setExecStart(Timestamp.from(instant.minusSeconds(20)));
        histTenant2.setExecStop(Timestamp.from(instant));
        histTenant2.setExecCompleted(true);
        histTenant2.setItemsValidated(20);
        histTenant2.setItemsWithFailedTcs(0);
        histTenant2.setItemsWithPendingTcs(22);
        histTenant2.setDeactivatedItems(23);
        histTenant2.setItemExceptions(0);

        List<HistTenant> histTenants = new ArrayList<>();
        histTenants.add(histTenant1);
        histTenants.add(histTenant2);
        return histTenants;
    }

    /**
     * Get a list of UserTenantJobSubscriptions.
     *
     * @return A list of UserTenantJobSubscriptions.
     */
    private List<UserTenantJobSubscription> getSubscriptions() {
        HumanUser humanUser1 = new HumanUser();
        humanUser1.setEmail("user1@pearson.com");
        HumanUser humanUser2 = new HumanUser();
        humanUser2.setEmail("user2@pearson.com");
        HumanUser humanUser3 = new HumanUser();
        humanUser3.setEmail("user3@pearson.com");

        UserTenantJobSubscription subscription1 = new UserTenantJobSubscription();
        subscription1.setHumanUser(humanUser1);
        subscription1.setPreviewerTenantId(1L);
        UserTenantJobSubscription subscription2 = new UserTenantJobSubscription();
        subscription2.setHumanUser(humanUser2);
        subscription2.setPreviewerTenantId(2L);
        UserTenantJobSubscription subscription3 = new UserTenantJobSubscription();
        subscription3.setHumanUser(humanUser3);
        subscription3.setPreviewerTenantId(2L);

        List<UserTenantJobSubscription> userTenantJobSubscriptions = new ArrayList<>();
        userTenantJobSubscriptions.add(subscription1);
        userTenantJobSubscriptions.add(subscription2);
        userTenantJobSubscriptions.add(subscription3);
        return userTenantJobSubscriptions;
    }

}
