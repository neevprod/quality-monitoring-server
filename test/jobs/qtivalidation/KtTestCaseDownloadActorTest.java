package jobs.qtivalidation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.pearson.itautomation.qtiscv.QtiScv;
import global.TestWithApplication;
import jobs.JobProtocol;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.db.jpa.JPAApi;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;
import services.qtivalidation.KtTestCaseService;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(QtiScv.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class KtTestCaseDownloadActorTest extends TestWithApplication {
    private ActorRef actor;
    private KtTestCaseService ktTestCaseService;

    @Before
    public void setUp() {
        ktTestCaseService = Mockito.mock(KtTestCaseService.class);

        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        actor = actorSystem.actorOf(Props.create(KtTestCaseDownloadActor.class, ktTestCaseService, app.injector().instanceOf(JPAApi.class)));
    }

    @Test
    public void onReceive_process_KT_testCases_success() throws Exception {
        // Given
        String jobId = "ktTestCaseUpload";
        JsonElement jsonElement = new JsonParser().parse(getDummyTestCases());

        // When
        Future<Object> future = Patterns.ask(actor, new KtTestCaseDownloadActor.RunMessage(jobId, jsonElement), 3000);
        JobProtocol.JobFinishMessage result = (JobProtocol.JobFinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The returned JobFinishMessage should have the right job ID", result.getJobId(), equalTo(jobId));
        assertThat("The returned JobFinishMessage should have success set to true", result.isSuccess(), equalTo(true));
        Mockito.verify(ktTestCaseService).deleteByIdentifiers("0052-M00313");
    }

    @Test
    public void onReceive_process_KT_testCases_with_missing_ID_attribute_fail() throws Exception {
        // Given
        String jobId = "ktTestCaseUpload";
        String resultJson = "[\"Item level data should contain the following fields: '_id', 'includeInBuild', 'itemIdentifier', 'responseIdentifier', 'data'.\"]";
        JsonElement jsonElement = new JsonParser().parse(getMissingIdAttributeTestCase());

        // When
        Future<Object> future = Patterns.ask(actor, new KtTestCaseDownloadActor.RunMessage(jobId, jsonElement), 3000);
        JobProtocol.JobFinishMessage result = (JobProtocol.JobFinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The returned JobFinishMessage should have the right job ID", result.getJobId(), equalTo(jobId));
        assertThat("The returned JobFinishMessage should have success set to false", result.isSuccess(), equalTo(false));
        assertThat("The returned JobFinishMessage should have result JSON", result.getResultJson().toString().equals(resultJson));
    }

    @Test
    public void onReceive_process_KT_testCases_with_missing_candidateResponse_attribute_fail() throws Exception {
        // Given
        String jobId = "ktTestCaseUpload";
        String resultJson = "[\"ktId=0052-M00313#RESPONSE_B: KT Test case level data should contain the following fields: 'candidateResponse', 'responseId', 'savedScore', 'savedScoreState'.\"]";
        JsonElement jsonElement = new JsonParser().parse(getMissingCandidateResponseTestCases());

        // When
        Future<Object> future = Patterns.ask(actor, new KtTestCaseDownloadActor.RunMessage(jobId, jsonElement), 3000);
        JobProtocol.JobFinishMessage result = (JobProtocol.JobFinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The returned JobFinishMessage should have the right job ID", result.getJobId(), equalTo(jobId));
        assertThat("The returned JobFinishMessage should have success set to false", result.isSuccess(), equalTo(false));
        assertThat("The returned JobFinishMessage should have result JSON", result.getResultJson().toString().equals(resultJson));
    }

    private BufferedReader getMissingIdAttributeTestCase() {
        String testCaseJson = "[ { \"data\": [ { \"responseId\": \"5d2fe38c-735f-41ac-be20-968e384eb10d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><divide/><cn>1</cn><cn>12</cn></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mfrac><mn>1</mn><mn>12</mn></mfrac></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"0052-M00313\", \"responseIdentifier\": \"RESPONSE_B\" } ]";
        InputStream is = new ByteArrayInputStream(testCaseJson.getBytes());
        return new BufferedReader(new InputStreamReader(is));
    }

    private BufferedReader getDummyTestCases() {
        String testCaseJson = "[ { \"_id\": \"0052-M00313#RESPONSE_B\", \"data\": [ { \"responseId\": \"5d2fe38c-735f-41ac-be20-968e384eb10d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><divide/><cn>1</cn><cn>12</cn></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mfrac><mn>1</mn><mn>12</mn></mfrac></math>\" } }, { \"responseId\": \"e9195fb3-36c8-4790-9812-e3ce030fd4fd\", \"savedScore\": 0, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><plus/><apply><divide/><cn>3</cn><cn>4</cn></apply><apply><minus/><apply><divide/><cn>1</cn><cn>3</cn></apply></apply></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mrow><mfrac><mn>3</mn><mn>4</mn></mfrac><mrow><mo form=\\\"infix\\\">-</mo><mfrac><mn>1</mn><mn>3</mn></mfrac></mrow></mrow></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"0052-M00313\", \"responseIdentifier\": \"RESPONSE_B\" }, { \"_id\": \"02119ed1-0631-4db5-aaa6-e1da2ad2474d\", \"data\": [ { \"responseId\": \"defe2c08-9085-4b5e-a64a-8850b615403d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"0 2 1 0\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><eq/><apply><times class=\\\"sign\\\"/><apply><divide/><cn>32</cn><cn>5</cn></apply><apply><divide/><cn>2</cn><cn>4</cn></apply></apply><apply><divide/><cn>64</cn><cn>5</cn></apply></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mrow><mrow><mfrac><mn>32</mn><mn>5</mn></mfrac><mo class=\\\"sign\\\">×</mo><mfrac><mn>2</mn><mn>4</mn></mfrac></mrow><mo>=</mo><mfrac><mn>64</mn><mn>5</mn></mfrac></mrow></math>\" } }, { \"responseId\": \"7a9941db-4a8e-46f6-b8d7-f130bc975384\", \"savedScore\": 0, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"0\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><csymbol cd=\\\"basic\\\">box</csymbol></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mstyle style=\\\"border: 1px dotted black\\\"><mphantom><mi mathvariant=\\\"normal\\\">x</mi></mphantom></mstyle></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"VH262599\", \"responseIdentifier\": \"RESPONSE_B\" } ]";
        InputStream is = new ByteArrayInputStream(testCaseJson.getBytes());
        return new BufferedReader(new InputStreamReader(is));
    }

    private BufferedReader getMissingCandidateResponseTestCases() {
        String testCaseJson = "[ { \"_id\": \"0052-M00313#RESPONSE_B\", \"data\": [ { \"responseId\": \"5d2fe38c-735f-41ac-be20-968e384eb10d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\" }, { \"responseId\": \"e9195fb3-36c8-4790-9812-e3ce030fd4fd\", \"savedScore\": 0, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><plus/><apply><divide/><cn>3</cn><cn>4</cn></apply><apply><minus/><apply><divide/><cn>1</cn><cn>3</cn></apply></apply></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mrow><mfrac><mn>3</mn><mn>4</mn></mfrac><mrow><mo form=\\\"infix\\\">-</mo><mfrac><mn>1</mn><mn>3</mn></mfrac></mrow></mrow></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"0052-M00313\", \"responseIdentifier\": \"RESPONSE_B\" }, { \"_id\": \"02119ed1-0631-4db5-aaa6-e1da2ad2474d\", \"data\": [ { \"responseId\": \"defe2c08-9085-4b5e-a64a-8850b615403d\", \"savedScore\": 1, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"0 2 1 0\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><eq/><apply><times class=\\\"sign\\\"/><apply><divide/><cn>32</cn><cn>5</cn></apply><apply><divide/><cn>2</cn><cn>4</cn></apply></apply><apply><divide/><cn>64</cn><cn>5</cn></apply></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mrow><mrow><mfrac><mn>32</mn><mn>5</mn></mfrac><mo class=\\\"sign\\\">×</mo><mfrac><mn>2</mn><mn>4</mn></mfrac></mrow><mo>=</mo><mfrac><mn>64</mn><mn>5</mn></mfrac></mrow></math>\" } }, { \"responseId\": \"7a9941db-4a8e-46f6-b8d7-f130bc975384\", \"savedScore\": 0, \"savedScoreState\": \"savedScoreState_accepted\", \"candidateResponse\": { \"location\": \"0\", \"content\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><apply><csymbol cd=\\\"basic\\\">box</csymbol></apply></math>\", \"presentation\": \"<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\"><mstyle style=\\\"border: 1px dotted black\\\"><mphantom><mi mathvariant=\\\"normal\\\">x</mi></mphantom></mstyle></math>\" } } ], \"includeInBuild\": true, \"itemIdentifier\": \"VH262599\", \"responseIdentifier\": \"RESPONSE_B\" } ]";
        InputStream is = new ByteArrayInputStream(testCaseJson.getBytes());
        return new BufferedReader(new InputStreamReader(is));
    }


}
