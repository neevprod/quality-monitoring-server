package jobs;

import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class JobRunnerStateTest {
    private JobRunnerState jobRunnerState;

    @Before
    public void setUp() {
        jobRunnerState = new JobRunnerState();
    }

    /**
     * Get a fixed Clock that returns the given time in milliseconds.
     *
     * @param time The time in milliseconds.
     * @return A Clock.
     */
    private Clock getClock(long time) {
        return Clock.fixed(Instant.ofEpochMilli(time), ZoneId.systemDefault());
    }

    @Test
    public void getRunningJob_specifiedJobNotRunning_returnEmptyOptional() {
        // When
        Optional<JobStarter> jobStarterOptional = jobRunnerState.getRunningJob("1");

        // Then
        assertThat("The optional should be empty", jobStarterOptional.isPresent(), equalTo(false));
    }

    @Test
    public void getRunningJob_specifiedJobRunning_returnOptionalWithJobStarter() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        JobStarter jobStarter2 = new MockJobStarter(getClock(2L));
        JobStarter jobStarter3 = new MockJobStarter(getClock(3L));
        jobRunnerState.addRunningJob(jobStarter1);
        jobRunnerState.addRunningJob(jobStarter2);
        jobRunnerState.addRunningJob(jobStarter3);

        // When
        Optional<JobStarter> jobStarterOptional1 = jobRunnerState.getRunningJob(jobStarter1.getJobId());
        Optional<JobStarter> jobStarterOptional2 = jobRunnerState.getRunningJob(jobStarter2.getJobId());
        Optional<JobStarter> jobStarterOptional3 = jobRunnerState.getRunningJob(jobStarter3.getJobId());

        // Then
        assertThat("The first optional should contain the first job starter",
                jobStarterOptional1.get(), equalTo(jobStarter1));
        assertThat("The second optional should contain the second job starter",
                jobStarterOptional2.get(), equalTo(jobStarter2));
        assertThat("The third optional should contain the third job starter",
                jobStarterOptional3.get(), equalTo(jobStarter3));
    }

    @Test
    public void removeRunningJob_specifiedJobIsRunning_jobRemoved() {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        jobRunnerState.addRunningJob(jobStarter);

        // When
        jobRunnerState.removeRunningJob(jobStarter.getJobId());
        Optional<JobStarter> jobStarterOptional = jobRunnerState.getRunningJob(jobStarter.getJobId());

        // Then
        assertThat("The optional should be empty", jobStarterOptional.isPresent(), equalTo(false));
    }

    @Test
    public void removeRunningJob_specifiedJobIsNotRunning_noExceptionThrown() {
        // When
        jobRunnerState.removeRunningJob("1");
    }

    @Test
    public void getRunningJobCount_jobsRunning_returnCorrectCount() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        JobStarter jobStarter2 = new MockJobStarter(getClock(2L));
        JobStarter jobStarter3 = new MockJobStarter(getClock(3L));
        JobStarter jobStarter4 = new MockJobStarter(getClock(4L));
        ((MockJobStarter) jobStarter4).setJobType("otherJob");
        JobStarter jobStarter5 = new MockJobStarter(getClock(5L));
        ((MockJobStarter) jobStarter5).setJobType("otherJob");

        jobRunnerState.addRunningJob(jobStarter1);
        jobRunnerState.addRunningJob(jobStarter2);
        jobRunnerState.addRunningJob(jobStarter3);
        jobRunnerState.addRunningJob(jobStarter4);
        jobRunnerState.addRunningJob(jobStarter5);

        // When
        long count1 = jobRunnerState.getRunningJobCount("mockJob");
        long count2 = jobRunnerState.getRunningJobCount("otherJob");

        // Then
        assertThat("The first count should be correct", count1, equalTo(3L));
        assertThat("The second count should be correct", count2, equalTo(2L));
    }

    @Test
    public void getRunningJobCount_noJobsRunning_returnZero() {
        // When
        long count = jobRunnerState.getRunningJobCount("mockJob");

        // Then
        assertThat("The count should be 0", count, equalTo(0L));
    }

    @Test
    public void getRunningJobIds_jobsRunning_returnIds() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        JobStarter jobStarter2 = new MockJobStarter(getClock(2L));
        JobStarter jobStarter3 = new MockJobStarter(getClock(3L));

        jobRunnerState.addRunningJob(jobStarter1);
        jobRunnerState.addRunningJob(jobStarter2);
        jobRunnerState.addRunningJob(jobStarter3);

        // When
        Set<String> ids = jobRunnerState.getRunningJobIds();

        // Then
        assertThat("The set size should be 3", ids.size(), equalTo(3));
        assertThat("The first job should be in the set", ids.contains(jobStarter1.getJobId()), equalTo(true));
        assertThat("The second job should be in the set", ids.contains(jobStarter2.getJobId()), equalTo(true));
        assertThat("The third job should be in the set", ids.contains(jobStarter3.getJobId()), equalTo(true));
    }

    @Test
    public void getRunningJobIds_noJobsRunning_returnEmptySet() {
        // When
        Set<String> ids = jobRunnerState.getRunningJobIds();

        // Then
        assertThat("The set size should be 0", ids.size(), equalTo(0));
    }

    @Test
    public void getQueuedJobCount_jobsQueued_returnCorrectCount() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        JobStarter jobStarter2 = new MockJobStarter(getClock(2L));
        JobStarter jobStarter3 = new MockJobStarter(getClock(3L));
        JobStarter jobStarter4 = new MockJobStarter(getClock(4L));
        ((MockJobStarter) jobStarter4).setJobType("otherJob");
        JobStarter jobStarter5 = new MockJobStarter(getClock(5L));
        ((MockJobStarter) jobStarter5).setJobType("otherJob");

        jobRunnerState.queueJob(jobStarter1);
        jobRunnerState.queueJob(jobStarter2);
        jobRunnerState.queueJob(jobStarter3);
        jobRunnerState.queueJob(jobStarter4);
        jobRunnerState.queueJob(jobStarter5);

        // When
        int count1 = jobRunnerState.getQueuedJobCount("mockJob");
        int count2 = jobRunnerState.getQueuedJobCount("otherJob");

        // Then
        assertThat("The first count should be correct", count1, equalTo(3));
        assertThat("The second count should be correct", count2, equalTo(2));
    }

    @Test
    public void getQueuedJobCount_noJobsQueued_returnZero() {
        // When
        int count = jobRunnerState.getQueuedJobCount("mockJob");

        // Then
        assertThat("The count should be 0", count, equalTo(0));
    }

    @Test
    public void removeQueuedJob_jobsQueued_removeJobFromQueueAndReturnJobStarter() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        JobStarter jobStarter2 = new MockJobStarter(getClock(2L));
        JobStarter jobStarter3 = new MockJobStarter(getClock(3L));
        JobStarter jobStarter4 = new MockJobStarter(getClock(4L));
        ((MockJobStarter) jobStarter4).setJobType("otherJob");
        JobStarter jobStarter5 = new MockJobStarter(getClock(5L));
        ((MockJobStarter) jobStarter5).setJobType("otherJob");

        jobRunnerState.queueJob(jobStarter1);
        jobRunnerState.queueJob(jobStarter2);
        jobRunnerState.queueJob(jobStarter3);
        jobRunnerState.queueJob(jobStarter4);
        jobRunnerState.queueJob(jobStarter5);

        // When
        Optional<JobStarter> jobStarterOptional1 = jobRunnerState.removeQueuedJob("mockJob");
        Optional<JobStarter> jobStarterOptional2 = jobRunnerState.removeQueuedJob("mockJob");
        Optional<JobStarter> jobStarterOptional3 = jobRunnerState.removeQueuedJob("mockJob");
        Optional<JobStarter> jobStarterOptional4 = jobRunnerState.removeQueuedJob("mockJob");

        Optional<JobStarter> jobStarterOptional5 = jobRunnerState.removeQueuedJob("otherJob");
        Optional<JobStarter> jobStarterOptional6 = jobRunnerState.removeQueuedJob("otherJob");
        Optional<JobStarter> jobStarterOptional7 = jobRunnerState.removeQueuedJob("otherJob");

        // Then
        assertThat("The first optional should contain the first job starter",
                jobStarterOptional1.get(), equalTo(jobStarter1));
        assertThat("The second optional should contain the second job starter",
                jobStarterOptional2.get(), equalTo(jobStarter2));
        assertThat("The third optional should contain the third job starter",
                jobStarterOptional3.get(), equalTo(jobStarter3));
        assertThat("The fourth optional should be empty", jobStarterOptional4.isPresent(), equalTo(false));

        assertThat("The fifth optional should contain the fourth job starter",
                jobStarterOptional5.get(), equalTo(jobStarter4));
        assertThat("The sixth optional should contain the fifth job starter",
                jobStarterOptional6.get(), equalTo(jobStarter5));
        assertThat("The seventh optional should be empty", jobStarterOptional7.isPresent(), equalTo(false));
    }

    @Test
    public void getQueuedJobIds_jobsQueued_returnJobIds() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        JobStarter jobStarter2 = new MockJobStarter(getClock(2L));
        JobStarter jobStarter3 = new MockJobStarter(getClock(3L));
        jobRunnerState.queueJob(jobStarter1);
        jobRunnerState.queueJob(jobStarter2);
        jobRunnerState.queueJob(jobStarter3);

        // When
        Set<String> ids = jobRunnerState.getQueuedJobIds();

        // Then
        assertThat("The set size should be 3", ids.size(), equalTo(3));
        assertThat("The first job should be in the set", ids.contains(jobStarter1.getJobId()), equalTo(true));
        assertThat("The second job should be in the set", ids.contains(jobStarter2.getJobId()), equalTo(true));
        assertThat("The third job should be in the set", ids.contains(jobStarter3.getJobId()), equalTo(true));
    }

    @Test
    public void getQueuedJobIds_noJobsQueued_returnEmptySet() {
        // When
        Set<String> ids = jobRunnerState.getQueuedJobIds();

        // Then
        assertThat("The set size should be 0", ids.size(), equalTo(0));
    }

    @Test
    public void jobIsQueuedOrRunning_jobRunning_returnTrue() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        jobRunnerState.addRunningJob(jobStarter1);

        // When
        boolean isQueuedOrRunning = jobRunnerState.jobIsQueuedOrRunning(jobStarter1.getJobId());

        // Then
        assertThat("The returned value should be true", isQueuedOrRunning, equalTo(true));
    }

    @Test
    public void jobIsQueuedOrRunning_jobQueued_returnTrue() {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        jobRunnerState.queueJob(jobStarter1);

        // When
        boolean isQueuedOrRunning = jobRunnerState.jobIsQueuedOrRunning(jobStarter1.getJobId());

        // Then
        assertThat("The returned value should be true", isQueuedOrRunning, equalTo(true));
    }

    @Test
    public void jobIsQueuedOrRunning_jobNotQueuedOrRunning_returnFalse() {
        // When
        boolean isQueuedOrRunning = jobRunnerState.jobIsQueuedOrRunning("1");

        // Then
        assertThat("The returned value should be false", isQueuedOrRunning, equalTo(false));
    }

}
