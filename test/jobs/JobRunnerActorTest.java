package jobs;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.testkit.TestActorRef;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import global.TestWithApplication;
import info.solidsoft.mockito.java8.LambdaMatcher;
import models.JobExecution;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import play.db.jpa.JPAApi;
import play.libs.Json;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;
import services.JobExecutionService;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class JobRunnerActorTest extends TestWithApplication {
    private JobExecutionService jobExecutionService;
    private JobRunnerState state;
    ActorSystem actorSystem;
    private ActorRef actor;
    private long actorAskTimeout;

    @Before
    public void setUp() throws Exception {
        jobExecutionService = Mockito.mock(JobExecutionService.class);
        state = new JobRunnerState();

        actorSystem = app.injector().instanceOf(ActorSystem.class);
        actor = actorSystem.actorOf(Props.create(JobRunnerActor.class, app.injector().instanceOf(JPAApi.class),
                jobExecutionService, state));

        actorAskTimeout = app.config().getDuration("application.actorAskTimeout").toMillis();
    }

    /**
     * Get a fixed Clock that returns the given time in milliseconds.
     *
     * @param time The time in milliseconds.
     * @return A Clock.
     */
    private Clock getClock(long time) {
        return Clock.fixed(Instant.ofEpochMilli(time), ZoneId.systemDefault());
    }

    @Test
    public void sendJobStartMessage_jobNotAlreadyStarted_startJobAndReturnJobId() throws Exception {
        // Given
        JobStarter jobStarter = Mockito.spy(new MockJobStarter(getClock(1L)));

        // When
        Future<Object> future = Patterns.ask(actor, new JobProtocol.JobStartMessage(jobStarter), actorAskTimeout);
        String response = (String) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The job should be in the collection of running jobs",
                state.getRunningJob(jobStarter.getJobId()).isPresent(), equalTo(true));
        assertThat("The response from the job runner should be the job ID", response, equalTo(jobStarter.getJobId()));

        Mockito.verify(jobStarter).startJob(Mockito.any(), Mockito.any());
    }

    @Test
    public void sendJobStartMessage_jobAlreadyStarted_doNotStartJobAndReturnJobId() throws Exception {
        // Given
        JobStarter jobStarter = Mockito.spy(new MockJobStarter(getClock(1L)));
        state.addRunningJob(jobStarter);

        // When
        Future<Object> future = Patterns.ask(actor, new JobProtocol.JobStartMessage(jobStarter), actorAskTimeout);
        String response = (String) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The response from the job runner should be the job ID", response, equalTo(jobStarter.getJobId()));

        Mockito.verify(jobStarter, Mockito.never()).startJob(Mockito.any(), Mockito.any());
    }

    @Test
    public void sendJobStartMessage_maxConcurrentThresholdMet_queueJob() throws Exception {
        // Given
        JobStarter runningJobStarter = new MockJobStarter(getClock(1L));
        state.addRunningJob(runningJobStarter);

        JobStarter jobStarter = Mockito.spy(new MockJobStarter(getClock(2L)));

        // When
        Future<Object> future = Patterns.ask(actor, new JobProtocol.JobStartMessage(jobStarter), actorAskTimeout);
        FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The job should be in the collection of queued jobs",
                state.getQueuedJobIds().contains(jobStarter.getJobId()), equalTo(true));
    }

    @Test
    public void sendJobFinishMessage_removeFromRunningJobs() {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        state.addRunningJob(jobStarter);

        Mockito.when(jobExecutionService.findLatest(jobStarter.getJobId())).thenReturn(Optional.empty());

        TestActorRef<JobRunnerActor> testActorRef = TestActorRef.create(actorSystem,
                Props.create(JobRunnerActor.class, app.injector().instanceOf(JPAApi.class),
                        jobExecutionService, state));

        // When
        testActorRef.tell(new JobProtocol.JobFinishMessage(jobStarter.getJobId(), true), null);

        // Then
        assertThat("The job should not be in the collection of running jobs",
                state.getRunningJobIds().contains(jobStarter.getJobId()), equalTo(false));
    }

    @Test
    public void sendJobFinishMessage_firstTimeJobWasRun_saveJobExecutionDetails() {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        state.addRunningJob(jobStarter);

        Mockito.when(jobExecutionService.findLatest(jobStarter.getJobId())).thenReturn(Optional.empty());

        TestActorRef<JobRunnerActor> testActorRef = TestActorRef.create(actorSystem,
                Props.create(JobRunnerActor.class, app.injector().instanceOf(JPAApi.class),
                        jobExecutionService, state));

        // When
        testActorRef.tell(new JobProtocol.JobFinishMessage(jobStarter.getJobId(), true), null);

        // Then
        Mockito.verify(jobExecutionService).create(
                LambdaMatcher.argLambda(je -> je.getSuccess() && je.getJobId().equals(jobStarter.getJobId())
                        && je.getResultJson().equals("") && je.getRunNumber().equals(1)));
    }

    @Test
    public void sendJobFinishMessage_jobWasRunPreviously_saveJobExecutionDetails() {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        state.addRunningJob(jobStarter);

        JobExecution jobExecution = new JobExecution();
        jobExecution.setRunNumber(3);
        Mockito.when(jobExecutionService.findLatest(jobStarter.getJobId())).thenReturn(Optional.of(jobExecution));

        TestActorRef<JobRunnerActor> testActorRef = TestActorRef.create(actorSystem,
                Props.create(JobRunnerActor.class, app.injector().instanceOf(JPAApi.class),
                        jobExecutionService, state));

        // When
        testActorRef.tell(new JobProtocol.JobFinishMessage(jobStarter.getJobId(), true), null);

        // Then
        Mockito.verify(jobExecutionService).create(
                LambdaMatcher.argLambda(je -> je.getSuccess() && je.getJobId().equals(jobStarter.getJobId())
                        && je.getResultJson().equals("") && je.getRunNumber().equals(4)));
    }

    @Test
    public void sendJobFinishMessage_jobFailed_saveJobExecutionDetails() {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        state.addRunningJob(jobStarter);

        Mockito.when(jobExecutionService.findLatest(jobStarter.getJobId())).thenReturn(Optional.empty());

        TestActorRef<JobRunnerActor> testActorRef = TestActorRef.create(actorSystem,
                Props.create(JobRunnerActor.class, app.injector().instanceOf(JPAApi.class),
                        jobExecutionService, state));

        // When
        testActorRef.tell(new JobProtocol.JobFinishMessage(jobStarter.getJobId(), false), null);

        // Then
        Mockito.verify(jobExecutionService).create(
                LambdaMatcher.argLambda(je -> !je.getSuccess() && je.getJobId().equals(jobStarter.getJobId())
                        && je.getResultJson().equals("") && je.getRunNumber().equals(1)));
    }

    @Test
    public void sendJobFinishMessage_hasJsonResult_saveJobExecutionDetails() {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        state.addRunningJob(jobStarter);

        ObjectNode jobData = Json.newObject();
        jobData.put("field1", 1);
        jobData.put("field2", 2);
        JsonNode resultJson = JobProtocol.getJobResultJson(jobData);

        Mockito.when(jobExecutionService.findLatest(jobStarter.getJobId())).thenReturn(Optional.empty());

        TestActorRef<JobRunnerActor> testActorRef = TestActorRef.create(actorSystem,
                Props.create(JobRunnerActor.class, app.injector().instanceOf(JPAApi.class),
                        jobExecutionService, state));

        // When
        testActorRef.tell(new JobProtocol.JobFinishMessage(jobStarter.getJobId(), true, resultJson), null);

        // Then
        Mockito.verify(jobExecutionService).create(
                LambdaMatcher.argLambda(je -> je.getSuccess() && je.getJobId().equals(jobStarter.getJobId())
                        && je.getResultJson().equals(Json.stringify(resultJson)) && je.getRunNumber().equals(1)));
    }

    @Test
    public void sendJobFinishMessage_jobQueued_startQueuedJob() {
        // Given
        JobStarter queuedJobStarter = Mockito.spy(new MockJobStarter(getClock(1L)));
        state.queueJob(queuedJobStarter);

        JobStarter jobStarter = new MockJobStarter(getClock(2L));
        state.addRunningJob(jobStarter);

        Mockito.when(jobExecutionService.findLatest(jobStarter.getJobId())).thenReturn(Optional.empty());

        // When
        TestActorRef<JobRunnerActor> testActorRef = TestActorRef.create(actorSystem,
                Props.create(JobRunnerActor.class, app.injector().instanceOf(JPAApi.class),
                        jobExecutionService, state));
        testActorRef.tell(new JobProtocol.JobFinishMessage(jobStarter.getJobId(), true), null);

        // Then
        Mockito.verify(queuedJobStarter).startJob(Mockito.any(), Mockito.any());
    }

    @Test
    public void sendJobCheckMessage_jobQueued_returnJsonWithIsRunningTrue() throws Exception {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        state.queueJob(jobStarter);

        // When
        Future<Object> future =
                Patterns.ask(actor, new JobProtocol.JobCheckMessage(jobStarter.getJobId()), actorAskTimeout);
        JsonNode response = (JsonNode) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The response should indicate that the job is running", response.get("isRunning").asBoolean(),
                equalTo(true));
    }

    @Test
    public void sendJobCheckMessage_jobRunning_returnJsonWithIsRunningTrue() throws Exception {
        // Given
        JobStarter jobStarter = new MockJobStarter(getClock(1L));
        state.addRunningJob(jobStarter);

        // When
        Future<Object> future =
                Patterns.ask(actor, new JobProtocol.JobCheckMessage(jobStarter.getJobId()), actorAskTimeout);
        JsonNode response = (JsonNode) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The response should indicate that the job is running", response.get("isRunning").asBoolean(),
                equalTo(true));
    }

    @Test
    public void sendJobCheckMessage_jobNotRunningAndHasNotBeenRun_returnFalse() throws Exception {
        // Give
        String jobId = "1";
        Mockito.when(jobExecutionService.findLatest(jobId)).thenReturn(Optional.empty());

        // When
        Future<Object> future =
                Patterns.ask(actor, new JobProtocol.JobCheckMessage(jobId), actorAskTimeout);
        boolean response = (boolean) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The response should be false", response, equalTo(false));
    }

    @Test
    public void sendJobCheckMessage_jobFinishedAndSucceeded_returnJsonWithDetails() throws Exception {
        // Given
        String jobId = "1";

        ObjectNode jobData = Json.newObject();
        jobData.put("field1", 1);
        jobData.put("field2", 2);
        JsonNode resultJson = JobProtocol.getJobResultJson(jobData);

        JobExecution jobExecution = new JobExecution();
        jobExecution.setSuccess(true);
        jobExecution.setResultJson(Json.stringify(resultJson));

        Mockito.when(jobExecutionService.findLatest(jobId)).thenReturn(Optional.of(jobExecution));

        // When
        Future<Object> future =
                Patterns.ask(actor, new JobProtocol.JobCheckMessage(jobId), actorAskTimeout);
        JsonNode response = (JsonNode) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The response should indicate that the job is not running", response.get("isRunning").asBoolean(),
                equalTo(false));
        assertThat("The response should indicate that the job succeeded", response.get("jobSuccessful").asBoolean(),
                equalTo(true));
        assertThat("The response should include the job data", Json.stringify(response.get("jobData")),
                equalTo(Json.stringify(jobData)));
    }

    @Test
    public void sendJobCheckMessage_jobFinishedAndSucceededWithError_returnJsonWithDetails() throws Exception {
        // Given
        String jobId = "1";

        ObjectNode jobData = Json.newObject();
        jobData.put("field1", 1);
        jobData.put("field2", 2);
        String jobError = "An error occurred while running the job.";
        JsonNode resultJson = JobProtocol.getJobResultJson(jobData, jobError);

        JobExecution jobExecution = new JobExecution();
        jobExecution.setSuccess(true);
        jobExecution.setResultJson(Json.stringify(resultJson));

        Mockito.when(jobExecutionService.findLatest(jobId)).thenReturn(Optional.of(jobExecution));

        // When
        Future<Object> future =
                Patterns.ask(actor, new JobProtocol.JobCheckMessage(jobId), actorAskTimeout);
        JsonNode response = (JsonNode) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The response should indicate that the job is not running", response.get("isRunning").asBoolean(),
                equalTo(false));
        assertThat("The response should indicate that the job succeeded", response.get("jobSuccessful").asBoolean(),
                equalTo(true));
        assertThat("The response should include the job data", Json.stringify(response.get("jobData")),
                equalTo(Json.stringify(jobData)));
        assertThat("The response should include the job error",
                response.get("jobErrors").get(0).get("message").asText(), equalTo(jobError));
    }

    @Test
    public void sendJobCheckMessage_jobFinishedAndFailed_returnJsonWithDetails() throws Exception {
        // Given
        String jobId = "1";

        String jobError = "An error occurred while running the job.";
        JsonNode resultJson = JobProtocol.getJobResultJson(jobError);

        JobExecution jobExecution = new JobExecution();
        jobExecution.setSuccess(false);
        jobExecution.setResultJson(Json.stringify(resultJson));

        Mockito.when(jobExecutionService.findLatest(jobId)).thenReturn(Optional.of(jobExecution));

        // When
        Future<Object> future =
                Patterns.ask(actor, new JobProtocol.JobCheckMessage(jobId), actorAskTimeout);
        JsonNode response = (JsonNode) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The response should indicate that the job is not running", response.get("isRunning").asBoolean(),
                equalTo(false));
        assertThat("The response should indicate that the job failed", response.get("jobSuccessful").asBoolean(),
                equalTo(false));
        assertThat("The response should include the job error",
                response.get("jobErrors").get(0).get("message").asText(), equalTo(jobError));
    }

    @Test
    public void sendGetRunningJobsMessage_jobsRunningAndQueued_returnRunningAndQueuedJobs() throws Exception {
        // Given
        JobStarter jobStarter1 = new MockJobStarter(getClock(1L));
        JobStarter jobStarter2 = new MockJobStarter(getClock(2L));
        JobStarter jobStarter3 = new MockJobStarter(getClock(3L));
        JobStarter jobStarter4 = new MockJobStarter(getClock(4L));

        state.addRunningJob(jobStarter1);
        state.addRunningJob(jobStarter2);
        state.queueJob(jobStarter3);
        state.queueJob(jobStarter4);

        // When
        Future<Object> future = Patterns.ask(actor, JobRunnerActor.GET_RUNNING_JOBS_MESSAGE, actorAskTimeout);
        JsonNode response = (JsonNode) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        Set<String> runningJobs = new HashSet<>();
        for (JsonNode node : response.get("runningJobs")) {
            runningJobs.add(node.asText());
        }
        assertThat("The number of returned running jobs should be 2", runningJobs.size(), equalTo(2));
        assertThat("The response should contain the first running job", runningJobs.contains(jobStarter1.getJobId()),
                equalTo(true));
        assertThat("The response should contain the second running job", runningJobs.contains(jobStarter2.getJobId()),
                equalTo(true));

        Set<String> queuedJobs = new HashSet<>();
        for (JsonNode node : response.get("queuedJobs")) {
            queuedJobs.add(node.asText());
        }
        assertThat("The number of returned queued jobs should be 2", queuedJobs.size(), equalTo(2));
        assertThat("The response should contain the first queued job", queuedJobs.contains(jobStarter3.getJobId()),
                equalTo(true));
        assertThat("The response should contain the second queued job", queuedJobs.contains(jobStarter4.getJobId()),
                equalTo(true));
    }

    @Test
    public void sendGetRunningJobsMessage_jobsNotRunningOrQueued_returnEmpty() throws Exception {
        // When
        Future<Object> future = Patterns.ask(actor, JobRunnerActor.GET_RUNNING_JOBS_MESSAGE, actorAskTimeout);
        JsonNode response = (JsonNode) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The number of returned running jobs should be 0", response.get("runningJobs").size(), equalTo(0));
        assertThat("The number of returned queued jobs should be 0", response.get("queuedJobs").size(), equalTo(0));
    }

}
