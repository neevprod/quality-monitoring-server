package jobs.systemvalidation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.pattern.Patterns;
import com.google.gson.JsonObject;
import com.pearson.itautomation.testmaps.models.enums.ItemStatus;
import com.typesafe.config.Config;
import global.TestWithApplication;
import jobs.JobProtocol.JobFinishMessage;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import jobs.systemvalidation.DataIntegrityValidationActor.RunMessage;
import models.HumanUser;
import models.systemvalidation.*;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import play.db.jpa.JPAApi;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;
import services.systemvalidation.*;
import services.usermanagement.HumanUserService;
import util.ConnectionPoolManager;
import util.systemvalidation.BackendData;
import util.systemvalidation.DivJobSetupUtil.StatusNames;

import javax.inject.Provider;
import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class DataIntegrityValidationActorTest extends TestWithApplication {
    private DivJobExecService divJobExecService;
    private EnvironmentService environmentService;
    private StudentScenarioService studentScenarioService;
    private StudentScenarioResultService studentScenarioResultService;
    private ScenarioService scenarioService;
    private HumanUserService humanUserService;
    private DivJobExecStatusService divJobExecStatusService;
    private StudentScenarioStatusService studentScenarioStatusService;
    private Provider<Email> emailProvider;
    private ActorRef actor;
    private Email email;
    private BackendData.Factory backendDataFactory;
    private BackendData backendData;

    public static class MockActor extends UntypedAbstractActor {
        @Override
        public void onReceive(Object message) throws Exception {
            if (message instanceof StudentScenario) {
                StudentScenario studentScenario = (StudentScenario) message;
                JsonObject finalResult = new JsonObject();
                if (studentScenario.getStudentScenarioStatusId() != null
                        && studentScenario.getStudentScenarioStatusId() == 3) {
                    finalResult.addProperty("success", "no");
                    FinishMessage finishMessage = new FinishMessage(studentScenario,
                            studentScenario.getDivJobExec().getDivJobExecId(),
                            DataIntegrityValidationActor.FAILED_MESSAGE, finalResult.toString());
                    getSender().tell(finishMessage, getSelf());
                } else {
                    finalResult.addProperty("success", "yes");
                    FinishMessage finishMessage = new FinishMessage(studentScenario,
                            studentScenario.getDivJobExec().getDivJobExecId(),
                            DataIntegrityValidationActor.FINISHED_MESSAGE, finalResult.toString());

                    getSender().tell(finishMessage, getSelf());
                }
            } else if (message instanceof BackendValidationActor.BackendInputMessage) {
                BackendValidationActor.BackendInputMessage msg = (BackendValidationActor.BackendInputMessage) message;
                StudentScenario studentScenario = msg.getStudentScenario();
                JsonObject finalResult = new JsonObject();
                finalResult.addProperty("success", "yes");
                FinishMessage finishMessage = new FinishMessage(studentScenario,
                        studentScenario.getDivJobExec().getDivJobExecId(),
                        DataIntegrityValidationActor.FINISHED_MESSAGE, finalResult.toString());

                getSender().tell(finishMessage, getSelf());
            } else if (message instanceof StatsValidationActor.StatsInputMessage) {
                StatsValidationActor.StatsInputMessage msg = (StatsValidationActor.StatsInputMessage) message;
                StudentScenario studentScenario = msg.getStudentScenario();
                JsonObject finalResult = new JsonObject();
                finalResult.addProperty("success", "yes");
                FinishMessage finishMessage = new FinishMessage(studentScenario,
                        studentScenario.getDivJobExec().getDivJobExecId(),
                        DataIntegrityValidationActor.FINISHED_MESSAGE, finalResult.toString());

                getSender().tell(finishMessage, getSelf());

            }
        }
    }

    @Before
    public void setup() {
        divJobExecService = Mockito.mock(DivJobExecService.class);
        environmentService = Mockito.mock(EnvironmentService.class);
        studentScenarioService = Mockito.mock(StudentScenarioService.class);
        studentScenarioResultService = Mockito.mock(StudentScenarioResultService.class);
        scenarioService = Mockito.mock(ScenarioService.class);
        humanUserService = Mockito.mock(HumanUserService.class);
        divJobExecStatusService = Mockito.mock(DivJobExecStatusService.class);
        studentScenarioStatusService = Mockito.mock(StudentScenarioStatusService.class);
        emailProvider = Mockito.mock(Provider.class);
        backendDataFactory = Mockito.mock(BackendData.Factory.class);
        MailerClient mailerClient = Mockito.mock(MailerClient.class);
        email = Mockito.mock(Email.class);
        ConnectionPoolManager connectionPoolManager = Mockito.mock(ConnectionPoolManager.class);

        Config configuration = Mockito.mock(Config.class);
        Mockito.doReturn(1).when(configuration)
                .getInt("application.jobs.dataIntegrityValidation.maxConcurrentStudents");

        Environment environment = Mockito.mock(Environment.class);
        Mockito.doReturn(Optional.of(environment)).when(environmentService).findById(1);
        DbConnection dbConnection = Mockito.mock(DbConnection.class);
        Mockito.doReturn(dbConnection).when(environment).getBeMysqlConnection();
        Mockito.doReturn(Optional.empty()).when(divJobExecStatusService)
                .findByDivJobExecStatusName(StatusNames.IN_PROGRESS.getName());
        Mockito.doReturn(Optional.empty()).when(divJobExecStatusService)
                .findByDivJobExecStatusName(StatusNames.FAILED.getName());
        Mockito.doReturn(Optional.empty()).when(divJobExecStatusService)
                .findByDivJobExecStatusName(StatusNames.COMPLETED.getName());

        Mockito.doReturn(Optional.empty()).when(studentScenarioStatusService)
                .findByStudentScenarioStatusName(StatusNames.IN_PROGRESS.getName());
        Mockito.doReturn(Optional.empty()).when(studentScenarioStatusService)
                .findByStudentScenarioStatusName(StatusNames.FAILED.getName());
        Mockito.doReturn(Optional.empty()).when(studentScenarioStatusService)
                .findByStudentScenarioStatusName(StatusNames.COMPLETED.getName());

        backendData = Mockito.mock(BackendData.class);
        Mockito.doReturn(backendData).when(backendDataFactory).create(dbConnection);

        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);

        actor = actorSystem.actorOf(Props.create(DataIntegrityValidationActor.class,
                app.injector().instanceOf(JPAApi.class), divJobExecService, environmentService, studentScenarioService,
                studentScenarioResultService, humanUserService, divJobExecStatusService, studentScenarioStatusService,
                scenarioService,
                app.injector().instanceOf(Tn8ApiDataLoaderActor.Factory.class),
                app.injector().instanceOf(IrisValidationActor.Factory.class),
                app.injector().instanceOf(BackendValidationActor.Factory.class),
                app.injector().instanceOf(Epen2ValidationActor.Factory.class),
                app.injector().instanceOf(StatsValidationActor.Factory.class),
                app.injector().instanceOf(BatteryStatsValidationActor.Factory.class),
                backendDataFactory,
                emailProvider, mailerClient,
                connectionPoolManager, configuration));
    }

    @Test
    public void divJobExecution_returnSuccessResult() throws Exception {
        // Given
        Optional<StudentScenario> studentScenarioResult = getStudentScenario(1, 2, true);
        StudentScenario studentScenario = studentScenarioResult.get();
        Optional<DivJobExec> divJobExec = getDivJobExecWithStudentScenario(false, 1, true);
        Mockito.doReturn(divJobExec).when(divJobExecService).findByDivJobExecId(100L);
        Mockito.doReturn(getHumanUser()).when(humanUserService).findByUserId(1);
        Mockito.doReturn(studentScenarioResult).when(studentScenarioService).findByStudentScenarioId(1L);
        Mockito.doReturn(Optional.of(studentScenario.getTestnavScenario())).when(scenarioService).findByScenarioId(1);
        Mockito.doReturn(email).when(emailProvider).get();
        Mockito.doReturn(Optional.empty()).when(studentScenarioResultService)
                .findMaxVersionByStudentScenarioIdAndStateId(Matchers.anyLong(), Matchers.anyInt());
        Set<ItemStatus> itemStatuses = new HashSet<>();
        itemStatuses.add(ItemStatus.OPERATIONAL);
        Mockito.doReturn(itemStatuses).when(backendData).getItemStatusesEligibleForEpenScoring(divJobExec.get().getScopeTreePath());

        // When
        Future<Object> future = Patterns.ask(actor, getRunMessage(), 3000);
        JobFinishMessage result = (JobFinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Last state of the DIV job execution should be stats",
                divJobExec.get().getStudentScenarios().get(0).getState().getStateName(),
                equalTo(DataIntegrityValidationActor.DivStateName.STATS.name()));
        assertThat("Value of success in result should be true", result.isSuccess(), equalTo(true));

        MatcherAssert.assertThat("Success message is Finished", result.getResultJson(), CoreMatchers.equalTo(null));
    }

    @Test
    public void divJobExecution_FailedAt_TN8_API_DATA_LOADER_returnFailResult() throws Exception {
        // Given
        Optional<DivJobExec> divJobExec = getDivJobExecWithStudentScenario(false, 1, false);
        Mockito.doReturn(divJobExec).when(divJobExecService).findByDivJobExecId(Matchers.anyLong());
        Mockito.doReturn(getHumanUser()).when(humanUserService).findByUserId(1);
        Mockito.doReturn(getStudentScenario(1, 3, false)).when(studentScenarioService).findByStudentScenarioId(1L);
        Mockito.doReturn(email).when(emailProvider).get();
        Mockito.doReturn(Optional.empty()).when(studentScenarioResultService)
                .findMaxVersionByStudentScenarioIdAndStateId(Matchers.anyLong(), Matchers.anyInt());
        Mockito.doReturn(3).when(divJobExec.get().getStudentScenarios().get(0)).getStudentScenarioStatusId();

        // When
        Future<Object> future = Patterns.ask(actor, getRunMessage(), 3000);
        JobFinishMessage result = (JobFinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Last state of the DIV job execution should be TN8_API_DATA_LOADER",
                divJobExec.get().getStudentScenarios().get(0).getState().getStateName(),
                equalTo(DataIntegrityValidationActor.DivStateName.TN8_API_DATA_LOADER.name()));
        assertThat(
                "The Last completed state should be null when the DIV job execution failed at TN8_API_DATA_LOADER"
                        + " state",
                divJobExec.get().getStudentScenarios().get(0).getLastCompletedStateId(), equalTo(null));
        assertThat("Value of success in result should be false", result.isSuccess(), equalTo(false));
        MatcherAssert.assertThat("Success message is Finished", result.getResultJson(), CoreMatchers.equalTo(null));
    }

    @Test
    public void divJobExecution_multiple_studentScenarios_withEpenScenario_returnSuccessResult() throws Exception {
        // Given
        Optional<DivJobExec> divJobExec = getDivJobExecWithStudentScenario(false, 30, true);
        Mockito.doReturn(divJobExec).when(divJobExecService).findByDivJobExecId(Matchers.anyLong());
        Mockito.doReturn(getHumanUser()).when(humanUserService).findByUserId(1);
        Set<ItemStatus> itemStatuses = new HashSet<>();
        itemStatuses.add(ItemStatus.OPERATIONAL);
        Mockito.doReturn(itemStatuses).when(backendData).getItemStatusesEligibleForEpenScoring(divJobExec.get().getScopeTreePath());

        Map<Long, StudentScenario> studentScenarioMap = new HashMap<>();
        for (long i = 0; i < 30; i++) {
            Optional<StudentScenario> studentScenario = getStudentScenario(i + 1, 2, true);
            StudentScenario ss = studentScenario.get();
            ss.getTestSession().getScopeCode();
            ss.getTestnavScenario().getTestmapDetail();
            ss.getEpenScenario().getScenario();
            studentScenarioMap.put(i, ss);

            Mockito.doReturn(studentScenario).when(studentScenarioService).findByStudentScenarioId(i + 1);
            Mockito.doReturn(Optional.of(ss.getTestnavScenario())).when(scenarioService).findByScenarioId(1);
        }
        Mockito.doReturn(email).when(emailProvider).get();
        Mockito.doReturn(Optional.empty()).when(studentScenarioResultService)
                .findMaxVersionByStudentScenarioIdAndStateId(Matchers.anyLong(), Matchers.anyInt());

        // When
        Future<Object> future = Patterns.ask(actor, getRunMessage(), 3000);
        JobFinishMessage result = (JobFinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        for (long i = 0; i < 30; i++) {
            StudentScenario spy = studentScenarioMap.get(i);
            Mockito.verify(spy).setLastCompletedStateId(1);
            Mockito.verify(spy).setLastCompletedStateId(2);
            Mockito.verify(spy).setLastCompletedStateId(3);
            Mockito.verify(spy).setLastCompletedStateId(4);
            Mockito.verify(spy).setLastCompletedStateId(5);
            Mockito.verify(spy).setLastCompletedStateId(6);

        }
        for (StudentScenario studentScenario : divJobExec.get().getStudentScenarios()) {

            assertThat("The Last state of the DIV job execution should be stats",
                    studentScenario.getState().getStateName(),
                    equalTo(DataIntegrityValidationActor.DivStateName.STATS.name()));
        }
        assertThat("Value of success in result should be true", result.isSuccess(), equalTo(true));
        MatcherAssert.assertThat("Success message is Finished", result.getResultJson(), CoreMatchers.equalTo(null));
    }

    @Test
    public void divJobExecution_multiple_studentScenarios_withoutEpenScenario_returnSuccessResult() throws Exception {
        // Given
        Optional<DivJobExec> divJobExec = getDivJobExecWithStudentScenario(false, 30, false);
        Mockito.doReturn(divJobExec).when(divJobExecService).findByDivJobExecId(Matchers.anyLong());
        Mockito.doReturn(getHumanUser()).when(humanUserService).findByUserId(1);
        Map<Long, StudentScenario> studentScenarioMap = new HashMap<>();
        for (long i = 0; i < 30; i++) {
            Optional<StudentScenario> studentScenario = getStudentScenario(i + 1, 2, false);
            studentScenarioMap.put(i, studentScenario.get());

            Mockito.doReturn(studentScenario).when(studentScenarioService).findByStudentScenarioId(i + 1);
            Mockito.doReturn(Optional.of(studentScenario.get().getTestnavScenario())).when(scenarioService).findByScenarioId(1);
        }
        Mockito.doReturn(email).when(emailProvider).get();
        Mockito.doReturn(Optional.empty()).when(studentScenarioResultService)
                .findMaxVersionByStudentScenarioIdAndStateId(Matchers.anyLong(), Matchers.anyInt());

        // When
        Future<Object> future = Patterns.ask(actor, getRunMessage(), 3000);
        JobFinishMessage result = (JobFinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        for (long i = 0; i < 30; i++) {
            StudentScenario spy = studentScenarioMap.get(i);
            Mockito.verify(spy).setLastCompletedStateId(1);
            Mockito.verify(spy).setLastCompletedStateId(2);
            Mockito.verify(spy).setLastCompletedStateId(3);
            Mockito.verify(spy).setLastCompletedStateId(6);

        }
        for (StudentScenario studentScenario : divJobExec.get().getStudentScenarios()) {

            assertThat("The Last state of the DIV job execution should be stats",
                    studentScenario.getState().getStateName(),
                    equalTo(DataIntegrityValidationActor.DivStateName.STATS.name()));
        }

        assertThat("Value of success in result should be true", result.isSuccess(), equalTo(true));

        MatcherAssert.assertThat("Success message is Finished", result.getResultJson(), CoreMatchers.equalTo(null));
    }

    private RunMessage getRunMessage() {
        return new RunMessage("1", 100);
    }

    private Optional<DivJobExec> getDivJobExecWithStudentScenario(boolean failedDivJob, int numberOfStudentScenarios,
                                                                  boolean isEpenScenarioExist) {
        DivJobExec divJobExec = getDivJobExec(numberOfStudentScenarios, isEpenScenarioExist);
        if (failedDivJob) {
            divJobExec.getStudentScenarios().get(0).setStudentScenarioStatusId(3);
        }
        for (int i = 0; i < numberOfStudentScenarios; i++) {
            divJobExec.getStudentScenarios().get(i).setDivJobExec(divJobExec);
        }

        return Optional.of(divJobExec);

    }

    private DivJobExec getDivJobExec(int numberOfStudentScenarios, boolean isEpenScenarioExist) {
        DivJobExec divJobExec = new DivJobExec();
        divJobExec.setDivJobExecId(100L);
        divJobExec.setCompleted(true);
        divJobExec.setSubmitUserId(1L);
        divJobExec.setNumStudentScenarios(1);
        divJobExec.setScopeTreePath("/ref/2015-16/refspr16");

        Environment environment = new Environment();
        environment.setEnvironmentId(1);
        environment.setName("INT REF");
        environment.setBeMysqlConnection(getDbConnection());
        environment.setDataWarehouseMongoConnection(getDbConnection());
        divJobExec.setEnvironment(environment);

        Path path = new Path();
        path.setPathStates(getPathStates());
        divJobExec.setPath(path);

        List<StudentScenario> studentScenarios = new ArrayList<>();
        for (int i = 1; i <= numberOfStudentScenarios; i++) {
            studentScenarios.add(getStudentScenario(i, null, isEpenScenarioExist).get());
        }
        divJobExec.setStudentScenarios(studentScenarios);
        return divJobExec;
    }

    private Optional<StudentScenario> getStudentScenario(long studentScenarioId, Integer status,
                                                         boolean isEpenScenarioExist) {
        StudentScenario studentScenario = new StudentScenario();
        studentScenario.setStudentScenarioId(studentScenarioId);
        if (status != null) {
            studentScenario.setStudentScenarioStatusId(status);
        }

        Scenario scenario = getTestNavScenario();
        scenario.setTestmapDetail(getTestMapDetail());
        scenario.setTestCode("123");
        scenario.setFormCode("123");
        studentScenario.setTestSession(getTestSession());
        studentScenario.setTestnavScenario(scenario);
        if (isEpenScenarioExist) {

            String epenScenario = "{ \"scenarioName\": \"MAXPOINTS\", \"data\": [{ \"uin\": \"EN1000251607\", \"itemName\": \"IN_RD1000251607\", \"assignment\": \"IN_RD1000251607\", \"traitLabels\": \"Overall\", \"scoreScorePointLabels\": \"2\", \"reliabilityScorePointLabels\": \"2\", \"resolutionScorePointLabels\": null, \"adjudicationScorePointLabels\": null, \"backreadScorePointLabels\": null, \"responseState\": null }], \"uuid\": null }";
            scenario.setScenario(epenScenario);
            studentScenario.setEpenScenario(scenario);
        }

        return Optional.of(Mockito.spy(studentScenario));
    }

    private Optional<HumanUser> getHumanUser() {
        HumanUser humanUser = new HumanUser();
        humanUser.setEmail("nand.joshi@pearson.com");
        return Optional.of(humanUser);
    }

    private List<PathState> getPathStates() {
        ArrayList<PathState> pathStates = new ArrayList<>();
        State tn8ApiDataLoaderState = getState(1, DataIntegrityValidationActor.DivStateName.TN8_API_DATA_LOADER.name());
        pathStates.add(getPathState(1, tn8ApiDataLoaderState, 1));

        State irisState = getState(2, DataIntegrityValidationActor.DivStateName.IRIS_PROCESSING_VALIDATOR.name());
        pathStates.add(getPathState(2, irisState, 2));

        State bePrimaryPathState = getState(3,
                DataIntegrityValidationActor.DivStateName.BE_PRIMARY_PATH_VALIDATOR.name());
        pathStates.add(getPathState(3, bePrimaryPathState, 3));

        State epen2AutomationState = getState(4, DataIntegrityValidationActor.DivStateName.EPEN2_AUTOMATION.name());
        pathStates.add(getPathState(4, epen2AutomationState, 4));

        State beEpenPathState = getState(5, DataIntegrityValidationActor.DivStateName.BE_EPEN_PATH_VALIDATOR.name());
        pathStates.add(getPathState(5, beEpenPathState, 5));

        State statsState = getState(6, DataIntegrityValidationActor.DivStateName.STATS.name());
        pathStates.add(getPathState(6, statsState, 6));
        return pathStates;
    }

    private PathState getPathState(int pathStateId, State state, int pathStateOrder) {
        PathState pathState = new PathState();
        pathState.setPathStateId(pathStateId);
        pathState.setState(state);
        pathState.setPathStateOrder(pathStateOrder);
        return pathState;
    }

    private State getState(int stateId, String stateName) {
        State state = new State();
        state.setStateId(stateId);
        state.setStateName(stateName);
        return state;
    }

    private DbConnection getDbConnection() {
        DbConnection connection = new DbConnection();
        connection.setDbConnectionId(1);
        connection.setHost("Test");
        connection.setDbName("Test");
        connection.setDbPass("Test");
        connection.setDbUser("Test");
        connection.setPort(123);
        return connection;
    }

    private Scenario getTestNavScenario() {
        Scenario scenario = new Scenario();
        scenario.setScenarioId(1);
        scenario.setTestmapDetail(getTestMapDetail());
        scenario.setTestCode("123");
        scenario.setFormCode("123");
        return scenario;
    }

    private TestmapDetail getTestMapDetail() {
        TestmapDetail testmapDetail = new TestmapDetail();
        testmapDetail.setTestmapName("Test test map");
        return testmapDetail;
    }

    private TestSession getTestSession() {
        TestSession testSession = new TestSession();
        testSession.setScopeCode("scope_code");
        return testSession;
    }
}
