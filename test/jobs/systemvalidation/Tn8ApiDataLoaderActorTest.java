package jobs.systemvalidation;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import global.TestWithApplication;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import models.qtivalidation.Previewer;
import models.qtivalidation.PreviewerTenant;
import models.systemvalidation.DivJobExec;
import models.systemvalidation.Scenario;
import models.systemvalidation.State;
import models.systemvalidation.StudentScenario;
import play.db.jpa.JPAApi;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;
import services.systemvalidation.StudentScenarioResultService;

/**
 * @author Nand Joshi
 *
 */
public class Tn8ApiDataLoaderActorTest extends TestWithApplication {
    private ActorRef actor;
    private StudentScenario studentScenario;
    private Scenario scenario;
    private Scenario mockScenario;
    private State state;
    private DivJobExec divJobExec;

    @Before
    public void setUp() throws Exception {
        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        StudentScenarioResultService studentScenarioResultService = Mockito.mock(StudentScenarioResultService.class);

        actor = actorSystem.actorOf(Props.create(Tn8ApiDataLoaderActor.class, app.injector().instanceOf(JPAApi.class),
                studentScenarioResultService));

        this.studentScenario = Mockito.mock(StudentScenario.class);
        this.scenario = invalidDummyScenario();
        this.state = Mockito.mock(State.class);
        this.divJobExec = Mockito.mock(DivJobExec.class);
        this.mockScenario = Mockito.mock(Scenario.class);

    }

    @Test
    public void onReceiveTestForEmptyScenario() throws Exception {
        // Given
        Mockito.doReturn(mockScenario).when(studentScenario).getTestnavScenario();
        Mockito.doReturn(state).when(studentScenario).getState();
        Mockito.doReturn(divJobExec).when(studentScenario).getDivJobExec();

        // When
        Future<Object> future = Patterns.ask(actor, studentScenario, 3000);
        FinishMessage result = (FinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        assertThat("The Finished message should be returned", result.getErrorMessage(), equalTo(
                "{\"contentKey\":\"\",\"wireKey\":\"\",\"urlGetTestDef\":\"\",\"success\":\"no\",\"validationError\":[\"Can not find scenario for studentScenarioId 0\"]}"));
    }

    @Test
    public void onReceiveTestForInvalidScenario() throws Exception {
        // Given
        Mockito.doReturn(scenario).when(studentScenario).getTestnavScenario();

        Mockito.doReturn(state).when(studentScenario).getState();
        Mockito.doReturn(divJobExec).when(studentScenario).getDivJobExec();
        studentScenario.setTestnavScenario(scenario);

        // When
        Future<Object> future = Patterns.ask(actor, studentScenario, 3000);
        FinishMessage result = (FinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();
        // Then
        assertThat("The Finished message should be returned", result.getErrorMessage(), equalTo(
                "{\"contentKey\":\"\",\"wireKey\":\"\",\"urlGetTestDef\":\"\",\"success\":\"no\",\"validationError\":[\"Failed to convert JsonScenario to Tn8ApiDataloaderScenario!\"]}"));
    }

    @SuppressWarnings("unused")
    private Scenario dummyScenario() {
        Scenario scenario = new Scenario();
        scenario.setScenario(
                "{\"scenarioName\": \"minPoints\", \"scenarioType\": \"TN8\", \"testNumber\": \"\", \"formNumber\": \"15EL11PBEO0001\", \"itemResponses\": [ { \"uin\": \"8098_A\", \"outcomes\": [{ \"SCORE\": \"0\" }], \"identifiersToInteractions\": [{ \"identifier\": \"RESPONSE_A\", \"interactionType\": \"CHOICEINTERACTION\" }, { \"identifier\": \"RESPONSE_B\", \"interactionType\": \"CHOICEINTERACTION\" }], \"mods\": [{\"id\":\"TESTID\",\"sid\":\"TESTSID\",\"r\":[\"C\"],\"did\":\"RESPONSE_A\"},{\"id\":\"TESTID\",\"sid\":\"TESTSID\",\"r\":[\"C\",\"E\",\"F\"],\"did\":\"RESPONSE_B\"}], \"paperResponseStr\": \"\" } ] }");
        scenario.setScenarioId(1);

        Previewer p = new Previewer();
        p.setUrl("tn8previewer.testnav.com");

        PreviewerTenant pt = new PreviewerTenant();
        pt.setPreviewerTenantId(1l);
        pt.setPreviewer(p);
        scenario.setPreviewerTenant(pt);

        return scenario;
    }

    private Scenario invalidDummyScenario() {
        Scenario scenario = new Scenario();
        scenario.setScenario(
                "{ \"scenarioName\": \"minPoints\", \"scenarioType\": \"TN8\", \"testNumber\": \"\", \"formNumber\": \"15EL11PBEO0001\", \"itemResponses\": [ { \"uin\": \"8098_A\", \"outcomes\": [{ \"SCORE\": \"0\" }], \"identifiersToInteractions\": [{ \"identifier\": \"RESPONSE_A\", \"interactionType\": \"CHOICEINTERACTION\" }, { \"identifier\": \"RESPONSE_B\", \"interactionType\": \"CHOICEINTERACTION\" }, \"mods\": [{\"id\":\"TESTID\",\"sid\":\"TESTSID\",\"r\":[\"C\"],\"did\":\"RESPONSE_A\"},{\"id\":\"TESTID\",\"sid\":\"TESTSID\",\"r\":[\"C\",\"E\",\"F\"],\"did\":\"RESPONSE_B\"}], \"paperResponseStr\": \"\" } ] }");
        scenario.setScenarioId(1);

        Previewer p = new Previewer();
        p.setUrl("tn8previewer.testnav.com");

        PreviewerTenant pt = new PreviewerTenant();
        pt.setPreviewerTenantId(1l);
        pt.setPreviewer(p);
        scenario.setPreviewerTenant(pt);

        return scenario;
    }
}
