package jobs.systemvalidation;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import com.pearson.itautomation.stats.service.StatsInput;
import com.pearson.itautomation.stats.service.StatsServiceCall;
import com.pearson.itautomation.testmaps.models.Testmap;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapItemsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import com.typesafe.config.Config;
import global.TestWithApplication;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import jobs.systemvalidation.StatsValidationActor.StatsInputMessage;
import models.qtivalidation.Previewer;
import models.qtivalidation.PreviewerTenant;
import models.systemvalidation.*;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import scala.compat.java8.FutureConverters;
import scala.concurrent.Future;
import util.systemvalidation.StatsFactory;
import util.systemvalidation.T3TestMapCache;

import java.util.ArrayList;

/**
 * @author Nand Joshi
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({T3TestMapCache.class, Testmap.class, TestNav8API.class, GetTestMapItemsResult.class})
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.auth.*", "javax.crypto.*"})
public class StatsValidationActorTest extends TestWithApplication {
    private static final String STATS_OUTPUT_SUCCESS = "{\"success\":\"yes\",\"numberOfStudentTestComparesMatched\":1,\"numberOfStudentTestComparesMismatched\":0,\"numberOfStudentTestComparesSkipped\":0,\"numberOfStudentTestComparesFailed\":0,\"numberOfStudentTestComparesNotCompared\":0,\"studentTestComparisons\":[{\"uuid\":\"91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"result\":\"MATCHED\",\"message\":\"All Fields Matched\",\"mismatchedFields\":[],\"matchedFields\":[{\"fieldName\":\"Group Level Objective2-A\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective2-B\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"}]}]}";
    private static final String STATS_OUTPUT_FAILED = "{\"success\":\"no\",\"numberOfStudentTestComparesMatched\":0,\"numberOfStudentTestComparesMismatched\":1,\"numberOfStudentTestComparesSkipped\":0,\"numberOfStudentTestComparesFailed\":0,\"numberOfStudentTestComparesNotCompared\":0,\"studentTestComparisons\":[{\"uuid\":\"91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"result\":\"MISMATCHED\",\"message\":\"At least one field was not correctly compared for uuid: 91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"mismatchedFields\":[{\"fieldName\":\"Raw Score\",\"actualValue\":\"30\",\"expectedValue\":\"31.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Item REF0815_0048\",\"itemStatus\":\"OPERATIONAL\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2-A\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"}],\"matchedFields\":[{\"fieldName\":\"Last Name\",\"actualValue\":\"D1S1LN0001751\",\"expectedValue\":\"D1S1LN0001751\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective1-A\",\"actualValue\":\"6\",\"expectedValue\":\"6.0\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective2-B\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"}]}]}";
    private static final String EXPECTED_SUCCESS_RESULT = "{\"success\":\"yes\",\"numberOfStudentTestComparesMatched\":1,\"numberOfStudentTestComparesMismatched\":0,\"numberOfStudentTestComparesSkipped\":0,\"numberOfStudentTestComparesFailed\":0,\"numberOfStudentTestComparesNotCompared\":0,\"studentTestComparisons\":[{\"uuid\":\"91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"result\":\"MATCHED\",\"message\":\"All Fields Matched\",\"mismatchedFields\":[],\"matchedFields\":[{\"fieldName\":\"Group Level Objective2-A\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective2-B\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"}]}]}";
    private static final String EXPECTED_FAIL_TESTMAP_DIFFERENT_VERSIONS = "{\"success\":\"no\",\"numberOfStudentTestComparesMatched\":0,\"numberOfStudentTestComparesMismatched\":1,\"numberOfStudentTestComparesSkipped\":0,\"numberOfStudentTestComparesFailed\":0,\"numberOfStudentTestComparesNotCompared\":0,\"testMapVersionComparison\":{\"matched\":false,\"message\":\"The current test map version does not match the captured during DIV Job setup.\",\"initialTestMapVersion\":11,\"currentTestMapVersion\":19},\"studentTestComparisons\":[{\"uuid\":\"91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"result\":\"MISMATCHED\",\"message\":\"At least one field was not correctly compared for uuid: 91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"mismatchedFields\":[{\"fieldName\":\"Raw Score\",\"actualValue\":\"30\",\"expectedValue\":\"31.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Item REF0815_0048\",\"itemStatus\":\"OPERATIONAL\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2-A\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"}],\"matchedFields\":[{\"fieldName\":\"Last Name\",\"actualValue\":\"D1S1LN0001751\",\"expectedValue\":\"D1S1LN0001751\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective1-A\",\"actualValue\":\"6\",\"expectedValue\":\"6.0\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective2-B\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"}]}]}";
    private static final String EXPECTED_FAIL_RESULT = "{\"success\":\"no\",\"numberOfStudentTestComparesMatched\":0,\"numberOfStudentTestComparesMismatched\":1,\"numberOfStudentTestComparesSkipped\":0,\"numberOfStudentTestComparesFailed\":0,\"numberOfStudentTestComparesNotCompared\":0,\"testMapVersionComparison\":{\"matched\":true,\"initialTestMapVersion\":19,\"currentTestMapVersion\":19},\"studentTestComparisons\":[{\"uuid\":\"91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"result\":\"MISMATCHED\",\"message\":\"At least one field was not correctly compared for uuid: 91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"mismatchedFields\":[{\"fieldName\":\"Raw Score\",\"actualValue\":\"30\",\"expectedValue\":\"31.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Item REF0815_0048\",\"itemStatus\":\"OPERATIONAL\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2-A\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"}],\"matchedFields\":[{\"fieldName\":\"Last Name\",\"actualValue\":\"D1S1LN0001751\",\"expectedValue\":\"D1S1LN0001751\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective1-A\",\"actualValue\":\"6\",\"expectedValue\":\"6.0\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective2-B\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"}]}]}";
    private static final String EXPECTED_FAILED_TESTMAP_DIFFERENT_VERSIONS = "{\"success\":\"no\",\"numberOfStudentTestComparesMatched\":0,\"numberOfStudentTestComparesMismatched\":1,\"numberOfStudentTestComparesSkipped\":0,\"numberOfStudentTestComparesFailed\":0,\"numberOfStudentTestComparesNotCompared\":0,\"testMapVersionComparison\":{\"matched\":false,\"message\":\"The current test map version does not match the captured during DIV Job setup.\",\"initialTestMapVersion\":11,\"currentTestMapVersion\":19},\"studentTestComparisons\":[{\"uuid\":\"91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"result\":\"MISMATCHED\",\"message\":\"At least one field was not correctly compared for uuid: 91c1cab8-3f58-4b3b-b79f-a5ad86031425\",\"mismatchedFields\":[{\"fieldName\":\"Raw Score\",\"actualValue\":\"30\",\"expectedValue\":\"31.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Item REF0815_0048\",\"itemStatus\":\"OPERATIONAL\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"},{\"fieldName\":\"Group Level Objective2-A\",\"actualValue\":\"0\",\"expectedValue\":\"1.0\",\"result\":\"MISMATCHED\"}],\"matchedFields\":[{\"fieldName\":\"Last Name\",\"actualValue\":\"D1S1LN0001751\",\"expectedValue\":\"D1S1LN0001751\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective1-A\",\"actualValue\":\"6\",\"expectedValue\":\"6.0\",\"result\":\"MATCHED\"},{\"fieldName\":\"Group Level Objective2-B\",\"actualValue\":\"0\",\"expectedValue\":\"0.0\",\"result\":\"MATCHED\"}]}]}";

    private ActorRef actor;
    private StatsInputMessage inputMsg;
    private StatsServiceCall sCall;

    private Scenario testnavScenario;
    private Testmap testmap;

    @Before
    public void init() throws Exception {
        StatsFactory statsFactory = Mockito.mock(StatsFactory.class);
        Config config = Mockito.mock(Config.class);
        ActorSystem actorSystem = app.injector().instanceOf(ActorSystem.class);
        this.actor = actorSystem.actorOf(Props.create(StatsValidationActor.class, statsFactory, config));

        StudentScenario studentScenario = Mockito.mock(StudentScenario.class);
        State state = Mockito.mock(State.class);
        DivJobExec divJobExec = Mockito.mock(DivJobExec.class);
        this.inputMsg = new StatsInputMessage(studentScenario, null, null, null, null, null, null);

        StatsInput statsInput = Mockito.mock(StatsInput.class);
        this.sCall = Mockito.mock(StatsServiceCall.class);

        this.testnavScenario = Mockito.mock(Scenario.class);

        this.testmap = PowerMockito.mock(Testmap.class);

        PreviewerTenant previewerTenant = Mockito.mock(PreviewerTenant.class);
        Previewer previewer = Mockito.mock(Previewer.class);

        TestNav8API testNav8API = PowerMockito.mock(TestNav8API.class);
        GetTestMapItemsResult testMapItemsResult = PowerMockito.mock(GetTestMapItemsResult.class);
        // Given
        Mockito.doReturn(state).when(studentScenario).getState();
        Mockito.doReturn(divJobExec).when(studentScenario).getDivJobExec();

        Mockito.doReturn(statsInput).when(statsFactory).getStatsInput(inputMsg);
        Mockito.doReturn(sCall).when(statsFactory).getStatsServiceCall(statsInput);

        long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();

        PowerMockito.mockStatic(T3TestMapCache.class);
        PowerMockito.when(T3TestMapCache.downloadTestMapFromT3(divJobExecId, "Test", null, null, null)).thenReturn(testmap);
        PowerMockito.when(T3TestMapCache.getT3TestMapFromCache(divJobExecId, "Test")).thenReturn(testmap);
        Mockito.doReturn(testnavScenario).when(studentScenario).getTestnavScenario();
        Mockito.doReturn("19").when(testmap).getVersion();

        Mockito.doReturn("Test").when(testmap).getName();

        Mockito.doReturn(previewerTenant).when(testnavScenario).getPreviewerTenant();
        Mockito.doReturn(previewer).when(previewerTenant).getPreviewer();
        Mockito.doReturn("tn8previewer.testnav.com").when(previewer).getApiUrl();
        Mockito.doReturn("http://").when(previewer).getApiProtocol();
        Mockito.doReturn(71L).when(previewerTenant).getTenantId();

        PowerMockito.mockStatic(TestNav8API.class);
        Mockito.when(TestNav8API.connectToPreviewer("http://tn8previewer.testnav.com", "".toCharArray(), "".toCharArray()))
                .thenReturn(testNav8API);
        Mockito.doReturn(testMapItemsResult).when(testNav8API).getItemsForTestmap(71, null);
        Mockito.doReturn(new ArrayList<>()).when(testMapItemsResult).getItems();
    }

    @Test
    public void onReceive_executeStats_valueOfSuccessYes() throws Exception {
        // Given
        Mockito.doReturn(mockTestmapDetail(19)).when(testnavScenario).getTestmapDetail();
        Mockito.doReturn(STATS_OUTPUT_SUCCESS).when(sCall).runComparison(false);
        Mockito.doReturn("19").when(testmap).getVersion();

        // When
        Future<Object> future = Patterns.ask(actor, inputMsg, 3000);
        FinishMessage result = (FinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        MatcherAssert.assertThat("STATS success message is valid JSON string.", result.getErrorMessage(),
                CoreMatchers.equalTo(EXPECTED_SUCCESS_RESULT));

        MatcherAssert.assertThat("Success message is Finished", result.getFinishMessage(),
                CoreMatchers.equalTo("Finished"));
    }

    @Test
    public void onReceive_executeStats_valueOfSuccessNo() throws Exception {
        // Given
        Mockito.doReturn(mockTestmapDetail(19)).when(testnavScenario).getTestmapDetail();
        Mockito.doReturn(STATS_OUTPUT_FAILED).when(sCall).runComparison(false);
        Mockito.doReturn("19").when(testmap).getVersion();

        // When
        Future<Object> future = Patterns.ask(actor, inputMsg, 3000);
        FinishMessage result = (FinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        MatcherAssert.assertThat("STATS Failed message is valid JSON string.", result.getErrorMessage(),
                CoreMatchers.equalTo(EXPECTED_FAIL_RESULT));

        MatcherAssert.assertThat("Success message is Finished", result.getFinishMessage(),
                CoreMatchers.equalTo("Failed"));
    }

    @Test
    public void onReceive_executeStats_valueOfFailedYesWithDifferentTestmapVersion() throws Exception {
        // Given
        Mockito.doReturn(mockTestmapDetail(11)).when(testnavScenario).getTestmapDetail();
        Mockito.doReturn(STATS_OUTPUT_FAILED).when(sCall).runComparison(false);
        Mockito.doReturn("19").when(testmap).getVersion();

        // When
        Future<Object> future = Patterns.ask(actor, inputMsg, 3000);
        FinishMessage result = (FinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        MatcherAssert.assertThat("STATs passed with different testmap version.", result.getErrorMessage(),
                CoreMatchers.equalTo(EXPECTED_FAIL_TESTMAP_DIFFERENT_VERSIONS));

        MatcherAssert.assertThat("Success message is Failed", result.getFinishMessage(),
                CoreMatchers.equalTo("Failed"));
    }

    @Test
    public void onReceive_executeStats_valueOfSuccessNoWithDifferentTestmapVersion() throws Exception {
        // Given
        Mockito.doReturn(mockTestmapDetail(11)).when(testnavScenario).getTestmapDetail();
        Mockito.doReturn(STATS_OUTPUT_FAILED).when(sCall).runComparison(false);
        Mockito.doReturn("19").when(testmap).getVersion();

        // When
        Future<Object> future = Patterns.ask(actor, inputMsg, 3000);
        FinishMessage result = (FinishMessage) FutureConverters.toJava(future).toCompletableFuture().get();

        // Then
        MatcherAssert.assertThat("STATs Failed with different testmap version.", result.getErrorMessage(),
                CoreMatchers.equalTo(EXPECTED_FAILED_TESTMAP_DIFFERENT_VERSIONS));

        MatcherAssert.assertThat("Success message is Finished", result.getFinishMessage(),
                CoreMatchers.equalTo("Failed"));
    }

    private TestmapDetail mockTestmapDetail(int testmapVersion) {
        TestmapDetail testmapDetail = new TestmapDetail();
        testmapDetail.setTestmapName("Test");
        testmapDetail.setTestmapVersion(testmapVersion);
        return testmapDetail;
    }
}
