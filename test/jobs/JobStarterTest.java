package jobs;

import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class JobStarterTest {

    @Test
    public void compareTo_jobStartersInPriorityQueue_jobStartersSortedCorrectly() {
        // Given
        long currentTime = System.currentTimeMillis();

        MockJobStarter jobStarter1 =
                new MockJobStarter(Clock.fixed(Instant.ofEpochMilli(currentTime + 2), ZoneId.systemDefault()));
        jobStarter1.setPriority(1);

        MockJobStarter jobStarter2 =
                new MockJobStarter(Clock.fixed(Instant.ofEpochMilli(currentTime + 1), ZoneId.systemDefault()));
        jobStarter2.setPriority(3);

        MockJobStarter jobStarter3 =
                new MockJobStarter(Clock.fixed(Instant.ofEpochMilli(currentTime + 1), ZoneId.systemDefault()));
        jobStarter3.setPriority(2);

        MockJobStarter jobStarter4 =
                new MockJobStarter(Clock.fixed(Instant.ofEpochMilli(currentTime), ZoneId.systemDefault()));
        jobStarter4.setPriority(1);

        Queue<JobStarter> priorityQueue = new PriorityQueue<>();
        priorityQueue.add(jobStarter1);
        priorityQueue.add(jobStarter2);
        priorityQueue.add(jobStarter3);
        priorityQueue.add(jobStarter4);

        // When
        List<JobStarter> list = new ArrayList<>();
        while (!priorityQueue.isEmpty()) {
            list.add(priorityQueue.remove());
        }

        // Then
        assertThat("The first retrieved item should be jobStarter4", list.get(0), equalTo(jobStarter4));
        assertThat("The second retrieved item should be jobStarter1", list.get(1), equalTo(jobStarter1));
        assertThat("The third retrieved item should be jobStarter3", list.get(2), equalTo(jobStarter3));
        assertThat("The fourth retrieved item should be jobStarter2", list.get(3), equalTo(jobStarter2));
    }

}
