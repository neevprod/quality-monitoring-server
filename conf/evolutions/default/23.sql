# Change user_permissions table to make wildcards work.

# --- !Ups
alter table `user_permissions` add column `previewer_id` int(11) after `is_wildcard`;
alter table `user_permissions` add constraint
  `fk_user_permissions_previewer` foreign key (`previewer_id`) references
  `previewer` (`previewer_id`);
update `user_permissions` up set `previewer_id` = (select pt.`previewer_id` from `previewer_tenant` pt
  where pt.`previewer_tenant_id` = up.`previewer_tenant_id`);

alter table `user_permissions` add column `tenant_id` bigint(20) after `previewer_id`;
alter table `user_permissions` add constraint
  `fk_user_permissions_tenant` foreign key (`previewer_id`, `tenant_id`) references
  `previewer_tenant` (`previewer_id`, `tenant_id`);
update `user_permissions` up set `tenant_id` = (select pt.`tenant_id` from `previewer_tenant` pt
  where pt.`previewer_tenant_id` = up.`previewer_tenant_id`);

alter table `user_permissions` drop foreign key `fk_user_permissions_previewer_tenant`;
alter table `user_permissions` drop column `previewer_tenant_id`;


# --- !Downs
alter table `user_permissions` add column `previewer_tenant_id` bigint(20) after `tenant_id`;
update `user_permissions` up set `previewer_tenant_id` = (select pt.`previewer_tenant_id` from `previewer_tenant` pt
  where pt.`previewer_id` = up.`previewer_id` and pt.`tenant_id` = up.`tenant_id`);
alter table `user_permissions` add constraint
  `fk_user_permissions_previewer_tenant` foreign key (`previewer_tenant_id`) references
  `previewer_tenant` (`previewer_tenant_id`);

alter table `user_permissions` drop foreign key `fk_user_permissions_tenant`;
alter table `user_permissions` drop column `tenant_id`;

alter table `user_permissions` drop foreign key `fk_user_permissions_previewer`;
alter table `user_permissions` drop column `previewer_id`;

