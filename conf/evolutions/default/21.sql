# Add environment related data and path, path_state, scenario_type and scenario_class meta data
# Warning: this a destructive evolution, which will remove most div-job related data in both the ups and the downs.

# --- !Ups

delete from `student_scenario_result`;
delete from `student_scenario`;
delete from `scenario`;
delete from `div_job_exec`;
delete from `environment`;
delete from `api_connection`;
delete from `db_connection`;
delete from `path_state`;
delete from `state`;
delete from `scenario_class`;
delete from `scenario_type`;

insert into `api_connection` (api_connection_id, token, secret, api_url) VALUES
(1, 'fe_api_connection_token', 'fe_api_connection_secret', 'http://fe_api_connection_url'), 
(2, 'be_api_connection_token', 'be_api_connection_secret', 'http://be_api_connection_url'), 
(3, 'epen2_api_connection_token', 'epen2_api_connection_secret', 'http://epen2_api_connection_url'), 
(4, 'iris_api_connection_token', 'iris_api_connection_secret', 'http://iris_api_connection_url'), 
(5, 'ice_bridge_api_connection_token', 'ice_bridge_api_connection_secret', 'http://ice_bridge_api_connection_url'), 
(6, 'data_warehouse_api_connection_token', 'data_warehouse_api_connection_secret', 'http://data_warehouse_api_connection_url');

insert into `db_connection` (db_connection_id, host, port, db_name, db_user, db_pass) VALUES
(1, 'pan-int-ref-3-0-masterdb.pearsondev.com', 3306, 'config', 'dba_admin', '4p8n66U6rh43XiS'), 
(2, 'epen2-int-ref-3-0-masterdb.pearsondev.com', 3306, 'epenops', 'dba_admin', '4p8n66U6rh43XiS'), 
(3, 'iris-int-ref-shared-masterdb.pearsondev.com', 3306, 'ref_config', 'readonly_user', 'thongeich?e5Ahshah1i'), 
(4, 'ice_bridge_oracle_connection_host', 3306, 'ice_bridge_db', 'user1', 'pass1'), 
(5, '10.168.12.146', 27017, 'na', 'na', 'na');

insert into `environment` (environment_id, name, fe_url, fe_mysql_connection_id, be_mysql_connection_id, epen2_mysql_connection_id, iris_mysql_connection_id, ice_bridge_oracle_connection_id, data_warehouse_mongo_connection_id, fe_api_connection_id, be_api_connection_id, epen2_api_connection_id, iris_api_connection_id, ice_bridge_api_connection_id, data_warehouse_api_connection_id) VALUES
(1, 'INT_REF', 'https://pan-int-ref-3-0-customer.pearsondev.com/', 1, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6);

insert into `state` (state_id, state_name, state_description, auto_retries) VALUES
(1, 'TN8_API_DATA_LOADER', 'state_description', 0), 
(2, 'IRIS_PROCESSING_VALIDATOR', 'state_description', 0), 
(3, 'BE_PATH_PRE_EPEN', 'state_description', 0), 
(4, 'EPEN2_AUTOMATION', 'state_description', 0), 
(5, 'BE_PATH_POST_EPEN', 'state_description', 0), 
(6, 'STATS', 'state_description', 0);

insert into `path_state` (path_state_id, path_id, state_id) VALUES
(1, 1, 1), 
(2, 1, 2), 
(3, 1, 3), 
(4, 1, 4), 
(5, 1, 5), 
(6, 1, 6);

insert into `scenario_class` (scenario_class_id, scenario_class) VALUES
(1, 'allCorrect'), 
(2, 'allIncorrect'), 
(3, 'maxFieldtest'), 
(4, 'maxPoints'), 
(5, 'minPoints'), 
(6, 'nonAttempted'), 
(7, 'percentCorrect'), 
(8, 'minToMax'), 
(9, 'maxOE'), 
(10, 'allOE');

insert into `scenario_type` (scenario_type_id, scenario_type) VALUES
(1, 'TN8'), 
(2, 'EPEN');

# --- !Downs

delete from `student_scenario_result`;
delete from `student_scenario`;
delete from `scenario`;
delete from `div_job_exec`;
delete from `environment`;
delete from `api_connection`;
delete from `db_connection`;
delete from `path_state`;
delete from `state`;
delete from `scenario_class`;
delete from `scenario_type`;