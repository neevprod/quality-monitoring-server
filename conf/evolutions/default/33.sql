# Add StudentReservation Table.

# --- !Ups
CREATE TABLE `student_reservation` (
  `student_reservation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(80) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_scenario_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`student_reservation_id`),
  UNIQUE KEY `uuid` (`uuid`)
)/*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

# --- !Downs
DROP TABLE IF EXISTS `student_reservation`;