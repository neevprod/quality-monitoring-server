# Update db_connection.host value for INT_REF epen2_mongo_connection.

# --- !Ups
UPDATE db_connection SET host = '10.168.27.129' WHERE host = '10.168.27.62';

# --- !Downs
UPDATE db_connection SET host = '10.168.27.62' WHERE host = '10.168.27.129';