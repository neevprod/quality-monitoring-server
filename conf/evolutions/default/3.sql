# Add tables for permissions

# --- !Ups

create table `tenant_permission_option` (
  `tenant_permission_option_id` bigint not null auto_increment,
  `name` varchar(50) not null unique,
  `create_access` bit not null,
  `read_access` bit not null,
  `update_access` bit not null,
  `delete_access` bit not null,
  primary key (`tenant_permission_option_id`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

create table `tenant_permission_settings` (
  `tenant_permission_settings_id` bigint not null auto_increment,
  `name` varchar(50) not null unique,
  `last_update` timestamp default current_timestamp /*! on update current_timestamp */,
  primary key (`tenant_permission_settings_id`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

create table `tenant_permission_settings_value` (
  `tenant_permission_settings_value_id` bigint not null auto_increment,
  `tenant_permission_settings_id` bigint not null,
  `tenant_permission_option_id` bigint not null,
  `create_access` bit not null,
  `read_access` bit not null,
  `update_access` bit not null,
  `delete_access` bit not null,
  primary key (`tenant_permission_settings_value_id`),
  constraint `fk_tenant_permission_settings_value_tenant_permission_settings` foreign key
    (`tenant_permission_settings_id`) references `tenant_permission_settings`(`tenant_permission_settings_id`),
  constraint `fk_tenant_permission_settings_value_tenant_permission_option` foreign key (`tenant_permission_option_id`)
    references `tenant_permission_option`(`tenant_permission_option_id`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

create table `user_permissions` (
  `user_permission_id` bigint not null auto_increment,
  `user_id` bigint not null,
  `previewer_tenant_id` bigint not null,
  `tenant_permission_settings_id` bigint not null,
  `last_update` timestamp default current_timestamp /*! on update current_timestamp */,
  constraint `fk_user_permissions_user` foreign key (`user_id`) references `user`(`id`),
  constraint `fk_user_permissions_previewer_tenant` foreign key (`previewer_tenant_id`) references
    `previewer_tenant`(`previewer_tenant_id`),
  constraint `fk_user_permissions_tenant_permission_settings` foreign key (`tenant_permission_settings_id`) references
    `tenant_permission_settings`(`tenant_permission_settings_id`),
  primary key (`user_permission_id`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

insert into `tenant_permission_option` (`name`, `create_access`, `read_access`, `update_access`, `delete_access`)
  values ('items', 0, 1, 0, 0);

# --- !Downs

drop table if exists `user_permissions`, `tenant_permission_settings_value`, `tenant_permission_settings`,
  `tenant_permission_option`;
