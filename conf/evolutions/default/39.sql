# Add columns to enable/disable scenario classes for TestNav8 and ePEN.

# --- !Ups
ALTER TABLE div_job_exec
   ADD COLUMN scope_tree_path varchar(2000) NOT NULL;

UPDATE div_job_exec AS dje
   SET dje.scope_tree_path =
          (SELECT DISTINCT s.scope_tree_path
             FROM scenario s
                  JOIN student_scenario ss
                     ON ss.testnav_scenario_id = s.scenario_id
            WHERE dje.div_job_exec_id = ss.div_job_exec_id);
 
ALTER TABLE scenario
   DROP COLUMN scope_tree_path;


# --- !Downs
ALTER TABLE `scenario`
   ADD COLUMN `scope_tree_path` varchar(2000) NOT NULL;

UPDATE `scenario` s
       JOIN `student_scenario` ss on s.`scenario_id` = ss.`epen_scenario_id` OR s.`scenario_id` = ss.`testnav_scenario_id`
       JOIN (SELECT dje.`div_job_exec_id` djei, dje.`scope_tree_path` stp
               FROM `div_job_exec` dje) x
          ON x.djei = ss.`div_job_exec_id`
   SET s.`scope_tree_path` = x.stp;

ALTER TABLE `div_job_exec`
   DROP COLUMN `scope_tree_path`;