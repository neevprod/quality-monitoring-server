# Add read test maps permission to the tenant_permission_option table.

# --- !Ups

insert into `tenant_permission_option` (`name`, `create_access`, `read_access`, `update_access`, `delete_access`) values
  ('testMaps', 0, 1, 0, 0);
  
# --- !Downs

delete from `tenant_permission_option` where `name` in ('testMaps');
