# Add column previewer_tenant_id to scenario table.

# --- !Ups
ALTER TABLE scenario
   ADD COLUMN previewer_tenant_id bigint(20);

# --- !Downs
ALTER TABLE `scenario` DROP COLUMN `previewer_tenant_id`;
