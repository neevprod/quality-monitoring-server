# Add new scenario class.

# --- !Ups
INSERT INTO `scenario_class` (`scenario_class_id`, `scenario_class`, `enabled_for_tn8`, `enabled_for_epen`) VALUES (11, 'rightToWrong', 1, 0);
INSERT INTO `scenario_class` (`scenario_class_id`, `scenario_class`, `enabled_for_tn8`, `enabled_for_epen`) VALUES (12, 'wrongToRight', 1, 0);
INSERT INTO `scenario_class` (`scenario_class_id`, `scenario_class`, `enabled_for_tn8`, `enabled_for_epen`) VALUES (13, 'wrongToWrong', 1, 0);

# --- !Downs

Delete from `scenario_class` where `scenario_class` in ('rightToWrong', 'wrongToRight', 'wrongToWrong');