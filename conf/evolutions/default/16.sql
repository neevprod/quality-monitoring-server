# Added pan_test_session_id and environment_id columns to test_session table. Changed div_job_exec.div_job_exec_status_id and student_scenario.student_scenario_status_id default values to 1.

# --- !Ups
ALTER TABLE `test_session` ADD COLUMN `pan_test_session_id` varchar(16) NOT NULL;
ALTER TABLE `test_session` ADD COLUMN `environment_id` mediumint(8) NOT NULL;
ALTER TABLE `div_job_exec` CHANGE `div_job_exec_status_id` `div_job_exec_status_id` tinyint(3) NOT NULL DEFAULT 1;
ALTER TABLE `student_scenario` CHANGE `student_scenario_status_id` `student_scenario_status_id` tinyint(3) NOT NULL DEFAULT 1;

# --- !Downs
ALTER TABLE `test_session` DROP COLUMN `pan_test_session_id`;
ALTER TABLE `test_session` DROP COLUMN `environment_id`;
ALTER TABLE `div_job_exec` CHANGE `div_job_exec_status_id` `div_job_exec_status_id` tinyint(3) NOT NULL;
ALTER TABLE `student_scenario` CHANGE `student_scenario_status_id` `student_scenario_status_id` tinyint(3) NOT NULL;