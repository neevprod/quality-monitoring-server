# Modify the previewer table to remove redundant columns and improve security.

# --- !Ups

update `previewer` set `url` = `breadcrumb_name`;

alter table `previewer` drop column `breadcrumb_name`;

update `previewer` set `protocol` = 'https://' where `previewer_id` <> 1;
update `previewer` set `protocol` = 'http://' where `previewer_id` = 1;

# --- !Downs

update `previewer` set `protocol` = 'http:';

alter table `previewer` add column `breadcrumb_name` varchar(255) not null after `api_url`;

update `previewer` set `breadcrumb_name` = `url`, `url` = 'https://dev-tn8previewer.pearsondev.com'
  where `previewer_id` = 1;
update `previewer` set `breadcrumb_name` = `url`, `url` = 'https://parccpreviewer.testnav.com'
  where `previewer_id` = 2;
update `previewer` set `breadcrumb_name` = `url`, `url` = 'https://tn8previewer.testnav.com'
  where `previewer_id` = 3;
update `previewer` set `breadcrumb_name` = `url`, `url` = 'https://conversion.actaspire.org'
  where `previewer_id` = 4;

