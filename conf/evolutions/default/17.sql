# Renamed student_scenario_failure and student_scenario_failure_comment tables and associated columns. 
# Added student_scenario.wf_db_connection_id, student_scenario_result.success and student_scenario_result.version columns.
# Added EPEN scenario type.

# --- !Ups
ALTER TABLE `student_scenario_failure` RENAME TO `student_scenario_result`;
ALTER TABLE `student_scenario_failure_comment` RENAME TO `student_scenario_result_comment`;
ALTER TABLE `failure_comment_category` RENAME TO `student_scenario_result_comment_category`;
ALTER TABLE `student_scenario` ADD COLUMN `wf_db_connection_id` mediumint(8) DEFAULT NULL;
ALTER TABLE `student_scenario` CHANGE COLUMN `scenario_id` `testnav_scenario_id` int(10) DEFAULT NULL;
ALTER TABLE `student_scenario` ADD COLUMN `epen_scenario_id` int(10) DEFAULT NULL;
ALTER TABLE `student_scenario_result` ADD COLUMN `success` tinyint(1) DEFAULT NULL;
ALTER TABLE `student_scenario_result` ADD COLUMN `version` smallint(10) DEFAULT NULL;
ALTER TABLE `student_scenario_result` CHANGE COLUMN `student_scenario_failure_id` `student_scenario_result_id` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_scenario_result` CHANGE COLUMN `failure_details` `result_details` blob NOT NULL;
ALTER TABLE `student_scenario_result_comment` CHANGE COLUMN `failure_comment_id` `student_scenario_result_comment_id` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_scenario_result_comment` CHANGE COLUMN `student_scenario_failure_id` `student_scenario_result_id` bigint(20) NOT NULL;
ALTER TABLE `student_scenario_result_comment` CHANGE COLUMN `failure_comment_category_id` `student_scenario_result_comment_category_id` smallint(5) NOT NULL;
ALTER TABLE `student_scenario_result_comment` CHANGE COLUMN `failure_comment` `result_comment` varchar(512) NOT NULL;
ALTER TABLE `student_scenario_result_comment_category` CHANGE COLUMN `failure_comment_category_id` `student_scenario_result_comment_category_id` smallint(5) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_scenario` ADD CONSTRAINT `fk_wf_db_connection_id` FOREIGN KEY (`wf_db_connection_id`) REFERENCES `db_connection`(`db_connection_id`);
ALTER TABLE `student_scenario` ADD CONSTRAINT `fk_epen_scenario_id` FOREIGN KEY (`epen_scenario_id`) REFERENCES `scenario`(`scenario_id`);
ALTER TABLE `student_scenario_result_comment` DROP FOREIGN KEY `fk_ssfc_failure_id_ssf_failure_id`;
ALTER TABLE `student_scenario_result_comment` ADD CONSTRAINT `fk_student_scenario_result_id` FOREIGN KEY (`student_scenario_result_id`) REFERENCES `student_scenario_result` (`student_scenario_result_id`);
ALTER TABLE `student_scenario_result_comment` DROP FOREIGN KEY `fk_ssfc_failure_cmnt_cat_id_fcc_failure_cmnt_cat_id`;
ALTER TABLE `student_scenario_result_comment` ADD CONSTRAINT `fk_student_scenario_result_comment_category_id` FOREIGN KEY (`student_scenario_result_comment_category_id`) REFERENCES `student_scenario_result_comment_category` (`student_scenario_result_comment_category_id`);

# --- !Downs
ALTER TABLE `student_scenario_result` RENAME TO `student_scenario_failure`;
ALTER TABLE `student_scenario_result_comment` RENAME TO `student_scenario_failure_comment`;
ALTER TABLE `student_scenario_result_comment_category` RENAME TO `failure_comment_category`;
ALTER TABLE `student_scenario` DROP FOREIGN KEY `fk_wf_db_connection_id`;
ALTER TABLE `student_scenario` DROP FOREIGN KEY `fk_epen_scenario_id`;
ALTER TABLE `student_scenario` DROP COLUMN `wf_db_connection_id`;
ALTER TABLE `student_scenario` CHANGE COLUMN `testnav_scenario_id` `scenario_id` int(10) DEFAULT NULL;
ALTER TABLE `student_scenario` DROP COLUMN `epen_scenario_id`;
ALTER TABLE `student_scenario_failure` DROP COLUMN `success`;
ALTER TABLE `student_scenario_failure` DROP COLUMN `version`;
ALTER TABLE `student_scenario_failure` CHANGE COLUMN `student_scenario_result_id` `student_scenario_failure_id` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_scenario_failure` CHANGE COLUMN `result_details` `failure_details` blob NOT NULL;
ALTER TABLE `student_scenario_failure_comment` CHANGE COLUMN `student_scenario_result_comment_id` `failure_comment_id` bigint(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_scenario_failure_comment` CHANGE COLUMN `student_scenario_result_id` `student_scenario_failure_id` bigint(20) NOT NULL;
ALTER TABLE `student_scenario_failure_comment` CHANGE COLUMN `student_scenario_result_comment_category_id` `failure_comment_category_id` smallint(5) NOT NULL;
ALTER TABLE `student_scenario_failure_comment` CHANGE COLUMN `result_comment` `failure_comment` varchar(512) NOT NULL;
ALTER TABLE `failure_comment_category` CHANGE COLUMN `student_scenario_result_comment_category_id` `failure_comment_category_id` smallint(5) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_scenario_failure_comment` DROP FOREIGN KEY `fk_student_scenario_result_id`;
ALTER TABLE `student_scenario_failure_comment` ADD CONSTRAINT `fk_ssfc_failure_id_ssf_failure_id` FOREIGN KEY (`student_scenario_failure_id`) REFERENCES `student_scenario_failure` (`student_scenario_failure_id`);
ALTER TABLE `student_scenario_failure_comment` DROP FOREIGN KEY `fk_student_scenario_result_comment_category_id`;
ALTER TABLE `student_scenario_failure_comment` ADD CONSTRAINT `fk_ssfc_failure_cmnt_cat_id_fcc_failure_cmnt_cat_id` FOREIGN KEY (`failure_comment_category_id`) REFERENCES `failure_comment_category` (`failure_comment_category_id`);