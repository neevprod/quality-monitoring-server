# Add column disable_epen_scenario_checked to student_scenario table.

# --- !Ups
alter table `student_scenario` add column `disable_epen_scenario_checked` BIT(1) DEFAULT 0;

# --- !Downs
ALTER TABLE `student_scenario` DROP COLUMN `disable_epen_scenario_checked`;
