# Add BATTERY_STATS into the state table.
# Introducing new path for Battery Test

# --- !Ups
INSERT INTO state(state_name, state_description, auto_retries)
VALUES (
  'BATTERY_STATS',
  'The Battery STATS calculates the expected raw and group scores, fetches the actual scores from the data warehouse for the battery tests, and compares expected to actual.',
  0);

INSERT INTO path(path_name, path_description)
VALUES ('Battery', 'This is default path for Battery tests');


INSERT INTO path_state(path_id, state_id, path_state_order)
VALUES ((SELECT path_id
         FROM path
         WHERE path_name = 'Battery'),
        (SELECT state_id
         FROM state
         WHERE state_name = 'TN8_API_DATA_LOADER'),
        1),
  ((SELECT path_id
    FROM path
    WHERE path_name = 'Battery'),
   (SELECT state_id
    FROM state
    WHERE state_name = 'IRIS_PROCESSING_VALIDATOR'),
   2),
  ((SELECT path_id
    FROM path
    WHERE path_name = 'Battery'),
   (SELECT state_id
    FROM state
    WHERE state_name = 'BE_PRIMARY_PATH_VALIDATOR'),
   3),
  ((SELECT path_id
    FROM path
    WHERE path_name = 'Battery'),
   (SELECT state_id
    FROM state
    WHERE state_name = 'EPEN2_AUTOMATION'),
   4),
  ((SELECT path_id
    FROM path
    WHERE path_name = 'Battery'),
   (SELECT state_id
    FROM state
    WHERE state_name = 'BE_EPEN_PATH_VALIDATOR'),
   5),
  ((SELECT path_id
    FROM path
    WHERE path_name = 'Battery'),
   (SELECT state_id
    FROM state
    WHERE state_name = 'STATS'),
   6),
  ((SELECT path_id
    FROM path
    WHERE path_name = 'Battery'),
   (SELECT state_id
    FROM state
    WHERE state_name = 'BATTERY_STATS'),
   7);
