# Clean up environment, dbconnection and apiconnection data inserted by evolution script 21.sql, if needed.

# --- !Ups
DELETE FROM `environment`
WHERE fe_url = 'https://pan-int-ref-3-0-customer.pearsondev.com/';

DELETE FROM `db_connection`
WHERE     host IN ('pan-int-ref-3-0-masterdb.pearsondev.com',
                   'epen2-int-ref-3-0-masterdb.pearsondev.com',
                   'iris-int-ref-shared-masterdb.pearsondev.com',
                   'ice_bridge_oracle_connection_host',
                   '10.168.12.146')
      AND db_pass IN ('4p8n66U6rh43XiS',
                      'thongeich?e5Ahshah1i',
                      'pass1',
                      'na');

DELETE FROM `api_connection`
WHERE     api_url IN ('http://fe_api_connection_url',
                      'http://be_api_connection_url',
                      'http://epen2_api_connection_url',
                      'http://iris_api_connection_url',
                      'http://ice_bridge_api_connection_url',
                      'http://data_warehouse_api_connection_url')
      AND secret IN ('fe_api_connection_secret',
                     'be_api_connection_secret',
                     'epen2_api_connection_secret',
                     'iris_api_connection_secret',
                     'ice_bridge_api_connection_secret',
                     'data_warehouse_api_connection_secret');


# --- !Downs
