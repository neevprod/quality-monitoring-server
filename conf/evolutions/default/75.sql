# Adding Grid Automation module, and permissions to access it
# adding grid automation tables and known values


# --- !Ups
INSERT INTO application_module(`application_module_name`, `permission_type_id`)
VALUES      ('gridAutomation', 1);

INSERT INTO `permission_option`(`name`, `application_module_id`, `create_access`, `read_access`, `update_access`, `delete_access`)
VALUES      ('gridAutomation', (SELECT application_module_id FROM application_module WHERE application_module_name = 'gridAutomation'), 1, 1, 1, 1);

CREATE TABLE grid_contracts(
  grid_contract_id int(3) NOT NULL AUTO_INCREMENT,
  contract_abb VARCHAR(2) NOT NULL,
  contract_name VARCHAR(30) NOT NULL,
  active TINYINT(1) NOT NULL,
  PRIMARY KEY (grid_contract_id)
)/*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_general_ci */;

CREATE TABLE grid_programs(
  program_id int(3) NOT NULL AUTO_INCREMENT,
  program_abb VARCHAR(4) NOT NULL,
  program_name VARCHAR(35) NOT NULL,
  active TINYINT(1) NOT NULL,
  PRIMARY KEY (program_id)
)/*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_general_ci */;

CREATE TABLE grid_admins(
  admin_id int(3) NOT NULL AUTO_INCREMENT,
  admin_abb VARCHAR(3) NOT NULL,
  admin_name VARCHAR(40) NOT NULL,
  active TINYINT(1) NOT NULL,
  PRIMARY KEY (admin_id)
)/*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_general_ci */;

CREATE TABLE grid_seasons(
  season_id int(3) NOT NULL AUTO_INCREMENT,
  season_abb VARCHAR(3) NOT NULL,
  season_name VARCHAR(20) NOT NULL,
  active TINYINT(1) NOT NULL,
  PRIMARY KEY (season_id)
)/*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_general_ci */;

CREATE TABLE `grid_job`(
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `run_type` varchar(20) DEFAULT NULL,
  `grid_contract_id` int(3) NOT NULL,
  `grid_program_id` int(3) NOT NULL,
  `grid_admin_id` int(3) NOT NULL,
  `grid_season_id` int(3) NOT NULL,
  `file_name` varchar(500),
  `pgmssn` varchar(6),
  `doc_code` varchar(10),
  `state_code` varchar(2),
  `litho_start` varchar(20),
  `delivery_date` timestamp NOT NULL,
  `qual_group` varchar(3) NOT NULL,
  `comments` text,
  `user_id` bigint(10) NOT NULL,
  `submit_time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `complete_time_stamp` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `status_id` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_grid_job_user_user_id` (`user_id`),
  KEY `fk_grid_job_grid_contracts_grid_contract_id` (`grid_contract_id`),
  KEY `fk_grid_job_grid_programs_grid_program_id` (`grid_program_id`),
  KEY `fk_grid_job_grid_admins_grid_admin_id` (`grid_admin_id`),
  KEY `fk_grid_job_grid_seasons_grid_season_id` (`grid_season_id`),
  KEY `fk_grid_job_div_job_exec_status_id` (`status_id`),
  CONSTRAINT `fk_grid_job_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_grid_job_grid_contract_id` FOREIGN KEY (`grid_contract_id`) REFERENCES `grid_contracts` (`grid_contract_id`),
  CONSTRAINT `fk_grid_job_grid_program_id` FOREIGN KEY (`grid_program_id`) REFERENCES `grid_programs` (`program_id`),
  CONSTRAINT `fk_grid_job_grid_admin_id` FOREIGN KEY (`grid_admin_id`) REFERENCES `grid_admins` (`admin_id`),
  CONSTRAINT `fk_grid_job_grid_season_id` FOREIGN KEY (`grid_season_id`) REFERENCES `grid_seasons` (`season_id`),
   CONSTRAINT `fk_grid_job_div_job_exec_status_id` FOREIGN KEY (`status_id`) REFERENCES `div_job_exec_status` (`div_job_exec_status_id`)
)/*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_general_ci */;

INSERT INTO grid_contracts(contract_abb, active, contract_name)
VALUES      ('AC', TRUE, 'ACT'), ('AD', FALSE, 'ADP'), ('AI', FALSE, 'AIR'),
  ('AL', FALSE, 'ALABAMA'), ('AZ', TRUE, 'ARIZONA'), ('AR', FALSE, 'ARKANSAS'),
  ('CH', FALSE, 'CAHSEE'), ('CA', FALSE, 'CALSTAR'), ('CB', TRUE, 'COLLEGE BOARD'),
  ('CO', TRUE, 'COLORADO'), ('DE', FALSE, 'DELAWARE'), ('DD', FALSE, 'DOD'),
  ('EA', TRUE, 'EA / CLINICAL'), ('FL', TRUE, 'FLORIDA'), ('GA', FALSE, 'GEORGIA'),
  ('GL', FALSE, 'GLOBAL'), ('GR', FALSE, 'GRANITE'), ('HO', FALSE, 'HOUSTON'),
  ('IL', FALSE, 'ILLINOIS'), ('IN', TRUE, 'INDIANA'), ('IP', FALSE, 'IPT'),
  ('KY', TRUE, 'KENTUCKY'), ('LA', FALSE, 'LOUISIANA'), ('MD', TRUE, 'MARYLAND'),
  ('MN', FALSE, 'MINNESOTA'), ('MS', FALSE, 'MISSISSIPPI'), ('NA', TRUE, 'NAEP'),
  ('NB', TRUE, 'NBPTS'), ('NJ', FALSE, 'NEW JERSEY'), ('NY', TRUE, 'NEW YORK'),
  ('N1', TRUE, 'NEW YORK CITY'), ('NN', TRUE, 'NNAT3'), ('NL', FALSE, 'NOEL LEVITZ'),
  ('NC', FALSE, 'NORTH CAROLINA'), ('NP', FALSE, 'NPS'), ('OK', FALSE, 'OKLAHOMA'),
  ('PC', TRUE, 'PARCC'), ('PR', TRUE, 'PUERTO RICO'), ('SC', FALSE, 'SOUTH CAROLINA'),
  ('SD', FALSE, 'SOUTH DAKOTA'), ('TN', TRUE, 'TENNESSEE'), ('TX', FALSE, 'TEXAS'),
  ('UI', FALSE, 'UNIVERSITY OF IOWA ITP'), ('UT', FALSE, 'UTAH'), ('VA', TRUE, 'VIRGINIA'),
  ('WA', FALSE, 'WASHINGTON'), ('DC', FALSE, 'WASHINGTON DC'), ('WY', FALSE, 'WYOMING');

INSERT INTO grid_programs(program_abb, active, program_name)
VALUES      ('ACC', TRUE, 'ACCUPLACER'), ('ACH', TRUE, 'ACHIEVEMENT'), ('ACS', TRUE, 'ACSI'),
  ('AIM', TRUE, 'AIMS'), ('AKA', FALSE, 'ALASKA'), ('ALA', FALSE, 'ALABAMA'),
  ('ALG', TRUE, 'ALGEBRA'), ('ALT', TRUE, 'ALT'), ('ARK', FALSE, 'ARKANSAS'),
  ('ASP', TRUE, 'ASPIRE'), ('AUD', FALSE, 'AUDIT'), ('AZE', TRUE, 'AZELLA'),
  ('BOM', FALSE, 'BOM'), ('BST', FALSE, 'BST'), ('CAH', FALSE, 'CAHSEE'),
  ('CAL', FALSE, 'CALSTAR'), ('BCAS', FALSE, 'CAS ALT'), ('CAT', TRUE, 'CAT'),
  ('CMA', FALSE, 'CMA'), ('CNY', TRUE, 'CUNY'), ('COL', TRUE, 'COLORADO'),
  ('CTG', TRUE, 'CATALOG'), ('DEL', FALSE, 'DELAWARE'), ('DOD', FALSE, 'DODEA'),
  ('DSS', TRUE, 'DSS'), ('EAP', FALSE, 'EAP'), ('ELL', TRUE, 'ELL'),
  ('EOC', TRUE, 'EOC'), ('EOI', FALSE, 'EOI'), ('EOT', FALSE, 'EOCT'),
  ('FCA', TRUE, 'FCAT'), ('FLK', FALSE, 'FLKRS'), ('GLB', FALSE, 'GLOBAL'),
  ('GRA', FALSE, 'GRAD'), ('GRN', FALSE, 'GRANITE'), ('GTA', TRUE, 'GIFTED & TALENTED ASSESSMENT'),
  ('GTW', TRUE, 'GATEWAY'), ('HAW', FALSE, 'HAWAII'), ('HOU', FALSE, 'HOUSTON'),
  ('HSA', TRUE, 'HSA'), ('HSG', FALSE, 'HSGT'), ('HSW', FALSE, 'HSWT'),
  ('IAA', FALSE, 'IAA'), ('IAI', FALSE, 'IA ICAM'), ('IN1', TRUE, 'ISTEP'),
  ('IN2', TRUE, 'IREAD'), ('IRP', TRUE, 'Item Research Pilot'), ('IST', FALSE, 'ISAT'),
  ('KY1', TRUE, 'KY'), ('LOU', FALSE, 'LOUISIANA'), ('LRN', FALSE, 'LEARNIA'),
  ('MAN', TRUE, 'MILLER ANALOGIES'), ('MCA', FALSE, 'MCA-II'), ('MCT', FALSE, 'MCT2'),
  ('MSA', TRUE, 'MSA'), ('MST', FALSE, 'MST2'), ('MTA', FALSE, 'MTAS'),
  ('MTL', FALSE, 'MTELL'), ('MYV', FALSE, 'MY VOICE'), ('NAA', TRUE, 'NAAL'),
  ('NAP', TRUE, 'NAEP'), ('NBP', TRUE, 'NBPTS'), ('NEJ', FALSE, 'NEW JERSEY'),
  ('NIE', TRUE, 'NIES'), ('NLL', FALSE, 'NOEL LOVITZ'), ('NN3', TRUE, 'NNAT3'),
  ('NPS', FALSE, 'NPS'), ('NYC', TRUE, 'NEW YORK CITY'), ('NYF', TRUE, 'NY FT'),
  ('NYK', TRUE, 'NEW YORK'), ('NYS', TRUE, 'NYSESLAT'), ('O38', TRUE, 'GRADES 3-8'),
  ('O58', TRUE, 'GRADES 5 & 8'), ('OAP', FALSE, 'OAAP'), ('OHI', FALSE, 'OHIO'),
  ('OLP', FALSE, 'OLPA'), ('P10', TRUE, 'PSAT10/NMSQT'), ('P2S', FALSE, 'P2 SCORE COLLECTION'),
  ('P89', TRUE, 'PSAT8/9'), ('PAW', FALSE, 'PAWS'), ('PC', TRUE, 'PARCC'),
  ('PCT', TRUE, 'PCAT'), ('PER', TRUE, 'PERIODIC ASSESSMENTS'), ('PIR', TRUE, 'PIRLS'),
  ('PIS', TRUE, 'PISA'), ('PPA', TRUE, 'PPAA'), ('PPE', TRUE, 'PPEA'),
  ('PTF', TRUE, 'PT / FT'), ('RDS', TRUE, 'READISTEP'), ('RSA', TRUE, 'rSAT'),
  ('SAT', TRUE, 'SAT'), ('SDK', FALSE, 'SOUTH DAKOTA'), ('SEL', TRUE, 'SELP'),
  ('SOL', TRUE, 'SOL'), ('SP2', FALSE, 'SATP2'), ('SPG', TRUE, 'SPECIAL PROGRAM'),
  ('STR', FALSE, 'STAAR'), ('T12', FALSE, 'GRADES 4 & 7 READ & WRITING'), ('TAK', FALSE, 'TAKS'),
  ('TAP', TRUE, 'TAP'), ('TEL', FALSE, 'TELPAS'), ('TEX', FALSE, 'TEXAS'),
  ('TIM', TRUE, 'TIMMS'), ('TMP', TRUE, 'TALENT MANAGEMENT PILOT'), ('TSM', FALSE, 'TEAE / SOLOM'),
  ('UTH', FALSE, 'UTAH'), ('VAA', FALSE, 'VAAP'), ('VGL', FALSE, 'VGLA'),
  ('VPG', FALSE, 'V PROGRAMS'), ('VSE', FALSE, 'VSEP'), ('WRI', TRUE, 'WRITING');

INSERT INTO grid_admins(admin_abb, admin_name, active)
VALUES      ('AD1', 'ALGEBRA I & II', FALSE), ('AD2', 'ALGEBRA II', FALSE),
  ('AI1', 'HAWAII', FALSE), ('AI2', 'BREACH', FALSE),
  ('AI3', 'K-8 ACHIEVEMENT', FALSE), ('AI4', 'SWD', FALSE),
  ('AL1', 'NNAT ONLINE', FALSE), ('AL2', 'SAT 10 - OFF GRADE', FALSE),
  ('ALT', 'ALT', TRUE), ('AR1', 'OFF GRADE', FALSE),
  ('AR2', 'ON GRADE - GRADE K - MAT8', FALSE), ('AR3', 'ON GRADE - GRADES 3 - 8', FALSE),
  ('ASP', 'ASPIRE', TRUE), ('AT1', 'ALT AUDIT', FALSE),
  ('AUD', 'AUDIT', FALSE), ('AZ1', 'MATH & SCIENCE', TRUE),
  ('AZ2', 'WRITING & READING', TRUE), ('AZ3', 'AZELLA', TRUE),
  ('B01', 'BOM 11-12', FALSE), ('CA1', 'PRIMARY', FALSE),
  ('CA2', 'WRITING', FALSE), ('CA3', 'FIELD TEST', FALSE),
  ('CA4', 'EAP', FALSE), ('CAS', 'CAS ALT', FALSE),
  ('CB1', 'ACCUPLACER', TRUE), ('CB2', 'CAT', TRUE),
  ('CB3', 'READISTEP', TRUE), ('CB4', 'SAT', TRUE),
  ('CH1', 'CAHSEE', FALSE), ('DD1', 'SOCIAL STUDIES & SCIENCE', FALSE),
  ('DD2', 'WORLD HISTORY FIELD TEST', FALSE), ('DD3', 'WORLD HISTORY OPERATIONAL', FALSE),
  ('DE1', 'MATH & ELA', FALSE), ('DE2', 'RETEST', FALSE),
  ('DSS', 'DSS', TRUE), ('EA1', 'DAT', TRUE),
  ('EA2', 'APRENDA 3 ONLINE', TRUE), ('EA3', 'OLSAT8 ONLINE', TRUE),
  ('EA4', 'SAT 10 ENHANCEMENTS', TRUE), ('EA5', 'EOC', TRUE),
  ('EA6', 'LEARNIA', FALSE), ('EA7', 'MILLER ANALOGIES', TRUE),
  ('EA8', 'MY VOICE', FALSE), ('EA9', 'ELL', TRUE),
  ('EAA', 'GIFTED & TALENTED', TRUE), ('EAB', 'PCAT', TRUE),
  ('EAC', 'AACS', FALSE), ('EAD', 'BROWARD COUNTY', FALSE),
  ('EAE', 'HILLSBOROUGH SPRING', FALSE), ('EAF', 'KANSAS READING FIRST', FALSE),
  ('EAG', 'OREGON SPANISH', FALSE), ('EAH', 'READING FIRST', FALSE),
  ('EAI', 'ROLLOVER NORMS', FALSE), ('EAJ', 'SOUTH CAROLINA READING FIRST', FALSE),
  ('EAK', 'HOUSTON APRENDA3 (GRADE K)', FALSE), ('EAL', 'HOUSTON APRENDA3 (GRADES 1 - 8)', FALSE),
  ('EAM', 'HISD SAT10/APR3 (GRADE K)', FALSE), ('EAN', 'HISD SAT10/APR3 (GRADES 1-8)', FALSE),
  ('EOC', 'End of Course', TRUE), ('FL1', 'Reading, Math, & Science', TRUE),
  ('FL2', 'RETAKE', TRUE), ('FL3', 'FLKRS', FALSE),
  ('GA1', 'EOCT', FALSE), ('GA2', 'MID-MONTH', FALSE),
  ('GA3', 'HSGT', FALSE), ('GA4', 'HSWT', FALSE),
  ('GA5', 'GRADE 3', FALSE), ('GA6', 'GRADE 5', FALSE),
  ('GA7', 'GRADE 8', FALSE), ('GLB', 'GLOBAL', FALSE),
  ('GRN', 'GRANITE', FALSE), ('GTW', 'GATEWAY', FALSE),
  ('IL1', 'IAA', FALSE), ('IL2', 'ISAT', FALSE),
  ('IN1', 'ISTEP', TRUE), ('IN2', 'IREAD', TRUE),
  ('IP1', 'ALASKA', FALSE), ('IRP', 'Item Research Pilot', TRUE),
  ('LA1', 'LOUSIANA', FALSE), ('MD1', 'ALT', TRUE),
  ('MD2', 'MODULE I', TRUE), ('MD3', 'MSA / MOD', TRUE),
  ('MD4', 'SCIENCE', TRUE), ('MN1', 'MATH & READING', TRUE),
  ('MN2', 'MATH, READING, & WRITING', FALSE), ('MN3', 'WRITING FIELD TEST', FALSE),
  ('MN4', 'MTAS', FALSE), ('MN5', 'MTELL', FALSE),
  ('MN6', 'TEAE/SOLOM', FALSE), ('MN7', ' WRITTEN COMPOSITION', FALSE),
  ('MS1', 'MCT2', FALSE), ('MS2', 'ONLINE', FALSE),
  ('MS3', 'PAPER PENCIL', FALSE), ('MS4', 'SENIOR PAPER PENCIL', FALSE),
  ('MS5', '4x4', FALSE), ('MS6', 'GRADES 4, 7, & 10', FALSE),
  ('MST', 'MST2', FALSE), ('NA1', 'NAAL', TRUE),
  ('NA2', 'NAEP', TRUE), ('NA3', 'NIES', TRUE),
  ('NA4', 'PIRLS', TRUE), ('NA5', 'PISA', TRUE),
  ('NA6', 'TIMMS', TRUE), ('NB1', 'NBPTS', TRUE),
  ('NC1', 'WRITING, OCS, NCEXTEND', FALSE), ('NJ1', 'APA', FALSE),
  ('NL1', 'NOEL LOVITZ', FALSE), ('NN3', 'ANY NNAT3', TRUE),
  ('NP1', 'DIAGNOSTIC', FALSE), ('NP2', 'PRE-ADMIN FT', FALSE),
  ('NP3', 'PREDICTOR', FALSE), ('NP4', 'PREDICTOR FT', FALSE),
  ('NY1', 'ELA & MATH', TRUE), ('NY2', 'CUNY', TRUE),
  ('NY3', 'GRADES 3 - 8', TRUE), ('NY4', 'ELA', TRUE),
  ('NY5', 'OPERATIONAL', TRUE), ('NY6', 'CAT TRIAL/CENSUS/REP SAMPLE', FALSE),
  ('NY7', 'PT / FT', TRUE), ('NYF', 'NY FT', TRUE),
  ('OA1', 'OAAP (1st Admin)', FALSE), ('OK1', 'OMAAP', FALSE),
  ('OK2', 'OCCT', FALSE), ('P10', 'PSAT10/NMSQT', TRUE),
  ('P89', 'PSAT8/9', TRUE), ('PR1', 'PPAA', TRUE),
  ('PR2', 'PPEA', TRUE), ('RSA', 'rSAT', TRUE),
  ('SD1', 'DSTEP', FALSE), ('SD2', 'STARS', FALSE),
  ('SD3', 'GRADES 5 & 9 WRITING', FALSE), ('SLP', 'SELP2', FALSE),
  ('SUM', 'SUMMATIVE', FALSE), ('T12', 'GRADES 4 & 7 READ & WRITING', FALSE),
  ('T1E', 'GRADE 11 ELA', FALSE), ('T1M', 'GRADE 11 M/S/SS', FALSE),
  ('TA2', 'TAP YR2', FALSE), ('TM2', 'TALENT MANAGEMENT PILOT (2nd Admin)', FALSE),
  ('TN1', 'ACHIEVEMENT', TRUE), ('TX2', 'GRADES 5 & 8 READ & MATH', FALSE),
  ('TX3', 'GRADES 5 & 8 READ & MATH RETEST', FALSE), ('TX7', 'EXIT RETEST', FALSE),
  ('TX9', 'TELPAS', FALSE), ('TXA', 'ITEM ANALYSIS REPORTS', FALSE),
  ('TXB', 'ITEM JUDGMENT FORMS', FALSE), ('TXC', 'RELEASED TESTS DISTRIBUTION', FALSE),
  ('TXD', 'RELEASED TESTS PROCESSING', FALSE), ('TXE', 'TOP RATER QUALIFICATION', FALSE),
  ('UI1', 'IA ICAM', FALSE), ('VA1', 'NON-WRITING', TRUE),
  ('VA2', 'MANUALS & SEI TAGS DISTRIBUTION', FALSE), ('VA3', 'PROCESSING', FALSE),
  ('VA4', 'PROCESSING & SCORING', FALSE), ('VA5', 'RETURN', FALSE),
  ('VA6', 'Writing Field Test', FALSE), ('VMT', 'VMAST', FALSE),
  ('WV1', 'WAVE 1', TRUE), ('WV2', 'WAVE 2', TRUE),
  ('WV3', 'WAVE 3', TRUE), ('WV4', 'WAVE 4', TRUE),
  ('WV5', 'WAVE 5', TRUE), ('WV6', 'WAVE 6', TRUE),
  ('WY1', 'PAWS', FALSE);

INSERT INTO grid_seasons(season_abb, season_name, active)
VALUES      ('ANN', 'ANNUAL', TRUE), ('APR', 'APRIL', FALSE), ('AUG', 'AUGUST', FALSE),
  ('DEC', 'DECEMBER', TRUE), ('FAL', 'FALL', TRUE), ('FEB', 'FEBRUARY', FALSE),
  ('GLB', 'GLOBAL', FALSE), ('JAN', 'JANUARY', TRUE), ('JUL', 'JULY', FALSE),
  ('JUN', 'JUNE', TRUE), ('MAR', 'MARCH', FALSE), ('MAY', 'MAY', TRUE),
  ('MtA', 'MARCH / APRIL', FALSE), ('NN3', 'ANY NNAT3', FALSE), ('NOV', 'NOVEMBER', FALSE),
  ('OCT', 'OCTOBER', TRUE), ('SEP', 'SEPTEMBER', FALSE), ('SPR', 'SPRING', TRUE),
  ('SUM', 'SUMMER', TRUE), ('WIN', 'WINTER', TRUE);


# --- !Downs
DELETE FROM permission_option
WHERE       name = 'gridAutomation';

DELETE FROM application_module
WHERE       application_module_name = 'gridAutomation';

DROP TABLE IF EXISTS grid_job;
DROP TABLE IF EXISTS grid_admins;
DROP TABLE IF EXISTS grid_programs;
DROP TABLE IF EXISTS grid_contracts;
DROP TABLE IF EXISTS grid_seasons;
