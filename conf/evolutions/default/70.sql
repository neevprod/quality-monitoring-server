# Add test_type table
# Add pathStates to path_state table
# Add test_type_id to div_job_exec table.

# --- !Ups

CREATE TABLE `test_type`(
  `test_type_id` MEDIUMINT(3) NOT NULL AUTO_INCREMENT, `name` varchar(50) NOT NULL, PRIMARY KEY(`test_type_id`)) /*!
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARSET = utf8 */;

INSERT INTO test_type(name)
VALUES      ('Standard'), ('Battery');

INSERT INTO path(path_name, path_description)
VALUES      ('Custom1', 'Custom1'), ('Custom2', 'Custom2'), ('Custom3', 'Custom3'), ('Custom4', 'Custom4'), ('Custom5', 'Custom5'), ('Custom6', 'Custom6'), ('Custom7', 'Custom7');

INSERT INTO path_state(path_id, state_id, path_state_order)
VALUES      ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom1'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'TN8_API_DATA_LOADER'),
             1),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom2'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'TN8_API_DATA_LOADER'),
             1),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom2'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'IRIS_PROCESSING_VALIDATOR'),
             2),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom3'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'TN8_API_DATA_LOADER'),
             1),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom3'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'IRIS_PROCESSING_VALIDATOR'),
             2),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom3'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'BE_PRIMARY_PATH_VALIDATOR'),
             3),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom4'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'TN8_API_DATA_LOADER'),
             1),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom4'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'IRIS_PROCESSING_VALIDATOR'),
             2),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom4'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'BE_PRIMARY_PATH_VALIDATOR'),
             3),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom4'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'EPEN2_AUTOMATION'),
             4),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom5'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'TN8_API_DATA_LOADER'),
             1),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom5'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'IRIS_PROCESSING_VALIDATOR'),
             2),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom5'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'BE_PRIMARY_PATH_VALIDATOR'),
             3),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom5'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'EPEN2_AUTOMATION'),
             4),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom5'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'BE_EPEN_PATH_VALIDATOR'),
             5),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom6'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'TN8_API_DATA_LOADER'),
             1),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom6'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'IRIS_PROCESSING_VALIDATOR'),
             2),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom6'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'BE_PRIMARY_PATH_VALIDATOR'),
             3),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom6'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'STATS'),
             4),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom7'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'TN8_API_DATA_LOADER'),
             1),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom7'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'IRIS_PROCESSING_VALIDATOR'),
             2),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom7'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'BE_PRIMARY_PATH_VALIDATOR'),
             3),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom7'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'STATS'),
             4),
            ((SELECT path_id
              FROM   path
              WHERE  path_name = 'Custom7'),
             (SELECT state_id
              FROM   state
              WHERE  state_name = 'BATTERY_STATS'),
             5);

ALTER TABLE `div_job_exec`
  ADD COLUMN `test_type_id` MEDIUMINT(3) NOT NULL;

UPDATE `div_job_exec`
SET    `test_type_id` = `path_id`;

ALTER TABLE `div_job_exec`
  ADD CONSTRAINT `fk_div_job_exec_test_type` FOREIGN KEY(`test_type_id`) REFERENCES `test_type`(`test_type_id`);