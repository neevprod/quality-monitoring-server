# Add Data Integrity Validation Job tables.

# --- !Ups
ALTER TABLE student_scenario DROP FOREIGN KEY fk_student_scenario_state_last_completed_state_id;
DELETE FROM `path_state`;
DELETE FROM `state`;

INSERT INTO `state` (`state_id`,`state_name`, `state_description`,`auto_retries`) VALUES
(1,'TN8_API_DATA_LOADER','state_description',0),
(2,'IRIS_PROCESSING_VALIDATOR','state_description',0),
(3,'BE_PATH_PROCESSING_VALIDATOR','state_description',0),
(4,'EPEN2_SCORING_SCENARIO_UPDATER','state_description',0),
(5,'EPEN2_BULK_SCORER','state_description',0),
(6,'EXPECTED_SCORE_GENERATOR','state_description',0),
(7,'DATAWAREHOUSE_EXTRACTOR','state_description',0),
(8,'EXPECTED_TO_ACTUAL_SCORE_COMPARER','state_description',0);

INSERT INTO `path_state` (`path_state_id`,`path_id`, `state_id`) VALUES 
(1,1,1),
(2,1,2),
(3,1,3),
(4,1,4),
(5,1,5),
(6,1,6),
(7,1,7),
(8,1,8);

ALTER TABLE student_scenario ADD CONSTRAINT `fk_student_scenario_state_last_completed_state_id` FOREIGN KEY (`last_completed_state_id`) REFERENCES `state` (`state_id`);

# --- !Downs
ALTER TABLE student_scenario DROP FOREIGN KEY fk_student_scenario_state_last_completed_state_id;
DELETE FROM `path_state`;
DELETE FROM `state`;

INSERT INTO `state` (`state_id`,`state_name`, `state_description`,`auto_retries`) VALUES
(1,'FE_STUDENT_SESSION_DETAIL_FETCHER','state_description',0),
(2,'TEST_SCENARIO_CREATOR','state_description',0),
(3,'TN8_API_DATA_LOADER','state_description',0),
(4,'IRIS_PROCESSING_VALIDATOR','state_description',0),
(5,'BE_PATH_PROCESSING_VALIDATOR','state_description',0),
(6,'EPEN2_SCORING_SCENARIO_CREATOR','state_description',0),
(7,'EPEN2_BULK_SCORER','state_description',0),
(8,'EXPECTED_SCORE_GENERATOR','state_description',0),
(9,'DATAWAREHOUSE_EXTRACTOR','state_description',0),
(10,'EXPECTED_TO_ACTUAL_SCORE_COMPARER','state_description',0);

INSERT INTO `path_state` (`path_state_id`,`path_id`, `state_id`) VALUES 
(1,1,1),
(2,1,2),
(3,1,3),
(4,1,4),
(5,1,5),
(6,1,6),
(7,1,7),
(8,1,8),
(9,1,9),
(10,1,10);

ALTER TABLE student_scenario ADD CONSTRAINT `fk_student_scenario_state_last_completed_state_id` FOREIGN KEY (`last_completed_state_id`) REFERENCES `state` (`state_id`);