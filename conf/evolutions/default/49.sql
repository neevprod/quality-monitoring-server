# Add initial list of comment categories

# --- !Ups
INSERT INTO student_scenario_result_comment_category (name) values 
('Back End - Application Outage'),
('Back End - Configuration Issue'),
('Back End - Daemon(s) Stopped'),
('Back End - Other'),
('Back End - State Failure'),
('Data Warehouse - Incorrect Scores'),
('Data Warehouse - Mongo Connectivity Issue'),
('Data Warehouse - Other'),
('Data Warehouse - Testmap Issue'),
('ePEN - Accept Scores Issue'),
('ePEN - Application Outage'),
('ePEN - Configuration Issue'),
('ePEN - Export Scores Issue'),
('ePEN - Other'),
('ePEN - Scoring Issue'),
('IRIS - Application Outage'),
('IRIS - Configuration Issue'),
('IRIS - Daemon(s) Stopped'),
('IRIS - Other'),
('IRIS - State Failure'),
('Quality Monitoring - Application Issue'),
('T3 - Application Outage '),
('T3 - Item configuration Issue'),
('T3 - Other'),
('TestNav - Application Outage'),
('TestNav - Invalid Student Test State'),
('TestNav - Invalid Test Window'),
('TestNav - Other'),
('TestNav - Test Session Not Started or Locked');

# --- !Downs
DELETE FROM student_scenario_result_comment;
ALTER TABLE student_scenario_result_comment AUTO_INCREMENT = 1;

DELETE FROM student_scenario_result_comment_category;
ALTER TABLE student_scenario_result_comment_category AUTO_INCREMENT = 1;