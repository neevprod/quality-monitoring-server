# Add Data Integrity Validation Job tables.

# --- !Ups

CREATE TABLE `external_defect_repository` (
  `external_defect_repository_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`external_defect_repository_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `failure_comment_category` (
  `failure_comment_category_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`failure_comment_category_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `scenario_type` (
  `scenario_type_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `scenario_type` varchar(50) NOT NULL,
  PRIMARY KEY (`scenario_type_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `scenario_name` (
  `scenario_name_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `scenario_name` varchar(50) NOT NULL,
  PRIMARY KEY (`scenario_name_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `scenario` (
  `scenario_id` int(10) NOT NULL AUTO_INCREMENT,
  `scenario_type_id` tinyint(3) NOT NULL,
  `scenario_name_id` smallint(5) NOT NULL,
  `scenario` blob NOT NULL,
  PRIMARY KEY (`scenario_id`),
  KEY `fk_scenario_scenario_type_scenario_type_id` (`scenario_type_id`),
  KEY `fk_scenario_scenario_name_scenario_name_id` (`scenario_name_id`),
  CONSTRAINT `fk_scenario_scenario_name_scenario_name_id` FOREIGN KEY (`scenario_name_id`) REFERENCES `scenario_name` (`scenario_name_id`),
  CONSTRAINT `fk_scenario_scenario_type_scenario_type_id` FOREIGN KEY (`scenario_type_id`) REFERENCES `scenario_type` (`scenario_type_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `scenario_param` (
  `scenario_param_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `scenario_id` int(10) NOT NULL,
  `scenario_param_key` varchar(50) NOT NULL,
  `scenario_param_value` varchar(50) NOT NULL,
  PRIMARY KEY (`scenario_param_id`),
  KEY `fk_scenario_param_scenario_scenario_id` (`scenario_id`),
  CONSTRAINT `fk_scenario_param_scenario_scenario_id` FOREIGN KEY (`scenario_id`) REFERENCES `scenario` (`scenario_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `db_connection` (
  `db_connection_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `host` varchar(140) NOT NULL,
  `port` smallint(5) NOT NULL,
  `db_name` varchar(50) NOT NULL,
  `db_user` varchar(50) NOT NULL,
  `db_pass` varchar(50) NOT NULL,
  PRIMARY KEY (`db_connection_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `api_connection` (
  `api_connection_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `token` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `api_url` varchar(140) NOT NULL,
  PRIMARY KEY (`api_connection_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `environment` (
  `environment_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fe_url` varchar(140) NOT NULL,
  `fe_mysql_connection_id` mediumint(8) NOT NULL,
  `be_mysql_connection_id` mediumint(8) NOT NULL,
  `epen2_mysql_connection_id` mediumint(8) NOT NULL,
  `iris_mysql_connection_id` mediumint(8) NOT NULL,
  `ice_bridge_oracle_connection_id` mediumint(8) NOT NULL,
  `data_warehouse_mongo_connection_id` mediumint(8) NOT NULL,
  `fe_api_connection_id` mediumint(8) NOT NULL,
  `be_api_connection_id` mediumint(8) NOT NULL,
  `epen2_api_connection_id` mediumint(8) NOT NULL,
  `iris_api_connection_id` mediumint(8) NOT NULL,
  `ice_bridge_api_connection_id` mediumint(8) NOT NULL,
  `data_warehouse_api_connection_id` mediumint(8) NOT NULL,
  PRIMARY KEY (`environment_id`),
  KEY `fk_environment_db_connection_fe_mysql_connection_id` (`fe_mysql_connection_id`),
  KEY `fk_environment_db_connection_be_mysql_connection_id` (`be_mysql_connection_id`),
  KEY `fk_environment_db_connection_epen2_mysql_connection_id` (`epen2_mysql_connection_id`),
  KEY `fk_environment_db_connection_iris_mysql_connection_id` (`iris_mysql_connection_id`),
  KEY `fk_environment_db_connection_ice_bridge_oracle_connection_id` (`ice_bridge_oracle_connection_id`),
  KEY `fk_environment_db_connection_data_warehouse_mongo_connection_id` (`data_warehouse_mongo_connection_id`),
  KEY `fk_environment_api_connection_be_api_connection_id` (`be_api_connection_id`),
  KEY `fk_environment_api_connection_fe_api_connection_id` (`fe_api_connection_id`),
  KEY `fk_environment_api_connection_epen2_api_connection_id` (`epen2_api_connection_id`),
  KEY `fk_environment_api_connection_data_warehouse_api_connection_id` (`data_warehouse_api_connection_id`),
  KEY `fk_environment_api_connection_ice_bridge_api_connection_id` (`ice_bridge_api_connection_id`),
  KEY `fk_environment_api_connection_iris_api_connection_id` (`iris_api_connection_id`),
  CONSTRAINT `fk_environment_api_connection_be_api_connection_id` FOREIGN KEY (`be_api_connection_id`) REFERENCES `api_connection` (`api_connection_id`),
  CONSTRAINT `fk_environment_api_connection_data_warehouse_api_connection_id` FOREIGN KEY (`data_warehouse_api_connection_id`) REFERENCES `api_connection` (`api_connection_id`),
  CONSTRAINT `fk_environment_api_connection_epen2_api_connection_id` FOREIGN KEY (`epen2_api_connection_id`) REFERENCES `api_connection` (`api_connection_id`),
  CONSTRAINT `fk_environment_api_connection_fe_api_connection_id` FOREIGN KEY (`fe_api_connection_id`) REFERENCES `api_connection` (`api_connection_id`),
  CONSTRAINT `fk_environment_api_connection_ice_bridge_api_connection_id` FOREIGN KEY (`ice_bridge_api_connection_id`) REFERENCES `api_connection` (`api_connection_id`),
  CONSTRAINT `fk_environment_api_connection_iris_api_connection_id` FOREIGN KEY (`iris_api_connection_id`) REFERENCES `api_connection` (`api_connection_id`),
  CONSTRAINT `fk_environment_db_connection_be_mysql_connection_id` FOREIGN KEY (`be_mysql_connection_id`) REFERENCES `db_connection` (`db_connection_id`),
  CONSTRAINT `fk_environment_db_connection_data_warehouse_mongo_connection_id` FOREIGN KEY (`data_warehouse_mongo_connection_id`) REFERENCES `db_connection` (`db_connection_id`),
  CONSTRAINT `fk_environment_db_connection_epen2_mysql_connection_id` FOREIGN KEY (`epen2_mysql_connection_id`) REFERENCES `db_connection` (`db_connection_id`),
  CONSTRAINT `fk_environment_db_connection_fe_mysql_connection_id` FOREIGN KEY (`fe_mysql_connection_id`) REFERENCES `db_connection` (`db_connection_id`),
  CONSTRAINT `fk_environment_db_connection_ice_bridge_oracle_connection_id` FOREIGN KEY (`ice_bridge_oracle_connection_id`) REFERENCES `db_connection` (`db_connection_id`),
  CONSTRAINT `fk_environment_db_connection_iris_mysql_connection_id` FOREIGN KEY (`iris_mysql_connection_id`) REFERENCES `db_connection` (`db_connection_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `path` (
  `path_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `path_name` varchar(50) NOT NULL,
  `path_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`path_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `state` (
  `state_id` smallint(5) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(50) NOT NULL,
  `state_description` varchar(255) DEFAULT NULL,
  `auto_retries` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`state_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `path_state` (
  `path_state_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `path_id` mediumint(8) NOT NULL,
  `state_id` smallint(5) NOT NULL,
  PRIMARY KEY (`path_state_id`),
  KEY `fk_path_state_path_path_id` (`path_id`),
  KEY `fk_path_state_state_state_id` (`state_id`),
  CONSTRAINT `fk_path_state_path_path_id` FOREIGN KEY (`path_id`) REFERENCES `path` (`path_id`),
  CONSTRAINT `fk_path_state_state_state_id` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `div_job_exec_status` (
  `div_job_exec_status_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`div_job_exec_status_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `student_scenario_status` (
  `student_scenario_status_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`student_scenario_status_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `div_job_exec` (
  `div_job_exec_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `environment_id` mediumint(8) NOT NULL,
  `path_id` mediumint(8) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `submit_time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `complete_time_stamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `submit_user_id` bigint(20) NOT NULL,
  `priority` tinyint(3) NOT NULL,
  `div_job_exec_status_id` tinyint(3) NOT NULL,
  PRIMARY KEY (`div_job_exec_id`),
  KEY `fk_div_job_exec_environment_environment_id` (`environment_id`),
  KEY `fk_div_job_exec_user_id` (`submit_user_id`),
  KEY `fk_div_job_exec_path_path_id` (`path_id`),
  KEY `fk_div_job_exec_djes_div_job_exec_status_id` (`div_job_exec_status_id`),
  CONSTRAINT `fk_div_job_exec_djes_div_job_exec_status_id` FOREIGN KEY (`div_job_exec_status_id`) REFERENCES `div_job_exec_status` (`div_job_exec_status_id`),
  CONSTRAINT `fk_div_job_exec_environment_environment_id` FOREIGN KEY (`environment_id`) REFERENCES `environment` (`environment_id`),
  CONSTRAINT `fk_div_job_exec_path_path_id` FOREIGN KEY (`path_id`) REFERENCES `path` (`path_id`),
  CONSTRAINT `fk_div_job_exec_user_id` FOREIGN KEY (`submit_user_id`) REFERENCES `user` (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `student_scenario` (
  `student_scenario_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(80) NOT NULL,
  `testnav_login_id` varchar(90) NOT NULL,
  `testnav_password` varchar(40) NOT NULL,
  `test_code` varchar(255) NOT NULL,
  `form_code` varchar(255) NOT NULL,
  `div_job_exec_id` bigint(20) NOT NULL,
  `scenario_id` int(10) NOT NULL,
  `last_completed_state_id` smallint(5) NOT NULL,
  `student_scenario_status_id` tinyint(3) NOT NULL,
  PRIMARY KEY (`student_scenario_id`),
  KEY `fk_student_scenario_div_job_exec_div_job_exec_id` (`div_job_exec_id`),
  KEY `fk_student_scenario_scenario_scenario_id` (`scenario_id`),
  KEY `fk_student_scenario_state_last_completed_state_id` (`last_completed_state_id`),
  KEY `fk_student_scenario_sss_student_scenario_status_id` (`student_scenario_status_id`),
  CONSTRAINT `fk_student_scenario_div_job_exec_div_job_exec_id` FOREIGN KEY (`div_job_exec_id`) REFERENCES `div_job_exec` (`div_job_exec_id`),
  CONSTRAINT `fk_student_scenario_scenario_scenario_id` FOREIGN KEY (`scenario_id`) REFERENCES `scenario` (`scenario_id`),
  CONSTRAINT `fk_student_scenario_sss_student_scenario_status_id` FOREIGN KEY (`student_scenario_status_id`) REFERENCES `student_scenario_status` (`student_scenario_status_id`),
  CONSTRAINT `fk_student_scenario_state_last_completed_state_id` FOREIGN KEY (`last_completed_state_id`) REFERENCES `state` (`state_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `student_scenario_failure` (
  `student_scenario_failure_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_scenario_id` bigint(20) NOT NULL,
  `state_id` smallint(5) NOT NULL,
  `failure_details` blob NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`student_scenario_failure_id`),
  KEY `fk_ssf_student_scenario_id_ss_student_scenario_id` (`student_scenario_id`),
  KEY `fk_ssf_state_id_s_state_id` (`state_id`),
  CONSTRAINT `fk_ssf_state_id_s_state_id` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`),
  CONSTRAINT `fk_ssf_student_scenario_id_ss_student_scenario_id` FOREIGN KEY (`student_scenario_id`) REFERENCES `student_scenario` (`student_scenario_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

CREATE TABLE `student_scenario_failure_comment` (
  `failure_comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `student_scenario_failure_id` bigint(20) NOT NULL,
  `failure_comment_category_id` smallint(5) NOT NULL,
  `failure_comment` varchar(512) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` bigint(20) NOT NULL,
  `external_defect_repository_id` tinyint(3) DEFAULT NULL,
  `external_defect_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`failure_comment_id`),
  KEY `fk_ssfc_failure_id_ssf_failure_id` (`student_scenario_failure_id`),
  KEY `fk_ssfc_failure_cmnt_cat_id_fcc_failure_cmnt_cat_id` (`failure_comment_category_id`),
  KEY `fk_ssfc_user_id_u_user_id` (`user_id`),
  KEY `fk_ssfc_ext_def_repo_id_edr_ext_def_repo_id` (`external_defect_repository_id`),
  CONSTRAINT `fk_ssfc_ext_def_repo_id_edr_ext_def_repo_id` FOREIGN KEY (`external_defect_repository_id`) REFERENCES `external_defect_repository` (`external_defect_repository_id`),
  CONSTRAINT `fk_ssfc_failure_cmnt_cat_id_fcc_failure_cmnt_cat_id` FOREIGN KEY (`failure_comment_category_id`) REFERENCES `failure_comment_category` (`failure_comment_category_id`),
  CONSTRAINT `fk_ssfc_failure_id_ssf_failure_id` FOREIGN KEY (`student_scenario_failure_id`) REFERENCES `student_scenario_failure` (`student_scenario_failure_id`),
  CONSTRAINT `fk_ssfc_user_id_u_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

INSERT INTO `div_job_exec_status` (`div_job_exec_status_id`,`status`) VALUES 
    (1,'Not Started'),
    (2,'In Progress'),
    (3,'Failed'),
    (4,'Completed');

INSERT INTO `student_scenario_status` (`student_scenario_status_id`,`status`) VALUES 
    (1,'Not Started'),
    (2,'In Progress'),
    (3,'Failed'),
    (4,'Completed');
	
INSERT INTO `external_defect_repository` (`external_defect_repository_id`,`name`) VALUES 
    (1,'Rally'),
    (2,'Jira'),
    (3,'Quality Center'),
    (4,'Team Track');
	
INSERT INTO `path` (`path_id`,`path_name`, `path_description`) VALUES 
    (1,'DefaultPath','This is the default path');
	
INSERT INTO `state` (`state_id`,`state_name`, `state_description`,`auto_retries`) VALUES 
(1,'FE_STUDENT_SESSION_DETAIL_FETCHER','state_description',0),
(2,'TEST_SCENARIO_CREATOR','state_description',0),
(3,'TN8_API_DATA_LOADER','state_description',0),
(4,'IRIS_PROCESSING_VALIDATOR','state_description',0),
(5,'BE_PATH_PROCESSING_VALIDATOR','state_description',0),
(6,'EPEN2_SCORING_SCENARIO_CREATOR','state_description',0),
(7,'EPEN2_BULK_SCORER','state_description',0),
(8,'EXPECTED_SCORE_GENERATOR','state_description',0),
(9,'DATAWAREHOUSE_EXTRACTOR','state_description',0),
(10,'EXPECTED_TO_ACTUAL_SCORE_COMPARER','state_description',0);

INSERT INTO `path_state` (`path_state_id`,`path_id`, `state_id`) VALUES 
(1,1,1),
(2,1,2),
(3,1,3),
(4,1,4),
(5,1,5),
(6,1,6),
(7,1,7),
(8,1,8),
(9,1,9),
(10,1,10);

# --- !Downs

DROP TABLE IF EXISTS student_scenario_failure_comment;
DROP TABLE IF EXISTS student_scenario_failure;
DROP TABLE IF EXISTS student_scenario;
DROP TABLE IF EXISTS div_job_exec;
DROP TABLE IF EXISTS div_job_exec_status;
DROP TABLE IF EXISTS student_scenario_status;
DROP TABLE IF EXISTS path_state;
DROP TABLE IF EXISTS state;
DROP TABLE IF EXISTS path;
DROP TABLE IF EXISTS environment;
DROP TABLE IF EXISTS api_connection;
DROP TABLE IF EXISTS db_connection;
DROP TABLE IF EXISTS scenario_param;
DROP TABLE IF EXISTS scenario;
DROP TABLE IF EXISTS scenario_name;
DROP TABLE IF EXISTS scenario_type;
DROP TABLE IF EXISTS failure_comment_category;
DROP TABLE IF EXISTS external_defect_repository;
DROP TABLE IF EXISTS student_scenario_failure;