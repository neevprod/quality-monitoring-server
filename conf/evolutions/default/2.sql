# Remove the PreviewerEnvironment table, since it's redundant.

# --- !Ups

alter table `ValidationHistory` drop foreign key `FK6402A9BBF88155E4`;
alter table `test_case_uploads` drop foreign key `test_case_uploads_ibfk_1`;

drop table if exists `PreviewerEnvironment`;

alter table `ValidationHistory` change `previewerEnv_id` `previewer_id` int(11);
alter table `test_case_uploads` modify `previewer_id` int(11);

alter table `ValidationHistory` add constraint `fk_ValidationHistory_previewer_previewer_id` foreign key (`previewer_id`) references `previewer`(`previewer_id`);
alter table `test_case_uploads` add constraint `fk_test_case_uploads_previewer_previewer_id` foreign key (`previewer_id`) references `previewer`(`previewer_id`);

# --- !Downs

alter table `ValidationHistory` drop foreign key `fk_ValidationHistory_previewer_previewer_id`;
alter table `test_case_uploads` drop foreign key `fk_test_case_uploads_previewer_previewer_id`;

alter table `ValidationHistory` change `previewer_id` `previewerEnv_id` bigint(20);
alter table `test_case_uploads` modify `previewer_id` bigint(20);

CREATE TABLE `PreviewerEnvironment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `previewerApiUrl` varchar(255) NOT NULL,
  `previewerUrl` varchar(255) NOT NULL,
  `protocol` varchar(10) NOT NULL,
  `supportsHttps` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

alter table `ValidationHistory` add constraint `FK6402A9BBF88155E4` foreign key (`previewerEnv_id`) references `PreviewerEnvironment`(`id`);
alter table `test_case_uploads` add constraint `test_case_uploads_ibfk_1` foreign key (`previewer_id`) references `PreviewerEnvironment`(`id`);

INSERT INTO PreviewerEnvironment(previewerApiUrl, previewerUrl, protocol, supportsHttps) values('tn8previewer-dev.elasticbeanstalk.com','tn8previewer-dev.elasticbeanstalk.com','http:',0),('api-parccpreviewer.testnav.com','parccpreviewer.testnav.com','http:',1),('api-tn8previewer.testnav.com','tn8previewer.testnav.com','http:',1),('localhost:9090','localhost:9090','http:',0),('api-conversion.actaspire.org','conversion.actaspire.org','http:',1);

