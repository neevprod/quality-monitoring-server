# Add wf_mysql_connection_id and epen_url columns to environment table. 
# Add wf_prod and wf_nonprod records to db_connection table.
# Add epen url for int environment.

# --- !Ups
ALTER TABLE `environment` ADD COLUMN `wf_mysql_connection_id`  mediumint(8) NOT NULL;
ALTER TABLE `environment` ADD COLUMN `epen_url`  varchar(140) NOT NULL;

insert into `db_connection` (host, port, db_name, db_user, db_pass) VALUES
('wf_prod', 3306, '', 'dba_admin', '4p8n66U6rh43XiS'), 
('wf_nonprod', 3306, '', 'dba_admin', '4p8n66U6rh43XiS');

update environment set wf_mysql_connection_id = (select db_connection_id from db_connection where host = 'wf_nonprod');
update environment set epen_url = 'https://epen2-int-ref-3-0.pearsondev.com' where fe_url = 'https://pan-int-ref-3-0-customer.pearsondev.com/';

# --- !Downs
ALTER TABLE `environment` DROP COLUMN `wf_mysql_connection_id`;
ALTER TABLE `environment` DROP COLUMN `epen_url`;

delete from `db_connection` WHERE host in ('wf_prod', 'wf_nonprod');