# Add student_test_session_assign_id column

# --- !Ups
ALTER TABLE `student_scenario`
  ADD COLUMN `student_test_session_assign_id` int(10) unsigned DEFAULT NULL;

# --- !Downs
ALTER TABLE `student_scenario`
  DROP COLUMN `student_test_session_assign_id`;