# Add unique key constraint to prevent duplicate records based on host and db_name.

# --- !Ups
ALTER TABLE `db_connection` ADD UNIQUE KEY `host_db_name`(`host`, `db_name`);

# --- !Downs
ALTER TABLE `db_connection` DROP KEY `host_db_name`;