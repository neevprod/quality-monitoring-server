# Create hist_item_compare_result table.
# Rename hist_item to hist_item_old.
# Create hist_item from hist_item_old without the compare_result column and add the hist_item_compare_result_id column.

# --- !Ups

ALTER TABLE `hist_test_case` RENAME TO `hist_test_case_old`;

CREATE TABLE `hist_test_case`
  /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */
AS SELECT * FROM `hist_test_case_old`;

ALTER TABLE `hist_test_case` MODIFY COLUMN `hist_test_case_id` int(11) NOT NULL;
ALTER TABLE `hist_test_case` ADD PRIMARY KEY (`hist_test_case_id`);
ALTER TABLE `hist_test_case` MODIFY COLUMN `hist_test_case_id` int(11) NOT NULL AUTO_INCREMENT;

DROP TABLE `hist_test_case_old`;

ALTER TABLE `hist_item` RENAME TO `hist_item_old`;

CREATE TABLE `hist_item`
  /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */
  AS SELECT `hist_item_id`, `hist_tenant_id`, `previewer_tenant_item_id`, `previewer_tenant_item_xml_id`,
    `hist_status_id`, `pending_item_test_cases_id`, `approved_item_test_cases_id`, `fingerprint_id`, `test_cases`,
    `failed_test_cases`, `pending_test_cases`, `all_test_cases_passed` FROM `hist_item_old`;

ALTER TABLE `hist_item` MODIFY COLUMN `hist_item_id` int(11) NOT NULL;
ALTER TABLE `hist_item` ADD PRIMARY KEY (`hist_item_id`);
ALTER TABLE `hist_item` MODIFY COLUMN `hist_item_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `hist_item` ADD CONSTRAINT `fk_hist_item_hist_tenant_id` FOREIGN KEY (`hist_tenant_id`) REFERENCES
  `hist_tenant` (`hist_tenant_id`);
ALTER TABLE `hist_item` ADD CONSTRAINT `fk_hist_item_previewer_tenant_id` FOREIGN KEY (`previewer_tenant_item_id`) REFERENCES
  `previewer_tenant_item` (`previewer_tenant_item_id`);
ALTER TABLE `hist_item` ADD CONSTRAINT `fk_hist_item_hist_status_id` FOREIGN KEY (`hist_status_id`) REFERENCES
  `hist_status` (`hist_status_id`);
ALTER TABLE `hist_item` ADD CONSTRAINT `fk_hist_item_previewer_tenant_item_xml_id` FOREIGN KEY (`previewer_tenant_item_xml_id`) REFERENCES
  `previewer_tenant_item_xml` (`previewer_tenant_item_xml_id`);
ALTER TABLE `hist_item` ADD CONSTRAINT `fk_hist_item_pending_item_test_cases_id` FOREIGN KEY (`pending_item_test_cases_id`) REFERENCES
  `previewer_tenant_item_test_cases` (`previewer_tenant_item_test_cases_id`);
ALTER TABLE `hist_item` ADD CONSTRAINT `fk_hist_item_approved_item_test_cases_id` FOREIGN KEY (`approved_item_test_cases_id`) REFERENCES
  `previewer_tenant_item_test_cases` (`previewer_tenant_item_test_cases_id`);
ALTER TABLE `hist_item` ADD CONSTRAINT `fk_hist_item_fingerprint_id` FOREIGN KEY (`fingerprint_id`) REFERENCES
  `previewer_tenant_item_fingerprint` (`previewer_tenant_item_fingerprint_id`);

CREATE TABLE `hist_item_compare_result` (
    `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
    `compare_result_hash` VARCHAR(100),
    `compare_result` LONGTEXT,
    PRIMARY KEY (`id`),
    KEY `compare_result_hash_index` (`compare_result_hash`)
) /*! ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8 DEFAULT COLLATE UTF8_UNICODE_CI */;

ALTER TABLE `hist_item`
   ADD COLUMN `hist_item_compare_result_id` BIGINT(11);

ALTER TABLE `hist_test_case` ADD CONSTRAINT `fk_hist_test_case_hist_item_id` FOREIGN KEY (`hist_item_id`) REFERENCES
  `hist_item` (`hist_item_id`);

# --- !Downs

DROP TABLE `hist_item_compare_result`;

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE `hist_item`;

ALTER TABLE `hist_item_old` RENAME TO `hist_item`;

SET FOREIGN_KEY_CHECKS = 1;
