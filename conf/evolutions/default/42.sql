# Enable the percentCorrect scenario class for ePEN.

# --- !Ups
UPDATE scenario_class SET enabled_for_epen = 1 WHERE scenario_class = 'percentCorrect';

# --- !Downs
UPDATE scenario_class SET enabled_for_epen = 0 WHERE scenario_class = 'percentCorrect';