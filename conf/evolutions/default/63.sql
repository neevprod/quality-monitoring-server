# Add battery_student_scenario table.
# Add column battery_student_scenario_id to student_scenario table.

# --- !Ups
CREATE TABLE `Battery_student_scenario` (
  `battery_student_scenario_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `battery_uuid` varchar(80) NOT NULL,
  `battery_test_code` varchar(80) NOT NULL,
  PRIMARY KEY (`battery_student_scenario_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

ALTER TABLE student_scenario
   ADD COLUMN battery_student_scenario_id bigint(20);
   
# --- !Downs
DROP TABLE IF EXISTS `Battery_student_scenario`;

ALTER TABLE `student_scenario`
   DROP COLUMN `battery_student_scenario_id`;