# Add index and foreign key to the student_reservation table.

# --- !Ups
alter table `student_reservation` add key (`start_time`);
alter table `student_reservation` add constraint
  `fk_student_reservation_student_scenario` foreign key (`student_scenario_id`) references
  `student_scenario` (`student_scenario_id`);

# --- !Downs
alter table `student_reservation` drop foreign key `fk_student_reservation_student_scenario`;
alter table `student_reservation` drop key `start_time`;
