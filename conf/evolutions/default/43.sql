# Disable the percentCorrect scenario for now, since that feature has not yet been completed.

# --- !Ups
UPDATE scenario_class SET enabled_for_tn8 = 0, enabled_for_epen = 0 WHERE scenario_class = 'percentCorrect';

# --- !Downs
UPDATE scenario_class SET enabled_for_tn8 = 1, enabled_for_epen = 1 WHERE scenario_class = 'percentCorrect';