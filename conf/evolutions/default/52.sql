# Increase length of previewer_tenant.name column to allow for tenants with names longer than 50 characters.

# --- !Ups
ALTER TABLE `previewer_tenant` MODIFY COLUMN `name` varchar(100) not null;

# --- !Downs
ALTER TABLE `previewer_tenant` MODIFY COLUMN `name` varchar(50) not null;