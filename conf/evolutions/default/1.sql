# Initial setup of the database.

# --- !Ups

CREATE TABLE `PreviewerEnvironment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `previewerApiUrl` varchar(255) NOT NULL,
  `previewerUrl` varchar(255) NOT NULL,
  `protocol` varchar(10) NOT NULL,
  `supportsHttps` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `User` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `ValidationHistory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `discrepancyItemIds` longtext,
  `runDate` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenant_id` bigint(20) NOT NULL,
  `previewerEnv_id` bigint(20) DEFAULT NULL,
  `nItems` int(11) NOT NULL,
  `nItemsProcessed` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6402A9BBF88155E4` (`previewerEnv_id`),
  CONSTRAINT `FK6402A9BBF88155E4` FOREIGN KEY (`previewerEnv_id`) REFERENCES `PreviewerEnvironment` (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `upload` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) DEFAULT NULL,
  `photoFileName` varchar(255) DEFAULT NULL,
  `file` tinyblob,
  PRIMARY KEY (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `test_case_uploads` (
  `test_case_uploads_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `previewer_id` bigint(20) NOT NULL,
  `previewer_tenant_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `items_count` int(11) NOT NULL,
  `test_cases_count` int(11) NOT NULL,
  `failed_test_cases_count` int(11) NOT NULL DEFAULT '0',
  `mismatched_test_cases_count` int(11) NOT NULL,
  `upload_json_result` longblob,
  `processed_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`test_case_uploads_id`),
  KEY `previewer_id` (`previewer_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `test_case_uploads_ibfk_1` FOREIGN KEY (`previewer_id`) REFERENCES `previewerenvironment` (`id`),
  CONSTRAINT `test_case_uploads_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `previewer` (
    `previewer_id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    `url` varchar(255) NOT NULL,
    `api_url` varchar(255) NOT NULL,
    `breadcrumb_name` varchar(255) NOT NULL,
    `protocol` varchar(10) NOT NULL,
    `supports_https` tinyint(1) NOT NULL,
    `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
    PRIMARY KEY (`previewer_id`),
    UNIQUE KEY `UN_PREVIEWER` (`name` , `url`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `previewer_tenant` (
    `previewer_tenant_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `previewer_id` int(11) NOT NULL,
    `tenant_id` bigint(20) NOT NULL,
    `name` varchar(50) NOT NULL,
    `active` tinyint(1) NOT NULL,
    `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
    PRIMARY KEY (`previewer_tenant_id`),
    UNIQUE KEY `UN_PREVIEWER_TENANT` (`previewer_id` , `tenant_id`),
    FOREIGN KEY (`previewer_id`)
        REFERENCES `previewer` (`previewer_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `status` (
    `status_id` int(2) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    PRIMARY KEY (`status_id`),
    UNIQUE KEY `UN_STATUS` (`name`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `previewer_tenant_item` (
    `previewer_tenant_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `previewer_tenant_id` bigint(20) NOT NULL,
    `item_id` bigint(20) DEFAULT NULL,
    `item_identifier` varchar(100) NOT NULL,
    `active` tinyint(1) NOT NULL,
    `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
    PRIMARY KEY (`previewer_tenant_item_id`),
    UNIQUE KEY `UN_PT_ITEM_IDENTIFIER` (`previewer_tenant_id` , `item_identifier`),
    FOREIGN KEY (`previewer_tenant_id`)
        REFERENCES `previewer_tenant` (`previewer_tenant_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `previewer_tenant_item_test_cases` (
    `previewer_tenant_item_test_cases_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `previewer_tenant_item_id` bigint(20) NOT NULL,
    `status_id` int(2) NOT NULL,
    `test_cases` longblob,
    `test_cases_version` int(10) NOT NULL,
    `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
    PRIMARY KEY (`previewer_tenant_item_test_cases_id`),
    UNIQUE KEY `UN_PT_ITEM_TC_V` (`previewer_tenant_item_id` , `status_id` , `test_cases_version`),
    FOREIGN KEY (`previewer_tenant_item_id`)
        REFERENCES `previewer_tenant_item` (`previewer_tenant_item_id`),
    FOREIGN KEY (`status_id`)
        REFERENCES `status` (`status_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `previewer_tenant_item_fingerprint` (
    `previewer_tenant_item_fingerprint_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `previewer_tenant_item_id` bigint(20) NOT NULL,
    `fingerprint` longblob,
    `fingerprint_version` int(10) NOT NULL,
    `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
    PRIMARY KEY (`previewer_tenant_item_fingerprint_id`),
    UNIQUE KEY `UN_PT_ITEM_FP_V` (`previewer_tenant_item_id` , `fingerprint_version`),
    FOREIGN KEY (`previewer_tenant_item_id`)
        REFERENCES `previewer_tenant_item` (`previewer_tenant_item_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `previewer_tenant_item_xml` (
    `previewer_tenant_item_xml_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `previewer_tenant_item_id` bigint(20) NOT NULL,
    `identifier_to_interaction_map` longtext DEFAULT NULL,
    `item_xml` longtext,
    `xsd_validation_errors` longtext DEFAULT NULL,
    `is_valid` tinyint(1) NOT NULL,
    `item_xml_version` int(10) NOT NULL,
    `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
    PRIMARY KEY (`previewer_tenant_item_xml_id`),
    UNIQUE KEY `UN_PT_ITEM_XML_V` (`previewer_tenant_item_id` , `item_xml_version`),
    FOREIGN KEY (`previewer_tenant_item_id`)
        REFERENCES `previewer_tenant_item` (`previewer_tenant_item_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_exec` (
    `hist_exec_id` int(11) NOT NULL AUTO_INCREMENT,
    `previewer_id` int(11) NOT NULL,
    `exec_start` timestamp DEFAULT CURRENT_TIMESTAMP,
    `exec_stop` timestamp /*! ON UPDATE CURRENT_TIMESTAMP */,
    `exec_completed` tinyint(1) NOT NULL DEFAULT false,
    PRIMARY KEY (`hist_exec_id`),
    FOREIGN KEY (`previewer_id`)
        REFERENCES `previewer` (`previewer_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_exec_report` (
    `hist_exec_report_id` int(11) NOT NULL AUTO_INCREMENT,
    `hist_exec_id` int(11) NOT NULL,
    `hist_exec_report` longblob,
    PRIMARY KEY (`hist_exec_report_id`),
    FOREIGN KEY (`hist_exec_id`)
        REFERENCES `hist_exec` (`hist_exec_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_status` (
    `hist_status_id` int(2) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    PRIMARY KEY (`hist_status_id`),
    UNIQUE KEY `UN_HIST_STATUS` (`name`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_tenant` (
    `hist_tenant_id` int(11) NOT NULL AUTO_INCREMENT,
    `hist_exec_id` int(11) NOT NULL,
    `previewer_tenant_id` bigint(20) NOT NULL,
    `exec_start` timestamp DEFAULT CURRENT_TIMESTAMP,
    `exec_stop` timestamp /*! ON UPDATE CURRENT_TIMESTAMP */,
    `exec_completed` tinyint(1) NOT NULL DEFAULT false,
    `hist_status_id` int(2) NOT NULL,
    `items_validated` int(11) NOT NULL DEFAULT '-1',
    `items_with_failed_tcs` int(11) NOT NULL DEFAULT '-1',
    `items_with_pending_tcs` int(11) NOT NULL DEFAULT '-1',
    `deactivated_items` int(11) NOT NULL DEFAULT '-1',
    PRIMARY KEY (`hist_tenant_id`),
    FOREIGN KEY (`hist_exec_id`)
        REFERENCES `hist_exec` (`hist_exec_id`),
    FOREIGN KEY (`previewer_tenant_id`)
        REFERENCES `previewer_tenant` (`previewer_tenant_id`),
    FOREIGN KEY (`hist_status_id`)
        REFERENCES `hist_status` (`hist_status_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_item` (
  `hist_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `hist_tenant_id` int(11) NOT NULL,
  `previewer_tenant_item_id` bigint(20) NOT NULL,
  `previewer_tenant_item_xml_id` bigint(20) NOT NULL,
  `hist_status_id` int(2) NOT NULL,
  `pending_item_test_cases_id` bigint(20) DEFAULT NULL,
  `approved_item_test_cases_id` bigint(20) DEFAULT NULL,
  `fingerprint_id` bigint(20) DEFAULT NULL,
  `test_cases` int(11) NOT NULL DEFAULT '-1',
  `failed_test_cases` int(11) NOT NULL DEFAULT '-1',
  `pending_test_cases` int(11) NOT NULL DEFAULT '-1',
  `compare_result` longblob,
  `all_test_cases_passed` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`hist_item_id`),
  KEY `hist_tenant_id` (`hist_tenant_id`),
  KEY `previewer_tenant_item_id` (`previewer_tenant_item_id`),
  KEY `hist_status_id` (`hist_status_id`),
  KEY `previewer_tenant_item_xml_id` (`previewer_tenant_item_xml_id`),
  CONSTRAINT `hist_item_ibfk_1` FOREIGN KEY (`hist_tenant_id`) REFERENCES `hist_tenant` (`hist_tenant_id`),
  CONSTRAINT `hist_item_ibfk_2` FOREIGN KEY (`previewer_tenant_item_id`) REFERENCES `previewer_tenant_item` (`previewer_tenant_item_id`),
  CONSTRAINT `hist_item_ibfk_3` FOREIGN KEY (`hist_status_id`) REFERENCES `hist_status` (`hist_status_id`),
  CONSTRAINT `hist_item_ibfk_4` FOREIGN KEY (`previewer_tenant_item_xml_id`) REFERENCES `previewer_tenant_item_xml` (`previewer_tenant_item_xml_id`),
  CONSTRAINT `hist_item_ibfk_5` FOREIGN KEY (`pending_item_test_cases_id`) REFERENCES `previewer_tenant_item_test_cases` (`previewer_tenant_item_test_cases_id`),
  CONSTRAINT `hist_item_ibfk_6` FOREIGN KEY (`approved_item_test_cases_id`) REFERENCES `previewer_tenant_item_test_cases` (`previewer_tenant_item_test_cases_id`),
  CONSTRAINT `hist_item_ibfk_7` FOREIGN KEY (`fingerprint_id`) REFERENCES `previewer_tenant_item_fingerprint` (`previewer_tenant_item_fingerprint_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_test_case` (
    `hist_test_case_id` int(11) NOT NULL AUTO_INCREMENT,
    `hist_item_id` int(11) NOT NULL,
    `failed_test_case_expected` longblob,
    `failed_test_case_actual` longblob,
    PRIMARY KEY (`hist_test_case_id`),
    FOREIGN KEY (`hist_item_id`)
        REFERENCES `hist_item` (`hist_item_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_item_exception` (
  `hist_item_exception_id` int(11) NOT NULL AUTO_INCREMENT,
  `hist_tenant_id` int(11) NOT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `item_identifier` varchar(100) NOT NULL,
  `exception` longblob,
  PRIMARY KEY (`hist_item_exception_id`),
  FOREIGN KEY (`hist_tenant_id`) REFERENCES `hist_tenant` (`hist_tenant_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_tenant_exception` (
  `hist_tenant_exception_id` int(11) NOT NULL AUTO_INCREMENT,
  `hist_tenant_id` int(11) NOT NULL,
  `exception` longblob,
  PRIMARY KEY (`hist_tenant_exception_id`),
  FOREIGN KEY (`hist_tenant_id`) REFERENCES `hist_tenant` (`hist_tenant_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_previewer_exception` (
  `hist_previewer_exception_id` int(11) NOT NULL AUTO_INCREMENT,
  `previewer_id` int(11) NOT NULL,
  `exception` longblob,
  PRIMARY KEY (`hist_previewer_exception_id`),
  FOREIGN KEY (`previewer_id`) REFERENCES `previewer` (`previewer_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `job_type` (
  `job_type_id` int(2) NOT NULL AUTO_INCREMENT,
  `code` varchar(8) DEFAULT NULL,
  `name` varchar(140) NOT NULL,
  PRIMARY KEY (`job_type_id`),
  UNIQUE KEY `UN_CODE` (`code`),
  UNIQUE KEY `UN_NAME` (`name`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `user_tenant_job_subscription` (
    `user_tenant_job_subscription_id` bigint(20) NOT NULL AUTO_INCREMENT,
    `user_id` bigint(20) DEFAULT NULL,
    `previewer_tenant_id` bigint(20) NOT NULL,
    `job_type_id` int(2) NOT NULL,
    `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
    PRIMARY KEY (`user_tenant_job_subscription_id`),
    UNIQUE KEY `UK_USER_TENANT_JOB` (`user_id` , `previewer_tenant_id` , `job_type_id`),
    FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
    FOREIGN KEY (`previewer_tenant_id`) REFERENCES `previewer_tenant` (`previewer_tenant_id`),
    FOREIGN KEY (`job_type_id`) REFERENCES `job_type` (`job_type_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

INSERT INTO PreviewerEnvironment(previewerApiUrl, previewerUrl, protocol, supportsHttps) values('tn8previewer-dev.elasticbeanstalk.com','tn8previewer-dev.elasticbeanstalk.com','http:',0),('api-parccpreviewer.testnav.com','parccpreviewer.testnav.com','http:',1),('api-tn8previewer.testnav.com','tn8previewer.testnav.com','http:',1),('localhost:9090','localhost:9090','http:',0),('api-conversion.actaspire.org','conversion.actaspire.org','http:',1);

INSERT INTO User(email, fullname, isAdmin, password) values('paul.grudnitski@gmail.com','Paul Grudnitski',1,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('jeremy.goodell@pearson.com','Jeremy Goodell',1,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('tate.durlam@pearson.com','Tate Durlam',1,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('chris.zinkula@pearson.com','Chris Zinkula',1,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('cole.cecil@pearson.com','Cole Cecil',1,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('wayne.ostler@pearson.com','Wayne Ostler',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('titus.bensigar@pearson.com','Titus Bensigar',1,'5f4dcc3b5aa765d61d8327deb882cf99'),('denny.way@pearson.com','Denny Way',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('kristi.thompson@pearson.com','Kristi Thompson',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('rachel.penticuff@pearson.com','Rachel Penticuff',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('robert.sowells@pearson.com','Robert Sowells',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('katherine.brien@pearson.com','Kate Brien',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('randa.elmoazzen@pearson.com','Randa Elmoazzen',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('james.hitchcock@pearson.com','Mitchell Hitchcock',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('jacqueline.lynch@pearson.com','Jacqueline Lynch',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('sean.duffy@pearson.com','Sean Duffy',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('theresa.meyer@pearson.com','Theresa Meyer',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('susan.mcnally@pearson.com','Susan McNally',0,'5ebe2294ecd0e0f08eab7690d2a6ee69'),('dcp-compprod@pearson.com','DCP CompProd',0,'070617a11f95a34a3288decf4870b9cf'),('dcp-techprod@pearson.com','DCP TechProd',0,'c7d27ccf203351207e157a89412101de'),('dcp-assetprod@pearson.com','DCP AssetProd',0,'70f227f1dbde1569b8c920abb726297c'),('dcp-compositionqa@pearson.com','DCP CompQA',0,'38a9a6fffaaf89242cc4c43667102018');

INSERT INTO status(name) values('PENDING'),('APPROVED');

INSERT INTO previewer(name, url, api_url, breadcrumb_name, protocol, supports_https, last_update) values('DEV','https://dev-tn8previewer.pearsondev.com','tn8previewer-dev.elasticbeanstalk.com','tn8previewer-dev.elasticbeanstalk.com','http:',0,NOW()),('PARCC','https://parccpreviewer.testnav.com','api-parccpreviewer.testnav.com','parccpreviewer.testnav.com','http:',1,NOW()),('TN8','https://tn8previewer.testnav.com','api-tn8previewer.testnav.com','tn8previewer.testnav.com','http:',1,NOW()),('ASPIRE', 'https://conversion.actaspire.org','api-conversion.actaspire.org','conversion.actaspire.org','http:',1,NOW());

INSERT INTO hist_status(name) values('ADDED'),('DEACTIVATED'),('EXECUTED'),('REACTIVATED');

INSERT INTO job_type(code, name) values('SCV','Score Consistency Validation');

# --- !Downs

DROP TABLE IF EXISTS `user_tenant_job_subscription`, `job_type`, `ValidationHistory`, `test_case_uploads`, `PreviewerEnvironment`, `User`, `upload`, `hist_test_case`, `hist_item`, `hist_item_exception`, `hist_tenant_exception`, `hist_previewer_exception`, `hist_tenant`, `hist_exec_report`, `hist_exec`, `hist_status`, `previewer_tenant_item_test_cases`, `previewer_tenant_item_fingerprint`, `previewer_tenant_item_xml`, `previewer_tenant_item`, `previewer_tenant`, `previewer`, `status`;

