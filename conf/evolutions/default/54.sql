# Increase size of scenario field in scenario table

# --- !Ups
ALTER TABLE `scenario`
  MODIFY `scenario` MEDIUMBLOB NOT NULL;

# --- !Downs
ALTER TABLE `scenario`
  MODIFY `scenario` BLOB NOT NULL;
