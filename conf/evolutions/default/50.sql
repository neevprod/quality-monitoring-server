# Increase size of result_details field in student_scenario_result table

# --- !Ups
ALTER TABLE `student_scenario_result`
  MODIFY `result_details` MEDIUMBLOB;

# --- !Downs
ALTER TABLE `student_scenario_result`
  MODIFY `result_details` BLOB;
