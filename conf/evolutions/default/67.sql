# Add archived field to environment table.

# --- !Ups

alter table `environment` add column `archived` tinyint(1) default 0 not null after `name`;
alter table `environment` add key `key_archived` (`archived`);

# --- !Downs

alter table `environment` drop column `archived`;