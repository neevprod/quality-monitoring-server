# Add permissions for user management module.

# --- !Ups
insert into `application_module` (`application_module_name`, `permission_type_id`) values
  ('userManagement', (select t.`permission_type_id` from `permission_type` t where t.`permission_type_name` = 'ModuleLevelPermission'));

insert into `permission_option` (`name`, `application_module_id`, `create_access`, `read_access`, `update_access`,`delete_access`) values
  ('user', (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'userManagement'), 1, 1, 1, 1),
  ('role', (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'userManagement'), 1, 1, 1, 1);

# --- !Downs
delete from `permission_option` where `name` = 'user' and `application_module_id` =
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'userManagement');
delete from `permission_option` where `name` = 'role' and `application_module_id` =
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'userManagement');

delete from `application_module` where `application_module_name` = 'userManagement';

