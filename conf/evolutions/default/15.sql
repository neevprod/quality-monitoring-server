# Split user into two subtypes, human_user and api_user.

# --- !Ups

create table `human_user` (
  `human_user_id` bigint(20) not null auto_increment,
  `user_id` bigint(20) not null,
  `email` varchar(255) not null,
  `password` varchar(255) not null,
  primary key (`human_user_id`),
  foreign key (`user_id`) references `user` (`id`),
  unique key `un_email` (`email`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

create table `api_user` (
  `api_user_id` bigint(20) not null auto_increment,
  `user_id` bigint(20) not null,
  `api_key_name` varchar(48) not null,
  `api_key` varchar(24) not null,
  primary key (`api_user_id`),
  foreign key (`user_id`) references `user` (`id`),
  unique key `un_api_key_name` (`api_key_name`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

insert into `human_user` (`user_id`, `email`, `password`)
  select `id`, `email`, `password`
  from `user`;

alter table `user` add column `is_api_user` tinyint(1);
update `user` set `is_api_user` = 0;
alter table `user` modify column `is_api_user` tinyint(1) not null;

alter table `user` drop column `email`;
alter table `user` drop column `password`;
alter table `user` modify column `fullname` varchar(255) not null;

drop table `user_tenant_job_subscription`;
CREATE TABLE `user_tenant_job_subscription` (
  `user_tenant_job_subscription_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `human_user_id` bigint(20) DEFAULT NULL,
  `previewer_tenant_id` bigint(20) NOT NULL,
  `job_type_id` int(2) NOT NULL,
  `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
  PRIMARY KEY (`user_tenant_job_subscription_id`),
  UNIQUE KEY `UK_USER_TENANT_JOB` (`human_user_id` , `previewer_tenant_id` , `job_type_id`),
  FOREIGN KEY (`human_user_id`) REFERENCES `human_user` (`human_user_id`),
  FOREIGN KEY (`previewer_tenant_id`) REFERENCES `previewer_tenant` (`previewer_tenant_id`),
  FOREIGN KEY (`job_type_id`) REFERENCES `job_type` (`job_type_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

# --- !Downs

drop table `user_tenant_job_subscription`;
CREATE TABLE `user_tenant_job_subscription` (
  `user_tenant_job_subscription_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `previewer_tenant_id` bigint(20) NOT NULL,
  `job_type_id` int(2) NOT NULL,
  `last_update` timestamp DEFAULT CURRENT_TIMESTAMP /*! ON UPDATE CURRENT_TIMESTAMP */,
  PRIMARY KEY (`user_tenant_job_subscription_id`),
  UNIQUE KEY `UK_USER_TENANT_JOB` (`user_id` , `previewer_tenant_id` , `job_type_id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  FOREIGN KEY (`previewer_tenant_id`) REFERENCES `previewer_tenant` (`previewer_tenant_id`),
  FOREIGN KEY (`job_type_id`) REFERENCES `job_type` (`job_type_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

alter table `user` modify column `fullname` varchar(255) default null;
alter table `user` add column `email` varchar(255) default null after `id`;
alter table `user` add column `password` varchar(255) default null;
update `user` u set
  u.`email` = (select nu.`email` from `human_user` nu where u.`id` = nu.`user_id`),
  u.`password` = (select nu.`password` from `human_user` nu where u.`id` = nu.`user_id`);
alter table `user` drop column `is_api_user`;

drop table if exists `human_user`, `api_user`;
