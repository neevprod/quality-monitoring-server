# Update testmp_xml and temp_response_pool from MEDIUMTEXT to LONGTEXT.

# --- !Ups
ALTER TABLE testmap_detail MODIFY COLUMN testmap_xml LONGTEXT;
ALTER TABLE testmap_detail MODIFY COLUMN temp_response_pool LONGTEXT;
