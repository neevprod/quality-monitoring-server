# Remove environment.wf_mysql_connection_id column as we already have this data in student_scenario.wf_db_connection_id

# --- !Ups
ALTER TABLE `environment` DROP COLUMN `wf_mysql_connection_id`;

# --- !Downs
ALTER TABLE `environment` ADD COLUMN `wf_mysql_connection_id`  mediumint(8) NOT NULL;