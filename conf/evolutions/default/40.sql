# Enable the percentCorrect scenario class.

# --- !Ups
UPDATE scenario_class SET enabled_for_tn8 = 1 WHERE scenario_class = 'percentCorrect';

# --- !Downs
UPDATE scenario_class SET enabled_for_tn8 = 0 WHERE scenario_class = 'percentCorrect';