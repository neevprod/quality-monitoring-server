# Create application_setting table

# --- !Ups
create table `application_setting` (
  `application_setting_id` int(11) not null auto_increment,
  `application_setting_name` varchar(50) not null,
  `application_setting_value` varchar(50) not null,
  primary key (`application_setting_id`),
  unique key `un_application_setting_name` (`application_setting_name`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

insert into `application_setting` (`application_setting_name`, `application_setting_value`) values
  ('nightly_job_enabled', '1'), ('nightly_job_start_hour', '23'), ('nightly_job_start_minute', '0'),
  ('old_response_generator_enabled', '1');

# --- !Downs

drop table `application_setting`;
