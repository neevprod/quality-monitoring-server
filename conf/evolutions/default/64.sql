# Add epen_user_reservation table.

# --- !Ups
CREATE TABLE `epen_user_reservation` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `locked` tinyint(1) NOT NULL DEFAULT '1',
  `locked_timestamp` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

# --- !Downs
DROP TABLE IF EXISTS `epen_user_reservation`;