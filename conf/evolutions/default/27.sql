# Change completion date to nullable in div_job_exec table.

# --- !Ups
alter table `div_job_exec` modify `complete_time_stamp` timestamp null;

# --- !Downs
alter table `div_job_exec` modify `complete_time_stamp` timestamp not null default '0000-00-00 00:00:00';
