# Add table for extra reporting on past job executions.

# --- !Ups

create table `job_execution` (
  `job_execution_id` bigint(20) not null auto_increment,
  `job_id` varchar(100) not null,
  `run_number` int(11) not null,
  `success` tinyint(1) not null,
  `result_json` longblob,
  `completion_time` timestamp not null default current_timestamp,
  primary key (`job_execution_id`),
  key `job_id` (`job_id`),
  unique key `job_id_run_number` (`job_id`, `run_number`),
  key `job_id_completion_time` (`job_id`, `completion_time`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;

# --- !Downs

drop table if exists `job_execution`;
