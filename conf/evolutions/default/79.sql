# Add KT Test Case table

# --- !Ups
CREATE TABLE `kt_test_case`
(
  kt_test_case_id       MEDIUMINT   NOT NULL AUTO_INCREMENT,
  kt_id                 VARCHAR(50),
  kt_candidate_response MEDIUMTEXT  NOT NULL,
  kt_response_id        VARCHAR(50),
  kt_saved_score        VARCHAR(50) NOT NULL,
  kt_saved_score_state  VARCHAR(50),
  kt_include_in_build   TINYINT(1),
  item_identifier       VARCHAR(50) NOT NULL,
  response_identifier   VARCHAR(50) NOT NULL,
  PRIMARY KEY (`kt_test_case_id`)
)/*! ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8 */;

# --- !Downs
DROP TABLE IF EXISTS `kt_test_case`;