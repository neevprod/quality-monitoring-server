# Update grid_job add boolean columns for release and is6by8 

# --- !Ups
ALTER TABLE grid_job ADD COLUMN autorelease tinyint(1) AFTER comments;
ALTER TABLE grid_job ADD COLUMN is6by8 tinyint(1) AFTER comments;