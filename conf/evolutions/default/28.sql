# Add columns to enable/disable scenario classes for TestNav8 and ePEN.

# --- !Ups
alter table `scenario_class` add column `enabled_for_tn8` tinyint(1) not null default 0;
alter table `scenario_class` add column `enabled_for_epen` tinyint(1) not null default 0;

update `scenario_class` set `enabled_for_tn8` = 1 where `scenario_class_id` in
  (select sc.`scenario_class_id` from (select * from `scenario_class`) sc
    where sc.`scenario_class` in ('allCorrect', 'allIncorrect', 'maxPoints', 'minPoints'));
update `scenario_class` set `enabled_for_epen` = 1 where `scenario_class_id` in
  (select sc.`scenario_class_id` from (select * from `scenario_class`) sc
    where sc.`scenario_class` in ('allCorrect', 'allIncorrect', 'maxPoints', 'minPoints'));

# --- !Downs
alter table `scenario_class` drop column `enabled_for_epen`;
alter table `scenario_class` drop column `enabled_for_tn8`;
