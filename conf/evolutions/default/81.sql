# Add allCorrectWithUniqueScore scenario class in scenario_class table.

# --- !Ups
INSERT INTO scenario_class(scenario_class_id, scenario_class, enabled_for_tn8, enabled_for_epen)
VALUES (14, 'allCorrectWithUniqueScore', 1, 0);