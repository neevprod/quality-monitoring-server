# Clear users inserted by earlier evolution.

# --- !Ups
delete from `user_tenant_job_subscription`;
delete from `test_case_uploads`;
delete from `user_permissions`;
delete from `human_user`;
delete from `user` where `is_api_user` = 0;

# --- !Downs
insert into `user` (`fullname`, `isAdmin`, `is_api_user`) values
  ('Paul Grudnitski',1,0),
  ('Jeremy Goodell',1,0),
  ('Tate Durlam',1,0),
  ('Chris Zinkula',1,0),
  ('Cole Cecil',1,0),
  ('Wayne Ostler',0,0),
  ('Titus Bensigar',1,0),
  ('Denny Way',0,0),
  ('Kristi Thompson',0,0),
  ('Rachel Penticuff',0,0),
  ('Robert Sowells',0,0),
  ('Kate Brien',0,0),
  ('Randa Elmoazzen',0,0),
  ('Mitchell Hitchcock',0,0),
  ('Jacqueline Lynch',0,0),
  ('Sean Duffy',0,0),
  ('Theresa Meyer',0,0),
  ('Susan McNally',0,0),
  ('DCP CompProd',0,0),
  ('DCP TechProd',0,0),
  ('DCP AssetProd',0,0),
  ('DCP CompQA',0,0);

insert into `human_user` (`email`, `user_id`, `password`) values
  ('paul.grudnitski@gmail.com',(select `id` from `user` where `fullname` = 'Paul Grudnitski'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('jeremy.goodell@pearson.com',(select `id` from `user` where `fullname` = 'Jeremy Goodell'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('tate.durlam@pearson.com',(select `id` from `user` where `fullname` = 'Tate Durlam'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('chris.zinkula@pearson.com',(select `id` from `user` where `fullname` = 'Chris Zinkula'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('cole.cecil@pearson.com',(select `id` from `user` where `fullname` = 'Cole Cecil'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('wayne.ostler@pearson.com',(select `id` from `user` where `fullname` = 'Wayne Ostler'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('titus.bensigar@pearson.com',(select `id` from `user` where `fullname` = 'Titus Bensigar'),'5f4dcc3b5aa765d61d8327deb882cf99'),
  ('denny.way@pearson.com',(select `id` from `user` where `fullname` = 'Denny Way'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('kristi.thompson@pearson.com',(select `id` from `user` where `fullname` = 'Kristi Thompson'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('rachel.penticuff@pearson.com',(select `id` from `user` where `fullname` = 'Rachel Penticuff'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('robert.sowells@pearson.com',(select `id` from `user` where `fullname` = 'Robert Sowells'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('katherine.brien@pearson.com',(select `id` from `user` where `fullname` = 'Kate Brien'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('randa.elmoazzen@pearson.com',(select `id` from `user` where `fullname` = 'Randa Elmoazzen'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('james.hitchcock@pearson.com',(select `id` from `user` where `fullname` = 'Mitchell Hitchcock'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('jacqueline.lynch@pearson.com',(select `id` from `user` where `fullname` = 'Jacqueline Lynch'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('sean.duffy@pearson.com',(select `id` from `user` where `fullname` = 'Sean Duffy'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('theresa.meyer@pearson.com',(select `id` from `user` where `fullname` = 'Theresa Meyer'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('susan.mcnally@pearson.com',(select `id` from `user` where `fullname` = 'Susan McNally'),'5ebe2294ecd0e0f08eab7690d2a6ee69'),
  ('dcp-compprod@pearson.com',(select `id` from `user` where `fullname` = 'DCP CompProd'),'070617a11f95a34a3288decf4870b9cf'),
  ('dcp-techprod@pearson.com',(select `id` from `user` where `fullname` = 'DCP TechProd'),'c7d27ccf203351207e157a89412101de'),
  ('dcp-assetprod@pearson.com',(select `id` from `user` where `fullname` = 'DCP AssetProd'),'70f227f1dbde1569b8c920abb726297c'),
  ('dcp-compositionqa@pearson.com',(select `id` from `user` where `fullname` = 'DCP CompQA'),'38a9a6fffaaf89242cc4c43667102018');

