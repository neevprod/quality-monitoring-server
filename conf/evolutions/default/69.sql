# Add unique constraint to name field.

# --- !Ups
UPDATE previewer
SET url = 'abbi-tn8previewer.testnav.com',
  api_url = 'abbi-tn8previewer.testnav.com'
WHERE name = 'ABBI';

UPDATE previewer
SET url = 'tn8previewer-dev.testnav.com',
  api_url = 'tn8previewer-dev.testnav.com'
WHERE name = 'DEV';

# --- !Downs
UPDATE previewer
SET url = 'abbi-previewer.elasticbeanstalk.com',
  api_url = 'abbi-previewer.elasticbeanstalk.com'
WHERE name = 'ABBI';

UPDATE previewer
SET url = 'dev-tn8previewer.pearsondev.com',
  api_url = 'tn8previewer-dev.elasticbeanstalk.com'
WHERE name = 'DEV';