# Add permissions for System Validation module.

# --- !Ups
insert into `application_module` (`application_module_name`, `permission_type_id`) values
  ('systemValidation', (select t.`permission_type_id` from `permission_type` t where t.`permission_type_name` = 'EnvironmentLevelPermission'));

insert into `permission_option` (`name`, `application_module_id`, `create_access`, `read_access`, `update_access`,
  `delete_access`) values
  ('environments', (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation'), 1, 0, 1, 0),
  ('divJobExecutions', (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation'), 1, 1, 0, 0),
  ('divJob', (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation'), 1, 1, 0, 0);

# --- !Downs
delete from `permission_option` where `name` = 'environments' and `application_module_id` =
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation');
delete from `permission_option` where `name` = 'divJobExecutions' and `application_module_id` =
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation');
delete from `permission_option` where `name` = 'divJob' and `application_module_id` =
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation');

delete from `application_module` where `application_module_name` = 'systemValidation';

