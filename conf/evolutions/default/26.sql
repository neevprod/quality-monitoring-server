# Add environment.epen2_mongo_connection_id column.

# --- !Ups
ALTER TABLE `environment` ADD COLUMN `epen2_mongo_connection_id`  mediumint(8) NOT NULL;

insert into `db_connection` (host, port, db_name, db_user, db_pass) VALUES ('10.168.27.62', 27017, 'na', 'na', 'na');
update environment set epen2_mongo_connection_id = (select db_connection_id from db_connection where host = '10.168.27.62');

# --- !Downs
ALTER TABLE `environment` DROP COLUMN `epen2_mongo_connection_id`;

delete from `db_connection` WHERE host = '10.168.27.62';