# Increase length of DB columns to allow encryption.

# --- !Ups
alter table `api_user` modify column `api_key` varchar(100) not null;
alter table `db_connection` modify column `db_pass` varchar(100) not null;
alter table `api_connection` modify column `secret` varchar(100) not null;

# --- !Downs
alter table `api_connection` modify column `secret` varchar(50) not null;
alter table `db_connection` modify column `db_pass` varchar(50) not null;
alter table `api_user` modify column `api_key` varchar(24) not null;
