# Add permissions for System Validation module.

# --- !Ups

INSERT INTO `permission_option` (`name`, `application_module_id`, `create_access`, `read_access`, `update_access`, `delete_access`) VALUES ('usageStatistics',(select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation'), 0, 1, 0, 0);

# --- !Downs

Delete from `permission_option` where `name` = 'usageStatistics' and `application_module_id` =
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'systemValidation');