# Remove column disable_epen_scenario_checked from student_scenario table.

# --- !Ups
ALTER TABLE `student_scenario` DROP COLUMN `disable_epen_scenario_checked`;

# --- !Downs
alter table `student_scenario` add column `disable_epen_scenario_checked` BIT(1) DEFAULT 0;