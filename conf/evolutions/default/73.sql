# Add columns to persist testmapXML, itemResponsePool and previewerTestmapId.

# --- !Ups
alter table `testmap_detail` add column `testmap_xml` mediumtext COMMENT 'The testmap XML';
alter table `testmap_detail` add column `temp_response_pool` mediumtext COMMENT 'The itemResponsePool';
alter table `testmap_detail` add column `previewer_testmap_id` int(11) COMMENT 'The previewerTestmapId';

# --- !Downs
alter table testmap_detail drop column `testmap_xml`;
alter table testmap_detail drop column `temp_response_pool`;
alter table testmap_detail drop column `previewer_testmap_id`;
