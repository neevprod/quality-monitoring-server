# Remove unused tables
# Convert colums that could contain non-English characters from BLOB to TEXT

# --- !Ups
DROP TABLE hist_exec_report;

DROP TABLE upload;

DROP TABLE ValidationHistory;

ALTER TABLE job_execution modify result_json LONGTEXT;

ALTER TABLE student_scenario_result modify result_details MEDIUMTEXT;

ALTER TABLE scenario modify scenario MEDIUMTEXT;

ALTER TABLE test_case_uploads modify upload_json_result LONGTEXT;

# --- !Downs

ALTER TABLE test_case_uploads modify upload_json_result LONGBLOB;

ALTER TABLE scenario modify scenario MEDIUMBLOB;

ALTER TABLE student_scenario_result modify result_details MEDIUMBLOB;

ALTER TABLE job_execution modify result_json LONGBLOB;

CREATE TABLE `ValidationHistory` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `discrepancyItemIds` longtext,
  `runDate` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tenant_id` bigint(20) NOT NULL,
  `previewer_id` int(11) DEFAULT NULL,
  `nItems` int(11) NOT NULL,
  `nItemsProcessed` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6402A9BBF88155E4` (`previewer_id`),
  CONSTRAINT `fk_ValidationHistory_previewer_previewer_id` FOREIGN KEY (`previewer_id`) REFERENCES `previewer` (`previewer_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `upload` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) DEFAULT NULL,
  `photoFileName` varchar(255) DEFAULT NULL,
  `file` tinyblob,
  PRIMARY KEY (`id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;

CREATE TABLE `hist_exec_report` (
    `hist_exec_report_id` int(11) NOT NULL AUTO_INCREMENT,
    `hist_exec_id` int(11) NOT NULL,
    `hist_exec_report` longblob,
    PRIMARY KEY (`hist_exec_report_id`),
    FOREIGN KEY (`hist_exec_id`)
        REFERENCES `hist_exec` (`hist_exec_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 ROW_FORMAT=COMPRESSED DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci */;
