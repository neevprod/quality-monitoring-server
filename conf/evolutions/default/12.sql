# Made last_completed_state_id nullable and renamed testnav_login_id & testnav_password columns in student_scenario table

# --- !Ups
ALTER TABLE `student_scenario` MODIFY COLUMN `last_completed_state_id` smallint(5) DEFAULT NULL;
ALTER TABLE `student_scenario` CHANGE COLUMN `testnav_login_id` `testnav_login_name` varchar(90) NOT NULL;
ALTER TABLE `student_scenario` CHANGE COLUMN `testnav_password` `testnav_login_password` varchar(40) NOT NULL;

# --- !Downs
ALTER TABLE `student_scenario` MODIFY COLUMN `last_completed_state_id` smallint(5) NOT NULL;
ALTER TABLE `student_scenario` CHANGE COLUMN `testnav_login_name` `testnav_login_id` varchar(90) NOT NULL;
ALTER TABLE `student_scenario` CHANGE COLUMN `testnav_login_password` `testnav_password` varchar(40) NOT NULL;