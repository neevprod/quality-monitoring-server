# Add Schoolnet Previewer.

# --- !Ups
INSERT INTO previewer(name,
                      url,
                      api_url,
                      protocol,
                      api_protocol,
                      supports_https,
                      last_update)
VALUES ('SCHOOLNET',
        'test-tn8previewer-sn.testnav.com',
        'test-tn8previewer-sn.testnav.com',
        'https://',
        'https://',
        1,
        NOW());

# --- !Downs

DELETE FROM previewer_tenant_item_xml
WHERE previewer_tenant_item_id IN
      (SELECT pti.item_id
       FROM previewer p
         JOIN previewer_tenant pt ON pt.previewer_id = p.previewer_id
         LEFT JOIN previewer_tenant_item pti
           ON pti.previewer_tenant_id = pt.previewer_tenant_id
       WHERE p.name = 'SCHOOLNET');

DELETE FROM previewer_tenant_item_test_cases
WHERE previewer_tenant_item_id IN
      (SELECT pti.item_id
       FROM previewer p
         JOIN previewer_tenant pt ON pt.previewer_id = p.previewer_id
         LEFT JOIN previewer_tenant_item pti
           ON pti.previewer_tenant_id = pt.previewer_tenant_id
       WHERE p.name = 'SCHOOLNET');

DELETE FROM previewer_tenant_item_fingerprint
WHERE previewer_tenant_item_id IN
      (SELECT pti.item_id
       FROM previewer p
         JOIN previewer_tenant pt ON pt.previewer_id = p.previewer_id
         LEFT JOIN previewer_tenant_item pti
           ON pti.previewer_tenant_id = pt.previewer_tenant_id
       WHERE p.name = 'SCHOOLNET');

DELETE FROM previewer_tenant_item
WHERE previewer_tenant_id IN
      (SELECT pt.previewer_tenant_id
       FROM previewer p
         JOIN previewer_tenant pt ON pt.previewer_id = p.previewer_id
       WHERE p.name = 'SCHOOLNET');

DELETE FROM previewer_tenant
WHERE previewer_id = (SELECT previewer_id
                      FROM previewer
                      WHERE name = 'SCHOOLNET');

DELETE FROM previewer
WHERE name = 'SCHOOLNET';