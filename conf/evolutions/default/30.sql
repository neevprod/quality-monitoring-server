# Add permission for QTI Response Generation and add missing auto_increments.

# --- !Ups
alter table `permission_settings_value` modify `permission_settings_value_id` bigint(20) not null auto_increment;

alter table `user_permissions` drop foreign key `fk_user_permissions_permission_settings`;
alter table `permission_settings_value` drop foreign key `fk_permission_settings_value_permission_settings`;
alter table `permission_settings` modify `permission_settings_id` bigint(20) not null auto_increment;
alter table `permission_settings_value` add constraint `fk_permission_settings_value_permission_settings`
  foreign key (`permission_settings_id`) references `permission_settings` (`permission_settings_id`);
alter table `user_permissions` add constraint `fk_user_permissions_permission_settings` foreign key
  (`permission_settings_id`) references `permission_settings` (`permission_settings_id`);

alter table `permission_settings_value` drop foreign key `fk_permission_settings_value_permission_option`;
alter table `permission_option` modify `permission_option_id` bigint(20) not null auto_increment;
alter table `permission_settings_value` add constraint
  `fk_permission_settings_value_permission_option` foreign key (`permission_option_id`) references
  `permission_option` (`permission_option_id`);

insert into `permission_option` (`name`, `application_module_id`, `create_access`, `read_access`, `update_access`,
  `delete_access`) values ('responses',
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'qtiRespGen'),
    0, 1, 0, 0);

# --- !Downs
delete from `permission_option` where `name` = 'responses' and `application_module_id` =
  (select m.`application_module_id` from `application_module` m where m.`application_module_name` = 'qtiRespGen');

alter table `permission_settings_value` drop foreign key `fk_permission_settings_value_permission_option`;
alter table `permission_option` modify `permission_option_id` bigint(20) not null;
alter table `permission_settings_value` add constraint
  `fk_permission_settings_value_permission_option` foreign key (`permission_option_id`) references
  `permission_option` (`permission_option_id`);

alter table `user_permissions` drop foreign key `fk_user_permissions_permission_settings`;
alter table `permission_settings_value` drop foreign key `fk_permission_settings_value_permission_settings`;
alter table `permission_settings` modify `permission_settings_id` bigint(20) not null;
alter table `permission_settings_value` add constraint `fk_permission_settings_value_permission_settings`
foreign key (`permission_settings_id`) references `permission_settings` (`permission_settings_id`);
alter table `user_permissions` add constraint `fk_user_permissions_permission_settings` foreign key
  (`permission_settings_id`) references `permission_settings` (`permission_settings_id`);

alter table `permission_settings_value` modify `permission_settings_value_id` bigint(20) not null;
