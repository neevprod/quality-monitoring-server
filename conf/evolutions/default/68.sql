# Add unique constraint to name field.

# --- !Ups
ALTER TABLE `environment` ADD CONSTRAINT `name` UNIQUE (`name`);

# --- !Downs
ALTER TABLE `environment` DROP INDEX `name`;