# Update scenario_class.enabled_for_tn8 and scenario_class.enabled_for_epen value.

# --- !Ups
UPDATE scenario_class SET enabled_for_tn8 = 1, enabled_for_epen=1 WHERE scenario_class = 'minToMax';

# --- !Downs
UPDATE scenario_class SET enabled_for_tn8 = 0, enabled_for_epen=0 WHERE scenario_class = 'minToMax';