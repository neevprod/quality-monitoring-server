# Change BE_PATH_PRE_EPEN and BE_PATH_POST_EPEN to BE_EPEN_PATH_VALIDATOR and BE_PRIMARY_PATH_VALIDATOR.

# --- !Ups
UPDATE `state`
SET `state_name`      = 'BE_EPEN_PATH_VALIDATOR',
  `state_description` = 'The Backend ePEN Path Validator component validates that the scores from ePEN have been loaded into the PA Next Backend and that all the states in the ePEN Item Complete Path have completed successfully.'
WHERE `state_name` = 'BE_PATH_PRE_EPEN';

UPDATE `state`
SET `state_name`      = 'BE_PRIMARY_PATH_VALIDATOR',
  `state_description` = 'The Backend Primary Path Validator component validates that the test attempt data from IRIS has been loaded to the PA Next Backend and that all the states in the Primary Path have completed successfully.'
WHERE `state_name` = 'BE_PATH_POST_EPEN';

ALTER TABLE `path_state`
  ADD COLUMN `path_state_order` INT(11);

UPDATE `path_state`
SET `path_state_order` = 1
WHERE `path_state_id` = (SELECT `path_state_id`
                         FROM (SELECT *
                               FROM `path_state`) ps
                           JOIN `path` p ON ps.`path_id` = p.`path_id`
                           JOIN `state` s ON ps.`state_id` = s.`state_id`
                         WHERE p.`path_name` = 'DefaultPath' AND s.`state_name` = 'TN8_API_DATA_LOADER');

UPDATE `path_state`
SET `path_state_order` = 2
WHERE `path_state_id` = (SELECT `path_state_id`
                         FROM (SELECT *
                               FROM `path_state`) ps
                           JOIN `path` p ON ps.`path_id` = p.`path_id`
                           JOIN `state` s ON ps.`state_id` = s.`state_id`
                         WHERE p.`path_name` = 'DefaultPath' AND s.`state_name` = 'IRIS_PROCESSING_VALIDATOR');

UPDATE `path_state`
SET `path_state_order` = 3
WHERE `path_state_id` = (SELECT `path_state_id`
                         FROM (SELECT *
                               FROM `path_state`) ps
                           JOIN `path` p ON ps.`path_id` = p.`path_id`
                           JOIN `state` s ON ps.`state_id` = s.`state_id`
                         WHERE p.`path_name` = 'DefaultPath' AND s.`state_name` = 'BE_PRIMARY_PATH_VALIDATOR');

UPDATE `path_state`
SET `path_state_order` = 4
WHERE `path_state_id` = (SELECT `path_state_id`
                         FROM (SELECT *
                               FROM `path_state`) ps
                           JOIN `path` p ON ps.`path_id` = p.`path_id`
                           JOIN `state` s ON ps.`state_id` = s.`state_id`
                         WHERE p.`path_name` = 'DefaultPath' AND s.`state_name` = 'EPEN2_AUTOMATION');

UPDATE `path_state`
SET `path_state_order` = 5
WHERE `path_state_id` = (SELECT `path_state_id`
                         FROM (SELECT *
                               FROM `path_state`) ps
                           JOIN `path` p ON ps.`path_id` = p.`path_id`
                           JOIN `state` s ON ps.`state_id` = s.`state_id`
                         WHERE p.`path_name` = 'DefaultPath' AND s.`state_name` = 'BE_EPEN_PATH_VALIDATOR');

UPDATE `path_state`
SET `path_state_order` = 6
WHERE `path_state_id` = (SELECT `path_state_id`
                         FROM (SELECT *
                               FROM `path_state`) ps
                           JOIN `path` p ON ps.`path_id` = p.`path_id`
                           JOIN `state` s ON ps.`state_id` = s.`state_id`
                         WHERE p.`path_name` = 'DefaultPath' AND s.`state_name` = 'STATS');

ALTER TABLE `path_state`
  MODIFY `path_state_order` INT(11) NOT NULL;

# --- !Downs
ALTER TABLE `path_state`
  DROP COLUMN `path_state_order`;

UPDATE `state`
SET `state_name`      = 'BE_PATH_PRE_EPEN',
  `state_description` = 'The Backend Path Processing Validator component validates that the test attempts created by IRIS have been loaded to the PA Next Back End and that they are in the expected states at various points throughout the data integrity validation job.'
WHERE `state_name` = 'BE_EPEN_PATH_VALIDATOR';

UPDATE `state`
SET `state_name` = 'BE_PATH_POST_EPEN',
  `state_description` = 'The Backend Path Processing Validator component validates that the test attempts created by IRIS have been loaded to the PA Next Back End and that they are in the expected states at various points throughout the data integrity validation job.'
WHERE `state_name` = 'BE_PRIMARY_PATH_VALIDATOR';
