# Extend permissions to work for multiple modules.

# --- !Ups

create table `permission_type` (
  `permission_type_id` int(11) not null auto_increment,
  `permission_type_name` varchar(50) not null,
  primary key (`permission_type_id`),
  unique key `un_permission_type_name` (`permission_type_name`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;
insert into `permission_type` (`permission_type_name`) values ('ModuleLevelPermission'), ('TenantLevelPermission'),
  ('EnvironmentLevelPermission');

create table `application_module` (
  `application_module_id` int(11) not null auto_increment,
  `application_module_name` varchar(50) not null,
  `permission_type_id` int(11) not null,
  primary key (`application_module_id`),
  unique key `un_application_module_name` (`application_module_name`),
  foreign key (`permission_type_id`) references `permission_type` (`permission_type_id`)
) /*! engine=InnoDB auto_increment=1 default charset=utf8 default collate utf8_unicode_ci */;
insert into `application_module` (`application_module_name`, `permission_type_id`) values
  ('qtiValidation', (select `permission_type_id` from `permission_type` where `permission_type_name` = 'TenantLevelPermission')),
  ('qtiRespGen', (select `permission_type_id` from `permission_type` where `permission_type_name` = 'ModuleLevelPermission'));

alter table `user_permissions` add column `application_module_id` int(11) after `user_id`;
alter table `user_permissions` add constraint `fk_user_permissions_application_module_id`
  foreign key (`application_module_id`) references `application_module`(`application_module_id`);
update `user_permissions` set `application_module_id` =
  (select `application_module_id` from `application_module` where `application_module_name` = 'qtiValidation');
alter table `user_permissions` modify column `application_module_id` int(11) not null;
alter table `user_permissions` modify column `previewer_tenant_id` bigint(20);
alter table `user_permissions` add column `environment_id` mediumint(8) after `previewer_tenant_id`;
alter table `user_permissions` add constraint `fk_user_permissions_environment_id`
  foreign key (`environment_id`) references `environment`(`environment_id`);
alter table `user_permissions` add column `is_wildcard` tinyint(1) not null default 0 after `application_module_id`;

alter table `tenant_permission_option` rename to `permission_option`;
alter table `tenant_permission_settings_value` drop foreign key
  `fk_tenant_permission_settings_value_tenant_permission_option`;
alter table `permission_option` change column `tenant_permission_option_id` `permission_option_id` bigint(20) not null;
alter table `tenant_permission_settings_value` change column `tenant_permission_option_id` `permission_option_id`
  bigint(20) not null;
alter table `tenant_permission_settings_value` add constraint
  `fk_permission_settings_value_permission_option` foreign key (`permission_option_id`) references
  `permission_option` (`permission_option_id`);
alter table `permission_option` add column `application_module_id` int(11) after `name`;
alter table `permission_option` add constraint `fk_permission_option_application_module_id`
  foreign key (`application_module_id`) references `application_module` (`application_module_id`);
update `permission_option` set `application_module_id` =
  (select `application_module_id` from `application_module` where `application_module_name` = 'qtiValidation');
alter table `permission_option` modify column `application_module_id` int(11) not null;

alter table `tenant_permission_settings` rename to `permission_settings`;
alter table `user_permissions` drop foreign key `fk_user_permissions_tenant_permission_settings`;
alter table `tenant_permission_settings_value` drop foreign key
  `fk_tenant_permission_settings_value_tenant_permission_settings`;
alter table `user_permissions` change column `tenant_permission_settings_id` `permission_settings_id`
  bigint(20) not null;
alter table `tenant_permission_settings_value` change column `tenant_permission_settings_id` `permission_settings_id`
bigint(20) not null;
alter table `permission_settings` change column `tenant_permission_settings_id` `permission_settings_id`
  bigint(20) not null;
alter table `tenant_permission_settings_value` add constraint `fk_permission_settings_value_permission_settings`
  foreign key (`permission_settings_id`) references `permission_settings` (`permission_settings_id`);
alter table `user_permissions` add constraint `fk_user_permissions_permission_settings` foreign key
  (`permission_settings_id`) references `permission_settings` (`permission_settings_id`);

alter table `permission_settings` add column `application_module_id` int(11) after `name`;
alter table `permission_settings` add constraint `fk_permission_settings_application_module_id`
  foreign key (`application_module_id`) references `application_module` (`application_module_id`);
update `permission_settings` set `application_module_id` =
  (select `application_module_id` from `application_module` where `application_module_name` = 'qtiValidation');
alter table `permission_settings` modify column `application_module_id` int(11) not null;

alter table `tenant_permission_settings_value` rename to `permission_settings_value`;
alter table `permission_settings_value` change column `tenant_permission_settings_value_id`
  `permission_settings_value_id` bigint(20) not null;

# --- !Downs

alter table `permission_settings_value` change column `permission_settings_value_id`
  `tenant_permission_settings_value_id` bigint(20) not null;
alter table `permission_settings_value` rename to `tenant_permission_settings_value`;

alter table `permission_settings` drop foreign key `fk_permission_settings_application_module_id`;
alter table `permission_settings` drop column `application_module_id`;

alter table `user_permissions` drop foreign key `fk_user_permissions_permission_settings`;
alter table `tenant_permission_settings_value` drop foreign key
`fk_permission_settings_value_permission_settings`;
alter table `permission_settings` change column `permission_settings_id` `tenant_permission_settings_id`
  bigint(20) not null;
alter table `user_permissions` change column `permission_settings_id` `tenant_permission_settings_id`
  bigint(20) not null;
alter table `tenant_permission_settings_value` change column `permission_settings_id` `tenant_permission_settings_id`
  bigint(20) not null;
alter table `tenant_permission_settings_value` add constraint
  `fk_tenant_permission_settings_value_tenant_permission_settings`
foreign key (`tenant_permission_settings_id`) references `permission_settings` (`tenant_permission_settings_id`);
alter table `user_permissions` add constraint `fk_user_permissions_tenant_permission_settings` foreign key
  (`tenant_permission_settings_id`) references `permission_settings` (`tenant_permission_settings_id`);
alter table `permission_settings` rename to `tenant_permission_settings`;

alter table `permission_option` drop foreign key `fk_permission_option_application_module_id`;
alter table `permission_option` drop column `application_module_id`;
alter table `tenant_permission_settings_value` drop foreign key
  `fk_permission_settings_value_permission_option`;
alter table `permission_option` change column `permission_option_id` `tenant_permission_option_id` bigint(20) not null;
alter table `tenant_permission_settings_value` change column `permission_option_id` `tenant_permission_option_id`
  bigint(20) not null;
alter table `tenant_permission_settings_value` add constraint
  `fk_tenant_permission_settings_value_tenant_permission_option` foreign key (`tenant_permission_option_id`) references
  `permission_option` (`tenant_permission_option_id`);
alter table `permission_option` rename to `tenant_permission_option`;

alter table `user_permissions` drop column `is_wildcard`;
alter table `user_permissions` drop foreign key `fk_user_permissions_environment_id`;
alter table `user_permissions` drop column `environment_id`;
alter table `user_permissions` modify column `previewer_tenant_id` bigint(20) not null;
alter table `user_permissions` drop foreign key `fk_user_permissions_application_module_id`;
alter table `user_permissions` drop column `application_module_id`;

drop table if exists `application_module`;

drop table if exists `permission_type`;
