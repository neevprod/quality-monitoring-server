# Add Other Option in External_Defect_Repository table.

# --- !Ups

INSERT INTO `external_defect_repository` (`name`) VALUES ('Other');
   
# --- !Downs

DELETE  FROM `external_defect_repository` where name='Other';
