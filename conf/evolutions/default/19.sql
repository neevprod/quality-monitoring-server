# Add testmap related columns and tables.

# --- !Ups

ALTER TABLE `scenario_name`
   RENAME TO `scenario_class`;

ALTER TABLE `scenario`
   DROP FOREIGN KEY `fk_scenario_scenario_name_scenario_name_id`;

ALTER TABLE `scenario_class`
   CHANGE COLUMN `scenario_name_id` `scenario_class_id`smallint(5) NOT NULL;

ALTER TABLE `scenario_class`
   CHANGE COLUMN `scenario_name` `scenario_class`varchar(50) NOT NULL;

CREATE TABLE `testmap_detail` (
  `testmap_detail_id` int NOT NULL AUTO_INCREMENT,
  `testmap_id` int NOT NULL,
  `testmap_name` varchar(255) NOT NULL,
  `testmap_version` int NOT NULL,
  `testmap_publish_format` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`testmap_detail_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

ALTER TABLE `scenario`
   ADD COLUMN`scope_tree_path`varchar(2000) NOT NULL;

ALTER TABLE `scenario`
   ADD COLUMN`test_code`varchar(100) NOT NULL;

ALTER TABLE `scenario`
   ADD COLUMN`form_code`varchar(255) NOT NULL;

ALTER TABLE `scenario`
   ADD COLUMN`testmap_detail_id`int;

ALTER TABLE `scenario`
   ADD COLUMN`scenario_name`varchar(255) NOT NULL;

ALTER TABLE `scenario`
   CHANGE COLUMN `scenario_name_id` `scenario_class_id`smallint(5) NOT NULL;

ALTER TABLE `scenario`
   ADD CONSTRAINT `fk_scenario_class_id` FOREIGN KEY(`scenario_class_id`)
       REFERENCES `scenario_class`(`scenario_class_id`);

ALTER TABLE `scenario`
   ADD CONSTRAINT `fk_testmap_detail_id` FOREIGN KEY(`testmap_detail_id`)
       REFERENCES `testmap_detail`(`testmap_detail_id`);

CREATE TABLE `div_job_exec_type` (
  `div_job_exec_type_id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `div_job_exec_type` varchar(50) NOT NULL,
  PRIMARY KEY (`div_job_exec_type_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

INSERT INTO `div_job_exec_type`(`div_job_exec_type_id`, `div_job_exec_type`)
VALUES (1, 'PANEXT');

ALTER TABLE `div_job_exec`
   ADD COLUMN`div_job_exec_type_id`tinyint(3) NOT NULL;
   
ALTER TABLE `student_scenario`
   DROP COLUMN `form_code`;
   
ALTER TABLE `student_scenario`
   DROP COLUMN `form_name`;
   
ALTER TABLE `student_scenario`
   DROP COLUMN `test_code`;
   
ALTER TABLE `student_scenario`
   DROP COLUMN `test_name`;

UPDATE `div_job_exec`
   SET `div_job_exec_type_id` = 1;

ALTER TABLE `div_job_exec`
   ADD CONSTRAINT `fk_div_job_exec_type_id` FOREIGN KEY
          (`div_job_exec_type_id`)
          REFERENCES `div_job_exec_type`(`div_job_exec_type_id`);
		  
		  
# --- !Downs

ALTER TABLE `div_job_exec`
   DROP FOREIGN KEY `fk_div_job_exec_type_id`;

ALTER TABLE `div_job_exec`
   DROP COLUMN `div_job_exec_type_id`;

DROP TABLE `div_job_exec_type`;

ALTER TABLE `scenario`
   DROP FOREIGN KEY `fk_testmap_detail_id`;

ALTER TABLE `scenario`
   DROP FOREIGN KEY `fk_scenario_class_id`;

ALTER TABLE `scenario`
   CHANGE COLUMN `scenario_class_id` `scenario_name_id`smallint(5) NOT NULL;

ALTER TABLE `scenario`
   DROP COLUMN `scenario_name`;

ALTER TABLE `scenario`
   DROP COLUMN `testmap_detail_id`;

ALTER TABLE `scenario`
   DROP COLUMN `form_code`;

ALTER TABLE `scenario`
   DROP COLUMN `test_code`;

ALTER TABLE `scenario`
   DROP COLUMN `scope_tree_path`;

DROP TABLE `testmap_detail`;

ALTER TABLE `scenario_class`
   CHANGE COLUMN `scenario_class_id` `scenario_name_id`smallint(5) NOT NULL;

ALTER TABLE `scenario_class`
   CHANGE COLUMN `scenario_class` `scenario_name`varchar(50) NOT NULL;

ALTER TABLE `scenario_class`
   RENAME TO `scenario_name`;
   
ALTER TABLE `student_scenario`
   ADD COLUMN `test_code` varchar(255) NOT NULL;
   
ALTER TABLE `student_scenario`
   ADD COLUMN `form_code` varchar(255) NOT NULL;
   
ALTER TABLE `student_scenario`
   ADD COLUMN `test_name` varchar(200) DEFAULT NULL;
   
ALTER TABLE `student_scenario`
   ADD COLUMN `form_name` varchar(100) DEFAULT NULL;

ALTER TABLE `scenario`
   ADD CONSTRAINT `fk_scenario_scenario_name_scenario_name_id` FOREIGN KEY
          (`scenario_name_id`)
          REFERENCES `scenario_name`(`scenario_name_id`);	  