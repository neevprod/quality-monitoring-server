# Add column changes from the Play 1 version of the application

# --- !Ups

alter table `hist_tenant` add column `item_exceptions` int(11) not null default '-1';

alter table `hist_test_case` add column `tc_id_from_previewer` int(11) not null;
alter table `hist_test_case` add column `mods` longblob not null;
alter table `hist_test_case` add column `failed_fingerprint_expected` longblob default null;
alter table `hist_test_case` add column `failed_fingerprint_actual` longblob default null;

alter table `hist_exec` add column `tenant_level_exec` tinyint(1) not null default false;

alter table `test_case_uploads` modify column `previewer_id` int(11) not null;
alter table `test_case_uploads` change column `previewer_tenant_id` `tenant_id` bigint(20) not null;
alter table `test_case_uploads` alter column `mismatched_test_cases_count` set default '0';

# --- !Downs

alter table `hist_tenant` drop column `item_exceptions`;

alter table `hist_test_case` drop column `tc_id_from_previewer`;
alter table `hist_test_case` drop column `mods`;
alter table `hist_test_case` drop column `failed_fingerprint_expected`;
alter table `hist_test_case` drop column `failed_fingerprint_actual`;

alter table `hist_exec` drop column `tenant_level_exec`;

alter table `test_case_uploads` modify column `previewer_id` int(11) default null;
alter table `test_case_uploads` change column `tenant_id` `previewer_tenant_id` bigint(20) not null;
alter table `test_case_uploads` alter column `mismatched_test_cases_count` drop default;

