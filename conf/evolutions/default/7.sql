# Add new feature to the tenant_permission_option table.

# --- !Ups

insert into `tenant_permission_option` (`name`, `create_access`, `read_access`, `update_access`, `delete_access`) values
  ('qtiScvTenantJobSubscriptions', 1, 1, 0, 1);

# --- !Downs

delete from `tenant_permission_option` where `name` = 'qtiScvTenantJobSubscriptions';
