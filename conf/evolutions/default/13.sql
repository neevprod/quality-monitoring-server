# Added test_session table and added columns to student_scenario_table to store data retrieved by StudentTestSessionExtractor

# --- !Ups
CREATE TABLE `test_session` (
  `test_session_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `testnav_application_url` varchar(500) NOT NULL COMMENT 'URL of the TestNav site displayed to user (testing tickets)',
  `testnav_webservice_url` varchar(500) NOT NULL COMMENT 'Base URL used to communicate with TestNav.',
  `testnav_client_secret` varchar(200) COMMENT 'Used for TestNav web service authentication.',
  `testnav_client_identifier` varchar(200) COMMENT 'Used for TestNav web service authentication.',
  `testnav_version` varchar(50) NOT NULL,
  `testnav_customer_code` varchar(200) NOT NULL COMMENT 'Key information sent to TestNav web service calls.',
  `scope_code` varchar(50) NOT NULL COMMENT 'Unique identifier (within parent scope) which can be used by applications as a key to retrieve a specific row. This value is used as the export_type.',
  `session_seal_codes` varchar(255) COMMENT 'A code that the user must enter before entering a sealed section of a test.',
  `session_name` varchar(50) COMMENT 'Name of this session (unique by session and org)',
  `session_scheduled_start_date` datetime COMMENT 'Scheduled start date. Set by user when session created',
  `schedule_start_date` date DEFAULT NULL COMMENT 'The date on which student testing can begin for this delivery schedule.',
  `schedule_start_time` time DEFAULT NULL COMMENT 'The time of day when student testing can begin. Applies to all days within the delivery schedule.',
  `schedule_end_date` date DEFAULT NULL COMMENT 'The date on which student testing ends for this delivery schedule.',
  `schedule_end_time` time DEFAULT NULL COMMENT 'The time of day when student testing ends. Applies to all days within the delivery schedule.',
  `session_start_date` datetime DEFAULT NULL COMMENT 'Date that session is started',
  `session_stop_date` datetime DEFAULT NULL COMMENT 'Date that session is stopped',
  `session_available_monday` tinyint(4) DEFAULT '0',
  `session_available_tuesday` tinyint(4) DEFAULT '0',
  `session_available_wednesday` tinyint(4) DEFAULT '0',
  `session_available_thursday` tinyint(4) DEFAULT '0',
  `session_available_friday` tinyint(4) DEFAULT '0',
  `session_available_saturday` tinyint(4) DEFAULT '0',
  `session_available_sunday` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`test_session_id`)
) /*! ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 */;

ALTER TABLE `student_scenario` ADD COLUMN `student_code` varchar(50) COMMENT 'Unique identifier (within a scope) which can be used by applications as a key to retrieve a specific row.';
ALTER TABLE `student_scenario` ADD COLUMN `last_name` varchar(100) COMMENT 'Last name';
ALTER TABLE `student_scenario` ADD COLUMN `first_name` varchar(100) COMMENT 'First name';
ALTER TABLE `student_scenario` ADD COLUMN `student_group_name` varchar(100) COMMENT 'Unique name (within an organization) which is displayed to the user.  This descriptive name can change and should not be be used as a key.';
ALTER TABLE `student_scenario` ADD COLUMN `student_test_status` varchar(50) COMMENT 'assign - not yet started. testing - testing in progress. attempt - test complete and valid. battery - battery test status needs to be derived from children';
ALTER TABLE `student_scenario` ADD COLUMN `session_assign_status` varchar(50);
ALTER TABLE `student_scenario` ADD COLUMN `test_name` varchar(200) COMMENT 'Unique name (within a scope) which is displayed to the user.  This descriptive name can change and should not be be used as a key.';
ALTER TABLE `student_scenario` ADD COLUMN `form_name` varchar(100) COMMENT 'Name of this form.';
ALTER TABLE `student_scenario` ADD COLUMN `export_type` varchar(20) COMMENT 'Export Type';
ALTER TABLE `student_scenario` ADD COLUMN `test_session_id` bigint(20) NOT NULL;
DELETE FROM `student_scenario_failure`;
DELETE FROM `student_scenario`;
ALTER TABLE `student_scenario` ADD CONSTRAINT `fk_studentscenario_testsessionid` FOREIGN KEY (`test_session_id`) REFERENCES `test_session` (`test_session_id`);



# --- !Downs
ALTER TABLE `student_scenario` DROP FOREIGN KEY `fk_studentscenario_testsessionid`;

ALTER TABLE `student_scenario` DROP COLUMN `student_code`;
ALTER TABLE `student_scenario` DROP COLUMN `last_name`;
ALTER TABLE `student_scenario` DROP COLUMN `first_name`;
ALTER TABLE `student_scenario` DROP COLUMN `student_group_name`;
ALTER TABLE `student_scenario` DROP COLUMN `student_test_status`;
ALTER TABLE `student_scenario` DROP COLUMN `session_assign_status`;
ALTER TABLE `student_scenario` DROP COLUMN `test_name`;
ALTER TABLE `student_scenario` DROP COLUMN `form_name`;
ALTER TABLE `student_scenario` DROP COLUMN `export_type`
ALTER TABLE `student_scenario` DROP COLUMN `test_session_id`;

DROP TABLE `test_session`;