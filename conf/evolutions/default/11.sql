# Fix issue with URL of first previewer and add ABBI previewer.

# --- !Ups

alter table `previewer` change column `protocol` `api_protocol` varchar(10) not null;
alter table `previewer` add column `protocol` varchar(10) not null after `api_url`;
update `previewer` set `protocol` = 'https://';

update `previewer` set `url` = 'dev-tn8previewer.pearsondev.com' where `name` = 'DEV';

insert into `previewer`(`name`, `url`, `api_url`, `protocol`, `api_protocol`, `supports_https`)
values('ABBI', 'abbi-previewer.elasticbeanstalk.com', 'abbi-previewer.elasticbeanstalk.com', 'http://', 'http://', 0);

# --- !Downs

delete from `previewer_tenant` where `previewer_id` = (select `previewer_id` from `previewer` where `name` = 'ABBI');
delete from `previewer` where `name` = 'ABBI';

update `previewer` set `url` = 'tn8previewer-dev.elasticbeanstalk.com' where `name` = 'DEV';

alter table `previewer` drop column `protocol`;
alter table `previewer` change column `api_protocol` `protocol` varchar(10) not null;

