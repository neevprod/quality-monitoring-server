# Modify columns for Active Directory authentication

# --- !Ups
alter table `human_user` add column `network_id` varchar(20) not null;
alter table `human_user` drop column `password`;

# --- !Downs
alter table `human_user` drop column `network_id`;
alter table `human_user` add column `password` varchar(255) not null;