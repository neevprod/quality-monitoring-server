# Add new divJobExec and studentScenario statuses.

# --- !Ups
INSERT INTO div_job_exec_status(status)
VALUES ('Completed (OE Excluded)'),
       ('Completed (OE-OP Excluded)'),
       ('Completed (OE-FT Excluded)');

INSERT INTO student_scenario_status(status)
VALUES ('Completed (OE Excluded)'),
       ('Completed (OE-OP Excluded)'),
       ('Completed (OE-FT Excluded)');