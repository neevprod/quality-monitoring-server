# Add state descriptions to the state table.

# --- !Ups
UPDATE state
   SET state_description =
          'The TestNav 8 API Data Loader component loads student response data into TestNav 8.'
 WHERE state_name = 'TN8_API_DATA_LOADER';

UPDATE state
   SET state_description =
          'The IRIS Path Validator component validates that a uuid has been successfully pulled from TestNav 8 and processed by IRIS.'
 WHERE state_name = 'IRIS_PROCESSING_VALIDATOR';

UPDATE state
   SET state_description =
          'The Backend Path Processing Validator component validates that the test attempts created by IRIS have been loaded to the PA Next Back End and that they are in the expected states at various points throughout the data integrity validation job.'
 WHERE state_name = 'BE_PATH_PRE_EPEN';

UPDATE state
   SET state_description =
          'The ePEN 2 Bulk Scorer component scores the responses that are routed to ePEN 2 for human scoring.  Once scoring has completed, it triggers the accept and export scores jobs.'
 WHERE state_name = 'EPEN2_AUTOMATION ';

UPDATE state
   SET state_description =
          'The Backend Path Processing Validator component validates that the test attempts created by IRIS have been loaded to the PA Next Back End and that they are in the expected states at various points throughout the data integrity validation job.'
 WHERE state_name = 'BE_PATH_POST_EPEN';

UPDATE state
   SET state_description =
          'The STATS component calculates the expected raw and group scores, fetches the actual scores from the data warehouse, and compares expected to actual.'
 WHERE state_name = 'STATS';
 
# --- !Downs
UPDATE state
   SET state_description = 'state_description'
 WHERE state_name in('TN8_API_DATA_LOADER', 'IRIS_PROCESSING_VALIDATOR', 'BE_PATH_PRE_EPEN', 'EPEN2_AUTOMATION', 'BE_PATH_POST_EPEN', 'STATS');