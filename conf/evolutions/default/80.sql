# Add indexes on testmap_detail table to improve performance.

# --- !Ups
CREATE INDEX testmap_id_idx ON testmap_detail (testmap_id);
CREATE INDEX testmap_name_idx ON testmap_detail (testmap_name);
CREATE INDEX testmap_version_idx ON testmap_detail (testmap_version);

# --- !Downs
DROP INDEX testmap_id_idx ON testmap_detail;
DROP INDEX testmap_name_idx ON testmap_detail;
DROP INDEX testmap_version_idx ON testmap_detail;