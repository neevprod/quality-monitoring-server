# Make unused API and DB connections nullable.

# --- !Ups
alter table `environment` modify `ice_bridge_oracle_connection_id` mediumint(8);
alter table `environment` modify `fe_api_connection_id` mediumint(8);
alter table `environment` modify `be_api_connection_id` mediumint(8);
alter table `environment` modify `iris_api_connection_id` mediumint(8);
alter table `environment` modify `epen2_api_connection_id` mediumint(8);
alter table `environment` modify `data_warehouse_api_connection_id` mediumint(8);
alter table `environment` modify `ice_bridge_api_connection_id` mediumint(8);

# --- !Downs
alter table `environment` modify `ice_bridge_oracle_connection_id` mediumint(8) not null;
alter table `environment` modify `fe_api_connection_id` mediumint(8) not null;
alter table `environment` modify `be_api_connection_id` mediumint(8) not null;
alter table `environment` modify `iris_api_connection_id` mediumint(8) not null;
alter table `environment` modify `epen2_api_connection_id` mediumint(8) not null;
alter table `environment` modify `data_warehouse_api_connection_id` mediumint(8) not null;
alter table `environment` modify `ice_bridge_api_connection_id` mediumint(8) not null;
