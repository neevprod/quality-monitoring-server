# Remove old_response_generator_enabled application setting.

# --- !Ups
DELETE FROM application_setting
WHERE application_setting_name = 'old_response_generator_enabled';

# --- !Downs
INSERT INTO `application_setting` (`application_setting_name`, `application_setting_value`) VALUES
  ('old_response_generator_enabled', '0');