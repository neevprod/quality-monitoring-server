# This is the main configuration file for the application.
# ~~~~~

application.name = "Quality Monitoring"
app.version = "1.8.18-SNAPSHOT"

# Register Guice modules for dependency injection
play.modules.enabled += "modules.DefaultModule"
play.server.provider = play.core.server.NettyServerProvider

# Secret key
# ~~~~~
# The secret key is used to secure cryptographics functions.
#
# See http://www.playframework.com/documentation/latest/ApplicationSecret for more details.
play.http.secret.key = ${?QM_HTTP_SECRET_KEY}

# Security settings
play.filters.headers.contentSecurityPolicy = "unsafe-inline"

# The application languages
play.i18n.langs = ["en"]

# Session configuration
session.cookie.name = "session"
session.timeout = 2 hours
play.http.session.httpOnly = false

# Database configuration

db.default.driver = com.mysql.cj.jdbc.Driver

db.default.host = ${?QM_DB_HOST}
db.default.port = ${?QM_DB_PORT}
db.default.schema = ${?QM_DB_SCHEMA}
db.default.username = ${?QM_DB_USER}
db.default.password = ${?QM_DB_PASS}

db.default.url = "jdbc:mysql://"${db.default.host}":"${db.default.port}"/"${db.default.schema}"?characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull"

play.crypto.secret = ${?QM_CRYPTO_SECRET}

http.port = ${?QM_HTTP_PORT_SETTING}
https.port = ${?QM_HTTPS_PORT}

play.server.https.keyStore.path = ${?QM_HTTPS_KEYSTORE_PATH}
play.server.https.keyStore.type = ${?QM_HTTPS_KEYSTORE_TYPE}
play.server.https.keyStore.password = ${?QM_HTTPS_KEYSTORE_PASS}
play.server.https.keyStore.algorithm = ${?QM_HTTPS_KEYSTORE_ALGO}

pidfile.path = ${?QM_PIDFILE_PATH}

db.default.jndiName = DefaultDS

# Connection pool configuration
db.default.hikaricp.poolName = defaultPool
db.default.hikaricp.registerMbeans = true
db.default.hikaricp.connectionTimeout = 10 seconds
db.default.hikaricp.maximumPoolSize = 20

# JPA configuration
jpa.default = defaultPersistenceUnit

# Evolutions
play.evolutions.db.default.enabled = true
play.evolutions.db.default.autoApply = ${?QM_DB_EVOLUTIONS_AUTOAPPLY}

# DataSourceManager connection pool configuration
application.connectionPoolManager.mysql.connectionTimeout = 30 seconds
application.connectionPoolManager.mysql.maximumPoolSize = 10
application.connectionPoolManager.mysql.minimumIdle = 1
application.connectionPoolManager.mysql.idleTimeout = 2 minutes

application.connectionPoolManager.mongo.connectionTimeout = 30 seconds
application.connectionPoolManager.mongo.maximumPoolSize = 10
application.connectionPoolManager.mongo.minimumPoolSize = 1
application.connectionPoolManager.mongo.idleTimeout = 2 minutes

# URL
application.baseUrl = ${?QM_APP_BASEURL}

# Issue report recipients
issue.report.recipients = ["gert.selis@pearson.com", "nand.joshi@pearson.com"]

# Email configuration
play.mailer.host = relay.mx.pearson.com
play.mailer.mock = ${?QM_MAILER_MOCK}

application.email.fromAddress = ${?QM_EMAIL_FROM_ADDRESS}

# Set custom Akka dispatcher to make MDC logging work properly.
akka {
  actor {
    default-dispatcher = {
      type = "global.MDCPropagatingDispatcherConfigurator"
      fork-join-executor {
        parallelism-min = 455 # dataIntegrityValidation.maxConcurrent (30) * dataIntegrityValidation.maxConcurrentStudents (15) + number of CPU cores (4) +  housekeeping (1)
        parallelism-max = 455 # dataIntegrityValidation.maxConcurrent (30) * dataIntegrityValidation.maxConcurrentStudents (15) + number of CPU cores (4) +  housekeeping (1)
      }
    }
  }
}

# Concurrency settings for jobs
# ~~~~~
# If the number of running instances of a job is at the max concurrent number, the next started job will be queued.
application.jobs {
  updateTenants.maxConcurrent = 1
  updateTenants.numberOfChildWorkers = 4

  nightlyJob.maxConcurrent = 1

  qtiScoringValidation.maxConcurrent = 5

  divJobSetup.maxConcurrent = 15
  testCaseUpload.maxConcurrent = 5

  dataIntegrityValidation.maxConcurrent = 30
  dataIntegrityValidation.maxConcurrentStudents = 15
}

# The time zone the development team is in
# ~~~~~
# This is here so that the nightly job start time can be configured in the developers' time zone. It will be converted
# to the server time zone when the application runs.
application.developerTimeZone = "America/Chicago"

# Timeout settings
application.actorAskTimeout = 5 seconds
application.studentReservationTimeOut = 30 minutes

# Valid API versions
application.apiVersions {
  core {
    min = 1
    max = 1
  }
  qtiValidation {
    min = 1
    max = 1
  }
  qtiRespGen {
    min = 1
    max = 1
  }
  systemValidation {
    min = 1
    max = 1
  }
  userManagement {
    min = 1
    max = 1
  }
  gridAutomation {
    min = 1
    max = 1
  }
}
#Response Generator Version Reader URL
application.respgenVersions.url = "http://nexus2.pearsondev.com/nexus/service/local/lucene/search?q=resp-gen-cli&collapseresults=false"
application.respgenDownload.url = "http://nexus2.pearsondev.com/nexus/service/local/artifact/maven/redirect?r=pearson-repo&g=com.pearson.itautomation&a=resp-gen-cli"

# Set filters
play.http.filters = global.Filters

# Set error handler
play.http.errorHandler = global.ErrorHandler

# Bindings to Previewer API
application.previewerApiToken = Automation2
application.previewerApiSecret = "yLd++nLsZCPFTa+qRDDV0X/Uq7wHOV/0"

# QTI Response Generator settings
application.responseGeneratorTimeout = 45
application.responseGenPath = ${?QM_RESPGEN_PATH}

# Epen User Prefix
application.epenUserPrefix = ${?QM_EPEN_USER_PREFIX}

# LDAP Authentication
application.ldapUser = "PAUTOMATIONDEV"
application.ldapPassword = "QzlXXgQT4dpv5aEN6U8GVG2ylnUa1fqG"

# S3 Authentication
application.s3.kt.clientRegion = "us-east-1"
application.s3.kt.bucket = "product-validation-math-scoring-manifest"