# Introduction

The Quality Monitoring Web App is built with __Java 1.8__ using __Play Framework 2.5__ (for the server-side code) and
__AngularJS 1.5__ (for the client-side code). The server-side code is responsible for providing a REST API to
communicate with the database and perform business logic. The client-side code is responsible for providing a user
interface to interact with, and it calls the server-side API in order to retrieve and store data. The same REST API can
also be called by external applications.

# Setting up your development environment

## Prerequisites

- Install Java 1.8.
- Install [Play Framework 2.5](https://playframework.com/documentation/2.5.x/Installing).
- Install [Node.js](https://nodejs.org).
- Install [Bower](http://bower.io) by running `npm install -g bower`.
- Install [MySQL Server 5.6](https://dev.mysql.com/downloads/mysql) if you want to run a local MySQL database (this is
recommended, especially if you will be making database changes).
  - In the C:\ProgramData\MySQL\MySQL Server 5.6\my.ini file, change the following values and restart the MySQL service when completed:
     - max_allowed_packet: change this value to 64M
     - innodb_log_file_size: change this value to 640M

## Starting the application

To get the application running, both the server-side (Play) application and the client-side (AngularJS) application need
to be started up.

### Starting the Play application

1. Make sure the application.conf file (in the conf directory) is pointing to the database you want to use with the
correct credentials. The relevant settings start with "db.default". If you are running a local database, make sure that
the schema your DB settings refer to has been created.
2. Copy developer.conf.template to developer.conf in the same location. 
Make sure that the application.epenUserPrefix value in the developer.conf file is updated with a value that is unique among all developers who work on the project.
Note that the maximum length for this value is 4 characters. A suggested value is the first 2 letters of your last name, followed by the first 2 letters of your first name.
3. To run the Play application in dev mode, just open a command prompt at the root of the project directory and type
`activator run`. This will start the application at [http://localhost:9000](http://localhost:9000).
4. If your database is not up to date, you will need to apply scripts to it. To do this, just go to
[http://localhost:9000](http://localhost:9000), and it will show you a notification if database scripts need to be
applied. Just click the button to apply the scripts. This is done with
[Play Framework Evolutions](https://playframework.com/documentation/2.5.x/Evolutions).

*Note: For some reason, after
successfully applying a database evolution, Play will report an error with the cache. When this happens, you'll need to
restart the Play application, and everything should work fine after that.*

More information on using the Play console can be found
[here](https://playframework.com/documentation/2.5.x/PlayConsole). Play can also be run from your IDE. For instructions
on setting up Play in your IDE and debugging, please
[visit this link](https://playframework.com/documentation/2.5.x/IDE).

### Starting the AngularJS application

We are using [Gulp](http://gulpjs.com) as the build tool for our client-side application. To run the application, follow
these steps:

1. Open a command prompt at webapp directory inside the project directory.
2. Run `npm install` to download the development dependencies. This will install the packages specified in
`package.json` and must be run every time this file is updated.
3. Run `bower install` to download the application dependencies. This will download the packages specified by
`bower.json` and must be run every time this file is updated.
4. To run the application in development mode, run `npm run serve`, which will call Gulp's "serve" task. The application
will be hosted at [http://localhost:9001](http://localhost:9001) by [BrowserSync](http://www.browsersync.io). The
BrowserSync settings can be accessed at [http://localhost:9002](http://localhost:9002).

Our Gulp build is based off of generator-gulp-angular (with some modifications). For more information, see
[the generator's documentation](https://github.com/Swiip/generator-gulp-angular).

## Running unit tests

- To run the Play unit tests, run `activator test` from the root of the project directory (these can also be run from
your IDE).
- To run the AngularJS unit tests, run `npm run test` from the `webapp` directory. To run the tests in continuous mode
(tests are rerun whenever changes are made), run `npm run test:auto`.

## Adding a user to the database

Before you can log into the application, you'll need to add a user to the database (note that before you can do this, 
you'll need to run the Play application and apply the database evolution scripts so that the tables are created). To do 
so, follow these steps:

1. Add an entry to the `user` table with `isAdmin` set to `1` and `is_api_user` set to `0`.
2. Add an entry to the `human_user` table that links to the entry you just created in the `user` table. You'll need to 
include your email and Pearson network ID.

After adding the entry, you'll be able to log in to the application with your regular Pearson network ID and password.

# Application architecture

Yet to be documented.

# Code overview and guidelines

## Play application

### SBT build

The Play application is built using SBT. There are a few files that will be commonly used to modify the build:
`build.sbt`, `project/plugins.sbt`, and `project/build.properties`. `build.sbt` has most of the build settings in it,
including the library dependencies. `plugins.sbt` needs to be modified when changing the Play version that the
application uses. `build.properties` needs to be modified when changing the SBT version (this should only need changed
when changing the Play version, and the Play documentation should specify which SBT version is needed).

There is an [overview of the build system](https://playframework.com/documentation/2.5.x/BuildOverview) in the Play
documentation where you can learn more if needed.

### Dependency injection

Play is integrated with the [Google Guice](https://github.com/google/guice) dependency injection framework, which we
are using in the application. A dependency injection framework allows a class' dependencies to be injected into it,
more easily allowing loose coupling and making unit testing much easier. For more information, see [Play's
documentation on dependency injection](https://playframework.com/documentation/2.5.x/JavaDependencyInjection) and
[Guice's documentation](https://github.com/google/guice/wiki/Motivation).

The Guice module for the application is located at `app/modules/DefaultModule.java`. There is also an alternate Guice
module used for the unit tests, which is located at `test/modules/TestModule.java`.

### Logging

#### Overview

Play uses [Logback](http://logback.qos.ch) as its logging engine, and Play's documentation on logging can be found
[here](https://playframework.com/documentation/2.5.x/JavaLogging). The application's logging configuration file is
found at `conf/logback.xml`. This configuration file can be used to set things like log format, rolling policy, and log 
levels. More documentation on log configuration can be found in
[Play's documentation](https://playframework.com/documentation/2.5.x/SettingsLogger) and in the
[Logback manual](http://logback.qos.ch/manual/index.html).

#### Logging standards

Logs should be human readable, searchable, and provide plenty of context. To meet these goals, we are logging
information in key-value pairs and making sure to provide helpful contextual information. The main message of the log
should have a key called "event", and other key-value pairs can be provided to give additional context. The log should
also be set at the appropriate level. Here is an example of a good log statement:

```java
Logger.info("event=\"Retrieving list of items for tenant.\", previewerId=\"{}\", tenantId=\"{}\"", previewerId, tenantId);
```

When the log statement above is run, it will produce an output similar to the one below:

```
2015-08-28 10:37:22.439 INFO [application-akka.actor.default-dispatcher-5][application] event="Retrieving list of items for tenant.", previewerId="1", tenantId="3", request="GET /api/qtiValidation/v1/previewers/1/tenants/3/items", sessionId="P9lfupZKh008teJFAZbPKA", userId="5"
```

Notice that the request, session ID, and user ID are automatically added to the log. The application is set up to use
[Logback's Mapped Diagnostic Context](http://logback.qos.ch/manual/mdc.html) to add these. The Mapped Diagnostic
Context (MDC) is a map that stores information per thread and automatically logs it. Information is added to the MDC
when a request starts and removed from it when it ends. This happens in `app/global/LoggingFilter.java`. For the MDC
to work with Play, we needed to also add a custom Akka dispatcher
(`app/global/MDCPropagatingDispatcherConfigurator.scala`), because Play can use multiple threads for a single request.
The custom Akka dispatcher propagates the MDC information to a new thread when the threads are switched during a
request. For more information on the custom Akka dispatcher, see
[this blog post](http://yanns.github.io/blog/2014/05/04/slf4j-mapped-diagnostic-context-mdc-with-play-framework/).

*Note: The custom Akka dispatcher was written in Scala because of limited Java documentation for doing that. All other 
code in the Play application is written in Java.*

For more information on writing good logs, please read
[Splunk's logging best practices](http://dev.splunk.com/view/logging-best-practices/SP-CAAADP6). This has a lot of
great tips and insight.

### Exception handling

In the Play application, any uncaught exception will be caught and handled by `global/ErrorHandler.java`. This class
will log the stack trace of the exception, along with a randomly generated ID that identifies the occurrence of the
exception. After the exception is logged, a 500 Internal Server Error response will be returned. This response also
contains the same generated ID that points to the exception in the log, so the user can file an error report if
they desire. Play documentation on error handling can be found
[here](https://playframework.com/documentation/2.5.x/JavaErrorHandling).

Because any uncaught exceptions will be caught and dealt with by `ErrorHandler`, try/catch blocks should only be used
to catch exceptions that we know how to handle (in other words, expected exceptions). Catching an exception that we
don't know how to deal with would just cause extra work, because we would have to duplicate the code that is already in
`ErrorHandler`.

### Database

#### Database version control

We manage changes to the database schema using Play Evolutions. This allows you to reliably make and apply incremental
changes to the database schema, and to make sure all the scripts are backed up in version control. Evolutions also
allows us to use an in-memory database for unit testing and ensures that all the latest updates are applied to it. For
an overview of Evolutions and how to use it, please see the
[documentation](https://playframework.com/documentation/2.5.x/Evolutions).

*Note: The H2 in-memory database that is used in the unit tests is set to run in its MySQL mode, but sometimes the
syntax it accepts doesn't perfectly match that of MySQL. In this case where you run into this issue, you'll have to
find a way to write the database script in a way that both databases (MySQL and H2) understand it. Ideally, we would
use the exact same database for the unit tests as we do elsewhere, but we haven't found a way to run MySQL in an
in-memory mode. However, the ability to automatically generate a database to run unit tests against outweighs this
issue. Though it's possible there could be some discrepancies by using different databases, our setup allows us to, at
minimum, ensure the correctness of our queries' logic through unit testing.*

#### Accessing the database

We are using JPA (the Java Persistence API) to persist and access data to/from the database from the application. JPA
is a Java interface specification for object relational mapping. JPA allows you to define entities that map tables to
objects, and it allows querying of those entities using JPQL (the Java Persistence Query Language). JPA is just an
interface that is implemented by persistence frameworks. We are using [Hibernate](http://hibernate.org/orm) as the JPA
provider in our application. To learn more about how to use JPA, there is a
[wiki book](https://en.wikibooks.org/wiki/Java_Persistence) that goes through it in detail.

All of the JPA models can be found within the `models` directory. All of our JPQL queries are written in service
classes, which are in the `services` directory. All queries in the application should be written in JPQL, and all
queries should be placed within a service class. Following these guidelines will ensure the queries are reusable and
keep the application more maintainable.

### Security

#### Authentication

For user authentication, we are using a standard called [JSON Web Tokens (JWT)](http://jwt.io). A JWT is a base-64
encoded string that consists of a header, a payload, and a signature. Once decoded, a payload consists of a JSON string
that contains whatever information you want to put in it. The signature is created by hashing the header plus the
payload using a secret key. This way, if someone tries to change the information in a JWT, it becomes invalid since the
signature no longer matches. For a more in-depth overview of JSON Web Tokens,
[here is a good article](https://scotch.io/tutorials/the-anatomy-of-a-json-web-token).

In our web application, we are using JWTs to store session data. When a user logs in, a JWT is generated containing the
session data in its payload, and it is signed using the application secret found in `application.conf`. Then for each
request a user makes that needs authentication, they include the JWT in the `X-Auth-Token` header. All requests that
need authentication will be checked by `global/Authenticator.java`. It will ensure that there is a valid JWT in the
header (checking its signature with the application secret) and that the JWT has not expired. If the JWT is valid, the
authenticator will store its payload in the request context so that subsequent code has access to the session data. If
the JWT is not valid, a 401 Unauthorized response will be returned instead of fulfilling the request.

For all requests that call a method in a controller that extends `SecureController`, authentication will be run before
fulfilling the request. Therefore, any controller that needs authentication needs to extend `SecureController`. Any
controller that should not have authentication needs to instead extend `BaseController`. `SecureController` itself
extends `BaseController`, so authenticated controllers also have access to the protected methods in `BaseController`.

There are a couple major benefits that we get from the way we are authenticating users. The first is that because all
session information is securely stored in the JWT that is included in each authenticated request, the server does not
have to keep track of sessions. This keeps our application stateless and, therefore, more RESTful. Because the server
does not have to keep track of sessions, our application is more scalable (any server running the application could
service a user's request, since it doesn't have to know about the user's session). The other major benefit of our
authentication method comes from the fact that we are validating the JWT in a custom header rather than in a cookie.
This protects our application from CSRF attacks, as described in
[this article](http://www.jamesward.com/2013/05/13/securing-single-page-apps-and-rest-services).

#### User permissions

The application has a user permissions implementation that is used to determine whether a user has permission to use
a feature that they are attempting to use. Permission settings consist of the features a user has access to, along with
what kinds of access they have to that feature (create, read, update, and delete). Each application module has a 
specific permission type: tenant-level, environment-level, or module-level. This specifies the granularity at which 
roles take effect in that module. For instance, in a module with tenant-level permissions, the user can have a 
separate role for each tenant. Users can also be admin users; in that case, they have access to all features of the 
application.

A user's permission settings are stored in their session data in the JWT token they receive when logging in (see the
"Authentication" section above for more details). This information can be easily obtained from a controller that
extends `SecureController` with the `sessionData()` method. It is also easy to check whether a user has permission for
a specific feature by using the methods in `SecureController` that start with "enforce", such as 
`SecureController.enforceTenantLevelPermission()`. These methods will throw a `QaApplicationPermissionException` if the 
user does not have the permission; otherwise, they will do nothing.

To keep the application secure, every method in a controller that extends `SecureController` should call
one of the permission enforcement methods (mentioned above) to make sure the user has permission to perform the action. 
If not, it should return `BaseController.accessDeniedResult()` (this will return a 403 FORBIDDEN response). New 
permission options should be added to the `permission_option` table in the database.

In addition, in cases where a CRUD operation is to be performed on something that a request claims belongs to a
specific tenant, we need to check and make sure that it actually belongs to that tenant. For instance, imagine a user
makes a request to read report 23 from tenant 2 in previewer 3. We check the permissions of the user, and they do have
the read report permission for tenant 2 in previewer 3. However, what if report 23 is not actually in that tenant, but
in a different tenant? Because of this, our service method that queries the database needs to also take the tenant ID
and previewer ID as parameters, and it needs to filter by the tenant to ensure it doesn't return anything that does not
belong to that tenant.

### REST API

#### REST API endpoint guidelines

[This article](http://blog.mwaysolutions.com/2014/06/05/10-best-practices-for-better-restful-api) is a good place to
start for learning about how we are writing our REST API, as we are following many of the principles listed in it. All
of our REST API endpoints are defined in `/conf/routes`.

Our web application will consist of several different sub-applications or modules. Each REST API endpoint will start
with "/api/", followed by the module name, followed by the API version (such as "v1"). This will be followed by a noun 
representing the resource involved in the API call, or more than one if a relation is involved. A couple examples would 
be "/api/qtiValidation/v1/previewers" and "/api/qtiValidation/v1/previewers/2/tenants". The available API versions for 
each module can be configured in `application.conf` under the key `application.apiVersions`.

In addition to the URI for a REST API, an HTTP verb is required as well. The options are GET, POST, PUT, and DELETE.
GET is used for reading, POST for creating, PUT for updating, and DELETE for deleting. In rare cases, POST may need to
be used for retrieving, because of some of the limitations of GET.

#### Response body guidelines

The body of all responses should be returned in a JSON object format following specific guidelines. All responses will
have a `success` field with a value of `true` or `false`, depending on whether the request was successful. If the
request was successful and there is data to return, there will also be a `data` field, which contains an object. The
data should be returned in fields inside the `data` object rather than being directly under the `data` field (this
ensures consistency and prevents having to redefine the API if you start with one piece of data and add more later).
If the request is not successful, there will be an `errors` field that contains an array of one or more objects, each
containing an error message. All of the possible response formats can be easily constructed with the protected methods
in `BaseController` (such as `successResult()` and `failResult()`). Whenever writing a return statement in a controller
method, you should return the result of one of these methods, rather than constructing the JSON yourself. This will
enforce consistency and maintainability (plus it's less work for you anyway).

Another thing to keep in mind when returning responses is to use the appropriate HTTP status code. The methods defined
in `BaseController` have these built in where there is only one option that makes sense. When more than one option
might make sense, you'll need to pass in the right status code (there are constants for these defined in Play's
`Http.Status` class).

### Jobs

The jobs in our application are written using [Akka](http://akka.io/), which is a framework that makes it easier to
build code that runs concurrently without running into problems of shared mutable state. It is also what Play uses for
running scheduled jobs. An Akka system is composed of different actors, each of which contains state and behavior, and
these actors communicate with each other in a hierarchical manner by passing messages. There is a tutorial with a good
example [here](http://doc.akka.io/docs/akka/2.0.2/intro/getting-started-first-java.html) that helps to explain how Akka
works. You can also refer to [their documenation](http://akka.io/docs) to learn more, as well as [Play's documentation
on Akka](https://playframework.com/documentation/2.5.x/JavaAkka) to learn how to use it with Play.

All of the code for our jobs is contained under the `app/jobs` directory. One very important class here is
`JobRunnerActor`. This is the top-level actor in our application, which is responsible for starting jobs and keeping
track of which jobs are currently running. For each job that is started, a string identifier is assigned based on the
job type and the input. When a request is made to start a job, but a job with the same string identifier is already
running, no extra action is taken. This happens to save system resources, since it would be pointless to run the same
job for the same data more than once at the same time.

Anytime a job needs to be started from outside the Akka system (in other words, from a controller or from the
scheduler), a message will be sent to `JobRunnerActor` to start the job. Messages can also be sent to `JobRunnerActor`
to check the status of a specific job (using the string identifier generated for the job). Another important
jobs-related class is `JobScheduler`. This class runs at application startup and schedules any jobs that need to be run
at specific times or intervals.

When a job finishes, it sends a `JobFinishMessage` to `JobRunnerActor`. This notifies it that the job is done running,
and it can optionally include JSON that contains the job results (this is constructed with
`JobProtocol.getJobResultJson()`). If this JSON is present, it will be saved to the `job_execution` table. The
information in the `job_execution` table is returned when someone checks the status of a job.

### Unit tests

The unit tests for the Play application are defined in the `test` directory. The tests use the
[JUnit framework](http://junit.org), along with [Mockito](http://mockito.org) for mocking. The Play documentation for
writing unit tests can be found [here](https://playframework.com/documentation/2.5.x/JavaTest) (note that there are
more pages about writing unit tests in Play linked to from this page).

Note that for some unit tests, you will need to have a running Play application. One example of this is testing
controller methods, where you'll need to send a fake request to the method. In order to get a Play application running
in the context of the unit test, it needs to be build with `GuiceApplicationBuilder`. However, this code is implemented
in `test/global/TestWithApplication.java`, so in test classes where you need a running Play application, you can just
extend `TestWithApplication`. When the Play application is started for testing `application-test.conf` is used instead
of `application.conf`. Most of the settings end up being the same since `application-test.conf` imports
`application.conf`, but some of the settings are overridden. The most important things that are overridden are the
Guice module used and the database used. In the unit tests, an in-memory H2 database is used (in MySQL mode).

For the unit tests that test the service classes (the ones that query the database), we are using a library called
[DbSetup](http://dbsetup.ninja-squad.com). This library provides an easy way of populating the database with data for
the unit tests. Check out [DbSetup's user guide](http://dbsetup.ninja-squad.com/user-guide.html) for more details on
how it works. One thing mentioned on that page is to create a common list of tables to clear before each test. We have
defined this in `test/services/CommonDbOperations.java`. As for table structure of the database, this is set up
automatically each time the unit tests are run using our Play Evolutions scripts (see the Database section above for
more information).

Because we are using dependency injection in our application, we can mock out dependencies of a class when writing a
unit test. We have been following the approach of mocking out functionality that does not belong to the class we are
testing, but making sure that the functionality we are mocking out is tested elsewhere (unless it's coming from an
external dependency, in which case it's not our application's responsibility). Another thing to keep in mind is that
you shouldn't mock out so much stuff that you are now testing whether the code works in a certain way, rather than
testing that it provides a certain output for certain inputs.

## AngularJS application

For the AngularJS application, there is a good [Angular Style Guide](https://github.com/johnpapa/angular-styleguide)
that we have been using. There may be a few parts that we aren't following, but we are following the vast majority of
it. It would be a good idea to get familiar with the style guide before doing development work on the AngularJS
application.

Other topics regarding the AngularJS application apart from the style guide are discussed below.

### Gulp build

Yet to be documented. In the meantime, check out [generator-gulp-angular](https://github.com/Swiip/generator-gulp-angular)
(the tool we used to generate our Gulp build).

### Modules

Yet to be documented. In the meantime, check out
[the AngularJS module documentation](https://code.angularjs.org/1.3.19/docs/guide/module).

### Session management

Yet to be documented. In the meantime, check out `webapp/src/app/core/session/session.service.js`.

### User permissions

Yet to be documented. In the meantime, check out `webapp/src/app/core/permissions/permissions.service.js` (provides
permission evaluation to controllers) and `webapp/src/app/core/main/main.controller.js` (provides permission evaluation
to views).

### HTTP interceptor

Yet to be documented. In the meantime, check out `webapp/src/app/core/http.interceptor.js`.

### Tables

Yet to be documented. In the meantime, check out [Angular Smart Table](http://lorenzofox3.github.io/smart-table-website/)
(the table implementation we are using).

### Unit tests

Yet to be documented. In the meantime, check out
[the AngularJS unit testing documentation](https://code.angularjs.org/1.3.19/docs/guide/unit-testing) (we are using
Jasmine).

# User interface guidelines

Yet to be documented.

# Deployment information

The application is deployed to AWS, and we have two environments: a dev environment at
[https://quality-monitoring-dev.pearsondev.com](https://quality-monitoring-dev.pearsondev.com) and a prod environment
at [https://quality-monitoring.pearsondev.com](https://quality-monitoring.pearsondev.com). Both environments have their
own application server, but they both share the same database server with different schemas. The dev schema is called
"quality_monitoring", and the prod schema is called "quality_monitoring_prod".

## Environment configuration

The environment configurations for dev and prod are in the files `application-dev.conf` and `application-prod.conf`.
`application-dev.conf` includes `application.conf`, so the dev configurations contain everything in the default
configurations, plus anything extra that is added in `application-dev.conf`. In the same way, `application-prod.conf`
includes `application-dev.conf`, so the prod configurations are the same as the dev configurations, plus anything extra
that is added in `application-prod.conf`.

You may notice that the dev and prod configuration files contain placeholders for the application secret and the
database connection. This is because this information is only stored on the server, for security purposes. These
properties are passed from the server to the application when it is being started.

## Server file structure

On the dev and prod servers, the release artifacts are placed in the directory `/opt/deployments/testnav8/releases`.
There is also a symbolic link to the current release at `/opt/deployments/testnav8/qti-scv/current`. This can be used
to see which is the currently deployed version. The log files will be generated in the log directory of the current
release's directory. However, our logs are also read into Splunk, which is a much easier way to search the logs.

## Viewing and searching logs in Splunk

We have Spunk set up to store our application logs. Splunk makes it much easier to view and search the logs. You can
log in to Splunk at
[https://splunk-aws.pearsondev.com/en-US/account/login](https://splunk-aws.pearsondev.com/en-US/account/login)
using your regular Pearson username and password.

Once you are logged in, you can search our dev logs by including `host="qtiscv_dev*"` in your search, and you can
search prod by including `host="qtiscv_prd*"`. If you only want to include the application logs in your search
(excluding other log sources on that server), you can also include `source="*application.log"`.

Splunk uses the key-value pairs in our logs (such as `userId="21"`) to extract fields and information. These can be
used in searches and other functions of Splunk.

## Application versioning rules

See: [Reference: Quality Monitoring Application Versioning Rules](https://confluence.assessment.pearson.com/display/AAD/Reference%3A+Quality+Monitoring+Application+Versioning+Rules)

## Deploying a new version of the application

See: [Guide: Deploying a new version of the Quality Monitoring application](https://confluence.assessment.pearson.com/display/AAD/Guide%3A+Deploying+a+new+version+of+the+Quality+Monitoring+application)

## Restarting the application

See: [Guide: Restarting the Quality Monitoring application](https://confluence.assessment.pearson.com/display/AAD/Guide%3A+Restarting+the+Quality+Monitoring+application)

## Deploying a new version of the QTI Response Generator web service

In order to generate responses, the application makes calls to a separate QTI Response Generator web service. The
reason this exists as a separate web service is because the current response generator library is coded in .NET. There
are two local Windows servers that host the response generator web service: a dev server and a prod server. In order to
deploy a new version of the QTI Response Generator onto the servers, follow these steps:

1. Obtain the two DLLs for the new version of QTI Response Generator. These will be called QtiRespGen.dll and ScoreEngine.dll.
2. Deploy the new version of response generator to the dev server:
    1. Use a Remote Desktop Connection (`mstsc.exe`) to log on to the DEV server (`icdwdaiqxweb01.ic.ncs.com`) using
    your DCSUTIL account (e.g. `DCSUTIL\QCAUTO`). Note: in the "*Options > Local Resources*" tab of the Remote Desktop
    Client, make sure that "*Clipboard*" is checked. Click "*Connect*" to establish the connection.
    2. On your local machine, select both *QtiRespGen.dll* and *ScoreEngine.dll* and press CTRL-C on your keyboard to
    copy these files.
    3. On the remote server machine, navigate to
    `D:\aiqautomationdevelopment.ic.ncs.com\Services\Global\QTIResponseGenerator\bin\` and press CTRL-V on your
    keyboard to paste the copied files. Say "*Yes*" when prompted if you would like to overwrite the existing files.
3. Test the dev response generator web service to make sure the deployment was successful. This can be done by running
the web app locally (`application.conf` is set to connect to the dev version of the response generator web service).
You can test response generation in the web app by telling it to create test cases for an item, or you can test it
through the API by making the `POST /api/qtiRespGen/<apiVersion>/responses` request and passing in item XML.
4. If everything looks good with the dev deployment, you can move on to the prod deployment:
    1. Use a Remote Desktop Connection (`mstsc.exe`) to log on to the PROD server (`icdwtaiqxweb01.ic.ncs.com`) using
    your DCSUTIL account (e.g. `DCSUTIL\QCAUTO`). Note: in the "*Options > Local Resources*" tab of the Remote Desktop
    Client, make sure that "*Clipboard*" is checked. Click "*Connect*" to establish the connection.
    2. On your local machine, select both *QtiRespGen.dll* and *ScoreEngine.dll* and press CTRL-C on your keyboard to
    copy these files.
    3. On the remote server machine, navigate to
    `D:\aiqautomation.ic.ncs.com\Services\Global\QTIResponseGenerator\bin\` and press CTRL-V on your keyboard to paste
    the copied files. Say "*Yes*" when prompted if you would like to overwrite the existing files.
5. Test the prod response generator web service to make sure the deployment was successful. This can be done the same
way as you tested the dev version, but by instead using the dev or prod environment of the web app
(`application-dev.conf` and `application-prod.conf` are set to connect to the prod version of the response generator
web service).

Note that the `maxReceivedMessageSize` and `maxStringContentLength` values in `D:\...\Services\Global\QTIResponseGenerator\Web.config` were changed from `65536` to `2147483647` to prevent "*HTTP 413 - Request entity too large*" error messages when the request size exceeded 64KB.