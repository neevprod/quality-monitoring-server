name := """Quality Monitoring"""

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayNettyServer).disablePlugins(PlayAkkaHttpServer)

scalaVersion := "2.11.11"

javacOptions += "-deprecation"

PlayKeys.externalizeResources := false
PlayKeys.devSettings += "play.server.provider" -> "play.core.server.NettyServerProvider"
PlayKeys.fileWatchService := play.dev.filewatch.FileWatchService.sbt(2000)

resolvers += "Pearson Repository" at "http://nexus2.pearsondev.com/nexus/content/repositories/pearson-repo"
resolvers += "Legacy Pearson Repository" at "http://nexus2.pearsondev.com/nexus/content/repositories/legacy-pearson-repo"
resolvers += "Pearson 3rd Party Repository" at "http://nexus2.pearsondev.com/nexus/content/repositories/thirdparty/"
resolvers += "Pearson 3rd Party Legacy Repository" at "http://nexus2.pearsondev.com/nexus/content/repositories/legacy-pearson-thirdparty/"

libraryDependencies ++= Seq(
  evolutions,
  "org.hibernate" % "hibernate-core" % "5.3.2.Final",
  "com.fasterxml.jackson.datatype" % "jackson-datatype-hibernate5" % "2.7.3",
  javaJpa,
  "mysql" % "mysql-connector-java" % "8.0.13",
  filters,
  ehcache,
  cacheApi,
  javaWs,
  guice,
  "org.jsr107.ri" % "cache-annotations-ri-guice" % "1.0.0",
  "com.typesafe.play" %% "play-mailer" % "6.0.1",
  "com.typesafe.play" %% "play-mailer-guice" % "6.0.1",
  "com.auth0" % "java-jwt" % "2.1.0",
  "org.jasypt" % "jasypt" % "1.9.2",
  "xalan" % "serializer" % "2.7.2",
  "com.pearson.itautomation" % "oldthrashbarg" % "1.0.0",
  "com.pearson.itautomation.tn8.previewer.api" % "TN8-PREVIEWER-APIS" % "1.0.9",
  "com.pearson.itautomation.qtiscv" % "QTI-SCV" % "1.0.51" exclude("com.pearson.itautomation", "pearson-file-formats"),
  "com.pearson.itautomation" % "iris-validation" % "0.0.37",
  "com.pearson.itautomation.panextstudentsessions" % "pa-next-student-sessions" % "0.0.35",
  "com.pearson.itautomation" % "pa-next-be-path-validation" % "0.0.23",
  "com.pearson.itautomation" % "tn8-api-data-loader" % "0.0.34",
  "com.pearson.itautomation" % "Stats" % "0.1.61" exclude("com.pearson.itautomation", "pearson-file-formats"),
  "com.pearson.itautomation" % "pearson-file-formats" % "0.0.160",
  "com.pearson.itautomation" % "TN8_TestScenarioCreator" % "0.0.32" exclude("com.pearson.itautomation", "pearson-file-formats"),
  "com.pearson.itautomation" % "epen-2-scoring-scenario-creator" % "0.0.26" exclude("com.pearson.itautomation", "pearson-file-formats"),
  "epen2bulkscoring" % "epen2bulkscoring" % "0.46" exclude("org.wildfly", "wildfly-client-all"),

  // version of ojalgo inherited from resp-gen doesn't work with sbt so using 35.0.1 instead
  "org.ojalgo" % "ojalgo" % "35.0.1",

  "com.jcraft" % "jsch" % "0.1.54",

  "com.pearson.odm" % "odm-grid-automation" % "1.0.25",
  "com.pearson.itautomation" % "qti-response-generator" % "0.0.62",
  "org.mockito" % "mockito-core" % "1.10.19" % "test",
  "info.solidsoft.mockito" % "mockito-java8" % "0.3.0" % "test",
  "org.powermock" % "powermock-module-junit4" % "1.6.2" % "test",
  "org.powermock" % "powermock-api-mockito" % "1.6.2" % "test" exclude("org.mockito", "mockito-all"),
  "org.skyscreamer" % "jsonassert" % "1.2.3" % "test",
  "com.ninja-squad" % "DbSetup" % "1.5.0" % "test",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.13" % "test",
  "com.amazonaws" % "aws-java-sdk-s3" % "1.11.419",
  "com.amazonaws" % "aws-java-sdk" % "1.11.435",
  "com.jayway.jsonpath" % "json-path" % "2.2.0",
  "org.apache.oodt" % "oodt-commons" % "1.0" exclude("xml-apis", "xml-apis"),
  "com.typesafe.play" %% "play-json" % "2.6.0",
  "com.h2database" % "h2" % "1.4.193" % Test,
  "org.apache.commons" % "commons-text" % "1.6"
).map(_.exclude("org.slf4j", "log4j-over-slf4j")
  .exclude("org.apache.logging.log4j", "log4j-slf4j-impl")
  .exclude("org.slf4j", "slf4j-log4j12"))

import com.typesafe.config._

val conf = ConfigFactory.parseFile(new File("conf/application.conf"))
version := conf.getString("app.version")