# ---------------------------
# Setup Stage
# ---------------------------
FROM amazonlinux:2.0.20190508 as setup

# Set versions of dependencies to use.
ENV \
    JAVA_VERSION="1.8.0_192.b12" \
    SCALA_VERSION="2.11.11" \
    SBT_VERSION="0.13.15" \
    WGET_VERSION="1.14" \
    TAR_VERSION="1.26" \
    UNZIP_VERSION="6.0" \
    MAN_DB_VERSION="2.6.3" \
    JAVA_HOME="/usr/lib/jvm/java-1.8.0-amazon-corretto.x86_64"
  
ENV PATH $PATH:/usr/lib/jvm/java-1.8.0-amazon-corretto.x86_64/jre/bin:/usr/lib/jvm/java-1.8.0-amazon-corretto.x86_64/bin:/opt/sbt/bin

# Install core dependencies as ROOT.
USER root

RUN \
    amazon-linux-extras enable corretto8 && \
    yum install -y java-1.8.0-amazon-corretto-devel-${JAVA_VERSION} \
    wget-${WGET_VERSION} \
    tar-${TAR_VERSION} \
    unzip-${UNZIP_VERSION} \
    man-db-${MAN_DB_VERSION}

RUN \
    wget -O /tmp/scala.rpm http://downloads.lightbend.com/scala/${SCALA_VERSION}/scala-${SCALA_VERSION}.rpm && \
    yum localinstall -y /tmp/scala.rpm && \
    wget -O /tmp/sbt.tgz https://piccolo.link/sbt-${SBT_VERSION}.tgz && \
    tar -xzf /tmp/sbt.tgz -C /opt && \
    yum clean all && \
    rm -rf /tmp/*

# Create the qma user, group and home directory.
RUN adduser --create-home --user-group qma

# Change ownership of qma user home directory.
WORKDIR /qma
RUN chown -R qma:qma /qma

# Switch to the qma user.
USER qma

# Create and set application working directory
RUN mkdir -p /qma/app
WORKDIR /qma/app

# ---------------------------
# Develop Stage
# ---------------------------
FROM setup as develop

# Run the application in debug mode
CMD ["sbt","-jvm-debug", "9999", "run"]

# ---------------------------
# Build Stage
# ---------------------------
FROM setup as build
COPY --chown=qma:qma . /qma/app

# Test the application and build the dist package
#RUN sbt test dist
RUN sbt dist

# ---------------------------
# Prod Stage
# ---------------------------
FROM build as prod
COPY --from=build /qma/app/target/universal/*.zip .
RUN unzip *.zip && \
  rm *.zip && \
  mv quality-monitoring* quality-monitoring

EXPOSE 9000

CMD ["./quality-monitoring/bin/quality-monitoring","-Dconfig.file=conf/application.conf"]