package controllers.usermanagement;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.typesafe.config.Config;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import models.*;
import models.qtivalidation.Previewer;
import models.qtivalidation.PreviewerTenant;
import models.systemvalidation.Environment;
import play.Logger;
import play.cache.AsyncCacheApi;
import play.db.jpa.Transactional;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.PermissionSettingsValueService;
import services.qtivalidation.PreviewerService;
import services.qtivalidation.PreviewerTenantService;
import services.systemvalidation.EnvironmentService;
import services.usermanagement.*;
import util.ActiveDirectoryAuthenticator;
import util.EncryptorDecryptor;
import util.SessionData;

import javax.inject.Inject;
import javax.inject.Provider;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Nand Joshi
 */
public class UserController extends SecureController {
    private static final String USER_PERMISSION = "user";
    private static final String ROLE_PERMISSION = "role";
    private final String ldapUser;
    private final String ldapPassword;
    private final HumanUserService humanUserService;
    private final PermissionSettingsService permissionSettingsService;
    private final EnvironmentService environmentService;
    private final PreviewerService previewerService;
    private final PreviewerTenantService tenantService;
    private final UserPermissionsService userPermissionsService;
    private final ApplicationModulesService applicationModulesService;
    private final PermissionSettingsValueService permissionSettingsValueService;
    private final PermissionOptionService permissionOptionService;
    private final AsyncCacheApi cache;
    private final ActiveDirectoryAuthenticator adAuth;

    @Inject
    public UserController(SessionData.Factory sessionDataFactory, HumanUserService humanUserService,
                          final PermissionSettingsService permissionSettingsService, final EnvironmentService environmentService,
                          final PreviewerService previewerService, final PreviewerTenantService tenantService,
                          final UserPermissionsService userPermissionsService,
                          final ApplicationModulesService applicationModulesService,
                          final PermissionSettingsValueService permissionSettingsValueService,
                          final PermissionOptionService permissionOptionService, final Config config, final AsyncCacheApi cache,
                          final Provider<ActiveDirectoryAuthenticator> adAuthProvider) {
        super(sessionDataFactory);
        this.humanUserService = humanUserService;
        this.permissionSettingsService = permissionSettingsService;
        this.environmentService = environmentService;
        this.previewerService = previewerService;
        this.tenantService = tenantService;
        this.userPermissionsService = userPermissionsService;
        this.applicationModulesService = applicationModulesService;
        this.permissionSettingsValueService = permissionSettingsValueService;
        this.permissionOptionService = permissionOptionService;
        this.ldapUser = config.getString("application.ldapUser");
        this.ldapPassword = EncryptorDecryptor.getDecryptedConfigurationString(config, "application.ldapPassword");
        this.cache = cache;
        this.adAuth = adAuthProvider.get();
    }

    /**
     * Provides the REST API to get all available human users.
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of users
     */
    @Transactional(readOnly = true)
    public Result getUsers(final String apiVersion) {
        Logger.info("event=\"Retrieving all human users.\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(humanUserService.findAll(), "users");
    }

    /**
     * Provides the REST API to get human user detail for the given <code>humanUserId</code>.
     *
     * @param apiVersion  The version of the API to use
     * @param humanUserId The human user id
     * @return A Result containing the user for given <code>humanUserId</code>
     */
    @Transactional(readOnly = true)
    public Result getHumanUser(final String apiVersion, final Integer humanUserId) {
        Logger.info("event=\"Retrieving human user by human user ID.\", human user ID=\"{}\"", humanUserId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
        if (humanUser.isPresent()) {
            return successResult(humanUser.get(), "humanUser");
        } else {
            Logger.error("event=\"User does not exist in database.\", user ID=\"{}\"", humanUserId);
            return failResult(NOT_FOUND, "User does not exist in database for human user id = " + humanUserId);
        }

    }

    /**
     * Provides the REST API to get user permissions for the given <code>humanUserId</code>.
     *
     * @param apiVersion  The version of the API to use
     * @param humanUserId The human user id
     * @return A Result containing the user permissions for given <code>humanUserId</code>
     */
    @Transactional(readOnly = true)
    public Result getUserPermissions(final String apiVersion, final Long humanUserId) {
        Logger.info("event=\"Retrieving user permissions.\", user ID\"=\"{}\"", humanUserId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
        if (humanUser.isPresent()) {
            return successResult(userPermissionsService.findByUserId(humanUser.get().getUserId()), "userPermissions");
        } else {
            Logger.error("event=\"User does not exist in database.\", user ID=\"{}\"", humanUserId);
            return failResult(NOT_FOUND, "User does not exist in database for environmentId = " + humanUserId);
        }
    }

    /**
     * Provides the REST API to get user permissions for the given
     * <code>humanUserId</code>,<code>applicationModuleId</code>,<code>permissionSettingsId</code>,<code>previewerId</code>.
     *
     * @param apiVersion           The version of the API to use
     * @param humanUserId          The human user id
     * @param applicationModuleId  The application module id
     * @param permissionSettingsId The role id
     * @param previewerId          The previewer id
     * @return A Result containing the user permissions.
     */
    @Transactional(readOnly = true)
    public Result getUserPermissionByTenant(final String apiVersion, final Long humanUserId,
                                            final Integer applicationModuleId, final Long permissionSettingsId, final Integer previewerId) {
        Logger.info("event=\"Retrieving user permission.\", human user ID\"=\"{}\"", humanUserId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
        if (humanUser.isPresent()) {
            Optional<UserPermissions> userPermission = userPermissionsService.find(humanUser.get().getUserId(),
                    applicationModuleId, permissionSettingsId, previewerId);
            if (userPermission.isPresent()) {
                return successResult(userPermission.get(), "userPermission");
            }
            return successResult();
        } else {
            Logger.error("event=\"User does not exist in database.\", user ID=\"{}\"", humanUserId);
            return failResult(NOT_FOUND, "User does not exist in database for environmentId = " + humanUserId);
        }

    }

    /**
     * Provides the REST API to get user permissions for the given
     * <code>humanUserId</code>,<code>applicationModuleId</code>,<code>permissionSettingsId</code>.
     *
     * @param apiVersion           The version of the API to use
     * @param humanUserId          The human user id
     * @param applicationModuleId  The application module id
     * @param permissionSettingsId The role id
     * @return A Result containing the user permissions.
     */
    @Transactional(readOnly = true)
    public Result getUserPermission(final String apiVersion, final Long humanUserId, final Integer applicationModuleId,
                                    final Long permissionSettingsId) {
        Logger.info("event=\"Retrieving user permission.\", user ID\"=\"{}\"", humanUserId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
        if (humanUser.isPresent()) {
            Optional<UserPermissions> userPermission = userPermissionsService.find(humanUser.get().getUserId(),
                    applicationModuleId, permissionSettingsId);
            if (userPermission.isPresent()) {
                return successResult(userPermission.get(), "userPermission");
            }
            return successResult();
        } else {
            Logger.error("event=\"User does not exist in database.\", user ID=\"{}\"", humanUserId);
            return failResult(NOT_FOUND, "User does not exist in database for environmentId = " + humanUserId);
        }

    }

    /**
     * Provides the REST API to get all available application modules which has at least one role is assigned.
     *
     * @param apiVersion The version of the API to use
     * @return The List of application modules which has at least one role is assigned
     */
    @Transactional(readOnly = true)
    public Result getAllApplicationModules(final String apiVersion) {
        Logger.info("event=\"Retrieving all application modules.\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(applicationModulesService.findAll(), "applicationModules");
    }

    /**
     * Provides the REST API to get all available application modules which has at least one role is assigned.
     *
     * @param apiVersion The version of the API to use
     * @return The List of application modules which has at least one role is assigned
     */
    @Transactional(readOnly = true)
    public Result getApplicationModules(final String apiVersion) {
        Logger.info("event=\"Retrieving all application modules.\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(applicationModulesService.findApplicationModules(), "applicationModules");
    }

    /**
     * Provides the REST API to get all available permission settings (roles)
     *
     * @param apiVersion The version of the API to use.
     * @return The List of permission settings (Roles)
     */
    @Transactional(readOnly = true)
    public Result getRoles(final String apiVersion) {
        Logger.info("event=\"Retrieving all permission settings.\"");

        try {
            enforceModuleLevelPermission(ROLE_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(permissionSettingsService.findAll(), "roles");
    }

    /**
     * Provides the REST API to get all available permission settings for given <code>applicationModuleId</code>
     *
     * @param apiVersion          The version of the API to use.
     * @param applicationModuleId The application module ID
     * @return The List of permission settings (Roles)
     */
    @Transactional(readOnly = true)
    public Result getPermissionSettings(final String apiVersion, final Integer applicationModuleId, final Long humanUserId) {
        Logger.info(
                "event=\"Retrieving all permission settings.\", \"applicationModuleId=\"" + applicationModuleId + "\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<ApplicationModule> applicationModule = applicationModulesService.findById(applicationModuleId);
        final List<PermissionSettings> modulelevelPermissionSettings = new ArrayList<>();
        if (applicationModule.isPresent() && "ModuleLevelPermission"
                .equalsIgnoreCase(applicationModule.get().getPermissionType().getPermissionTypeName())) {

            Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
            if (!humanUser.isPresent()) {
                return failResult(NOT_FOUND, "Can not find user with human user id " + humanUserId);
            }

            modulelevelPermissionSettings
                    .addAll(userPermissionsService.findByApplicationModuleIdAndUserId
                            (applicationModuleId, humanUser.get().getUserId()).stream().map
                            (UserPermissions::getPermissionSettings).collect(Collectors.toList()));
        }

        List<PermissionSettings> permissionSettings = permissionSettingsService
                .findByApplicationModuleId(applicationModuleId).stream()
                .filter(ps -> !modulelevelPermissionSettings.contains(ps)).collect
                        (Collectors.toList());
        return successResult(permissionSettings, "permissionSettings");
    }

    /**
     * Provides the REST API to get environments that are not assigned to the user.
     *
     * @param apiVersion          The version of the API to use
     * @param humanUserId         The human user id
     * @param permissionSettingId The role id
     * @return The List of {@link Environment}
     */
    @Transactional(readOnly = true)
    public Result getEnvironments(final String apiVersion, final Long humanUserId, final Long permissionSettingId) {
        Logger.info(
                "event=\"Retrieving environments that are not assigned to the user.\", humanUserId=\"{}\", permissionSettingId=\"{}\"",
                humanUserId, permissionSettingId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
        if (!humanUser.isPresent()) {
            return failResult(NOT_FOUND, "Can not find user with human user id " + humanUserId);
        }
        List<Integer> assignedEnvironmentIds = userPermissionsService
                .findEnvironmentsByUserId(humanUser.get().getUserId(), permissionSettingId);
        List<Environment> unassignedEnvironemtns = environmentService.findAll().stream()
                .filter(env -> !assignedEnvironmentIds.contains(env.getEnvironmentId())).collect(Collectors.toList());
        return successResult(unassignedEnvironemtns, "environments");
    }

    /**
     * Provides the REST API to get all previewers.
     *
     * @param apiVersion The version of the API to use.
     * @return The List of permission settings (Roles)
     */
    @Transactional(readOnly = true)
    public Result getPreviewers(final String apiVersion) {
        Logger.info("event=\"Retrieving all previewers.\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(previewerService.findAll(), "previewers");
    }

    /**
     * Provides the REST API to get all previewers.
     *
     * @param apiVersion  The version of the API to use
     * @param humanUserId The user id
     * @param roleId      The role id
     * @param previewerId The previewer id
     * @return The List of permission settings (Roles)
     */
    @Transactional(readOnly = true)
    public Result getPreviewerTenants(final String apiVersion, final Long humanUserId, final Long roleId,
                                      final Integer previewerId) {
        Logger.info("event=\"Retrieving tenants .\", userId=\"{}\", previewerId=\"{}\", roleId=\"{}\"", humanUserId,
                previewerId, roleId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
        if (!humanUser.isPresent()) {
            return failResult(NOT_FOUND, "Can not find user with human user id " + humanUserId);
        }

        List<PreviewerTenant> totalActiveTenants = tenantService.findAllActive(previewerId);
        List<Long> assignedTenants = userPermissionsService
                .findTenantByUserIdAndPermissionSettingId(humanUser.get().getUserId(), roleId);
        List<PreviewerTenant> unassignedTenants = totalActiveTenants.stream()
                .filter(tenant -> !assignedTenants.contains(tenant.getTenantId())).collect(Collectors.toList());
        return successResult(unassignedTenants, "tenants");
    }

    /**
     * Provides the REST API to add a user.
     *
     * @param apiVersion The version of the API to use.
     * @return A Result indicating whether the user was added successfully.
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result addUser(String apiVersion) {

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.CREATE, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode body = request().body().asJson();
        if (body.get("email") == null || body.get("email").asText().isEmpty()) {
            final String message = "The user's email address is required.";
            Logger.error("event= \"{}\"", message);
            return failResult(BAD_REQUEST, message);
        }
        String newUserEmail = body.get("email").asText();
        Optional<HumanUser> humanUserOptional = humanUserService.findByEmail(newUserEmail);

        if (humanUserOptional.isPresent()) {
            final String message = "The specified user already exists.";
            Logger.error("event= \"{}\"", message);
            return failResult(BAD_REQUEST, message);
        }
        HumanUser user = adAuth.findUser(ldapUser, ldapPassword, newUserEmail);
        if (user == null || user.getNetworkId() == null) {
            final String message = "The new user could not be created (user not found in Active Directory)";
            Logger.error("event= \"{}\"", message);
            return failResult(BAD_REQUEST, message);
        }
        humanUserService.save(user);
        return successResult(user, "user");
    }

    /**
     * Provides the REST API to add User Permissions.
     *
     * @param apiVersion  The version of the API to use
     * @param humanUserId The humanUserId
     * @return A Result indicating whether the user permissions added successfully
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result addUserPermissions(final String apiVersion, final Long humanUserId) {

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.CREATE, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUserResult = humanUserService.findByHumanUserId(humanUserId);
        if (!humanUserResult.isPresent()) {
            return failResult(NOT_FOUND, "Can not find user with human user id " + humanUserId);
        }
        HumanUser humanUser = humanUserResult.get();
        JsonNode body = request().body().asJson();

        final JsonNode applicationModuleNode = body.get("applicationModule");
        final JsonNode permissionSettingNode = body.get("permissionSetting");

        if (applicationModuleNode != null && permissionSettingNode != null) {

            Gson gson = new Gson();

            ApplicationModule applicationModule = gson.fromJson(applicationModuleNode.toString(),
                    ApplicationModule.class);
            ObjectNode permissionNode = (ObjectNode) permissionSettingNode;
            permissionNode.remove("lastUpdate");
            PermissionSettings permissionSettings = gson.fromJson(permissionNode.toString(), PermissionSettings.class);
            JsonNode allEnvironments = body.get("allEnvironments");
            JsonNode allTenants = body.get("allTenants");
            JsonNode environments = body.get("environment");
            JsonNode previewerNode = body.get("previewer");
            JsonNode tenants = body.get("tenant");
            if (environments != null || allEnvironments != null) {
                if (allEnvironments != null && allEnvironments.asBoolean()) {
                    UserPermissions userPermissions = getUserPermissions(humanUser, applicationModule,
                            permissionSettings);
                    userPermissions.setIsWildcard(true);
                    userPermissionsService.save(userPermissions);
                } else {
                    for (JsonNode environmentNode : environments) {
                        Environment environment = gson.fromJson(environmentNode.toString(), Environment.class);

                        UserPermissions userPermissions = getUserPermissions(humanUser, applicationModule,
                                permissionSettings);
                        userPermissions.setIsWildcard(false);
                        userPermissions.setEnvironmentId(environment.getEnvironmentId());

                        userPermissionsService.save(userPermissions);
                    }

                }
            } else if ((previewerNode != null && tenants != null) || allTenants != null) {
                ObjectNode previewerObjectNode = (ObjectNode) previewerNode;
                previewerObjectNode.remove("lastUpdate");
                Previewer previewer = gson.fromJson(previewerObjectNode.toString(), Previewer.class);

                if (allTenants != null && allTenants.asBoolean()) {
                    UserPermissions userPermissions = getUserPermissions(humanUser, applicationModule,
                            permissionSettings);
                    userPermissions.setPreviewerId(previewer.getPreviewerId());
                    userPermissions.setIsWildcard(true);
                    userPermissionsService.save(userPermissions);
                } else {
                    for (JsonNode tenant : tenants) {
                        ObjectNode tenantObjectNode = (ObjectNode) tenant;
                        tenantObjectNode.remove("lastUpdate");

                        PreviewerTenant previewerTenant = gson.fromJson(tenantObjectNode.toString(),
                                PreviewerTenant.class);
                        UserPermissions userPermissions = getUserPermissions(humanUser, applicationModule,
                                permissionSettings);
                        userPermissions.setPreviewerId(previewer.getPreviewerId());
                        userPermissions.setIsWildcard(false);
                        userPermissions.setTenantId(previewerTenant.getTenantId());

                        userPermissionsService.save(userPermissions);

                    }
                }
            } else {
                UserPermissions userPermissions = getUserPermissions(humanUser, applicationModule, permissionSettings);
                userPermissionsService.save(userPermissions);
            }

            return successResult("Selected permissions successfully applied to the user.", "message");
        }
        return failResult(BAD_REQUEST, "Failed to apply selected permissions.");
    }

    /**
     * Provides the REST API to get all environments.
     *
     * @param apiVersion The version of the API to use.
     * @return The List of permission settings (Roles)
     */
    @Transactional
    public Result deleteUserPermission(final String apiVersion, final Long userPermissionId) {
        Logger.info("event=\"Deleting the user permission.\",userPermissionsId=\"{}\"", userPermissionId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.DELETE, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<UserPermissions> userPermissions = userPermissionsService.findByUserPermissionId(userPermissionId);
        if (userPermissions.isPresent()) {
            userPermissionsService.delete(userPermissions.get());
            return successResult("User Permission deleted successfully ", "message");
        } else {
            Logger.error("event=\"User permission not found\", userPermissionId=\"{}\"", userPermissionId);
            return failResult(NOT_FOUND, "User permission not found.");
        }
    }

    /**
     * Provides the REST API to update admin user role.
     *
     * @param apiVersion  The version of the API to use.
     * @param humanUserId the human user id
     * @return The result containing success for failure message.
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateAdminRole(final String apiVersion, final Long humanUserId) {
        Logger.info("event=\"Updating user role to Admin\", humanUserId=\"{}\"", humanUserId);

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.UPDATE, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUser = humanUserService.findByHumanUserId(humanUserId);
        if (humanUser.isPresent()) {
            Gson gson = new Gson();
            JsonNode body = request().body().asJson();
            HumanUser hu = gson.fromJson(body.toString(), HumanUser.class);

            humanUser.get().getUser().setIsAdmin(hu.getUser().getIsAdmin());
            return successResult("Admin user role is updated successfully", "message");
        }
        return failResult(NOT_FOUND, "Can not find user with human user id " + humanUserId);
    }

    /**
     * Provides the REST API to save a role (User Permission) in to the database.
     *
     * @param apiVersion The version of the API to use.
     * @return The result containing success for failure message.
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result addRole(final String apiVersion) {
        Logger.info("event=\"Saving new role\"");

        try {
            enforceModuleLevelPermission(ROLE_PERMISSION, AccessType.CREATE, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode body = request().body().asJson();
        String errorMsg = "";
        if (body == null) {
            errorMsg = "Can not find request body";
            Logger.error("event=\"{}\"", errorMsg);
            return failResult(NOT_FOUND, errorMsg);
        }

        if (body.get("name") == null) {
            errorMsg = "Can not find role name in the request body";
            Logger.error("event=\"{}\", Request Body= \"{}\"", errorMsg, body);
            return failResult(NOT_FOUND, errorMsg);
        }

        if (body.get("applicationModule") == null) {
            errorMsg = "Can not find applicationModule information in the request body";
            Logger.error("event=\"{}\", Request Body= \"{}\"", errorMsg, body);
            return failResult(NOT_FOUND, errorMsg);
        }
        if (body.get("lastUpdate") != null) {
            ((ObjectNode) body).remove("lastUpdate");
        }
        Gson gson = new Gson();
        try {
            PermissionSettings permissionSettings = gson.fromJson(body.toString(), PermissionSettings.class);
            permissionSettings
                    .setApplicationModuleId(permissionSettings.getApplicationModule().getApplicationModuleId());
            String message = "Failed to create/update role.";
            if (permissionSettings.getPermissionSettingsId() != null) {
                Optional<PermissionSettings> result = permissionSettingsService
                        .findByPermissionSettingsId(permissionSettings.getPermissionSettingsId());
                if (result.isPresent()) {
                    result.get().setName(permissionSettings.getName());
                    cache.remove("permissionSettings." + permissionSettings.getPermissionSettingsId());
                }

                message = "Role updated successfully.";
            } else {
                permissionSettingsService.save(permissionSettings);
                message = "New role saved successfully.";
            }
            return successResult(message, "message");
        } catch (JsonParseException ex) {
            errorMsg = "Failed to save role into database.";
            Logger.error("event=\"{}\", Request Body= \"{}\"", errorMsg, body, ex);
            return failResult(BAD_REQUEST, errorMsg);
        }

    }

    /**
     * Provides the REST API to delete a role (User Permission) from database.
     *
     * @param apiVersion          The version of the API to use.
     * @param permissionSettingId The role id
     * @return The result containing success for failure message.
     */
    @Transactional
    public Result deleteRole(final String apiVersion, final Long permissionSettingId) {
        Logger.info("event=\"Deleting the role.\",permissionSettingId=\"{}\"", permissionSettingId);

        try {
            enforceModuleLevelPermission(ROLE_PERMISSION, AccessType.DELETE, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<PermissionSettings> permissionSettings = permissionSettingsService
                .findByPermissionSettingsId(permissionSettingId);
        if (permissionSettings.isPresent()) {
            List<UserPermissions> userPermissions = userPermissionsService
                    .findByPermissionSettingId(permissionSettingId);
            if (!userPermissions.isEmpty()) {
                List<String> userNames = userPermissions.stream().map(up -> up.getUser().getFullname())
                        .collect(Collectors.toList());

                return failResult(CONFLICT,
                        "The " + permissionSettings.get().getName() + " role is " + "assigned to one or"
                                + " more users and "
                                + "cannot be deleted. This role is assigned to the following users: "
                                + String.join(", ", userNames));
            }
            permissionSettingsService.delete(permissionSettings.get());
            return successResult("Role deleted successfully ", "message");
        } else {
            Logger.error("event=\"Role not found\", permissionSettingId=\"{}\"", permissionSettingId);
            return failResult(NOT_FOUND, "User permission not found.");
        }
    }

    /**
     * Provides the REST API to retrieve application modules that has at least on permission option exist.
     *
     * @param apiVersion The version of the API to use.
     * @return The list of {@link ApplicationModule}
     */
    @Transactional(readOnly = true)
    public Result getApplicationModuleWithPermissionOption(final String apiVersion) {
        Logger.info("event=\"Retrieving application modules that has at least on permission option exist.\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(applicationModulesService.findPermissionOptionAssignedApplicationModules(),
                "applicationModules");
    }

    /**
     * Provides the REST API to get permission options for given <code>applicationModuleId</code>
     *
     * @param apiVersion          The version of the API to use.
     * @param roleId              The role ID
     * @param applicationModuleId The application module id
     * @return The list of permission options
     */
    @Transactional(readOnly = true)
    public Result getPermissionSettingsValues(final String apiVersion, final Long applicationModuleId,
                                              final Long roleId) {
        Logger.info("event=\"Retrieving permission setting values.\", \"roleId=\"" + roleId + "\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.READ, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        List<PermissionOption> permissionOptions = permissionOptionService
                .findByApplicationModuleId(applicationModuleId);
        List<PermissionSettingsValue> permissionSettingsValues = permissionSettingsValueService
                .findByPermissionSettingsId(applicationModuleId, roleId);
        for (PermissionSettingsValue psv : permissionSettingsValues) {
            permissionOptions.remove(psv.getPermissionOption());
        }
        if (permissionOptions.isEmpty()) {
            return successResult(permissionSettingsValues, "permissionSettingsValues");
        }

        Optional<PermissionSettings> permissionSettingsResult = permissionSettingsService
                .findByPermissionSettingsId(roleId);
        if (permissionSettingsResult.isPresent()) {
            for (PermissionOption permissionOption : permissionOptions) {
                PermissionSettingsValue permissionSettingsValue = new PermissionSettingsValue();
                permissionSettingsValue.setPermissionOption(permissionOption);
                permissionSettingsValue.setPermissionSettings(permissionSettingsResult.get());
                permissionSettingsValues.add(permissionSettingsValue);
            }

        }
        return successResult(permissionSettingsValues, "permissionSettingsValues");
    }

    /**
     * Provides the REST API to save a permission setting options in to the database.
     *
     * @param apiVersion The version of the API to use.
     * @return The result containing success for failure message.
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result addPermissionSettingValues(final String apiVersion) {
        Logger.info("event=\"creating permissionSetting options\"");

        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.CREATE, Module.USER_MANAGEMENT.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode body = request().body().asJson();
        String errorMsg = "";
        if (body == null) {
            errorMsg = "Can not find request body";
            Logger.error("event=\"{}\"", errorMsg);
            return failResult(NOT_FOUND, errorMsg);
        }
        if (!body.isArray()) {
            errorMsg = "The response is not in array format. The response is supposed to be the list of PermissionSettingsValue";
            Logger.error("event=\"{}\", Request Body= \"{}\"", errorMsg, body.toString());
            return failResult(NOT_FOUND, errorMsg);
        }

        for (JsonNode permissionSettingValueNode : body) {
            ((ObjectNode) permissionSettingValueNode.get("permissionSettings")).remove("lastUpdate");
        }

        try {
            final Type listType = new TypeToken<List<PermissionSettingsValue>>() {
            }.getType();
            Gson gson = new Gson();
            List<PermissionSettingsValue> permissionSettingsValues = gson.fromJson(body.toString(), listType);

            for (PermissionSettingsValue permissionSettingsValue : permissionSettingsValues) {
                if (permissionSettingsValue.getPermissionSettingsValueId() != null) {
                    Optional<PermissionSettingsValue> result = permissionSettingsValueService
                            .findPermissionSettingValueById(permissionSettingsValue.getPermissionSettingsValueId());

                    if (result.isPresent()) {
                        PermissionSettingsValue updatedPermissionSettingsValue = result.get();
                        cache.remove("permissionSettings."
                                + updatedPermissionSettingsValue.getPermissionSettings().getPermissionSettingsId());
                        updatedPermissionSettingsValue.setCreateAccess(permissionSettingsValue.getCreateAccess());
                        updatedPermissionSettingsValue.setReadAccess(permissionSettingsValue.getReadAccess());
                        updatedPermissionSettingsValue.setUpdateAccess(permissionSettingsValue.getUpdateAccess());
                        updatedPermissionSettingsValue.setDeleteAccess(permissionSettingsValue.getDeleteAccess());
                        if (!containsPermissionOptions(updatedPermissionSettingsValue)) {
                            permissionSettingsValueService.delete(updatedPermissionSettingsValue);
                        }

                    }
                } else if (containsPermissionOptions(permissionSettingsValue)) {
                    permissionSettingsValueService.create(permissionSettingsValue);
                }
            }
            return successResult("Permission option updated successfully.", "message");
        } catch (JsonParseException ex) {
            errorMsg = "Failed to save role into database.";
            Logger.error("event=\"{}\", Request Body= \"{}\"", errorMsg, body, ex);
            return failResult(BAD_REQUEST, errorMsg);
        }

    }

    private boolean containsPermissionOptions(final PermissionSettingsValue permissionSettingsValue) {
        return (permissionSettingsValue.getCreateAccess() || permissionSettingsValue.getReadAccess()
                || permissionSettingsValue.getUpdateAccess() || permissionSettingsValue.getDeleteAccess());

    }

    private UserPermissions getUserPermissions(HumanUser humanUser, ApplicationModule applicationModule,
                                               PermissionSettings permissionSettings) {
        UserPermissions userPermissions = new UserPermissions();
        userPermissions.setUserId(humanUser.getUserId());
        userPermissions.setUser(humanUser.getUser());
        userPermissions.setApplicationModule(applicationModule);
        userPermissions.setPermissionSettings(permissionSettings);
        userPermissions.setIsWildcard(false);
        return userPermissions;
    }

}
