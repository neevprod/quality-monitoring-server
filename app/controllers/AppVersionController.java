package controllers;

import com.google.inject.Inject;
import com.typesafe.config.Config;
import play.mvc.Result;


public class AppVersionController extends BaseController {
	private String appVersion;
	
	@Inject
	public AppVersionController(Config config) {
		appVersion = config.getString("app.version");
	}
	public Result getAppVersion(String apiVersion) {
         
        return successResult(appVersion, "appVersion");
    }

   
}
