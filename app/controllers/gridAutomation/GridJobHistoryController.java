package controllers.gridAutomation;


import com.fasterxml.jackson.databind.JsonNode;
import controllers.SecureController;
import models.gridautomation.GridJob;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import services.gridautomation.GridAutomationService;
import util.SessionData;

import javax.inject.Inject;
import java.util.List;

/**
 * @author Bryce Rodgers
 */
public class GridJobHistoryController extends SecureController {
    private final GridAutomationService gridAutomationService;


    @Inject
    public GridJobHistoryController(SessionData.Factory sessionDataFactory, GridAutomationService gridAutomationService) {
        super(sessionDataFactory);
        this.gridAutomationService = gridAutomationService;
    }


    /**
     * Provides the REST API to get all active contracts.
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of contracts
     */
    @Transactional(readOnly = true)
    public Result getGridJobs(final String apiVersion) {
        Logger.info("event=\"Retrieving grid jobs.\"");

        List<GridJob> gridJobExecutions = gridAutomationService.findAllGridJobs();

        JsonNode gridJobExecsJson = Json.toJson(gridJobExecutions);

        return successResult(gridJobExecsJson, "gridJobExecs");
    }
}
