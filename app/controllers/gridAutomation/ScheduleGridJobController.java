package controllers.gridAutomation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.pearson.shipment.gridder.GridFile;
import com.pearson.shipment.gridder.Gridder;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import models.gridautomation.GridJob;
import models.systemvalidation.DivJobExecStatus;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;
import services.gridautomation.GridAutomationService;
import services.systemvalidation.DivJobExecStatusService;
import util.SessionData;

import javax.inject.Inject;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * @author Bryce Rodgers
 */
public class ScheduleGridJobController extends SecureController {
    private static final String QUAL_GROUP = "TST";
    private static final String USER_PERMISSION = "gridAutomation";
    private final GridAutomationService gridAutomationService;
    private final DivJobExecStatusService divJobExecStatusService;


    @Inject
    public ScheduleGridJobController(final SessionData.Factory sessionDataFactory,
                                     final GridAutomationService gridAutomationService,
                                     final DivJobExecStatusService divJobExecStatusService) {
        super(sessionDataFactory);
        this.gridAutomationService = gridAutomationService;
        this.divJobExecStatusService = divJobExecStatusService;
    }


    @Transactional
    @BodyParser.Of(BodyParser.MultipartFormData.class)
    public Result submitJob(final String apiVersion) {
        try {
            enforceModuleLevelPermission(USER_PERMISSION, AccessType.CREATE, Module.GRID_AUTOMATION.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        try {
            Http.MultipartFormData<File> body = request().body().asMultipartFormData();
            String[] data = body.asFormUrlEncoded().get("data");
            if (body.getFiles().isEmpty()) {
                Logger.error("event=\"No Bubbler Files selected to upload in request body\"");
                return failResult(BAD_REQUEST, "Please select at least one Bubbler file to upload!");
            }
            ObjectMapper jsonMapper = new ObjectMapper();
            JsonNode jsonData = jsonMapper.readTree(data[0]);

            if (hasRequiredFields(jsonData)) {
                String email = sessionData().getUserEmail().orElse(null);
                GridJob gridJob = createGridJob(jsonData);
                gridJob.setUserId(sessionData().getUserId());
                List<Http.MultipartFormData.FilePart<File>> files = body.getFiles();
                for (Http.MultipartFormData.FilePart<File> file : files) {
                    GridFile gridFile = new GridFile(file.getFile(), gridJob.getQualGroup(), gridJob.getLithoStart(), "false",
                            gridJob.getDocCode(), gridJob.getStateCode(), gridJob.getRunType(), "_newGrid_validation", "1", gridJob.getIs6By8(), "",
                            email, gridJob.getComments());

                    //update gridJob with new fileName
                    if (gridJob.getFileName() == null) {
                        gridJob.setFileName(file.getFilename());
                    } else if ((gridJob.getFileName().length() + file.getFilename().length() + 2) > 500) {
                        return failResult(BAD_REQUEST, "Combined File Names are over limit of total length of 500.");
                    } else {
                        gridJob.setFileName(gridJob.getFileName() + ", " + file.getFilename());
                    }

                    //Creating temp files because library needs physical file
                    File gridJobTempDir = gridFile(gridFile);

                    //sending FTPed file to Owatonna
                    processGridFile(gridJobTempDir, gridJob);
                }
            } else {
                return failResult(BAD_REQUEST, "Required fields are missing!");
            }

            Logger.info("event=\"Finished processing single Grid File.\"");
        } catch (Exception e) {
            Logger.error("error=\"Error Processing Grid File.\" Grid Job may or may not have saved.", e);
        }

        return successResult("success!", "sampleVarOutput");
    }

    private GridJob createGridJob(JsonNode jsonData) {
        GridJob gridJob = new GridJob();

        int gridContractId = jsonData.get("gridContractId").asInt();
        gridJob.setGridContractId(gridContractId);

        int programId = jsonData.get("programId").asInt();
        gridJob.setGridProgramId(programId);

        int adminId = jsonData.get("adminId").asInt();
        gridJob.setGridAdminId(adminId);

        int seasonId = jsonData.get("seasonId").asInt();
        gridJob.setGridSeasonId(seasonId);

        gridJob.setRunType(jsonData.get("runType").asText());
        gridJob.setPgmssn(jsonData.get("pgmssn").asText());
        gridJob.setDocCode(jsonData.get("docCode").asText());
        gridJob.setStateCode(jsonData.get("stateCode").asText());
        gridJob.setLithoStart(jsonData.get("lithoStart").asText());
        gridJob.setQualGroup(QUAL_GROUP);
        String deliveryDate = jsonData.get("deliveryDate").asText();
        Timestamp ts = Timestamp.from(Instant.parse(deliveryDate));
        gridJob.setDeliveryDate(ts);
        if (jsonData.has("comments")) {
            gridJob.setComments(jsonData.get("comments").asText());
        }

        if (jsonData.has("is6by8")) {
            gridJob.setIs6By8(jsonData.get("is6by8").asBoolean());
        } else {
            gridJob.setIs6By8(false);//form does not report for option values, setting to 0 for default
        }

        if (jsonData.has("autorelease")) {
            gridJob.setAutoRelease(jsonData.get("autorelease").asBoolean());
        } else {
            gridJob.setAutoRelease(false);//form does not report for option values, setting to 0 for default
        }

        return gridJob;
    }

    private boolean hasRequiredFields(JsonNode jsonData) {
        return !(jsonData.path("jobAdmin").path("adminId").asText() == null ||
                jsonData.path("jobContract").path("gridContractId").asText() == null ||
                jsonData.path("jobProgram").path("programId").asText() == null ||
                jsonData.path("jobSeason").path("seasonId").asText() == null ||
                jsonData.path("runType").asText() == null ||
                jsonData.path("docCode").asText() == null);
    }

    private void processGridFile(final File gridJobTempDir, GridJob gridJob) {
        try {
            if (gridJob.getRunType().equals("DV")) {
                Logger.info("event=\"GridJob finished processing for runType: DV. Skipping SFTP.\"");
                updateJobStatus(gridJob, "Completed");
            } else if (gridJobTempDir != null) {
                Logger.info("event=\"GridJob finished processing for runType: " + gridJob.getRunType() + ". Starting SFTP.\"");
                ftpToOwa(gridJobTempDir, gridJob.getAutoRelease());
                updateJobStatus(gridJob, "Completed");
            } else {
                updateJobStatus(gridJob, "Failed");
                Logger.error("error=\"no grid job temporary directory was created.\"");
            }
            gridAutomationService.saveJob(gridJob);
        } catch (Exception e) {
            Logger.error("error=\"Exception while constructing or handling Grid file.\"" + e.toString());
            Logger.error(e.getMessage());
            updateJobStatus(gridJob, "Failed");
            gridAutomationService.saveJob(gridJob);
        }
    }

    private void updateJobStatus(GridJob gridJob, String statusName) {
        Optional<DivJobExecStatus> jobStatus = this.divJobExecStatusService.findByDivJobExecStatusName(statusName);
        jobStatus.ifPresent(divJobExecStatus -> gridJob.setJobStatusId(jobStatus.get().getDivJobExecStatusId()));
    }

    /**
     * Processes a GridFile using Gridder engine
     * <p>
     * Corrupts all system.out until the engine is updated to not redirect console/Sysout
     * Engine needs to be updated to not require JTextArea as well
     *
     * @param gridFile The GridFile to be processed
     */
    private File gridFile(GridFile gridFile) {

        Logger.info("event=\"Processing single Grid file.\"");
        Gridder gridder = new Gridder();
        gridder.init();
        File result = null;
        String runType = gridFile.getTypeOfRun();
        String email = sessionData().getUserEmail().orElse(null);
        try {
            if (!runType.equals("DV")) {
                if (email != null) {
                    result = gridder.processCSV(gridFile, email);
                } else {
                    result = gridder.processCSV(gridFile);
                }
            } else {
                if (email != null) {
                    gridder.validateData(gridFile, email);
                } else {
                    gridder.validateData(gridFile);
                }
            }
        } catch (Exception e) {
            Logger.error("error=\"Exception processing Grid file.\"" + e.toString());
        }
        return result;
    }

    /**
     * SFTP a file to Owatonna by hostname
     * <p>
     * this should simply work by hostname
     *
     * @param folder The folder to be SFTP'd
     */
    private void ftpToOwa(File folder, boolean autoRelease) {

        Logger.info("event=\"transferring Grid file to Owatonna\"");
        Session session = null;
        ChannelSftp sftpChannel = null;

        try {
            JSch jsch = new JSch();
            session = jsch.getSession("owatgridding001", "owatgriddingsftp.pearson.com", 22);//TODO: move to config file
            session.setPassword("VC4x66wZ");
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            sftpChannel = (ChannelSftp) session.openChannel("sftp");
            sftpChannel.connect();

            String sourcePath = folder.getAbsolutePath();
            String destPath = autoRelease ? "GriddingAutomation" : "GriddingAutomationTest";

            Logger.info("event=\"ftp line is: " + sourcePath + File.separatorChar + folder.getName() + ", " + destPath + File.separatorChar + folder.getName() + "\"");

            for (Iterator<Path> files = Files.walk(Paths.get(sourcePath)).iterator(); files.hasNext(); ) {
                Path p = files.next();

                if (p.toFile().isDirectory()) {
                    Logger.info("Creating directory " + folder.getName());
                    sftpChannel.cd(destPath);
                    sftpChannel.mkdir(folder.getName());
                    sftpChannel.cd(folder.getName());
                } else {
                    sftpChannel.put(p.toFile().getAbsolutePath(), p.toFile().getName());
                }
            }

            //TODO: verify files exists on OWA SFTP site before logging "complete" and disconnecting
            Logger.info("event=\"SFTP transfer complete\"");

            //TODO: in production, after SFTP is successful perform delete on object 'folder'
        } catch (Throwable e) {
            Logger.error("error=\"Exception during file transfer to Owatonna.\"" + e.toString());

        } finally {
            if (sftpChannel != null) {
                sftpChannel.exit();
            }
            if (session != null) {
                session.disconnect();
            }
        }
    }


    /**
     * Provides the REST API to get all available contracts.
     * <p>
     * Should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of contracts
     */
    @Transactional(readOnly = true)
    public Result getAllContracts(final String apiVersion) {
        Logger.info("event=\"Retrieving all contracts.\"");

        return successResult(gridAutomationService.findAllContracts(), "contracts");
    }

    /**
     * Provides the REST API to get all active contracts.
     * <p>
     * should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of contracts
     */
    @Transactional(readOnly = true)
    public Result getActiveContracts(final String apiVersion) {
        Logger.info("event=\"Retrieving active contracts.\"");

        return successResult(gridAutomationService.findActiveContracts(), "contracts");
    }

    /**
     * Provides the REST API to get all available programs.
     * <p>
     * should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of programs
     */
    @Transactional(readOnly = true)
    public Result getAllPrograms(final String apiVersion) {
        Logger.info("event=\"Retrieving all programs.\"");
        return successResult(gridAutomationService.findAllPrograms(), "programs");
    }

    /**
     * Provides the REST API to get all active programs.
     * <p>
     * should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of programs
     */
    @Transactional(readOnly = true)
    public Result getActivePrograms(final String apiVersion) {
        Logger.info("event=\"Retrieving active programs.\"");

        return successResult(gridAutomationService.findActivePrograms(), "programs");
    }

    /**
     * Provides the REST API to get all available admins.
     * <p>
     * should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of admins
     */
    @Transactional(readOnly = true)
    public Result getAllAdmins(final String apiVersion) {
        Logger.info("event=\"Retrieving all admins.\"");

        return successResult(gridAutomationService.findAllAdmins(), "admins");
    }

    /**
     * Provides the REST API to get all active admins.
     * <p>
     * should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of admins
     */
    @Transactional(readOnly = true)
    public Result getActiveAdmins(final String apiVersion) {
        Logger.info("event=\"Retrieving active admins.\"");

        return successResult(gridAutomationService.findActiveAdmins(), "admins");
    }

    /**
     * Provides the REST API to get all available seasons.
     * <p>
     * should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of seasons
     */
    @Transactional(readOnly = true)
    public Result getAllSeasons(final String apiVersion) {
        Logger.info("event=\"Retrieving all seasons.\"");

        return successResult(gridAutomationService.findAllSeasons(), "seasons");
    }

    /**
     * Provides the REST API to get all active seasons.
     * <p>
     * should try permissions here
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the list of seasons
     */
    @Transactional(readOnly = true)
    public Result getActiveSeasons(final String apiVersion) {
        Logger.info("event=\"Retrieving active seasons.\"");

        return successResult(gridAutomationService.findActiveSeasons(), "seasons");
    }
}
