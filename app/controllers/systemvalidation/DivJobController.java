package controllers.systemvalidation;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoClient;
import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariDataSource;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import jobs.JobRunnerActor;
import jobs.systemvalidation.DataIntegrityValidationActor.DivStateName;
import models.systemvalidation.*;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import services.systemvalidation.*;
import util.ConnectionPoolManager;
import util.SessionData;
import util.systemvalidation.DivJobSetupUtil;
import util.systemvalidation.DivJobSetupUtil.StatusNames;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceException;
import javax.persistence.Tuple;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DivJobController extends SecureController {
    // These information will not be displayed in the report.
    private static final List<String> REMOVED_KEYS = Arrays.asList("contentKey", "wireKey", "urlGetTestDef");
    private static final String DIV_JOB_EXECUTIONS = "divJobExecutions";
    private static final String DIV_JOB_EXEC = "divJobExec";
    private static final String STUDENT_SCENARIO_RESULTS = "studentScenarioResults";
    private static final String ENVIRONMENTS = "environments";
    private static final String DIV_JOB = "divJob";
    private static final String ENVIRONMENT = "environment";
    private static final String STATUS = "status";
    private final DivJobExecService divJobExecService;
    private final DivJobExecStatusService divJobExecStatusService;
    private final StudentScenarioService studentScenarioService;
    private final StudentScenarioResultService stdScenarioResultService;
    private final ScenarioService scenarioService;
    private final EnvironmentService environmentService;
    private final DbConnectionService dbConnectionservice;
    private final StudentScenarioResultCommentService studentScenarioResultCommentService;
    private final ApiConnectionService apiConnectionService;
    private final StudentScenarioResultCommentCategoryService studentScenarioResultCommentCategoryService;
    private final ExternalDefectRepositoryService externalDefectRepositoryService;
    private final StudentScenarioStatusService studentScenarioStatusService;
    private final StateService stateService;
    private final ActorRef jobRunnerActor;
    private final long actorAskTimeout;
    private final ConnectionPoolManager connectionPoolManager;

    @Inject
    DivJobController(SessionData.Factory sessionDataFactory, DivJobExecService divJobExecService, DivJobExecStatusService divJobExecStatusService,
                     StudentScenarioService studentScenarioService, StudentScenarioResultService stdScenarioResultService,
                     ScenarioService scenarioService, EnvironmentService environmentService,
                     DbConnectionService dbConnectionservice,
                     StudentScenarioResultCommentService studentScenarioResultCommentService,
                     ApiConnectionService apiConnectionService,
                     StudentScenarioResultCommentCategoryService studentScenarioResultCommentCategoryService,
                     ExternalDefectRepositoryService externalDefectRepositoryService,
                     StudentScenarioStatusService studentScenarioStatusService, StateService stateService,
                     @Named("jobRunnerActor") ActorRef jobRunnerActor, Config configuration, ConnectionPoolManager connectionPoolManager) {
        super(sessionDataFactory);
        this.divJobExecService = divJobExecService;
        this.divJobExecStatusService = divJobExecStatusService;
        this.studentScenarioService = studentScenarioService;
        this.stdScenarioResultService = stdScenarioResultService;
        this.scenarioService = scenarioService;
        this.environmentService = environmentService;
        this.dbConnectionservice = dbConnectionservice;
        this.studentScenarioResultCommentService = studentScenarioResultCommentService;
        this.apiConnectionService = apiConnectionService;
        this.studentScenarioResultCommentCategoryService = studentScenarioResultCommentCategoryService;
        this.externalDefectRepositoryService = externalDefectRepositoryService;
        this.studentScenarioStatusService = studentScenarioStatusService;
        this.stateService = stateService;
        this.jobRunnerActor = jobRunnerActor;
        this.actorAskTimeout = configuration.getDuration("application.actorAskTimeout").toMillis();
        this.connectionPoolManager = connectionPoolManager;
    }

    /**
     * Post the new environment to save into database.
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the environment
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result saveEnvironment(final String apiVersion) {
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.CREATE,
                Module.SYSTEM_VALIDATION.getName())) {
            JsonNode body = request().body().asJson();
            Environment environment = Json.fromJson(body, Environment.class);

            try {
                environmentService.save(environment);
            } catch (PersistenceException e) {
                if (e.getCause() != null && e.getCause().getCause() != null
                        && e.getCause().getCause().getMessage().contains("Duplicate entry")) {
                    return failResult(BAD_REQUEST, "An environment with the specified name already exists.");
                }
                return failResult(BAD_REQUEST, "Failed to save the environment.");
            }
            return successResult(environment, "environment");
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Updates the environment.
     *
     * @param apiVersion    The version of the API to use
     * @param environmentId The environment Id
     * @return A Result containing the environment.
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateEnvironment(final String apiVersion, final Integer environmentId) {
        if (sessionData().hasEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName(), environmentId)) {
            JsonNode body = request().body().asJson();
            Environment environmentFromForm = Json.fromJson(body, Environment.class);
            Optional<Environment> environmentFromDb = environmentService.findById(environmentId);
            if (environmentFromDb.isPresent()) {
                Environment env = environmentFromDb.get();
                env.setName(environmentFromForm.getName());
                env.setFeUrl(environmentFromForm.getFeUrl());
                env.setEpenUrl(environmentFromForm.getEpenUrl());

                env.setFeMysqlConnection(environmentFromForm.getFeMysqlConnection());
                env.setFeApiConnection(environmentFromForm.getFeApiConnection());

                env.setBeMysqlConnection(environmentFromForm.getBeMysqlConnection());
                env.setBeApiConnection(environmentFromForm.getBeApiConnection());

                env.setIrisMysqlConnection(environmentFromForm.getIrisMysqlConnection());
                env.setIrisApiConnection(environmentFromForm.getIrisApiConnection());

                env.setDataWarehouseMongoConnection(environmentFromForm.getDataWarehouseMongoConnection());
                env.setDataWarehouseApiConnection(environmentFromForm.getDataWarehouseApiConnection());

                env.setEpen2MongoConnection(environmentFromForm.getEpen2MongoConnection());
                env.setEpen2MysqlConnection(environmentFromForm.getEpen2MysqlConnection());
                env.setEpen2ApiConnection(environmentFromForm.getEpen2ApiConnection());

                env.setIceBridgeOracleConnection(environmentFromForm.getIceBridgeOracleConnection());
                env.setIceBridgeApiConnection(environmentFromForm.getIceBridgeApiConnection());

                return successResult(env, "environment");
            } else {
                Logger.error("event=\"Environment does not exist in database.\", environmentId=\"{}\"", environmentId);
                return failResult(NOT_FOUND, "Environment does not exist in database for environmentId = "
                        + environmentId);
            }
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Sets the archive status of an environment.
     *
     * @param apiVersion    The version of the API to use
     * @param environmentId The environment Id
     * @return A Result containing the environment.
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result setEnvironmentArchiveStatus(final String apiVersion, final Integer environmentId) {
        if (sessionData().hasEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName(), environmentId)) {
            JsonNode body = request().body().asJson();
            String archiveStatusField = "archiveStatus";
            boolean newArchiveStatus;

            if (body.get(archiveStatusField) != null && body.get(archiveStatusField).isBoolean()) {
                newArchiveStatus = body.get(archiveStatusField).asBoolean();
            } else {
                return failResult(BAD_REQUEST, "Request body must include a boolean value for archiveStatus.");
            }

            Optional<Environment> optionalEnvironment = environmentService.findById(environmentId);
            if (optionalEnvironment.isPresent()) {
                Environment environment = optionalEnvironment.get();
                environment.setArchived(newArchiveStatus);
                return successResult(environment, "environment");
            } else {
                Logger.error("event=\"Environment does not exist in database.\", environmentId=\"{}\"", environmentId);
                return failResult(NOT_FOUND, "No environment exists with ID " + environmentId + ".");
            }
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Get the environments that the logged in user has access to. If the user does not have update access to any
     * environment, filter out the archived environments.
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the environments
     */
    @Transactional(readOnly = true)
    public Result getEnvironments(final String apiVersion) {
        Logger.info("event=\"Retrieving all environments.\"");
        SessionData sessionData = sessionData();
        boolean includeArchivedEnvironments = sessionData.hasAnyEnvironmentLevelPermission(ENVIRONMENTS,
                AccessType.UPDATE, Module.SYSTEM_VALIDATION.getName());

        if (sessionData.isAdmin() || sessionData.hasWildcardAccessToEnvironments(Module.SYSTEM_VALIDATION.getName())) {
            List<Environment> environments = environmentService.findAll().stream()
                    .filter(e -> !e.getArchived() || includeArchivedEnvironments).collect(Collectors.toList());
            return successResult(environments, "environments");
        } else {
            Set<Integer> allowedEnvironments = sessionData.getAllowedEnvironmentIds(Module.SYSTEM_VALIDATION.getName());
            List<Environment> environments = environmentService.findAll(allowedEnvironments).stream()
                    .filter(e -> !e.getArchived() || includeArchivedEnvironments).collect(Collectors.toList());
            return successResult(environments, "environments");
        }
    }

    /**
     * Get the Environment for the given <code>environmentId</code>.
     *
     * @param apiVersion    The version of the API to use
     * @param environmentId The Environment Id
     * @return The Result containing Environment
     */
    @Transactional(readOnly = true)
    public Result getEnvironmentById(final String apiVersion, final Integer environmentId) {
        Logger.info("event=\"Retrieving environment.\", \"environmentId\"=\"{}\"", environmentId);

        Optional<Environment> environment;
        SessionData sessionData = sessionData();
        if (sessionData.isAdmin() || sessionData.hasWildcardAccessToEnvironments(Module.SYSTEM_VALIDATION.getName())) {
            environment = environmentService.findById(environmentId);
        } else {
            if (sessionData.getAllowedEnvironmentIds(Module.SYSTEM_VALIDATION.getName()).contains(environmentId)) {
                environment = environmentService.findById(environmentId);
            } else {
                environment = Optional.empty();
            }
        }

        if (environment.isPresent()) {
            return successResult(environment.get(), "environment");
        } else {
            return failResult(NOT_FOUND, "Unable to find an environment with the given ID.");
        }
    }

    /**
     * Gets the list of non-archived environments which excludes the environment with given environment id and must have the given dbConnectionId.
     *
     * @param apiVersion     The version of the API to use
     * @param environmentId  The Environment Id
     * @param dbConnectionId The dbConnection  Id
     * @return The Result containing list of Environments
     */
    @Transactional(readOnly = true)
    public Result getEnvironmentNamesByDbConnectionId(String apiVersion, int environmentId, int dbConnectionId) {
        Logger.info("event=\"Retrieving environments by dbConnectionId.\", \"dbConnectionId\"=\"{}\"", dbConnectionId);
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.CREATE,
                Module.SYSTEM_VALIDATION.getName())) {
            List<String> environments = environmentService.findAllByDbConnectionId(dbConnectionId).stream()
                    .filter(e -> !e.getArchived())
                    .filter(e -> e.getEnvironmentId() != environmentId)
                    .map(Environment::getName)
                    .collect(Collectors.toList());
            return successResult(environments, "environments");
        } else {
            return accessDeniedResult();
        }
    }

    @Transactional(readOnly = true)
    public Result getDivJobByDivJobId(final String apiVersion, final Integer environmentId, final Long divJobExecId) {
        Logger.info("event=\"Retrieving DIV job.\", divJobExecId=\"{}\"", divJobExecId);

        Optional<DivJobExec> divJobExec = divJobExecService.findByDivJobExecId(divJobExecId);

        if (divJobExec.isPresent()) {
            DivJobExec dje = divJobExec.get();
            List<Long> divJobExecIds = Collections.singletonList(divJobExecId);
            List<Tuple> ssscr = divJobExecService.getDivJobStudentScenarioStatusCounts(divJobExecIds);

            dje.setNumInProgress(ssscr.get(0).get("inProgress", BigDecimal.class).intValueExact());
            boolean hasStatsState=dje.getPath().getPathStates().stream().anyMatch(ps -> ps.getState().getStateName().equalsIgnoreCase("STATS"));
            dje.setHasStatsState(hasStatsState);
            JsonNode divJobExecJson = Json.toJson(dje);
            ObjectNode data = Json.newObject();
            data.set(DIV_JOB, divJobExecJson);
            return successResult(data);
        } else {
            return failResult(NOT_FOUND, "Unable to find the requested divJobExecId.");
        }
    }

    /**
     * Get the list of DIV Jobs executions for the given <code>environmentId</code>.
     *
     * @param apiVersion    The version of the API to use
     * @param environmentId The ID of the environment to retrieve the DIV jobs for
     * @return The list of DIV job executions
     */
    @Transactional(readOnly = true)
    public Result getDivJobsByEnvironmentId(final String apiVersion, final Integer environmentId) {
        Logger.info("event=\"Retrieving all DIV jobs\", environmentId=\"{}\"", environmentId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        List<DivJobExec> divJobExecutions = divJobExecService.findByEnvironmentId(environmentId);

        if (divJobExecutions.isEmpty()) {
            JsonNode divJobExecsJson = Json.toJson(divJobExecutions);
            ObjectNode data = Json.newObject();
            data.set(DIV_JOB_EXECUTIONS, divJobExecsJson);
            return successResult(data);
        }

        List<Long> divJobExecIds = new ArrayList<>();

        for (DivJobExec divJobExec : divJobExecutions) {
            divJobExecIds.add(divJobExec.getDivJobExecId());
        }

        List<Tuple> ssscr = divJobExecService.getDivJobStudentScenarioStatusCounts(divJobExecIds);

        Map<Long, Tuple> divJobExecStudentScenarioCounts = new HashMap<>();

        for (Tuple sssc : ssscr) {
            Long divJobExecId = sssc.get("div_job_exec_id", BigInteger.class).longValue();
            divJobExecStudentScenarioCounts.put(divJobExecId, sssc);
        }

        for (DivJobExec divJobExec : divJobExecutions) {
            updateDivJobCounts(divJobExecStudentScenarioCounts, divJobExec);
        }

        try {
            JsonNode runningAndQueuedJobs = (JsonNode) FutureConverters
                    .toJava(Patterns.ask(jobRunnerActor, JobRunnerActor.GET_RUNNING_JOBS_MESSAGE, actorAskTimeout))
                    .toCompletableFuture().get(actorAskTimeout, TimeUnit.MILLISECONDS);
            JsonNode queuedJobs = runningAndQueuedJobs.get("queuedJobs");

            Set<Long> queuedDivJobExecs = new HashSet<>();
            Pattern pattern = Pattern.compile("^dataIntegrityValidation_(\\d+)$");
            for (JsonNode queuedJob : queuedJobs) {
                Matcher matcher = pattern.matcher(queuedJob.asText());
                if (matcher.matches()) {
                    queuedDivJobExecs.add(Long.valueOf(matcher.group(1)));
                }
            }

            JsonNode divJobExecsJson = Json.toJson(divJobExecutions);
            int i = 0;
            while (!queuedDivJobExecs.isEmpty() && i < divJobExecsJson.size()) {
                long divJobExecId = divJobExecsJson.get(i).get("divJobExecId").asLong();
                if (queuedDivJobExecs.contains(divJobExecId)) {
                    queuedDivJobExecs.remove(divJobExecId);

                    ObjectNode divJobExecStatusJson = (ObjectNode) divJobExecsJson.get(i).get("divJobExecStatus");
                    divJobExecStatusJson.put("divJobExecStatusId", 0);
                    divJobExecStatusJson.put("status", "Queued");
                }
                i++;
            }

            ObjectNode data = Json.newObject();
            data.set(DIV_JOB_EXECUTIONS, divJobExecsJson);
            return successResult(data);
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            Logger.error("event=\"Issue getting response from job runner actor when retrieving list of queued DIV "
                    + "jobs.\"");
            return successResult(divJobExecutions, DIV_JOB_EXECUTIONS);
        }
    }

    /**
     * Update the student scenario counts by status for the given DIV job
     *
     * @param divJobExecStudentScenarioCounts Map containing the studentScenarioCounts by DIV job.
     * @param divJobExec                      The DIV Job to update the student scenario counts for.
     */
    private void updateDivJobCounts(Map<Long, Tuple> divJobExecStudentScenarioCounts, DivJobExec divJobExec) {
        int numCompletedOeExcluded = 0;
        int numCompletedOeOpExcluded = 0;
        int numCompletedOeFtExcluded = 0;
        int numCompleted = 0;
        int numInProgress = 0;
        int numFailed = 0;
        int numNotStarted = 0;
        String testType = "Standard";

        Tuple divJobExecStudentScenarioStatusCounts = divJobExecStudentScenarioCounts.get(divJobExec.getDivJobExecId());

        if (divJobExecStudentScenarioStatusCounts != null) {
            numCompletedOeExcluded = divJobExecStudentScenarioStatusCounts.get("completedOeExcluded", BigDecimal.class).intValueExact();
            numCompletedOeOpExcluded = divJobExecStudentScenarioStatusCounts.get("completedOeOpExcluded", BigDecimal.class).intValueExact();
            numCompletedOeFtExcluded = divJobExecStudentScenarioStatusCounts.get("completedOeFtExcluded", BigDecimal.class).intValueExact();

            numCompleted = divJobExecStudentScenarioStatusCounts.get("completed", BigDecimal.class).intValueExact()
                    + numCompletedOeExcluded + numCompletedOeOpExcluded + numCompletedOeFtExcluded;

            numInProgress = divJobExecStudentScenarioStatusCounts.get("inProgress", BigDecimal.class).intValueExact();
            numFailed = divJobExecStudentScenarioStatusCounts.get("failed", BigDecimal.class).intValueExact();
            numNotStarted = divJobExecStudentScenarioStatusCounts.get("notStarted", BigDecimal.class).intValueExact();
            testType = divJobExecStudentScenarioStatusCounts.get("testType", String.class);
        }

        divJobExec.setNumCompleted(numCompleted);
        divJobExec.setNumInProgress(numInProgress);
        divJobExec.setNumFailed(numFailed);
        divJobExec.setNumNotStarted(numNotStarted);
        divJobExec.setHasStatsState(divJobExec.getPath().getPathStates().stream().anyMatch(ps -> ps.getState().getStateName().equalsIgnoreCase("STATS")));

        divJobExec.setNumStudentScenarios(numCompleted + numInProgress + numFailed + numNotStarted);
        divJobExec.setTestType(testType);

        if (divJobExec.getDivJobExecStatus().getStatus().equals("Completed")) {
            if (numCompletedOeExcluded > 0) {
                divJobExec.setDivJobExecStatus(divJobExecStatusService.findByDivJobExecStatusName(StatusNames.COMPLETED_OE_EXCLUDED.getName()).get());
            } else if (numCompletedOeOpExcluded > 0) {
                divJobExec.setDivJobExecStatus(divJobExecStatusService.findByDivJobExecStatusName(StatusNames.COMPLETED_OE_OP_EXCLUDED.getName()).get());
            } else if (numCompletedOeFtExcluded > 0) {
                divJobExec.setDivJobExecStatus(divJobExecStatusService.findByDivJobExecStatusName(StatusNames.COMPLETED_OE_FT_EXCLUDED.getName()).get());
            }
        }
    }

    /**
     * Get the list of battery units filtered by <code>environmentId</code> and <code>divJobExecId</code>.
     *
     * @param apiVersion    The version of the API to use
     * @param environmentId The environment Id
     * @param divJobExecId  The DIV Job Id
     * @return The Result containing List of StudentScenarios
     */
    @Transactional(readOnly = true)
    public Result getBatteryStudentScenarios(String apiVersion, Integer environmentId, Long divJobExecId) {
        Logger.info("event=\"Retrieving battery units.\", environmentId=\"{}\", divJobExecId=\"{}\"", environmentId,
                divJobExecId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        Optional<State> batteryState = stateService.findByStateName(DivStateName.BATTERY_STATS.name());
        List<StudentScenario> studentScenarios = studentScenarioService.findByDivJobExecId(divJobExecId, environmentId);
        Map<String, List<StudentScenario>> batteryStudentScenarios = studentScenarios.stream().collect(
                Collectors.groupingBy(ss -> ss.getBatteryStudentScenario().getBatteryUuid()));

        ArrayNode arrayNode = Json.newArray();
        for (Map.Entry<String, List<StudentScenario>> battery : batteryStudentScenarios.entrySet()) {
            ObjectNode objectNode = Json.newObject();
            Optional<StudentScenario> firstResult = battery.getValue().stream().findFirst();
            if (firstResult.isPresent()) {
                StudentScenario firstUnit = firstResult.get();
                BatteryStudentScenario batteryStudentScenario = firstUnit.getBatteryStudentScenario();
                objectNode.put("batteryStudentScenarioId", batteryStudentScenario.getBatteryStudentScenarioId());
                objectNode.put("batteryTestCode", batteryStudentScenario.getBatteryTestCode());
                objectNode.put("batteryUuid", batteryStudentScenario.getBatteryUuid());
                objectNode.put("environmentId", environmentId);
                if (batteryState.isPresent()) {
                    objectNode.put("stateId", batteryState.get().getStateId());
                }
                StudentScenarioResult ssrForBatteryState = null;
                int numberOfPreviousExecutions = 0;
                for (StudentScenario ss : battery.getValue()) {
                    List<StudentScenarioResult> studentScenarioResults = stdScenarioResultService
                            .findByStudentScenarioIdAndStateName(ss.getStudentScenarioId(),
                                    DivStateName.BATTERY_STATS.name());

                    Optional<StudentScenarioResult> maxVersionSSR = studentScenarioResults.stream().max(
                            Comparator.comparing(StudentScenarioResult::getVersion));
                    if (maxVersionSSR.isPresent()) {
                        ssrForBatteryState = maxVersionSSR.get();
                        numberOfPreviousExecutions = studentScenarioResults.size() - 1;
                        break;
                    }
                }
                if (ssrForBatteryState != null) {
                    objectNode.put("details", ssrForBatteryState.getResultDetails());
                    objectNode.put("studentScenarioId", ssrForBatteryState.getStudentScenarioId());

                }
                objectNode.put("numberOfPreviousExecutions", numberOfPreviousExecutions);
                Optional<StudentScenarioStatus> inProgressStatus = studentScenarioStatusService
                        .findByStudentScenarioStatusName(StatusNames.IN_PROGRESS.name());
                long runningUnits = 0;
                if (inProgressStatus.isPresent()) {
                    runningUnits = battery
                            .getValue()
                            .stream()
                            .filter(ss -> ss.getStudentScenarioStatusId().equals(
                                    inProgressStatus.get().getStudentScenarioStatusId())
                                    && DivStateName.BATTERY_STATS.name().equals(ss.getState().getStateName())).count();
                }

                String status = "Failed";
                if (ssrForBatteryState == null) {
                    status = "Not Started";
                } else if (runningUnits != 0) {
                    status = "In Progress";
                } else if (ssrForBatteryState.isSuccess()) {
                    status = "Completed";
                }
                objectNode.put("status", status);
            }
            ArrayNode units = Json.newArray();
            for (StudentScenario studentScenario : battery.getValue()) {
                ObjectNode unit = Json.newObject();
                Scenario testNavScenario = studentScenario.getTestnavScenario();
                unit.put("formCode", testNavScenario.getFormCode());
                unit.put("testSessionName", studentScenario.getTestSession().getSessionName());
                unit.put("unitUuid", studentScenario.getUuid());
                unit.put("testNavScenarioName", testNavScenario.getScenarioName());
                if (studentScenario.getEpenScenario() != null) {
                    unit.put("epenScenarioName", studentScenario.getEpenScenario().getScenarioName());
                }

                unit.put("status", studentScenario.getStudentScenarioStatus().getStatus());
                units.add(unit);
            }
            objectNode.putArray("batteryUnits").addAll(units);
            arrayNode.add(objectNode);
        }

        return successResult(arrayNode, "batteryStudentScenarios");
    }

    @Transactional(readOnly = true)
    public Result getBatteryUnits(final String apiVersion, final Integer environmentId, final Long divJobExecId,
                                  final Long batteryStudentScenarioId) {
        Logger.info("event=\"Retrieving battery units.\", environmentId=\"{}\", divJobExecId=\"{}\", "
                + "batteryStudentScenarioId= \"{}\"", environmentId, divJobExecId, batteryStudentScenarioId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        List<StudentScenario> studentScenarios = studentScenarioService.findByDivJobExecIdAndBatteryStudentScenarioId(
                divJobExecId, batteryStudentScenarioId, environmentId);

        studentScenarios.forEach(ss -> loadLazyFetchedEpenScenarioAndTestSession(ss));

        return successResult(studentScenarios, "studentScenarios");
    }

    /**
     * Get the list of Student Scenarios filtered by <code>environmentId</code> and <code>divJobExecId</code>.
     *
     * @param apiVersion    The version of the API to use
     * @param environmentId The environment Id
     * @param divJobExecId  The DIV Job Id
     * @return The Result containing List of StudentScenarios
     */
    @Transactional(readOnly = true)
    public Result getStudentScenarios(String apiVersion, Integer environmentId, Long divJobExecId) {
        Logger.info("event=\"Retrieving student scenarios.\", environmentId=\"{}\", divJobExecId=\"{}\"",
                environmentId, divJobExecId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        List<StudentScenario> studentScenarios = studentScenarioService.findByDivJobExecId(divJobExecId, environmentId);
        //Since ePENScenario is LAZY loaded and used to render data in front end.
        for (StudentScenario ss : studentScenarios) {
            loadLazyFetchedEpenScenarioAndTestSession(ss);
        }

        return successResult(studentScenarios, "studentScenarios");
    }

    /**
     * Get the Student Scenario filtered by <code>environmentId</code> ,<code>divJobExecId</code> and
     * <code>studentScenarioId</code>.
     *
     * @param apiVersion        The version of the API to use
     * @param environmentId     The environment Id
     * @param divJobExecId      The DIV job Id
     * @param studentScenarioId The Student Scenario Id
     * @return The Result containing List of StudentScenarioResult
     */
    @Transactional(readOnly = true)
    public Result getStudentScenarioResult(String apiVersion, Integer environmentId, Long divJobExecId,
                                           Long studentScenarioId) {
        Logger.info("event=\"Retrieving student scenario results.\", environmentId=\"{}\", divJobExecId=\"{}\", "
                + "studentScenarioId=\"{}\"", environmentId, divJobExecId, studentScenarioId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<StudentScenario> studentScenario = studentScenarioService.findByStudentScenarioId(studentScenarioId,
                environmentId, divJobExecId);

        if (studentScenario.isPresent()) {
            StudentScenario sScenario = studentScenario.get();
            DivJobSetupUtil.initializeLazyFetchedData(studentScenarioService, sScenario);
            List<StudentScenarioResult> studentScenarioResult = stdScenarioResultService
                    .findByStudentScenarioId(studentScenarioId);

            // Getting StudentScenarioResult which has highest version number
            final List<StudentScenarioResult> ssResult = new ArrayList<>();
            Map<Integer, List<StudentScenarioResult>> collect = studentScenarioResult.stream().collect(
                    Collectors.groupingBy(StudentScenarioResult::getStateId));
            for (Map.Entry<Integer, List<StudentScenarioResult>> entry : collect.entrySet()) {
                Optional<StudentScenarioResult> resultOptional = entry.getValue().stream()
                        .max(Comparator.comparingInt(StudentScenarioResult::getVersion));
                if (resultOptional.isPresent()) {
                    ssResult.add(resultOptional.get());
                }
            }

            // Getting List of all states
            List<State> states = sScenario.getDivJobExec().getPath().getPathStates().stream()
                    .sorted(Comparator.comparing(PathState::getPathStateOrder))
                    .map(PathState::getState).collect(Collectors.toList());

            Map<Integer, StudentScenarioResult> stateIdToSSR = new HashMap<>();
            for (StudentScenarioResult ssr : ssResult) {
                ssr.setResultDetails(removeAttributes(ssr.getResultDetails()));
                stateIdToSSR.put(ssr.getState().getStateId(), ssr);
            }

            ObjectNode data = Json.newObject();
            List<StudentScenarioResult> result = new ArrayList<>();
            for (State state : states) {
                if ("BATTERY_STATS".equals(state.getStateName()) && sScenario.getBatteryStudentScenario() != null) {
                    Long batteryStudentScenarioId = sScenario.getBatteryStudentScenario()
                            .getBatteryStudentScenarioId();
                    List<StudentScenario> studentScenarios = studentScenarioService
                            .findByDivJobExecIdAndBatteryStudentScenarioId(divJobExecId, batteryStudentScenarioId,
                                    environmentId);
                    StudentScenarioResult ssrForBatteryState = null;
                    for (StudentScenario ss : studentScenarios) {
                        List<StudentScenarioResult> studentScenarioResults = stdScenarioResultService
                                .findByStudentScenarioIdAndStateName(ss.getStudentScenarioId(),
                                        DivStateName.BATTERY_STATS.name());

                        Optional<StudentScenarioResult> maxVersionSSR = studentScenarioResults.stream().max(
                                Comparator.comparing(StudentScenarioResult::getVersion));
                        if (maxVersionSSR.isPresent()) {
                            ssrForBatteryState = maxVersionSSR.get();
                            break;
                        }
                    }

                    if (ssrForBatteryState != null && !stateIdToSSR.containsKey(ssrForBatteryState.getStateId())) {
                        stateIdToSSR.put(ssrForBatteryState.getStateId(), ssrForBatteryState);
                    }
                }
                if (stateIdToSSR.keySet().contains(state.getStateId())) {
                    result.add(stateIdToSSR.get(state.getStateId()));
                } else {
                    StudentScenarioResult ssr = new StudentScenarioResult();
                    ssr.setStudentScenario(sScenario);
                    ssr.setState(state);
                    ssr.setStateId(state.getStateId());
                    result.add(ssr);
                }
            }

            Optional<DivJobExec> divJobExec = divJobExecService.findByDivJobExecId(divJobExecId);

            if (divJobExec.isPresent()) {
                DivJobExec dje = divJobExec.get();

                List<Long> divJobExecIds = new ArrayList<>();
                divJobExecIds.add(divJobExecId);


                List<Tuple> ssscr = divJobExecService.getDivJobStudentScenarioStatusCounts(divJobExecIds);

                Map<Long, Tuple> divJobExecStudentScenarioCounts = new HashMap<>();

                for (Tuple sssc : ssscr) {
                    divJobExecStudentScenarioCounts.put(divJobExecId, sssc);
                }

                updateDivJobCounts(divJobExecStudentScenarioCounts, dje);

                JsonNode divJobExecJson = Json.toJson(dje);

                data.set(DIV_JOB_EXEC, divJobExecJson);

            }
            JsonNode studentScenarioResultJson = Json.toJson(result);
            data.set(STUDENT_SCENARIO_RESULTS, studentScenarioResultJson);
            return successResult(data);
        } else {
            return failResult(NOT_FOUND, "Unable to find the requested student scenario.");
        }
    }

    private void loadLazyFetchedEpenScenarioAndTestSession(StudentScenario studentScenario) {
        Optional<StudentScenario> ssResult = studentScenarioService.findByStudentScenarioId(studentScenario.getStudentScenarioId());
        if (ssResult.isPresent()) {
            StudentScenario ss = ssResult.get();
            ss.getTestSession().getScopeCode(); //to load test session details
            if (ss.getEpenScenario() != null) {
                Scenario ePenScenario = ss.getEpenScenario();
                ePenScenario.getScenario(); //to load ePEN scenario
                studentScenario.setEpenScenario(ePenScenario);
            }
        }
    }

    /**
     * Get the Student Scenario Result history filtered by <code>environmentId</code> ,<code>divJobExecId</code> ,
     * <code>studentScenarioId</code> and <code>stateId</code>.
     *
     * @param apiVersion        The version of the API to use
     * @param environmentId     The environment Id
     * @param divJobExecId      The div Job Id
     * @param studentScenarioId The Student Scenario Id
     * @param stateId           The state Id
     * @return The Result containing List of StudentScenarioResultHistory
     */
    @Transactional(readOnly = true)
    public Result getStudentScenarioResultHistory(final String apiVersion, final Integer environmentId,
                                                  final Long divJobExecId, final Long studentScenarioId, final Integer stateId) {
        Logger.info("event=\"Retrieving student scenario result history.\", environmentId=\"{}\", "
                        + "divJobExecId=\"{}\", studentScenarioId=\"{}\", stateId=\"{}\"", environmentId, divJobExecId,
                studentScenarioId, stateId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        List<StudentScenarioResult> studentScenarioResultHistory;
        Optional<State> stateOptional = stateService.findByStateId(stateId);

        if (stateOptional.isPresent() && DivStateName.BATTERY_STATS.name().equals(stateOptional.get().getStateName())) {
            studentScenarioResultHistory = stdScenarioResultService.findByStudentScenarioIdAndStateIdFromBattery(
                    studentScenarioId, stateId, environmentId);
        } else {
            studentScenarioResultHistory = stdScenarioResultService.findByStudentScenarioIdAndStateId(
                    studentScenarioId, stateId, environmentId);
        }
        studentScenarioResultHistory.forEach(result -> result.setResultDetails(removeAttributes(result
                .getResultDetails())));

        return successResult(studentScenarioResultHistory, "studentScenarioResults");
    }

    /**
     * Removes the provided JSON attributes from the given JSON string.
     *
     * @param jsonString The JSON String from which attributes to be removed.
     * @return The JSON string without provided keys
     */
    private String removeAttributes(String jsonString) {
        ObjectNode resultJson = (ObjectNode) Json.parse(jsonString);
        for (String key : REMOVED_KEYS) {
            if (resultJson.findValue(key) != null) {
                resultJson.remove(key);
            }
        }
        return resultJson.toString();
    }

    /**
     * Get the TestNav scenario associated with the given student scenario.
     *
     * @param apiVersion        The version of the API to use.
     * @param environmentId     The environment ID.
     * @param divJobExecId      The DIV job exec ID.
     * @param studentScenarioId The student scenario ID.
     * @return A Result containing the TestNav scenario.
     */
    @Transactional(readOnly = true)
    public Result getTestNavScenario(final String apiVersion, final int environmentId, final long divJobExecId,
                                     final long studentScenarioId) {
        Logger.info("event=\"Retrieving TestNav scenario.\", scenarioId=\"{}\"", studentScenarioId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<Scenario> scenarioOptional = scenarioService.findTestNavScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);
        if (scenarioOptional.isPresent()) {
            ObjectNode returnData = Json.newObject();
            ObjectNode scenarioJson = (ObjectNode) Json.toJson(scenarioOptional.get());

            ObjectNode scenarioContent = (ObjectNode) Json.parse(scenarioOptional.get().getScenario());

            if (scenarioOptional.get().getTestmapDetail() != null) {
                scenarioContent.put("testMapId", scenarioOptional.get().getTestmapDetail().getTestmapId());
                scenarioContent.put("testMapName", scenarioOptional.get().getTestmapDetail().getTestmapName());
                scenarioContent.put("testMapVersion", scenarioOptional.get().getTestmapDetail().getTestmapVersion());
            }

            if (scenarioOptional.get().getPreviewerTenant() != null
                    && scenarioOptional.get().getPreviewerTenant().getPreviewer() != null) {
                scenarioContent
                        .put("previewerUrl", scenarioOptional.get().getPreviewerTenant().getPreviewer().getUrl());
                scenarioContent.put("tenantId", scenarioOptional.get().getPreviewerTenant().getTenantId());
            }

            // The reason for removing and then adding the item responses is so that the previously added information
            // is closer to the top for readability.
            JsonNode itemResponses = scenarioContent.get("itemResponses");
            scenarioContent.remove("itemResponses");
            scenarioContent.set("itemResponses", itemResponses);

            scenarioJson.set("scenarioContent", scenarioContent);

            returnData.set("scenario", scenarioJson);
            return successResult(returnData);
        } else {
            return failResult(NOT_FOUND,
                    "Unable to find a TestNav scenario associated with the given student scenario.");
        }
    }

    /**
     * Get the ePEN scenario associated with the given student scenario.
     *
     * @param apiVersion        The version of the API to use.
     * @param environmentId     The environment ID.
     * @param divJobExecId      The DIV job exec ID.
     * @param studentScenarioId The student scenario ID.
     * @return A Result containing the TestNav scenario.
     */
    @Transactional(readOnly = true)
    public Result getEpenScenario(final String apiVersion, final int environmentId, final long divJobExecId,
                                  final long studentScenarioId) {
        Logger.info("event=\"Retrieving ePEN scenario.\", scenarioId=\"{}\"", studentScenarioId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<Scenario> scenarioOptional = scenarioService.findEpenScenarioByStudentScenarioId(environmentId,
                divJobExecId, studentScenarioId);
        if (scenarioOptional.isPresent()) {
            ObjectNode returnData = Json.newObject();
            ObjectNode scenarioJson = (ObjectNode) Json.toJson(scenarioOptional.get());

            ObjectNode scenarioContent = (ObjectNode) Json.parse(scenarioOptional.get().getScenario());
            if (scenarioContent.has("inputData")) {
                scenarioContent = (ObjectNode) scenarioContent.get("inputData").get(0);
            }

            scenarioContent.remove("uin");
            scenarioContent.remove("uuid");

            ArrayNode scenarioData = (ArrayNode) scenarioContent.get("data");
            for (JsonNode item : scenarioData) {
                ((ObjectNode) item).remove("responseState");
            }

            scenarioJson.set("scenarioContent", scenarioContent);

            returnData.set("scenario", scenarioJson);
            return successResult(returnData);
        } else {
            return failResult(NOT_FOUND, "Unable to find an ePEN scenario associated with the given student scenario.");
        }
    }

    /**
     * Get the List of {@link DbConnection} filtered by <code>dbConnectionId</code>.
     *
     * @param apiVersion     The version of the API to use
     * @param dbConnectionId The dbConnectionId
     * @return The Result containing List of {@link DbConnection}
     */
    @Transactional(readOnly = true)
    public Result getDbConnectionById(final String apiVersion, final Integer dbConnectionId) {
        Logger.info("event=\"Retrieving Database Connection.\", dbConnectionId=\"{}\"", dbConnectionId);

        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName())) {
            Optional<DbConnection> dbConnectionResult = dbConnectionservice.findByDbConnectionId(dbConnectionId);
            if (dbConnectionResult.isPresent()) {
                DbConnection dbConnection = dbConnectionResult.get();

                DbConnectionDto result = new DbConnectionDto();
                result.setDbConnectionId(dbConnection.getDbConnectionId());
                result.setHost(dbConnection.getHost());
                result.setDbName(dbConnection.getDbName());
                result.setDbUser(dbConnection.getDbUser());
                result.setDbPass(dbConnection.getDbPass());
                result.setPort(dbConnection.getPort());

                return successResult(result, "dbConnection");
            }
            return failResult(NOT_FOUND, "Database connection not exist in database for dbConnectionId = "
                    + dbConnectionId);
        } else {
            return accessDeniedResult();
        }

    }

    /**
     * Get the List of {@link DbConnection}.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing List of {@link DbConnection}
     */
    @Transactional(readOnly = true)
    public Result getDbConnections(final String apiVersion) {
        Logger.info("event=\"Retrieving all Database Connections\"");
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName())) {
            return successResult(dbConnectionservice.findAll(), "dbConnections");
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Post the new instance of {@link DbConnection} to save into database.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing {@link DbConnection}
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result saveDbConnection(final String apiVersion) {
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.CREATE,
                Module.SYSTEM_VALIDATION.getName())) {
            JsonNode body = request().body().asJson();
            DbConnectionDto dto = Json.fromJson(body, DbConnectionDto.class);
            DbConnection dbConnection = new DbConnection();
            dbConnection.setHost(dto.getHost());
            dbConnection.setDbName(dto.getDbName());
            dbConnection.setDbUser(dto.getDbUser());
            dbConnection.setDbPass(dto.getDbPass());
            dbConnection.setPort(dto.getPort());
            dbConnectionservice.save(dbConnection);
            return successResult(dbConnection, "dbConnection");
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Updates the {@link DbConnection}.
     *
     * @param apiVersion     The version of the API to use
     * @param dbConnectionId The dbConnectionId
     * @return The Result containing {@link DbConnection}
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateDbConnection(final String apiVersion, final Integer dbConnectionId) {
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName())) {
            JsonNode body = request().body().asJson();
            DbConnectionDto dbConnection = Json.fromJson(body, DbConnectionDto.class);
            Optional<DbConnection> dbConnectionResult = dbConnectionservice.findByDbConnectionId(dbConnectionId);
            if (!(dbConnection.getDbPass().equals(dbConnectionResult.get().getDbPass())) || !(dbConnection.getPort().equals(dbConnectionResult.get().getPort()))) {
                Map<String, HikariDataSource> connections = connectionPoolManager.getMapOfSqlConnectionPools();
                Map<String, MongoClient> mongoClients = connectionPoolManager.getMapOfMongoConnectionPools();
                String key = dbConnection.getHost() + "," + dbConnection.getDbName() + "," + dbConnection.getDbUser();
                HikariDataSource dataSource = connections.get(key);
                MongoClient mongoClient = mongoClients.get(key);
                if (dataSource != null) {
                    dataSource.setJdbcUrl("jdbc:mysql://" + dbConnection.getHost() + ":" + dbConnection.getPort() + "/" + dbConnection.getDbName());
                    dataSource.setPassword(dbConnection.getDbPass());
                }
                if (mongoClient != null) {
                    mongoClient.close();
                }
            }
            if (dbConnectionResult.isPresent()) {
                DbConnection dbCon = dbConnectionResult.get();
                dbCon.setHost(dbConnection.getHost());
                dbCon.setDbName(dbConnection.getDbName());
                dbCon.setDbUser(dbConnection.getDbUser());
                dbCon.setDbPass(dbConnection.getDbPass());
                dbCon.setPort(dbConnection.getPort());
                return successResult(dbCon, "dbConnection");
            } else {
                Logger.error("event=\"Database connection does not exist in database.\", dbConnectionId=\"{}\"",
                        dbConnectionId);
                return failResult(NOT_FOUND, "Database connection does not exist in database for dbConnectionId = "
                        + dbConnectionId);
            }
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Get the List of {@link ApiConnection}.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing the list of {@link ApiConnection}
     */
    @Transactional(readOnly = true)
    public Result getApiConnections(final String apiVersion) {
        Logger.info("event=\"Retrieving all API Connections\"");
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName())) {
            return successResult(apiConnectionService.findAll(), "apiConnections");
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Get the {@link ApiConnection} filtered by <code>apiConnectionId</code>.
     *
     * @param apiVersion      The version of the API to use
     * @param apiConnectionId The apiConnectionId
     * @return The Result containing {@link ApiConnection}
     */
    @Transactional(readOnly = true)
    public Result getApiConnectionById(final String apiVersion, final Integer apiConnectionId) {
        Logger.info("event=\"Retrieving API Connection.\", apiConnectionId=\"{}\"", apiConnectionId);

        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName())) {

            Optional<ApiConnection> apiConnection = apiConnectionService.findByApiConnectionId(apiConnectionId);
            if (apiConnection.isPresent()) {
                return successResult(apiConnection.get(), "apiConnection");
            }
            return failResult(NOT_FOUND, "Database connection not exist in database for dbConnectionId = "
                    + apiConnectionId);
        } else {
            return accessDeniedResult();
        }

    }

    /**
     * Post the new {@link ApiConnection} to save into database.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing {@link ApiConnection}
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result saveApiConnection(final String apiVersion) {
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.CREATE,
                Module.SYSTEM_VALIDATION.getName())) {
            JsonNode body = request().body().asJson();
            ApiConnection apiConnection = Json.fromJson(body, ApiConnection.class);
            apiConnectionService.save(apiConnection);
            return successResult(apiConnection, "apiConnection");
        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Updates the {@link ApiConnection} filtered bye <code>apiConnectionId</code>.
     *
     * @param apiVersion      The version of the API to use
     * @param apiConnectionId The apiConnectionId
     * @return The Result containing {@link ApiConnection}
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateApiConnection(final String apiVersion, final Integer apiConnectionId) {
        if (sessionData().hasAnyEnvironmentLevelPermission(ENVIRONMENTS, AccessType.UPDATE,
                Module.SYSTEM_VALIDATION.getName())) {
            JsonNode body = request().body().asJson();
            ApiConnection apiConnectionData = Json.fromJson(body, ApiConnection.class);
            Optional<ApiConnection> apiConnection = apiConnectionService.findByApiConnectionId(apiConnectionId);
            if (apiConnection.isPresent()) {
                ApiConnection apiCon = apiConnection.get();
                apiCon.setApiUrl(apiConnectionData.getApiUrl());
                apiCon.setSecret(apiConnectionData.getSecret());
                apiCon.setToken(apiConnectionData.getToken());
                return successResult(apiCon, "apiConnection");
            } else {
                Logger.error("event=\"API connection does not exist in database.\", apiConnectionId=\"{}\"",
                        apiConnectionId);
                return failResult(NOT_FOUND, "API connection does not exist in database for apiConnectionId = "
                        + apiConnectionId);
            }

        } else {
            return accessDeniedResult();
        }
    }

    /**
     * Get the List of {@link StudentScenarioResultComment}.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing List of {@link StudentScenarioResultComment}
     */
    @Transactional(readOnly = true)
    public Result getComments(final String apiVersion, final Integer environmentId, final Long studentScenarioResultId) {

        Logger.info("event=\"Retrieving all comments for studentScenarioResultId {}\"", studentScenarioResultId);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(studentScenarioResultCommentService.findAll(environmentId, studentScenarioResultId),
                "studentScenarioResultComments");
    }

    /**
     * Get the List of {@link StudentScenarioResultCommentCategory}.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing List of {@link StudentScenarioResultCommentCategory}
     */
    @Transactional(readOnly = true)
    public Result getStudentScenarioCommentCategories(final String apiVersion, final Integer environmentId) {
        Logger.info("event=\"Retrieving all comment categories\"");

        try {
            enforceEnvironmentLevelPermission(DIV_JOB, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(studentScenarioResultCommentCategoryService.findAll(),
                "studentScenarioResultCommentCategories");
    }

    /**
     * Get the List of {@link ExternalDefectRepository}.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing List of {@link ExternalDefectRepository}
     */
    @Transactional(readOnly = true)
    public Result getExternalDefectRepositories(final String apiVersion, final Integer environmentId) {
        Logger.info("event=\"Retrieving all external defect repositories\"");

        try {
            enforceEnvironmentLevelPermission(DIV_JOB, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(externalDefectRepositoryService.findAll(), "externalDefectRepositories");
    }

    /**
     * Post a new instance of {@link StudentScenarioResultComment} to save into the database.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing {@link StudentScenarioResultComment}
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result saveComment(final String apiVersion, final Integer environmentId, final Long studentScenarioResultId) {

        try {
            enforceEnvironmentLevelPermission(DIV_JOB, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        JsonNode body = request().body().asJson();
        if (body == null) {
            return failResult(BAD_REQUEST, "Failed to parse request body as json (null)");
        }
        try {
            StudentScenarioResultComment ssrc = Json.fromJson(body, StudentScenarioResultComment.class);
            ssrc.setStudentScenarioResultId(studentScenarioResultId);
            ssrc.setUserId(sessionData().getUserId());
            studentScenarioResultCommentService.create(ssrc);
            return successResult(ssrc, "ssrc");
        } catch (Exception e) {
            Logger.error("event= \"Failed to parse request body as json\"", e);
            return failResult(BAD_REQUEST, "Failed to parse request body as json: " + body.toString());
        }

    }

    /**
     * Update an instance of {@link StudentScenarioResultComment} in the database.
     *
     * @param apiVersion The version of the API to use
     * @return The Result containing {@link StudentScenarioResultComment}
     */
    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateComment(final String apiVersion, final Integer environmentId,
                                final Long studentScenarioResultId, final Long studentScenarioResultCommentId) {

        try {
            enforceEnvironmentLevelPermission(DIV_JOB, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode body = request().body().asJson();
        if (body == null) {
            return failResult(BAD_REQUEST, "Failed to parse request body as json (null)");
        }
        try {
            StudentScenarioResultComment ssrc = Json.fromJson(body, StudentScenarioResultComment.class);

            Optional<StudentScenarioResultComment> studentScenarioResultCommentResult = studentScenarioResultCommentService
                    .findByStudentScenarioResultCommentId(environmentId, studentScenarioResultCommentId);

            if (studentScenarioResultCommentResult.isPresent()) {
                StudentScenarioResultComment studentScenarioResultComment = studentScenarioResultCommentResult.get();

                studentScenarioResultComment.setStudentScenarioResultCommentCategory(ssrc
                        .getStudentScenarioResultCommentCategory());
                studentScenarioResultComment.setResultComment(ssrc.getResultComment());
                studentScenarioResultComment.setExternalDefectRepository(ssrc.getExternalDefectRepository());
                studentScenarioResultComment.setExternalDefectId(ssrc.getExternalDefectId());

                return successResult(studentScenarioResultComment, "studentScenarioResultComment");

            } else {
                Logger.error(
                        "event=\"Student Scenario Result Comment does not exist in database.\", studentScenarioResultCommentId=\"{}\"",
                        studentScenarioResultCommentId);
                return failResult(NOT_FOUND,
                        "Student Scenario Result Comment does not exist in database for studentScenarioResultCommentId = "
                                + studentScenarioResultCommentId);
            }
        } catch (Exception e) {
            Logger.error("event= \"Failed to parse request body as json\"", e);
            return failResult(BAD_REQUEST, "Failed to parse request body as json: " + body.toString());
        }
    }

    /**
     * Get the status of currently selected div job.
     *
     * @param apiVersion   The version of the API to use.
     * @param divJobExecId The DIV job execution ID.
     * @return A Result containing the status of selected div job and the environment name, environment id to which the
     * div job belongs.
     */
    @Transactional
    public Result getDivJobStatus(final String apiVersion, final Long divJobExecId) {
        Logger.info("event=\"Request to get the status of the DIV job.\",divJobExecId=\"{}\"", divJobExecId);
        Optional<DivJobExec> divJobExec = divJobExecService.findStatusByDivJobExecId(divJobExecId);

        if (divJobExec.isPresent()) {
            DivJobExec dje = divJobExec.get();
            JsonNode divJobExecJson = Json.toJson(dje.getDivJobExecStatus().getStatus());
            JsonNode environmentExecJson = Json.toJson(dje.getEnvironment().getName());
            ObjectNode data = Json.newObject();
            data.set(STATUS, divJobExecJson);
            data.set(ENVIRONMENT, environmentExecJson);
            return successResult(data);
        } else {
            return failResult(NOT_FOUND, "Unable to find the requested divJobExecId.");
        }
    }
}
