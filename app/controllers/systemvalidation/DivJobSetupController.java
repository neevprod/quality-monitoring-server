package controllers.systemvalidation;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.pearson.itautomation.panext.studentsession.SessionDetails;
import com.pearson.itautomation.panext.studentsession.SessionInfo;
import com.pearson.itautomation.panext.studentsession.StudentTestSession;
import com.pearson.itautomation.panext.studentsession.exception.FrontendDatabaseException;
import com.pearson.itautomation.panext.studentsession.util.StudentTestSessionExtractor;
import com.pearson.itautomation.testmaps.models.Testmap;
import com.pearson.itautomation.testmaps.models.enums.ItemStatus;
import com.pearson.itautomation.testscenariocreator.inputs.ItemResponsePool;
import com.pearson.itautomation.testscenariocreator.inputs.T3TestMapItem;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import com.typesafe.config.Config;
import controllers.SecureController;
import global.Json1MbBodyParser;
import global.exceptions.QaApplicationPermissionException;
import jobs.JobProtocol;
import jobs.systemvalidation.DivJobSetupJobStarter;
import models.JobExecution;
import models.qtivalidation.PreviewerTenant;
import models.systemvalidation.*;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import services.JobExecutionService;
import services.qtivalidation.PreviewerTenantService;
import services.systemvalidation.*;
import util.EncryptorDecryptor;
import util.SessionData;
import util.systemvalidation.*;
import util.systemvalidation.DivJobSetupUtil.TestDetail;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Gert Selis
 */
public class DivJobSetupController extends SecureController {
    private static final String DIV_JOB_EXECUTIONS = "divJobExecutions";
    private final DivJobSetupUtil divJobSetupUtil;
    private final EnvironmentService environmentService;
    private final DivJobExecService divJobExecService;
    private final PathService pathService;
    private final TestSessionService testSessionService;
    private final StudentReservationService studentReservationService;
    private final long studentReservationTimeOut;
    private final long actorAskTimeout;
    private final ActorRef jobRunnerActor;
    private final DivJobSetupJobStarter.Factory divJobSetupStarterFactory;
    private final BatteryStudentScenarioService batteryStudentScenarioService;
    private final String token;
    private final String secret;
    private final Provider<EpenScenarioCreator> epenScenarioCreatorProvider;
    private final ScenarioService scenarioService;
    private final ScenarioTypeService scenarioTypeService;
    private final BackendData.Factory backendDataFactory;
    private final ScenarioClassService scenarioClassService;
    private final TestmapDetailService testmapDetailService;
    private final PreviewerTenantService previewerTenantService;
    private final JobExecutionService jobExecutionService;
    private final JPAApi jpaApi;

    @Inject
    protected DivJobSetupController(DivJobSetupUtil divJobSetupUtil, SessionData.Factory sessionDataFactory,
                                    EnvironmentService environmentService, DivJobExecService divJobExecService, PathService pathService,
                                    TestSessionService testSessionService, StudentReservationService studentReservationService,
                                    Config config, @Named("jobRunnerActor") ActorRef jobRunnerActor,
                                    DivJobSetupJobStarter.Factory divJobSetupStarterFactory,
                                    BatteryStudentScenarioService batteryStudentScenarioService,
                                    Provider<EpenScenarioCreator> epenScenarioCreatorProvider,
                                    ScenarioService scenarioService, ScenarioTypeService scenarioTypeService,
                                    BackendData.Factory backendDataFactory, ScenarioClassService scenarioClassService,
                                    TestmapDetailService testmapDetailService, PreviewerTenantService previewerTenantService,
                                    JobExecutionService jobExecutionService, JPAApi jpaApi) {
        super(sessionDataFactory);
        this.divJobSetupUtil = divJobSetupUtil;
        this.environmentService = environmentService;
        this.divJobExecService = divJobExecService;
        this.pathService = pathService;
        this.testSessionService = testSessionService;
        this.studentReservationService = studentReservationService;
        this.studentReservationTimeOut = config.getDuration("application.studentReservationTimeOut").getSeconds();
        this.actorAskTimeout = config.getDuration("application.actorAskTimeout").toMillis();
        this.jobRunnerActor = jobRunnerActor;
        this.divJobSetupStarterFactory = divJobSetupStarterFactory;
        this.batteryStudentScenarioService = batteryStudentScenarioService;
        this.token = config.getString("application.previewerApiToken");
        this.secret = EncryptorDecryptor.getDecryptedConfigurationString(config, "application.previewerApiSecret");
        this.epenScenarioCreatorProvider = epenScenarioCreatorProvider;
        this.scenarioService = scenarioService;
        this.scenarioTypeService = scenarioTypeService;
        this.backendDataFactory = backendDataFactory;
        this.scenarioClassService = scenarioClassService;
        this.testmapDetailService = testmapDetailService;
        this.jobExecutionService = jobExecutionService;
        this.previewerTenantService = previewerTenantService;
        this.jpaApi = jpaApi;
    }

    /**
     * Retrieves the DIV job execution Path Id for the selected state Ids
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the divjobExec Path Id
     */
    @Transactional(readOnly = true)
    public Result getDivJobExecPathId(final String apiVersion) {
        Logger.info("event=\"Request to get divJobExecPath Id.\"");
        String[] selectedStatesIds = request().queryString().get("selectedStatesIds");
        String[] notSelectedStatesIds = request().queryString().get("notSelectedStatesIds");
        List<Integer> notSelectedIds = null;
        List<Integer> selectedIds = Stream.of(selectedStatesIds).map(Integer::parseInt).collect(Collectors.toList());
        if (null != notSelectedStatesIds && notSelectedStatesIds.length > 0) {
            notSelectedIds = Stream.of(notSelectedStatesIds).map(Integer::parseInt).collect(Collectors.toList());
        }
        int pathId = pathService.findPathId(selectedIds, notSelectedIds);
        if (pathId == 0) {
            return failResult(NOT_FOUND, "Path Id not found for selected states " + pathId);
        }
        return successResult(pathId, "path");
    }

    /**
     * Retrieves the DIV jov execution Path States for selected test Type
     *
     * @param apiVersion The version of the API to use
     * @param testType   Default or battery
     * @return A Result containing the divjobExec Path States
     */
    @Transactional(readOnly = true)
    public Result getDivJobExecPathStates(final String apiVersion, final String testType) {
        Logger.info("event=\"Request to get divJobExecPathStates for the given testType.\"", testType);
        int testTypeId = pathService.findByTestTypeName(testType);
        List<Object[]> statesList = pathService.findByTestTypeId(testTypeId);
        ArrayNode pathStates = Json.newArray();
        ObjectNode node;
        if (!statesList.isEmpty()) {
            for (Object[] obj : statesList) {
                node = Json.newObject();
                node.put("key", ((Number) obj[0]).intValue());
                node.put("value", (obj[1].toString()));
                pathStates.add(node);
            }
        }
        return successResult(pathStates, "pathStates");
    }

    @Transactional
    public Result getScopeTreePaths(final String apiVersion, final int environmentId, final String testType) {
        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        final Optional<Environment> environment = environmentService.findById(environmentId);

        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return failResult(NOT_FOUND, "Can not find Environment for the environmentId " + environmentId);
        }

        StudentTestSessionExtractor stse = divJobSetupUtil
                .getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());

        try {
            if ("Battery".equals(testType)) {
                return successResult(stse.getBatteryScopeTreePaths(), "scopeTreePaths");
            } else {
                return successResult(stse.getScopeTreePaths(), "scopeTreePaths");
            }
        } catch (FrontendDatabaseException e) {
            return failResult(NOT_FOUND, "Failed to retrieve data from the PANext Front End Database. Please verify the Front End Database connection details.");
        }
    }

    @Transactional
    public Result getTestCodes(final String apiVersion, final int environmentId, final String testType,
                               final String scopeTreePath) {
        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        final Optional<Environment> environment = environmentService.findById(environmentId);

        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return failResult(NOT_FOUND, "Can not find Environment for the environmentId " + environmentId);
        }

        StudentTestSessionExtractor stse = divJobSetupUtil
                .getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());

        try {
            if ("Battery".equals(testType)) {
                List<TestDetail> testDetails = stse.getBatteryTestCodes(StringUtils.replace(scopeTreePath, "|", "/"))
                        .stream().map(divJobSetupUtil::createTestDetail).collect(Collectors.toList());

                return successResult(testDetails, "testDetails");
            } else {
                List<TestDetail> testDetails = stse.getTestCodes(StringUtils.replace(scopeTreePath, "|", "/")).stream()
                        .map(divJobSetupUtil::createTestDetail).collect(Collectors.toList());

                return successResult(testDetails, "testDetails");
            }
        } catch (FrontendDatabaseException e) {
            return failResult(NOT_FOUND, "Failed to retrieve data from PANext Frontend Database");
        }
    }

    @Transactional
    public Result getTestSessionDetails(final String apiVersion, final int environmentId, final String testType,
                                        final String scopeTreePath, final String testCodes) {
        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        final List<String> testCodesList = Arrays.asList(testCodes.split("\\s*,\\s*"));
        final Optional<Environment> environment = environmentService.findById(environmentId);
        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return failResult(NOT_FOUND, "Can not find Environment for the environmentId " + environmentId);
        }

        StudentTestSessionExtractor stse = divJobSetupUtil
                .getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());
        SessionDetails sessionDetails;
        try {
            if ("Battery".equals(testType)) {
                sessionDetails = stse.getSessionDetailsForBatteryTests(StringUtils.replace(scopeTreePath, "|", "/"),
                        testCodesList);
            } else {
                sessionDetails = stse.getSessionDetails(StringUtils.replace(scopeTreePath, "|", "/"), testCodesList);
            }
        } catch (FrontendDatabaseException e) {
            return failResult(NOT_FOUND, "Failed to retrieve data from PANext Frontend Database");
        }

        MultiMap testSessionDetails = sessionDetails.getTestCodeSessionMap();

        @SuppressWarnings("unchecked")
        Set<String> formCodes = (Set<String>) testSessionDetails.keySet();
        List<TestDetail> testDetails = new ArrayList<>();
        for (String formCode : formCodes) {

            @SuppressWarnings("unchecked")
            List<SessionInfo> testSessions = (List<SessionInfo>) testSessionDetails.get(formCode);

            TestDetail testDetail = new TestDetail();
            testDetail.setTestCode(formCode);
            testDetail.setTestSessions(testSessions);
            testDetails.add(testDetail);
        }
        return successResult(testDetails, "testSessions");
    }

    public CompletionStage<Result> startGetTestFormsJob(final String apiVersion, final int environmentId,
                                                        final String testType, final String scopeTreePath, final String sessionIds, final String testCodeString) {
        Logger.info(
                "event=\"Request to start DIV job setup get forms job.\", environmentId=\"{}\", "
                        + "scopeTreePath=\"{}\", testCode=\"{}\", sessionIds=\"{}\"",
                environmentId, scopeTreePath, testCodeString, sessionIds);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(divJobSetupStarterFactory
                .create(environmentId, testType, scopeTreePath, sessionIds, testCodeString, sessionData().getUserId()));
        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                .thenApplyAsync(this::jobStartResult);
    }

    public CompletionStage<Result> checkGetTestFormsJob(final String apiVersion, final String jobId) {
        Logger.info("event=\"Request to check status of DIV job setup get forms job.\", jobId=\"{}\"", jobId);

        Pattern pattern = Pattern.compile("^divJobSetup_getForms_(\\d+)_\\d+$");
        Matcher matcher = pattern.matcher(jobId);

        // Check whether the given job ID is in the correct format.
        if (!matcher.matches()) {
            return CompletableFuture.completedFuture(failResult(BAD_REQUEST, "Invalid job ID."));
        }

        // Check whether the given job ID is associated with the logged in user.
        String userIdFromJobId = matcher.group(1);
        if (!sessionData().getUserId().toString().equals(userIdFromJobId)) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobCheckMessage jobCheckMessage = new JobProtocol.JobCheckMessage(jobId);

        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobCheckMessage, actorAskTimeout))
                .thenApplyAsync(this::jobCheckResult);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> startGetStudentScenariosJob(final String apiVersion, final int environmentId,
                                                               final String testType, final String scopeTreePath) {
        Logger.info("event=\"Request to start DIV job setup get student scenarios job.\", environmentId=\"{}\", "
                + "scopeTreePath=\"{}\"", environmentId, scopeTreePath);

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(divJobSetupStarterFactory
                .create(environmentId, testType, scopeTreePath, request().body().asJson(), sessionData().getUserId()));
        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                .thenApplyAsync(this::jobStartResult);
    }

    public CompletionStage<Result> checkGetStudentScenariosJob(final String apiVersion, final String jobId) {
        Logger.info("event=\"Request to check status of DIV job setup get student scenarios job.\", jobId=\"{}\"",
                jobId);

        Pattern pattern = Pattern.compile("^divJobSetup_getStudentScenarios_(\\d+)_\\d+$");
        Matcher matcher = pattern.matcher(jobId);

        // Check whether the given job ID is in the correct format.
        if (!matcher.matches()) {
            return CompletableFuture.completedFuture(failResult(BAD_REQUEST, "Invalid job ID."));
        }

        // Check whether the given job ID is associated with the logged in user.
        String userIdFromJobId = matcher.group(1);
        if (!sessionData().getUserId().toString().equals(userIdFromJobId)) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobCheckMessage jobCheckMessage = new JobProtocol.JobCheckMessage(jobId);

        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobCheckMessage, actorAskTimeout))
                .thenApplyAsync(this::jobCheckResult);
    }

    @Transactional
    @BodyParser.Of(Json1MbBodyParser.class)
    public Result submitStudentScenarios(final String apiVersion, final int environmentId, final String scopeTreePath,
                                         final int divJobPathId, final String testType) {
        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        final Optional<Environment> environment = environmentService.findById(environmentId);

        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return failResult(NOT_FOUND, "Can not find Environment for the environmentId " + environmentId);
        }

        final Optional<Path> pathResult = pathService.findByPathId(divJobPathId);
        final int testTypeId = pathService.findByTestTypeName(testType);

        if (!pathResult.isPresent()) {
            return failResult(NOT_FOUND, "Path with pathID not found: " + divJobPathId);
        }
        final Path path = pathResult.get();

        JsonNode body = request().body().asJson();

        DivJobExec divJobExec = new DivJobExec();

        divJobExec.setEnvironment(environment.get());
        divJobExec.setPath(path);
        divJobExec.setScopeTreePath(StringUtils.replace(scopeTreePath, "|", "/"));

        divJobExec.setSubmitUserId(sessionData().getUserId());
        divJobExec.setPriority(1);
        divJobExec.setDivJobExecStatusId(1); //statusId 1 = Not Started
        divJobExec.setDivJobExecTypeId(1); //divJobExecTypeId 1 = PANEXT
        divJobExec.setTestTypeId(testTypeId);
        try {
            if ("Battery".equalsIgnoreCase(testType)) {
                return createBatteryDivJob(body, environment.get(), scopeTreePath, divJobExec);
            }
            return createStandardDivJob(body, environment.get(), scopeTreePath, divJobExec);
        } catch (IOException e) {
            Logger.error("event=\"Failed to create Job\"");
            throw new RuntimeException(e);
        }
    }


    private Result createStandardDivJob(JsonNode body, Environment environment, String scopeTreePath, DivJobExec divJobExec) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, StudentScenario.class);
        List<StudentScenario> studentScenarios = mapper.readValue(body.toString(), type);
        boolean result = true;
        for (StudentScenario studentScenario : studentScenarios) {
            if (studentScenario.getUuid() == null) {
                result = false;
                break;
            }
        }
        if (!result) {
            return failResult(NOT_FOUND, "Some scenarios do not have a student assigned. Please either use a different session or remove some scenarios.");
        }


        String decodedScopeTreePath = StringUtils.replace(scopeTreePath, "|", "/");
        List<String> usedUuids = new ArrayList<>();
        StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.getFeMysqlConnection());
        try {
            String prevFormCode = "";
            String prevTestCode = "";
            List<StudentTestSession> studentTestSessions = new ArrayList<>();
            for (StudentScenario studentScenario : studentScenarios) {
                String testCode = studentScenario.getTestnavScenario().getTestCode();
                String currFormCode = studentScenario.getTestnavScenario().getFormCode();
                String currTestCode = studentScenario.getTestnavScenario().getTestCode();
                if (!prevFormCode.equals(currFormCode) || (!prevTestCode.equals(currTestCode))) {
                    prevFormCode = currFormCode;
                    prevTestCode = currTestCode;
                    studentTestSessions.addAll(stse.getStudTestSessionDetails(decodedScopeTreePath, testCode, currFormCode));
                }
                String uuid = studentScenario.getUuid();
                Optional<StudentTestSession> studentTestSession = studentTestSessions.stream().filter(s -> s.getStudentTestUuid().equals(uuid)).findFirst();
                if (!studentTestSession.isPresent()) {
                    usedUuids.add(uuid + " -> " + studentScenario.getTestnavScenario().getScenarioName());
                    continue;
                }
                TestSession testSession = constructTestSession(studentTestSession.get(), environment);
                studentScenario.setTestSession(testSession);
                updateStudentScenario(studentScenario, studentTestSession.get());
                divJobExec.addStudentScenario(studentScenario);
            }
        } catch (FrontendDatabaseException e) {
            return failResult(NOT_FOUND, "Failed to retrieve student test session details from PANext Frontend Database. " + e.getMessage());
        }
        if (!usedUuids.isEmpty()) {
            return failResult(BAD_REQUEST, "Following uuid(s) already used. Please try again to setup job." + Json.toJson(usedUuids));
        }
        List<String> uuids = divJobExec.getStudentScenarios().stream().map(StudentScenario::getUuid).collect(Collectors.toList());
        if (studentReservationService.isExpired(uuids, studentReservationTimeOut)) {
            return failResult(BAD_REQUEST, "Student reservation time has expired. Please submit job within " + studentReservationTimeOut + " minutes.");
        }

        divJobExecService.createDivJobExec(divJobExec);
        // To update studentScenarioId to StudentReservation table.
        for (StudentScenario studentScenario : divJobExec.getStudentScenarios()) {
            Optional<StudentReservation> studentReservation = studentReservationService
                    .findByUUID(studentScenario.getUuid());
            studentReservation.ifPresent(sr -> sr.setStudentScenarioId(studentScenario.getStudentScenarioId()));
        }
        return successResult(divJobExec, "divJob");
    }


    private Result createBatteryDivJob(JsonNode body, Environment environment, String scopeTreePath, DivJobExec divJobExec) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, BatteryStudentScenario.class);

        List<BatteryStudentScenario> batteryStudentScenarios = mapper.readValue(body.toString(), type);

        boolean result = true;
        for (BatteryStudentScenario batteryStudentScenario : batteryStudentScenarios) {
            if (batteryStudentScenario.getBatteryUuid() == null) {
                result = false;
                break;
            }
        }
        if (!result) {
            return failResult(NOT_FOUND, "Some scenarios do not have a student assigned. Please either use a different session or remove some scenarios.");
        }


        String decodedScopeTreePath = StringUtils.replace(scopeTreePath, "|", "/");
        List<String> usedUuids = new ArrayList<>();
        StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.getFeMysqlConnection());

        List<Integer> panTestSessionIds = new ArrayList<>();
        List<String> batteryTestCodes = new ArrayList<>();

        for (BatteryStudentScenario batteryStudentScenario : batteryStudentScenarios) {
            panTestSessionIds.addAll(batteryStudentScenario.getStudentScenarios().stream().map(s -> s.getTestSession().getPanTestSessionId()).map(Integer::parseInt).collect(Collectors.toList()));
            batteryTestCodes.addAll(Collections.singletonList(batteryStudentScenario.getBatteryTestCode()));
        }

        Map<String, List<StudentTestSession>> availableBatteryTestSessions = stse.getBatteryStudTestSessionDetails(decodedScopeTreePath, batteryTestCodes, panTestSessionIds);
        for (BatteryStudentScenario batteryStudentScenario : batteryStudentScenarios) {
            for (StudentScenario studentScenario : batteryStudentScenario.getStudentScenarios()) {
                //Checking if the student is already consumed.
                String uuid = studentScenario.getUuid();
                List<StudentTestSession> studentTestSessions = availableBatteryTestSessions.get(batteryStudentScenario.getBatteryUuid());
                Optional<StudentTestSession> studentTestSession = studentTestSessions.stream().filter(s -> s.getStudentTestUuid().equals(uuid)).findFirst();
                if (!studentTestSession.isPresent()) {
                    usedUuids.add(uuid + " -> " + studentScenario.getTestnavScenario().getScenarioName());
                    continue;
                }

                TestSession testSession = constructTestSession(studentTestSession.get(), environment);
                studentScenario.setTestSession(testSession);
                updateStudentScenario(studentScenario, studentTestSession.get());
                divJobExec.addStudentScenario(studentScenario);
            }
            //Checking student reservation time.
            List<String> uuids = batteryStudentScenario.getStudentScenarios().stream().map(StudentScenario::getUuid).collect(Collectors.toList());
            if (studentReservationService.isExpired(uuids, studentReservationTimeOut)) {
                return failResult(BAD_REQUEST, "Student reservation time has expired. Please submit job within " + studentReservationTimeOut + " minutes.");
            }
        }
        if (!usedUuids.isEmpty()) {
            return failResult(BAD_REQUEST, "Following uuid(s) already used. Please try again to setup div job." + Json.toJson(usedUuids));
        }

        for (BatteryStudentScenario batteryStudentScenario : batteryStudentScenarios) {
            batteryStudentScenarioService.create(batteryStudentScenario);
            for (StudentScenario studentScenario : batteryStudentScenario.getStudentScenarios()) {
                studentScenario.setBatteryStudentScenario(batteryStudentScenario);
            }
        }

        divJobExecService.createDivJobExec(divJobExec);

        // To update studentScenarioId in StudentReservation table.
        for (StudentScenario studentScenario : divJobExec.getStudentScenarios()) {
            Optional<StudentReservation> studentReservation = studentReservationService
                    .findByUUID(studentScenario.getUuid());
            studentReservation.ifPresent(sr -> sr.setStudentScenarioId(studentScenario.getStudentScenarioId()));
        }
        return successResult(divJobExec, "divJob");
    }

    private TestSession constructTestSession(final StudentTestSession studentTestSession, final Environment environment) {
        Optional<TestSession> testSessionResult = testSessionService.findByEnvironmentIdAndPANTestSessionId(environment, studentTestSession.getSessionId());
        TestSession testSession;
        if (testSessionResult.isPresent()) {
            testSession = testSessionResult.get();
        } else {
            testSession = new TestSession();
            testSession.setTestnavApplicationUrl(studentTestSession.getTn8URL());
            testSession.setTestnavWebserviceUrl(studentTestSession.getTestnavWebServiceURL());
            testSession.setTestnavClientSecret(studentTestSession.getTestnavClientSecret());
            testSession.setTestnavClientIdentifier(studentTestSession.getTestnavClientIdentifier());
            testSession.setTestnavVersion(studentTestSession.getTestnavVersion());
            testSession.setTestnavCustomerCode(studentTestSession.getTn8CustomerCode());
            testSession.setScopeCode(studentTestSession.getScopeCode());
            testSession.setSessionSealCodes(studentTestSession.getSessionSealCodes());
            testSession.setSessionName(studentTestSession.getSessionName());

            testSession.setSessionScheduledStartDate(studentTestSession.getSessionScheduledStartDate());
            testSession.setScheduleStartDate(null);
            testSession.setScheduleStartTime(null);
            testSession.setScheduleEndDate(null);
            testSession.setScheduleEndTime(null);
            testSession.setSessionStartDate(studentTestSession.getStartDate());
            testSession.setSessionStopDate(null);

            testSession.setSessionAvailableMonday(null);
            testSession.setSessionAvailableTuesday(null);
            testSession.setSessionAvailableWednesday(null);
            testSession.setSessionAvailableThursday(null);
            testSession.setSessionAvailableFriday(null);
            testSession.setSessionAvailableSaturday(null);
            testSession.setSessionAvailableSunday(null);

            testSession.setEnvironment(environment);
            testSession.setPanTestSessionId(studentTestSession.getSessionId());

            testSessionService.create(testSession);
        }
        return testSession;
    }

    private void updateStudentScenario(final StudentScenario studentScenario, final StudentTestSession studentTestSession) {
        studentScenario.setTestnavLoginName(studentTestSession.getTestnavLogin());
        studentScenario.setTestnavLoginPassword(studentTestSession.getTestnavPassword());
        // studentScenarioStatusId = 1(Not Started)
        studentScenario.setStudentScenarioStatusId(1);
        studentScenario.setStudentCode(studentTestSession.getStudentCode());
        studentScenario.setLastName(studentTestSession.getLastName());
        studentScenario.setFirstName(studentTestSession.getFirstName());
        studentScenario.setStudentGroupName(studentTestSession.getStudentGroupName());
        studentScenario.setStudentTestStatus(studentTestSession.getStudentTestStatus());
        studentScenario.setSessionAssignStatus(studentTestSession.getSessionAssignStatus());
        studentScenario.setExportType(studentTestSession.getScopeCode());
        studentScenario.setStudentTestSessionAssignId(studentTestSession.getStudentTestSessionAssignId());
    }

    @Transactional
    public Result releaseReservedStudent(final String apiVersion, final String uuid) {
        Logger.info("event=\"Request to release reserved student.\", studentTestUUID=\"{}\"", uuid);
        List<String> uuids = Arrays.asList(StringUtils.split(uuid, "_"));
        if (uuids.size() == 1) {
            Optional<StudentReservation> reservedStudent = studentReservationService.findByUUID(uuid);
            reservedStudent.ifPresent(studentReservationService::delete);
        } else if (uuids.size() > 1) {
            studentReservationService.deleteByUuids(uuids);
        }
        return successResult();
    }

    @Transactional
    public Result getAvailableStudentsCount(final String apiVersion, int environmentId, String scopeTreePath, String testCode, String formCode) {
        Logger.info("event=\"Request to get available students from PAN front end.\", scopeTreePath = \"{}\", testCode = \"{}\", formCode = \"{}\"", scopeTreePath, testCode, formCode);
        final Optional<Environment> environment = environmentService.findById(environmentId);

        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return failResult(NOT_FOUND, "Can not find Environment for the environmentId " + environmentId);
        }
        StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());

        String decodedScopeTreePath = StringUtils.replace(scopeTreePath, "|", "/");
        List<StudentTestSession> studentTestSessions;
        long availableStudents;
        try {
            studentTestSessions = stse.getStudTestSessionDetails(decodedScopeTreePath, testCode, formCode);
            List<String> reservedStudents = studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut);
            availableStudents = studentTestSessions.stream()
                    .filter(sts -> !reservedStudents.contains(sts.getStudentTestUuid()))
                    .filter(sts -> sts.getFirstName() != null && sts.getLastName() != null)
                    .count();
        } catch (FrontendDatabaseException e) {
            return failResult(NOT_FOUND, "Unable to retrieve student test session details from PANext Frontend Database.");
        }

        ObjectNode result = Json.newObject();
        result.put("availableStudentsCount", availableStudents);
        return successResult(result);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result getStudentCountForSelectedScenarios(final String apiVersion, final int environmentId) {
        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        JsonNode data = request().body().asJson();
        String scopeTreePath = data.get("scopeTreePath").asText();
        String previewerUrl = data.get("previewerUrl").asText();
        int tenant = data.get("tenant").asInt();
        String includesAdaptiveIndicator = data.get("includesAdaptiveIndicator").asText();
        String testCode = data.get("testCode").asText();
        String formCode = data.get("formCode").asText();
        String tn8CustomerCode = data.get("tn8CustomerCode").asText();
        String testnavScenarioClass = data.get("testnavScenarioClass").asText();
        JsonNode epenScenarioClassNode = data.get("epenScenarioClass");
        String epenScenarioClass = (epenScenarioClassNode != null && !epenScenarioClassNode.isNull()) ? epenScenarioClassNode.asText() : null;
        ArrayNode selectedScenarios = (ArrayNode) data.get("selectedScenarios");
        ArrayNode selectedSessions = (ArrayNode) data.get("testSessions");
        List<String> sessionIds = new ArrayList<>();
        for (int i = 0; i < selectedSessions.size(); i++) {
            String selectedSession = selectedSessions.get(i).get("sessionId").asText();
            sessionIds.add(selectedSession);
        }
        Logger.info("event= \"Request to add scenario class\", testCode= \"{}\", formCode= \"{}\", testnavScenarioClass= \"{}\", epenScenarioClass= \"{}\"", testCode, formCode, testnavScenarioClass, epenScenarioClass);

        final Optional<Environment> environment = jpaApi.withTransaction(() -> environmentService.findById(environmentId));

        if (!environment.isPresent()) {
            String errorMessage = "Can not find Environment";
            Logger.error("event= \"{}\", environmentId= \"{}\"", errorMessage, environmentId);
            return failResult(NOT_FOUND, errorMessage);
        }

        Optional<ScenarioType> testNavScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("TN8"));
        if (!testNavScenarioType.isPresent()) {
            String errorMessage = "Can not find TN8 ScenarioType in database";
            Logger.error("event= \"{}\", \"scenarioType\" = \"TN8\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }

        Optional<ScenarioType> epenScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("EPEN"));
        if (!epenScenarioType.isPresent()) {
            String errorMessage = "Can not find EPEN ScenarioType in database";
            Logger.error("event= \"{}\", \"epenScenario\" \"EPEN\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }

        DivJobSetupUtil.ScopeTreePath stp;
        try {
            stp = new DivJobSetupUtil.ScopeTreePath(scopeTreePath);
        } catch (IllegalArgumentException ex) {
            String errorMessage = "Invalid ScopeTreePath found.";
            Logger.error("event=\"\", scopeTreePath=\"{}\"", errorMessage, scopeTreePath);
            return failResult(BAD_REQUEST, errorMessage);
        }
        StudentTestSession studentTestSession = new StudentTestSession();
        studentTestSession.setTestCode(testCode);
        studentTestSession.setFormCode(formCode);
        studentTestSession.setTn8CustomerCode(tn8CustomerCode);
        TestDetail testDetail = divJobSetupUtil.createTestDetailObject(testCode, formCode);
        testDetail.setTestNavCustomerCode(tn8CustomerCode);
        List<Testmap> t3Testmaps = jpaApi.withTransaction(() -> divJobSetupUtil.getT3Testmap(stp, testDetail, testmapDetailService));
        ScenarioCreatorUtil sc = new ScenarioCreatorUtil(scopeTreePath, stp.getAdminCode(),
                stp.getAccountCode(), studentTestSession, token, secret, previewerUrl,
                tenant, includesAdaptiveIndicator, null, t3Testmaps);

        List<T3TestMapItem> epenItems = sc.getEpenItems(testCode, formCode);
        List<String> errors = new ArrayList<>();
        String key = testCode + "_" + formCode;
        Map<String, List<String>> percentCorrectItems = new HashMap<>();
        List<Scenario> epenScenarios = new ArrayList<>();
        if (epenScenarioClass != null) {
            List<String> scenarioClassNames = Collections.singletonList(epenScenarioClass);
            epenScenarios = generateEpenScenario(scopeTreePath, environment.get(), epenScenarioType.get(),
                    studentTestSession, epenItems, errors, percentCorrectItems, scenarioClassNames);
            if (epenScenarios.isEmpty()) {
                String msg = "Failed to create ePEN2 Scenario.";
                Logger.error("event=\"{}\", \"TestCode\"=\"{}\",\"FormCode\"=\"{}\"", msg, testCode, formCode);

                msg += StringUtils.join(errors, ", ");
                ObjectNode error = Json.newObject();
                error.put("testCode", testCode);
                error.put("formCode", formCode);
                return failResult(BAD_REQUEST, msg, error);
            }
        }

        Testmap testmap = t3Testmaps.iterator().next();
        int t3TestmapId = Integer.parseInt(testmap.getId());
        int testmapVersion = Integer.parseInt(testmap.getVersion());
        Optional<TestmapDetail> testmapDetailResult = jpaApi.withTransaction(() -> testmapDetailService.findByTestmapIdNameAndVersion(t3TestmapId, testmap.getName(), testmapVersion));
        if (!testmapDetailResult.isPresent()) {
            return failResult(NOT_FOUND, "Testmap not found in the previewer. Previewer url=\"" + previewerUrl + "\"");
        }
        TestmapDetail testmapDetail = testmapDetailResult.get();
        ScenarioCreatorUtil.PreviewerDetail previewerDetail = new ScenarioCreatorUtil.PreviewerDetail(tenant, previewerUrl, formCode, includesAdaptiveIndicator);
        final Type type = new TypeToken<ItemResponsePool>() {
        }.getType();
        ItemResponsePool irp = DivJobSetupUtil.GSON.fromJson(testmapDetail.getTempResponsePool(), type);

        List<Scenario> testNavScenarios = generateTestnavScenario(testNavScenarioType.get(), studentTestSession, sc, previewerDetail, percentCorrectItems, testnavScenarioClass, irp);
        int maxTestnavScenarioCount = testNavScenarios.size();
        int maxEpenScenarioCount = epenScenarios.size();
        int totalStudentsUsed = (maxTestnavScenarioCount > maxEpenScenarioCount) ? maxTestnavScenarioCount : maxEpenScenarioCount;
        StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());

        try {
            List<StudentTestSession> totalStudentTestSessions = stse.getStudTestSessionDetails(scopeTreePath, testCode, formCode)
                    .stream()
                    .filter(sts -> sessionIds.contains(sts.getSessionId()))
                    .collect(Collectors.toList());
            List<String> reservedStudents = jpaApi.withTransaction(() -> studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut));

            List<StudentTestSession> studentFilteredByReservedStudent = totalStudentTestSessions
                    .stream().filter(st -> !reservedStudents.contains(st.getStudentTestUuid()))
                    .collect(Collectors.toList());

            List<StudentTestSession> availableStudentTestSessions = totalStudentTestSessions.stream()
                    .filter(sts -> !reservedStudents.contains(sts.getStudentTestUuid()))
                    .filter(sts -> sts.getFirstName() != null && sts.getLastName() != null)
                    .collect(Collectors.toList());
            if (availableStudentTestSessions.isEmpty()) {
                int totalReservedStudents = (totalStudentTestSessions.size() - studentFilteredByReservedStudent.size());
                return failResult(NOT_FOUND, "No available student sessions found. Number of students in \"Ready\" status:" + totalStudentTestSessions.size()
                        + ". Number of students reserved for other DIV jobs: " + totalReservedStudents);
            }
            Map<String, String> usedStudentUuids = getStudentTestSessions(availableStudentTestSessions, totalStudentsUsed).stream()
                    .collect(Collectors.toMap(StudentTestSession::getStudentTestUuid, StudentTestSession::getSessionName));

            List<String> testNavScenarioNames = testNavScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());
            List<String> epenScenarioNames = epenScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());
            ObjectNode result = Json.newObject();
            result.set("testNavScenarioNames", Json.toJson(testNavScenarioNames));
            result.set("epenScenarioNames", Json.toJson(epenScenarioNames));
            result.put("numberOfTestnavScenario", maxTestnavScenarioCount);
            result.put("numberOfEpenScenario", maxEpenScenarioCount);
            int remainingStudents = availableStudentTestSessions.size() - usedStudentUuids.size();
            result.put("remainingStudents", (remainingStudents > 0) ? remainingStudents : 0);

            if (usedStudentUuids.size() >= totalStudentsUsed) {
                result.put("totalStudentsUsed", totalStudentsUsed);
            } else {
                result.put("totalStudentsUsed", usedStudentUuids.size());
                result.put("warningMessage", usedStudentUuids.size() + " (" + (totalStudentsUsed - usedStudentUuids.size()) + " scenarios not assigned a student)");
                result.put("warning", true);
            }


            result.set("uuids", Json.toJson(usedStudentUuids));
            result.put("testCode", testCode);
            result.put("formCode", formCode);
            return successResult(result);
        } catch (FrontendDatabaseException e) {
            String errorMessage = "Unable to retrieve student test session details from PANext Frontend Database.";
            Logger.error("event=\"{}\", scopeTreePath=\"{}\", testCode=\"{}\", formCode=\"{}\"", errorMessage, scopeTreePath, testCode, formCode);
            return failResult(BAD_REQUEST, errorMessage);
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result getBatteryStudentCountForSelectedScenarios(final String apiVersion, final int environmentId) {
        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        JsonNode data = request().body().asJson();
        String scopeTreePath = data.get("scopeTreePath").asText();
        String batteryTestCode = data.get("batteryTestCode").asText();
        String testnavScenarioClass = data.get("selectedTestNavScenario").asText();
        JsonNode selectedEpenScenario = data.get("selectedEpenScenario");
        String epenScenarioClass = (selectedEpenScenario != null && !selectedEpenScenario.isNull()) ? selectedEpenScenario.asText() : null;
        ArrayNode units = (ArrayNode) data.get("units");
        ArrayNode selectedScenarios = (ArrayNode) data.get("selectedScenarios");
        ArrayNode testSessions = (ArrayNode) data.get("testSessions");
        List<Integer> sessions = new ArrayList<>();
        for (JsonNode testSession : testSessions) {
            sessions.add(testSession.get("sessionId").asInt());
        }

        Optional<Environment> environment = jpaApi.withTransaction(() -> environmentService.findById(environmentId));

        if (!environment.isPresent()) {
            String errorMessage = "Can not find Environment";
            Logger.error("event= \"{}\", environmentId= \"{}\"", errorMessage, environmentId);
            return failResult(NOT_FOUND, errorMessage);
        }

        Optional<ScenarioType> testNavScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("TN8"));
        if (!testNavScenarioType.isPresent()) {
            String errorMessage = "Can not find TN8 ScenarioType in database";
            Logger.error("event= \"{}\", \"scenarioType\" = \"TN8\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }

        Optional<ScenarioType> epenScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("EPEN"));
        if (!epenScenarioType.isPresent()) {
            String errorMessage = "Can not find EPEN ScenarioType in database";
            Logger.error("event= \"{}\", \"epenScenario\" \"EPEN\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }

        DivJobSetupUtil.ScopeTreePath stp;
        try {
            stp = new DivJobSetupUtil.ScopeTreePath(scopeTreePath);
        } catch (IllegalArgumentException ex) {
            String errorMessage = "Invalid ScopeTreePath found.";
            Logger.error("event=\"{}\", scopeTreePath=\"{}\"", errorMessage, scopeTreePath);
            return failResult(BAD_REQUEST, errorMessage);
        }

        int maximumScenarios = 0;
        List<String> unitTestCodes = new ArrayList<>();
        List<String> unitFormCodes = new ArrayList<>();
        ArrayNode unitArray = Json.newArray();
        for (int i = 0; i < units.size(); i++) {
            JsonNode unit = units.get(i);
            String previewerUrl = unit.get("previewerUrl").asText();
            int tenant = unit.get("tenant").asInt();
            String includesAdaptiveIndicator = unit.get("includesAdaptiveIndicator").asText();
            String testCode = unit.get("testCode").asText();
            String formCode = unit.get("formCode").asText();
            String tn8CustomerCode = unit.get("testNavCustomerCode").asText();
            boolean epenScenarioDisable = true;
            if (unit.get("epenScenarioDisable") != null) {
                epenScenarioDisable = unit.get("epenScenarioDisable").asBoolean();
            }
            Logger.info("event= \"Request to add scenario class.\", testCode= \"{}\", formCode= \"{}\", testnavScenarioClass= \"{}\", epenScenarioClass= \"{}\"", testCode, formCode, testnavScenarioClass, epenScenarioClass);

            StudentTestSession studentTestSession = new StudentTestSession();
            studentTestSession.setTestCode(testCode);
            studentTestSession.setFormCode(formCode);
            studentTestSession.setTn8CustomerCode(tn8CustomerCode);
            TestDetail testDetail = divJobSetupUtil.createTestDetailObject(testCode, formCode);
            testDetail.setTestNavCustomerCode(tn8CustomerCode);
            List<Testmap> t3Testmaps = jpaApi.withTransaction(() -> divJobSetupUtil.getT3Testmap(stp, testDetail, testmapDetailService));
            ScenarioCreatorUtil sc = new ScenarioCreatorUtil(scopeTreePath, stp.getAdminCode(),
                    stp.getAccountCode(), studentTestSession, token, secret, previewerUrl,
                    tenant, includesAdaptiveIndicator, null, t3Testmaps);

            List<T3TestMapItem> epenItems = sc.getEpenItems(testCode, formCode);
            List<String> errors = new ArrayList<>();
            Map<String, List<String>> percentCorrectItems = new HashMap<>();
            List<Scenario> epenScenarios = new ArrayList<>();
            if (epenScenarioClass != null && !epenScenarioDisable) {
                List<String> scenarioClassNames = Collections.singletonList(epenScenarioClass);
                epenScenarios = generateEpenScenario(scopeTreePath, environment.get(), epenScenarioType.get(),
                        studentTestSession, epenItems, errors, percentCorrectItems, scenarioClassNames);
                if (epenScenarios.isEmpty()) {
                    StringBuilder msg = new StringBuilder("Failed to create ePEN2 Scenario.");
                    Logger.error("event=\"{}\", TestCode =\"{}\", FormCode=\"{}\"", msg, testCode, formCode);

                    if (!errors.isEmpty()) {
                        msg.append(errors.get(0));
                        return failResult(BAD_REQUEST, msg.toString());
                    }
                }
            }

            Testmap testmap = t3Testmaps.iterator().next();
            int t3TestmapId = Integer.parseInt(testmap.getId());
            int testmapVersion = Integer.parseInt(testmap.getVersion());
            String testmapName = testmap.getName();

            ScenarioCreatorUtil.PreviewerDetail previewerDetail = new ScenarioCreatorUtil.PreviewerDetail(tenant, previewerUrl, formCode, includesAdaptiveIndicator);
            List<Scenario> testNavScenarios = new ArrayList<>();
            Optional<TestmapDetail> testmapDetailResult = jpaApi.withTransaction(() ->
                    testmapDetailService.findByTestmapIdNameAndVersion(t3TestmapId, testmapName, testmapVersion));
            if (testmapDetailResult.isPresent()) {
                final Type type = new TypeToken<ItemResponsePool>() {
                }.getType();
                ItemResponsePool irp = DivJobSetupUtil.GSON.fromJson(testmapDetailResult.get().getTempResponsePool(), type);
                List<Scenario> scenarios = generateTestnavScenario(testNavScenarioType.get(), studentTestSession, sc, previewerDetail, percentCorrectItems, testnavScenarioClass, irp);
                testNavScenarios.addAll(scenarios);
            }

            int maxTestnavScenarioCount = testNavScenarios.size();
            int maxEpenScenarioCount = epenScenarios.size();
            int totalRequiredStudents = (maxTestnavScenarioCount > maxEpenScenarioCount) ? maxTestnavScenarioCount : maxEpenScenarioCount;
            maximumScenarios = (totalRequiredStudents > maximumScenarios) ? totalRequiredStudents : maximumScenarios;

            unitTestCodes.add(testCode);
            unitFormCodes.add(formCode);

            List<String> testNavScenarioNames = testNavScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());
            List<String> epenScenarioNames = epenScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());
            ObjectNode unitDetail = Json.newObject();
            unitDetail.set("testNavScenarioNames", Json.toJson(testNavScenarioNames));
            unitDetail.set("epenScenarioNames", Json.toJson(epenScenarioNames));
            unitDetail.put("numberOfTestnavScenario", maxTestnavScenarioCount);
            unitDetail.put("numberOfEpenScenario", maxEpenScenarioCount);
            unitDetail.put("testCode", testCode);
            unitDetail.put("formCode", formCode);
            unitArray.add(unitDetail);
        }
        StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());
        Map<String, List<StudentTestSession>> studentTestSessionMap = stse.getBatteryStudTestSessionDetails(scopeTreePath, Collections.singletonList(batteryTestCode), sessions);

        String batteryCodes = batteryTestCode + StringUtils.join(unitTestCodes, "#") + StringUtils.join(unitFormCodes, "#");
        filterBatteryStudentsByUnitForms(studentTestSessionMap, batteryCodes);
        int paNextStudentCount = studentTestSessionMap.size();
        Iterator<Map.Entry<String, List<StudentTestSession>>> iterator = studentTestSessionMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, List<StudentTestSession>> entry = iterator.next();
            List<String> uuids = entry.getValue().stream().map(StudentTestSession::getStudentTestUuid).collect(Collectors.toList());
            List<String> reservedUuids = jpaApi.withTransaction(() -> studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut));
            // checking whether all the students are reserved or not
            if (!ListUtils.intersection(reservedUuids, uuids).isEmpty()) {
                iterator.remove();
            }
        }
        //removing the reserved battery students.
        filterReservedBatteryStudent(studentTestSessionMap);

        if (studentTestSessionMap.isEmpty()) {
            int totalReservedStudents = (paNextStudentCount - studentTestSessionMap.size());
            return failResult(NOT_FOUND, "No available student sessions found. Number of students in \"Ready\" status:" + paNextStudentCount
                    + ". Number of students reserved for other DIV jobs: " + totalReservedStudents);
        }
        int requiredStudents = (studentTestSessionMap.size() < maximumScenarios) ? studentTestSessionMap.size() : maximumScenarios;
        List<String> batteryUuids = studentTestSessionMap.keySet().stream().limit(requiredStudents).collect(Collectors.toList());

        ObjectNode result = Json.newObject();
        createSelectedScenarioResultForAllUnits(unitArray, units, studentTestSessionMap, batteryUuids);
        for (JsonNode unit : unitArray) {
            if (unit.get("error") != null) {
                return failResult(NOT_FOUND, unit.get("error").asText());
            }
        }
        result.set("units", Json.toJson(unitArray));
        if (studentTestSessionMap.size() >= maximumScenarios) {
            result.put("totalStudentsUsed", maximumScenarios);
        } else {
            result.put("totalStudentsUsed", studentTestSessionMap.size());
            result.put("warningMessage", studentTestSessionMap.size() + " (" + (maximumScenarios - studentTestSessionMap.size()) + " scenarios not assigned a student)");
            result.put("warning", true);
        }

        int remainingStudents = studentTestSessionMap.size() - requiredStudents;
        result.put("remainingStudents", (remainingStudents > 0) ? remainingStudents : 0);
        return successResult(result);
    }

    private void filterReservedBatteryStudent(Map<String, List<StudentTestSession>> testSessionDetailMap) {
        List<String> reservedUuids = jpaApi.withTransaction(() -> studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut));
        Iterator<Map.Entry<String, List<StudentTestSession>>> iterator = testSessionDetailMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<StudentTestSession>> entry = iterator.next();
            List<String> uuids = entry.getValue().stream().map(StudentTestSession::getStudentTestUuid).collect(Collectors.toList());
            if (!ListUtils.intersection(reservedUuids, uuids).isEmpty()) {
                iterator.remove();
            }
        }
    }

    private void filterBatteryStudentsByUnitForms(Map<String, List<StudentTestSession>> testSessionDetailMap, String batteryKeys) {
        Iterator<Map.Entry<String, List<StudentTestSession>>> itr = testSessionDetailMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, List<StudentTestSession>> entry = itr.next();
            List<StudentTestSession> testSessions = entry.getValue();
            String batteryCodes = DivJobSetupUtil.getBatteryUnitFormCodes(testSessions);
            if (!batteryKeys.equals(batteryCodes)) {
                itr.remove();
            }
        }
    }

    private List<Scenario> generateTestnavScenario(final ScenarioType testNavScenarioType,
                                                   final StudentTestSession studentTestSession,
                                                   final ScenarioCreatorUtil sc,
                                                   ScenarioCreatorUtil.PreviewerDetail previewerDetail,
                                                   final Map<String, List<String>> percentCorrectItems,
                                                   final String sClassName, ItemResponsePool irp) {
        final String testCode = studentTestSession.getTestCode();
        final String formCode = studentTestSession.getFormCode();
        List<String> epenPercentCorrectItems = new ArrayList<>();
        String tempScenarioClassName = sClassName;
        if (sClassName.startsWith("percentCorrect")) {
            epenPercentCorrectItems = percentCorrectItems.get(sClassName);
            tempScenarioClassName = sClassName.replaceAll("[%, ]", "");
        }

        List<TestNavScenario> testNavScenarios = new ArrayList<>();
        if (NavigationScenarioCreator.NAVIGATION_SCENARIOS.contains(tempScenarioClassName)) {
            NavigationScenarioCreator navigationScenarioCreator = divJobSetupUtil.getNavigationScenarioCreator(testCode, formCode, tempScenarioClassName);
            TestNavScenario navigationScenario = navigationScenarioCreator.generateNavigationScenario(sc, irp);
            if (navigationScenario != null) {
                testNavScenarios.add(navigationScenario);
            }
        } else {
            testNavScenarios = sc.getTestNavScenario(testCode, formCode, tempScenarioClassName.toUpperCase(), epenPercentCorrectItems, irp);
        }

        List<Scenario> scenarios = new ArrayList<>();
        TestmapDetail testmapDetail = null;
        Set<Integer> testMapIds = new HashSet<>();
        for (TestNavScenario testNavScenario : testNavScenarios) {

            final int testmapId = Integer.parseInt(testNavScenario.getTestmapId());
            final int testmapVersion = Integer.parseInt(testNavScenario.getTestmapVersion());
            if (!testMapIds.contains(testmapId)) {
                testMapIds.add(testmapId);

                Optional<TestmapDetail> testmapDetailResult = jpaApi.withTransaction(() -> testmapDetailService.findByTestmapIdAndVersion(testmapId, testmapVersion));
                if (testmapDetailResult.isPresent()) {
                    testmapDetail = testmapDetailResult.get();
                }
            }
            Optional<PreviewerTenant> previewerTenant = jpaApi.withTransaction(() -> previewerTenantService
                    .findByTenantAndPreviewerUrl(previewerDetail.getTenant(), previewerDetail.getPreviewerUrl()));

            if (previewerTenant.isPresent()) {
                Long previewerTenantId = previewerTenant.get().getPreviewerTenantId();
                Optional<ScenarioClass> scenarioClassOptional = jpaApi.withTransaction(() -> {
                    String scenarioClassName = sClassName;
                    if (sClassName.startsWith("percentCorrect")) {
                        scenarioClassName = "percentCorrect";
                    }
                    return scenarioClassService.findByScenarioClass(scenarioClassName);
                });

                if (!scenarioClassOptional.isPresent()) {
                    return new ArrayList<>();
                }

                ScenarioClass scenarioClass = scenarioClassOptional.get();
                Integer scenarioClassId = scenarioClass.getScenarioClassId();
                String scenarioName = testNavScenario.getScenarioName();
                String newScenario = testNavScenario.getScenario();

                boolean isActiveScenario = jpaApi.withTransaction(() -> scenarioService.isActiveTestNavScenario(testCode, formCode,
                        testNavScenarioType.getScenarioTypeId(), scenarioClassId, scenarioName, testmapId, testmapVersion, newScenario,
                        previewerTenantId));

                if (!isActiveScenario) {
                    Scenario scenario = buildScenario(testCode, formCode, scenarioClass, newScenario,
                            testNavScenarioType, scenarioName);
                    scenario.setTestmapDetail(testmapDetail);
                    scenario.setPreviewerTenantId(previewerTenantId);
                    jpaApi.withTransaction(() -> scenarioService.create(scenario));
                }
                Optional<Scenario> scenarioOptional = jpaApi.withTransaction(() -> scenarioService.getActiveScenario(testCode, formCode,
                        testNavScenarioType.getScenarioTypeId(), scenarioClassId, scenarioName, testmapId, testmapVersion, newScenario,
                        previewerTenantId));
                scenarioOptional.ifPresent(scenarios::add);
            }
        }
        return scenarios;
    }

    private List<Scenario> generateEpenScenario(final String scopeTreePath, final Environment environment,
                                                final ScenarioType epenScenarioType, final StudentTestSession studentTestSession,
                                                List<T3TestMapItem> epenItems, final List<String> errors,
                                                final Map<String, List<String>> percentCorrectItems, List<String> scenarioClassNames) {

        final String testCode = studentTestSession.getTestCode();
        final String formCode = studentTestSession.getFormCode();
        EpenScenarioCreator epenScenarioCreator = epenScenarioCreatorProvider.get();

        List<EpenScenarioCreator.EpenScenarioResult> epenScenarioResults = epenScenarioCreator.readEpen2Scenario(environment, scenarioClassNames, scopeTreePath, testCode, formCode);

        //To add itemStatus in the epenScenarios.
        addItemStatus(epenItems, epenScenarioResults);
        List<Scenario> epenScenarios = new ArrayList<>();

        if (epenScenarioCreator.getPercentCorrectItems() != null) {
            percentCorrectItems.putAll(epenScenarioCreator.getPercentCorrectItems());
        }

        for (EpenScenarioCreator.EpenScenarioResult epenScenario : epenScenarioResults) {

            if (!epenScenario.getErrors().isEmpty()) {
                errors.addAll(epenScenario.getErrors());
                continue;
            }

            JsonArray data = new JsonParser().parse(epenScenario.getScenarioJson()).getAsJsonObject().get("data")
                    .getAsJsonArray();
            if (data.size() != epenItems.size()) {
                DbConnection beDbConnection = environment.getBeMysqlConnection();
                BackendData backendData = backendDataFactory.create(beDbConnection);

                Set<ItemStatus> eligibleItemStatuses = backendData.getItemStatusesEligibleForEpenScoring(scopeTreePath);
                long numItemsExpected = epenItems.stream()
                        .filter(item -> eligibleItemStatuses.contains(item.getItemStatus())).count();

                if (data.size() < numItemsExpected) {
                    Logger.error(
                            "event= \"Only {} out of {} ePEN scorable items are configured properly for ePEN scoring. "
                                    + "Please verify that the scoring guide, traits and score points are properly set up for these items.\"",
                            data.size(), numItemsExpected);

                    String errorMsg = "Only " + data.size() + " out of " + numItemsExpected
                            + " ePEN scorable items are configured properly for ePEN scoring. "
                            + "Please verify that the scoring guide, traits and score points are properly set up for "
                            + "these items.";
                    errors.add(errorMsg);
                }
            }
            Optional<ScenarioClass> scenarioClassResult = jpaApi.withTransaction(() -> scenarioClassService.findByScenarioClass(epenScenario.getScenarioClass()));

            if (!scenarioClassResult.isPresent()) {
                Logger.error("Can not find ScenarioClass {}", epenScenario.getScenarioClass());
                continue;
            }
            ScenarioClass scenarioClass = scenarioClassResult.get();

            Integer scenarioClassId = scenarioClass.getScenarioClassId();
            Integer epenScenarioTypeId = epenScenarioType.getScenarioTypeId();
            String epenScenarioName = epenScenario.getScenarioName();
            String epenScenarioJson = epenScenario.getScenarioJson();

            boolean active = jpaApi.withTransaction(() -> scenarioService.isActiveScenario(testCode, formCode, epenScenarioTypeId, scenarioClassId, epenScenarioName, epenScenarioJson));
            if (!active) {
                Scenario scenario = buildScenario(testCode, formCode, scenarioClass, epenScenario.getScenarioJson(),
                        epenScenarioType, epenScenario.getScenarioName());
                jpaApi.withTransaction(() -> scenarioService.create(scenario));
            }
            Optional<Scenario> scenarioOptional = jpaApi.withTransaction(() -> scenarioService.getEpenActiveScenario(testCode, formCode, epenScenarioTypeId, scenarioClassId, epenScenarioName, epenScenarioJson));
            scenarioOptional.ifPresent(epenScenarios::add);
        }
        return epenScenarios;
    }

    /**
     * Adds the itemStatus attribute in the epenScenario.
     *
     * @param epenItems     The List of epnItems
     * @param epenScenarios The List of epenScenario
     */
    private void addItemStatus(List<T3TestMapItem> epenItems, List<EpenScenarioCreator.EpenScenarioResult> epenScenarios) {
        for (EpenScenarioCreator.EpenScenarioResult epenScenario : epenScenarios) {
            if (!epenScenario.getErrors().isEmpty() && epenScenario.getScenarioJson() == null) {
                return;
            }
            JsonObject epenScenarioJson = new JsonParser().parse(epenScenario.getScenarioJson()).getAsJsonObject();
            JsonArray data = epenScenarioJson.getAsJsonObject().get("data")
                    .getAsJsonArray();
            for (JsonElement ele : data) {
                String uin = ele.getAsJsonObject().get("uin").getAsString();
                Optional<T3TestMapItem> item = epenItems.stream().filter(it -> it.getUin().equalsIgnoreCase(uin)).findFirst();
                item.ifPresent(t3TestMapItem -> ele.getAsJsonObject().addProperty("itemStatus", t3TestMapItem.getItemStatus().name()));
            }
            epenScenario.setScenarioJson(epenScenarioJson.toString());
        }
    }

    /**
     * Creates the new instance of Scenario.
     *
     * @param testCode      The test code
     * @param formCode      The form Code
     * @param scenarioClass The instance of {@link ScenarioClass}
     * @param scenarioJson  The scenario JSON string
     * @param scType        The instance of {@link ScenarioType}
     * @param scenarioName  The scenario name
     * @return The new instance of {@link Scenario}
     */
    private Scenario buildScenario(final String testCode, final String formCode, ScenarioClass scenarioClass,
                                   String scenarioJson, ScenarioType scType, String scenarioName) {
        Scenario scenario = new Scenario();
        scenario.setScenarioType(scType);
        scenario.setScenarioClass(scenarioClass);
        scenario.setScenario(scenarioJson);
        scenario.setTestCode(testCode);
        scenario.setFormCode(formCode);
        scenario.setScenarioName(scenarioName);
        return scenario;
    }

    private List<StudentTestSession> getStudentTestSessions(final List<StudentTestSession> studentTestSessions,
                                                            final int numberOfRequiredStudent) {
        int requiredStudents = numberOfRequiredStudent;

        if (numberOfRequiredStudent > studentTestSessions.size()) {
            requiredStudents = studentTestSessions.size();
        }

        List<String> reservedStudents = jpaApi.withTransaction(() -> studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut));
        List<StudentTestSession> availableStudents = studentTestSessions.stream()
                .filter(sts -> !reservedStudents.contains(sts.getStudentTestUuid()))
                .filter(sts -> sts.getFirstName() != null && sts.getLastName() != null).collect(Collectors.toList());

        if (availableStudents.isEmpty()) {
            return availableStudents;
        }

        List<StudentTestSession> currentStudents = availableStudents;

        if (availableStudents.size() > requiredStudents) {
            currentStudents = availableStudents.subList(0, requiredStudents);
        }

        for (StudentTestSession sts : currentStudents) {
            StudentReservation sr = new StudentReservation();
            sr.setUuid(sts.getStudentTestUuid());
            jpaApi.withTransaction(() -> studentReservationService.create(sr));
        }
        return currentStudents;
    }

    public Result getAvailableBatteryStudentsCount(String apiVersion, int environmentId, String scopeTreePath, String sessionIdsString, String batteryTestCode, String key) {
        Logger.info("event= \"Request to release students.\", batteryTestcode= \"{}\"", batteryTestCode);
        final Optional<Environment> environment = jpaApi.withTransaction(() -> environmentService.findById(environmentId));

        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return failResult(NOT_FOUND, "Can not find Environment for the environmentId." + environmentId);
        }

        List<Integer> sessionIds = Arrays.stream(sessionIdsString.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<String> batteryTestCodes = Collections.singletonList(batteryTestCode);
        String decodedScopeTreePath = StringUtils.replace(scopeTreePath, "|", "/");
        StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());
        Map<String, List<StudentTestSession>> testSessionDetailMap = stse.getBatteryStudTestSessionDetails(decodedScopeTreePath, batteryTestCodes, sessionIds);

        String batteryTestFormCode = StringUtils.replace(key, "_", "#");
        filterBatteryStudentsByUnitForms(testSessionDetailMap, batteryTestFormCode);
        filterReservedBatteryStudent(testSessionDetailMap);
        ObjectNode result = Json.newObject();
        result.put("remainingStudents", testSessionDetailMap.size());
        return successResult(Json.toJson(result));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result applySelectedScenarios(final String apiVersion, final int environmentId) {

        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode data = request().body().asJson();
        String scopeTreePath = data.get("scopeTreePath").asText();
        String previewerUrl = data.get("previewerUrl").asText();
        int tenant = data.get("tenant").asInt();
        String includesAdaptiveIndicator = data.get("includesAdaptiveIndicator").asText();
        String testCode = data.get("testCode").asText();
        String formCode = data.get("formCode").asText();
        String tn8CustomerCode = data.get("tn8CustomerCode").asText();
        ArrayNode selectedScenarios = (ArrayNode) data.get("selectedScenarios");

        if (data.get("existingScenarios") != null) {
            ArrayNode existingScenarios = (ArrayNode) data.get("existingScenarios");
            List<String> reservedStudentUuids = extractReservedStudentUuids(existingScenarios);

            if (!reservedStudentUuids.isEmpty()) {
                //Deleting already reserved but not used students from student_reservation table for this form
                jpaApi.withTransaction(() -> studentReservationService.deleteByUuids(reservedStudentUuids));
            }
        }

        final Optional<Environment> environment = jpaApi.withTransaction(() -> environmentService.findById(environmentId));

        if (!environment.isPresent()) {
            String errorMessage = "Can not find Environment";
            Logger.error("event= \"{}\", environmentId= \"{}\"", errorMessage, environmentId);
            return failResult(NOT_FOUND, errorMessage);
        }

        Optional<ScenarioType> testNavScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("TN8"));

        if (!testNavScenarioType.isPresent()) {
            String errorMessage = "Can not find TN8 ScenarioType in database";
            Logger.error("event= \"{}\", \"scenarioType\" = \"TN8\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }

        Optional<ScenarioType> epenScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("EPEN"));

        if (!epenScenarioType.isPresent()) {
            String errorMessage = "Can not find EPEN ScenarioType in database";
            Logger.error("event= \"{}\", \"epenScenario\" \"EPEN\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }

        DivJobSetupUtil.ScopeTreePath stp;

        try {
            stp = new DivJobSetupUtil.ScopeTreePath(scopeTreePath);
        } catch (IllegalArgumentException ex) {
            String errorMessage = "Invalid ScopeTreePath found.";
            Logger.error("event=\"\", scopeTreePath=\"{}\"", errorMessage, scopeTreePath);
            return failResult(BAD_REQUEST, errorMessage);
        }

        StudentTestSession studentTestSession = new StudentTestSession();
        studentTestSession.setTestCode(testCode);
        studentTestSession.setFormCode(formCode);
        studentTestSession.setTn8CustomerCode(tn8CustomerCode);
        TestDetail testDetail = divJobSetupUtil.createTestDetailObject(testCode, formCode);
        testDetail.setTestNavCustomerCode(tn8CustomerCode);
        List<Testmap> t3Testmaps = jpaApi.withTransaction(() -> divJobSetupUtil.getT3Testmap(stp, testDetail, testmapDetailService));
        Testmap testmap = t3Testmaps.iterator().next();
        int t3TestmapId = Integer.parseInt(testmap.getId());
        int testmapVersion = Integer.parseInt(testmap.getVersion());
        Optional<TestmapDetail> testmapDetailResult = jpaApi.withTransaction(() -> testmapDetailService.findByTestmapIdNameAndVersion(t3TestmapId, testmap.getName(), testmapVersion));
        if (!testmapDetailResult.isPresent()) {
            return failResult(NOT_FOUND, "Testmap not found in the previewer. Previewer url=\"" + previewerUrl + "\"");
        }
        TestmapDetail testmapDetail = testmapDetailResult.get();
        TestNav8API testNav8api = TestNav8API.connectToPreviewer("http://" + previewerUrl, token.toCharArray(), secret.toCharArray());
        ScenarioCreatorUtil.PreviewerDetail previewerDetail = new ScenarioCreatorUtil.PreviewerDetail(tenant, previewerUrl, formCode, includesAdaptiveIndicator);
        Integer previewerTestmapId = readPreviewerTestmapId(tenant, testCode, formCode, testmapDetail, testNav8api);
        ItemResponsePool irp = ScenarioCreatorUtil.getItemResponsePool(tenant, testNav8api, previewerTestmapId);

        ScenarioCreatorUtil sc = new ScenarioCreatorUtil(scopeTreePath, stp.getAdminCode(),
                stp.getAccountCode(), studentTestSession, token, secret, previewerUrl,
                tenant, includesAdaptiveIndicator, null, t3Testmaps);

        List<T3TestMapItem> epenItems = sc.getEpenItems(testCode, formCode);
        List<StudentTestSession> totalStudentTestSessions;
        try {
            StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());
            totalStudentTestSessions = stse.getStudTestSessionDetails(scopeTreePath, testCode, formCode);
        } catch (FrontendDatabaseException e) {
            String errorMessage = "Unable to retrieve student test session details from PANext Frontend Database.";
            Logger.error("event=\"{}\", scopeTreePath=\"{}\", testCode=\"{}\", formCode=\"{}\"", errorMessage, scopeTreePath, testCode, formCode);
            return failResult(BAD_REQUEST, errorMessage);
        }

        List<String> errors = new ArrayList<>();
        ObjectNode result = Json.newObject();
        ArrayNode selectedScenariosResult = result.arrayNode();
        List<String> testNavScenarioClassWithoutStudents = new ArrayList<>();
        for (JsonNode selectedScenario : selectedScenarios) {
            String testnavScenarioClass = selectedScenario.get("testNav").asText();
            String epenScenarioClass = (selectedScenario.get("epen") != null && !selectedScenario.get("epen").isNull()) ? selectedScenario.get("epen").asText() : null;
            Logger.info("event= \"Request to add scenario class\", testCode= \"{}\", formCode= \"{}\", testnavScenarioClass= \"{}\", epenScenarioClass= \"{}\"", testCode, formCode, testnavScenarioClass, epenScenarioClass);
            List<String> reservedStudents = jpaApi.withTransaction(() -> studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut));
            List<StudentTestSession> availableStudentTestSessions = totalStudentTestSessions.stream()
                    .filter(sts -> !reservedStudents.contains(sts.getStudentTestUuid()))
                    .filter(sts -> sts.getFirstName() != null && sts.getLastName() != null)
                    .collect(Collectors.toList());
            Map<String, List<String>> percentCorrectItems = new HashMap<>();
            List<Scenario> epenScenarios = new ArrayList<>();
            if (epenScenarioClass != null) {
                List<String> scenarioClassNames = Collections.singletonList(epenScenarioClass);
                epenScenarios = generateEpenScenario(scopeTreePath, environment.get(), epenScenarioType.get(),
                        studentTestSession, epenItems, errors, percentCorrectItems, scenarioClassNames);
                if (epenScenarios.isEmpty()) {
                    String msg = "Failed to create ePEN2 Scenario.";
                    Logger.error("event=\"{}\", \"TestCode\"=\"{}\",\"FormCode\"=\"{}\"", msg, testCode, formCode);
                    errors.add(msg);
                    ObjectNode errorNode = Json.newObject();
                    errorNode.put("testCode", testCode);
                    errorNode.put("formCode", formCode);
                    return failResult(BAD_REQUEST, StringUtils.join(errors, ", "), errorNode);
                }
            }

            List<Scenario> testNavScenarios = generateTestnavScenario(testNavScenarioType.get(), studentTestSession, sc, previewerDetail, percentCorrectItems, testnavScenarioClass, irp);

            if (!availableStudentTestSessions.isEmpty()) {
                ObjectNode selectedScenarioResult = generateSelectedScenarioResult(availableStudentTestSessions, result, testnavScenarioClass, epenScenarioClass, testNavScenarios, epenScenarios);
                selectedScenariosResult.add(selectedScenarioResult);
                result.set("selectedScenariosResult", selectedScenariosResult);
            } else {
                testNavScenarioClassWithoutStudents.add(testnavScenarioClass);
            }
        }

        if (!testNavScenarioClassWithoutStudents.isEmpty()) {
            result.put("alertMessage", "Due to the lack of students following selected TestNav scenarios are not applied to this form: " + String.join(", ", testNavScenarioClassWithoutStudents));
        }

        result.put("testCode", testCode);
        result.put("formCode", formCode);
        return successResult(result);
    }

    private List<String> extractReservedStudentUuids(ArrayNode existingScenarios) {
        List<String> uuids = new ArrayList<>();
        for (JsonNode selectedScenario : existingScenarios) {
            Iterator<String> fieldNames = selectedScenario.get("uuids").fieldNames();
            while (fieldNames.hasNext()) {
                uuids.add(fieldNames.next());
            }
        }
        return uuids;
    }

    private Integer readPreviewerTestmapId(int tenant, String testCode, String formCode, TestmapDetail testmapDetail, TestNav8API testNav8api) {
        Integer previewerTestmapId = null;
        if (testmapDetail != null && testmapDetail.getPreviewerTestmapId() == null) {
            GetTestMapsResult testMaps = testNav8api.getTestMaps(tenant);
            previewerTestmapId = testMaps.getTestMaps().stream()
                    .filter(tm -> tm.getIdentifier().equals(testCode) && tm.getFormId().equals(formCode))
                    .map(GetTestMapsResult.TestMap::getId).findFirst().orElse(null);
            testmapDetail.setPreviewerTestmapId(previewerTestmapId);
        } else if (testmapDetail != null) {
            previewerTestmapId = testmapDetail.getPreviewerTestmapId();
        }
        return previewerTestmapId;
    }

    private ObjectNode generateSelectedScenarioResult(List<StudentTestSession> availableStudentTestSessions,
                                                      ObjectNode result, String testnavScenarioClass, String epenScenarioClass,
                                                      List<Scenario> testNavScenarios, List<Scenario> epenScenarios) {

        int maxTestnavScenarioCount = testNavScenarios.size();
        int maxEpenScenarioCount = epenScenarios.size();
        int totalStudentsUsed = (maxTestnavScenarioCount > maxEpenScenarioCount) ? maxTestnavScenarioCount : maxEpenScenarioCount;


        Map<String, String> usedStudentUuids = getStudentTestSessions(availableStudentTestSessions, totalStudentsUsed).stream()
                .collect(Collectors.toMap(StudentTestSession::getStudentTestUuid, StudentTestSession::getSessionName));

        List<String> testNavScenarioNames = testNavScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());
        List<String> epenScenarioNames = epenScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());

        ObjectNode selectedScenarioResult = Json.newObject();
        selectedScenarioResult.put("testNav", testnavScenarioClass);
        selectedScenarioResult.put("epen", epenScenarioClass);
        selectedScenarioResult.set("testNavScenarioNames", Json.toJson(testNavScenarioNames));
        selectedScenarioResult.set("epenScenarioNames", Json.toJson(epenScenarioNames));
        selectedScenarioResult.put("numberOfTestnavScenario", maxTestnavScenarioCount);
        selectedScenarioResult.put("numberOfEpenScenario", maxEpenScenarioCount);
        if (usedStudentUuids.size() >= totalStudentsUsed) {
            selectedScenarioResult.put("maxCount", totalStudentsUsed);
        } else {
            selectedScenarioResult.put("maxCount", usedStudentUuids.size());
            selectedScenarioResult.put("warningMessage", usedStudentUuids.size() + " (" + (totalStudentsUsed - usedStudentUuids.size()) + " scenarios not assigned a student)");
            selectedScenarioResult.put("warning", true);
        }

        selectedScenarioResult.set("uuids", Json.toJson(usedStudentUuids));
        int remainingStudents = availableStudentTestSessions.size() - usedStudentUuids.size();
        result.put("remainingStudents", (remainingStudents > 0) ? remainingStudents : 0);
        return selectedScenarioResult;
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result applySelectedScenariosToBattery(final String apiVersion, final int environmentId) {
        try {
            enforceEnvironmentLevelPermission(DIV_JOB_EXECUTIONS, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<Environment> environment = jpaApi.withTransaction(() -> environmentService.findById(environmentId));
        if (!environment.isPresent()) {
            String errorMessage = "Can not find Environment";
            Logger.error("event= \"{}\", environmentId= \"{}\"", errorMessage, environmentId);
            return failResult(NOT_FOUND, errorMessage);
        }
        Optional<ScenarioType> testNavScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("TN8"));
        if (!testNavScenarioType.isPresent()) {
            String errorMessage = "Can not find TN8 ScenarioType in database";
            Logger.error("event= \"{}\", \"scenarioType\" = \"TN8\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }

        Optional<ScenarioType> epenScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("EPEN"));
        if (!epenScenarioType.isPresent()) {
            String errorMessage = "Can not find EPEN ScenarioType in database";
            Logger.error("event= \"{}\", \"epenScenario\" \"EPEN\"", errorMessage);
            return failResult(NOT_FOUND, errorMessage);
        }
        JsonNode data = request().body().asJson();

        String scopeTreePath = data.get("scopeTreePath").asText();
        DivJobSetupUtil.ScopeTreePath stp;

        try {
            stp = new DivJobSetupUtil.ScopeTreePath(scopeTreePath);
        } catch (IllegalArgumentException ex) {
            String errorMessage = "Invalid ScopeTreePath found.";
            Logger.error("event=\"{}\", scopeTreePath=\"{}\"", errorMessage, scopeTreePath);
            return failResult(BAD_REQUEST, errorMessage);
        }

        ArrayNode testSessions = (ArrayNode) data.get("testSessions");
        List<Integer> sessions = new ArrayList<>();
        for (JsonNode testSession : testSessions) {
            sessions.add(testSession.get("sessionId").asInt());
        }
        String batteryTestCode = data.get("batteryTestCode").asText();

        ArrayNode selectedScenarios = (ArrayNode) data.get("selectedScenarios");

        ObjectNode result = Json.newObject();
        ArrayNode selectedScenariosResult = result.arrayNode();
        Set<String> keys = new HashSet<>();
        for (JsonNode selectedScenario : selectedScenarios) {
            String testnavScenarioClass = selectedScenario.get("testNav").asText();
            JsonNode selectedEpenScenario = selectedScenario.get("epen");
            String epenScenarioClass = (selectedEpenScenario != null && !selectedEpenScenario.isNull()) ? selectedEpenScenario.asText() : null;

            int maxStudents = 0;

            List<String> unitTestCodes = new ArrayList<>();
            List<String> unitFormCodes = new ArrayList<>();


            ArrayNode unitArray = Json.newArray();
            ArrayNode units = (ArrayNode) data.get("units");
            //This key is used at client side code to select correct form.
            StringBuilder key = new StringBuilder();
            for (int i = 0; i < units.size(); i++) {
                JsonNode unit = units.get(i);
                String previewerUrl = unit.get("previewerUrl").asText();
                int tenant = unit.get("tenant").asInt();
                String includesAdaptiveIndicator = unit.get("includesAdaptiveIndicator").asText();
                String testCode = unit.get("testCode").asText();
                String formCode = unit.get("formCode").asText();
                String tn8CustomerCode = unit.get("testNavCustomerCode").asText();
                boolean epenScenarioDisable = true;
                if (unit.get("epenScenarioDisable") != null) {
                    epenScenarioDisable = unit.get("epenScenarioDisable").asBoolean();
                }
                Logger.info("event= \"Request to add scenario class.\", testCode= \"{}\", formCode= \"{}\", testnavScenarioClass= \"{}\", epenScenarioClass= \"{}\"", testCode, formCode, testnavScenarioClass, epenScenarioClass);

                key.append(testCode).append("-").append(formCode).append("_");
                StudentTestSession studentTestSession = new StudentTestSession();
                studentTestSession.setTestCode(testCode);
                studentTestSession.setFormCode(formCode);
                studentTestSession.setTn8CustomerCode(tn8CustomerCode);
                TestDetail testDetail = divJobSetupUtil.createTestDetailObject(testCode, formCode);
                testDetail.setTestNavCustomerCode(tn8CustomerCode);
                List<Testmap> t3Testmaps = jpaApi.withTransaction(() -> divJobSetupUtil.getT3Testmap(stp, testDetail, testmapDetailService));
                ScenarioCreatorUtil sc = new ScenarioCreatorUtil(scopeTreePath, stp.getAdminCode(),
                        stp.getAccountCode(), studentTestSession, token, secret, previewerUrl,
                        tenant, includesAdaptiveIndicator, null, t3Testmaps);

                List<T3TestMapItem> epenItems = sc.getEpenItems(testCode, formCode);
                List<String> errors = new ArrayList<>();
                Map<String, List<String>> percentCorrectItems = new HashMap<>();
                List<Scenario> epenScenarios = new ArrayList<>();
                if ("null".equals(epenScenarioClass)) {
                    epenScenarioClass = null;
                }
                if (epenScenarioClass != null && !epenScenarioDisable) {
                    List<String> scenarioClassNames = Collections.singletonList(epenScenarioClass);
                    epenScenarios = jpaApi.withTransaction(() -> generateEpenScenario(scopeTreePath, environment.get(), epenScenarioType.get(),
                            studentTestSession, epenItems, errors, percentCorrectItems, scenarioClassNames));
                    if (epenScenarios.isEmpty()) {
                        StringBuilder msg = new StringBuilder("Failed to create ePEN2 Scenario.");
                        Logger.error("event=\"{}\", TestCode =\"{}\", FormCode=\"{}\"", msg.toString(), testCode, formCode);
                        msg.append(", testcode= ");
                        msg.append(testCode);
                        msg.append(", formCode= ");
                        msg.append(formCode);
                        errors.add(msg.toString());
                        ObjectNode errorNode = Json.newObject();
                        errorNode.put("testCode", testCode);
                        errorNode.put("formCode", formCode);
                        return failResult(BAD_REQUEST, StringUtils.join(errors, ", "), errorNode);
                    }
                }

                Testmap testmap = t3Testmaps.iterator().next();
                int t3TestmapId = Integer.parseInt(testmap.getId());
                int testmapVersion = Integer.parseInt(testmap.getVersion());
                Optional<TestmapDetail> testmapDetailResult = jpaApi.withTransaction(() -> testmapDetailService.findByTestmapIdNameAndVersion(t3TestmapId, testmap.getName(), testmapVersion));
                if (!testmapDetailResult.isPresent()) {
                    return failResult(NOT_FOUND, "Testmap not found in the previewer. Previewer url=\"" + previewerUrl + "\"");
                }
                TestmapDetail testmapDetail = testmapDetailResult.get();
                TestNav8API testNav8api = TestNav8API.connectToPreviewer("http://" + previewerUrl, token.toCharArray(), secret.toCharArray());
                ScenarioCreatorUtil.PreviewerDetail previewerDetail = new ScenarioCreatorUtil.PreviewerDetail(tenant, previewerUrl, formCode, includesAdaptiveIndicator);
                Integer previewerTestmapId = readPreviewerTestmapId(tenant, testCode, formCode, testmapDetail, testNav8api);
                ItemResponsePool irp = ScenarioCreatorUtil.getItemResponsePool(tenant, testNav8api, previewerTestmapId);

                List<Scenario> testNavScenarios = jpaApi.withTransaction(() -> generateTestnavScenario(testNavScenarioType.get(), studentTestSession, sc, previewerDetail, percentCorrectItems, testnavScenarioClass, irp));


                int maxTestnavScenarioCount = testNavScenarios.size();
                int maxEpenScenarioCount = epenScenarios.size();
                int totalStudentsUsed = (maxTestnavScenarioCount > maxEpenScenarioCount) ? maxTestnavScenarioCount : maxEpenScenarioCount;
                maxStudents = (maxStudents < totalStudentsUsed) ? totalStudentsUsed : maxStudents;

                List<String> testNavScenarioNames = testNavScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());
                List<String> epenScenarioNames = epenScenarios.stream().map(Scenario::getScenarioName).collect(Collectors.toList());
                unitTestCodes.add(testCode);
                unitFormCodes.add(formCode);
                ObjectNode unitDetail = Json.newObject();
                unitDetail.set("testNavScenarioNames", Json.toJson(testNavScenarioNames));
                unitDetail.set("epenScenarioNames", Json.toJson(epenScenarioNames));
                unitDetail.put("numberOfTestnavScenario", maxTestnavScenarioCount);
                unitDetail.put("numberOfEpenScenario", maxEpenScenarioCount);
                unitDetail.put("testCode", testCode);
                unitDetail.put("formCode", formCode);
                unitArray.add(unitDetail);
            }
            keys.add(key.toString());
            StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());
            Map<String, List<StudentTestSession>> studentTestSessionMap = stse.getBatteryStudTestSessionDetails(scopeTreePath, Collections.singletonList(batteryTestCode), sessions);
            String batteryCodes = batteryTestCode + StringUtils.join(unitTestCodes, "#") + StringUtils.join(unitFormCodes, "#");
            filterBatteryStudentsByUnitForms(studentTestSessionMap, batteryCodes);
            int paNextStudentCount = studentTestSessionMap.size();
            //removing the reserved battery students.
            jpaApi.withTransaction(() -> filterReservedBatteryStudent(studentTestSessionMap));
            if (studentTestSessionMap.isEmpty()) {
                int totalReservedStudents = (paNextStudentCount - studentTestSessionMap.size());
                return failResult(NOT_FOUND, "No available student sessions found. Number of students in \"Ready\" status:" + paNextStudentCount
                        + ". Number of students reserved for other DIV jobs: " + totalReservedStudents);
            }
            List<String> batteryUuids = studentTestSessionMap.keySet().stream().limit(maxStudents).collect(Collectors.toList());

            ObjectNode selectedScenarioResult = Json.newObject();
            jpaApi.withTransaction(() -> createSelectedScenarioResultForAllUnits(unitArray, units, studentTestSessionMap, batteryUuids));

            for (JsonNode unit : unitArray) {
                if (unit.get("error") != null) {
                    return failResult(NOT_FOUND, unit.get("error").asText());
                }
            }

            selectedScenarioResult.put("testNav", testnavScenarioClass);
            selectedScenarioResult.put("epen", epenScenarioClass);
            selectedScenarioResult.set("units", Json.toJson(unitArray));
            if (studentTestSessionMap.size() >= maxStudents) {
                selectedScenarioResult.put("maxCount", maxStudents);
            } else {
                selectedScenarioResult.put("maxCount", studentTestSessionMap.size());
                selectedScenarioResult.put("warningMessage", studentTestSessionMap.size() + " (" + (maxStudents - studentTestSessionMap.size()) + " scenarios not assigned a student)");
                selectedScenarioResult.put("warning", true);
            }
            int remainingStudents = studentTestSessionMap.size() - maxStudents;
            result.put("remainingStudents", (remainingStudents > 0) ? remainingStudents : 0);
            selectedScenariosResult.add(selectedScenarioResult);
        }

        result.put("key", batteryTestCode + "_" + keys.iterator().next());
        result.set("selectedScenariosResult", selectedScenariosResult);
        return successResult(result);
    }

    private void createSelectedScenarioResultForAllUnits(ArrayNode unitArray, ArrayNode units, Map<String, List<StudentTestSession>> studentTestSessionMap, List<String> batteryUuids) {
        for (String batteryUuid : batteryUuids) {
            List<StudentTestSession> studentTestSessions = studentTestSessionMap.get(batteryUuid);
            for (int i = 0; i < units.size(); i++) {
                JsonNode unit = unitArray.get(i);
                StudentTestSession studentTestSession;
                try {
                    studentTestSession = studentTestSessions.get(i);
                } catch (IndexOutOfBoundsException ex) {
                    ((ObjectNode) unit).put("error", "Student not found. TestCode = " + unit.get("testCode") + ", FormCode= " + unit.get("formCode"));
                    return;
                }
                String uuid = studentTestSession.getStudentTestUuid();
                StudentReservation sr = new StudentReservation();
                sr.setUuid(uuid);
                jpaApi.withTransaction(() -> studentReservationService.create(sr));
                JsonNode uuidsNode = unit.get("uuids");
                if (uuidsNode == null) {
                    ArrayNode uuidsArrayNode = Json.newArray();
                    ObjectNode temp = Json.newObject();
                    temp.put("uuid", uuid);
                    temp.put("batteryUuid", batteryUuid);
                    temp.put("testSessionId", studentTestSession.getSessionId());
                    temp.put("testSessionName", studentTestSession.getSessionName());
                    uuidsArrayNode.add(temp);
                    ((ObjectNode) unit).set("uuids", Json.toJson(uuidsArrayNode));
                } else {
                    ObjectNode temp = Json.newObject();
                    temp.put("uuid", uuid);
                    temp.put("batteryUuid", batteryUuid);
                    temp.put("testSessionId", studentTestSession.getSessionId());
                    temp.put("testSessionName", studentTestSession.getSessionName());
                    ((ArrayNode) uuidsNode).add(temp);
                }
            }
        }
    }

    /**
     * Retrieves the date/time on which the most recent KT test case manifest was processed.
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing the date/time the most recent KT test case manifest was processed.
     */
    @Transactional(readOnly = true)
    public Result getLastKtTestCaseDownloadTime(String apiVersion) {
        Optional<JobExecution> result = jobExecutionService.findLatest("ktTestCaseDownload%");
        String lastKtTestCaseDownloadTime = "N/A";
        if (result.isPresent()) {
            lastKtTestCaseDownloadTime = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a").format(result.get().getCompletionTime());
        }
        return successResult(lastKtTestCaseDownloadTime, "lastKtTestCaseDownloadTime");
    }
}