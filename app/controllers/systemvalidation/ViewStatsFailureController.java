package controllers.systemvalidation;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.SecureController;
import org.json.JSONArray;
import org.json.JSONObject;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import services.systemvalidation.ViewStatsFailureService;
import util.SessionData;

import javax.inject.Inject;
import java.util.List;

public class ViewStatsFailureController extends SecureController {

    private final ViewStatsFailureService viewStatsFailureService;

    @Inject
    ViewStatsFailureController(SessionData.Factory sessionDataFactory, ViewStatsFailureService viewStatsFailureService) {
        super(sessionDataFactory);
        this.viewStatsFailureService = viewStatsFailureService;

    }

    /**
     * Get Failure Report for Stats State
     *
     * @param apiVersion The version of the API to use
     * @return A Result containing failure report.
     */
    @Transactional
    public Result getStatsFailureReport(final String apiVersion) {
        String[] selectedDivJobIdsStrings = request().queryString().get("selectedDivJobIds");
        ArrayNode failed = Json.newArray();
        ObjectNode node;
        List<Object[]> failedStatsFailure = viewStatsFailureService.findStatsFailureReport(java.util.Arrays
                .asList(selectedDivJobIdsStrings));
        if (null != failedStatsFailure && !failedStatsFailure.isEmpty()) {
            for (Object obj[] : failedStatsFailure) {
                if (obj[5] != null && obj[5] != "") {
                    String resultDetail = obj[5].toString();
                    JSONObject object = new JSONObject(resultDetail);
                    if (object.has("studentTestComparisons")) {
                        JSONArray info = object.getJSONArray("studentTestComparisons");
                        JSONObject jsonObject = (JSONObject) info.get(0);
                        if (jsonObject.has("mismatchedFields")) {
                            JSONArray subInfo = jsonObject.getJSONArray("mismatchedFields");
                            for (int i = 0; i < subInfo.length(); i++) {
                                JSONObject subObject = subInfo.getJSONObject(i);
                                node = Json.newObject();
                                node.put("idValue", ((Number) obj[0]).intValue());
                                node.put("batteryValue", obj[1] != null ? obj[1].toString() : "N/A");
                                node.put("studentValue", ((Number) obj[2]).intValue());
                                node.put("formValue", obj[3].toString());
                                node.put("uuidValue", obj[4].toString());
                                node.put("misMatchedValue", subObject.get("fieldName") != null ? subObject.get("fieldName").toString() : "N/A");

                                if (subObject.has("actualValue")) {
                                    node.put("actualValue", subObject.get("actualValue") != null ? subObject.get("actualValue").toString() : "N/A");
                                } else {
                                    node.put("actualValue", "N/A");
                                }

                                if (subObject.has("expectedValue")) {
                                    node.put("expectedValue", subObject.get("expectedValue") != null ? subObject.get("expectedValue").toString() : "N/A");
                                } else {
                                    node.put("actualValue", "N/A");
                                }

                                node.put("resultValue", subObject.get("result") != null ? subObject.get("result").toString() : "N/A");
                                failed.add(node);
                            }
                        }
                    }
                }
            }
        }
        if (failed.elements().hasNext()) {
            return successResult(failed, "failedStats");
        } else {
            return failResult(NOT_FOUND, "no result found");
        }
    }
}