package controllers.systemvalidation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pearson.itautomation.epen2.scoringscenario.creator.Epen2ScoringScenarioCreator;
import com.pearson.itautomation.epen2.scoringscenario.creator.Epen2ScoringScenarioCreator.Epen2ScoringScenarioCreatorResult;
import com.pearson.itautomation.epen2.scoringscenario.database.DbConfigProperty;
import com.pearson.itautomation.epen2.scoringscenario.model.json.Epen2ScoringScenario;
import models.systemvalidation.DbConnection;
import models.systemvalidation.Environment;
import play.Logger;
import util.ConnectionPoolManager;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Nand Joshi
 */
public class EpenScenarioCreator {
    private static final Gson GSON = new GsonBuilder().serializeNulls().create();
    private ConnectionPoolManager connectionPoolManager;
    private Map<String, List<String>> percentCorrectItems;

    @Inject
    public EpenScenarioCreator(ConnectionPoolManager connectionPoolManager) {
        this.connectionPoolManager = connectionPoolManager;
    }

    public List<EpenScenarioResult> readEpen2Scenario(final Environment environment,
                                                      final List<String> epenScenarioClasses, final String scopeTreePath,
                                                      final String testCode, final String formCode) {
        final List<EpenScenarioResult> epenScenarios = new ArrayList<>();
        DbConnection epenDatabaseCon = environment.getEpen2MysqlConnection();
        DbConnection backendDatabaseCon = environment.getBeMysqlConnection();

        DataSource epenDataSource = connectionPoolManager.getMysqlConnectionPool(epenDatabaseCon.getHost(),
                epenDatabaseCon.getDbName(), epenDatabaseCon.getDbUser(), epenDatabaseCon.getDbPass(),
                epenDatabaseCon.getPort());
        DbConfigProperty backendConfigProperty = new DbConfigProperty(backendDatabaseCon.getHost(),
                backendDatabaseCon.getDbName(), backendDatabaseCon.getDbUser(), backendDatabaseCon.getDbPass(),
                backendDatabaseCon.getPort());

        Epen2ScoringScenarioCreator scoringScenarioCreator = new Epen2ScoringScenarioCreator(epenDataSource,
                backendConfigProperty);

        Epen2ScoringScenarioCreatorResult result = scoringScenarioCreator.execute(scopeTreePath, testCode, formCode,
                epenScenarioClasses);
        if (result.isSuccessful()) {
            percentCorrectItems = scoringScenarioCreator.getPercentCorrectItems();
            List<Epen2ScoringScenario> scenarios = result.getScenarios();
            for (Epen2ScoringScenario scenario : scenarios) {
                String epenScenario = GSON.toJson(scenario);
                epenScenarios.add(
                        new EpenScenarioResult(scenario.getScenarioClass(), epenScenario, scenario.getScenarioName(),
                                result.getAdminName(), result.getAccountCode(), result.getApplicationUrl()));

            }
        } else {
            Logger.error(result.getMessages().toString());
            EpenScenarioResult errorResult = new EpenScenarioResult(null, null, null, result.getAdminName(), result.getAccountCode(), result.getApplicationUrl());
            errorResult.addError(result.getMessages().toString());
            epenScenarios.add(errorResult);
        }
        return epenScenarios;
    }

    /**
     * @return the percentCorrectItems
     */
    public final Map<String, List<String>> getPercentCorrectItems() {
        return percentCorrectItems;
    }

    public class EpenScenarioResult {
        private final String scenarioClass;
        private final String scenarioName;
        private final String administration;
        private final String accountCode;
        private final String epenApplicationUrl;
        private final List<String> errors;
        private String scenarioJson;

        /**
         * @param scenarioClass
         * @param scenarioJson
         * @param scenarioName
         */
        public EpenScenarioResult(final String scenarioClass, final String scenarioJson, final String scenarioName,
                                  final String administration, final String accountCode, final String epenApplicationUrl) {
            this.scenarioClass = scenarioClass;
            this.scenarioJson = scenarioJson;
            this.scenarioName = scenarioName;
            this.administration = administration;
            this.accountCode = accountCode;
            this.epenApplicationUrl = epenApplicationUrl;
            this.errors = new ArrayList<>();
        }

        /**
         * @return the scenarioClass
         */
        public final String getScenarioClass() {
            return scenarioClass;
        }

        /**
         * @return the scenarioJson
         */
        public final String getScenarioJson() {
            return scenarioJson;
        }

        public void setScenarioJson(String scenarioJson) {
            this.scenarioJson = scenarioJson;
        }

        /**
         * @return the scenarioName
         */
        public final String getScenarioName() {
            return scenarioName;
        }

        /**
         * @return the administration
         */
        public final String getAdministration() {
            return administration;
        }

        /**
         * @return the accountCode
         */
        public final String getAccountCode() {
            return accountCode;
        }

        /**
         * @return the epenApplicationUrl
         */
        public final String getEpenApplicationUrl() {
            return epenApplicationUrl;
        }

        public List<String> getErrors() {
            return errors;
        }

        public void addError(String errorMessage) {
            this.errors.add(errorMessage);
        }
    }
}
