package controllers.systemvalidation;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import models.systemvalidation.Environment;
import org.apache.commons.lang3.StringUtils;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import services.systemvalidation.EnvironmentService;
import services.systemvalidation.UsageStatisticsService;
import util.SessionData;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UsageStatisticsController extends SecureController {

    private final UsageStatisticsService usageStatisticsService;
    private static final String USAGE_STATISTICS = "usageStatistics";
    private final EnvironmentService environmentService;

    @Inject
    UsageStatisticsController(SessionData.Factory sessionDataFactory, UsageStatisticsService usageStatisticsService,
            EnvironmentService environmentService) {
        super(sessionDataFactory);
        this.usageStatisticsService = usageStatisticsService;
        this.environmentService = environmentService;

    }

    /**
     * Get usage statistics data for selected environment scopeTreePath for the given duration
     *
     * @param apiVersion
     *            The version of the API to use
     * @param environmentId
     *            The environment Id
     * @param scopeTreePath
     *            The scopeTreePath for selected environment
     * @param sTime
     *            The Start date
     * @param eTime
     *            The End date
     * @return A Result containing usage statistics data.
     */
    @Transactional
    public Result getTestAttempts(final String apiVersion, final Integer environmentId, final String scopeTreePath,
            final String sTime, final String eTime) {
        try {
            enforceEnvironmentLevelPermission(USAGE_STATISTICS, AccessType.READ, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        Timestamp startTime = new Timestamp(Long.parseLong(sTime));
        Timestamp endTime = new Timestamp(Long.parseLong(eTime));
        int totalTestAttempts = usageStatisticsService.findAllTestAttempts(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        int passedTestAttempts = usageStatisticsService.findAllPassedTestAttempts(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        int failedTestAttempts = usageStatisticsService.findAllFailedTestAttempts(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        int passedResumedTestAttempts = usageStatisticsService.findAllPassedResumedTestAttempts(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        int failedResumedTestAttempts = usageStatisticsService.findAllFailedResumedTestAttempts(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        int uniqueUsers = usageStatisticsService.findAllUniqueUsers(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        int uniqueForms = usageStatisticsService.findAllUniqueForms(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        List<Object[]> failedTestAttemptsForState = usageStatisticsService.findFailedTestAttemptsForState(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        List<Object[]> testAttemptsForForm = usageStatisticsService.findTestAttemptsForForm(environmentId,
                StringUtils.replace(scopeTreePath, "|", "/"), startTime, endTime);
        ArrayNode overAllStatistics = Json.newArray();
        ObjectNode node = Json.newObject();
        node.put("key", "Total Test Attempts");
        node.put("value", totalTestAttempts);
        overAllStatistics.add(node);
        node = Json.newObject();
        node.put("key", "Passed Test Attempts");
        node.put("value", passedTestAttempts);
        overAllStatistics.add(node);
        node = Json.newObject();
        node.put("key", "Failed Test Attempts");
        node.put("value", failedTestAttempts);
        overAllStatistics.add(node);
        node = Json.newObject();
        node.put("key", "Passed Resumed Test Attempts");
        node.put("value", passedResumedTestAttempts);
        overAllStatistics.add(node);
        node = Json.newObject();
        node.put("key", "Failed Resumed Test Attempts");
        node.put("value", failedResumedTestAttempts);
        overAllStatistics.add(node);
        node = Json.newObject();
        node.put("key", "Unique Users");
        node.put("value", uniqueUsers);
        overAllStatistics.add(node);
        node = Json.newObject();
        node.put("key", "Unique Forms");
        node.put("value", uniqueForms);
        overAllStatistics.add(node);
        ArrayNode failedTestAttemptsPerState = Json.newArray();
        if (null != failedTestAttemptsForState && !failedTestAttemptsForState.isEmpty()) {
            for (Object obj[] : failedTestAttemptsForState) {
                node = Json.newObject();
                node.put("key", obj[0].toString());
                node.put("value", ((Number) obj[1]).intValue());
                failedTestAttemptsPerState.add(node);
            }
        }
        ArrayNode testAttemptsPerForm = Json.newArray();
        if (null != testAttemptsForForm && !testAttemptsForForm.isEmpty()) {
            for (Object obj[] : testAttemptsForForm) {
                node = Json.newObject();
                node.put("key", obj[0].toString());
                node.put("value", ((Number) obj[1]).intValue());
                testAttemptsPerForm.add(node);
            }
        }
        Map<String, Object> finalMap = new HashMap<>();
        finalMap.put("overAllStatistics", overAllStatistics);
        finalMap.put("failedTestAttemptsPerState", failedTestAttemptsPerState);
        finalMap.put("testAttemptsPerForm", testAttemptsPerForm);
        return successResult(finalMap, "finalMap");
    }

    /**
     * Get all environments
     *
     * @param apiVersion
     *            The version of the API to use
     * @return A Result containing the list of environments
     */
    @Transactional
    public Result getEnvironments(final String apiVersion) {
        List<Environment> envList = environmentService.findAllEnvironmentsWithDivJob();
        ArrayNode envArrays = Json.newArray();
        ObjectNode node;
        if (envList.size() > 0) {
            for (Environment obj : envList) {
                node = Json.newObject();
                node.put("name", obj.getName());
                node.put("environmentId", obj.getEnvironmentId());
                node.put("archived", obj.getArchived());
                envArrays.add(node);
            }
        }
        return successResult(envArrays, "environments");
    }

    /**
     * Get environment for given environmentId
     *
     * @param apiVersion
     *            The version of the API to use
     * @param environmentId
     *            The environment Id
     * @return A Result containing the environment for given environmentId
     */
    @Transactional
    public Result getEnvironment(final String apiVersion, final Integer environmentId) {
        Optional<Environment> env = environmentService.findByIdWithDivJob(environmentId);
        ObjectNode node = Json.newObject();
        if (env.isPresent()) {
            node.put("name", env.get().getName());
            node.put("environmentId", env.get().getEnvironmentId());
        } else {
            return failResult(NOT_FOUND, "Unable to find environment for the given id");
        }
        return successResult(node, "environment");
    }

    /**
     * Get all scopeTreePaths
     *
     * @param apiVersion
     *            The version of the API to use
     * @param environmentId
     *            The environment Id
     * @return A Result containing scopeTreePaths for given environmentId
     */
    @Transactional
    public Result getScopeTreePaths(final String apiVersion, final Integer environmentId) {
        return successResult(usageStatisticsService.findAllScopeTreePath(environmentId), "scopeTreePaths");
    }
}
