package controllers.systemvalidation;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.SecureController;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import services.systemvalidation.DivjobFormsService;
import services.systemvalidation.EnvironmentService;
import util.SessionData;

import javax.inject.Inject;
import java.util.List;

public class DivjobFormsController extends SecureController {

    private final DivjobFormsService divjobFormsService;

    @Inject
    DivjobFormsController(SessionData.Factory sessionDataFactory, DivjobFormsService divjobFormsService,
                          EnvironmentService environmentService) {
        super(sessionDataFactory);
        this.divjobFormsService = divjobFormsService;

    }

    /**
     * Get DIV Job forms for the given environment ID
     *
     * @param apiVersion    The version of the API to use
     * @param environmentId The environment Id
     * @return A Result containing the DIV Job forms for the given environment ID
     */
    @Transactional
    public Result getDivjobForms(final String apiVersion, final Integer environmentId) {
        List<Object[]> divjobForms = divjobFormsService.getDivjobForms(environmentId);
        ArrayNode divjobFormsArray = Json.newArray();

        for (Object divjobForm[] : divjobForms) {
            ObjectNode node = Json.newObject();
            node.put("divjobId", divjobForm[0].toString());
            node.put("testCode", divjobForm[1].toString());
            node.put("formCode", divjobForm[2].toString());
            node.put("testnavScenarios", divjobForm[3].toString());
            divjobFormsArray.add(node);
        }
        return successResult(divjobFormsArray, "divjobForms");
    }
}