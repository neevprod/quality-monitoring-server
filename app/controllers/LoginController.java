package controllers;

import com.auth0.jwt.JWTSigner;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import models.ApiUser;
import models.HumanUser;
import models.LoginUser;
import models.UserPermissions;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.ApiUserService;
import services.usermanagement.HumanUserService;
import services.usermanagement.UserPermissionsService;
import util.ActiveDirectoryAuthenticator;
import util.UserPermissionData;

import javax.inject.Inject;
import javax.inject.Provider;
import java.time.Clock;
import java.util.*;

public class LoginController extends BaseController {

    private final int sessionLengthSeconds;
    private final String applicationSecret;
    private final Clock clock;
    private final HumanUserService humanUserService;
    private final ApiUserService apiUserService;
    private final UserPermissionsService userPermissionsService;
    private final FormFactory formFactory;
    private final ActiveDirectoryAuthenticator adAuth;

    @Inject
    LoginController(Config configuration, Clock clock, HumanUserService humanUserService,
                    ApiUserService apiUserService, UserPermissionsService userPermissionsService, FormFactory formFactory,
                    Provider<ActiveDirectoryAuthenticator> adAuthProvider) {
        adAuth = adAuthProvider.get();
        this.sessionLengthSeconds = (int) configuration.getDuration("session.timeout").getSeconds();
        this.applicationSecret = configuration.getString("play.http.secret.key");
        this.clock = clock;
        this.humanUserService = humanUserService;
        this.apiUserService = apiUserService;
        this.userPermissionsService = userPermissionsService;
        this.formFactory = formFactory;
    }

    /**
     * Authenticates the user based on the given credentials.
     *
     * @param apiVersion The version of the API to use.
     * @return A Result containing the session token and user permissions list.
     */
    @Transactional(readOnly = true)
    @BodyParser.Of(BodyParser.Json.class)
    public Result login(String apiVersion) {

        JsonNode body = request().body().asJson();
        if (body.get("networkId") != null) {
            Form<LoginUser> humanUserForm = formFactory.form(LoginUser.class).bindFromRequest();
            if (humanUserForm.hasErrors()) {
                Logger.info("event=\"Invalid token request.\"");
                String message;
                message = "NetworkId and password are required.";
                return failResult(BAD_REQUEST, message);
            }
            return getHumanUserResult(humanUserForm.get());
        } else if (body.get("apiKeyName") != null) {
            Form<ApiUser> apiUserForm = formFactory.form(ApiUser.class).bindFromRequest();
            if (apiUserForm.hasErrors()) {
                Logger.info("event=\"Invalid token request.\"");
                return failResult(BAD_REQUEST, "API key name and API key are required.");
            }
            return getApiUserResult(apiUserForm.get());
        } else {
            Logger.info("event=\"Invalid token request.\"");
            return failResult(BAD_REQUEST, "NetworkId or API key name is required.");
        }
    }

    /**
     * Gets a token result for a requested human user.
     *
     * @param requestedHumanUser The requested human user.
     * @return A Result containing the token information.
     */
    private Result getHumanUserResult(LoginUser requestedHumanUser) {

        Optional<HumanUser> humanUserOptional = humanUserService.findByNetworkId(requestedHumanUser.getNetworkId());

        if (humanUserOptional.isPresent()
                && adAuth.authenticate(requestedHumanUser.getNetworkId(), requestedHumanUser.getPassword())) {
            HumanUser humanUser = humanUserOptional.get();
            List<UserPermissions> userPermissions = null;
            Map<String, Object> jwtClaims = new HashMap<>();

            jwtClaims.put("userId", humanUser.getUser().getId());
            jwtClaims.put("userEmail", humanUser.getEmail());
            jwtClaims.put("userName", humanUser.getUser().getFullname());
            jwtClaims.put("apiUser", false);

            UserPermissionData userPermissionData;
            if (humanUser.getUser().getIsAdmin()) {
                userPermissionData = new UserPermissionData(true, new ArrayList<>());
            } else {
                userPermissions = userPermissionsService.findAllByUserId(humanUser.getUser().getId());
                userPermissionData = new UserPermissionData(false, userPermissions);
            }
            jwtClaims.put("userPermissions", userPermissionData);

            return getSessionResult(jwtClaims, humanUser.getUser().getId(), humanUser.getUser().getIsAdmin(),
                    userPermissionData.getJson());
        } else {
            Logger.info("event=\"Invalid login credentials for human user.\"");
            return failResult(UNAUTHORIZED, "Invalid networkId and password combination.");
        }
    }

    /**
     * Gets a token result for a requested API user.
     *
     * @param requestedApiUser The requested API user.
     * @return A Result containing the token information.
     */
    private Result getApiUserResult(ApiUser requestedApiUser) {
        Optional<ApiUser> apiUserOptional = apiUserService.findByApiKeyName(requestedApiUser.getApiKeyName());

        if (apiUserOptional.isPresent() && apiUserOptional.get().getApiKey().equals(requestedApiUser.getApiKey())) {
            ApiUser apiUser = apiUserOptional.get();
            List<UserPermissions> userPermissions = null;
            Map<String, Object> jwtClaims = new HashMap<>();

            jwtClaims.put("userId", apiUser.getUser().getId());
            jwtClaims.put("userName", apiUser.getUser().getFullname());
            jwtClaims.put("apiUser", true);

            UserPermissionData userPermissionData;
            if (apiUser.getUser().getIsAdmin()) {
                userPermissionData = new UserPermissionData(true, new ArrayList<>());
            } else {
                userPermissions = userPermissionsService.findAllByUserId(apiUser.getUser().getId());
                userPermissionData = new UserPermissionData(false, userPermissions);
            }
            jwtClaims.put("userPermissions", userPermissionData);

            return getSessionResult(jwtClaims, apiUser.getUser().getId(), apiUser.getUser().getIsAdmin(),
                    userPermissionData.getJson());
        } else {
            Logger.info("event=\"Invalid login credentials for API user.\"");
            return failResult(UNAUTHORIZED, "Invalid API key name and API key combination.");
        }
    }

    /**
     * Constructs a session token and permissions list with the given JWT claims.
     *
     * @param jwtClaims           The JWT claims.
     * @param userPermissionsJson JSON representing the user permissions.
     * @return A Result containing the session token and user permissions list.
     */
    private Result getSessionResult(Map<String, Object> jwtClaims, long userId, boolean isAdmin,
                                    JsonNode userPermissionsJson) {
        String jwtId = UUID.randomUUID().toString();
        long currentTimeSeconds = clock.millis() / 1000L;
        jwtClaims.put("jti", jwtId);
        jwtClaims.put("iat", currentTimeSeconds);
        jwtClaims.put("exp", currentTimeSeconds + sessionLengthSeconds);

        JWTSigner jwtSigner = new JWTSigner(applicationSecret);
        String jwt = jwtSigner.sign(jwtClaims);

        Logger.info("event=\"Session created.\", userId=\"{}\", sessionId=\"{}\"", userId, jwtId);

        ObjectNode resultData = Json.newObject();
        resultData.put("token", jwt);
        resultData.set("userPermissions", userPermissionsJson);
        return successResult(resultData);
    }
}
