package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import global.Authenticator;
import global.exceptions.QaApplicationPermissionException;
import play.Logger;
import play.mvc.Security;
import util.SessionData;

import javax.inject.Inject;

/**
 * All controllers that need users to be authenticated should extend this controller.
 * It will authenticate that the user is logged in before any request is fulfilled.
 * If a controller does not need authentication, it should instead extend BaseController.
 */
@Security.Authenticated(Authenticator.class)
public abstract class SecureController extends BaseController {
    private final SessionData.Factory sessionDataFactory;

    public enum Module {
        QTI_VALIDATION("qtiValidation"),
        QTI_RESP_GEN("qtiRespGen"),
        SYSTEM_VALIDATION("systemValidation"),
        USER_MANAGEMENT("userManagement"),
        GRID_AUTOMATION("gridAutomation");

        private final String name;

        /**
         * Create a module with the given name.
         *
         * @param name The module name.
         */
        Module(String name) {
            this.name = name;
        }

        /**
         * Get the module name.
         *
         * @return The module name.
         */
        public String getName() {
            return name;
        }
    }

    public enum AccessType {
        CREATE, READ, UPDATE, DELETE
    }

    @Inject
    protected SecureController(SessionData.Factory sessionDataFactory) {
        this.sessionDataFactory = sessionDataFactory;
    }

    /**
     * Get the session data that is stored in the context.
     *
     * @return The session data.
     */
    protected SessionData sessionData() {
        return sessionDataFactory.create((JsonNode) ctx().args.get("session"));
    }

    /**
     * Throws an ApplicationPermissionException if the current user doesn't have permission for the given access type
     * of the given feature for the given module.
     *
     * @param permissionName The name of the permission to check.
     * @param accessType The access type to check.
     * @param moduleName The module name.
     * @throws QaApplicationPermissionException
     */
    protected void enforceModuleLevelPermission(String permissionName, AccessType accessType, String moduleName)
            throws QaApplicationPermissionException {
        if (!sessionData().hasModuleLevelPermission(permissionName, accessType, moduleName)) {
            Logger.info("event=\"Insufficient permission to perform request.\"");
            throw new QaApplicationPermissionException();
        }
    }

    /**
     * Throws an ApplicationPermissionException if the current user doesn't have permission for the given access type
     * of the given feature for the given tenant in the given module.
     *
     * @param permissionName The name of the permission to check.
     * @param accessType The access type to check.
     * @param moduleName The module name.
     * @param previewerId The previewer the tenant is under.
     * @param tenantId The tenant the action is being applied to.
     * @throws QaApplicationPermissionException
     */
    protected void enforceTenantLevelPermission(String permissionName, AccessType accessType, String moduleName,
                                                Integer previewerId, Long tenantId)
            throws QaApplicationPermissionException {
        if (!sessionData().hasTenantLevelPermission(permissionName, accessType, moduleName, previewerId, tenantId)) {
            Logger.info("event=\"Insufficient permission to perform request.\"");
            throw new QaApplicationPermissionException();
        }
    }

    /**
     * Throws an ApplicationPermissionException if the current user doesn't have permission for the given access type
     * of the given feature for at least one tenant under the given previewer in the given module.
     *
     * @param permissionName The name of the permission to check.
     * @param accessType The access type to check.
     * @param moduleName The module name.
     * @param previewerId The previewer to check.
     * @throws QaApplicationPermissionException
     */
    protected void enforceTenantLevelPermission(String permissionName, AccessType accessType, String moduleName,
                                                Integer previewerId)
            throws QaApplicationPermissionException {
        if (!sessionData().hasAnyTenantLevelPermission(permissionName, accessType, moduleName, previewerId)) {
            Logger.info("event=\"Insufficient permission to perform request.\"");
            throw new QaApplicationPermissionException();
        }
    }

    /**
     * Throws an ApplicationPermissionException if the current user doesn't have permission for the given access type
     * of the given feature for the given environment in the given module.
     *
     * @param permissionName The name of the permission to check.
     * @param accessType The access type to check.
     * @param moduleName The module name.
     * @throws QaApplicationPermissionException
     */
    protected void enforceEnvironmentLevelPermission(String permissionName, AccessType accessType, String moduleName,
                                                     Integer environmentId)
            throws QaApplicationPermissionException {
        if (!sessionData().hasEnvironmentLevelPermission(permissionName, accessType, moduleName, environmentId)) {
            Logger.info("event=\"Insufficient permission to perform request.\"");
            throw new QaApplicationPermissionException();
        }
    }

    /**
     * Throws an ApplicationPermissionException if the current user doesn't have permission for the given access type
     * of the given feature for at least one environment in the given module.
     *
     * @param permissionName The name of the permission to check.
     * @param accessType The access type to check.
     * @param moduleName The module name.
     * @throws QaApplicationPermissionException
     */
    protected void enforceEnvironmentLevelPermission(String permissionName, AccessType accessType, String moduleName)
            throws QaApplicationPermissionException {
        if (!sessionData().hasAnyEnvironmentLevelPermission(permissionName, accessType, moduleName)) {
            Logger.info("event=\"Insufficient permission to perform request.\"");
            throw new QaApplicationPermissionException();
        }
    }
}
