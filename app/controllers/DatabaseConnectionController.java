package controllers;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.management.ConnectionPoolStatisticsMBean;
import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import play.libs.Json;
import play.mvc.Result;
import util.ConnectionPoolManager;
import util.SessionData;

import javax.inject.Inject;
import javax.management.JMX;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Set;

public class DatabaseConnectionController extends SecureController {
    private final ConnectionPoolManager connectionPoolManager;
    private final MBeanServer mBeanServer;
    private final String defaultDbUrl;
    private final String defaultDbUsername;

    @Inject
    protected DatabaseConnectionController(SessionData.Factory sessionDataFactory, ConnectionPoolManager
            connectionPoolManager,
                                           Config configuration) {
        super(sessionDataFactory);
        this.connectionPoolManager = connectionPoolManager;
        this.defaultDbUrl = configuration.getString("db.default.url").split("\\?")[0];
        this.defaultDbUsername = configuration.getString("db.default.username");
        mBeanServer = ManagementFactory.getPlatformMBeanServer();
    }

    /**
     * Get information about the currently existing DB connection pools.
     *
     * @param apiVersion The API version.
     * @return Result containing DB connection pool information.
     */
    public Result getDatabaseConnectionPools(String apiVersion) {
        if (!sessionData().isAdmin()) {
            return accessDeniedResult();
        }

        ObjectNode connectionPoolsJson = Json.newObject();
        ArrayNode mySqlConnectionPoolsJson = connectionPoolsJson.putArray("mySqlConnectionPools");
        ArrayNode mongoConnectionPoolsJson = connectionPoolsJson.putArray("mongoConnectionPools");

        // Get information about default DB connection pool.
        ObjectNode defaultConnectionPoolJson = mySqlConnectionPoolsJson.addObject();
        defaultConnectionPoolJson.put("url", defaultDbUrl);
        defaultConnectionPoolJson.put("username", defaultDbUsername);
        try {
            ObjectName poolName = new ObjectName("com.zaxxer.hikari:type=Pool (defaultPool)");
            HikariPoolMXBean poolProxy = JMX.newMBeanProxy(mBeanServer, poolName, HikariPoolMXBean.class);
            defaultConnectionPoolJson.put("totalConnections", poolProxy.getTotalConnections());
            defaultConnectionPoolJson.put("activeConnections", poolProxy.getActiveConnections());
        } catch (MalformedObjectNameException e) {
            throw new RuntimeException(e);
        }

        // Get information about MySQL DB connection pools.
        Set<HikariDataSource> mysqlDataSources = connectionPoolManager.getAllMysqlConnectionPools();
        try {
            for (HikariDataSource dataSource : mysqlDataSources) {
                if (dataSource.getPoolName() != null) {
                    ObjectNode connectionPoolJson = mySqlConnectionPoolsJson.addObject();
                    connectionPoolJson.put("url", dataSource.getJdbcUrl());
                    connectionPoolJson.put("username", dataSource.getUsername());
                    connectionPoolJson.put("password", dataSource.getPassword());

                    ObjectName poolName = new ObjectName(
                            "com.zaxxer.hikari:type=Pool (" + dataSource.getPoolName() + ")");

                    HikariPoolMXBean poolProxy = JMX.newMBeanProxy(mBeanServer, poolName, HikariPoolMXBean.class);
                    connectionPoolJson.put("totalConnections", poolProxy.getTotalConnections());
                    connectionPoolJson.put("activeConnections", poolProxy.getActiveConnections());
                }
            }
        } catch (MalformedObjectNameException e) {
            throw new RuntimeException(e);
        }

        // Get information about Mongo DB connection pools.
        try {
            ObjectName mongoPoolNamePattern = new ObjectName("org.mongodb.driver:type=ConnectionPool,*");
            Set<ObjectName> objectNames = mBeanServer.queryNames(mongoPoolNamePattern, null);
            for (ObjectName objectName : objectNames) {
                ConnectionPoolStatisticsMBean poolProxy = JMX.newMBeanProxy(mBeanServer, objectName,
                        ConnectionPoolStatisticsMBean.class);
                ObjectNode connectionPoolJson = mongoConnectionPoolsJson.addObject();
                connectionPoolJson.put("url", poolProxy.getHost());
                connectionPoolJson.put("totalConnections", poolProxy.getSize());
                connectionPoolJson.put("activeConnections", poolProxy.getCheckedOutCount());
            }
        } catch (MalformedObjectNameException e) {
            throw new RuntimeException(e);
        }

        return successResult(connectionPoolsJson);
    }
}
