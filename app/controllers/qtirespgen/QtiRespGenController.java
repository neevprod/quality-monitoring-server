package controllers.qtirespgen;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import play.Logger;
import play.mvc.BodyParser;
import play.mvc.Result;
import util.SessionData;
import util.qtirespgen.QtiResponseGenerator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class QtiRespGenController extends SecureController {
    private final QtiResponseGenerator qtiResponseGenerator;

    @Inject
    QtiRespGenController(SessionData.Factory sessionDataFactory, QtiResponseGenerator qtiResponseGenerator) {
        super(sessionDataFactory);
        this.qtiResponseGenerator = qtiResponseGenerator;
    }

    /**
     * Generate responses based on the information passed in the request body.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @return A Result containing a list of generated responses.
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result generateResponses(String apiVersion) {
        Logger.info("event=\"Generating responses for item.\"");

        try {
            enforceModuleLevelPermission("responses", AccessType.READ, Module.QTI_RESP_GEN.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode body = request().body().asJson();

        List<String> errors = new ArrayList<>();

        String itemXml = null;
        if (body.get("itemXml") != null) {
            itemXml = body.get("itemXml").asText();
        } else {
            errors.add("itemXml is required.");
        }

        int maxCorrect = 1;
        if (body.get("maxCorrect") != null) {
            if (body.get("maxCorrect").isIntegralNumber() && body.get("maxCorrect").asInt() >= 0) {
                maxCorrect = body.get("maxCorrect").asInt();
            } else {
                errors.add("maxCorrect must be a non-negative integer.");
            }
        }

        int maxIncorrect = 1;
        if (body.get("maxIncorrect") != null) {
            if (body.get("maxIncorrect").isIntegralNumber() && body.get("maxIncorrect").asInt() >= 0) {
                maxIncorrect = body.get("maxIncorrect").asInt();
            } else {
                errors.add("maxIncorrect must be a non-negative integer.");
            }
        }

        int outputType = 0;
        if (body.get("outputType") != null) {
            if (body.get("outputType").isIntegralNumber() && body.get("outputType").asInt() >= 0
                    && body.get("outputType").asInt() <= 2) {
                outputType = body.get("outputType").asInt();
            } else {
                errors.add("outputType must be 0, 1, or 2.");
            }
        }

        boolean removeOeScores = true;
        if (body.get("removeOeScores") != null) {
            removeOeScores = body.get("removeOeScores").asBoolean();
        }

        if (!errors.isEmpty()) {
            Logger.info("event=\"Invalid request for Generating QTI Responses.\"");
            return failResult(BAD_REQUEST, errors);
        }

        Logger.info("event=\"Generating QTI Responses.\"");
        JsonNode returnJson = qtiResponseGenerator.generateResponses(itemXml, maxCorrect, maxIncorrect, outputType,
                removeOeScores);
        return jsonResult(OK, returnJson);
    }

    /**
     * Provides the REST API to get outcomes for the given item xml and responses.
     * 
     * @param apiVersion
     *            The version of the API to use.
     * @return A Result containing a list of response outcomes.
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result getResponseOutcome(final String apiVersion) {
        Logger.info("event=\"Generating outcomes for an item.\"");

        try {
            enforceModuleLevelPermission("responses", AccessType.READ, Module.QTI_RESP_GEN.getName());
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode body = request().body().asJson();

        List<String> errors = new ArrayList<>();

        String itemXml = null;
        if (body.get("itemXml") != null) {
            itemXml = body.get("itemXml").asText();
        } else {
            Logger.error("event= \"Item XML is required.\"");
            errors.add("Item XML is required.");
        }
        ArrayNode responses = null;
        if (body.get("responses") != null) {
            responses = (ArrayNode) body.get("responses");
        } else {
            Logger.error("event= \"No responses found to generate outcome.\"");
            errors.add("No responses found to generate outcome.");
        }

        if (!errors.isEmpty()) {
            return failResult(BAD_REQUEST, errors);
        }

        QtiResponseGenerator.ResponseOutcomeResult result = qtiResponseGenerator.getResponseOutcomes(itemXml,
                responses);
        if (result.isSuccess()) {
            result.getData();
            return successResult(result.getData());
        } else {
            return failResult(BAD_REQUEST, result.getErrors());
        }
    }
}
