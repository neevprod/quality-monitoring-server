package controllers;

import com.typesafe.config.Config;
import play.Logger;
import play.api.libs.mailer.MailerClient;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.data.validation.Constraints;
import play.libs.mailer.Email;
import play.mvc.BodyParser;
import play.mvc.Result;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.ArrayList;
import java.util.List;

public class IssueReportController extends BaseController {
    private final List<String> issueReportRecipients;
    private final FormFactory formFactory;
    private final Provider<Email> emailProvider;
    private final MailerClient mailer;
    private final String fromAddress;

    @Inject
    IssueReportController(FormFactory formFactory, Provider<Email> emailProvider, MailerClient mailer,
                          Config configuration) {
        this.formFactory = formFactory;
        this.emailProvider = emailProvider;
        this.mailer = mailer;
        this.fromAddress = configuration.getString("application.email.fromAddress");
        this.issueReportRecipients = configuration.getStringList("issue.report.recipients");
    }

    /**
     * This will email an issue report based on the form that the user filled out. The emailed
     * report will include the error ID, the user's email address (if they included it), and the
     * user's comments.
     *
     * @param apiVersion The version of the API to use.
     * @return a Result indicating whether the operation was successful
     */
    @BodyParser.Of(BodyParser.Json.class)
    public Result reportIssue(String apiVersion) {
        String email = null;
        String description = null;
        String errorId = null;

        try {
            DynamicForm requestData = formFactory.form().bindFromRequest();
            email = requestData.get("email");
            description = requestData.get("description");
            errorId = requestData.get("errorId");

            // Do form validation.
            List<String> errors = new ArrayList<>();
            if (description == null || description.isEmpty()) {
                Logger.info("event=\"Description missing from issue report.\"");
                errors.add("Please include a description of the issue.");
            }
            if (email != null && !email.isEmpty() && !(new Constraints.EmailValidator().isValid(email))) {
                Logger.info("event=\"Invalid email address in issue report.\"");
                errors.add("Please enter a valid email address.");
            }
            if (!errors.isEmpty()) {
                return failResult(BAD_REQUEST, errors);
            }

            // Send email.
            Logger.info("event=\"Sending issue report.\", errorId=\"{}\"", errorId);
            Email mail = constructIssueReportEmail(errorId, description, email);
            mailer.send(mail);

            return successResult();
        } catch (Exception e) {
            Logger.error("event=\"Exception occurred while sending issue report.\", errorId=\"{}\"", errorId, e);
            return successResult();
        }
    }

    /**
     * Construct an issue report email.
     *
     * @param errorId the ID of the error the email is about
     * @param description the user's description of the issue
     * @param emailAddress the user's email address
     * @return the email
     */
    private Email constructIssueReportEmail(String errorId, String description, String emailAddress) {
        Email email = emailProvider.get();

        email.setSubject("Issue Report from Quality Monitoring Web App - " + errorId);
        email.setFrom(fromAddress);
        issueReportRecipients.stream().forEach(email::addTo);

        StringBuilder sb = new StringBuilder();
        sb.append("<p><b>Error ID:</b> ");
        sb.append(errorId);
        sb.append("</p>");
        if (emailAddress != null && !emailAddress.isEmpty()) {
            sb.append("<p><b>User email address:</b> ");
            sb.append(emailAddress);
            sb.append("</p>");
        }
        sb.append("<p><b>Issue description:</b></p>");
        sb.append("<p>");
        sb.append(description.replaceAll("\\n", "<br/>"));
        sb.append("</p>");
        email.setBodyHtml(sb.toString());

        return email;
    }

}
