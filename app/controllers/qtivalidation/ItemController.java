package controllers.qtivalidation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pearson.itautomation.tn8.previewer.api.GetTenantItemsResult;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapItemsResult;
import com.pearson.itautomation.tn8.previewer.api.GetXMLByIdentifierResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import util.SessionData;
import util.qtivalidation.TestNav8APIFactory;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ItemController extends SecureController {
    private static final String ITEMS = "items";
    private static final int ITEMS_TO_GET = 500;
    private final TestNav8APIFactory apiFactory;

    @Inject
    ItemController(SessionData.Factory sessionDataFactory, TestNav8APIFactory apiFactory) {
        super(sessionDataFactory);
        this.apiFactory = apiFactory;
    }

    /**
     * Get the list of items associated with the given tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId    the tenant ID
     * @return a Result containing a list of items
     */
    @Transactional(readOnly = true)
    public Result getItems(String apiVersion, int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving list of items for tenant.\", previewerId=\"{}\", tenantId=\"{}\"",
                previewerId, tenantId);

        try {
            enforceTenantLevelPermission(ITEMS, AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND, "Unable to find a previewer with the given ID.");
        } else {
            Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = getParamMap();
            GetTenantItemsResult result;
            try {
                result = apiOptional.get().getTenantItems((int) tenantId, paramMap);
            } catch (NullPointerException e) {
                // The API library throws a NullPointerException if the given tenant is not found.
                Logger.info("event=\"Tenant not found when calling TN8 get items API.\", previewerId=\"{}\", tenantId=\"{}\"",

                        previewerId, tenantId);
                return failResult(NOT_FOUND, "Unable to find a tenant with the given ID.");
            }

            ObjectNode data = Json.newObject();
            data.put("totalResults", result.getTotalItemsAfterFilter());
            data.set(ITEMS, Json.toJson(result.getItems()));
            return successResult(data);
        }
    }

    /**
     * Get the list of items associated with the given tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId    the tenant ID
     * @param formId      id of form
     * @return a Result containing a list of items for testMap
     */
    @Transactional(readOnly = true)
    public Result getItemsForTestMap(String apiVersion, int previewerId, long tenantId, String formIdentifier, String formId) {
        Logger.info("event=\"Retrieving list of items for testMap.\", previewerId=\"{}\", tenantId=\"{}\", formId=\"{}\"",
                previewerId, tenantId, formId);
        try {
            enforceTenantLevelPermission(ITEMS, AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND, "Unable to find a previewer with the given ID.");
        } else {
            GetTestMapItemsResult result;
            try {
                result = apiOptional.get().getItemsForTestmap((int) tenantId, formIdentifier, formId);
            } catch (NullPointerException e) {
                // The API library throws a NullPointerException if the given tenant is not found.
                Logger.info("event=\"Tenant not found when calling TN8 get items API.\", previewerId=\"{}\", tenantId=\"{}\"",

                        previewerId, tenantId);
                return failResult(NOT_FOUND, "Unable to retrieve the items for formId :: " + formId + ", testmapIdentifier :: " + formIdentifier + " in tenant :: " + tenantId);
            }

            ObjectNode data = Json.newObject();
            data.put("totalResults", result.getTotalItemsAfterFilter());
            data.set(ITEMS, Json.toJson(result.getItems()));
            return successResult(data);
        }
    }

    /**
     * From the values included in the query string, build a param map that can be used in the get items API call.
     *
     * @return The param map.
     */
    private Map<GetTenantItemsResult.GetTenantItemsParam, String> getParamMap() {
        Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = new HashMap<>();

        final String startIndex = "startIndex";
        final String numberToShow = "numberToShow";
        final String sortColumn = "sortColumn";
        final String search = "search";
        final String testCaseFilter = "testCaseFilter";
        final String fingerprintFilter = "fingerprintFilter";
        final String itemIds = "itemIds";

        String[] items = request().queryString().get(itemIds);
        if (request().getQueryString(startIndex) != null) {
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_START,
                    request().getQueryString(startIndex));
        }
        if (request().getQueryString(numberToShow) != null) {
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_LENGTH,
                    request().getQueryString(numberToShow));
        }
        if (request().getQueryString(sortColumn) != null) {
            if ("true".equals(request().getQueryString("sortReversed"))) {
                paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ORDER_BY_CLAUSE,
                        request().getQueryString(sortColumn) + " desc");
            } else {
                paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ORDER_BY_CLAUSE,
                        request().getQueryString(sortColumn) + " asc");
            }
        }
        if (items != null) {
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS, StringUtils.join(items, ","));
        }
        if (request().getQueryString(search) != null) {
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.SEARCH,
                    request().getQueryString(search));
        }
        if (request().getQueryString(testCaseFilter) != null) {
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.TESTCASE_FILTER,
                    request().getQueryString(testCaseFilter));
        }
        if (request().getQueryString(fingerprintFilter) != null) {
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.FINGERPRINT_FILTER,
                    request().getQueryString(fingerprintFilter));
        }

        return paramMap;
    }

    /**
     * Get the given item in the given tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId    the tenant ID
     * @param itemId      the ID of the item
     * @return a Result containing the item
     */
    @Transactional(readOnly = true)
    public Result getItem(String apiVersion, int previewerId, long tenantId, int itemId) {
        Logger.info("event=\"Retrieving item for tenant.\", previewerId=\"{}\", tenantId=\"{}\", itemId=\"{}\"",
                previewerId, tenantId, itemId);

        try {
            enforceTenantLevelPermission(ITEMS, AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND, "Unable to find a previewer with the given ID.");
        } else {
            Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = new HashMap<>();
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS, String.valueOf(itemId));

            List<GetTenantItemsResult.Item> items;
            try {
                items = apiOptional.get().getTenantItems((int) tenantId, paramMap).getItems();
            } catch (NullPointerException e) {
                // The API library throws a NullPointerException if the given tenant is not found.
                Logger.info("event=\"Tenant not found when calling TN8 get items API.\", previewerId=\"{}\", " +
                        "tenantId=\"{}\"", previewerId, tenantId);
                return failResult(NOT_FOUND, "Unable to find a tenant with the given ID.");
            }

            if (!items.isEmpty()) {
                return successResult(items.get(0), "item");
            } else {
                return failResult(NOT_FOUND, "Unable to find the requested item.");
            }
        }
    }

    /**
     * Get the list of items associated with the given tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId the ID of the previewer containing the tenant
     * @param tenantId    the tenant ID
     * @return a Result containing a list of items to download
     */
    @Transactional(readOnly = true)
    public Result getItemsForDownload(String apiVersion, int previewerId, long tenantId) {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode data = mapper.createArrayNode();
        ObjectNode node = mapper.createObjectNode();
        int startIndex = 0;
        Logger.info("event=\"Retrieving list of items for tenant.\", previewerId=\"{}\", tenantId=\"{}\"",
                previewerId, tenantId);

        try {
            enforceTenantLevelPermission(ITEMS, AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND, "Unable to find a previewer with the given ID.");
        } else {
            Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = new HashMap<>();
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_START, String.valueOf(startIndex));
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_LENGTH, String.valueOf(ITEMS_TO_GET));
            try {
                GetTenantItemsResult result = apiOptional.get().getTenantItems((int) tenantId, paramMap);
                int totalItems = result.getTotalItemsInTenant();
                int remainingItems = totalItems - ITEMS_TO_GET;
                node.set(ITEMS, Json.toJson(result.getItems()));
                data.add(node);
                while (remainingItems > 0) {
                    paramMap.replace(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_START,
                            String.valueOf((Integer.parseInt(paramMap.get(GetTenantItemsResult.GetTenantItemsParam.DISPLAY_START)) + ITEMS_TO_GET)));
                    result = apiOptional.get().getTenantItems((int) tenantId, paramMap);
                    ObjectNode node1 = mapper.createObjectNode();
                    node1.set(ITEMS, Json.toJson(result.getItems()));
                    data.add(node1);
                    if (remainingItems >= ITEMS_TO_GET) {
                        remainingItems = remainingItems - ITEMS_TO_GET;
                    } else {
                        remainingItems = 0;
                    }
                }
            } catch (NullPointerException e) {
                // The API library throws a NullPointerException if the given tenant is not found.
                Logger.info("event=\"Tenant not found when calling TN8 get items API.\", previewerId=\"{}\", tenantId=\"{}\"",

                        previewerId, tenantId);
                return failResult(NOT_FOUND, "Unable to find a tenant with the given ID.");
            }

            return successResult(data);
        }
    }
    
    /**
     * Get the item xml for the given item identifier.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId the ID of the previewer containing the tenant.
     * @param tenantId    the tenant ID
     * @param itemIdentifier identifier of item
     * @return a Result containing item xml.
     */
    @Transactional(readOnly = true)
    public Result getItemXMLByIdentifier(String apiVersion, int previewerId, long tenantId, String itemIdentifier) {
        Logger.info("event=\"Retrieving item xml for item identifier.\", previewerId=\"{}\",tenantId=\"{}\", itemIdentifier=\"{}\"",previewerId, tenantId, itemIdentifier);
        try {
            enforceTenantLevelPermission(ITEMS, AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND, "Unable to find a previewer with the given ID.");
        } else {
          GetXMLByIdentifierResult result = null;
            try {
                  result = apiOptional.get().getItemXMLByIdentifier((int) tenantId, itemIdentifier);
            } 
            catch(Exception ex){
              
             return failResult(NOT_FOUND, "Unable to retrieve the item xml for Item identifier : " + itemIdentifier);
            }
             
              ObjectNode data = Json.newObject();
              data.put("itemXml",result.getXML());
              
            return successResult(data);
        }
    }
    
}
