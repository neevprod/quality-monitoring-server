package controllers.qtivalidation;

import controllers.SecureController;
import models.qtivalidation.PreviewerTenant;
import models.qtivalidation.UserTenantJobSubscription;
import play.db.jpa.Transactional;
import play.mvc.Result;
import services.qtivalidation.PreviewerTenantService;
import services.qtivalidation.UserTenantJobSubscriptionService;
import util.SessionData;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class TenantController extends SecureController {

    private final PreviewerTenantService previewerTenantService;
    private final UserTenantJobSubscriptionService userTenantJobSubscriptionService;

    @Inject
    TenantController(SessionData.Factory sessionDataFactory, PreviewerTenantService previewerTenantService,
            UserTenantJobSubscriptionService userTenantJobSubscriptionService) {
        super(sessionDataFactory);
        this.previewerTenantService = previewerTenantService;
        this.userTenantJobSubscriptionService = userTenantJobSubscriptionService;
    }

    /**
     * Get a list of tenants by previewer ID. If the user is an admin user, this will return all tenants under the given
     * previewer. If the user is not an admin user, it will only return the tenants the user has access to.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            the ID of the previewer to filter by
     * @return a Result containing the list of tenants
     */
    @Transactional(readOnly = true)
    public Result getTenants(String apiVersion, int previewerId) {
        SessionData sessionData = sessionData();
        List<PreviewerTenant> tenants;

        if (sessionData.isAdmin()
                || sessionData.hasWildcardAccessToPreviewer(Module.QTI_VALIDATION.getName(), previewerId)) {
            tenants = previewerTenantService.findAllActive(previewerId);
        } else {
            Set<Long> allowedTenantIds = sessionData.getAllowedTenantIds(Module.QTI_VALIDATION.getName(), previewerId);
            tenants = previewerTenantService.findAllActive(previewerId, allowedTenantIds);
        }

        if (!tenants.isEmpty()) {
            List<UserTenantJobSubscription> userTenantJobSubscriptions = userTenantJobSubscriptionService
                    .findByUserIdAndPreviewerId(sessionData.getUserId(), previewerId);
            Set<Long> subscribedTenants = userTenantJobSubscriptions.stream()
                    .map(UserTenantJobSubscription::getPreviewerTenantId).collect(Collectors.toSet());
            for (PreviewerTenant tenant : tenants) {
                if (subscribedTenants.contains(tenant.getPreviewerTenantId())) {
                    tenant.setSubscribed("yes");
                } else {
                    tenant.setSubscribed("no");
                }
            }
        }
        return successResult(tenants, "tenants");
    }

    /**
     * Get a tenant by its ID, but only return the tenant if the user has access to it.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            the ID of the previewer the tenant resides in
     * @param tenantId
     *            the ID of the tenant to fetch
     * @return a Result containing the tenant
     */
    @Transactional(readOnly = true)
    public Result getTenant(String apiVersion, int previewerId, long tenantId) {
        SessionData sessionData = sessionData();
        Optional<PreviewerTenant> optionalTenant;

        if (sessionData.isAdmin()
                || sessionData.hasWildcardAccessToPreviewer(Module.QTI_VALIDATION.getName(), previewerId)) {
            optionalTenant = previewerTenantService.findActive(previewerId, tenantId);
        } else {
            Set<Long> allowedTenantIds = sessionData.getAllowedTenantIds(Module.QTI_VALIDATION.getName(), previewerId);
            if (allowedTenantIds.contains(tenantId)) {
                optionalTenant = previewerTenantService.findActive(previewerId, tenantId);
            } else {
                optionalTenant = Optional.empty();
            }
        }

        if (optionalTenant.isPresent()) {
            return successResult(optionalTenant.get(), "tenant");
        } else {
            return failResult(NOT_FOUND, "Unable to find the requested tenant.");
        }
    }

}
