package controllers.qtivalidation;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pearson.itautomation.qtiscv.ItemResponse;
import com.pearson.itautomation.qtiscv.ItemResponsePool;
import com.pearson.itautomation.qtiscv.ItemTestCaseCoverage;
import com.pearson.itautomation.qtiscv.ItemTestCaseCoverage.ItemCoverage;
import com.pearson.itautomation.tn8.previewer.api.*;
import com.typesafe.config.Config;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import jobs.JobProtocol;
import jobs.qtivalidation.TestCaseUploadJobStarter;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import util.SessionData;
import util.qtivalidation.ItemResponsePoolFactory;
import util.qtivalidation.TestCaseCoverageFactory;
import util.qtivalidation.TestNav8APIFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class TestCaseController extends SecureController {
    private static final String ITEMS = "items";
    private final TestNav8APIFactory apiFactory;
    private final ItemResponsePoolFactory itemResponsePoolFactory;
    private final TestCaseCoverageFactory testCaseCoverageFactory;
    private final ActorRef jobRunnerActor;
    private final TestCaseUploadJobStarter.Factory testCaseUploadJobStarterFactory;
    private final long actorAskTimeout;

    @Inject
    TestCaseController(SessionData.Factory sessionDataFactory,
                       TestNav8APIFactory apiFactory,
                       ItemResponsePoolFactory itemResponsePoolFactory,
                       TestCaseCoverageFactory testCaseCoverageFactory,
                       @Named("jobRunnerActor") ActorRef jobRunnerActor,
                       TestCaseUploadJobStarter.Factory testCaseUploadJobStarterFactory,
                       Config configuration) {
        super(sessionDataFactory);
        this.apiFactory = apiFactory;
        this.itemResponsePoolFactory = itemResponsePoolFactory;
        this.testCaseCoverageFactory = testCaseCoverageFactory;
        this.jobRunnerActor = jobRunnerActor;
        this.testCaseUploadJobStarterFactory = testCaseUploadJobStarterFactory;
        actorAskTimeout = configuration.getDuration("application.actorAskTimeout").toMillis();
    }

    /**
     * Get the list of test cases associated with the given item, along with the
     * fingerprint data.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer containing the item.
     * @param tenantId    The ID of the tenant containing the item.
     * @param itemId      The ID of the item.
     * @return A Result containing the list of test cases, along with the
     * fingerprint data.
     */
    @Transactional(readOnly = true)
    public Result getTestCases(String apiVersion, int previewerId,
                               long tenantId, int itemId) {
        Logger.info(
                "event=\"Retrieving list of test cases for item.\", previewerId=\"{}\", tenantId=\"{}\", "
                        + "itemId=\"{}\"", previewerId, tenantId, itemId);

        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND,
                    "Unable to find a previewer with the given ID.");
        } else {
            String itemIdentifier = getItemIdentifier(apiOptional.get(),
                    tenantId, itemId);

            if (!itemIdentifier.isEmpty()) {
                GetItemScoringTestCasesResult testCasesResult;
                try {
                    testCasesResult = apiOptional
                            .get().getItemScoringTestCases((int) tenantId,
                                    itemIdentifier);
                } catch (Exception e) {
                    Logger.error("event=\"Unable to process Previewer data.\" tenantId=\"{}\", " +
                            "itemId=\"{}\"", tenantId, itemId, e);
                    return failResult(FAILED_DEPENDENCY, "Invalid or no data was returned by Previewer" +
                            ". Please contact the Previewer development team for further assistance.");
                }

                List<GetItemScoringTestCasesResult.TestCase> testCases = testCasesResult
                        .getTestCases();

                GetItemScoringFingerprintResult fingerprint = apiOptional.get()
                        .getItemScoringFingerprint((int) tenantId,
                                itemIdentifier);
                Map<String, String> fingerprintMap = fingerprint
                        .getInputsToOutputsMap();

                ObjectNode dataJson = Json.newObject();
                ArrayNode testCasesJson = dataJson.putArray("testCases");
                if ("true".equals(request().getQueryString("run"))) {
                    Logger.info(
                            "event=\"Running test cases for item.\", previewerId=\"{}\", tenantId=\"{}\", "
                                    + "itemId=\"{}\"", previewerId, tenantId,
                            itemId);
                    Map<Integer, JsonNode> testCaseResultsMap = runTestCases(
                            apiOptional.get(), tenantId, itemId,
                            itemIdentifier, testCasesResult, fingerprint);
                    for (GetItemScoringTestCasesResult.TestCase testCase : testCases) {
                        testCasesJson.add(getTestCaseJson(testCase,
                                fingerprintMap, testCaseResultsMap));
                    }
                } else {
                    for (GetItemScoringTestCasesResult.TestCase testCase : testCases) {
                        testCasesJson.add(getTestCaseJson(testCase,
                                fingerprintMap));
                    }
                }

                return successResult(dataJson);
            } else {
                return failResult(NOT_FOUND,
                        "Unable to find an item with the given ID.");
            }
        }
    }

    /**
     * Update the fingerprint for the given item. Only the test cases included
     * in the request body are added/updated in the fingerprint.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer containing the item.
     * @param tenantId    The ID of the tenant containing the item.
     * @param itemId      The ID of the item.
     * @return A Result indicating whether the operation was successful.
     */
    @Transactional(readOnly = true)
    @BodyParser.Of(BodyParser.Json.class)
    public Result updateFingerprint(String apiVersion, int previewerId,
                                    long tenantId, int itemId) {
        Logger.info(
                "event=\"Updating fingerprint for item.\", previewerId=\"{}\", tenantId=\"{}\", itemId=\"{}\"",
                previewerId, tenantId, itemId);

        try {
            enforceTenantLevelPermission("fingerprint", AccessType.UPDATE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        JsonNode testCaseIdsJson = request().body().asJson().get("testCaseIds");

        if (testCaseIdsJson == null || !testCaseIdsJson.isArray()
                || testCaseIdsJson.size() == 0) {
            Logger.info("event=\"List of test cases missing from update fingerprint request.\"");
            return failResult(BAD_REQUEST,
                    "Please include a list of test case IDs to update in the fingerprint.");
        }

        Set<Integer> testCaseIds = new HashSet<>();
        ArrayNode testCaseIdsJsonArray = (ArrayNode) testCaseIdsJson;
        for (int i = 0; i < testCaseIdsJsonArray.size(); i++) {
            JsonNode value = testCaseIdsJsonArray.get(i);
            if (value.isNumber()) {
                testCaseIds.add(value.asInt());
            } else {
                return failResult(BAD_REQUEST,
                        "Values in the list of test case IDs must be numeric.");
            }
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND,
                    "Unable to find a previewer with the given ID.");
        } else {
            String itemIdentifier = getItemIdentifier(apiOptional.get(),
                    tenantId, itemId);

            if (!itemIdentifier.isEmpty()) {
                // Retrieve the given test cases.
                List<GetItemScoringTestCasesResult.TestCase> testCases = apiOptional
                        .get()
                        .getItemScoringTestCases((int) tenantId, itemIdentifier)
                        .getTestCases();
                List<GetItemScoringTestCasesResult.TestCase> chosenTestCases = testCases
                        .stream()
                        .filter(testCase -> testCaseIds.contains(testCase
                                .getTestCaseId())).collect(Collectors.toList());
                if (chosenTestCases.size() < testCaseIds.size()) {
                    return failResult(NOT_FOUND,
                            "Unable to find one or more of the given test cases.");
                }

                // Bulk score the given test cases.
                BulkScoreItemsByIdentifier bulkScores = apiOptional.get()
                        .bulkScoreItemsByIdentifier(
                                (int) tenantId,
                                createBulkScoreRequestData(itemIdentifier,
                                        chosenTestCases));
                List<BulkScoreItemsByIdentifier.Score> scores = bulkScores
                        .getScores();

                // Get the current fingerprint of the given item.
                GetItemScoringFingerprintResult fingerprint = apiOptional.get()
                        .getItemScoringFingerprint((int) tenantId,
                                itemIdentifier);

                Map<String, String> fingerprintMap = fingerprint
                        .getInputsToOutputsMap();

                // Update the fingerprint based on the bulk score outcomes of
                // the given test cases.
                for (int i = 0; i < chosenTestCases.size(); i++) {
                    String mods = chosenTestCases.get(i).getMods().toString();
                    String outcomes = scores.get(i).getAsString();
                    fingerprintMap.put(mods, outcomes);
                }
                Map<String, String> updatedFingerprintData = getDataFromFingerprintMap(
                        itemIdentifier, fingerprintMap);
                apiOptional.get().updateItemScoringFingerprint((int) tenantId,
                        itemIdentifier, updatedFingerprintData.get("input"),
                        updatedFingerprintData.get("output"));

                return successResult();
            } else {
                return failResult(NOT_FOUND,
                        "Unable to find an item with the given ID.");
            }
        }
    }

    /**
     * Delete the test cases specified by the test case IDs included in the
     * query string.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer containing the item.
     * @param tenantId    The ID of the tenant containing the item.
     * @param itemId      The ID of the item.
     * @return A Result indicating success or failure.
     */
    @Transactional(readOnly = true)
    public Result deleteItemTestCases(String
                                              apiVersion, int previewerId, long tenantId, int itemId) {
        Logger.info(
                "event=\"Deleting test cases for item.\", previewerId=\"{}\", tenantId=\"{}\", itemId=\"{}\""
                , previewerId, tenantId, itemId);

        try {
            enforceTenantLevelPermission("testCases", AccessType.DELETE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch
        (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        String[] testCaseIdStrings = request().queryString().get("testCaseIds");
        if (testCaseIdStrings == null || testCaseIdStrings.length == 0) {
            return
                    failResult(BAD_REQUEST,
                            "Please include one or more test case IDs to delete.");
        }

        final Set<Integer> testCaseIds;
        try {
            testCaseIds =
                    Arrays.stream(testCaseIdStrings).map(Integer::valueOf)
                            .collect(Collectors.toSet());
        } catch (NumberFormatException e) {
            return
                    failResult(BAD_REQUEST, "Test case IDs must be numeric.");
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if
        (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND,
                    "Unable to find a previewer with the given ID.");
        } else {
            String
                    itemIdentifier = getItemIdentifier(apiOptional.get(), tenantId, itemId);
            if (!itemIdentifier.isEmpty()) { // Retrieve the given test cases.
                List<GetItemScoringTestCasesResult.TestCase> testCases =
                        apiOptional.get().getItemScoringTestCases((int) tenantId,
                                itemIdentifier).getTestCases();
                List<GetItemScoringTestCasesResult.TestCase> chosenTestCases =
                        testCases.stream().filter(testCase ->
                                testCaseIds.contains(testCase.getTestCaseId()))
                                .collect(Collectors.toList());
                if (chosenTestCases.size() <
                        testCaseIds.size()) {
                    return failResult(NOT_FOUND,
                            "Unable to find one or more of the given test cases.");
                }

                // Get the current fingerprint of the given item.
                GetItemScoringFingerprintResult fingerprint = apiOptional.get()
                        .getItemScoringFingerprint((int) tenantId, itemIdentifier);

                Map<String, String> fingerprintMap = fingerprint.getInputsToOutputsMap();

                // Remove the given test cases from the fingerprint if they are in the fingerprint.
                int initialFingerprintSize = fingerprintMap.size();
                for
                (GetItemScoringTestCasesResult.TestCase testCase : chosenTestCases) {
                    String mods = testCase.getMods().toString();
                    fingerprintMap.remove(mods);
                }
                if (initialFingerprintSize != fingerprintMap.size()) {
                    Map<String,
                            String> updatedFingerprintData =
                            getDataFromFingerprintMap(itemIdentifier, fingerprintMap);
                    apiOptional.get().updateItemScoringFingerprint((int) tenantId,
                            itemIdentifier, updatedFingerprintData.get("input"),
                            updatedFingerprintData.get("output"));
                }
                // Delete the given test cases.
                for (int testCaseId : testCaseIds) {
                    apiOptional.get().deleteItemScoringTestCase((int) tenantId, testCaseId);
                }

                return successResult();
            } else {
                return failResult(NOT_FOUND,
                        "Unable to find an item with the given ID.");
            }
        }
    }


    /**
     * Delete the test cases specified by the test case IDs included in the
     * query string.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer containing the item.
     * @param tenantId    The ID of the tenant containing the item.
     * @return A Result indicating success or failure.
     */
    @Transactional(readOnly = true)
    public Result deleteTestCases(String apiVersion, int previewerId,
                                  long tenantId) {
        Logger.info(
                "event=\"Deleting test cases for item.\", previewerId=\"{}\", tenantId=\"{}\"",
                previewerId, tenantId);

        try {
            enforceTenantLevelPermission("testCases", AccessType.DELETE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        String[] parsedCsvData = request().queryString().get("parsedCsv");
        if (parsedCsvData == null || parsedCsvData.length == 0) {
            return failResult(BAD_REQUEST,
                    "Please include one or more test case IDs to delete.");
        }
        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        int itemId = 0;
        String identifierFromDb;
        String identifierFromCsv;

        List<Integer> failTestCaseIds = new ArrayList<>();
        for (String aParsedCsvData : parsedCsvData) {
            List<Integer> testCaseIds = new ArrayList<>();
            String rowString = aParsedCsvData.replace("[", "").replace("]", "").replaceAll("\"", "");
            String[] currentRow = rowString.split(",");
            if (currentRow.length > 1) {
                if (currentRow[0].startsWith("`")) {
                    itemId = Integer.parseInt(currentRow[0].substring(1));
                } else {
                    itemId = Integer.parseInt(currentRow[0]);
                }
                if (currentRow[1].startsWith("`")) {
                    identifierFromCsv = currentRow[1].substring(1);
                } else {
                    identifierFromCsv = currentRow[1];
                }
                Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = new HashMap<>();
                paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS,
                        String.valueOf(itemId));

                List<GetTenantItemsResult.Item> items;
                try {
                    GetTenantItemsResult tenantItemsResult = apiOptional.get().getTenantItems((int) tenantId, paramMap);
                    items = tenantItemsResult.getItems();
                } catch (NullPointerException e) {
                    // The API library throws a NullPointerException if the
                    // given tenant is not found.
                    Logger.info(
                            "event=\"Tenant not found when calling TN8 get items API.\", previewerId=\"{}\", "
                                    + "tenantId=\"{}\"", previewerId, tenantId);
                    return failResult(NOT_FOUND,
                            "Unable to find a tenant with the given ID.");
                }

                if (!items.isEmpty()) {
                    identifierFromDb = items.get(0).getIdentifier();
                    if (identifierFromDb.equals(identifierFromCsv)) {

                        try {
                            enforceTenantLevelPermission(ITEMS,
                                    AccessType.READ, Module.QTI_VALIDATION.getName(),
                                    previewerId, tenantId);
                        } catch
                        (QaApplicationPermissionException e) {
                            return
                                    accessDeniedResult();
                        }

                        if (!identifierFromDb.isEmpty()) {
                            GetItemScoringTestCasesResult testCasesResult = apiOptional
                                    .get().getItemScoringTestCases(
                                            (int) tenantId, identifierFromDb);


                            List<GetItemScoringTestCasesResult.TestCase> testCases = testCasesResult
                                    .getTestCases();
                            List<GetItemScoringTestCasesResult.TestCase> chosenTestCases =
                                    testCases.stream().filter(testCase ->
                                            testCaseIds.contains(testCase.getTestCaseId()))
                                            .collect(Collectors.toList());
                            if (chosenTestCases.size() <
                                    testCaseIds.size()) {
                                return failResult(NOT_FOUND,
                                        "Unable to find one or more of the given test cases.");
                            }

                            // Get the current fingerprint of the given item.
                            GetItemScoringFingerprintResult fingerprint = apiOptional.get()
                                    .getItemScoringFingerprint((int) tenantId, identifierFromDb);

                            Map<String, String> fingerprintMap = fingerprint.getInputsToOutputsMap();

                            // Remove the given test cases from the fingerprint if they are in the fingerprint.
                            int initialFingerprintSize = fingerprintMap.size();
                            for
                            (GetItemScoringTestCasesResult.TestCase testCase : chosenTestCases) {
                                String mods = testCase.getMods().toString();
                                fingerprintMap.remove(mods);
                            }
                            if (initialFingerprintSize != fingerprintMap.size()) {
                                Map<String,
                                        String> updatedFingerprintData =
                                        getDataFromFingerprintMap(identifierFromDb, fingerprintMap);
                                apiOptional.get().updateItemScoringFingerprint((int) tenantId,
                                        identifierFromDb, updatedFingerprintData.get("input"),
                                        updatedFingerprintData.get("output"));
                            }
                            for (GetItemScoringTestCasesResult.TestCase testCase : testCases) {
                                testCaseIds.add(testCase.getTestCaseId());
                            }
                            // Delete the given test cases.
                            for (int testCaseId : testCaseIds) {
                                apiOptional.get().deleteItemScoringTestCase((int) tenantId, testCaseId);
                            }

                        } else {
                            failTestCaseIds.add(itemId);
                            Logger.error("event=\"Unable to find the item for the given identifier .\"" + identifierFromCsv);
                        }
                    } else {
                        failTestCaseIds.add(itemId);
                        Logger.error("event=\"Unable to find the item for the given identifier.\"" + identifierFromCsv);
                    }
                } else {
                    failTestCaseIds.add(itemId);
                    Logger.error("event=\"No items found for the given tenantId.\"" + tenantId);
                }
            } else {
                failTestCaseIds.add(itemId);
                Logger.error("event=\"No identifier for given itemId \"" + itemId);
            }
        }
        if (!failTestCaseIds.isEmpty()) {
            return failResult(NOT_FOUND, "Unable to find the items "
                    + failTestCaseIds);
        }

        return successResult();

    }

    /**
     * Update the given test case to have its outcomes match the actual
     * outcomes.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer containing the item.
     * @param tenantId    The ID of the tenant containing the item.
     * @param itemId      The ID of the item.
     * @param testCaseId  The test case ID.
     * @return A Result indicating success or failure.
     */
    @Transactional(readOnly = true)
    public Result updateTestCase(String apiVersion, int previewerId,
                                 long tenantId, int itemId, int testCaseId) {
        Logger.info(
                "event=\"Updating test case.\", previewerId=\"{}\", tenantId=\"{}\", itemId=\"{}\", "
                        + "testCaseId=\"{}\"", previewerId, tenantId, itemId,
                testCaseId);

        try {
            enforceTenantLevelPermission("testCases", AccessType.UPDATE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND,
                    "Unable to find a previewer with the given ID.");
        } else {
            String itemIdentifier = getItemIdentifier(apiOptional.get(),
                    tenantId, itemId);

            if (!itemIdentifier.isEmpty()) {
                // Retrieve the given test case.
                List<GetItemScoringTestCasesResult.TestCase> testCases = apiOptional
                        .get()
                        .getItemScoringTestCases((int) tenantId, itemIdentifier)
                        .getTestCases();
                List<GetItemScoringTestCasesResult.TestCase> testCaseList = testCases
                        .stream()
                        .filter(testCase -> testCase.getTestCaseId() == testCaseId)
                        .limit(1).collect(Collectors.toList());
                if (testCaseList.isEmpty()) {
                    return failResult(NOT_FOUND,
                            "Unable to find the given test case.");
                }

                // Score the test case to find the actual outcomes.
                BulkScoreItemsByIdentifier bulkScores = apiOptional.get()
                        .bulkScoreItemsByIdentifier(
                                (int) tenantId,
                                createBulkScoreRequestData(itemIdentifier,
                                        testCaseList));
                List<BulkScoreItemsByIdentifier.Score> scores = bulkScores
                        .getScores();

                // Update the test case.
                ArrayNode mods = (ArrayNode) Json.parse(testCaseList.get(0)
                        .getMods().toString());
                ArrayNode outcomes = (ArrayNode) Json.parse(
                        scores.get(0).getAsString()).get("outcomes");
                for (JsonNode outcome : outcomes) {
                    if (outcome.get("key") != null) {
                        ObjectNode outcomeObj = (ObjectNode) outcome;
                        String identifier = outcome.get("key").asText();
                        String value = outcome.get("value").asText();
                        outcomeObj.removeAll();
                        outcomeObj.put("identifier", identifier);
                        outcomeObj.put("value", value);
                    }
                }
                ObjectNode testCaseDescription = Json.newObject();
                ObjectNode testCaseJson = testCaseDescription.putArray(
                        "testcases").addObject();
                testCaseJson.set("mods", mods);
                testCaseJson.set("outcomes", outcomes);
                apiOptional.get().addItemScoringTestCases((int) tenantId,
                        itemIdentifier, testCaseDescription.toString());

                return successResult();
            } else {
                return failResult(NOT_FOUND,
                        "Unable to find an item with the given ID.");
            }
        }
    }

    /**
     * Given a fingerprint inputs to outputs map and an item name, construct an
     * input string and output string that can be sent to the update fingerprint
     * API.
     *
     * @param itemName       The name of the item.
     * @param fingerprintMap The map of fingerprint inputs to outputs.
     * @return A map containing the input string and output string.
     */
    private Map<String, String> getDataFromFingerprintMap(String itemName,
                                                          Map<String, String> fingerprintMap) {
        ObjectNode input = Json.newObject();
        ArrayNode items = input.putArray("items");

        ObjectNode itemRef = Json.newObject();
        itemRef.put("externalItemId", itemName);
        itemRef.put("id", "");

        ObjectNode output = Json.newObject();
        output.put("api", "scoreBulkItemsByIdentifierTN8");
        output.put("success", "yes");
        ObjectNode msg = output.putObject("msg");
        ArrayNode scores = msg.putArray("scores");

        for (Map.Entry fingerprintEntry : fingerprintMap.entrySet()) {
            ObjectNode item = items.addObject();
            item.set("itemRef", itemRef);
            ObjectNode state = item.putObject("state");
            state.set("mods", Json.parse(fingerprintEntry.getKey().toString()));

            scores.add(Json.parse(fingerprintEntry.getValue().toString()));
        }

        Map<String, String> data = new HashMap<>();
        data.put("input", input.toString());
        data.put("output", output.toString());

        return data;
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on
     * a Tenants Dashboard.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return Result containing a string representation of the CSV data, or a
     * failure if not successful.
     */
    @Transactional(readOnly = true)
    public Result downloadTestCasesForTenant(String apiVersion,
                                             int previewerId, long tenantId) {
        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        try {
            ItemResponsePool itemResponsePool = itemResponsePoolFactory
                    .create(previewerId);
            List<ItemResponse> responses = itemResponsePool
                    .getTenantItemResponsePool((int) tenantId);
            return successResult(createResult(itemResponsePool, responses));
        } catch (Exception e) {
            return failResult(NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on
     * a Tenants Dashboard.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return Result containing a string representation of the CSV data, or a
     * failure if not successful.
     */
    @Transactional(readOnly = true)
    public Result downloadTestCaseCoverageForTenant(String apiVersion,
                                                    int previewerId, long tenantId) {
        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        try {
            ItemTestCaseCoverage testCaseCoverage = testCaseCoverageFactory
                    .create(previewerId);
            List<ItemCoverage> itemsWithNoTestCases = testCaseCoverage
                    .getItemCoverageByTenant((int) tenantId, true);
            StringWriter outputWriter = new StringWriter();
            testCaseCoverage.itemCoverageListToCsv(itemsWithNoTestCases,
                    outputWriter);
            return successResult(outputWriter.toString(), "download");
        } catch (Exception e) {
            return failResult(NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on
     * the Items page for a Tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return Result containing a string representation of the CSV data, or a
     * failure if not successful.
     */
    @Transactional(readOnly = true)
    public Result downloadTestCasesForItems(String apiVersion, int previewerId,
                                            long tenantId) {
        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        String[] itemIdStrings = request().queryString().get("itemIds");
        if (itemIdStrings == null || itemIdStrings.length == 0) {
            return failResult(BAD_REQUEST,
                    "Please include one or more item IDs to download test cases for.");
        }

        List<String> itemIds = Arrays.asList(itemIdStrings);

        try {
            ItemResponsePool itemResponsePool = itemResponsePoolFactory
                    .create(previewerId);
            List<ItemResponse> responses = itemResponsePool
                    .getItemsItemResponsePool((int) tenantId, itemIds);
            return successResult(createResult(itemResponsePool, responses));
        } catch (Exception e) {
            return failResult(NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on
     * the Items page for a Tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return Result containing a string representation of the CSV data, or a
     * failure if not successful.
     */
    @Transactional(readOnly = true)
    public Result downloadTestCaseCoverageForItems(String apiVersion,
                                                   int previewerId, long tenantId) {
        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        String[] itemIdStrings = request().queryString().get("itemIds");
        if (itemIdStrings == null || itemIdStrings.length == 0) {
            return failResult(BAD_REQUEST,
                    "Please include one or more item IDs to download test cases for.");
        }

        List<String> itemIds = Arrays.asList(itemIdStrings);

        try {
            ItemTestCaseCoverage testCaseCoverage = testCaseCoverageFactory
                    .create(previewerId);
            List<ItemCoverage> itemsWithNoTestCases = testCaseCoverage
                    .getItemCoverageByItems((int) tenantId, itemIds, true);
            StringWriter outputWriter = new StringWriter();
            testCaseCoverage.itemCoverageListToCsv(itemsWithNoTestCases,
                    outputWriter);
            return successResult(outputWriter.toString(), "download");
        } catch (Exception e) {
            return failResult(NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on
     * the Items page for a Tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return Result containing a string representation of the CSV data, or a
     * failure if not successful.
     */
    @Transactional(readOnly = true)
    public Result downloadTestCasesForTestMaps(String apiVersion,
                                               int previewerId, long tenantId) {
        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        String[] testMapIdStrings = request().queryString().get("testMapIds");
        if (testMapIdStrings == null || testMapIdStrings.length == 0) {
            return failResult(BAD_REQUEST,
                    "Please include one or more test map IDs to download test cases for.");
        }

        List<Integer> testMapIds = new ArrayList<>();

        for (String testMapIdString : testMapIdStrings) {
            testMapIds.add(Integer.parseInt(testMapIdString));
        }

        try {
            ItemResponsePool itemResponsePool = itemResponsePoolFactory
                    .create(previewerId);
            List<ItemResponse> responses = itemResponsePool
                    .getTestmapsItemResponsePool((int) tenantId, testMapIds);
            return successResult(createResult(itemResponsePool, responses));
        } catch (Exception e) {
            return failResult(NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Get the CSV data for a Item Response Pool test case download performed on
     * the Items page for a Tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return Result containing a string representation of the CSV data, or a
     * failure if not successful.
     */
    @Transactional(readOnly = true)
    public Result downloadTestCaseCoverageForTestMaps(String apiVersion,
                                                      int previewerId, long tenantId) {
        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        String[] testMapIdStrings = request().queryString().get("testMapIds");
        if (testMapIdStrings == null || testMapIdStrings.length == 0) {
            return failResult(BAD_REQUEST,
                    "Please include one or more test map IDs to download test cases for.");
        }

        List<Integer> testMapIdsList = new ArrayList<>();

        for (String testMapIdString : testMapIdStrings) {
            testMapIdsList.add(Integer.parseInt(testMapIdString));
        }

        Set<Integer> testMapIds = new HashSet<>(testMapIdsList);

        try {
            ItemTestCaseCoverage testCaseCoverage = testCaseCoverageFactory
                    .create(previewerId);
            List<ItemCoverage> itemsWithNoTestCases = testCaseCoverage
                    .getItemCoverageByTestMaps((int) tenantId, testMapIds, true);
            StringWriter outputWriter = new StringWriter();
            testCaseCoverage.itemCoverageListToCsv(itemsWithNoTestCases,
                    outputWriter);
            return successResult(outputWriter.toString(), "download");
        } catch (Exception e) {
            return failResult(NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Get the show test case URL for the given tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return A Result containing the show test case URL.
     */
    @Transactional(readOnly = true)
    public Result getShowTestCaseUrl(String apiVersion, int previewerId,
                                     long tenantId) {
        Logger.info(
                "event=\"Retrieving URL for showing test case.\", previewerId=\"{}\", tenantId=\"{}\"",
                previewerId, tenantId);

        try {
            enforceTenantLevelPermission("testCases", AccessType.READ,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND,
                    "Unable to find a previewer with the given ID.");
        } else {
            String url = apiOptional.get().getPreviewerUrl()
                    + "/api/epen/tenants/" + tenantId + "/item/show/"
                    + apiOptional.get().getAuthTicket();

            return successResult(url, "showTestCaseUrl");
        }
    }

    /**
     * Get the name of the given item. Returns an empty string if the item was
     * not found.
     *
     * @param api      The TestNav8API object to call.
     * @param tenantId The ID of the tenant containing the item.
     * @param itemId   The ID of the item.
     * @return The name of the item.
     */
    private String getItemIdentifier(TestNav8API api, long tenantId, int itemId) {
        Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = new HashMap<>();
        paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS,
                String.valueOf(itemId));

        List<GetTenantItemsResult.Item> items;
        try {
            items = api.getTenantItems((int) tenantId, paramMap).getItems();
        } catch (NullPointerException e) {
            // The API library throws a NullPointerException if the given tenant
            // is not found.
            Logger.info(
                    "event=\"Tenant not found when calling TN8 get items API.\", tenantId=\"{}\"",
                    tenantId);
            return "";
        }

        if (!items.isEmpty()) {
            return items.get(0).getIdentifier();
        } else {
            return "";
        }
    }

    /**
     * Construct a JSON node from a test case. The given map of fingerprint
     * inputs/outputs is used to determine whether the test case is
     * fingerprinted.
     *
     * @param testCase       The test case to convert to a JSON node.
     * @param fingerprintMap The map of fingerprint inputs to outputs.
     * @return A JsonNode representing the test case.
     */
    private JsonNode getTestCaseJson(
            final GetItemScoringTestCasesResult.TestCase testCase,
            final Map<String, String> fingerprintMap) {
        String id = "id";
        String tenantId = "tenant_id";
        String fingerprinted = "fingerprinted";
        ObjectNode json = (ObjectNode) Json.parse(testCase.getAsString());

        // Convert ID from String to int.
        int idValue = json.get(id).asInt();
        json.put(id, idValue);

        // Convert tenant ID from String to long.
        long tenantIdValue = json.get(tenantId).asLong();
        json.put(tenantId, tenantIdValue);

        if (fingerprintMap.get(json.get("mods").toString()) != null) {
            json.put(fingerprinted, true);
        } else {
            json.put(fingerprinted, false);
        }

        return json;
    }

    /**
     * Construct a JSON node from a test case. The given map of fingerprint
     * inputs/outputs is used to determine whether the test case is
     * fingerprinted, and the given map of test case results is used to include
     * the results of running the test cases.
     *
     * @param testCase           The test case to convert to a JSON node.
     * @param fingerprintMap     The map of fingerprint inputs to outputs.
     * @param testCaseResultsMap The map of test case results.
     * @return A JsonNode representing the test case.
     */
    private JsonNode getTestCaseJson(
            final GetItemScoringTestCasesResult.TestCase testCase,
            final Map<String, String> fingerprintMap,
            final Map<Integer, JsonNode> testCaseResultsMap) {
        ObjectNode json = (ObjectNode) getTestCaseJson(testCase, fingerprintMap);
        JsonNode testCaseResults = testCaseResultsMap.get(json.get("id")
                .asInt());

        ArrayNode actualOutcomes = (ArrayNode) testCaseResults
                .get("testCaseToBulkScore").get("fromBulkScore")
                .get("outcomes");
        for (JsonNode outcome : actualOutcomes) {
            if (outcome.get("key") != null) {
                ObjectNode outcomeObj = (ObjectNode) outcome;
                String identifier = outcome.get("key").asText();
                String value = outcome.get("value").asText();
                outcomeObj.removeAll();
                outcomeObj.put("identifier", identifier);
                outcomeObj.put("value", value);
            }
        }
        json.set("actualOutcomes", actualOutcomes);

        if (testCaseResults.get("fingerprintToBulkScore").get("fromTestCase") != null) {
            ArrayNode fingerprintedOutcomes = (ArrayNode) testCaseResults
                    .get("fingerprintToBulkScore").get("fromTestCase")
                    .get("outcomes");
            json.set("fingerprintedOutcomes", fingerprintedOutcomes);
        }

        json.put("passed",
                "yes".equals(testCaseResults.get("matched").asText()));

        return json;
    }

    /**
     * Creates the bulk scoring request data from the test cases.
     *
     * @param itemName  The name of the item.
     * @param testCases The test cases.
     * @return The data to feed to the bulk scoring API.
     */
    private String createBulkScoreRequestData(final String itemName,
                                              final List<GetItemScoringTestCasesResult.TestCase> testCases) {
        ObjectNode responses = Json.newObject();
        ArrayNode items = responses.putArray("items");
        ObjectNode itemRef = Json.newObject();
        itemRef.put("id", "");
        itemRef.put("externalItemId", itemName);

        for (GetItemScoringTestCasesResult.TestCase testCase : testCases) {
            ObjectNode item = items.addObject();
            ObjectNode state = item.putObject("state");
            state.set("mods", Json.parse(testCase.getMods().toString()));
            item.set("itemRef", itemRef);
        }

        return responses.toString();
    }

    /**
     * Run the test cases and return the results.
     *
     * @param api             An instance of TestNav8API for the previewer containing the
     *                        item.
     * @param tenantId        The ID of the tenant containing the item.
     * @param itemId          The ID of the item.
     * @param itemIdentifier  The name of the item.
     * @param testCasesResult The test cases.
     * @param fingerprint     The fingerprint.
     * @return A map of test case ID to test case results.
     */
    private Map<Integer, JsonNode> runTestCases(TestNav8API api, long tenantId,
                                                int itemId, String itemIdentifier,
                                                GetItemScoringTestCasesResult testCasesResult,
                                                GetItemScoringFingerprintResult fingerprint) {
        String maxScore = "MAXSCORE";
        String scoreSystem = "scoreSystem";

        BulkScoreItemsByIdentifier testCaseBulkScores = api
                .bulkScoreItemsByIdentifier(
                        (int) tenantId,
                        createBulkScoreRequestData(itemIdentifier,
                                testCasesResult.getTestCases()));
        String testCaseCompareResults = testCaseBulkScores.compareTo(
                testCasesResult, itemIdentifier, maxScore, scoreSystem);

        String fingerprintCompareResults = "";
        if (!fingerprint.getInputMods().isEmpty()) {
            GetItemScoringTestCasesResult fingerprintToTestCasesResult = GetItemScoringTestCasesResult
                    .createInstanceFromFingerprintResult(fingerprint,
                            (int) tenantId, itemId, itemIdentifier);
            BulkScoreItemsByIdentifier fingerprintBulkScores = api
                    .bulkScoreItemsByIdentifier(
                            (int) tenantId,
                            createBulkScoreRequestData(itemIdentifier,
                                    fingerprintToTestCasesResult.getTestCases()));
            fingerprintCompareResults = fingerprintBulkScores.compareTo(
                    fingerprintToTestCasesResult, itemIdentifier, maxScore,
                    scoreSystem);
        }

        String combinedCompareResults = BulkScoreItemsByIdentifier
                .combineCompareResults(itemIdentifier, (int) tenantId,
                        testCaseCompareResults, fingerprintCompareResults);

        Map<Integer, JsonNode> testCaseResultsMap = new HashMap<>();
        ArrayNode combinedCompareResultsJson = (ArrayNode) Json.parse(
                combinedCompareResults).get("testCases");
        for (JsonNode testCaseResult : combinedCompareResultsJson) {
            testCaseResultsMap.put(testCaseResult.get("testCaseId").asInt(),
                    testCaseResult);
        }

        return testCaseResultsMap;
    }

    /**
     * Upload the test cases in the JSON body for a single item.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @param itemId      The ID of the item.
     * @return A Result containing the job ID for the upload job.
     */
    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> uploadTestCasesForSingleItem(
            String apiVersion, int previewerId, long tenantId, int itemId) {
        Logger.info(
                "event=\"Request to upload test cases.\", previewerId=\"{}\", tenantId=\"{}\", itemId=\"{}\"",
                previewerId, tenantId, itemId);

        try {
            enforceTenantLevelPermission("testCases", AccessType.CREATE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JsonNode testCases = request().body().asJson();
        if (!testCases.isArray()) {
            ArrayNode testCasesArray = Json.newArray();
            testCasesArray.add(testCases);
            return startTestCaseUploadJob(previewerId, tenantId, testCasesArray);
        } else {
            return startTestCaseUploadJob(previewerId, tenantId,
                    (ArrayNode) testCases);
        }
    }

    /**
     * Upload test cases to the given tenant.
     * <p>
     * If the JSON body contains an "itemIds" property, generate test cases for
     * the given items. If the JSON body contains a "testMapIds" property,
     * generate test cases for the given test maps. Otherwise, upload the test
     * cases given in the JSON body.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer.
     * @param tenantId    The ID of the tenant.
     * @return A Result containing the job ID for the upload job.
     */
    @Transactional(readOnly = true)
    @BodyParser.Of(BodyParser.Json.class)
    public CompletionStage<Result> uploadTestCases(String apiVersion,
                                                   int previewerId, long tenantId) {
        try {
            enforceTenantLevelPermission("testCases", AccessType.CREATE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JsonNode body = request().body().asJson();

        JsonNode itemIds = body.get("itemIds");
        JsonNode testMapIds = body.get("testMapIds");
        if (itemIds != null) {
            String itemIdsString = StreamSupport
                    .stream(itemIds.spliterator(), false)
                    .map(JsonNode::toString).collect(Collectors.joining(","));
            Logger.info(
                    "event=\"Request to upload test cases.\", previewerId=\"{}\", tenantId=\"{}\", itemIds=\"{}\"",
                    previewerId, tenantId, itemIdsString);

            List<String> errors = new ArrayList<>();

            Set<Integer> itemIdSet = new HashSet<>();
            if (itemIds.isArray()) {
                for (JsonNode itemId : itemIds) {
                    if (itemId.isIntegralNumber() && itemId.asInt() >= 0) {
                        itemIdSet.add(itemId.asInt());
                    } else {
                        errors.add("itemIds must be an array of numeric IDs.");
                        break;
                    }
                }
            } else {
                errors.add("itemIds must be an array of numeric IDs.");
            }

            int maxCorrect = 1;
            JsonNode maxCorrectJson = body.get("maxCorrect");
            if (maxCorrectJson != null) {
                if (maxCorrectJson.isIntegralNumber()
                        && maxCorrectJson.asInt() >= 0) {
                    maxCorrect = maxCorrectJson.asInt();
                } else {
                    errors.add("maxCorrect must be a non-negative integer.");
                }
            }

            int maxIncorrect = 1;
            JsonNode maxIncorrectJson = body.get("maxIncorrect");
            if (maxIncorrectJson != null) {
                if (maxIncorrectJson.isIntegralNumber()
                        && maxIncorrectJson.asInt() >= 0) {
                    maxIncorrect = maxIncorrectJson.asInt();
                } else {
                    errors.add("maxIncorrect must be a non-negative integer.");
                }
            }

            if (errors.isEmpty()) {
                return startTestCaseUploadJob(previewerId, tenantId, itemIdSet,
                        maxCorrect, maxIncorrect);
            } else {
                return CompletableFuture.completedFuture(failResult(
                        BAD_REQUEST, errors));
            }
        } else if (testMapIds != null) {
            String testMapIdsString = StreamSupport
                    .stream(testMapIds.spliterator(), false)
                    .map(JsonNode::toString).collect(Collectors.joining(","));
            Logger.info(
                    "event=\"Request to upload test cases.\", previewerId=\"{}\", tenantId=\"{}\", testMapIds=\"{}\"",
                    previewerId, tenantId, testMapIdsString);

            List<String> errors = new ArrayList<>();

            Set<Integer> testMapIdSet = new HashSet<>();
            if (testMapIds.isArray()) {
                for (JsonNode testMapId : testMapIds) {
                    if (testMapId.isIntegralNumber() && testMapId.asInt() >= 0) {
                        testMapIdSet.add(testMapId.asInt());
                    } else {
                        errors.add("testMapIds must be an array of numeric IDs.");
                        break;
                    }
                }
            } else {
                errors.add("testMapIds must be an array of numeric IDs.");
            }

            int maxCorrect = 1;
            JsonNode maxCorrectJson = body.get("maxCorrect");
            if (maxCorrectJson != null) {
                if (maxCorrectJson.isIntegralNumber()
                        && maxCorrectJson.asInt() >= 0) {
                    maxCorrect = maxCorrectJson.asInt();
                } else {
                    errors.add("maxCorrect must be a non-negative integer.");
                }
            }

            int maxIncorrect = 1;
            JsonNode maxIncorrectJson = body.get("maxIncorrect");
            if (maxIncorrectJson != null) {
                if (maxIncorrectJson.isIntegralNumber()
                        && maxIncorrectJson.asInt() >= 0) {
                    maxIncorrect = maxIncorrectJson.asInt();
                } else {
                    errors.add("maxIncorrect must be a non-negative integer.");
                }
            }

            if (!errors.isEmpty()) {
                return CompletableFuture.completedFuture(failResult(
                        BAD_REQUEST, errors));
            }

            Optional<TestNav8API> tn8ApiOptional = apiFactory
                    .create(previewerId);
            if (!tn8ApiOptional.isPresent()) {
                return CompletableFuture.completedFuture(failResult(NOT_FOUND,
                        "Unable to find a previewer with the given ID."));
            } else {

                Set<Integer> itemIdSet = new HashSet<>();
                for (int testMapId : testMapIdSet) {
                    try {
                        List<Integer> testMapItemIds = tn8ApiOptional.get()
                                .getTestMapJson((int) tenantId, testMapId)
                                .getItemIdList();
                        itemIdSet.addAll(testMapItemIds);
                    } catch (Exception e) {
                        return CompletableFuture.completedFuture(failResult(
                                BAD_REQUEST, "Test map ID " + testMapId
                                        + " not found."));
                    }
                }

                return startTestCaseUploadJob(previewerId, tenantId, itemIdSet,
                        maxCorrect, maxIncorrect);
            }
        } else {
            if (!body.isArray()) {
                ArrayNode testCasesArray = Json.newArray();
                testCasesArray.add(body);
                return startTestCaseUploadJob(previewerId, tenantId,
                        testCasesArray);
            } else {
                return startTestCaseUploadJob(previewerId, tenantId,
                        (ArrayNode) body);
            }
        }
    }

    /**
     * Start a test case upload job for the test cases in the given JSON array.
     *
     * @param previewerId The ID of the previewer containing the tenant.
     * @param tenantId    The ID of the tenant to upload test cases to.
     * @param testCases   A JSON array containing the test cases to upload.
     * @return A Promise of a Result containing the ID of the created test case
     * upload job.
     */
    private CompletionStage<Result> startTestCaseUploadJob(int previewerId,
                                                           long tenantId, ArrayNode testCases) {
        JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(
                testCaseUploadJobStarterFactory.create(previewerId, tenantId,
                        sessionData().getUserId(), testCases));

        return FutureConverters.toJava(
                Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                .thenApplyAsync(this::jobStartResult);
    }

    /**
     * Start a test case upload job to generate and upload test cases for the
     * given item IDs.
     *
     * @param previewerId  The ID of the previewer containing the tenant.
     * @param tenantId     The ID of the tenant to upload test cases to.
     * @param itemIds      The IDs of the items to generate test cases for.
     * @param maxCorrect   The maximum number of correct responses to generate for each
     *                     item.
     * @param maxIncorrect The maximum number of incorrect responses to generate for each
     *                     item.
     * @return A Promise of a Result containing the ID of the created test case
     * upload job.
     */
    private CompletionStage<Result> startTestCaseUploadJob(int previewerId,
                                                           long tenantId, Set<Integer> itemIds, int maxCorrect,
                                                           int maxIncorrect) {
        JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(
                testCaseUploadJobStarterFactory.create(previewerId, tenantId,
                        sessionData().getUserId(), itemIds, maxCorrect,
                        maxIncorrect));

        return FutureConverters.toJava(
                Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                .thenApplyAsync(this::jobStartResult);
    }

    /**
     * Checks whether an Upload Test Cases Job for the given Job ID is still
     * running.
     *
     * @param apiVersion The version of the API to use.
     * @param jobId      The Job ID of the upload being checked.
     * @return Boolean value for isRunning attribute of job.
     */
    public CompletionStage<Result> checkUploadTestCasesStatus(
            String apiVersion, String jobId) {
        Logger.info(
                "event=\"Request to check status of test case upload job.\", jobId=\"{}\"",
                jobId);

        Pattern pattern = Pattern.compile("^testCaseUpload_(\\d+)_\\d+$");
        Matcher matcher = pattern.matcher(jobId);

        // Check whether the given job ID is in the correct format.
        if (!matcher.matches()) {
            return CompletableFuture.completedFuture(failResult(BAD_REQUEST,
                    "Invalid job ID."));
        }

        // Check whether the given job ID is associated with the logged in user.
        String userIdFromJobId = matcher.group(1);
        if (!sessionData().getUserId().toString().equals(userIdFromJobId)) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobCheckMessage jobCheckMessage = new JobProtocol.JobCheckMessage(
                jobId);

        return FutureConverters.toJava(
                Patterns.ask(jobRunnerActor, jobCheckMessage, actorAskTimeout))
                .thenApplyAsync(this::jobCheckResult);
    }

    /**
     * Creates the result JSON with download attribute and adds additional
     * attributes (errorItemIdentifiers, errorType) if failed to create
     * responses for any item. if responses for any item is not created.
     *
     * @param itemResponsePool The instance of {@link ItemResponsePool}
     * @param responses        The list of {@link ItemResponse}
     * @return The JSON object with download attribute and adds additional
     * attributes (errorItemIdentifiers, errorType) if failed to create
     * responses for any item. if responses for any item is not created
     */
    private ObjectNode createResult(final ItemResponsePool itemResponsePool,
                                    final List<ItemResponse> responses) {
        StringWriter outputWriter = new StringWriter();
        itemResponsePool.itemResponseListToCsv(responses, outputWriter);
        ObjectNode result = Json.newObject();
        result.put("download", outputWriter.toString());
        Set<String> errorItemIdentifiers = itemResponsePool
                .getErrorItemIdentifiers();
        if (!errorItemIdentifiers.isEmpty()) {
            String errorItems = errorItemIdentifiers.stream()
                    .map(Object::toString).collect(Collectors.joining(","));
            result.put("errorItemIdentifiers",
                    "Failed to create response for the following items: "
                            + errorItems);
            if (responses.isEmpty()) {
                result.put("errorType", "ERROR");
            } else {
                result.put("errorType", "WARNING");
            }
        }
        return result;
    }
}