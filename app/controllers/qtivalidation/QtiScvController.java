package controllers.qtivalidation;

import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import models.HumanUser;
import models.JobType;
import models.qtivalidation.HistTenant;
import models.qtivalidation.PreviewerTenant;
import models.qtivalidation.UserTenantJobSubscription;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.Result;
import services.JobTypeService;
import services.qtivalidation.*;
import services.usermanagement.HumanUserService;
import util.SessionData;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * This class contains methods that will retrieve information about QTI Scoring Validation job executions.
 */
public class QtiScvController extends SecureController {
    private static final String QTI_SCV_EXECUTIONS = "qtiScvExecutions";

    private final HistExecService histExecService;
    private final HistTenantService histTenantService;
    private final HistItemService histItemService;
    private final HistItemExceptionService histItemExceptionService;
    private final HistTestCaseService histTestCaseService;
    private final JobTypeService jobTypeService;
    private final PreviewerTenantService previewerTenantService;
    private final UserTenantJobSubscriptionService userTenantJobSubscriptionService;
    private final HumanUserService humanUserService;

    /**
     * Create an instance of QtiScvController
     *
     * @param sessionDataFactory
     *            A session data factory.
     * @param histExecService
     *            An instance of HistExecService.
     */
    @Inject
    QtiScvController(SessionData.Factory sessionDataFactory, HistExecService histExecService,
            HistTenantService histTenantService, HistItemService histItemService,
            HistItemExceptionService histItemExceptionService, HistTestCaseService histTestCaseService,
            JobTypeService jobTypeService, PreviewerTenantService previewerTenantService,
            UserTenantJobSubscriptionService userTenantJobSubscriptionService, HumanUserService humanUserService) {
        super(sessionDataFactory);
        this.histExecService = histExecService;
        this.histTenantService = histTenantService;
        this.histItemService = histItemService;
        this.histItemExceptionService = histItemExceptionService;
        this.histTestCaseService = histTestCaseService;
        this.jobTypeService = jobTypeService;
        this.previewerTenantService = previewerTenantService;
        this.userTenantJobSubscriptionService = userTenantJobSubscriptionService;
        this.humanUserService = humanUserService;
    }

    /**
     * Get the list of QTI Scoring Validation executions for the given previewer ID. Only returns executions that
     * contain data that the user has access to.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @return A Response containing a list of QTI Scoring Validation executions.
     */
    @Transactional(readOnly = true)
    public Result getQtiScvExecutions(final String apiVersion, final int previewerId) {
        Logger.info("event=\"Retrieving list of QtiScv executions for previewer.\", previewerId=\"{}\"", previewerId);

        try {
            enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                    previewerId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        SessionData sessionData = sessionData();
        if (sessionData.isAdmin()
                || sessionData.hasWildcardAccessToPreviewer(Module.QTI_VALIDATION.getName(), previewerId)) {
            return successResult(histExecService.findAllByPreviewerId(previewerId), QTI_SCV_EXECUTIONS);
        } else {
            Set<Long> tenantIds = sessionData.getAllowedTenantIds(Module.QTI_VALIDATION.getName(), previewerId);
            return successResult(histExecService.findAllByPreviewerId(previewerId, tenantIds), QTI_SCV_EXECUTIONS);
        }
    }

    /**
     * Get the list of QTI Scoring Validation tenant executions for the QTI-SCV execution ID. Only returns tenant
     * executions that the user has access to.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @param qtiScvExecutionId
     *            The ID of the QTI-SCV execution.
     * @return A Response containing a list of QTI Scoring Validation tenant executions.
     */
    @Transactional(readOnly = true)
    public Result getQtiScvTenantExecutions(final String apiVersion, final int previewerId,
            final int qtiScvExecutionId) {
        Logger.info("event=\"Retrieving list of QtiScv tenant executions for QtiScv execution.\", "
                + "previewerId=\"{}\", qtiScvExecutionId=\"{}\"", previewerId, qtiScvExecutionId);

        String qtiScvTenantExecutions = "qtiScvTenantExecutions";
        try {
            enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                    previewerId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        SessionData sessionData = sessionData();
        List<HistTenant> histTenants;

        if (sessionData.isAdmin()
                || sessionData.hasWildcardAccessToPreviewer(Module.QTI_VALIDATION.getName(), previewerId)) {
            histTenants = histTenantService.findAllByHistExecId(qtiScvExecutionId);
        } else {
            Set<Long> tenantIds = sessionData.getAllowedTenantIds(Module.QTI_VALIDATION.getName(), previewerId);
            histTenants = histTenantService.findAllByHistExecId(qtiScvExecutionId, previewerId, tenantIds);
        }

        histTenants.removeIf(t -> !sessionData.hasTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ,
                Module.QTI_VALIDATION.getName(), previewerId, t.getPreviewerTenant().getTenantId()));

        return successResult(histTenants, qtiScvTenantExecutions);
    }

    /**
     * Get the list of QTI Scoring Validation tenant executions for the provided tenant ID. Only returns tenant
     * executions if the user has access.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @param tenantId
     *            The ID of the tenant.
     * @return A Response containing a list of QTI Scoring Validation tenant executions for the given tenant.
     */
    @Transactional(readOnly = true)
    public Result getQtiScvTenantExecutionsByTenant(final String apiVersion, int previewerId, long tenantId) {
        try {
            enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                    previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }
        List<HistTenant> executions = histTenantService.findAllByTenantId(previewerId, tenantId);
        return successResult(executions, "qtiScvTenantExecutions");
    }

    /**
     * Get a QTI-SCV tenant execution by its ID.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @param qtiScvExecutionId
     *            The ID of the QTI-SCV execution.
     * @param qtiScvTenantExecutionId
     *            The ID of the QTI-SCV tenant execution.
     * @return A Response containing a QTI Scoring Validation tenant execution.
     */
    @Transactional(readOnly = true)
    public Result getQtiScvTenantExecution(final String apiVersion, final int previewerId, final int qtiScvExecutionId,
            final int qtiScvTenantExecutionId) {
        Logger.info("event=\"Retrieving QtiScv tenant execution.\", previewerId=\"{}\", qtiScvExecutionId=\"{}\", "
                + "qtiScvTenantExecutionId=\"{}\"", previewerId, qtiScvExecutionId, qtiScvTenantExecutionId);

        String qtiScvTenantExecution = "qtiScvTenantExecution";
        try {
            enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                    previewerId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        SessionData sessionData = sessionData();
        Optional<HistTenant> histTenantOptional = Optional.empty();
        if (sessionData.isAdmin()
                || sessionData.hasWildcardAccessToPreviewer(Module.QTI_VALIDATION.getName(), previewerId)) {
            histTenantOptional = histTenantService.find(qtiScvTenantExecutionId, qtiScvExecutionId);
        } else {
            Set<Long> tenantIds = sessionData.getAllowedTenantIds(Module.QTI_VALIDATION.getName(), previewerId);
            histTenantOptional = histTenantService.find(qtiScvTenantExecutionId, qtiScvExecutionId, previewerId,
                    tenantIds);
        }

        if (histTenantOptional.isPresent()) {
            try {
                enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                        previewerId, histTenantOptional.get().getPreviewerTenant().getTenantId());
                return successResult(histTenantOptional.get(), qtiScvTenantExecution);
            } catch (QaApplicationPermissionException e) {
                return accessDeniedResult();
            }
        } else {
            return failResult(NOT_FOUND, "Unable to find a QTI-SCV tenant execution with the given ID.");
        }
    }

    /**
     * Get the list of validated items for the given qtiScvTenantExecutionId.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @param tenantId
     *            The ID of the tenant the previewer belongs to.
     * @param qtiScvTenantExecutionId
     *            The ID of the QTI-SCV tenant execution.
     * @return A Response containing a list of validated items.
     */
    @Transactional(readOnly = true)
    public Result getValidatedItems(final String apiVersion, final int previewerId, final long tenantId,
            final int qtiScvTenantExecutionId) {
        Logger.info(
                "event=\"Retrieving list of validated items for QtiScv tenant execution.\", "
                        + "previewerId=\"{}\", tenantId=\"{}\", qtiScvTenantExecutionId=\"{}\"",
                previewerId, tenantId, qtiScvTenantExecutionId);

        try {
            enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                    previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(histItemService.findAllByHistTenantId(qtiScvTenantExecutionId, previewerId, tenantId),
                "validatedItems");
    }

    /**
     * Get the list of item exceptions for the given qtiScvTenantExecutionId.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @param tenantId
     *            The ID of the tenant the previewer belongs to.
     * @param qtiScvTenantExecutionId
     *            The ID of the QTI-SCV tenant execution.
     * @return A Response containing a list of item exceptions.
     */
    @Transactional(readOnly = true)
    public Result getItemExceptions(final String apiVersion, final int previewerId, final long tenantId,
            final int qtiScvTenantExecutionId) {
        Logger.info(
                "event=\"Retrieving list of item exceptions for QtiScv tenant execution.\", previewerId=\"{}\", "
                        + "tenantId=\"{}\", qtiScvTenantExecutionId=\"{}\"",
                previewerId, tenantId, qtiScvTenantExecutionId);

        try {
            enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                    previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(
                histItemExceptionService.findAllByHistTenantId(qtiScvTenantExecutionId, previewerId, tenantId),
                "itemExceptions");
    }

    /**
     * Get the list of failed test cases for the given validated item.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @param tenantId
     *            The ID of the tenant the previewer belongs to.
     * @param validatedItemId
     *            The ID of the validated item.
     * @return A Response containing a list of validated items.
     */
    @Transactional(readOnly = true)
    public Result getFailedTestCases(final String apiVersion, final int previewerId, final long tenantId,
            final int validatedItemId) {
        Logger.info("event=\"Retrieving list of failed test cases for validated item.\", previewerId=\"{}\", "
                + "tenantId=\"{}\", validatedItemId=\"{}\"", previewerId, tenantId, validatedItemId);

        try {
            enforceTenantLevelPermission(QTI_SCV_EXECUTIONS, AccessType.READ, Module.QTI_VALIDATION.getName(),
                    previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        return successResult(histTestCaseService.findAllByHistItemId(validatedItemId, previewerId, tenantId),
                "failedTestCases");
    }

    /**
     * Create QTI-SCV job subscriptions to tenant in the given previewer for the current user. The tenant ID needs to be
     * included in the request body.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @return A Result indicating whether the subscription was successful.
     */
    @Transactional
    public Result createQtiScvTenantJobSubscriptions(final String apiVersion, final int previewerId,
            final Long tenantId) {
        Logger.info("event=\"Subscribing to QTI-SCV job for tenants in previewer.\", previewerId=\"{}\"", previewerId);
        try {
            enforceTenantLevelPermission("qtiScvTenantJobSubscriptions", AccessType.CREATE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUserOptional = humanUserService.findByUserId(sessionData().getUserId());
        if (!humanUserOptional.isPresent()) {
            return failResult(BAD_REQUEST, "Job subscriptions cannot be made using an API key.");
        }

        Optional<JobType> jobTypeOptional = jobTypeService.findByName("Score Consistency Validation");
        if (jobTypeOptional.isPresent()) {
            Optional<PreviewerTenant> previewerTenant = previewerTenantService.findActiveWithNoJobSubscription(
                    previewerId, tenantId, humanUserOptional.get().getHumanUserId(),
                    jobTypeOptional.get().getJobTypeId());
            previewerTenant.ifPresent(pt -> {
                UserTenantJobSubscription subscription = new UserTenantJobSubscription(humanUserOptional.get(),
                        pt.getPreviewerTenantId(), jobTypeOptional.get());
                userTenantJobSubscriptionService.create(subscription);
            });
            return successResult();
        } else {
            throw new RuntimeException("Score Consistency Job type not found.");
        }
    }

    /**
     * Removes QTI-SCV job subscription for tenant in the given previewer for the current user.
     *
     * @param apiVersion
     *            The version of the API to use.
     * @param previewerId
     *            The ID of the previewer.
     * @param tenantId
     *            The ID of the tenant
     * @return A Result indicating whether the unsubscription was successful.
     */
    @Transactional
    public Result removeQtiScvTenantJobSubscription(final String apiVersion, final int previewerId,
            final Long tenantId) {
        Logger.info("event=\"Unsubscribing to QTI-SCV job for tenant in previewer.\", previewerId=\"{}\","
                + " tenantId=\"{}\"", previewerId, tenantId);

        try {
            enforceTenantLevelPermission("qtiScvTenantJobSubscriptions", AccessType.DELETE,
                    Module.QTI_VALIDATION.getName(), previewerId, tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        Optional<HumanUser> humanUserOptional = humanUserService.findByUserId(sessionData().getUserId());
        if (!humanUserOptional.isPresent()) {
            return failResult(BAD_REQUEST, "Job unsubscription cannot be made using an API key.");
        }

        Optional<JobType> jobTypeOptional = jobTypeService.findByName("Score Consistency Validation");
        if (jobTypeOptional.isPresent()) {
            Optional<UserTenantJobSubscription> subscription = userTenantJobSubscriptionService
                    .findByPreviewerIdTenantIdUserIdAndJobTypeId(previewerId, tenantId,
                            humanUserOptional.get().getUserId(), jobTypeOptional.get().getJobTypeId());
            subscription.ifPresent(s -> userTenantJobSubscriptionService.delete(s));
            return successResult();
        } else {
            throw new RuntimeException("Score Consistency Job type not found.");
        }

    }

}
