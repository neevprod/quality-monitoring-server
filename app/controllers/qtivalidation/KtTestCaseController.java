package controllers.qtivalidation;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.typesafe.config.Config;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import jobs.JobProtocol;
import jobs.qtivalidation.KtTestCaseDownloadJobStarter;
import play.Logger;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import util.S3Client;
import util.SessionData;
import util.qtivalidation.S3ClientFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.BufferedReader;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class KtTestCaseController extends SecureController {
    private final KtTestCaseDownloadJobStarter.Factory ktTestCaseDownloadStarterFactory;
    private final String bucket;
    private final S3ClientFactory s3ClientFactory;
    private final ActorRef jobRunnerActor;
    private final long actorAskTimeout;

    @Inject
    KtTestCaseController(SessionData.Factory sessionDataFactory, S3ClientFactory s3ClientFactory,
                         @Named("jobRunnerActor") ActorRef jobRunnerActor,
                         KtTestCaseDownloadJobStarter.Factory ktTestCaseDownloadStarterFactory,
                         Config configuration) {
        super(sessionDataFactory);
        this.s3ClientFactory = s3ClientFactory;
        this.jobRunnerActor = jobRunnerActor;
        this.ktTestCaseDownloadStarterFactory = ktTestCaseDownloadStarterFactory;

        this.bucket = configuration.getString("application.s3.kt.bucket");
        actorAskTimeout = configuration.getDuration("application.actorAskTimeout").toMillis();
    }

    /**
     * Downloads the KT test cases form Amazon S3.<br>
     * Persist the KT test cases in the Quality Monitoring database.
     *
     * @param apiVersion The version of the API to use.
     * @return A Result containing the job ID for kt testcase download job.
     */
    public CompletionStage<Result> updateKtTestCases(String apiVersion, String manifestName) {
        Logger.info("event=\"Downloading test cases for KT items.\", bucket=\"{}\", manifestName=\"{}\"", bucket, manifestName);
        try {
            enforceModuleLevelPermission("responses", AccessType.READ, Module.QTI_RESP_GEN.getName());
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        S3Client s3Client = s3ClientFactory.create();
        JsonElement jsonElement;
        try (BufferedReader br = s3Client.downFileFromS3(bucket, manifestName)) {
            jsonElement = new JsonParser().parse(br);
        } catch (Exception ex) {
            Logger.error("event=\"Test case download failed.\", bucket=\"{}\", manifestName=\"{}\"", bucket, manifestName, ex);
            return CompletableFuture.completedFuture(failResult(BAD_REQUEST, ex.getMessage()));
        }
        if (!jsonElement.isJsonArray()) {
            return CompletableFuture.completedFuture(failResult(UNPROCESSABLE_ENTITY, "Item data should be in Json Array format."));
        }
        return startKtTestCaseDownloadJob(jsonElement);

    }

    private CompletionStage<Result> startKtTestCaseDownloadJob(JsonElement jsonElement) {
        JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(
                ktTestCaseDownloadStarterFactory.create(sessionData().getUserId(), jsonElement));
        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                .thenApplyAsync(this::jobStartResult);
    }
}
