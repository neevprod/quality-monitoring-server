package controllers.qtivalidation;

import com.typesafe.config.Config;
import controllers.SecureController;
import models.qtivalidation.Previewer;
import play.db.jpa.Transactional;
import play.mvc.Result;
import services.qtivalidation.PreviewerService;
import util.SessionData;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class PreviewerController extends SecureController {

    private PreviewerService previewerService;
    private final String responseGeneratorVersionUrl;
    private final String responseGeneratorVersionDownload;
    private String responseGeneratorPath;

    @Inject
    PreviewerController(SessionData.Factory sessionDataFactory, PreviewerService previewerService, Config configuration) {
        super(sessionDataFactory);
        this.previewerService = previewerService;
        this.responseGeneratorVersionUrl = configuration.getString("application.respgenVersions.url");
        this.responseGeneratorVersionDownload = configuration.getString("application.respgenDownload.url");
        this.responseGeneratorPath = configuration.getString("application.responseGenPath");
    }

    /**
     * Get a list of previewers. If the user is an admin user, the list will contain all previewers. If the user is not
     * an admin user, the list will contain all previewers containing tenants they have access to.
     *
     * @param apiVersion The version of the API to use.
     * @return a Result containing the list of previewers
     */
    @Transactional(readOnly = true)
    public Result getPreviewers(String apiVersion) {
        SessionData sessionData = sessionData();
        List<Previewer> previewerList = null;

        if (sessionData.isAdmin()) {
            previewerList = previewerService.findAll();
        } else {
            Set<Integer> allowedPreviewerIds = sessionData.getAllowedPreviewerIds(Module.QTI_VALIDATION.getName());
            previewerList = previewerService.findAll(allowedPreviewerIds);
        }

        return successResult(previewerList, "previewers");
    }

    /**
     * Get a previewer by its ID, but only return the previewer if the user has access to it.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId the ID of the previewer to fetch
     * @return a Result containing the previewer
     */
    @Transactional(readOnly = true)
    public Result getPreviewer(String apiVersion, int previewerId) {
        SessionData sessionData = sessionData();
        Optional<Previewer> optionalPreviewer;

        if (sessionData.isAdmin()) {
            optionalPreviewer = previewerService.find(previewerId);
        } else {
            Set<Integer> allowedPreviewerIds = sessionData.getAllowedPreviewerIds(Module.QTI_VALIDATION.getName());
            if (allowedPreviewerIds.contains(previewerId)) {
                optionalPreviewer = previewerService.find(previewerId);
            } else {
                optionalPreviewer = Optional.empty();
            }
        }

        if (optionalPreviewer.isPresent()) {
            return successResult(optionalPreviewer.get(), "previewer");
        } else {
            return failResult(NOT_FOUND, "Unable to find a previewer with the given ID.");
        }
    }

    /**
     * @param apiVersion The version of the API to use.
     * @return a Result containing the list of versions
     */

    public Result getRespGenVersions(String apiVersion) {
        List<String> respGenVersionList = null;
        respGenVersionList = previewerService.findAllRespGenVersions(responseGeneratorVersionUrl);
        return successResult(respGenVersionList, "respGenVersions");
    }

    public Result downloadSelectedRespGenVersion(String apiVersion, String versionId) {
        String result = previewerService.downloadSelectedRespGenVersion(versionId, responseGeneratorVersionDownload, responseGeneratorPath);
        if (!result.equals("success")) {
            return failResult(NO_CONTENT, "Failed to download file");
        }
        return successResult(result, "success");
    }
}
