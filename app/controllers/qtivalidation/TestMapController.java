package controllers.qtivalidation;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import play.Logger;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import util.SessionData.Factory;
import util.qtivalidation.TestNav8APIFactory;

import javax.inject.Inject;
import java.util.Optional;

public class TestMapController extends SecureController {
    private static final String TEST_MAPS = "testMaps";

    private final TestNav8APIFactory apiFactory;

    @Inject
    protected TestMapController(Factory sessionDataFactory, TestNav8APIFactory apiFactory) {
        super(sessionDataFactory);
        this.apiFactory = apiFactory;
    }

    @Transactional(readOnly = true)
    public Result getTestMaps(String apiVersion, int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving list of test maps for tenant.\", previewerId=\"{}\", tenantId=\"{}\"",
                previewerId, tenantId);

        // Check permission
        try {
            enforceTenantLevelPermission(TEST_MAPS, AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        // Get TN8 previewer
        Optional<TestNav8API> apiOptional = apiFactory.create(previewerId);
        if (!apiOptional.isPresent()) {
            return failResult(NOT_FOUND, "Unable to find a previewer with the given ID.");
        } else {
            // Get test maps
            GetTestMapsResult result;
            try {
                result = apiOptional.get().getTestMaps((int) tenantId);
            } catch (NullPointerException e) {
                // The API library throws a NullPointerException if the given tenant is not found.
                Logger.info("event=\"Tenant not found when calling TN8 get test maps API.\", previewerId=\"{}\", " +
                        "tenantId=\"{}\"", previewerId, tenantId);
                return failResult(NOT_FOUND, "Unable to find a tenant with the given ID.");
            }

            // Build result
            ObjectNode data = Json.newObject();
            data.put("totalResults", result.getNumberOfTestMaps());
            data.set(TEST_MAPS, Json.toJson(result.getTestMaps()));

            return successResult(data);
        }
    }
}
