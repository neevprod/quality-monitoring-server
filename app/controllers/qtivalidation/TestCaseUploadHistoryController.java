package controllers.qtivalidation;

import controllers.SecureController;
import global.exceptions.QaApplicationPermissionException;
import models.qtivalidation.TestCaseUpload;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.Result;
import services.qtivalidation.TestCaseUploadService;
import util.SessionData;

import javax.inject.Inject;
import java.util.List;

public class TestCaseUploadHistoryController extends SecureController {
    private final TestCaseUploadService testCaseUploadService;

    @Inject
    TestCaseUploadHistoryController(SessionData.Factory sessionDataFactory,
            TestCaseUploadService testCaseUploadService) {
        super(sessionDataFactory);
        this.testCaseUploadService = testCaseUploadService;
    }

    /**
     * Get the test case upload history for a given previewer and tenant
     * 
     * @param apiVersion
     *            Unused.
     * @param previewerId
     *            The ID of the previewer containing the item.
     * @param tenantId
     *            The ID of the tenant containing the item.
     * @return A Result indicating whether the operation was successful.
     */
    @Transactional(readOnly = true)
    public Result getTestCaseUploadHistory(String apiVersion, int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving test case upload history for tenant.\", previewerId=\"{}\", tenantId=\"{}\", ",
                previewerId, tenantId);

        try {
            enforceTenantLevelPermission("testCases", AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return accessDeniedResult();
        }

        List<TestCaseUpload> resultList = this.testCaseUploadService.findAllByTenantId(previewerId, tenantId);
        return successResult(resultList, "testCaseUploads");
    }
}
