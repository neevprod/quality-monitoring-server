package controllers;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import global.exceptions.QaApplicationPermissionException;
import jobs.JobProtocol;
import jobs.JobRunnerActor;
import jobs.NightlyJobStarter;
import jobs.qtivalidation.QtiScoringValidationJobStarter;
import jobs.systemvalidation.DataIntegrityValidationActor;
import jobs.systemvalidation.DivJobStarter;
import models.systemvalidation.DivJobExec;
import models.systemvalidation.PathState;
import models.systemvalidation.StudentScenario;
import models.systemvalidation.StudentScenarioResult;
import play.Logger;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import services.systemvalidation.DivJobExecService;
import services.systemvalidation.StudentScenarioService;
import util.SessionData;
import util.systemvalidation.DivJobSetupUtil;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class JobController extends SecureController {
    private static final String QTI_SCV_JOB = "qtiScvJob";
    private static final String DIV_JOB = "divJob";
    private final ActorRef jobRunnerActor;
    private final NightlyJobStarter nightlyJobStarter;
    private final QtiScoringValidationJobStarter.Factory qtiScvJobStarterFactory;
    private final DivJobStarter.Factory divJobStarterFactory;
    private final DivJobExecService divJobExecService;
    private final StudentScenarioService studentScenarioService;
    private final long actorAskTimeout;
    private final JPAApi jpaApi;

    @Inject
    JobController(SessionData.Factory sessionDataFactory, @Named("jobRunnerActor") ActorRef jobRunnerActor,
                  NightlyJobStarter nightlyJobStarter, QtiScoringValidationJobStarter.Factory qtiScvJobStarterFactory,
                  DivJobStarter.Factory divJobStarterFactory, DivJobExecService divJobExecService,
                  StudentScenarioService studentScenarioService, JPAApi jpaApi, Config configuration) {
        super(sessionDataFactory);
        this.jobRunnerActor = jobRunnerActor;
        this.nightlyJobStarter = nightlyJobStarter;
        this.qtiScvJobStarterFactory = qtiScvJobStarterFactory;
        this.divJobStarterFactory = divJobStarterFactory;
        this.studentScenarioService = studentScenarioService;
        this.divJobExecService = divJobExecService;
        this.jpaApi = jpaApi;
        this.actorAskTimeout = configuration.getDuration("application.actorAskTimeout").toMillis();
    }

    /**
     * Get the list of currently running jobs and the list of queued jobs.
     *
     * @param apiVersion The version of the API to use.
     * @return A Result containing the list of currently running jobs and the list of queued jobs.
     */
    public CompletionStage<Result> getJobs(String apiVersion) {
        Logger.info("event=\"Request to get the running and queued jobs.\"");

        if (!sessionData().isAdmin()) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        return FutureConverters
                .toJava(Patterns.ask(jobRunnerActor, JobRunnerActor.GET_RUNNING_JOBS_MESSAGE, actorAskTimeout))
                .thenApplyAsync(response -> successResult((JsonNode) response));
    }

    /**
     * Start the nightly job.
     *
     * @param apiVersion The version of the API to use.
     * @return A Result indicating success or failure.
     */
    public CompletionStage<Result> startNightlyJob(String apiVersion) {
        Logger.info("event=\"Request to start the nightly job.\"");

        if (!sessionData().isAdmin()) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(nightlyJobStarter);

        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                .thenApplyAsync(this::jobStartResult);
    }

    /**
     * Check if the nightly job is running.
     *
     * @param apiVersion The version of the API to use.
     * @return A Response indicating whether the job is running.
     */
    public CompletionStage<Result> checkNightlyJobStatus(String apiVersion) {
        Logger.info("event=\"Request to check status of the nightly job.\"");

        if (!sessionData().isAdmin()) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobCheckMessage jobCheckMessage = new JobProtocol.JobCheckMessage("nightlyJob");

        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobCheckMessage, actorAskTimeout))
                .thenApplyAsync(this::jobCheckResult);
    }

    /**
     * Start the QTI Scoring Validation job for the given previewer and tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer to start the job for.
     * @param tenantId    The ID of the tenant to start the job for.
     * @return A Result indicating success or failure.
     */
    public CompletionStage<Result> startQtiScoringValidationJob(String apiVersion, int previewerId, long tenantId) {
        Logger.info("event=\"Request to start QTI scoring validation job.\", previewerId=\"{}\", tenantId=\"{}\"",
                previewerId, tenantId);

        try {
            enforceTenantLevelPermission(QTI_SCV_JOB, AccessType.CREATE, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(
                qtiScvJobStarterFactory.create(previewerId, tenantId));

        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                .thenApplyAsync(this::jobStartResult);
    }

    /**
     * Check if the QTI Scoring Validation Job is running for the given previewer and tenant.
     *
     * @param apiVersion  The version of the API to use.
     * @param previewerId The ID of the previewer to check.
     * @param tenantId    The ID of the tenant to check.
     * @return A Response indicating whether the job is running.
     */
    public CompletionStage<Result> checkQtiScoringValidationJobStatus(String apiVersion, int previewerId,
                                                                      long tenantId) {
        Logger.info("event=\"Request to check status of QTI scoring validation job.\", previewerId=\"{}\", "
                + "tenantId=\"{}\"", previewerId, tenantId);

        try {
            enforceTenantLevelPermission(QTI_SCV_JOB, AccessType.READ, Module.QTI_VALIDATION.getName(), previewerId,
                    tenantId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }

        JobProtocol.JobCheckMessage jobCheckMessage = new JobProtocol.JobCheckMessage(
                "qtiScoringValidation_" + previewerId + "_" + tenantId);

        return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobCheckMessage, actorAskTimeout))
                .thenApplyAsync(this::jobCheckResult);
    }

    /**
     * Start the DIV job with the given environment ID and DIV job execution ID.
     *
     * @param apiVersion    The version of the API to use.
     * @param environmentId The environment ID.
     * @param divJobExecId  The DIV job execution ID.
     * @return A Response with the job ID.
     */
    @Transactional(readOnly = true)
    public CompletionStage<Result> startDivJobExecution(final String apiVersion, final Integer environmentId,
                                                        final Long divJobExecId) {
        Logger.info("event=\"Request to start DIV Job.\", environmentId=\"{}\", divJobExecId=\"{}\"", environmentId,
                divJobExecId);
        try {
            enforceEnvironmentLevelPermission(DIV_JOB, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }
        Optional<DivJobExec> divJobExec = divJobExecService.findByDivJobExecId(divJobExecId);
        if (divJobExec.isPresent() && divJobExec.get().getDivJobExecStatus().getStatus().equalsIgnoreCase(DivJobSetupUtil.StatusNames.COMPLETED.name())) {
            return CompletableFuture.completedFuture(successResult());
        }
        return execute(environmentId, divJobExecId);

    }

    /**
     * Re-executes stats state for the given completed DIV job.
     *
     * @param apiVersion    The version of the API to use.
     * @param environmentId The environment ID.
     * @param divJobExecId  The DIV job execution ID.
     * @return A Response with the job ID.
     */
    @Transactional
    public CompletionStage<Result> reExecuteStatsForCompletedDivJob(final String apiVersion, final Integer environmentId,
                                                                    final Long divJobExecId) {
        Logger.info("event=\"Request to start STATS for DIV Job.\", environmentId=\"{}\", divJobExecId=\"{}\"", environmentId,
                divJobExecId);
        try {
            enforceEnvironmentLevelPermission(DIV_JOB, AccessType.CREATE, Module.SYSTEM_VALIDATION.getName(),
                    environmentId);
        } catch (QaApplicationPermissionException e) {
            return CompletableFuture.completedFuture(accessDeniedResult());
        }
        List<StudentScenario> studentScenarios = jpaApi.withTransaction(() -> studentScenarioService.findByDivJobExecId(divJobExecId, environmentId));

        if (studentScenarios.isEmpty()) {
            Logger.error("event=\"Can not find studentScenarios.\", environmentId=\"{}\", divJobExecId=\"{}\"", environmentId, divJobExecId);
            return CompletableFuture.completedFuture(failResult(NOT_FOUND, "Can not find studentScenarios."));
        }

        DivJobExec divJobExec = studentScenarios.get(0).getDivJobExec();
        PathState pathState = getPathStateOrderBeforeStats(divJobExec);
        if (pathState == null) {
            Logger.error("event=\"Can not find STATS state in the path.\", environmentId=\"{}\", divJobExecId=\"{}\"", environmentId, divJobExecId);
            ObjectNode content = Json.newObject();
            content.put("message", "Can not find STATS state in the path.");
            return CompletableFuture.completedFuture(successResult(content));
        }
        jpaApi.withTransaction(() -> {
            List<StudentScenario> stdScenarios = studentScenarioService.findByDivJobExecId(divJobExecId, environmentId);
            for (StudentScenario ss : stdScenarios) {
                if (DivJobSetupUtil.StatusNames.COMPLETED.name().equalsIgnoreCase(ss.getStudentScenarioStatus().getStatus()) || isStatsStateCompleted(ss)) {
                    ss.setLastCompletedStateId(pathState.getState().getStateId());
                }
            }
        });
        return execute(environmentId, divJobExecId);
    }

    private PathState getPathStateOrderBeforeStats(DivJobExec divJobExec) {
        Map<Integer, PathState> pathStateOrderMap = new HashMap<>();
        Integer statsPathStateOrder = 0;
        for (PathState ps : divJobExec.getPath().getPathStates()) {
            pathStateOrderMap.put(ps.getPathStateOrder(), ps);
            if (DataIntegrityValidationActor.DivStateName.STATS.name().equalsIgnoreCase(ps.getState().getStateName())) {
                statsPathStateOrder = ps.getPathStateOrder();
            }
        }
        return pathStateOrderMap.get(statsPathStateOrder - 1);
    }

    private boolean isStatsStateCompleted(StudentScenario ss) {
        for (StudentScenarioResult ssr : ss.getStudentScenarioResults()) {
            if (DataIntegrityValidationActor.DivStateName.STATS.name().equalsIgnoreCase(ssr.getState().getStateName()) && ssr.isSuccess()) {
                return true;
            }
        }
        return false;
    }

    private CompletionStage<Result> execute(Integer environmentId, Long divJobExecId) {

        if (divJobExecService.find(environmentId, divJobExecId).isPresent()) {
            JobProtocol.JobStartMessage jobStartMessage = new JobProtocol.JobStartMessage(
                    divJobStarterFactory.create(divJobExecId));

            return FutureConverters.toJava(Patterns.ask(jobRunnerActor, jobStartMessage, actorAskTimeout))
                    .thenApplyAsync(this::jobStartResult);
        } else {
            return CompletableFuture
                    .completedFuture(failResult(NOT_FOUND, "Unable to find the given DIV job execution."));
        }
    }
}
