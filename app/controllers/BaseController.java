package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import global.ApiVersionCheckAction;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import java.util.List;

/**
 * All controllers that do not need users to be authenticated should extend this controller.
 * If access to a controller should be restricted to logged in users, it should instead
 * extend SecureController.
 */
@With(ApiVersionCheckAction.class)
public abstract class BaseController extends Controller {
    private static final String SUCCESS = "success";
    private static final String DATA = "data";
    private static final String ERRORS = "errors";
    private static final String MESSAGE = "message";
    private static final String JSON = "application/json";

    /**
     * Create a Result indicating success.
     *
     * @return the result
     */
    protected Result successResult() {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, true);
        return ok(Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating success that also has some data.
     *
     * @param data additional data about the result of the request
     * @return the result
     */
    protected Result successResult(JsonNode data) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, true);
        content.set(DATA, data);
        return ok(Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating success that also has some data and some error/warning messages.
     *
     * @param data Additional data about the result of the request.
     * @param messages A list of human-readable error/warning messages.
     * @return The result.
     */
    protected Result successResult(JsonNode data, List<String> messages) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, true);
        content.set(DATA, data);
        addErrors(content, messages);
        return ok(Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating success that also has some data.
     *
     * @param data additional data about the result of the request
     * @param dataName the name to give the data in the result
     * @return the result
     */
    protected Result successResult(Object data, String dataName) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, true);
        ObjectNode dataNode = content.putObject(DATA);
        dataNode.set(dataName, Json.toJson(data));
        return ok(Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating failure.
     *
     * @param statusCode the HTTP status code
     * @param message a human-readable message indicating the reason for failure
     * @return the result
     */
    protected Result failResult(int statusCode, String message) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        addErrors(content, message);
        return status(statusCode, Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating failure.
     *
     * @param statusCode the HTTP status code
     * @param messages a list of human-readable message indicating the reasons for failure
     * @return the result
     */
    protected Result failResult(int statusCode, List<String> messages) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        addErrors(content, messages);
        return status(statusCode, Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating failure that also has some data.
     *
     * @param statusCode the HTTP status code
     * @param message a human-readable message indicating the reason for failure
     * @param data additional data about the failure
     * @return the result
     */
    protected Result failResult(int statusCode, String message, JsonNode data) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        addErrors(content, message);
        content.set(DATA, data);
        return status(statusCode, Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating failure that also has some data.
     *
     * @param statusCode the HTTP status code
     * @param messages a list of human-readable messages indicating the reasons for failure
     * @param data additional data about the failure
     * @return the result
     */
    protected Result failResult(int statusCode, List<String> messages, JsonNode data) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        addErrors(content, messages);
        content.set(DATA, data);
        return status(statusCode, Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating failure that also has some data.
     *
     * @param statusCode the HTTP status code
     * @param message a human-readable message indicating the reason for failure
     * @param data additional data about the failure
     * @param dataName the name to give the data in the result
     * @return the result
     */
    protected Result failResult(int statusCode, String message, Object data, String dataName) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        addErrors(content, message);
        ObjectNode dataNode = content.putObject(DATA);
        dataNode.set(dataName, Json.toJson(data));
        return status(statusCode, Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating failure that also has some data.
     *
     * @param statusCode the HTTP status code
     * @param messages a list of human-readable messages indicating the reasons for failure
     * @param data additional data about the failure
     * @param dataName the name to give the data in the result
     * @return the result
     */
    protected Result failResult(int statusCode, List<String> messages, Object data, String dataName) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        addErrors(content, messages);
        ObjectNode dataNode = content.putObject(DATA);
        dataNode.set(dataName, Json.toJson(data));
        return status(statusCode, Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result indicating insufficient permissions.
     *
     * @return the result
     */
    protected Result accessDeniedResult() {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        addErrors(content, "You do not have sufficient permission to perform this request.");
        return forbidden(Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Create a Result of the given status with exactly the given JSON.
     *
     * @param statusCode The HTTP status code.
     * @param content The JSON content of the Result.
     * @return The Result.
     */
    protected Result jsonResult(int statusCode, JsonNode content) {
        return status(statusCode, Json.prettyPrint(content)).as(JSON);
    }

    /**
     * Construct a Result from the job runner's job start response.
     *
     * @param jobStartResponse The job runner's job start response.
     * @return The Result.
     */
    protected Result jobStartResult(Object jobStartResponse) {
        return successResult(jobStartResponse, "jobId");
    }

    /**
     * Construct a Result from the job runner's job check response.
     *
     * @param jobCheckResponse The job runner's job check response.
     * @return The Result.
     */
    protected Result jobCheckResult(Object jobCheckResponse) {
        if (jobCheckResponse instanceof JsonNode) {
            return successResult((JsonNode) jobCheckResponse);
        } else {
            return failResult(BAD_REQUEST, "The given job does not exist.");
        }
    }

    /**
     * Add the given error message to the given JSON response.
     *
     * @param objectNode The JSON response to add the error message to.
     * @param message The error message to add.
     */
    private void addErrors(ObjectNode objectNode, String message) {
        ArrayNode errors = objectNode.putArray(ERRORS);
        ObjectNode error = errors.addObject();
        error.put(MESSAGE, message);
    }

    /**
     * Add the given list of error messages to the given JSON response.
     *
     * @param objectNode The JSON response to add the error messages to.
     * @param messages The list of error messages to add.
     */
    private void addErrors(ObjectNode objectNode, List<String> messages) {
        ArrayNode errors = objectNode.putArray(ERRORS);
        for (String message: messages) {
            ObjectNode error = errors.addObject();
            error.put(MESSAGE, message);
        }
    }

}
