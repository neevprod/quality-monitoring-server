package global;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ApiVersionCheckAction extends Action.Simple {
    private final Config configuration;

    @Inject
    ApiVersionCheckAction(Config configuration) {
        this.configuration = configuration;
    }

    /**
     * Check whether the given API version is valid. If not, return a Result indicating an error. If so, continue
     * fulfilling the request.
     *
     * @param context The HTTP context.
     * @return An error Result or the Result produced by fulfilling the request.
     */
    @Override
    public CompletionStage<Result> call(Http.Context context) {
        String path = context.request().path();
        String[] pathArray = path.split("/");
        String appName = pathArray[2];
        Integer version = getVersionFromString(pathArray[3]).orElse(null);

        Integer minVersion = configuration.getInt("application.apiVersions." + appName + ".min");
        Integer maxVersion = configuration.getInt("application.apiVersions." + appName + ".max");

        if (minVersion == null || maxVersion == null || version == null || version < minVersion ||
                version > maxVersion) {
            ObjectNode content = Json.newObject();
            content.put("success", false);
            ArrayNode errors = content.putArray("errors");
            ObjectNode error = errors.addObject();
            error.put("message", "Please use a valid API version.");
            return CompletableFuture.completedFuture(notFound(Json.prettyPrint(content)).as("application/json"));
        } else {
            return delegate.call(context);
        }
    }

    /**
     * Convert a string representing a version to an Integer.
     *
     * @param versionString A string representing a version, such as "v1" or "v2".
     * @return An Optional containing the integer that represents the given version.
     */
    private Optional<Integer> getVersionFromString(String versionString) {
        Optional<Integer> versionOptional = Optional.empty();

        if (versionString.startsWith("v")) {
            try {
                versionOptional = Optional.of(Integer.parseInt(versionString.substring(1)));
            } catch (NumberFormatException e) {
                versionOptional = Optional.empty();
            }
        }

        return versionOptional;
    }
}
