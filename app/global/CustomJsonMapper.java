package global;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import play.libs.Json;

public class CustomJsonMapper {

    protected CustomJsonMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        // Don't include null or empty fields in JSON.
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        // Fixes issues with mapping Hibernate objects to JSON.
        Hibernate5Module hibernateModule = new Hibernate5Module();
        hibernateModule.disable(Hibernate5Module.Feature.USE_TRANSIENT_ANNOTATION);
        objectMapper.registerModule(hibernateModule);

        Json.setObjectMapper(objectMapper);
    }

}
