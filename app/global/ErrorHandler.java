package global;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import play.Environment;
import play.Logger;
import play.api.OptionalSourceMapper;
import play.api.UsefulException;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static play.mvc.Results.*;

/**
 * This class is the global error handler, which will handle errors that are not caught anywhere else.
 */
@Singleton
public class ErrorHandler extends DefaultHttpErrorHandler {
    private static final String SUCCESS = "success";
    private static final String DATA = "data";
    private static final String ERRORS = "errors";
    private static final String MESSAGE = "message";
    private static final String JSON = "application/json";

    @Inject
    public ErrorHandler(Config configuration, Environment environment,
                        OptionalSourceMapper sourceMapper, Provider<Router> routes) {
        super(configuration, environment, sourceMapper, routes);
    }

    /**
     * This is called when an uncaught exception is thrown while in dev mode. It behaves the same way
     * as onProdServerError.
     *
     * @param request the HTTP request
     * @param exception the exception that was thrown
     */
    @Override
    protected CompletionStage<Result> onDevServerError(Http.RequestHeader request, UsefulException exception) {
        return onProdServerError(request, exception);
    }

    /**
     * This is called when an uncaught exception is thrown while in production mode. It will return a
     * 500 error, along with the ID of the exception that was logged.
     *
     * @param request the HTTP request
     * @param exception the exception that was thrown
     */
    @Override
    protected CompletionStage<Result> onProdServerError(Http.RequestHeader request, UsefulException exception) {
        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        ArrayNode errors = content.putArray(ERRORS);
        ObjectNode error = errors.addObject();
        error.put(MESSAGE, "An unexpected server error occurred.");
        content.set(DATA, Json.newObject().put("errorId", exception.id.toUpperCase()));
        return CompletableFuture.completedFuture(internalServerError(Json.prettyPrint(content)).as(JSON));
    }

    /**
     * If an uncaught exception is thrown, this method will be called. It will log the exception, along with
     * contextual information, including the exception ID.
     *
     * @param request the HTTP request
     * @param exception the exception that was thrown
     */
    @Override
    protected void logServerError(Http.RequestHeader request, UsefulException exception) {
        Logger.error("event=\"Exception occurred.\", errorId=\"{}\"", exception.id.toUpperCase(), exception.getCause());
    }

    /**
     * If no action is found for the request, this method will be called. It will return a response indicating that no
     * action was found.
     *
     * @param request the HTTP request
     * @param message the error message
     * @return a Result indicating that no action was found for the request
     */
    @Override
    protected CompletionStage<Result> onNotFound(Http.RequestHeader request, String message) {
        Logger.debug("event=\"Requested action not found.\"");

        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        ArrayNode errors = content.putArray(ERRORS);
        ObjectNode error = errors.addObject();
        error.put(MESSAGE, "No action was found for the given request.");
        return CompletableFuture.completedFuture(notFound(Json.prettyPrint(content)).as(JSON));
    }

    /**
     * If a bad request is made (such as sending wrong parameter types), this method will be called. It will return a
     * response that states what the problem is.
     *
     * @param request the HTTP request
     * @param message the error message
     * @return a Result with a message that states the problem
     */
    @Override
    protected CompletionStage<Result> onBadRequest(Http.RequestHeader request, String message) {
        Logger.info("event=\"Bad request was made.\", details=\"{}\"", message);

        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        ArrayNode errors = content.putArray(ERRORS);
        ObjectNode error = errors.addObject();
        error.put(MESSAGE, message);
        return CompletableFuture.completedFuture(badRequest(Json.prettyPrint(content)).as(JSON));
    }


    /**
     * Invoked when a client error occurs, that is, an error in the 4xx series, which is not handled
     * by any of the other methods in this class already.
     *
     * @param request The request that caused the client error.
     * @param statusCode The error status code.  Must be greater or equal to 400, and less than 500.
     * @param message The error message.
     * @return a Result with a message that states the problem
     */
    @Override
    protected CompletionStage<Result> onOtherClientError(Http.RequestHeader request, int statusCode, String message) {
        Logger.info("event=\"Bad request was made.\", details=\"{}\"", message);

        ObjectNode content = Json.newObject();
        content.put(SUCCESS, false);
        ArrayNode errors = content.putArray(ERRORS);
        ObjectNode error = errors.addObject();
        error.put(MESSAGE, message);
        return CompletableFuture.completedFuture(badRequest(Json.prettyPrint(content)).as(JSON));
    }

}
