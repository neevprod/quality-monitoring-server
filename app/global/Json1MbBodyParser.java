package global;

import play.http.HttpErrorHandler;
import play.mvc.BodyParser;

import javax.inject.Inject;

/**
 * This is just like the regular JSON body parser, but it accepts request bodies up to 1 MB in length.
 */
public class Json1MbBodyParser extends BodyParser.Json {
    @Inject
    public Json1MbBodyParser(HttpErrorHandler errorHandler) {
        super(1024L * 1024L, errorHandler);
    }
}
