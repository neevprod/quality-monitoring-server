package global;

import play.filters.gzip.GzipFilter;
import play.filters.headers.SecurityHeadersFilter;
import play.http.DefaultHttpFilters;
import play.mvc.EssentialFilter;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Applies the global filters.
 */
public class Filters extends DefaultHttpFilters {
    private final GzipFilter gzipFilter;
    private final SecurityHeadersFilter securityHeadersFilter;
    private final LoggingFilter loggingFilter;

    @Inject
    public Filters(GzipFilter gzipFilter, SecurityHeadersFilter securityHeadersFilter, LoggingFilter loggingFilter) {
        super(gzipFilter, securityHeadersFilter, loggingFilter);
        this.gzipFilter = gzipFilter;
        this.securityHeadersFilter = securityHeadersFilter;
        this.loggingFilter = loggingFilter;

    }

    @Override
    public List<EssentialFilter> getFilters() {
        List<EssentialFilter> filters = new ArrayList<>();
        filters.add(gzipFilter.asJava());
        filters.add(securityHeadersFilter.asJava());
        filters.add(loggingFilter.asJava());
        return filters;
    }


}
