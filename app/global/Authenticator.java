package global;

import com.auth0.jwt.JWTExpiredException;
import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import org.apache.commons.codec.binary.Base64;
import play.Logger;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;
import java.io.IOException;
import java.security.SignatureException;
import java.time.Clock;
import java.util.Map;

/**
 * This is used for session authentication by the SecureController class.
 */
public class Authenticator extends Security.Authenticator {

    private final Clock clock;
    private final String applicationSecret;
    private final String sessionCookieName;
    private final int sessionLengthSeconds;

    @Inject
    Authenticator(Clock clock, Config configuration) {
        this.clock = clock;
        this.applicationSecret = configuration.getString("play.http.secret.key");
        this.sessionCookieName = configuration.getString("session.cookie.name");
        this.sessionLengthSeconds = (int) configuration.getDuration("session.timeout").getSeconds();
    }

    /**
     * Determines whether the user has an active session. If there is an active session, the session data is stored
     * in the HTTP context.
     *
     * @param context the HTTP context
     * @return the name of the logged in user (null if there is no active session)
     */
    @Override
    public String getUsername(Http.Context context) {
        String token = context.request().header("X-Auth-Token").orElse(null);

        JWTVerifier jwtVerifier = new JWTVerifier(applicationSecret);
        Map<String, Object> claims;
        JsonNode jsonClaims;
        try {
            claims = jwtVerifier.verify(token);
            jsonClaims = getJsonFromToken(token);
        } catch (SignatureException e) {
            Logger.info("event=\"Token signature is invalid.\"");
            return null;
        } catch (JWTExpiredException e) {
            Logger.info("event=\"Token has expired.\", expirationTime=\"{}\"", e.getExpiration());
            return null;
        } catch (IllegalStateException e) {
            Logger.info("event=\"Token structure is invalid.\"");
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // If the JWT is stored in a cookie in the request, update its expiration time and issued at time.
        if (context.request().cookie(sessionCookieName) != null) {
            updateCookie(claims, context, clock.millis() / 1000L);
        }

        context.args.put("session", jsonClaims);
        return (String) claims.get("userName");
    }

    /**
     * Update the session cookie with a new expiration time and issued at time.
     *
     * @param claims             The JWT claims.
     * @param context            The HTTP context.
     * @param currentTimeSeconds The current time in seconds.
     */
    private void updateCookie(Map<String, Object> claims, Http.Context context, long currentTimeSeconds) {
        claims.put("iat", currentTimeSeconds);
        claims.put("exp", currentTimeSeconds + sessionLengthSeconds);

        JWTSigner jwtSigner = new JWTSigner(applicationSecret);
        String jwt = jwtSigner.sign(claims);

        context.response().setCookie(Http.Cookie.builder(sessionCookieName, jwt).build());
    }

    /**
     * Get the JSON from the JWT token.
     *
     * @param token The token.
     * @return The JSON.
     * @throws IOException
     */
    private JsonNode getJsonFromToken(String token) throws IOException {
        String[] parts = token.split("\\.");
        if (parts.length < 2) {
            throw new IllegalArgumentException("The given token is not in the proper format.");
        }
        String jsonString = new String(Base64.decodeBase64(parts[1]), "UTF-8");
        return Json.mapper().readValue(jsonString, JsonNode.class);
    }

    /**
     * This method is called if there is no active session. Redirects to the login page.
     *
     * @param context - the HTTP context
     */
    @Override
    public Result onUnauthorized(Http.Context context) {
        Logger.info("event=\"Access not authorized. No valid session.\"");
        ObjectNode content = Json.newObject();
        content.put("success", false);
        ArrayNode errors = content.putArray("errors");
        ObjectNode error = errors.addObject();
        error.put("message", "Access not authorized. No valid session.");
        return unauthorized(Json.prettyPrint(content)).as("application/json");
    }

}
