package global.exceptions;

public class QaApplicationException extends Exception {

    public QaApplicationException(String message) {
        super(message);
    }

}
