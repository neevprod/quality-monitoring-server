package global;

import akka.stream.Materializer;
import com.auth0.jwt.JWTVerifier;
import com.typesafe.config.Config;
import org.slf4j.MDC;
import play.mvc.Filter;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class LoggingFilter extends Filter {
    private final JWTVerifier jwtVerifier;

    @Inject
    public LoggingFilter(Materializer materializer, Config configuration) {
        super(materializer);
        String applicationSecret = configuration.getString("play.http.secret.key");
        jwtVerifier = new JWTVerifier(applicationSecret);
    }

    /**
     * Before the request is processed, add contextual information to the MDC for logging purposes. After the request is
     * processed, remove the information from the MDC.
     * 
     * @param nextFilter
     *            The next action in the filter chain.
     * @param requestHeader
     *            The request header of the incoming request.
     * @return A CompletionStage of type Result.
     */
    @Override
    public CompletionStage<Result> apply(Function<Http.RequestHeader, CompletionStage<Result>> nextFilter,
            Http.RequestHeader requestHeader) {
        if (!requestHeader.hasHeader("X-Auth-Token")) {
            MDC.put("ipAddress", "\"" + requestHeader.remoteAddress() + "\"");
            MDC.put("userAgent", "\"" + requestHeader.header("user-agent").orElse(null) + "\"");
        } else {
            try {
                String token = requestHeader.header("X-Auth-Token").orElse(null);
                Map<String, Object> claims = jwtVerifier.verify(token);
                MDC.put("userId", "\"" + claims.get("userId") + "\"");
                MDC.put("sessionId", "\"" + claims.get("jti") + "\"");
            } catch (Exception e) {
                MDC.put("ipAddress", "\"" + requestHeader.remoteAddress() + "\"");
                MDC.put("userAgent", "\"" + requestHeader.header("user-agent").orElse(null) + "\"");
            }
        }
        MDC.put("request", "\"" + requestHeader.method() + " " + requestHeader.path() + "\"");

        return nextFilter.apply(requestHeader).thenApply(result -> {
            MDC.remove("userId");
            MDC.remove("sessionId");
            MDC.remove("request");
            MDC.remove("ipAddress");
            MDC.remove("userAgent");
            return result;
        });
    }
}
