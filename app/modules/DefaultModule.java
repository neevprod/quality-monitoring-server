package modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.pearson.itautomation.qtiscv.QtiScv;
import com.typesafe.config.Config;
import controllers.systemvalidation.EpenScenarioCreator;
import global.CustomJsonMapper;
import jobs.*;
import jobs.qtivalidation.*;
import jobs.systemvalidation.*;
import play.Environment;
import play.libs.akka.AkkaGuiceSupport;
import play.libs.mailer.Email;
import util.ActiveDirectoryAuthenticator;
import util.ConnectionPoolManager;
import util.EncryptorDecryptor;
import util.SessionData;
import util.systemvalidation.BackendData;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Clock;

public class DefaultModule extends AbstractModule implements AkkaGuiceSupport {
    private final Environment environment;
    private final Config configuration;

    public DefaultModule(Environment environment, Config configuration) {
        this.environment = environment;
        this.configuration = configuration;
    }

    @Override
    protected void configure() {
        bind(JobScheduler.class).asEagerSingleton();
        bind(CustomJsonMapper.class).asEagerSingleton();

        bind(Clock.class).toInstance(Clock.systemDefaultZone());
        install(new FactoryModuleBuilder().build(SessionData.Factory.class));
        install(new FactoryModuleBuilder().build(BackendData.Factory.class));

        install(new FactoryModuleBuilder().build(QtiScoringValidationJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(DivJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(TestCaseUploadJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(DivJobSetupJobStarter.Factory.class));
        install(new FactoryModuleBuilder().build(KtTestCaseDownloadJobStarter.Factory.class));

        bindActor(JobRunnerActor.class, "jobRunnerActor");
        bindActorFactory(NightlyJobActor.class, NightlyJobActor.Factory.class);
        bindActorFactory(UpdateTenantsActor.class, UpdateTenantsActor.Factory.class);
        bindActorFactory(UpdateTenantsChildActor.class, UpdateTenantsChildActor.Factory.class);
        bindActorFactory(QtiScoringValidationActor.class, QtiScoringValidationActor.Factory.class);
        bindActorFactory(DataIntegrityValidationActor.class, DataIntegrityValidationActor.Factory.class);
        bindActorFactory(Tn8ApiDataLoaderActor.class, Tn8ApiDataLoaderActor.Factory.class);
        bindActorFactory(IrisValidationActor.class, IrisValidationActor.Factory.class);
        bindActorFactory(TestCaseUploadActor.class, TestCaseUploadActor.Factory.class);
        bindActorFactory(BackendValidationActor.class, BackendValidationActor.Factory.class);
        bindActorFactory(Epen2ValidationActor.class, Epen2ValidationActor.Factory.class);
        bindActorFactory(StatsValidationActor.class, StatsValidationActor.Factory.class);
        bindActorFactory(BatteryStatsValidationActor.class,BatteryStatsValidationActor.Factory.class);
        bindActorFactory(DivJobSetupActor.class, DivJobSetupActor.Factory.class);
        bindActorFactory(KtTestCaseDownloadActor.class, KtTestCaseDownloadActor.Factory.class);
    }

    @Provides
    Email provideEmail() {
        return new Email();
    }

    @Provides
    ActiveDirectoryAuthenticator adAuth() {
        return ActiveDirectoryAuthenticator.getInstance();
    }

    /**
     * Create a new instance of QtiScv using the application configuration properties.
     *
     * @return The instance of QtiScv.
     */
    @Provides
    QtiScv provideQtiScv() {
        String properties = "DB_HOST=" + configuration.getString("db.default.host") + "\n" + "DB_USER="
                + configuration.getString("db.default.username") + "\n" + "DB_PASS="
                + configuration.getString("db.default.password") + "\n" + "DB_PORT="
                + configuration.getString("db.default.port") + "\n" + "DB_DB="
                + configuration.getString("db.default.schema") + "\n" + "TN8_TOKEN="
                + configuration.getString("application.previewerApiToken") + "\n" + "TN8_SECRET="
                + EncryptorDecryptor.getDecryptedConfigurationString(configuration, "application.previewerApiSecret");

        InputStream propertiesInputStream = new ByteArrayInputStream(properties.getBytes());
        return new QtiScv(propertiesInputStream);
    }

    @Provides
    EpenScenarioCreator provideEpenScenarioCreator(ConnectionPoolManager connectionPoolManager) {
        return new EpenScenarioCreator(connectionPoolManager);
    }

}
