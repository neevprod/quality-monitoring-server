package util;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.typesafe.config.Config;
import com.zaxxer.hikari.HikariDataSource;
import play.Logger;
import play.inject.ApplicationLifecycle;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.sql.DataSource;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class is a singleton that creates and manages connection pools for database connections.
 */
@Singleton
public class ConnectionPoolManager {
    private final long mysqlConnectionTimeout;
    private final int mysqlMaximumPoolSize;
    private final int mysqlMinimumIdle;
    private final long mysqlIdleTimeout;

    private final int mongoConnectionTimeout;
    private final int mongoMaximumPoolSize;
    private final int mongoMinimumPoolSize;
    private final int mongoIdleTimeout;

    private Map<String, HikariDataSource> mysqlConnectionPools;
    private Map<String, MongoClient> mongoConnectionPools;

    /**
     * Create an instance of ConnectionPoolManager and add a hook to close all connection pools when the application
     * shuts down.
     *
     * @param configuration        The Play configuration.
     * @param applicationLifecycle The application lifecycle register.
     */
    @Inject
    ConnectionPoolManager(Config configuration, ApplicationLifecycle applicationLifecycle) {
        Logger.info("event=\"Initializing the connection pool manager.\"");

        mysqlConnectionTimeout = configuration
                .getDuration("application.connectionPoolManager.mysql.connectionTimeout").toMillis();
        mysqlMaximumPoolSize = configuration.getInt("application.connectionPoolManager.mysql.maximumPoolSize");
        mysqlMinimumIdle = configuration.getInt("application.connectionPoolManager.mysql.minimumIdle");
        mysqlIdleTimeout = configuration.getDuration("application.connectionPoolManager.mysql.idleTimeout").toMillis();

        mongoConnectionTimeout = (int)configuration
                .getDuration("application.connectionPoolManager.mongo.connectionTimeout").getSeconds();
        mongoMaximumPoolSize = configuration.getInt("application.connectionPoolManager.mongo.maximumPoolSize");
        mongoMinimumPoolSize = configuration.getInt("application.connectionPoolManager.mongo.minimumPoolSize");
        mongoIdleTimeout = (int) configuration.getDuration("application.connectionPoolManager.mongo.idleTimeout").getSeconds();

        mysqlConnectionPools = new ConcurrentHashMap<>();
        mongoConnectionPools = new ConcurrentHashMap<>();

        applicationLifecycle.addStopHook(() -> {
            closeConnectionPools();
            return CompletableFuture.completedFuture(null);
        });
    }

    /**
     * Get a connection pool for the MySQL database with the given details. If a connection pool already exists, it will
     * return that. If one does not yet exist, it will create one and return that.
     *
     * @param host     The host.
     * @param database The database.
     * @param username The username.
     * @param password The password.
     * @param port     The port.
     * @return The connection pool as a DataSource.
     */
    public DataSource getMysqlConnectionPool(String host, String database, String username, String password, int port) {
        String key = host + "," + database + "," + username;
        HikariDataSource dataSource = mysqlConnectionPools.get(key);

        if (dataSource == null) {
            dataSource = createMysqlConnectionPool(host, database, username, password, port);
            HikariDataSource previousValue = mysqlConnectionPools.putIfAbsent(key, dataSource);

            if (previousValue != null) {
                dataSource.close();
                dataSource = previousValue;
            }
        }

        return dataSource;
    }

    /**
     * Get a connection pool for the Mongo database with the given details. If a connection pool already exists, it will
     * return that. If one does not yet exist, it will create one and return that.
     *
     * @param host              The host.
     * @param adminDatabaseName The admin database name.
     * @param username          The username.
     * @param password          The password.
     * @param port              The port.
     * @return The connection pool as a DataSource.
     */
    public MongoClient getMongoConnectionPool(String host, String adminDatabaseName, String username, String password,
                                              int port) {
        String key = host + "," + adminDatabaseName + "," + username;
        MongoClient mongoClient = mongoConnectionPools.get(key);

        if (mongoClient == null) {
            mongoClient = createMongoConnectionPool(host, adminDatabaseName, username, password, port);
            MongoClient previousValue = mongoConnectionPools.putIfAbsent(key, mongoClient);

            if (previousValue != null) {
                mongoClient.close();
                mongoClient = previousValue;
            }
        }

        return mongoClient;
    }

    /**
     * Get a connection pool for the Mongo database with the given details. If a connection pool already exists, it will
     * return that. If one does not yet exist, it will create one and return that.
     *
     * @param host The host.
     * @param port The port.
     * @return The connection pool as a DataSource.
     */
    public MongoClient getMongoConnectionPool(String host, int port) {
        MongoClient mongoClient = mongoConnectionPools.get(host);

        if (mongoClient == null) {
            mongoClient = createMongoConnectionPool(host, port);
            MongoClient previousValue = mongoConnectionPools.putIfAbsent(host, mongoClient);

            if (previousValue != null) {
                mongoClient.close();
                mongoClient = previousValue;
            }
        }

        return mongoClient;
    }

    /**
     * Get a set of all the MySQL connection pools.
     *
     * @return Set of connection pools as HikariDataSources.
     */
    public Set<HikariDataSource> getAllMysqlConnectionPools() {
        return new HashSet<>(mysqlConnectionPools.values());
    }

    /**
     * Get a map of all the MySQL connection pools.
     *
     * @return map of connection pools as HikariDataSources.
     */
    public Map<String, HikariDataSource> getMapOfSqlConnectionPools() {
        return mysqlConnectionPools;
    }

    /**
     * Get a map of all the Mongo connection pools.
     *
     * @return map of connection pools as Mongo clients.
     */
    public Map<String, MongoClient> getMapOfMongoConnectionPools() {
        return mongoConnectionPools;
    }

    /**
     * Create a connection pool for a MySQL database.
     *
     * @param host     The host.
     * @param database The database.
     * @param username The username.
     * @param password The password.
     * @param port     The port.
     * @return The new connection pool as a DataSource.
     */
    private HikariDataSource createMysqlConnectionPool(String host, String database, String username, String password,
                                                       int port) {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setConnectionTimeout(mysqlConnectionTimeout);
        dataSource.setMaximumPoolSize(mysqlMaximumPoolSize);
        dataSource.setMinimumIdle(mysqlMinimumIdle);
        dataSource.setIdleTimeout(mysqlIdleTimeout);
        dataSource.setRegisterMbeans(true);
        return dataSource;
    }

    /**
     * Create a connection pool for a Mongo database.
     *
     * @param host              The host.
     * @param adminDatabaseName The admin database name.
     * @param username          The username.
     * @param password          The password.
     * @param port              The port.
     * @return The new connection pool as a MongoClient.
     */
    private MongoClient createMongoConnectionPool(String host, String adminDatabaseName, String username,
                                                  String password, int port) {
        String uri = "mongodb://" + username + ":" + password + "@" + host + ":" + port + "/?authSource="
                + adminDatabaseName + "&authMechanism=SCRAM-SHA-1";
        return createMongoConnectionPoolFromUri(uri);
    }

    /**
     * Create a connection pool for a Mongo database.
     *
     * @param host The host.
     * @param port The port.
     * @return The new connection pool as a MongoClient.
     */
    private MongoClient createMongoConnectionPool(String host, int port) {
        String uri = "mongodb://" + host + ":" + port + "/";
        return createMongoConnectionPoolFromUri(uri);
    }

    /**
     * Given a URI, create a connection pool for a Mongo database.
     *
     * @param uriString The URI.
     * @return The new connection pool as a MongoClient.
     */
    private MongoClient createMongoConnectionPoolFromUri(String uriString) {
        MongoClientOptions.Builder optionsBuilder = MongoClientOptions.builder()
                .connectionsPerHost(mongoMaximumPoolSize)
                .minConnectionsPerHost(mongoMinimumPoolSize)
                .maxConnectionIdleTime(mongoIdleTimeout)
                .maxWaitTime(mongoConnectionTimeout);
        return new MongoClient(new MongoClientURI(uriString, optionsBuilder));
    }

    /**
     * Close all connection pools.
     */
    private void closeConnectionPools() {
        Logger.info("event=\"Closing all connection pools in the connection pool manager.\"");

        for (HikariDataSource dataSource : mysqlConnectionPools.values()) {
            dataSource.close();
        }
        for (MongoClient mongoClient : mongoConnectionPools.values()) {
            mongoClient.close();
        }
    }
}
