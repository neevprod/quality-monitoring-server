package util;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.assistedinject.Assisted;
import controllers.SecureController;
import play.db.jpa.JPAApi;
import play.libs.Json;
import services.PermissionSettingsValueService;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class SessionData {
    private final JPAApi jpaApi;
    private final PermissionSettingsValueService permissionSettingsValueService;

    private final Long userId;
    private final String userEmail;
    private final String userName;
    private final boolean apiUser;
    private final UserPermissionData userPermissionData;

    public interface Factory {
        SessionData create(JsonNode sessionJson);
    }

    @Inject
    SessionData(JPAApi jpaApi, PermissionSettingsValueService permissionSettingsValueService,
                        @Assisted JsonNode sessionJson) {
        this.jpaApi = jpaApi;
        this.permissionSettingsValueService = permissionSettingsValueService;

        this.userId = sessionJson.get("userId").asLong();
        if (sessionJson.get("userEmail") != null) {
            this.userEmail = sessionJson.get("userEmail").asText();
        } else {
            this.userEmail = null;
        }
        this.userName = sessionJson.get("userName").asText();
        this.apiUser = sessionJson.get("apiUser").asBoolean();

        JsonNode userPermissionsJson = sessionJson.get("userPermissions");
        UserPermissionData userPermissionData;
        try {
            userPermissionData = Json.mapper().treeToValue(userPermissionsJson, UserPermissionData.class);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        this.userPermissionData = userPermissionData;
    }

    /**
     * Get the user ID of the logged in user.
     *
     * @return The user ID.
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Get the email of the logged in user.
     *
     * @return The user's email.
     */
    public Optional<String> getUserEmail() {
        if (userEmail != null) {
            return Optional.of(userEmail);
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get the name of the logged in user.
     *
     * @return The user name.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Find out whether the logged in user is an API user.
     *
     * @return A boolean indicating whether the user is an API user.
     */
    public boolean isApiUser() {
        return apiUser;
    }

    /**
     * Find out whether the logged in user is an admin user.
     *
     * @return A boolean value indicating whether the user is an admin user.
     */
    public boolean isAdmin() {
        return userPermissionData.isAdmin();
    }

    /**
     * Find out whether the logged in user has the given access type to the given permission in the given module.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param moduleName The module name.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasModuleLevelPermission(String permissionName, SecureController.AccessType accessType,
                                            String moduleName) {
        if (isAdmin()) {
            return true;
        } else {
            Optional<ModuleLevelPermissions> permissionsOptional =
                    userPermissionData.getModuleLevelPermissions(moduleName, jpaApi, permissionSettingsValueService);
            return permissionsOptional.isPresent() &&
                    permissionsOptional.get().hasPermission(permissionName, accessType);
        }
    }

    /**
     * Get the set of previewers that the logged in user has access to for the given module.
     *
     * @param moduleName The module name.
     * @return A set of previewerIds.
     */
    public Set<Integer> getAllowedPreviewerIds(String moduleName) {
        Optional<TenantLevelPermissions> permissionsOptional =
                userPermissionData.getTenantLevelPermissions(moduleName, jpaApi, permissionSettingsValueService);
        if (permissionsOptional.isPresent()) {
            return permissionsOptional.get().getAllowedPreviewerIds();
        } else {
            return new HashSet<>();
        }
    }

    /**
     * Get the set of tenants that the logged in user has access to for the given module and previewer. This method
     * will filter out wildcards and not take them into account. To find out if the user has wildcard access to a
     * previewer, use the method {@link #hasWildcardAccessToPreviewer(String, Integer)}.
     *
     * @param moduleName The module name.
     * @param previewerId The ID of the previewer in question.
     * @return A set of tenantIds.
     */
    public Set<Long> getAllowedTenantIds(String moduleName, Integer previewerId) {
        Optional<TenantLevelPermissions> permissionsOptional =
                userPermissionData.getTenantLevelPermissions(moduleName, jpaApi, permissionSettingsValueService);
        if (permissionsOptional.isPresent()) {
            return permissionsOptional.get().getAllowedTenantIds(previewerId);
        } else {
            return new HashSet<>();
        }
    }

    /**
     * Find out whether the logged in user has wildcard access to the given previewer in the given module.
     *
     * @param moduleName The module name.
     * @param previewerId The ID of the previewer.
     * @return A boolean indicating whether the user has wildcard access.
     */
    public boolean hasWildcardAccessToPreviewer(String moduleName, Integer previewerId) {
        Optional<TenantLevelPermissions> permissionsOptional =
                userPermissionData.getTenantLevelPermissions(moduleName, jpaApi, permissionSettingsValueService);
        return permissionsOptional.isPresent() && permissionsOptional.get().hasWildcardAccessToPreviewer(previewerId);
    }

    /**
     * Find out whether the logged in user has the given access type to the given permission for the given tenant in
     * the given module.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param moduleName The module name.
     * @param previewerId The ID of the previewer that contains the tenant to access.
     * @param tenantId The ID of the tenant to access.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasTenantLevelPermission(String permissionName, SecureController.AccessType accessType,
                                            String moduleName, Integer previewerId, Long tenantId) {
        if (isAdmin()) {
            return true;
        } else {
            Optional<TenantLevelPermissions> permissionsOptional =
                    userPermissionData.getTenantLevelPermissions(moduleName, jpaApi, permissionSettingsValueService);
            return permissionsOptional.isPresent() &&
                    permissionsOptional.get().hasPermission(permissionName, accessType, previewerId, tenantId);
        }
    }

    /**
     * Find out whether the logged in user has the given access type to the given permission for any tenant under the
     * given previewer in the given module.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param moduleName The module name.
     * @param previewerId The ID of the previewer.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasAnyTenantLevelPermission(String permissionName, SecureController.AccessType accessType,
                                               String moduleName, Integer previewerId) {
        if (isAdmin()) {
            return true;
        } else {
            Optional<TenantLevelPermissions> permissionsOptional =
                    userPermissionData.getTenantLevelPermissions(moduleName, jpaApi,
                            permissionSettingsValueService);
            return permissionsOptional.isPresent() &&
                    permissionsOptional.get().hasPermissionForAnyTenant(permissionName, accessType, previewerId);
        }
    }

    /**
     * Get the set of environments that the logged in user has access to for the given module. This method
     * will filter out wildcards and not take them into account. To find out if the user has wildcard access to the
     * environments, use the method {@link #hasWildcardAccessToEnvironments(String)}.
     *
     * @param moduleName The module name.
     * @return A set of tenantIds.
     */
    public Set<Integer> getAllowedEnvironmentIds(String moduleName) {
        Optional<EnvironmentLevelPermissions> permissionsOptional =
                userPermissionData.getEnvironmentLevelPermissions(moduleName, jpaApi, permissionSettingsValueService);
        if (permissionsOptional.isPresent()) {
            return permissionsOptional.get().getAllowedEnvironmentIds();
        } else {
            return new HashSet<>();
        }
    }

    /**
     * Find out whether the logged in user has wildcard access to the environments.
     *
     * @param moduleName The module name.
     * @return A boolean indicating whether the user has wildcard access.
     */
    public boolean hasWildcardAccessToEnvironments(String moduleName) {
        Optional<EnvironmentLevelPermissions> permissionsOptional =
                userPermissionData.getEnvironmentLevelPermissions(moduleName, jpaApi, permissionSettingsValueService);
        return permissionsOptional.isPresent() && permissionsOptional.get().hasWildcardAccess();
    }

    /**
     * Find out whether the logged in user has the given access type to the given permission for the given environment
     * in the given module.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param moduleName The module name.
     * @param environmentId The ID of the environment to access.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasEnvironmentLevelPermission(String permissionName, SecureController.AccessType accessType,
                                            String moduleName, Integer environmentId) {
        if (isAdmin()) {
            return true;
        } else {
            Optional<EnvironmentLevelPermissions> permissionsOptional =
                    userPermissionData.getEnvironmentLevelPermissions(moduleName, jpaApi,
                            permissionSettingsValueService);
            return permissionsOptional.isPresent() &&
                    permissionsOptional.get().hasPermission(permissionName, accessType, environmentId);
        }
    }

    /**
     * Find out whether the logged in user has the given access type to the given permission for any environment in the
     * given module.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param moduleName The module name.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasAnyEnvironmentLevelPermission(String permissionName, SecureController.AccessType accessType,
                                                    String moduleName) {
        if (isAdmin()) {
            return true;
        } else {
            Optional<EnvironmentLevelPermissions> permissionsOptional =
                    userPermissionData.getEnvironmentLevelPermissions(moduleName, jpaApi,
                            permissionSettingsValueService);
            return permissionsOptional.isPresent() &&
                    permissionsOptional.get().hasPermissionForAnyEnvironment(permissionName, accessType);
        }
    }
}
