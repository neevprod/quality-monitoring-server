package util;

import models.UserPermissions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TenantLevelPermissionData extends ModulePermissionData {
    private final Map<Integer, Map<String, Long>> permissions;

    /**
     * Create a new TenantLevelPermissionData instance.
     */
    public TenantLevelPermissionData() {
        super(null);
        permissions = new HashMap<>();
    }

    /**
     * Create a new TenantLevelPermissionData instance with the given module name and user permissions.
     *
     * @param moduleName The module name.
     * @param userPermissions The user permissions.
     */
    public TenantLevelPermissionData(String moduleName, List<UserPermissions> userPermissions) {
        super(moduleName);
        permissions = userPermissions.stream()
                .collect(Collectors.groupingBy(UserPermissions::getPreviewerId,
                        Collectors.toMap(p -> p.getIsWildcard() ? "*" : p.getTenantId().toString(),
                                UserPermissions::getPermissionSettingsId)));
    }

    /**
     * Get the permissions.
     *
     * @return The permissions.
     */
    public Map<Integer, Map<String, Long>> getPermissions() {
        return permissions;
    }
}
