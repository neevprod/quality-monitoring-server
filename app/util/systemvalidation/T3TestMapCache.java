package util.systemvalidation;

import com.pearson.itautomation.testmaps.models.Testmap;
import org.apache.commons.text.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;
import play.Logger;
import util.t3.T3TestMapException;
import util.t3.T3Testmap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class T3TestMapCache {
    private T3TestMapCache() {
    }

    private static final Map<CacheKey, Testmap> cachedTestMaps = new ConcurrentHashMap<>();

    public static Testmap getT3TestMapFromCache(long divJobExecId, String t3TestMapName) {
        T3TestMapCache.CacheKey key = new T3TestMapCache.CacheKey(divJobExecId, t3TestMapName);
        return T3TestMapCache.cachedTestMaps.get(key);
    }

    public static synchronized Testmap downloadTestMapFromT3(long divJobExecId, String accountCode, String adminCode, String testCode, String formCode)
            throws T3TestMapException {
        T3Testmap.T3TestMapResult t3TestMapResult = T3Testmap.getLatestTestMapVersion(accountCode, adminCode, testCode, formCode);
        String t3TestMapName = t3TestMapResult.getName();
        String testmapId = t3TestMapResult.getId();
        String testmapVersion = t3TestMapResult.getVersion();

        T3TestMapCache.CacheKey key = new T3TestMapCache.CacheKey(divJobExecId, t3TestMapName);
        Testmap t3TestMap = T3TestMapCache.cachedTestMaps.get(key);
        if (t3TestMap == null) {
            Logger.info("event= \"Downloading testMap from T3.\", name= \"{}\", Id= \"{}\", version= \"{}\"", t3TestMapName, testmapId, testmapVersion);
            t3TestMap = T3Testmap.getTestMap(testmapId, testmapVersion, t3TestMapName);
            T3TestMapCache.cachedTestMaps.put(key, t3TestMap);
        }
        return t3TestMap;
    }

    public static void removeT3TestMapFromCache(long divJobExecId) {
        Set<CacheKey> cacheKeys = cachedTestMaps.keySet().stream().filter(key -> key.getJobId() == divJobExecId).collect(Collectors.toSet());
        for (T3TestMapCache.CacheKey key : cacheKeys) {
            T3TestMapCache.cachedTestMaps.remove(key);
        }
    }

    public static Document updateNamespace(Document t3TestMapDocument) throws ParserConfigurationException, SAXException, IOException, TransformerException {
        Node node = t3TestMapDocument.getElementsByTagName("xmlExtract").item(0);
        String nodeText = StringEscapeUtils.unescapeXml(innerXml(node));

        int lastIndex = 0;
        int testmapIndex = nodeText.indexOf("<tes:testMap");
        int count = 0;
        while (lastIndex != -1 && lastIndex < testmapIndex) {
            lastIndex = nodeText.indexOf("?xml version=", lastIndex);
            if (lastIndex != -1 && lastIndex < testmapIndex) {
                count++;
                lastIndex += "xml version=".length();
            }
        }

        if (count > 1) {
            nodeText = nodeText.replaceFirst("<\\?xml version=\".\\..\" encoding=\"UTF-.{1,4}\"\\?>", "");
        }

        nodeText = nodeText.replaceAll("tes:", "");
        nodeText = nodeText.replaceAll("xmlns:tes=\"http://pearson.com/ai/testcontentmanagement\"",
                "xmlns=\"http://pearson.com/ai/testcontentmanagement/testnext\"");
        nodeText = nodeText.replaceAll("xmlns:tes=\"http://pearson.com/ai/testcontentmanagement/testnext\"",
                "xmlns=\"http://pearson.com/ai/testcontentmanagement/testnext\"");

        DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
        f.setNamespaceAware(true);
        DocumentBuilder db = f.newDocumentBuilder();
        Document document = db.parse(new ByteArrayInputStream(nodeText.getBytes(StandardCharsets.UTF_8)));
        writeStuff(document);
        return document;
    }

    private static String innerXml(Node node) {
        DOMImplementationLS lsImpl = (DOMImplementationLS) node.getOwnerDocument().getImplementation().getFeature("LS", "3.0");
        LSSerializer lsSerializer = lsImpl.createLSSerializer();
        NodeList childNodes = node.getChildNodes();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < childNodes.getLength(); i++) {
            sb.append(lsSerializer.writeToString(childNodes.item(i)));
        }
        return sb.toString();
    }

    private static void writeStuff(Document doc) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
    }

    private static class CacheKey {
        private final long jobId;
        private final String testMapName;

        private CacheKey(long jobId, String testMapName) {
            this.jobId = jobId;
            this.testMapName = testMapName;
        }

        private long getJobId() {
            return jobId;
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CacheKey cacheKey = (CacheKey) o;
            return jobId == cacheKey.jobId &&
                    Objects.equals(testMapName, cacheKey.testMapName);
        }

        @Override
        public int hashCode() {

            return Objects.hash(jobId, testMapName);
        }
    }
}
