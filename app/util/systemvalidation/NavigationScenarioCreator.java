package util.systemvalidation;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pearson.itautomation.testscenariocreator.inputs.ItemResponsePool;
import org.apache.commons.lang3.Validate;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Used to create navigation scenarios like rightToWrong, wrongToRight and wrongToWrong.
 */
public class NavigationScenarioCreator {
    public static final Set<String> NAVIGATION_SCENARIOS = Stream.of("rightToWrong", "wrongToRight", "wrongToWrong")
            .collect(Collectors.toSet());
    private final String testCode;
    private final String formCode;
    private final String scenarioName;

    /**
     * @param testCode     The test code
     * @param formCode     The form Code
     * @param scenarioName The scenario class name
     */
    NavigationScenarioCreator(final String testCode, final String formCode, final String scenarioName) {
        Validate.notEmpty(testCode, "Test Code should not be empty.");
        Validate.notEmpty(formCode, "Form Code should not be empty.");
        Validate.notEmpty(scenarioName, "Scenario Class should not be empty.");
        this.testCode = testCode;
        this.formCode = formCode;
        this.scenarioName = scenarioName;
    }

    /**
     * @param sc  The instance of {@link ScenarioCreatorUtil}
     * @param irp The instance of {@link ItemResponsePool}
     * @return The instance of {@link TestNavScenario} if correct and incorrect responses found in the given <code>irp</code> otherwise null.
     */
    public final TestNavScenario generateNavigationScenario(final ScenarioCreatorUtil sc, final ItemResponsePool irp) {
        if (sc == null || irp == null) {
            return null;
        }
        List<String> epenPercentCorrectItems = new ArrayList<>();
        List<TestNavScenario> incorrectScenarios = sc.getTestNavScenario(testCode, formCode, "ALLINCORRECT", epenPercentCorrectItems, irp);
        Random random = new Random();
        TestNavScenario testNavScenario = null;
        if ("rightToWrong".equalsIgnoreCase(scenarioName) || "wrongToRight".equalsIgnoreCase(scenarioName)) {
            List<TestNavScenario> correctScenarios = sc.getTestNavScenario(testCode, formCode, "ALLCORRECT", epenPercentCorrectItems, irp);
            if (!correctScenarios.isEmpty() && !incorrectScenarios.isEmpty()) {
                TestNavScenario correctScenario = correctScenarios.get(random.nextInt(correctScenarios.size()));
                TestNavScenario incorrectScenario = incorrectScenarios.get(random.nextInt(incorrectScenarios.size()));
                if ("rightToWrong".equalsIgnoreCase(scenarioName)) {
                    testNavScenario = createNavigationScenario(correctScenario, incorrectScenario, sc);
                } else {
                    testNavScenario = createNavigationScenario(incorrectScenario, correctScenario, sc);
                }
            }
        } else if ("wrongToWrong".equalsIgnoreCase(scenarioName)) {

            if (incorrectScenarios.size() > 1) {
                TestNavScenario correctScenario = new ArrayList<>(incorrectScenarios).remove(random.nextInt(incorrectScenarios.size()));
                TestNavScenario incorrectScenario = incorrectScenarios.get(random.nextInt(incorrectScenarios.size()));
                testNavScenario = createNavigationScenario(incorrectScenario, correctScenario, sc);

            } else if (incorrectScenarios.size() == 1) {
                List<TestNavScenario> nonAttemptedScenarios = sc.getTestNavScenario(testCode, formCode, "NONATTEMPTED", epenPercentCorrectItems, irp);
                TestNavScenario incorrectScenario = incorrectScenarios.get(0);
                TestNavScenario nonAttemptedScenario = nonAttemptedScenarios.get(0);
                testNavScenario = createNavigationScenario(incorrectScenario, nonAttemptedScenario, sc);
            }
        }
        return testNavScenario;
    }

    private TestNavScenario createNavigationScenario(final TestNavScenario firstScenario, final TestNavScenario secondScenario, ScenarioCreatorUtil sc) {

        JsonObject firstTestNavScenario = new JsonParser().parse(firstScenario.getScenario()).getAsJsonObject();
        JsonObject secondTestNavScenario = new JsonParser().parse(secondScenario.getScenario()).getAsJsonObject();
        firstTestNavScenario.addProperty("scenarioName", scenarioName);
        firstTestNavScenario.addProperty("totalPoints", secondTestNavScenario.get("totalPoints").getAsDouble());
        JsonArray firstScenarioItemResponses = firstTestNavScenario.get("itemResponses").getAsJsonArray();
        JsonArray secondScenarioItemResponses = secondTestNavScenario.get("itemResponses").getAsJsonArray();
        Map<String, JsonObject> itemIdentifierToSecondScenarioResponses = getItemIdentifierToResponseMap(secondScenarioItemResponses);
        for (JsonElement firstScenarioItemResponse : firstScenarioItemResponses) {
            JsonObject firstItemResponse = firstScenarioItemResponse.getAsJsonObject();
            String uin = firstItemResponse.get("uin").getAsString();
            JsonObject secondItemResponse = itemIdentifierToSecondScenarioResponses.get(uin);
            if (secondItemResponse == null && (sc.getSampleItems().contains(uin) || sc.getSurveyItems().contains(uin))) {
                secondItemResponse = new JsonParser().parse(firstItemResponse.getAsJsonObject().toString()).getAsJsonObject();
            }
            addVersionToScoreOutcome(firstItemResponse, secondItemResponse);
            Map<String, JsonObject> didToResponseMap = getDidToResponseMap(secondItemResponse);
            addVersionToResponse(firstItemResponse, didToResponseMap);
        }

        return new TestNavScenario(firstScenario.getScopeTreePath(), testCode, formCode,
                firstScenario.getTestmapId(), firstScenario.getTestmapName(), firstScenario.getTestmapVersion(),
                firstScenario.getTestMapPublishFormat(), scenarioName, scenarioName,
                firstTestNavScenario.toString());
    }

    private Map<String, JsonObject> getItemIdentifierToResponseMap(final JsonArray inCorrectItemResponses) {
        Map<String, JsonObject> itemIdentifierToIncorrectResponses = new HashMap<>();
        for (JsonElement inCorrectItemResponse : inCorrectItemResponses) {
            String uin = inCorrectItemResponse.getAsJsonObject().get("uin").getAsString();
            itemIdentifierToIncorrectResponses.put(uin, inCorrectItemResponse.getAsJsonObject());
        }
        return itemIdentifierToIncorrectResponses;
    }

    private void addVersionToScoreOutcome(final JsonObject correctItemResponse, final JsonObject incorrectItemResponse) {
        JsonArray outComes = correctItemResponse.getAsJsonObject().get("outcomes").getAsJsonArray();
        for (JsonElement outcome : outComes) {
            JsonObject score = outcome.getAsJsonObject();
            score.addProperty("version", 0);
        }

        JsonArray incorrectResponseOutComes = incorrectItemResponse.getAsJsonObject().get("outcomes").getAsJsonArray();
        for (JsonElement outcome : incorrectResponseOutComes) {
            JsonObject score = outcome.getAsJsonObject();
            score.addProperty("version", 1);
            outComes.add(score);
        }
    }

    private Map<String, JsonObject> getDidToResponseMap(final JsonObject incorrectItemResponse) {
        Map<String, JsonObject> didToResponseMap = new HashMap<>();
        JsonArray incorrectMods = incorrectItemResponse.getAsJsonObject().get("mods").getAsJsonArray();
        for (JsonElement mod : incorrectMods) {
            JsonObject response = mod.getAsJsonObject();
            JsonObject obj = createResponseNodeWithVersion(1, response);
            String did = response.get("did").getAsString();
            didToResponseMap.put(did, obj);
        }
        return didToResponseMap;
    }

    private void addVersionToResponse(final JsonObject correctItemResponse, final Map<String, JsonObject> didToResponseMap) {
        JsonArray mods = correctItemResponse.getAsJsonObject().get("mods").getAsJsonArray();
        for (JsonElement mod : mods) {
            JsonArray responses = new JsonArray();
            JsonObject response = mod.getAsJsonObject();
            JsonObject obj = createResponseNodeWithVersion(0, response);
            responses.add(obj);
            String did = response.get("did").getAsString();
            responses.add(didToResponseMap.get(did));

            response.add("responses", responses);
            response.remove("r");
        }
    }

    private JsonObject createResponseNodeWithVersion(final int responseVersion, final JsonObject response) {
        JsonObject obj = new JsonObject();
        obj.add("r", response.get("r"));
        obj.addProperty("version", responseVersion);
        return obj;
    }

    /**
     * Removes the lower version of score from outcome node and response from responses.
     *
     * @param scenario The Navigation testNav Scenario string
     * @return The testNav scenario with highest version of outcomes and responses.
     */
    public static String removeLowerVersionResponseAndOutcome(final String scenario) {
        JsonObject scenarioJson = new JsonParser().parse(scenario).getAsJsonObject();
        JsonArray itemResponses = scenarioJson.get("itemResponses").getAsJsonArray();
        for (JsonElement itemResponseEle : itemResponses) {
            JsonObject itemResponse = itemResponseEle.getAsJsonObject();
            removeLowerVersionOutcomes(itemResponse);
            JsonArray mods = itemResponse.getAsJsonObject().get("mods").getAsJsonArray();
            removeLowerVersionResponses(mods);
        }
        return scenarioJson.toString();
    }

    private static void removeLowerVersionResponses(final JsonArray mods) {
        for (JsonElement mod : mods) {
            JsonArray responses = mod.getAsJsonObject().get("responses").getAsJsonArray();
            JsonElement highestVersionOutcome = getHighestVersionNode(responses);
            mod.getAsJsonObject().remove("responses");
            mod.getAsJsonObject().add("r", highestVersionOutcome.getAsJsonObject().get("r"));
        }
    }

    private static void removeLowerVersionOutcomes(final JsonObject itemResponse) {
        JsonArray outComes = itemResponse.getAsJsonObject().get("outcomes").getAsJsonArray();
        JsonElement highestVersionOutcome = getHighestVersionNode(outComes);
        outComes.add(highestVersionOutcome);
    }

    private static JsonElement getHighestVersionNode(final JsonArray responses) {
        Iterator<JsonElement> itr = responses.iterator();
        JsonElement highestVersionOutcome = null;
        while (itr.hasNext()) {
            JsonElement outcome = itr.next();
            if (highestVersionOutcome == null
                    || highestVersionOutcome.getAsJsonObject().get("version").getAsInt() < outcome.getAsJsonObject().get("version").getAsInt()) {
                highestVersionOutcome = outcome;
            }
            itr.remove();
        }
        return highestVersionOutcome;
    }
}
