package util.systemvalidation.oe_response;

import com.google.gson.JsonObject;

public class DefaultOpenEndedInteractionResponse extends OpenEndedInteractionResponse {
    public DefaultOpenEndedInteractionResponse(JsonObject mod, String newResponse, String responseIdentifier) {
        super(mod, newResponse, responseIdentifier);
    }

    @Override
    public void update() {
        mod.remove("r");
        mod.addProperty("r", newResponse + ", " + "InteractionIdentifier: " + responseIdentifier);

    }
}
