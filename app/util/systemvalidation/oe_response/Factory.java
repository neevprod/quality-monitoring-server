package util.systemvalidation.oe_response;

import com.google.gson.JsonObject;

public class Factory {

    public static OpenEndedInteractionResponse getHumanScorableResponseInstance(String interactionType, JsonObject mod, String extraResponse, String responseIdentifier) {
        if (OpenEndedInteractionResponse.getInteractionType().contains(interactionType)) {
            return new DefaultOpenEndedInteractionResponse(mod, extraResponse, responseIdentifier);
        } else if (ExtendedTextInteractionRteResponse.NAME.equalsIgnoreCase(interactionType)) {
            return new ExtendedTextInteractionRteResponse(mod, extraResponse, responseIdentifier);
        } else if (ExtendedTextInteractionTeiEeResponse.NAME.equals(interactionType)) {
            return new ExtendedTextInteractionTeiEeResponse(mod, extraResponse, responseIdentifier);
        }
        return null;
    }
}
