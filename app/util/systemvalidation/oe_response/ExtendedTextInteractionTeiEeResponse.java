package util.systemvalidation.oe_response;

import com.google.gson.JsonObject;

public class ExtendedTextInteractionTeiEeResponse extends OpenEndedInteractionResponse {
    public static final String NAME = "extendedTextInteraction.tei-ee";

    public ExtendedTextInteractionTeiEeResponse(JsonObject mod, String newResponse, String responseIdentifier) {
        super(mod, newResponse, responseIdentifier);
    }

    @Override
    public void update() {
        mod.remove("r");
        JsonObject content = new JsonObject();
        content.addProperty("content",
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"><body><p>" + newResponse + ", "
                        + "InteractionIdentifier: " + responseIdentifier + " </p></body></html>");
        mod.add("r", content);
    }
}
