package util.systemvalidation.oe_response;

import com.google.gson.JsonObject;

public class ExtendedTextInteractionRteResponse extends OpenEndedInteractionResponse {
    public static final String NAME = "extendedTextInteraction.rte";

    public ExtendedTextInteractionRteResponse(JsonObject mod, String newResponse, String responseIdentifier) {
        super(mod, newResponse, responseIdentifier);
    }

    @Override
    public void update() {
        mod.remove("r");
        JsonObject content = new JsonObject();
        content.addProperty("rawText", newResponse + ", InteractionIdentifier: " + responseIdentifier);
        content.addProperty("richText", newResponse + ", InteractionIdentifier: " + responseIdentifier);
        mod.add("r", content);
    }
}
