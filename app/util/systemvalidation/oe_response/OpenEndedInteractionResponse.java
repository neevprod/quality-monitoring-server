package util.systemvalidation.oe_response;

import com.google.gson.JsonObject;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Used to update the response of open ended items so that the response contains test code, form code and item uin.
 * This updated response is helpful to the ePEN team.
 */
public abstract class OpenEndedInteractionResponse {
    final JsonObject mod;
    final String newResponse;
    final String responseIdentifier;

    private static final Set<String> INTERACTION_TYPES_TO_UPDATE = Stream.of("extendedTextInteraction")
            .collect(Collectors.toSet());

    OpenEndedInteractionResponse(JsonObject mod, String newResponse, String responseIdentifier) {
        this.mod = mod;
        this.newResponse = newResponse;
        this.responseIdentifier = responseIdentifier;
    }

    /**
     * Replaces the original response with test code, form code and item uin.
     */
    public abstract void update();

    public static Set<String> getInteractionType() {
        return Collections.unmodifiableSet(INTERACTION_TYPES_TO_UPDATE);
    }
}
