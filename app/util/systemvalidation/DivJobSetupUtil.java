package util.systemvalidation;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.pearson.itautomation.panext.studentsession.SessionInfo;
import com.pearson.itautomation.panext.studentsession.StudentTestSession;
import com.pearson.itautomation.panext.studentsession.util.StudentTestSessionExtractor;
import com.pearson.itautomation.testmaps.models.Item;
import com.pearson.itautomation.testmaps.models.Testmap;
import models.systemvalidation.*;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import play.Logger;
import play.libs.Json;
import services.systemvalidation.StudentScenarioService;
import services.systemvalidation.TestmapDetailService;
import util.ConnectionPoolManager;
import util.t3.T3Testmap;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.*;

public class DivJobSetupUtil {
    public static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss.S").registerTypeAdapter(Timestamp.class, new TimestampDeserializer()).create();
    private static final Type TESTMAP_TYPE = new TypeToken<List<Testmap>>() {
    }.getType();
    private final ConnectionPoolManager connectionPoolManager;

    @Inject
    DivJobSetupUtil(ConnectionPoolManager connectionPoolManager) {
        this.connectionPoolManager = connectionPoolManager;
    }

    public static String getBatteryUnitFormCodes(List<StudentTestSession> testSessions) {
        List<String> unitTestCodes = new ArrayList<>();
        List<String> unitFormCodes = new ArrayList<>();
        String batteryTestCode = "";
        for (StudentTestSession studentTestSession : testSessions) {
            batteryTestCode = studentTestSession.getBatteryTestCode();
            String unitTestCode = studentTestSession.getTestCode();
            unitTestCodes.add(unitTestCode);
            String unitFormCode = studentTestSession.getFormCode();
            unitFormCodes.add(unitFormCode);
        }
        return batteryTestCode + StringUtils.join(unitTestCodes, "#") + StringUtils.join(unitFormCodes, "#");
    }

    public List<Testmap> getT3Testmap(DivJobSetupUtil.ScopeTreePath stp, TestDetail testDetail, TestmapDetailService testmapDetailService) {
        List<Testmap> testMaps = new ArrayList<>();
        try {
            T3Testmap.T3TestMapResult t3Testmap = T3Testmap.getLatestTestMapVersion(stp.getAccountCode(), stp.getAdminCode(), testDetail.getTestCode(), testDetail.getFormCode());
            int testmapId = Integer.parseInt(t3Testmap.getId());
            String testmapName = t3Testmap.getName();
            int testmapVersion = Integer.parseInt(t3Testmap.getVersion());

            Optional<TestmapDetail> result = testmapDetailService.findByTestmapIdNameAndVersion(testmapId, testmapName, testmapVersion);

            if (result.isPresent() && result.get().getTestmapXml() != null) {
                testMaps.addAll(GSON.fromJson(result.get().getTestmapXml(), TESTMAP_TYPE));
            } else {
                Testmap testmapXml = T3Testmap.getTestMap(t3Testmap.getId(), t3Testmap.getVersion(), testmapName);
                testMaps.add(testmapXml);
            }

            if (testMaps.size() != 1) {
                Logger.error("event=\"Failed to retrieve the latest version of T3Testmap.\", adminCode= \"{}\", accountCode= \"{}\"", stp.getAdminCode(), stp.getAccountCode());
                throw new RuntimeException("Failed to retrieve the latest version of T3Testmap");
            }
            JsonNode json = Json.toJson(testMaps);
            if (result.isPresent()) {
                result.get().setTestmapXml(json.toString());
            } else {
                TestmapDetail testmapDetail = new TestmapDetail();
                testmapDetail.setTestmapId(testmapId);
                testmapDetail.setTestmapName(testmapName);
                testmapDetail.setTestmapVersion(testmapVersion);
                testmapDetail.setTestmapXml(json.toString());
                testmapDetail.setTestmapPublishFormat("E");
                testmapDetailService.create(testmapDetail);
            }
        } catch (Exception ex) {
            Logger.error("event=\"Failed to retrieve the latest version of T3Testmap.\", " +
                            "adminCode= \"{}\", accountCode= \"{}\"",
                    stp.getAdminCode(), stp.getAccountCode(), ex);
        }
        return testMaps;
    }

    /**
     * Construct a StudentTestSessionExtractor.
     *
     * @param connection The instance of {@link DbConnection}
     * @return The new instance of StudentTestSessionExtractor
     */
    public StudentTestSessionExtractor getStudentTestSessionExtractor(final DbConnection connection) {
        DataSource dataSource = connectionPoolManager.getMysqlConnectionPool(connection.getHost(),
                connection.getDbName(), connection.getDbUser(), connection.getDbPass(), connection.getPort());
        return new StudentTestSessionExtractor(dataSource);
    }

    /**
     * loads the lazy initialized testNavScenario, ePEN Scenario and testSessions for a studentScenario.
     *
     * @param studentScenarioService The instance of {@link StudentScenarioService}
     * @param studentScenario        The {@link StudentScenario}
     */
    public static void initializeLazyFetchedData(StudentScenarioService studentScenarioService, StudentScenario studentScenario) {

        Optional<StudentScenario> ssResult = studentScenarioService.findByStudentScenarioId(studentScenario.getStudentScenarioId());
        if (ssResult.isPresent()) {
            StudentScenario ss = ssResult.get();
            Scenario testNavScenario = ss.getTestnavScenario();
            testNavScenario.getScenario(); //to load testNavScenario
            studentScenario.setTestnavScenario(testNavScenario);
            TestSession ts = ss.getTestSession();
            ts.getScopeCode();// to load testSession object
            studentScenario.setTestSession(ts);

            if (ss.getEpenScenario() != null) {
                Scenario ePenScenario = ss.getEpenScenario();
                ePenScenario.getScenario(); //to load ePEN scenario
                studentScenario.setEpenScenario(ePenScenario);
            }
        }
    }

    /**
     * @param testCode The testCode
     * @return The new instance of TestDetail
     */
    public TestDetail createTestDetail(final String testCode) {
        final TestDetail testDetail = new TestDetail();
        testDetail.setTestCode(testCode);
        return testDetail;
    }

    public TestDetail createTestDetailObject(String testCode, String formCode) {
        TestDetail testDetail = new TestDetail();
        testDetail.setTestCode(testCode);
        testDetail.setFormCode(formCode);
        return testDetail;
    }

    public BatteryTest createBatteryTestObject(String batteryTestCode, List<TestDetail> units) {
        return new BatteryTest(batteryTestCode, units);
    }

    /**
     * Checks ScoringDestination = 'human', if not found then reads itemXML and evaluates the XPath
     * count(//outcomeDeclaration[contains(@interpretation, 'scoreSystem:human')]).
     *
     * @param item The {@link Item}
     * @return true if ScoringDestination='human' or the output of
     * XPath(count(//outcomeDeclaration[contains(@interpretation, 'scoreSystem:human')]))>0 otherwise false.
     */
    public boolean containsEpenItem(final Item item) {
        if ("human".equals(item.getScoringDestination())) {
            return true;
        }
        final String expression = "count(//outcomeDeclaration[contains(@interpretation, 'scoreSystem:human')])";
        return containsScoreSystem(item, expression);
    }

    /**
     * Checks ScoringDestination = 'human', if not found then reads itemXML and evaluates the XPath
     * count(//outcomeDeclaration[contains(@interpretation, 'scoreSystem:human')]).
     *
     * @param item The {@link Item}
     * @return true if ScoringDestination='human' or the output of
     * XPath(count(//outcomeDeclaration[contains(@interpretation, 'scoreSystem:human')]))>0 otherwise false.
     */
    public boolean isKtItem(final Item item) {
        if ("ai".equalsIgnoreCase(item.getScoringDestination())) {
            return true;
        }
        final String expression = "count(//outcomeDeclaration[contains(@interpretation, 'scoreSystem:ai')])";
        return containsScoreSystem(item, expression);
    }

    private boolean containsScoreSystem(Item item, String expression) {
        try {
            XPath xPath = XPathFactory.newInstance().newXPath();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(item.getItemXML()));

            Document doc = dBuilder.parse(is);

            int count = ((Number) xPath.compile(expression).evaluate(doc, XPathConstants.NUMBER)).intValue();
            return count > 0;
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            Logger.error("Failed to find XPath {} in {}", expression, item.getItemXML());
        }
        return false;
    }

    /**
     * @param testCode          The test code
     * @param formCode          The form code
     * @param scenarioClassName The scenario class name
     * @return The instance of {@link NavigationScenarioCreator}
     */
    public NavigationScenarioCreator getNavigationScenarioCreator(final String testCode, final String formCode, final String scenarioClassName) {
        return new NavigationScenarioCreator(testCode, formCode, scenarioClassName);
    }

    public StudentTestSession buildStudentTestSession(TestDetail unit) {
        StudentTestSession studentTestSession = new StudentTestSession();
        studentTestSession.setTestCode(unit.getTestCode());
        studentTestSession.setFormCode(unit.getFormCode());
        studentTestSession.setTn8CustomerCode(unit.getTestNavCustomerCode());
        return studentTestSession;
    }

    public enum StatusNames {
        NOT_STARTED("Not Started"),
        IN_PROGRESS("In Progress"),
        FAILED("Failed"),
        COMPLETED("Completed"),
        COMPLETED_OE_EXCLUDED("Completed (OE Excluded)"),
        COMPLETED_OE_OP_EXCLUDED("Completed (OE-OP Excluded)"),
        COMPLETED_OE_FT_EXCLUDED("Completed (OE-FT Excluded)");
        String statusName;

        StatusNames(String name) {
            this.statusName = name;
        }

        public String getName() {
            return statusName;
        }
    }

    /**
     * This class is used to extract adminCode and accountCode from scopeTreePath.
     *
     * @author Nand Joshi
     */
    public static class ScopeTreePath {
        private final String adminCode;
        private final String accountCode;

        public ScopeTreePath(final String scopeTreePath) {
            if (!scopeTreePath.startsWith("/") || scopeTreePath.split("/").length <= 2) {
                throw new IllegalArgumentException("Invalid scopeTreePath found " + scopeTreePath);
            }
            String[] scopeTreeData = scopeTreePath.split("/");
            this.accountCode = scopeTreeData[1];
            this.adminCode = scopeTreeData[scopeTreeData.length - 1];

        }

        /**
         * @return the adminCode
         */
        public final String getAdminCode() {
            return adminCode;
        }

        /**
         * @return the accountCode
         */
        public final String getAccountCode() {
            return accountCode;
        }

    }

    @SuppressWarnings("unused")
    public static class TestDetail {
        private String testCode;
        private String formCode;
        private List<SessionInfo> testSessions;
        private boolean epenScenarioDisable;
        private List<ScenarioDetail> selectedScenarios;

        private Integer totalAvailableStudents;
        private Integer remainingStudents;
        private Integer numberOfScenarios;

        private String includesAdaptiveIndicator;
        private String previewerUrl;
        private int tenant;

        private String testNavCustomerCode;
        private String testnavWebServiceUrl;
        private String testnavClientIdentifier;
        private String testnavClientSecret;
        private String formName;

        private String testCaseCoverage;

        public final String getTestCode() {
            return testCode;
        }

        /**
         * @param testCode the testCode to set
         */
        public final void setTestCode(String testCode) {
            this.testCode = testCode;
        }

        /**
         * @return the formCode
         */
        public final String getFormCode() {
            return formCode;
        }

        /**
         * @param formCode the formCode to set
         */
        public final void setFormCode(String formCode) {
            this.formCode = formCode;
        }

        /**
         * @return the testSessions
         */
        public final List<SessionInfo> getTestSessions() {
            return testSessions;
        }

        /**
         * @param testSessions the testSessions to set
         */
        public final void setTestSessions(List<SessionInfo> testSessions) {
            this.testSessions = testSessions;
        }

        public final boolean isEpenScenarioDisable() {
            return epenScenarioDisable;
        }

        /**
         * @param epenScenarioDisable the epenScenarioDisable to set
         */
        public final void setEpenScenarioDisable(boolean epenScenarioDisable) {
            this.epenScenarioDisable = epenScenarioDisable;
        }

        public List<ScenarioDetail> getSelectedScenarios() {
            return selectedScenarios;
        }

        public void setSelectedScenarios(List<ScenarioDetail> selectedScenarios) {
            this.selectedScenarios = selectedScenarios;
        }

        /**
         * @return the totalAvailableStudents
         */
        public final Integer getTotalAvailableStudents() {
            return totalAvailableStudents;
        }

        /**
         * @param totalAvailableStudents the totalAvailableStudents to set
         */
        public final void setTotalAvailableStudents(Integer totalAvailableStudents) {
            this.totalAvailableStudents = totalAvailableStudents;
        }

        public Integer getRemainingStudents() {
            return remainingStudents;
        }

        public void setRemainingStudents(Integer remainingStudents) {
            this.remainingStudents = remainingStudents;
        }

        /**
         * @return the numberOfScenarios
         */
        public final Integer getNumberOfScenarios() {
            return numberOfScenarios;
        }

        /**
         * @param numberOfScenarios the numberOfScenarios to set
         */
        public final void setNumberOfScenarios(Integer numberOfScenarios) {
            this.numberOfScenarios = numberOfScenarios;
        }

        public String getIncludesAdaptiveIndicator() {
            return includesAdaptiveIndicator;
        }

        public void setIncludesAdaptiveIndicator(String includesAdaptiveIndicator) {
            this.includesAdaptiveIndicator = includesAdaptiveIndicator;
        }

        public String getPreviewerUrl() {
            return previewerUrl;
        }

        public void setPreviewerUrl(String previewerUrl) {
            this.previewerUrl = previewerUrl;
        }

        public int getTenant() {
            return tenant;
        }

        public void setTenant(int tenant) {
            this.tenant = tenant;
        }

        public String getTestNavCustomerCode() {
            return testNavCustomerCode;
        }

        public void setTestNavCustomerCode(String testNavCustomerCode) {
            this.testNavCustomerCode = testNavCustomerCode;
        }

        public String getTestnavWebServiceUrl() {
            return testnavWebServiceUrl;
        }

        public void setTestnavWebServiceUrl(String testnavWebServiceUrl) {
            this.testnavWebServiceUrl = testnavWebServiceUrl;
        }

        public String getTestnavClientIdentifier() {
            return testnavClientIdentifier;
        }

        public void setTestnavClientIdentifier(String testnavClientIdentifier) {
            this.testnavClientIdentifier = testnavClientIdentifier;
        }

        public String getTestnavClientSecret() {
            return testnavClientSecret;
        }

        public void setTestnavClientSecret(String testnavClientSecret) {
            this.testnavClientSecret = testnavClientSecret;
        }

        public String getFormName() {
            return formName;
        }

        public void setFormName(String formName) {
            this.formName = formName;
        }

        public String getTestCaseCoverage() {
            return testCaseCoverage;
        }

        public void setTestCaseCoverage(String testCaseCoverage) {
            this.testCaseCoverage = testCaseCoverage;
        }
    }

    public static class ScenarioDetail {
        @SerializedName("testNav")
        private String testNavScenarioClassName;
        @SerializedName("testNavScenarioNames")
        private List<String> testNavScenarioNames;
        @SerializedName("epen")
        private String epenScenarioClassName;
        @SerializedName("epenScenarioNames")
        private List<String> epenScenarioNames;
        @SerializedName("maxCount")
        private int totalNumberOfScenarios;
        @SerializedName("uuids")
        private Map<String, String> uuids;

        public String getTestNavScenarioClassName() {
            return testNavScenarioClassName;
        }

        public void setTestNavScenarioClassName(String testNavScenarioClassName) {
            this.testNavScenarioClassName = testNavScenarioClassName;
        }

        public List<String> getTestNavScenarioNames() {
            return testNavScenarioNames;
        }

        public void setTestNavScenarioNames(List<String> testNavScenarioNames) {
            this.testNavScenarioNames = testNavScenarioNames;
        }

        public String getEpenScenarioClassName() {
            return epenScenarioClassName;
        }

        public void setEpenScenarioClassName(String epenScenarioClassName) {
            this.epenScenarioClassName = epenScenarioClassName;
        }

        public List<String> getEpenScenarioNames() {
            return epenScenarioNames;
        }

        public void setEpenScenarioNames(List<String> epenScenarioNames) {
            this.epenScenarioNames = epenScenarioNames;
        }

        public int getTotalNumberOfScenarios() {
            return totalNumberOfScenarios;
        }

        public void setTotalNumberOfScenarios(int totalNumberOfScenarios) {
            this.totalNumberOfScenarios = totalNumberOfScenarios;
        }

        public Map<String, String> getUuids() {
            return uuids;
        }

        public void setUuids(Map<String, String> uuids) {
            this.uuids = uuids;
        }
    }


    public static class BatteryTest {
        private final String batteryTestCode;
        private final List<TestDetail> units;
        private List<SessionInfo> testSessions;
        private int remainingStudents;
        private String testCaseCoverage;
        private Set<String> errorFormCodes;

        BatteryTest(String batteryTestCode, List<TestDetail> units) {
            this.batteryTestCode = batteryTestCode;
            this.units = units;
        }

        public String getBatteryTestCode() {
            return batteryTestCode;
        }

        public List<TestDetail> getUnits() {
            return units;
        }

        public List<SessionInfo> getTestSessions() {
            return testSessions;
        }

        public void setTestSessions(List<SessionInfo> testSessions) {
            this.testSessions = testSessions;
        }

        public int getRemainingStudents() {
            return remainingStudents;
        }

        public void setRemainingStudents(int remainingStudents) {
            this.remainingStudents = remainingStudents;
        }

        public String getTestCaseCoverage() {
            return testCaseCoverage;
        }

        public void setTestCaseCoverage(String testCaseCoverage) {
            this.testCaseCoverage = testCaseCoverage;
        }

        public Set<String> getErrorFormCodes() {
            return errorFormCodes;
        }

        public void setErrorFormCodes(Set<String> errorFormCodes) {
            this.errorFormCodes = errorFormCodes;
        }
    }

    public static class TimestampDeserializer implements JsonDeserializer<Timestamp> {
        @Override
        public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            long time = Long.parseLong(json.getAsString());
            return new Timestamp(time);
        }
    }
}