package util.systemvalidation;

import com.google.inject.assistedinject.Assisted;
import com.pearson.itautomation.bevalidation.exception.BackendDatabaseException;
import com.pearson.itautomation.testmaps.models.enums.ItemStatus;
import com.typesafe.config.Config;
import com.zaxxer.hikari.pool.HikariPool;
import models.systemvalidation.DbConnection;
import play.Logger;
import util.ConnectionPoolManager;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class BackendData {
    private final ConnectionPoolManager connectionPoolManager;
    private final DataSource dataSource;
    private final String wfUser;
    private final String wfPassword;

    public interface Factory {
        BackendData create(DbConnection dbConnection);
    }

    @Inject
    public BackendData(ConnectionPoolManager connectionPoolManager, Config configuration,
                       @Assisted DbConnection dbConnection) {
        this.connectionPoolManager = connectionPoolManager;
        this.dataSource = connectionPoolManager.getMysqlConnectionPool(dbConnection.getHost(), dbConnection.getDbName(),
                dbConnection.getDbUser(), dbConnection.getDbPass(), dbConnection.getPort());
        this.wfUser = dbConnection.getDbUser();
        this.wfPassword = dbConnection.getDbPass();
    }

    /**
     * Get information about the workflow DBs associated with this master DB.
     *
     * @return A set of workflow DB details.
     */
    public Set<BackendWorkflowDetails> getWorkflows() throws BackendDatabaseException {
        Set<BackendWorkflowDetails> workflows = new HashSet<>();
        String query = "SELECT h.fqdn host, hp.port, w.name dbName "
                + "FROM ops.workflow w "
                + "JOIN ops.host_port hp ON hp.host_port_id = w.host_port_id "
                + "JOIN ops.host h ON h.host_id = hp.host_id;";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement stmt = connection.prepareStatement(query);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                DataSource dataSource = connectionPoolManager.getMysqlConnectionPool(rs.getString("host"),
                        rs.getString("dbName"), wfUser, wfPassword, rs.getInt("port"));
                workflows.add(new BackendWorkflowDetails(rs.getString("host"), rs.getString("dbName"), dataSource));
            }
        } catch (final SQLException | HikariPool.PoolInitializationException e) {
            Logger.error("event=\"An error occurred while establishing a connection to the database or while executing a query." +
                    " Please verify the Back End database connection details.\", query=\"{}\"", query, e);
            throw new BackendDatabaseException.Builder("An error occurred while establishing a connection to the database " +
                    "or while executing a query. Please verify the Back End database connection details.").build();
        }

        return workflows;
    }

    /**
     * For the given scope tree path, get the item statuses that are eligible for ePEN scoring.
     *
     * @param scopeTreePath The scope tree path.
     * @return A set of item statuses eligible for scoring in ePEN.
     */
    public Set<ItemStatus> getItemStatusesEligibleForEpenScoring(String scopeTreePath) {
        Set<ItemStatus> result = new HashSet<>();

        String query = "SELECT CASE " +
                "    WHEN x.arg = '-i' " +
                "    THEN " +
                "    x.itemStatuses " +
                "    WHEN x.arg = '-q' " +
                "    THEN " +
                "           (SELECT value " +
                "                    FROM config.proc_arg pa " +
                "                    WHERE pa.proc_id = x.itemStatuses AND pa.arg = '-i') " +
                "    END as itemStatuses " +
                "    FROM (SELECT pa.arg, " +
                "            CASE " +
                "           WHEN pa.arg = '-i' " +
                "            THEN " +
                "            pa.value " +
                "            WHEN pa.arg = '-q' " +
                "            THEN " +
                "            REPLACE(pa.value, 'legacy_proc_id=', '') " +
                "            END " +
                "            AS itemStatuses " +
                "            FROM config.path p " +
                "            JOIN config.path_state ps ON p.path_id = ps.path_id " +
                "            JOIN config.state st ON st.state_id = ps.state_id " +
                "            JOIN config.proc_arg pa ON st.proc_id = pa.proc_id " +
                "            JOIN config.scope_producer sp ON sp.path_id = p.path_id " +
                "            JOIN config.producer pr ON pr.producer_id = sp.producer_id " +
                "            JOIN core.scope s ON s.scope_id = sp.scope_id " +
                "            WHERE     p.path_id = sp.path_id " +
                "           AND pr.name = 'QTI' " +
                "           AND st.name LIKE 'PS Item Scoring Elig%ble Flag Setter - %' " +
                "           AND pa.arg IN ('-i', '-q') " +
                "           AND lcase(s.code) = " +
                "            lcase(SUBSTRING_INDEX(?, '/', -1)) " +
                "    ORDER BY ps.seq) x; " ;

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, scopeTreePath);
            try (ResultSet rs = statement.executeQuery()) {
                while (rs.next()) {
                    String[] itemStatuses = rs.getString("itemStatuses").split(",");
                    for (String itemStatus : itemStatuses) {
                        result.add(ItemStatus.parseItemstatusStr(itemStatus));
                    }
                }
            }
        } catch (SQLException e) {
            Logger.error(
                    "event=\"SQL exception trying to fetch Backend path states.\", scopeTreePath=\"{}\" query=\"{}\"",
                    scopeTreePath, query, e);
            throw new RuntimeException(e);
        }
        return result;
    }

    /**
     * A class to hold information about a Backend workflow DB.
     */
    public static class BackendWorkflowDetails {
        private final String host;
        private final String database;
        private final DataSource dataSource;

        /**
         * Create an instance of BackendWorkflowDetails.
         *
         * @param host       The host.
         * @param database   The database.
         * @param dataSource The DataSource.
         */
        BackendWorkflowDetails(String host, String database, DataSource dataSource) {
            this.host = host;
            this.database = database;
            this.dataSource = dataSource;
        }

        public String getHost() {
            return host;
        }

        public String getDatabase() {
            return database;
        }

        public DataSource getDataSource() {
            return dataSource;
        }
    }
}
