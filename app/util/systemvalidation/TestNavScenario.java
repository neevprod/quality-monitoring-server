package util.systemvalidation;

/**
 * This class contains the information about a Testnav Scenario. It contains the following information:
 * <ul>
 * <li>scopeTreePath</li>
 * <li>testCode</li>
 * <li>formCode</li>
 * <li>testmapId</li>
 * <li>testmapName</li>
 * <li>testmapVersion</li>
 * <li>scenarioClass</li>
 * <li>scenarioType</li>
 * <li>scenarioName</li>
 * <li>The scenario as JSON string</li>
 * </ul>
 * 
 * @author Nand Joshi
 *
 */
public class TestNavScenario {
    private final String scopeTreePath;
    private final String testCode;
    private final String formCode;
    private final String testmapId;
    private final String testmapName;
    private final String testmapVersion;
    private final String testMapPublishFormat;
    private final String scenarioClass;
    private final String scenarioType;
    private final String scenarioName;
    private final String scenario;

    /**
     * Creates an instance of {@link TestNavScenario} class.
     * 
     * @param scopeTreePath
     *            The scope tree path
     * @param testCode
     *            The test code
     * @param formCode
     *            The form code
     * @param testmapId
     *            The testmap id
     * @param testmapName
     *            The testmap name
     * @param testmapVersion
     *            The testmap version
     * @param testMapPublishFormat
     *            The testmap publish format
     * @param scenarioClass
     *            The scenario class
     * @param scenarioName
     *            The scenario name
     * @param scenario
     *            The scenario as JSON string
     */
    public TestNavScenario(final String scopeTreePath, final String testCode, final String formCode,
            final String testmapId, final String testmapName, final String testmapVersion,
            final String testMapPublishFormat, final String scenarioClass,
            final String scenarioName, final String scenario) {
        this.scopeTreePath = scopeTreePath;
        this.testCode = testCode;
        this.formCode = formCode;
        this.testmapId = testmapId;
        this.testmapName = testmapName;
        this.testmapVersion = testmapVersion;
        this.testMapPublishFormat = testMapPublishFormat;
        this.scenarioClass = scenarioClass;
        this.scenarioType = "TN8";
        this.scenarioName = scenarioName;
        this.scenario = scenario;
    }

    /**
     * Get the Scope Tree Path for this scenario.
     * 
     * @return the scopeTreePath
     */
    public final String getScopeTreePath() {
        return scopeTreePath;
    }

    /**
     * Get the test code for this scenario.
     * 
     * @return the testCode
     */
    public final String getTestCode() {
        return testCode;
    }

    /**
     * Get the form code for this scenario.
     * 
     * @return the formCode
     */
    public final String getFormCode() {
        return formCode;
    }

    /**
     * Get the testmap id for this scenario.
     * 
     * @return the testmapId
     */
    public final String getTestmapId() {
        return testmapId;
    }

    /**
     * Get the testmap name for this scenario.
     * 
     * @return the testmapName
     */
    public final String getTestmapName() {
        return testmapName;
    }

    /**
     * Get the testmap version for this scenario.
     * 
     * @return the testmapVersion
     */
    public final String getTestmapVersion() {
        return testmapVersion;
    }

    /**
     * Get the testmap publish format for this scenario.
     * 
     * @return the testMapPublishFormat
     */
    public final String getTestMapPublishFormat() {
        return testMapPublishFormat;
    }

    /**
     * Get the scenario class for this scenario.
     * 
     * @return the scenarioClassId
     */
    public final String getScenarioClass() {
        return scenarioClass;
    }

    /**
     * Get the scenario type for this scenario.
     * 
     * @return the scenarioType
     */
    public final String getScenarioType() {
        return scenarioType;
    }

    /**
     * Get the scenario name for this scenario.
     * 
     * @return the scenarioName
     */
    public final String getScenarioName() {
        return scenarioName;
    }

    /**
     * Get the JSON scenario string for this scenario.
     * 
     * @return the scenario
     */
    public final String getScenario() {
        return scenario;
    }

}
