package util.systemvalidation;

import com.pearson.itautomation.exceptions.ParseTestmapException;
import com.pearson.itautomation.panext.datafetcher.DataFetcherException;
import com.pearson.itautomation.stats.service.MongoInfo;
import com.pearson.itautomation.stats.service.StatsInput;
import com.pearson.itautomation.stats.service.StatsServiceCall;
import com.pearson.itautomation.stats.service.StudentInfo;
import com.pearson.itautomation.testmaps.models.enums.ItemStatus;
import jobs.systemvalidation.StatsValidationActor.StatsInputMessage;
import models.systemvalidation.Scenario;
import models.systemvalidation.StudentScenario;
import models.systemvalidation.TestSession;

import javax.sql.DataSource;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;
import javax.xml.xpath.XPathExpressionException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Nand Joshi
 */
public class StatsFactory {

    /**
     * Creates an instance of StatsInput.
     *
     * @param inputMsg A StatsInputMessage containing all the necessary information.
     * @return The new instance of {@link StatsInput}
     */
    public StatsInput getStatsInput(final StatsInputMessage inputMsg) {

        final Set<DataSource> backendWfDataSources = inputMsg.getBackendWfDataSources();
        final MongoInfo mongoInfo = inputMsg.getMongoInfo();
        final List<StudentInfo> students = getStudentsInfo(inputMsg.getStudentScenario());
        final boolean hasEpenScenario = inputMsg.getStudentScenario().getEpenScenario() != null;
        final Set<ItemStatus> eligibleItemStatuses = inputMsg.getEligibleItemStatuses();

        return new StatsInput(backendWfDataSources, mongoInfo, students, hasEpenScenario, eligibleItemStatuses, false);
    }

    /**
     * Creates an instance of StatsInput for battery tests.
     *
     * @param inputMsg A StatsInputMessage containing all the necessary information.
     * @return The new instance of {@link StatsInput}
     */
    public StatsInput getBatteryStatsInput(final StatsInputMessage inputMsg) {

        final Set<DataSource> backendWfDataSources = inputMsg.getBackendWfDataSources();
        final MongoInfo mongoInfo = inputMsg.getMongoInfo();

        final List<StudentInfo> students = getBatteryStudentsInfo(inputMsg.getBatteryUnitStudentScenarios());

        final boolean hasEpenScenario = inputMsg.getStudentScenario().getEpenScenario() != null;
        final Set<ItemStatus> eligibleItemStatuses = inputMsg.getEligibleItemStatuses();

        return new StatsInput(backendWfDataSources, mongoInfo, students, hasEpenScenario, eligibleItemStatuses, true);
    }

    /**
     * @param statsInput The instance of {@link StatsInput}
     * @return The new instance of {@link StatsServiceCall}
     * @throws XPathExpressionException
     * @throws SQLException
     * @throws SOAPException
     * @throws JAXBException
     * @throws ParseTestmapException
     */
    public StatsServiceCall getStatsServiceCall(final StatsInput statsInput) throws XPathExpressionException,
            SQLException, SOAPException, JAXBException, ParseTestmapException, DataFetcherException {
        return new StatsServiceCall(statsInput);

    }

    /**
     * @param studentScenario The instance of {@link StudentScenario}
     * @return The List of {@link StudentInfo}
     */
    private List<StudentInfo> getStudentsInfo(final StudentScenario studentScenario) {
        final List<StudentInfo> students = new ArrayList<>();
        String startTime = "";
        final TestSession testSession = studentScenario.getTestSession();
        if (testSession != null && testSession.getSessionStartDate() != null) {
            startTime = testSession.getSessionStartDate().toString();
        }
        Scenario scenario = studentScenario.getTestnavScenario();
        if (NavigationScenarioCreator.NAVIGATION_SCENARIOS.contains(scenario.getScenarioName())) {
            String scenarioJson = studentScenario.getTestnavScenario().getScenario();
            String updatedScenarioJson = NavigationScenarioCreator.removeLowerVersionResponseAndOutcome(scenarioJson);
            scenario.setScenario(updatedScenarioJson);
            studentScenario.setTestnavScenario(scenario);
        }
        StudentInfo stdInfo = new StudentInfo(studentScenario.getUuid(), null,
                startTime, null, studentScenario.getFirstName(),
                studentScenario.getLastName(), studentScenario.getTestnavScenario().getTestCode(),
                studentScenario.getTestnavScenario().getFormCode(), studentScenario.getTestnavScenario().getScenario());
        students.add(stdInfo);
        return students;
    }

    /**
     * @param batteryUnitStudentScenarios The instance of {@link StudentScenario}
     * @return The List of {@link StudentInfo}
     */
    private List<StudentInfo> getBatteryStudentsInfo(final List<StudentScenario> batteryUnitStudentScenarios) {
        final List<StudentInfo> students = new ArrayList<>();
        String batteryUuid;
        TestSession testSession;

        for (StudentScenario studentScenario : batteryUnitStudentScenarios) {
            batteryUuid = studentScenario.getBatteryStudentScenario().getBatteryUuid();
            testSession = studentScenario.getTestSession();

            StudentInfo stdInfo = new StudentInfo(studentScenario.getUuid(), batteryUuid,
                    testSession.getSessionStartDate().toString(), null, studentScenario.getFirstName(),
                    studentScenario.getLastName(), studentScenario.getTestnavScenario().getTestCode(),
                    studentScenario.getTestnavScenario().getFormCode(),
                    studentScenario.getTestnavScenario().getScenario());
            students.add(stdInfo);
        }
        return students;
    }
}