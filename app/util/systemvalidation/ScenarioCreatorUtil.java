package util.systemvalidation;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pearson.itautomation.panext.studentsession.StudentTestSession;
import com.pearson.itautomation.testmaps.models.Item;
import com.pearson.itautomation.testmaps.models.SubItem;
import com.pearson.itautomation.testmaps.models.Testmap;
import com.pearson.itautomation.testmaps.models.TestmapForm;
import com.pearson.itautomation.testmaps.models.enums.ItemStatus;
import com.pearson.itautomation.testscenariocreator.exceptions.NotFoundException;
import com.pearson.itautomation.testscenariocreator.inputs.ItemResponse;
import com.pearson.itautomation.testscenariocreator.inputs.ItemResponsePool;
import com.pearson.itautomation.testscenariocreator.inputs.T3TestMapItem;
import com.pearson.itautomation.testscenariocreator.output.TestScenario;
import com.pearson.itautomation.testscenariocreator.testscenariosuites.TestScenarioSuite;
import com.pearson.itautomation.testscenariocreator.testscenariosuites.TestScenarioSuiteFactory;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapsResult;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapsResult.TestMap;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import org.apache.commons.lang3.Validate;
import play.Logger;
import util.ConnectionPoolManager;
import util.systemvalidation.oe_response.Factory;
import util.systemvalidation.oe_response.OpenEndedInteractionResponse;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * Creates the list of {@link TestNavScenario} for the given List of studentTestSession.<br>
 * It also checks whether the epenScorable items exists in the given form or not.
 *
 * @author Nand Joshi
 */
public class ScenarioCreatorUtil {
    private final String token;
    private final String secret;
    private final String scopeTreePath;
    private final String adminCode;
    private final String accountCode;

    /**
     * Stores TestForm to epenScorable items.
     */
    private final Map<FormDetail, List<T3TestMapItem>> formToEpenScorableItems;
    private final Map<FormDetail, List<Item>> formToHumanScorableItems;

    /**
     * Stores TestForm to aiScorable items.
     */
    private final Map<FormDetail, List<Item>> formToKtScorableItems;

    /**
     * To store TestForm to T3TestMap items.
     */
    private final Map<FormDetail, List<T3TestMapItem>> formToT3TestMapItems;
    private final List<PreviewerDetail> previewerItemDetails;
    private final Set<String> sampleItems;
    private final Set<String> surveyItems;

    private final String previewerUrl;
    private final int tenant;
    private final String includesAdaptiveIndicator;
    private final DivJobSetupUtil divJobSetupUtil;

    public ScenarioCreatorUtil(String scopeTreePath, String adminCode, String accountCode,
                               final StudentTestSession studentTestSession, final String token, final String secret,
                               final String previewerUrl, final int tenant, final String includesAdaptiveIndicator,
                               ConnectionPoolManager connectionPoolManager, final List<Testmap> testMaps) {
        // Validating arguments
        Validate.notEmpty(scopeTreePath, "Scope Tree Path can not be empty.");
        Validate.notEmpty(adminCode, "Admin Code can not be empty.");
        Validate.notEmpty(accountCode, "Account Code can not be empty.");
        Validate.notNull(studentTestSession, "Student TestSession can not be null.");

        this.scopeTreePath = scopeTreePath;
        this.adminCode = adminCode;
        this.accountCode = accountCode;

        formToEpenScorableItems = new HashMap<>();
        formToT3TestMapItems = new HashMap<>();
        formToKtScorableItems = new HashMap<>();
        previewerItemDetails = new ArrayList<>();
        formToHumanScorableItems = new HashMap<>();
        sampleItems = new HashSet<>();
        surveyItems = new HashSet<>();
        this.token = token;
        this.secret = secret;

        this.previewerUrl = previewerUrl;
        this.tenant = tenant;
        this.includesAdaptiveIndicator = includesAdaptiveIndicator;
        divJobSetupUtil = new DivJobSetupUtil(connectionPoolManager);
        execute(studentTestSession, testMaps);
    }

    /**
     * Creates an instance of {@link ItemResponsePool}.
     *
     * @param tenant      The tenant Id
     * @param testNav8api The {@link TestNav8API}
     * @param testMapId   The testmap Id
     * @return The {@link ItemResponsePool}
     */
    public static ItemResponsePool getItemResponsePool(int tenant, TestNav8API testNav8api, int testMapId) {
        List<ItemResponse> irs = getItemResponses(tenant, testNav8api, testMapId);
        return new ItemResponsePool(irs);
    }

    private static List<ItemResponse> getItemResponses(int tenant, TestNav8API testNav8api, int testMapId) {
        com.pearson.itautomation.qtiscv.ItemResponsePool itemResponsePool = new com.pearson.itautomation.qtiscv.ItemResponsePool(
                testNav8api);

        return itemResponsePool.getTestmapItemResponsePool(tenant, testMapId).stream()
                .map(itemResponse -> new ItemResponse(itemResponse.getUIN(), itemResponse.getPoints(),
                        itemResponse.isAttempted(), itemResponse.getIdentifierToInteraction(), itemResponse.getMods()))
                .collect(Collectors.toList());
    }

    /**
     * Starts the scenarioCreation for the given List of studentTestSession.
     *
     * @param studentTestSession The instance of {@link StudentTestSession}
     */
    private void execute(final StudentTestSession studentTestSession, final List<Testmap> testMaps) {
        String testNumber = studentTestSession.getTestCode();
        String formCode = studentTestSession.getFormCode();

        List<PreviewerDetail> previewerDetails = processTestForm(previewerUrl, tenant, includesAdaptiveIndicator,
                testNumber, formCode);

        if (testMaps.isEmpty()) {
            Logger.error("event=\"Failed to download test map from T3.\", admin=\"{}\", account=\"{}\", "
                    + "testCode=\"{}\", formCode=\"{}\"", adminCode, accountCode, testNumber, formCode);
        } else {
            List<PreviewerDetail> previewerDetailsWithItem = processTestMapItems(testMaps, previewerDetails);
            previewerItemDetails.addAll(previewerDetailsWithItem);
        }
    }

    private List<PreviewerDetail> processTestForm(final String previewerUrl, final int tenant,
                                                  final String includesAdaptiveIndicator, final String testcode, final String formCode) {

        List<PreviewerDetail> previewerDetails = new ArrayList<>();
        TestNav8API testNav8api = TestNav8API.connectToPreviewer("http://" + previewerUrl, token.toCharArray(), secret.toCharArray());
        GetTestMapsResult testMaps = testNav8api.getTestMaps(tenant);
        for (TestMap testMap : testMaps.getTestMaps()) {
            if (testMap.getIdentifier().equals(testcode) && testMap.getFormId().equals(formCode)) {
                previewerDetails.add(new PreviewerDetail(tenant, previewerUrl, testMap.getFormId()));
            }
        }
        return previewerDetails;
    }

    /**
     * Loads the {@link #formToEpenScorableItems} and {@link #formToT3TestMapItems}.
     *
     * @param testMaps         The List of testmaps
     * @param previewerDetails The List of {@link PreviewerDetail}
     * @return The List of {@link PreviewerDetail} with Item info
     */
    private List<PreviewerDetail> processTestMapItems(final List<Testmap> testMaps,
                                                      final List<PreviewerDetail> previewerDetails) {
        final List<PreviewerDetail> previewerDetailsWithItem = new ArrayList<>();
        for (Testmap testMapXml : testMaps) {
            String testMapId = testMapXml.getId();
            String testMapName = testMapXml.getName();
            String testMapVersion = testMapXml.getVersion();
            String testMapPublishFormat = testMapXml.getPublishFormat();

            for (TestmapForm testMapForm : testMapXml.getForms()) {
                List<T3TestMapItem> testMapItems = new ArrayList<>();

                // To store epenScorable items
                List<T3TestMapItem> epenItems = new ArrayList<>();
                List<Item> humanScorableItems = new ArrayList<>();
                //To store AI scorable Items
                List<Item> ktItems = new ArrayList<>();


                Optional<PreviewerDetail> previewerDetailObj = previewerDetails.stream()
                        .filter(pd -> pd.getFormId().equals(testMapForm.getFormId())).findFirst();
                if (!previewerDetailObj.isPresent()) {
                    Logger.error("Failed to read test from from previewer");
                    continue;
                }
                PreviewerDetail previewerDetail = previewerDetailObj.get();
                List<String> t3ItemIdentifiers = new ArrayList<>();
                for (Item item : testMapForm.getItems()) {

                    if (item.getUin() != null && !item.getUin().isEmpty()) {
                        T3TestMapItem t3TestMapItem = new T3TestMapItem(item.getUin(), item.getSequenceNumber(),
                                item.isWc(), Double.parseDouble(item.getMaxPoints()), item.getItemStatus());
                        if (ItemStatus.NA == item.getItemStatus() || ItemStatus.SAMPLE == item.getItemStatus()) {
                            sampleItems.add(item.getUin());
                        }

                        if (ItemStatus.SURVEY == item.getItemStatus()) {
                            surveyItems.add(item.getUin());
                        }

                        if (divJobSetupUtil.containsEpenItem(item) && item.getSpoiledItemType() == null) {
                            epenItems.add(t3TestMapItem);
                            humanScorableItems.add(item);
                        }
                        if (divJobSetupUtil.isKtItem(item) && item.getSpoiledItemType() == null) {
                            ktItems.add(item);
                        }
                        testMapItems.add(t3TestMapItem);
                        t3ItemIdentifiers.add(t3TestMapItem.getUin());
                    }
                }
                previewerDetail.addItemIdentifiers(t3ItemIdentifiers);
                previewerDetailsWithItem.add(previewerDetail);
                FormDetail formDetail = new FormDetail(testMapForm.getTestNumber(), testMapForm.getFormId(),
                        testMapPublishFormat, testMapId, testMapName, testMapVersion, testMapPublishFormat);
                formToEpenScorableItems.put(formDetail, epenItems);
                formToT3TestMapItems.put(formDetail, testMapItems);
                formToHumanScorableItems.put(formDetail, humanScorableItems);
                formToKtScorableItems.put(formDetail, ktItems);

            }

        }
        return previewerDetailsWithItem;
    }

    /**
     * Creates the JSON Scenario String for the given testCode and formCode.
     *
     * @param scenario The {@link TestScenario}
     * @param testCode The testCode
     * @param formCode The formCode
     * @return The Scenario JSON String
     */
    private String getJsonScenario(final TestScenario scenario, final String testCode, final String formCode,
                                   String scenarioClass) {
        List<Item> humanScorableItems = formToHumanScorableItems
                .get(new FormDetail(testCode, formCode, "E", null, null, null, null));
        JsonObject jsonScenario = new JsonObject();
        jsonScenario.addProperty("scenarioName", scenario.getDescription());
        jsonScenario.addProperty("scenarioType", "TN8");
        jsonScenario.addProperty("testNumber", testCode);
        jsonScenario.addProperty("formNumber", formCode);
        jsonScenario.addProperty("totalPoints", scenario.getTotalPoints());
        jsonScenario.add("itemResponses",
                getJsonItemResponses(scenario, humanScorableItems, testCode, formCode, scenarioClass));

        return jsonScenario.toString();
    }

    /**
     * Gets the JsonArray of itemResponses.
     *
     * @param scenario The {@link TestScenario}
     * @return The JsonArray of itemResponses
     */
    private JsonArray getJsonItemResponses(final TestScenario scenario, final List<Item> humanScorableItems,
                                           final String formCode, final String testCode, String scenarioClass) {
        List<ItemResponse> scenarioItems = scenario.getItemResponses();
        JsonArray itemResponses = new JsonArray();

        for (ItemResponse scenarioItem : scenarioItems) {
            // Creating outcomes
            JsonArray outComes = new JsonArray();
            JsonObject outcome = new JsonObject();
            outcome.addProperty("SCORE", scenarioItem.getPoints());
            outComes.add(outcome);

            JsonArray updatedMods = processModsString(scenarioItem.getMods());
            if (!scenarioClass.equals("NONATTEMPTED")) {
                for (Item item : humanScorableItems) {
                    updateHumanScorableItem(formCode, testCode, scenarioItem, updatedMods, item);
                }
            }
            // Creating itemResponse
            JsonObject itemResponse = new JsonObject();
            itemResponse.addProperty("uin", scenarioItem.getUin());
            itemResponse.add("outcomes", outComes);
            itemResponse.add("identifiersToInteractions",
                    processIdentifierToInteraction(scenarioItem.getIdToInteraction()));
            itemResponse.add("mods", updatedMods);
            itemResponse.addProperty("paperResponseStr", "");
            itemResponses.add(itemResponse);
        }
        return itemResponses;
    }

    private void updateHumanScorableItem(String formCode, String testCode, ItemResponse scenarioItem, JsonArray updatedMods, Item item) {
        String finalResponse = "TestCode: " + testCode + ", " + "FormCode: " + formCode + ", " + "ItemUin: "
                + item.getUin();
        if (null != updatedMods && item.getUin().equals(scenarioItem.getUin())) {
            List<SubItem> subItems = item.getItemparts();
            String idToInteraction = scenarioItem.getIdToInteraction();
            if (null != subItems) {
                for (SubItem subItem : subItems) {
                    updateSubItemMods(updatedMods, finalResponse, idToInteraction, subItem);
                }
            } else {
                for (int i = 0; i < updatedMods.size(); i++) {
                    String scenarioResponseIdentifier = updatedMods.get(i).getAsJsonObject().get("did").getAsString();
                    JsonObject modObject = updatedMods.get(i).getAsJsonObject();
                    updateResponse(modObject, finalResponse, idToInteraction, scenarioResponseIdentifier);

                }
            }
        }
    }

    private void updateSubItemMods(JsonArray updatedMods, String finalResponse, String idToInteraction, SubItem subItem) {
        for (int i = 0; i < updatedMods.size(); i++) {
            String scenarioResponseIdentifier = updatedMods.get(i).getAsJsonObject().get("did").getAsString();
            if (null != subItem.getResponseIdentifier()
                    && subItem.getResponseIdentifier().equals(scenarioResponseIdentifier)
                    && subItem.getScoringDestination().equals("human")) {

                JsonObject modObject = updatedMods.get(i).getAsJsonObject();
                updateResponse(modObject, finalResponse, idToInteraction, scenarioResponseIdentifier);
                updatedMods.set(i, modObject);
            }
        }
    }

    private void updateResponse(JsonObject modObject, String finalResponse, String idToInteraction, String scenarioResponseIdentifier) {
        String interactionType = getIdentifierInteraction(idToInteraction, scenarioResponseIdentifier);

        OpenEndedInteractionResponse response = Factory.getHumanScorableResponseInstance(interactionType, modObject, finalResponse, scenarioResponseIdentifier);
        if (response != null) {
            response.update();
        }

    }


    /**
     * Returns the interaction type for the given identifier
     *
     * @param identifiersToInteractions A delimited list of identifier and interaction type pairs
     * @param identifier                The identifier to retrieve the interaction type for
     * @return The interaction type for the given identifier
     */
    private String getIdentifierInteraction(String identifiersToInteractions, String identifier) {
        String result = "";
        String[] identifierToInteractionsArray = identifiersToInteractions.split(";");
        for (String identifierToInteraction : identifierToInteractionsArray) {
            String[] identifierInteractionArray = identifierToInteraction.split(":");
            String id = identifierInteractionArray[0];
            String interaction = identifierInteractionArray[1];
            if (id.equalsIgnoreCase(identifier)) {
                result = interaction;
            }
        }
        return result;
    }

    /**
     * Splits identifiersToInteractions string by semi-colon for multiple identifiersToInteraction and splits by
     * colon(:) to read identifier and interaction.
     *
     * @param identifierToInteraction The identifierToInteraction
     * @return The JsonArray of identifiersToInteractions
     */
    private JsonArray processIdentifierToInteraction(final String identifierToInteraction) {
        // creating identifiersToInteractions
        // handling multiple identifierToInteraction separated by semi-colon
        final JsonArray identifiersToInteractions = new JsonArray();
        for (String itiString : identifierToInteraction.split(";")) {
            // Extracting Identifier and InteractionType separated by colon
            JsonObject idToInteraction = new JsonObject();
            String[] identifierAndInteraction = itiString.split(":");
            if (identifierAndInteraction.length > 1) {
                idToInteraction.addProperty("identifier", identifierAndInteraction[0]);
                idToInteraction.addProperty("interactionType", identifierAndInteraction[1]);
                identifiersToInteractions.add(idToInteraction);
            }
        }
        return identifiersToInteractions;
    }

    /**
     * Converts modsString to JsonArray.
     *
     * @param modsString The modsString
     * @return The JsonArray of mods
     */
    private JsonArray processModsString(final String modsString) {
        JsonParser parser = new JsonParser();
        JsonElement modsElement = parser.parse(modsString);
        if (modsElement.isJsonArray()) {
            return modsElement.getAsJsonArray();
        } else {
            Logger.error("mods is not in Array format.");
            return new JsonArray();
        }

    }

    /**
     * Get the list of {@link TestNavScenario} for the given <code>testCode></code> <code>formCode></code> and
     * <code>scenarioClass></code>
     *
     * @param testCode                The Test Code
     * @param formCode                The Form Code
     * @param scenarioClass           The scenario class name (e.g. ALLCORRECT, MINPOINTS, MAXPOINTS, NONATTEMPTED, etc)
     * @param epenPercentCorrectItems The list of epenScorable itemIds
     * @return The unmodifiable List of {@link TestNavScenario}
     */
    public List<TestNavScenario> getTestNavScenario(final String testCode, final String formCode,
                                                    final String scenarioClass, final List<String> epenPercentCorrectItems, final ItemResponsePool itemResponsePool) {
        Validate.notEmpty(testCode, "Test Code can not be empty.");
        Validate.notEmpty(formCode, "Form Code can not be empty.");
        Validate.notEmpty(scenarioClass, "Scenario Class can not be empty.");

        List<TestNavScenario> scenarios = new ArrayList<>();
        FormDetail key = new FormDetail(testCode, formCode, "E", null, null, null, null);
        try {
            Optional<Entry<FormDetail, List<T3TestMapItem>>> formDetailWithIr = formToT3TestMapItems.entrySet().stream()
                    .filter(entry -> entry.getKey().equals(key)).findFirst();

            if (formDetailWithIr.isPresent()) {
                Entry<FormDetail, List<T3TestMapItem>> entry = formDetailWithIr.get();
                FormDetail formDetail = entry.getKey();
                List<T3TestMapItem> items = entry.getValue();

                List<String> epenAndSampleItemUins = new ArrayList<>();

                if (scenarioClass.toUpperCase().startsWith("PERCENTCORRECT")) {
                    epenAndSampleItemUins.addAll(sampleItems);
                    List<T3TestMapItem> epenItems = formToEpenScorableItems.get(formDetail);
                    epenAndSampleItemUins
                            .addAll(epenItems.stream().map(T3TestMapItem::getUin).collect(Collectors.toList()));
                }

                TestScenarioSuite scenarioSuite = TestScenarioSuiteFactory.getTestScenarioSuite(scenarioClass, items,
                        itemResponsePool, epenAndSampleItemUins, epenPercentCorrectItems);
                List<TestScenario> testScenarios = scenarioSuite.generate();

                for (TestScenario testScenario : testScenarios) {

                    String jsonScenario = getJsonScenario(testScenario, testCode, formCode, scenarioClass);
                    TestNavScenario scenario = new TestNavScenario(scopeTreePath, testCode, formCode,
                            formDetail.getTestMapId(), formDetail.getTestMapName(), formDetail.getTestMapVersion(),
                            formDetail.getTestMapPublishFormat(), scenarioClass, testScenario.getDescription(),
                            jsonScenario);

                    scenarios.add(scenario);
                }
            }
        } catch (NotFoundException e) {
            Logger.error("Failed to generate scenario for {}", scenarioClass, e);
        }
        return Collections.unmodifiableList(scenarios);
    }

    public List<T3TestMapItem> getEpenItems(final String testCode, final String formCode) {
        return formToEpenScorableItems.get(new FormDetail(testCode, formCode, "E", null, null, null, null));
    }

    public List<Item> getKtItems(final String testCode, final String formCode) {
        return formToKtScorableItems.get(new FormDetail(testCode, formCode, "E", null, null, null, null));
    }

    /**
     * @return the previewerItemDetails
     */
    public final List<PreviewerDetail> getPreviewerItemDetails() {
        return previewerItemDetails;
    }

    /**
     * @return the sampleItems
     */
    public final Set<String> getSampleItems() {
        return sampleItems;
    }

    public Set<String> getSurveyItems() {
        return surveyItems;
    }

    /**
     * Used to store the previewer related information such as:<br>
     * tenant id, previewer URL, formId and the List of item identifiers.
     */
    public static class PreviewerDetail {
        private final int tenant;
        private final String previewerUrl;
        private final String formId;
        private final List<String> itemIdentifiers;
        private final String includesAdaptiveIndicator;

        /**
         * @param tenant       The tenant id
         * @param previewerUrl The previewer url
         * @param formId       The form id
         */
        PreviewerDetail(int tenant, String previewerUrl, String formId) {
            this.tenant = tenant;
            this.previewerUrl = previewerUrl;
            this.formId = formId;
            itemIdentifiers = new ArrayList<>();
            includesAdaptiveIndicator = null;
        }

        public PreviewerDetail(int tenant, String previewerUrl, String formId, String includesAdaptiveIndicator) {
            this.tenant = tenant;
            this.previewerUrl = previewerUrl;
            this.includesAdaptiveIndicator = includesAdaptiveIndicator;
            this.formId = formId;
            itemIdentifiers = new ArrayList<>();
        }

        /**
         * @return the tenant
         */
        public final int getTenant() {
            return tenant;
        }

        /**
         * @return the previewerUrl
         */
        public final String getPreviewerUrl() {
            return previewerUrl;
        }

        /**
         * @return the formcode
         */
        public final String getFormId() {
            return formId;
        }

        final void addItemIdentifiers(final List<String> itemIdentifiers) {
            this.itemIdentifiers.addAll(itemIdentifiers);
        }

        /**
         * @return the itemIdentifiers
         */
        public final List<String> getItemIdentifiers() {
            return itemIdentifiers;
        }

        public String getIncludesAdaptiveIndicator() {
            return includesAdaptiveIndicator;
        }
    }

    /**
     * Used as key to identify ItemResponsePool for the combination of testCode, formCode within a testMap.
     *
     * @author Nand Joshi
     */
    class FormDetail {
        private final String testCode;
        private final String formCode;
        private final String publishFormat;
        private final String testMapId;
        private final String testMapName;
        private final String testMapVersion;
        private final String testMapPublishFormat;

        /**
         * @param testCode       The Test Code
         * @param formCode       The Form Code
         * @param publishFormat  The publish format
         * @param testMapId      The testMapId
         * @param testMapName    The testMapName
         * @param testMapVersion The testMapVersion
         */
        FormDetail(final String testCode, final String formCode, final String publishFormat,
                   final String testMapId, final String testMapName, final String testMapVersion,
                   final String testMapPublishFormat) {
            this.testCode = testCode;
            this.formCode = formCode;
            this.publishFormat = publishFormat;
            this.testMapId = testMapId;
            this.testMapName = testMapName;
            this.testMapVersion = testMapVersion;
            this.testMapPublishFormat = testMapPublishFormat;
        }

        /**
         * Get the Testmap Id for this Form.
         *
         * @return the testMapId
         */
        final String getTestMapId() {
            return testMapId;
        }

        /**
         * Get the Testmap Name for this Form.
         *
         * @return the testMapName
         */
        final String getTestMapName() {
            return testMapName;
        }

        /**
         * Get the Testmap Version for this Form.
         *
         * @return the testMapVersion
         */
        final String getTestMapVersion() {
            return testMapVersion;
        }

        /**
         * @return the testMapPublishFormat
         */
        final String getTestMapPublishFormat() {
            return testMapPublishFormat;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            FormDetail that = (FormDetail) o;
            return Objects.equals(testCode, that.testCode) && Objects.equals(formCode, that.formCode)
                    && Objects.equals(publishFormat, that.publishFormat);
        }

        @Override
        public int hashCode() {
            return Objects.hash(testCode, formCode, publishFormat);
        }
    }
}