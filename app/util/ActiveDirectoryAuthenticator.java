package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.apache.oodt.commons.exec.ExecHelper;

import models.HumanUser;
import models.User;

public class ActiveDirectoryAuthenticator {

    public static final String DOMAIN = "peroot.com";

    private static int MAX_SERVERS_TO_TRY_ON_EXCEPTION = 5;

    // fields used to cache the list of ldap servers
    private long lastCacheReload = -1;
    private static final long ONE_DAY = 1000 * 60 * 60 * 24;
    private List<String> serverCache;

    private static ActiveDirectoryAuthenticator instance;

    private ActiveDirectoryAuthenticator() {
    }

    public static ActiveDirectoryAuthenticator getInstance() {
        if (instance == null) {
            instance = new ActiveDirectoryAuthenticator();
        }
        return instance;
    }

    /**
     * 
     * @param username
     *            the user to authenticate for
     * @param password
     *            the password of the user to authenticate for
     * @return true if security credentials are correct, false if they do not exist
     */
    public boolean authenticate(String username, String password) {
        // getServerContext() throws an AuthenticationException if auth fails, we return null if the exception is thrown
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)
                || getServerContext(username, password) == null)
            return false;
        return true;
    }

    /**
     * 
     * @param username
     *            the user to authenticate to Active Directory and perform the search with
     * @param password
     *            the password for the user used to perform the Active Directory search
     * @param searchEmail
     *            the email address of the user to search for
     * @return the first user it finds that matches searchEmail
     */
    public HumanUser findUser(String username, String password, String searchEmail) {

        Map<String, String> userAttributes = findUserAttributeMap(username, password, searchEmail);

        if (userAttributes == null || userAttributes.isEmpty()) {
            return null;
        }

        HumanUser user = new HumanUser();
        user.setUser(new User());
        user.getUser().setFullname(userAttributes.get("givenName") + " " + userAttributes.get("sn"));
        user.getUser().setIsApiUser(false);
        user.setEmail(userAttributes.get("mail"));
        user.setNetworkId(userAttributes.get("sAMAccountName"));

        return user;
    }

    public Map<String, String> findUserAttributeMap(String username, String password, String searchEmail) {

        if (StringUtils.isBlank(username) || StringUtils.isBlank(password))
            return null;

        Map<String, String> userAttributes = new HashMap<String, String>();

        DirContext ctx = getServerContext(username, password);
        if (ctx == null) {
            return null;
        }

        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);

        String searchCtx = "dc=" + StringUtils.replace(DOMAIN, ".", ", dc=");

        try {
            NamingEnumeration<SearchResult> results = ctx.search(searchCtx, "mail" + "=" + searchEmail, sc);

            if (results.hasMore()) {
                SearchResult searchResult = results.next();
                Attributes attributes = searchResult.getAttributes();
                NamingEnumeration<? extends Attribute> attributeEnum = attributes.getAll();
                while (attributeEnum.hasMore()) {
                    Attribute attribute = attributeEnum.next();
                    userAttributes.put(attribute.getID(), toString(attributes.get(attribute.getID())));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return userAttributes;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private DirContext getServerContext(String username, String password) {
        Hashtable props = new Hashtable<String, String>();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.SECURITY_PRINCIPAL, username + '@' + DOMAIN);
        props.put(Context.SECURITY_CREDENTIALS, password);
        props.put(Context.REFERRAL, "follow");

        // Determine the list of active directory servers to use for auth.
        List<String> servers = getServers();
        try {
            return queryServers(servers, props);
        } catch (RuntimeException rt) {
            // this will happen if its unable to connect to any servers,
            // so we are going to force lookup on the cache
            servers = getServers();
            return queryServers(servers, props);
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private DirContext queryServers(List<String> servers, Hashtable props) {
        Exception exception = null;
        // Loop thru each server until one returns with no exception.
        if (servers != null && !servers.isEmpty()) {
            for (String server : servers) {
                try {
                    props.put(Context.PROVIDER_URL, "ldap://" + server + "/");
                    return new InitialDirContext(props);
                } catch (AuthenticationException ae) {
                    // authentication failed
                    return null;
                } catch (Exception e) {
                    // Catch the exception and try the next server.
                    exception = e;
                }
            }
        } else {
            exception = new Exception("Unable to locate LDAP servers (nslookup failure).");
        }
        throw new RuntimeException(
                "Unable to connect to any active directory servers (" + servers + ") to authenticate user.", exception);
    }

    /**
     * Takes an Attribute and returns a concatenated string of all its values
     * 
     * @param attribute
     *            the Active Directory attribute
     * @return concatenated string of all attribute values
     * @throws NamingException
     */
    private String toString(Attribute attribute) throws NamingException {
        StringBuilder result = new StringBuilder();
        if (attribute != null) {
            for (NamingEnumeration<?> enumeration = attribute.getAll(); enumeration.hasMore(); result
                    .append(enumeration.next()))
                ;
        }
        return result.toString();
    }

    /**
     * Executes the nsLookup command to retrieve the list of available LDAP servers.
     * 
     * @return List of available LDAP servers.
     */
    private List<String> getServers() {
        List<String> servers = new ArrayList<>();
        servers.add("10.40.70.196");
        servers.add("10.160.82.25");
        servers.add("10.50.1.135");
        servers.add("10.161.21.198");
        servers.add("10.27.28.222");
        servers.add("10.100.39.20");
        servers.add("10.101.128.231");
        servers.add("10.134.11.154");
        servers.add("168.146.185.32");
        servers.add("10.40.114.196");
        servers.add("10.161.133.198");
        servers.add("10.159.0.101");
        servers.add("10.162.69.198");
        servers.add("10.100.39.21");
        servers.add("10.150.2.154");
        servers.add("10.40.200.71");
        servers.add("10.161.21.131");
        servers.add("168.146.24.23");
        servers.add("10.161.69.38");
        servers.add("10.133.16.198");
        servers.add("168.146.121.23");
        servers.add("10.61.73.8");
        servers.add("10.61.20.194");
        servers.add("168.146.24.17");
        servers.add("168.146.1.196");
        
        return servers;        
    }
}
