package util;

import com.pearson.itautomation.oldthrashbarg.OldThrashbarg;
import com.typesafe.config.Config;
import org.jasypt.util.text.BasicTextEncryptor;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EncryptorDecryptor implements AttributeConverter<String, String> {

    /**
     * Encrypt the given string.
     *
     * @param decryptedString
     *            The string to encrypt.
     * @return The encrypted string.
     */
    @Override
    public String convertToDatabaseColumn(String decryptedString) {
        return encrypt(decryptedString);
    }

    /**
     * Decrypt the given string.
     *
     * @param encryptedString
     *            The string to decrypt.
     * @return The decrypted string.
     */
    @Override
    public String convertToEntityAttribute(String encryptedString) {
        return decrypt(encryptedString);
    }

    /**
     * Retrieve the string with the given key from the given configuration and decrypt it.
     *
     * @param configuration
     *            The configuration from which to retrieve the encrypted string.
     * @param key
     *            The key of the string to retrieve from the configuration.
     * @return The decrypted string.
     */
    public static String getDecryptedConfigurationString(Config configuration, String key) {
        return decrypt(configuration.getString(key));
    }

    /**
     * Encrypt the given string.
     *
     * @param decryptedString
     *            The string to encrypt.
     * @return The encrypted string.
     */
    private static String encrypt(String decryptedString) {
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPasswordCharArray(OldThrashbarg.KEY);
        return encryptor.encrypt(decryptedString);
    }

    /**
     * Decrypt the given string.
     *
     * @param encryptedString
     *            The string to decrypt.
     * @return The decrypted string.
     */
    private static String decrypt(String encryptedString) {
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPasswordCharArray(OldThrashbarg.KEY);
        return encryptor.decrypt(encryptedString);
    }
}
