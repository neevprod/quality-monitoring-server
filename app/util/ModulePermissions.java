package util;

import controllers.SecureController;
import models.PermissionSettingsValue;
import play.db.jpa.JPAApi;
import services.PermissionSettingsValueService;

public abstract class ModulePermissions {
    protected final boolean admin;
    private final JPAApi jpaApi;
    private final PermissionSettingsValueService permissionSettingsValueService;

    protected ModulePermissions(boolean admin, JPAApi jpaApi,
            PermissionSettingsValueService permissionSettingsValueService) {
        this.admin = admin;
        this.jpaApi = jpaApi;
        this.permissionSettingsValueService = permissionSettingsValueService;
    }

    /**
     * Find out whether the given permission settings has the given permission.
     *
     * @param permissionSettingsId
     *            The ID of the permission settings.
     * @param permissionName
     *            The name of the permission.
     * @param accessType
     *            The access type of the permission.
     * @return A boolean indicating whether the permission settings have the permission.
     */
    protected boolean permissionSettingsHasPermission(Long permissionSettingsId, String permissionName,
            SecureController.AccessType accessType) {
        if (permissionSettingsId != null) {
            boolean hasPermission = false;

            PermissionSettingsValue value = jpaApi.withTransaction(() -> permissionSettingsValueService
                    .findAllByPermissionSettingsId(permissionSettingsId).get(permissionName));

            if (value != null) {
                switch (accessType) {
                    case CREATE:
                        hasPermission = value.getCreateAccess();
                        break;
                    case READ:
                        hasPermission = value.getReadAccess();
                        break;
                    case UPDATE:
                        hasPermission = value.getUpdateAccess();
                        break;
                    case DELETE:
                        hasPermission = value.getDeleteAccess();
                        break;
                    default:
                        hasPermission = false;
                        break;
                }
            }

            return hasPermission;
        } else {
            return false;
        }
    }
}
