package util;

import controllers.SecureController;
import org.apache.commons.lang.math.NumberUtils;
import play.db.jpa.JPAApi;
import services.PermissionSettingsValueService;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class EnvironmentLevelPermissions extends ModulePermissions {
    private final EnvironmentLevelPermissionData environmentLevelPermissionData;

    protected EnvironmentLevelPermissions(EnvironmentLevelPermissionData environmentLevelPermissionData, boolean admin,
                                          JPAApi jpaApi,
                                          PermissionSettingsValueService permissionSettingsValueService) {
        super(admin, jpaApi, permissionSettingsValueService);
        this.environmentLevelPermissionData = environmentLevelPermissionData;
    }

    /**
     * Get whether the user has wildcard access to the environments.
     *
     * @return Whether the user has wildcard access.
     */
    public boolean hasWildcardAccess() {
        return environmentLevelPermissionData.getPermissions().get("*") != null;
    }

    /**
     * Get the set of environments that the user has access to. This method will filter out wildcards and not take them
     * into account. To find out if the user has wildcard access, use the method {@link #hasWildcardAccess()}.
     *
     * @return A set of environment IDs.
     */
    public Set<Integer> getAllowedEnvironmentIds() {
        return environmentLevelPermissionData.getPermissions().keySet().stream()
                .filter(NumberUtils::isNumber)
                .map(Integer::valueOf)
                .collect(Collectors.toSet());
    }

    /**
     * Find out whether the user has the given access type to the given permission for the given environment.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param environmentId The ID of the environment to access.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasPermission(String permissionName, SecureController.AccessType accessType, Integer environmentId) {
        if (admin) {
            return true;
        } else {
            Map<String, Long> permissions = environmentLevelPermissionData.getPermissions();

            Long permissionSettingsId = null;
            Long wildcardPermissionSettingsId = null;

            try {
                permissionSettingsId = permissions.get(environmentId.toString());
                wildcardPermissionSettingsId = permissions.get("*");
            } catch (NumberFormatException e) {
                permissionSettingsId = null;
            }

            boolean hasPermission =
                    permissionSettingsHasPermission(permissionSettingsId, permissionName, accessType);
            boolean hasWildcardPermission =
                    permissionSettingsHasPermission(wildcardPermissionSettingsId, permissionName, accessType);

            return hasPermission || hasWildcardPermission;
        }
    }

    /**
     * Find out whether the user has the given access type to the given permission for any environment.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasPermissionForAnyEnvironment(String permissionName, SecureController.AccessType accessType) {
        if (admin) {
            return true;
        } else {
            return environmentLevelPermissionData.getPermissions().values().stream()
                    .distinct()
                    .anyMatch(settingsId -> permissionSettingsHasPermission(settingsId, permissionName, accessType));
        }
    }
}
