package util;

import models.UserPermissions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EnvironmentLevelPermissionData extends ModulePermissionData {
    private final Map<String, Long> permissions;

    /**
     * Create a new EnvironmentLevelPermissionData instance.
     */
    public EnvironmentLevelPermissionData() {
        super(null);
        permissions = new HashMap<>();
    }

    /**
     * Create a new EnvironmentLevelPermissionData instance with the given module name and user permissions.
     *
     * @param moduleName The module name.
     * @param userPermissions The user permissions.
     */
    public EnvironmentLevelPermissionData(String moduleName, List<UserPermissions> userPermissions) {
        super(moduleName);
        permissions = userPermissions.stream()
                .collect(Collectors.toMap(p -> p.getIsWildcard() ? "*" : p.getEnvironmentId().toString(),
                                UserPermissions::getPermissionSettingsId));
    }

    /**
     * Get the permissions.
     *
     * @return The permissions.
     */
    public Map<String, Long> getPermissions() {
        return permissions;
    }
}
