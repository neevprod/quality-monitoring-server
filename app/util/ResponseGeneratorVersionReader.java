package util;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ResponseGeneratorVersionReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseGeneratorVersionReader.class);

    private static final int CONNECT_TIMEOUT = 10000;
    private static final int CONNECT_REQUEST_TIMEOUT = 10000;
    private static final int SOCKET_TIMEOUT = 60000;
    private static CloseableHttpClient httpClient = null;

    public static List<String> getAllVersions(String url) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        List<String> list = new ArrayList<>();
        try {
            URI httpRequest = new URI(url);
            HttpGet httpGet = new HttpGet(httpRequest);
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(getResult(httpGet));
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("artifact");
            for (int i = 0; i < nList.getLength(); i++) {
                list.add(getArtifact(nList.item(i)));
            }
        } catch (SAXException | ParserConfigurationException | IOException | URISyntaxException e1) {
        }
        return list;
    }

    private static InputStream getResult(final HttpGet httpGet) throws UnsupportedOperationException, IOException {
        if (httpClient == null) {
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(CONNECT_TIMEOUT)
                    .setSocketTimeout(SOCKET_TIMEOUT).setConnectionRequestTimeout(CONNECT_REQUEST_TIMEOUT).build();
            httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
        }
        return makeRequest(httpGet);
    }

    private static InputStream makeRequest(final HttpGet httpGet) throws UnsupportedOperationException, IOException {
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
        } catch (IOException e) {
            LOGGER.error(String.format("There was an error processing the request '%s'.", httpGet.getRequestLine()), e);
        }
        return response.getEntity().getContent();
    }

    private static String getArtifact(Node node) {
        Element element = null;
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            element = (Element) node;
        }
        return getTagValue("version", element);
    }

    private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }

    public static String getSelectedVersionId(String versionId, String url, String responseGeneratorPath) {
        String requestData = String.format(url + "&v=" + versionId + "&e=jar");
        HttpGet httpGet = new HttpGet(requestData);
        //try {
        String filename = "resp-gen-cli-" + versionId + ".jar";
        Path path = Paths.get(responseGeneratorPath + File.separator);
        String absoluteFilePath = path + File.separator + filename;
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
            if (new File(absoluteFilePath).exists()) {
                Files.write(Paths.get(responseGeneratorPath + File.separator + "responseGenActiveVersion.txt"), versionId.getBytes());
                return "success";
            }
        } catch (IOException e) {
            return "failure";
        }

        try (FileOutputStream fos = new FileOutputStream(new File(absoluteFilePath))) {
            InputStream inputStream = getResult(httpGet);
            int inByte;
            while ((inByte = inputStream.read()) != -1) {
                fos.write(inByte);
            }
            Files.write(Paths.get(responseGeneratorPath + File.separator + "responseGenActiveVersion.txt"), versionId.getBytes());
            return "success";
        } catch (IOException e) {
            return "failure";
        }
    }
}
