package util.qtirespgen;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pearson.itautomation.qtirespgen.*;
import com.pearson.itautomation.qtirespgen.visitor.DefaultAnswerVisitor;
import com.pearson.itautomation.qtirespgen.visitor.scoringengine.ScoringVisitor;
import com.typesafe.config.Config;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jdom2.JDOMException;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;
import play.libs.ws.WSClient;
import services.ApplicationSettingService;

import javax.inject.Inject;
import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.pearson.itautomation.qtirespgen.IssueType.P_XSD_VALIDATION_ERROR;

public class QtiResponseGenerator {
    private static final String SUCCESS = "success";
    private static final String DATA = "data";
    private static final String ERRORS = "errors";
    private static final String MESSAGE = "message";
    private static final String SCORABLE = "scorable";

    private final ApplicationSettingService applicationSettingService;
    private final JPAApi jpaApi;
    private final WSClient wsClient;
    private final play.Environment environment;
    private final long responseGeneratorTimeout;
    private String responseGeneratorPath;

    @Inject
    public QtiResponseGenerator(ApplicationSettingService applicationSettingService, JPAApi jpaApi,
                                Config configuration, WSClient wsClient, play.Environment environment) {
        this.applicationSettingService = applicationSettingService;
        this.jpaApi = jpaApi;
        this.wsClient = wsClient;
        this.environment = environment;
        this.responseGeneratorTimeout = configuration.getLong("application.responseGeneratorTimeout");
        this.responseGeneratorPath = configuration.getString("application.responseGenPath");
    }

    /**
     * Call the QTI Response Generator web service and transform the result to the required format.
     *
     * @param itemXml        The item XML to generate responses for.
     * @param maxCorrect     The max number of incorrect responses.
     * @param maxIncorrect   The max number of correct responses.
     * @param outputType     The output type.
     * @param removeOeScores Whether to remove OE scores.
     * @param previewerId    The previewer ID.
     * @param tenantId       The tenant ID.
     * @param itemIdentifier The item identifier.
     * @return The result.
     */
    public JsonNode generateResponses(final String itemXml, final int previewerId, final long tenantId,
                                      final int maxCorrect, final int maxIncorrect, final int outputType,
                                      final boolean removeOeScores, final String itemIdentifier) {
        Logger.info(
                "event=\"Generating responses for item.\", previewerId=\"{}\", tenantId=\"{}\", itemIdentifier=\"{}\"",
                previewerId, tenantId, itemIdentifier);
        return generateResponses(itemXml, maxCorrect, maxIncorrect, outputType, removeOeScores);
    }

    /**
     * Call the QTI Response Generator web service and transform the result to the required format.
     *
     * @param itemXml        The item XML to generate responses for.
     * @param maxCorrect     The max number of incorrect responses.
     * @param maxIncorrect   The max number of correct responses.
     * @param outputType     The output type.
     * @param removeOeScores Whether to remove OE scores.
     * @return The result.
     */
    public JsonNode generateResponses(final String itemXml, final int maxCorrect, final int maxIncorrect,
                                      final int outputType, final boolean removeOeScores) {
        JsonNode result;

        try {
            result = getResponseFromLibrary(itemXml, maxCorrect, maxIncorrect);
        } catch (IOException e) {
            Logger.error("event=\"Could not generate responses for item.\"", e);
            return constructErrorJson("Couldn't generate responses for item. " + e.getMessage().replace("\r\n", " "));
        } catch (JDOMException e) {
            Logger.error("event=\"Could not generate responses for item.\"", e);
            return constructErrorJson("Couldn't generate responses for item. Error parsing XML.");
        }

        return result;
    }

    private JsonNode getResponseFromLibrary(final String itemXml, final int maxCorrect, final int maxIncorrect)
            throws IOException, JDOMException {
        int correctCount = 0;
        int incorrectCount = 0;

        final String IGNORED_IDENTIFIERS = "(?i)SIMULATION.*";

        ObjectNode resultJson = Json.newObject();
        resultJson.put(SUCCESS, true);

        QtiItem qtiItem = new QtiItem(itemXml);
        String itemIdentifier = qtiItem.getAssessmentItem().getIdentifier();
        IssueList issueList = qtiItem.getIssues();
        if (issueList.count(IssueSeverity.CRITICAL) > 0 || issueList.count(IssueSeverity.ERROR) > 0) {
            // Add all error messages to a Set, since there can be duplicates.
            Set<String> messages = StreamSupport.stream(issueList.spliterator(), false).filter(issue -> issue.getIssueSeverity() == IssueSeverity.CRITICAL
                    || (issue.getIssueSeverity() == IssueSeverity.ERROR && issue.getIssueType() != P_XSD_VALIDATION_ERROR)).map(Issue::getIssueText).collect(Collectors.toSet());

            ArrayNode errorsNode = resultJson.putArray(ERRORS);
            for (String message : messages) {
                ObjectNode errorNode = errorsNode.addObject();
                errorNode.put(MESSAGE, message);
            }
        }

        if (qtiItem.getAssessmentItem().getItemBody().isPresent() && !qtiItem.getAssessmentItem().getItemBody().get()
                .getInteractions().stream()
                .filter(i -> !i.getResponseIdentifier().matches(IGNORED_IDENTIFIERS) && !i.supportsResponseGeneration())
                .findFirst().isPresent()) {

            String javaHome = System.getProperty("java.home");
            String javaBin = javaHome +
                    File.separator + "bin" +
                    File.separator + "java";

            List<JsonElement> responses = new ArrayList<>();
            String versionId = new String(Files.readAllBytes(Paths.get(responseGeneratorPath + File.separator + "responseGenActiveVersion.txt")));
            String responseGeneratorFileName = "resp-gen-cli-" + versionId + ".jar";
            String responseGenJarPath = responseGeneratorPath + File.separator + responseGeneratorFileName;
            String className = "com.pearson.itautomation.respgencli.Application";
            String responsesOutputPath = responseGeneratorPath + File.separator + "temp";

            String itemXmlInputFile = responsesOutputPath + File.separator + UUID.randomUUID().toString();
            String responsesOutputFile = responsesOutputPath + File.separator + UUID.randomUUID().toString();

            File directory = new File(responsesOutputPath);

            if (!directory.exists()) {
                directory.mkdirs();
            }

            FileUtils.writeStringToFile(new File(itemXmlInputFile), itemXml, StandardCharsets.UTF_8);

            final File tmp = File.createTempFile("out", null);
            tmp.deleteOnExit();

            ProcessBuilder builder = new ProcessBuilder(
                    javaBin, "-cp", responseGenJarPath, className, "-i", itemXmlInputFile,
                    "-o", responsesOutputFile, "-ignoredResponses", "(?i)SIMULATION.*")
                    .redirectErrorStream(true).redirectOutput(tmp);

            Process process = builder.start();

            try {
                if (!process.waitFor(responseGeneratorTimeout, TimeUnit.SECONDS)) {
                    Logger.error("event=\"Could not generate responses for item within the specified timeout.\"");
                }
            } catch (InterruptedException e) {
                Logger.error("event=\"The response generation process was interrupted before completing.\"", e);
            }

            tmp.delete();

            if (process.isAlive()) {
                process.destroyForcibly();
            }

            File inputFile = new File(itemXmlInputFile);

            if (inputFile.exists()) {
                inputFile.delete();
            }

            File outputFile = new File(responsesOutputFile);

            if (outputFile.exists()) {
                FileReader fileReader = null;
                try {
                    fileReader = new FileReader(outputFile);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    String line;

                    while (null != (line = bufferedReader.readLine())) {
                        JsonElement jsonElement = new JsonParser().parse(line);
                        responses.add(jsonElement);
                    }
                    bufferedReader.close();

                } catch (FileNotFoundException e) {
                    Logger.error("event=\"Could not find the temporary response file created by response generator.\"", e);
                } finally {
                    if (null != fileReader) {
                        fileReader.close();
                    }
                    outputFile.delete();
                }
            }

            if (responses.isEmpty()) {
                Logger.warn("event=\"No responses could be generated for item.\", itemIdentifier=\"{}\"",
                        itemIdentifier);

                resultJson.put(SUCCESS, false);
                JsonNode errorsNode = resultJson.get(ERRORS);
                if (errorsNode == null) {
                    errorsNode = resultJson.putArray(ERRORS);
                }
                ObjectNode errorNode = ((ArrayNode) errorsNode).addObject();
                errorNode.put(MESSAGE, "Couldn't generate responses for item.");

                return resultJson;
            }

            DefaultAnswerVisitor defaultAnswerVisitor = new DefaultAnswerVisitor();
            qtiItem.accept(defaultAnswerVisitor);
            final Map<String, String> defaultAnswers = defaultAnswerVisitor.getDefaultAnswers();
            JsonElement defaultResponse = defaultAnswerVisitor.getAsResponse();
            responses.remove(defaultResponse);
            responses.add(0, defaultResponse);

            ObjectNode dataNode = resultJson.putObject(DATA);

            dataNode.put("itemId", itemIdentifier);

            ArrayNode identToInterArray = Json.newArray();
            dataNode.set("identifiersToInteractions", identToInterArray);

            final List<IdentifierToInteraction> identifierToInteractionMappings = qtiItem
                    .getIdentifierToInteractionMappings();
            for (IdentifierToInteraction identifierToInteraction : identifierToInteractionMappings) {
                final ObjectNode identToInterObj = Json.newObject();
                identToInterArray.add(identToInterObj);
                identToInterObj.put("identifier", identifierToInteraction.getIdentifier());
                String interactionName = identifierToInteraction.getInteraction();
                if (!identifierToInteraction.getSubType().isEmpty()) {
                    interactionName += "." + identifierToInteraction.getSubType();
                }
                identToInterObj.put("interaction", interactionName);
            }

            ArrayNode responsesArray = Json.newArray();
            dataNode.set("responses", responsesArray);

            filterResponses(qtiItem, responses, maxCorrect, maxIncorrect);

            for (int i = 0; i < responses.size(); i++) {
                JsonElement response = responses.get(i);
                ScoringVisitor scoringVisitor = new ScoringVisitor(response.toString());
                qtiItem.accept(scoringVisitor);

                ObjectNode responseObj = Json.newObject();
                responsesArray.add(responseObj);

                ArrayNode modsArray = Json.newArray();
                final Iterator<JsonElement> modsIterator = response.getAsJsonObject().getAsJsonArray("mods").iterator();
                boolean attempted = false;
                while (modsIterator.hasNext()) {
                    final JsonObject currMod = modsIterator.next().getAsJsonObject();
                    ObjectNode modObj = Json.newObject();
                    modsArray.add(modObj);
                    modObj.set("r", Json.parse(currMod.get("r").toString()));
                    modObj.put("did", currMod.get("did").getAsString());

                    if (!currMod.get("r").toString().equals(defaultAnswers.get(currMod.get("did").getAsString()))) {
                        attempted = true;
                    }
                }
                responseObj.put("attempted", attempted);
                responseObj.set("mods", modsArray);

                final Iterator<Map.Entry<String, JsonElement>> outcomesIterator = scoringVisitor.getOutcomes()
                        .entrySet().iterator();

                ArrayNode outcomesArray = Json.newArray();
                responseObj.set("outcomes", outcomesArray);

                while (outcomesIterator.hasNext()) {
                    final Map.Entry<String, JsonElement> currOutcome = outcomesIterator.next();
                    ObjectNode outcomeObj = Json.newObject();
                    outcomesArray.add(outcomeObj);
                    outcomeObj.put("identifier", currOutcome.getKey());
                    outcomeObj.set("value", Json.parse(currOutcome.getValue().toString()));
                }
            }

            Logger.info(
                    "event=\"Created responses using response generator version: \"{}\", itemIdentifier=\"{}\", returnJson=\"{}\"",
                    versionId, itemIdentifier, resultJson);
            return resultJson;
        }
        Logger.warn(
                "event=\"No responses could be generated for item. One or more interaction types are not supported.\", "
                        + "itemIdentifier=\"{}\"",
                itemIdentifier);

        resultJson.put(SUCCESS, false);
        JsonNode errorsNode = resultJson.get(ERRORS);
        if (errorsNode == null) {
            errorsNode = resultJson.putArray(ERRORS);
        }
        ObjectNode errorNode = ((ArrayNode) errorsNode).addObject();
        errorNode.put(MESSAGE,
                "Couldn't generate responses for item. One or more interaction types are not supported.");
        return resultJson;
    }

    /**
     * This will take a list of responses and manipulate it so the default response and the appropriate number of
     * maxCorrect and maxIncorrect responses remain.  The default response should be in the first position on the list.
     * The other responses will be scored and it will attempt to return at least one response at each score point without
     * violating the maxCorrect and maxIncorrect parameters.
     *
     * @param qtiItem      The qtiItem to score against.
     * @param responses    The responses list that will be altered.
     * @param maxCorrect   The maximum number of correct responses to be returned.
     * @param maxIncorrect The maximum number of incorrect responses to be returned.
     */
    private void filterResponses(final QtiItem qtiItem, final List<JsonElement> responses,
                                 final int maxCorrect, final int maxIncorrect) {
        if (responses.isEmpty()) {
            return;
        }

        Map<JsonElement, BigDecimal> scoredResponses = new HashMap<>();

        JsonElement defaultResponse = responses.remove(0);

        // score all the things
        for (int i = 0; i < responses.size(); i++) {
            JsonElement response = responses.get(i);
            ScoringVisitor scoringVisitor = new ScoringVisitor(response.toString());
            qtiItem.accept(scoringVisitor);

            if (scoringVisitor.getOutcomes().containsKey("SCORE")) {
                scoredResponses.put(response, scoringVisitor.getOutcomes().get("SCORE").getAsBigDecimal());
            } else {
                Logger.warn(
                        "event=\"The \"SCORE\" outcome could not be found and the response cannot be used.\", response=\"{}\"",
                        response.toString());
                continue;
            }
        }

        // empty out the responses so we can repopulate it
        responses.clear();

        // add the default response and remove it from the scoredResponses
        responses.add(defaultResponse);
        scoredResponses.remove(defaultResponse);

        // add maxIncorrect (or less) answers with a score of 0.
        responses.addAll(scoredResponses.entrySet().stream().filter(es -> es.getValue().compareTo(BigDecimal.ZERO) == 0)
                .limit(maxIncorrect).map(Map.Entry::getKey).collect(Collectors.toList()));

        // remove all answers with a score of 0
        scoredResponses.keySet().removeAll(scoredResponses.entrySet().stream()
                .filter(es -> es.getValue().compareTo(BigDecimal.ZERO) == 0)
                .map(Map.Entry::getKey).collect(Collectors.toSet()));

        // determine all the possible scores and sort them
        List<BigDecimal> possibleScores = scoredResponses.values().stream().distinct()
                .sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        // get a list of correct answers for each score point but don't exceed maxCorrect
        List<JsonElement> correctAnswers = new ArrayList<>();
        for (BigDecimal score : possibleScores) {
            if (correctAnswers.size() >= maxCorrect) {
                break;
            }
            // .get() should be safe here since the filter is based on data from the map
            JsonElement response = scoredResponses.entrySet().stream()
                    .filter(e -> e.getValue().compareTo(score) == 0).findFirst().get().getKey();
            correctAnswers.add(response);
        }

        // if we still have need of more correct answers, just select some more until we hit the limit
        if (correctAnswers.size() < maxCorrect) {
            scoredResponses.keySet().removeAll(correctAnswers);
            correctAnswers.addAll(scoredResponses.entrySet().stream().limit(maxCorrect - correctAnswers.size())
                    .map(Map.Entry::getKey).collect(Collectors.toList()));
        }

        responses.addAll(correctAnswers);
    }

    /**
     * Construct JSON with the given message.
     *
     * @return The JSON.
     */
    private JsonNode constructErrorJson(String message) {
        ObjectNode json = Json.newObject();
        json.put(SUCCESS, false);
        ArrayNode errorsJson = json.putArray(ERRORS);
        ObjectNode errorJson = errorsJson.addObject();
        errorJson.put(MESSAGE, message);
        return json;
    }

    /**
     * Calls the scoring visitor to get the outcome for the given <code>responses</code>
     *
     * @param itemXml   The item xml of an item against which scoring is applied
     * @param responses The List of responses going to be scored.
     * @return An instance of {@link ResponseOutcomeResult}
     */
    public ResponseOutcomeResult getResponseOutcomes(final String itemXml, final ArrayNode responses) {
        QtiItem qtiItem;
        try {
            qtiItem = new QtiItem(itemXml);
        } catch (Exception ex) {
            String message = "Found invalid item XML. Can not load item XML by QTI Response Generator.";
            Logger.error("event= \"{}\"", message, ex);
            return new ResponseOutcomeResult(false, null, Collections.singletonList(message));
        }

        ObjectNode dataNode = Json.newObject();
        ArrayNode responseOutcomes = dataNode.putArray("responseOutcomes");

        if (qtiItem.getAssessmentItem().getItemBody().isPresent()) {
            for (JsonNode response : responses) {
                ObjectNode responseOutcome = responseOutcomes.addObject();
                responseOutcome.put(SCORABLE, true);
                Map<String, JsonElement> initializedOutcomeMap = new HashMap<>();
                if (response.get("initializedOutcomes") != null) {
                    ArrayNode initializedOutcomes = (ArrayNode) response.get("initializedOutcomes");
                    for (JsonNode initializedOutcome : initializedOutcomes) {
                        initializedOutcomeMap.put(initializedOutcome.get("identifier").asText(),
                                new JsonParser().parse(initializedOutcome.get("value").toString()));
                    }
                }

                try {
                    ScoringVisitor scoringVisitor = new ScoringVisitor(response.toString(), initializedOutcomeMap);
                    qtiItem.accept(scoringVisitor);
                    checkErrors(qtiItem, responseOutcome);
                    if (responseOutcome.get(SCORABLE).asBoolean()) {
                        Map<String, JsonElement> outcomesMap = scoringVisitor.getOutcomes();
                        ArrayNode outcomes = responseOutcome.putArray("outcomes");
                        for (String identifier : outcomesMap.keySet()) {
                            ObjectNode outcome = outcomes.addObject();
                            outcome.put("identifier", identifier);
                            outcome.put("value", outcomesMap.get(identifier).getAsString());
                        }
                    }
                } catch (Exception ex) {
                    Logger.error("event=\"Failed to score item.\", itemId= \"{}\", response= \"{}\"",
                            qtiItem.getAssessmentItem().getIdentifier(), response, ex);
                    ArrayNode errorsJson = responseOutcome.putArray(ERRORS);
                    responseOutcome.put(SCORABLE, false);
                    ObjectNode errorJson = errorsJson.addObject();
                    errorJson.put(MESSAGE, "Failed to score item for response " + response.toString());
                }

            }
        } else {
            List<String> errors = StreamSupport.stream(qtiItem.getIssues().spliterator(), false)
                    .filter(issue -> issue.getIssueSeverity() == IssueSeverity.CRITICAL
                            || issue.getIssueSeverity() == IssueSeverity.ERROR)
                    .map(Issue::getIssueText).collect(Collectors.toList());
            errors.add(0, "ItemBody not found for item id " + qtiItem.getAssessmentItem().getIdentifier());
            Logger.error("event= \"ItemBody not found.\", itemId= \"{}\", issues found in item= \"{}\"",
                    qtiItem.getAssessmentItem().getIdentifier(), StringUtils.join(errors, "," + " "));
            return new ResponseOutcomeResult(false, null, errors);
        }
        return new ResponseOutcomeResult(true, dataNode, null);
    }

    private void checkErrors(final QtiItem qtiItem, final ObjectNode responseOutcome) {
        ArrayNode errorsJson = responseOutcome.putArray(ERRORS);
        for (Issue issue : qtiItem.getIssues()) {
            if (issue.getIssueSeverity() == IssueSeverity.CRITICAL || (issue.getIssueSeverity() == IssueSeverity.ERROR
                    && issue.getIssueType() != P_XSD_VALIDATION_ERROR)) {
                responseOutcome.put(SCORABLE, false);
                ObjectNode errorJson = errorsJson.addObject();
                errorJson.put(MESSAGE, issue.getIssueText());
            }
        }
    }

    /**
     * Represents the response of CDQ API. Contains the following information:
     * <ul>
     * <li>success: true/false</li>
     * <li>data: the json representation of responseOutcomes</li>
     * <li>errors: the list of errors</li>
     * </ul>
     */
    public static final class ResponseOutcomeResult {
        private final boolean success;
        private final ObjectNode data;
        private final List<String> errors;

        public ResponseOutcomeResult(final boolean success, final ObjectNode data, final List<String> errors) {
            this.success = success;
            this.errors = errors;
            this.data = data;
        }

        public final boolean isSuccess() {
            return success;
        }

        public final List<String> getErrors() {
            return errors;
        }

        public final ObjectNode getData() {
            return data;
        }
    }
}
