package util;

import models.UserPermissions;

public class ModuleLevelPermissionData extends ModulePermissionData {
    private final Long permissions;

    /**
     * Create a new ModuleLevelPermissions instance.
     */
    public ModuleLevelPermissionData() {
        super(null);
        permissions = null;
    }

    /**
     * Create a new ModuleLevelPermissions instance with the given module name and user permissions.
     *
     * @param moduleName The module name.
     * @param userPermissions The user permissions.
     */
    public ModuleLevelPermissionData(String moduleName, UserPermissions userPermissions) {
        super(moduleName);
        permissions = userPermissions.getPermissionSettingsId();
    }

    /**
     * Get the permissions.
     *
     * @return The permissions.
     */
    public Long getPermissions() {
        return permissions;
    }
}
