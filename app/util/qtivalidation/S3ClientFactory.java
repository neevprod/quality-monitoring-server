package util.qtivalidation;

import com.typesafe.config.Config;
import play.Logger;
import util.S3Client;

import javax.inject.Inject;

public class S3ClientFactory {
    private final String clientRegion;

    @Inject
    S3ClientFactory(Config configuration) {
        this.clientRegion = configuration.getString("application.s3.kt.clientRegion");
    }

    /**
     * @return the new instance of S3Client
     */
    public S3Client create() {
        Logger.debug("event=\"Creating new instance of S3Client.\", clientRegion=\"{}\"", clientRegion);
        return new S3Client(clientRegion);
    }
}
