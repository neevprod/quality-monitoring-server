package util.qtivalidation;

import com.pearson.itautomation.qtiscv.TestCaseLoader;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import com.typesafe.config.Config;

import javax.inject.Inject;
import java.util.Optional;

/**
 * A factory for creating instances of TestCaseLoader.
 */
public class TestCaseLoaderFactory {
    private final TestNav8APIFactory testNav8APIFactory;
    private final String dbUrl;
    private final String dbUsername;
    private final String dbPassword;

    @Inject
    TestCaseLoaderFactory(TestNav8APIFactory testNav8APIFactory, Config configuration) {
        this.testNav8APIFactory = testNav8APIFactory;
        this.dbUrl = configuration.getString("db.default.url");
        this.dbUsername = configuration.getString("db.default.username");
        this.dbPassword = configuration.getString("db.default.password");
    }

    /**
     * Create a new instance of TestCaseLoader for the given previewer.
     *
     * @param previewerId The ID of the previewer.
     * @return An Optional of TestCaseLoader.
     */
    public Optional<TestCaseLoader> create(int previewerId) {
        Optional<TestNav8API> testNav8ApiOptional = testNav8APIFactory.create(previewerId);
        if (testNav8ApiOptional.isPresent()) {
            return Optional.of(new TestCaseLoader(dbUrl, dbUsername, dbPassword, testNav8ApiOptional.get()));
        } else {
            return Optional.empty();
        }
    }
}
