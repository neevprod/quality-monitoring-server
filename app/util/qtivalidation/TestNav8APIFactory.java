package util.qtivalidation;

import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import com.typesafe.config.Config;
import models.qtivalidation.Previewer;
import play.Logger;
import play.cache.SyncCacheApi;
import play.db.jpa.JPAApi;
import services.qtivalidation.PreviewerService;
import util.EncryptorDecryptor;

import javax.inject.Inject;
import java.util.Optional;

public class TestNav8APIFactory {
    private final String apiToken;
    private final String apiSecret;
    private final SyncCacheApi cache;
    private final JPAApi jpa;
    private final PreviewerService previewerService;

    @Inject
    TestNav8APIFactory(Config configuration, SyncCacheApi cacheApi, JPAApi jpa, PreviewerService previewerService) {
        this.apiToken = configuration.getString("application.previewerApiToken");
        this.apiSecret = EncryptorDecryptor.getDecryptedConfigurationString(configuration,
                "application.previewerApiSecret");
        this.cache = cacheApi;
        this.jpa = jpa;
        this.previewerService = previewerService;
    }

    /**
     * Retrieve an instance of TestNav8API from the cache for the given previewer ID. If no instance exists, a new one
     * will be created and stored in the cache. If the given previewer ID is invalid, an empty Optional will be
     * returned.
     * <p>
     * Note: This must be called from within a JPA transaction.
     *
     * @param previewerId indicates the previewer to get the instance of TestNav8API for
     * @return an Optional of TestNav8Api
     */
    public Optional<TestNav8API> create(int previewerId) {
        Logger.debug("event=\"Retrieving TestNav8API from cache.\", previewerId=\"{}\"", previewerId);

        // Todo: Fix concurrency issue in TestNav8Api so we can cache and reuse instances of it.
        // return cache.getOrElse("testNav8Api." + previewerId, () -> createNewTestNav8Api(previewerId));
        return createNewTestNav8Api(previewerId);
    }

    /**
     * Returns a new instance of TestNav8API for the given previewer ID. If the given previewer ID is invalid, an empty
     * Optional will be returned.
     *
     * @param previewerId indicates the previewer to get the instance of TestNav8API for
     * @return an Optional of TestNav8Api
     */
    private Optional<TestNav8API> createNewTestNav8Api(int previewerId) {
        Logger.debug("event=\"Creating new instance of TestNav8API.\", previewerId=\"{}\"", previewerId);

        Optional<Previewer> optionalPreviewer = previewerService.find(previewerId);

        if (optionalPreviewer.isPresent()) {
            Previewer previewer = optionalPreviewer.get();
            return Optional.of(TestNav8API.connectToPreviewer(previewer.getApiProtocol() + previewer.getApiUrl(),
                    apiToken.toCharArray(), apiSecret.toCharArray()));
        } else {
            return Optional.empty();
        }
    }

}
