package util.qtivalidation;

import com.pearson.itautomation.qtiscv.ItemTestCaseCoverage;
import com.typesafe.config.Config;
import global.exceptions.QaApplicationException;
import models.qtivalidation.Previewer;
import play.Logger;
import services.qtivalidation.PreviewerService;
import util.EncryptorDecryptor;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Optional;

/**
 * Created by UFAJAJO on 10/6/2015.
 */
public class TestCaseCoverageFactory {

    private Config configuration;
    private PreviewerService previewerService;

    @Inject
    TestCaseCoverageFactory(Config configuration, PreviewerService previewerService){
        this.configuration = configuration;
        this.previewerService = previewerService;
    }

    public ItemTestCaseCoverage create(int previewerId)  throws QaApplicationException{
        Logger.info("event=\"Creating new instance of Item Response Pool.\", previewerId=\"{}\"", previewerId);

        String propString = "DB_HOST=" + configuration.getString("db.default.host") + "\n" + "DB_USER="
                + configuration.getString("db.default.username") + "\n" + "DB_PASS="
                + configuration.getString("db.default.password") + "\n" + "DB_PORT="
                + configuration.getString("db.default.port") + "\n" + "DB_DB="
                + configuration.getString("db.default.schema") + "\n" + "TN8_TOKEN="
                + configuration.getString("application.previewerApiToken") + "\n" + "TN8_SECRET="
                + EncryptorDecryptor.getDecryptedConfigurationString(configuration, "application.previewerApiSecret");

        InputStream properties = new ByteArrayInputStream(propString.getBytes());

        Optional<Previewer> optionalPreviewer = previewerService.find(previewerId);

        if (optionalPreviewer.isPresent()) {
            Previewer previewer = optionalPreviewer.get();
            return new ItemTestCaseCoverage(properties, previewer.getApiProtocol() + previewer.getApiUrl());

        } else {
            Logger.info("event=\"Previewer not found when creating instance of Test Case Coverage\", previewerId=\"{}\"",
                    previewerId);
            throw new QaApplicationException("Unable to find a previewer with the given ID.");
        }
    }

}
