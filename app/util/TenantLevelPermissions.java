package util;

import controllers.SecureController;
import org.apache.commons.lang.math.NumberUtils;
import play.db.jpa.JPAApi;
import services.PermissionSettingsValueService;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class TenantLevelPermissions extends ModulePermissions {
    private final TenantLevelPermissionData tenantLevelPermissionData;

    public TenantLevelPermissions(TenantLevelPermissionData tenantLevelPermissionData, boolean admin, JPAApi jpaApi,
                                  PermissionSettingsValueService permissionSettingsValueService) {
        super(admin, jpaApi, permissionSettingsValueService);
        this.tenantLevelPermissionData = tenantLevelPermissionData;
    }

    /**
     * Get whether the user has wildcard access to the given previewer.
     *
     * @param previewerId The previewer ID.
     * @return Whether the user has wildcard access.
     */
    public boolean hasWildcardAccessToPreviewer(Integer previewerId) {
        Map<String, Long> previewerPermissions = tenantLevelPermissionData.getPermissions().get(previewerId);
        return previewerPermissions != null && previewerPermissions.get("*") != null;
    }

    /**
     * Get the set of previewer IDs that the user has access to.
     *
     * @return A set of previewer IDs.
     */
    public Set<Integer> getAllowedPreviewerIds() {
        return tenantLevelPermissionData.getPermissions().keySet().stream()
                .map(Integer::valueOf)
                .collect(Collectors.toSet());
    }

    /**
     * Get the set of tenants that the user has access to for the given previewer. This method will filter out
     * wildcards and not take them into account. To find out if the user has wildcard access to a previewer, use the
     * method {@link #hasWildcardAccessToPreviewer(Integer)}.
     *
     * @param previewerId The previewer ID.
     * @return A set of tenantIds.
     */
    public Set<Long> getAllowedTenantIds(Integer previewerId) {
        Map<String, Long> previewerPermissions = tenantLevelPermissionData.getPermissions().get(previewerId);
        if (previewerPermissions != null) {
            return previewerPermissions.keySet().stream()
                    .filter(NumberUtils::isNumber)
                    .map(Long::valueOf)
                    .collect(Collectors.toSet());
        } else {
            return new HashSet<Long>();
        }
    }

    /**
     * Find out whether the user has the given access type to the given permission for the given tenant.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param previewerId The ID of the previewer that contains the tenant to access.
     * @param tenantId The ID of the tenant to access.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasPermission(String permissionName, SecureController.AccessType accessType, Integer previewerId,
                                 Long tenantId) {
        if (admin) {
            return true;
        } else {
            Map<String, Long> previewerPermissions = tenantLevelPermissionData.getPermissions().get(previewerId);

            if (previewerPermissions != null) {
                Long permissionSettingsId = null;
                Long wildcardPermissionSettingsId = null;

                try {
                    permissionSettingsId = previewerPermissions.get(tenantId.toString());
                    wildcardPermissionSettingsId = previewerPermissions.get("*");
                } catch (NumberFormatException e) {
                    permissionSettingsId = null;
                }

                boolean hasPermission =
                        permissionSettingsHasPermission(permissionSettingsId, permissionName, accessType);
                boolean hasWildcardPermission =
                        permissionSettingsHasPermission(wildcardPermissionSettingsId, permissionName, accessType);

                return hasPermission || hasWildcardPermission;
            }

            return false;
        }
    }

    /**
     * Find out whether the user has the given access type to the given permission for any tenant under the given
     * previewer.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @param previewerId The ID of the previewer.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasPermissionForAnyTenant(String permissionName, SecureController.AccessType accessType,
                                             Integer previewerId) {
        if (admin) {
            return true;
        } else {
            Map<String, Long> previewerPermissions = tenantLevelPermissionData.getPermissions().get(previewerId);
            if (previewerPermissions != null) {
                return previewerPermissions.values().stream()
                        .distinct()
                        .anyMatch(id -> permissionSettingsHasPermission(id, permissionName, accessType));
            } else {
                return false;
            }
        }
    }
}
