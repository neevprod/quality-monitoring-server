package util;

import controllers.SecureController;
import play.db.jpa.JPAApi;
import services.PermissionSettingsValueService;

public class ModuleLevelPermissions extends ModulePermissions {
    private final ModuleLevelPermissionData moduleLevelPermissionData;

    protected ModuleLevelPermissions(ModuleLevelPermissionData moduleLevelPermissionData, boolean admin, JPAApi jpaApi,
                                     PermissionSettingsValueService permissionSettingsValueService) {
        super(admin, jpaApi, permissionSettingsValueService);
        this.moduleLevelPermissionData = moduleLevelPermissionData;
    }

    /**
     * Find out whether the user has the given access type to the given permission.
     *
     * @param permissionName The name of the permission in question.
     * @param accessType The access type desired.
     * @return A boolean value indicating whether the user has access to the permission.
     */
    public boolean hasPermission(String permissionName, SecureController.AccessType accessType) {
        if (admin) {
            return true;
        } else {
            Long permissionSettingsId = moduleLevelPermissionData.getPermissions();
            return permissionSettingsHasPermission(permissionSettingsId, permissionName, accessType);
        }
    }
}
