package util;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This class is used to connect Amazon S3 using accessKey and secretKey.<br>
 * The {@link #downFileFromS3(String, String)} method is used to download the content of S3 for the given bucket and key.
 */
public class S3Client {
    private AmazonS3 amazonS3Client;

    /**
     * Initializes the Amazon S3 client using {@link DefaultAWSCredentialsProviderChain}
     *
     * @param clientRegion The clientRegion
     */
    public S3Client(final String clientRegion) {
        this.amazonS3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new DefaultAWSCredentialsProviderChain())
                .withRegion(clientRegion)
                .build();
    }

    public void setAmazonS3Client(AmazonS3 amazonS3Client) {
        this.amazonS3Client = amazonS3Client;
    }

    /**
     * Downloads the file from S3 based on the given provided S3 bucket and key information.
     * Creates the instance of {@link BufferedReader} and returns it.
     *
     * @param bucketName The S3 bucket name
     * @param key        The S3 file name
     * @return The instance of {@link BufferedReader} which contains the content of file to be downloaded
     */
    public BufferedReader downFileFromS3(final String bucketName, final String key) {
        S3Object s3object = this.amazonS3Client.getObject(bucketName, key);
        S3ObjectInputStream inputStream = s3object.getObjectContent();
        return new BufferedReader(new InputStreamReader(inputStream));
    }
}