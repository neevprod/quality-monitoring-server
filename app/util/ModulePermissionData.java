package util;

/*
Note: These annotations are repeated here because Jackson is repackaged inside of the JWT library we are using. We need
to have one version for the JWT library since it does the serializing, and another version for the web app code, since
it does the deserializing. This is unfortunate, and hopefully the JWT library will fix this issue in the future.
 */
@com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonTypeInfo(
        use = com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME,
        include = com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY,
        property = "permissionType"
)
@com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonSubTypes({
        @com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonSubTypes.Type(
                value = ModuleLevelPermissionData.class,
                name="ModuleLevelPermission"
        ),
        @com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonSubTypes.Type(
                value = TenantLevelPermissionData.class,
                name="TenantLevelPermission"
        ),
        @com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonSubTypes.Type(
                value = EnvironmentLevelPermissionData.class,
                name="EnvironmentLevelPermission"
        )
})
@com.fasterxml.jackson.annotation.JsonTypeInfo(
        use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME,
        include = com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY,
        property = "permissionType"
)
@com.fasterxml.jackson.annotation.JsonSubTypes({
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(
                value = ModuleLevelPermissionData.class,
                name="ModuleLevelPermission"
        ),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(
                value = TenantLevelPermissionData.class,
                name="TenantLevelPermission"
        ),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(
                value = EnvironmentLevelPermissionData.class,
                name="EnvironmentLevelPermission"
        )
})
public abstract class ModulePermissionData {
    private final String moduleName;

    /**
     * Create a new ModulePermissionData instance.
     *
     * @param moduleName The module name.
     */
    protected ModulePermissionData(String moduleName) {
        this.moduleName = moduleName;
    }

    /**
     * Get the module name.
     *
     * @return The module name.
     */
    public String getModuleName() {
        return moduleName;
    }
}
