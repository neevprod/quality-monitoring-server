package util.t3;

import com.pearson.itautomation.testmaps.models.Testmap;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import play.Logger;
import util.systemvalidation.T3TestMapCache;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.nio.charset.StandardCharsets;

public class T3Testmap {
    private static final int CONNECT_TIME_OUT = 10000;
    private static final int READ_TIME_OUT = 60000;
    private static final String SERVER_URI = "http://publish.service.t3Services.t3.com/";
    private static final String TARGET_URL = "http://10.25.10.107:6060/T3Services/services/PANextServiceBeanPort";

    /**
     * @param accountCode The account code
     * @param adminCode   The admin code
     * @param testCode    The test code
     * @param formCode    The form code
     * @return The instance of {@link T3TestMapResult}
     * @throws T3TestMapException if failed to make SOAP api call to retrieve the version of testmap.
     */
    public static T3TestMapResult getLatestTestMapVersion(final String accountCode,
                                                          final String adminCode,
                                                          final String testCode,
                                                          final String formCode) throws T3TestMapException {
        Logger.info("event= \"Retrieving the name, Id and version of latest testmap from T3\", " +
                        "accountCode= \"{}\", adminCode= \"{}\", " +
                        "testCode= \"{}\", formCode= \"{}\", targetURL= \"{}\"",
                accountCode, adminCode, testCode, formCode, TARGET_URL);
        SOAPMessage requestMessage = getSOAPResponse(accountCode, adminCode, testCode, formCode);

        URL url = constructUrlWithTimeout();
        SOAPMessage responseMessage;
        try {
            SOAPConnectionFactory factory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = factory.createConnection();

            responseMessage = soapConnection.call(requestMessage, url);
        } catch (SOAPException e) {
            Logger.error("event= \"Failed to make SOAP API call\", " +
                            "accountCode= \"{}\", adminCode= \"{}\", " +
                            "testCode= \"{}\", testCode= \"{}\", targetURL= \"{}\"",
                    accountCode, adminCode, testCode, formCode, TARGET_URL);
            String errorMsg = "Failed to make SOAP API call. accountCode='" + accountCode + "', accountCode='" + accountCode + "', " +
                    "adminCode='" + adminCode + "', testCode='" + testCode + "', formCode='" + formCode + "'";
            throw new T3TestMapException.Builder(errorMsg).build();
        }


        return getLatestVersion(responseMessage);
    }

    private static T3TestMapResult getLatestVersion(final SOAPMessage responseMessage) throws T3TestMapException {
        try {
            SOAPBody body = responseMessage.getSOAPBody();
            Document bodyDoc = body.extractContentAsDocument();
            NodeList successNodes = bodyDoc.getElementsByTagName("success");
            if ((successNodes.getLength() <= 0) || (!"true".equalsIgnoreCase(successNodes.item(0).getTextContent()))) {
                String testMapString = getSOAPMessageAsString(responseMessage);
                String errorMessage = "success attribute is missing or not true in soap body! There is an issue with the T3 SOAP API.";
                throw new T3TestMapException.Builder(errorMessage).testMapString(testMapString).build();
            }
            NodeList testMaps = bodyDoc.getElementsByTagName("testmap");
            if (testMaps.getLength() <= 0) {
                String testMapString = getSOAPMessageAsString(responseMessage);
                String errorMessage = "Found empty test map in T3. There is an issue with the T3 SOAP API.";
                throw new T3TestMapException.Builder(errorMessage).testMapString(testMapString).build();
            }
            String id = null;
            String latestVersion = null;
            String name = null;
            boolean testMapExists = false;
            for (int i = 0; i < testMaps.getLength(); i++) {
                Node testMap = testMaps.item(i);
                if (testMap.getNodeType() == Node.ELEMENT_NODE) {
                    Element docElement = (Element) testMap;
                    NodeList formatNodes = docElement.getElementsByTagName("format");
                    String format = formatNodes.item(0).getTextContent();
                    if ("e".equalsIgnoreCase(format)) {
                        latestVersion = getNodeValue(docElement, "latest_version");
                        id = getNodeValue(docElement, "id");
                        name = getNodeValue(docElement, "name");
                        testMapExists = true;
                    }
                }
            }
            if (!testMapExists) {
                String testMapString = getSOAPMessageAsString(responseMessage);
                String errorMessage = "Can not find testMap in T3 with format \"e\". There is an issue with the T3 SOAP API.";
                Logger.error("event=\"{}\", testmap=\"{}\"", errorMessage, testMapString);
                throw new T3TestMapException.Builder(errorMessage).testMapString(testMapString).build();
            }
            boolean containsAllAttributes = checkAttributes(latestVersion, id, name);
            if (!containsAllAttributes) {
                String testMapString = getSOAPMessageAsString(responseMessage);
                String errorMessage = "Required attributes are missing in the test map. " +
                        "Required attributes are: \"latestVersion\", \"id\", \"name\".";
                Logger.error("event=\"{}\", testmap=\"{}\"", errorMessage, testMapString);
                throw new T3TestMapException.Builder(errorMessage)
                        .testMapString(testMapString)
                        .id(id)
                        .name(name)
                        .latestVersion(latestVersion)
                        .build();
            }
            return new T3TestMapResult(id, name, latestVersion);
        } catch (IOException | SOAPException ex) {
            String errorMessage = "Failed to read SOAP message from SOAP body";
            Logger.error("event= \"{}\"", errorMessage, ex);
            String testMapString = "";
            try {
                testMapString = getSOAPMessageAsString(responseMessage);
            } catch (SOAPException | IOException e) {
                Logger.error("event= \"{}\"", errorMessage, e);
            }
            throw new T3TestMapException.Builder(errorMessage).testMapString(testMapString).build();
        }
    }

    private static boolean checkAttributes(final String latestVersion, final String id, final String name) {
        return StringUtils.isNotEmpty(latestVersion) &&
                StringUtils.isNotEmpty(id) &&
                StringUtils.isNotEmpty(name);
    }

    private static String getNodeValue(final Element docElement, final String tagName) {
        String content = null;
        NodeList nodes = docElement.getElementsByTagName(tagName);
        Node node = nodes.item(0);
        if (node != null) {
            content = node.getTextContent();
        }
        return content;
    }

    private static String getSOAPMessageAsString(SOAPMessage responseMessage) throws IOException, SOAPException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        responseMessage.writeTo(out);
        return new String(out.toByteArray());
    }


    private static URL constructUrlWithTimeout() throws T3TestMapException {
        try {
            return new URL(null, TARGET_URL, new URLStreamHandler() {
                @Override
                protected URLConnection openConnection(URL url) throws IOException {
                    URL target = new URL(url.toString());
                    URLConnection connection = target.openConnection();
                    connection.setConnectTimeout(CONNECT_TIME_OUT);
                    connection.setReadTimeout(READ_TIME_OUT);
                    return connection;
                }
            });
        } catch (MalformedURLException ex) {
            Logger.error("event= \"Failed to create URL\", target= \"{}\"", TARGET_URL, ex);
            throw new T3TestMapException.Builder("Failed to create URL for url " + TARGET_URL).build();
        }
    }

    private static SOAPMessage getSOAPResponse(final String accountCode,
                                               final String adminCode,
                                               final String testCode,
                                               final String formCode)
            throws T3TestMapException {
        try {
            MessageFactory factory = MessageFactory.newInstance();
            SOAPMessage message = factory.createMessage();
            SOAPPart part = message.getSOAPPart();

            SOAPEnvelope envelope = part.getEnvelope();
            envelope.addNamespaceDeclaration("ns10", SERVER_URI);
            SOAPBody body = envelope.getBody();
            SOAPElement bodyElement = body.addChildElement("getTestMapsByFormCodeTestCode", "ns10");

            SOAPElement request = bodyElement.addChildElement("request");
            request.addChildElement("acctCode").addTextNode(accountCode);
            request.addChildElement("adminCode").addTextNode(adminCode);
            request.addChildElement("testCode").addTextNode(testCode);
            request.addChildElement("formCode").addTextNode(formCode);

            addHeaders(message);
            message.saveChanges();
            return message;
        } catch (SOAPException ex) {
            Logger.error("event= \"Failed to call getTestMapsByFormCodeTestCode T3 API. " +
                            "\", accountCode= \"{}\", adminCode= \"{}\", testCode= \"{}\", " +
                            "formCode= \"{}\", serverURI= \"{}\"",
                    accountCode, adminCode, testCode, formCode, SERVER_URI, ex);
            String errorMessage = "Failed to call getTestMapsByFormCodeTestCode T3 API. " +
                    "accountCode= " + accountCode + ", adminCode= " + adminCode
                    + ", testCode= " + testCode + ", formCode= " + formCode;
            throw new T3TestMapException.Builder(errorMessage).build();
        }
    }

    private static void addHeaders(final SOAPMessage message) {
        MimeHeaders header = message.getMimeHeaders();
        header.addHeader("SOAPAction", "");
        header.addHeader("style", "rpc");
    }

    /**
     * Retrieves the XML Testmap from T3 and after deserialization returns the instance of {@link Testmap}. It returns Null if failed to download Testmap from T3.
     *
     * @param testmapId   The testmap Id to be downloaded
     * @param version     The version of testmap to be downloaded
     * @param testmapName The name of testmap to be downloaded
     * @return The instance of Testmap. Null if failed to download Testmap from T3.
     * @throws T3TestMapException if failed to download Testmap from T3
     */

    public static Testmap getTestMap(String testmapId, String version, String testmapName)
            throws T3TestMapException {
        try {
            SOAPMessage testmapXML = getTestmapXML(testmapId, version);

            OutputStream os = new ByteArrayOutputStream();
            testmapXML.writeTo(os);
            String fullTestMap = os.toString();

            int soapBodyStartIdx = fullTestMap.toLowerCase().indexOf("<soap:body>");
            int soapBodyEndIdx = fullTestMap.toLowerCase().indexOf("</soap:body>");

            if (soapBodyStartIdx > 0) {
                String soapBody = fullTestMap.substring(soapBodyStartIdx + 11, soapBodyEndIdx);

                InputStream is = new ByteArrayInputStream(soapBody.getBytes(StandardCharsets.UTF_8));
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setNamespaceAware(true);
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document soapBodyDoc = builder.parse(is);
                is.close();
                soapBodyDoc = T3TestMapCache.updateNamespace(soapBodyDoc);
                Testmap t3TestMap = new Testmap(soapBodyDoc);
                t3TestMap.setName(testmapName);
                t3TestMap.setId(testmapId);
                t3TestMap.setVersion(version);
                try {
                    XPathFactory xpathFactory = XPathFactory.newInstance();
                    XPath xpath = xpathFactory.newXPath();

                    XPathExpression format = xpath.compile(
                            "//testmaps/testmap[./name[contains(.,'" + testmapName + "')]]/format");

                    XPathExpression grade = xpath.compile(
                            "//testmaps/testmap[./name[contains(.,'" + testmapName + "')]]/grade");

                    XPathExpression status = xpath.compile(
                            "//testmaps/testmap[./name[contains(.,'" + testmapName.substring(0, testmapName.length() - 2) + "')]]/status");

                    XPathExpression subject = xpath.compile(
                            "//testmaps/testmap[./name[contains(.,'" + testmapName.substring(0, testmapName.length() - 2) + "')]]/subject");

                    t3TestMap.setFormat(format.evaluate(soapBodyDoc));
                    t3TestMap.setGrade(grade.evaluate(soapBodyDoc));
                    t3TestMap.setStatus(status.evaluate(soapBodyDoc));
                    t3TestMap.setSubject(subject.evaluate(soapBodyDoc));

                } catch (XPathExpressionException e) {
                    throw new T3TestMapException.Builder("Xpath error loading additional test map information" + e.getMessage()).build();
                }
                return t3TestMap;

            }
        } catch (SOAPException | IOException | ParserConfigurationException | SAXException | JAXBException | TransformerException e) {
            throw new T3TestMapException.Builder("Failed to download Testmap from T3. : " + e.getMessage())
                    .name(testmapName)
                    .id(testmapId).build();
        }
        return null;
    }

    /**
     * @param testmapId The testmap Id to be downloaded
     * @param version   The version of testmap to be downloaded
     * @return The instance of SOAPMessage for getTestmapXML by testmap Id and version SOAP API. Null if there is any error.
     * @throws T3TestMapException if there was an error creating the <code>SOAPConnectionFactory</code>
     */
    private static SOAPMessage getTestmapXML(String testmapId, String version) throws T3TestMapException {
        SOAPMessage soapResponse;
        try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            soapResponse = soapConnection.call(getTestmapXMLRequest(testmapId, version), constructUrlWithTimeout());
        } catch (SOAPException ex) {
            throw new T3TestMapException.Builder("Unable to create SOAP connection factory: " + ex.getMessage()).build();
        }
        return soapResponse;
    }

    private static SOAPMessage getTestmapXMLRequest(String testmapId, String version) throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("ns10", SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("getTestMap", "ns10");

        SOAPElement soapBodyReq = soapBodyElem.addChildElement("request");
        SOAPElement soapBodyElem1 = soapBodyReq.addChildElement("testmap_id");
        soapBodyElem1.addTextNode(testmapId);
        SOAPElement soapBodyElem2 = soapBodyReq.addChildElement("version");
        soapBodyElem2.addTextNode(version);

        javax.xml.soap.MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", "");
        headers.addHeader("style", "rpc");

        soapMessage.saveChanges();

        return soapMessage;
    }

    /**
     * This is a wrapper class for the result of the SOAP api call that retrieves the latest version of Testmap from T3 and
     * contains the following information:
     * <li>Testmap ID</li>
     * <li>Testmap Name</li>
     * <li>Latest Testmap Version</li>
     */
    public static class T3TestMapResult {
        private final String id;
        private final String name;
        private final String version;

        T3TestMapResult(final String id, final String name, final String version) {
            this.id = id;
            this.name = name;
            this.version = version;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getVersion() {
            return version;
        }

        @Override
        public String toString() {
            return "T3TestMapResult{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", version='" + version + '\'' +
                    '}';
        }
    }
}
