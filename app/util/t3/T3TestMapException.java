package util.t3;

import com.google.gson.JsonObject;

public class T3TestMapException extends Exception {
    private static final long serialVersionUID = -21290567583867365L;
    private final String errorMessage;
    private final String testMapString;
    private final String id;
    private final String latestVersion;
    private final String name;

    private T3TestMapException(Builder builder) {
        super(builder.errorMessage);
        this.errorMessage = builder.errorMessage;
        this.testMapString = builder.testMapString;
        this.id = builder.id;
        this.latestVersion = builder.latestVersion;
        this.name = builder.name;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getTestMapString() {
        return testMapString;
    }

    public String getId() {
        return id;
    }

    public String getLatestVersion() {
        return latestVersion;
    }

    public String getName() {
        return name;
    }

    @Override
    public String getMessage() {
        JsonObject result = new JsonObject();
        if (id != null) {
            result.addProperty("id", id);
        }
        if (name != null) {
            result.addProperty("name", name);
        }
        if (testMapString != null) {
            result.addProperty("testMap", testMapString);
        }
        if (latestVersion != null) {
            result.addProperty("latestVersion", latestVersion);
        }
        if (errorMessage != null) {
            result.addProperty("message", errorMessage);
        }
        return result.toString();
    }

    public static class Builder {
        // Required parameter
        private final String errorMessage;

        // Optional parameters
        private String testMapString;
        private String id;
        private String latestVersion;
        private String name;

        public Builder(final String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public Builder testMapString(final String testMapString) {
            this.testMapString = testMapString;
            return this;
        }

        public Builder id(final String id) {
            this.id = id;
            return this;
        }

        public Builder latestVersion(String latestVersion) {
            this.latestVersion = latestVersion;
            return this;
        }

        public Builder name(final String name) {
            this.name = name;
            return this;
        }

        public T3TestMapException build() {
            return new T3TestMapException(this);
        }
    }
}
