package util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.ApplicationModule;
import models.PermissionSettingsValue;
import models.UserPermissions;
import play.db.jpa.JPAApi;
import play.libs.Json;
import services.PermissionSettingsValueService;

import java.util.*;
import java.util.stream.Collectors;

public class UserPermissionData {
    private final boolean admin;
    private final List<ModulePermissionData> modulePermissions;

    /*
    Note: This annotations is repeated here because Jackson is repackaged inside of the JWT library we are using. We
    need to have one version for the JWT library since it does the serializing, and another version for the web app
    code, since it does the deserializing. This is unfortunate, and hopefully the JWT library will fix this issue in
    the future.
     */
    @com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnore
    @com.fasterxml.jackson.annotation.JsonIgnore
    private JsonNode json;

    /**
     * Create a new UserPermissionData instance.
     */
    public UserPermissionData() {
        admin = false;
        modulePermissions = new ArrayList<>();
    }

    /**
     * Create a new UserPermissionData instance containing the given user permissions.
     *
     * @param admin Whether the user is an admin user.
     * @param userPermissions The user permissions.
     */
    public UserPermissionData(boolean admin, List<UserPermissions> userPermissions) {
        this.admin = admin;
        this.modulePermissions = new ArrayList<>();

        Map<ApplicationModule, List<UserPermissions>> partitionedPermissions = new HashMap<>();
        if (userPermissions != null && !userPermissions.isEmpty()) {
            partitionedPermissions = userPermissions.stream()
                    .collect(Collectors.groupingBy(UserPermissions::getApplicationModule));

            for (Map.Entry<ApplicationModule, List<UserPermissions>> entry : partitionedPermissions.entrySet()) {
                ApplicationModule module = entry.getKey();
                switch (module.getPermissionType().getPermissionTypeName()) {
                    case "ModuleLevelPermission": {
                        this.modulePermissions.add(
                                new ModuleLevelPermissionData(module.getApplicationModuleName(),
                                        entry.getValue().get(0)));
                        break;
                    }
                    case "TenantLevelPermission": {
                        this.modulePermissions.add(
                                new TenantLevelPermissionData(module.getApplicationModuleName(), entry.getValue()));
                        break;
                    }
                    case "EnvironmentLevelPermission": {
                        this.modulePermissions.add(
                                new EnvironmentLevelPermissionData(module.getApplicationModuleName(),
                                        entry.getValue()));
                        break;
                    }
                }
            }
        }

        this.json = generateJson(partitionedPermissions);
    }

    /**
     * Get the module permissions.
     *
     * @return The module permissions.
     */
    public List<ModulePermissionData> getModulePermissions() {
        return modulePermissions;
    }

    /**
     * Get whether the user is an admin user.
     *
     * @return Whether the user is an admin user.
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Get the JSON representing the user permissions.
     *
     * @return The JSON.
     */
    public JsonNode getJson() {
        return json;
    }

    /**
     * Generate JSON that represents the user permission data.
     *
     * @param partitionedPermissions A map of user permissions partitioned by module.
     * @return The user permission JSON.
     */
    private JsonNode generateJson(Map<ApplicationModule, List<UserPermissions>> partitionedPermissions) {
        ObjectNode userPermissionsJson = Json.newObject();
        userPermissionsJson.put("admin", admin);

        if (!admin) {
            ArrayNode modulePermissionsJson = userPermissionsJson.putArray("modulePermissions");

            for (Map.Entry<ApplicationModule, List<UserPermissions>> entry : partitionedPermissions.entrySet()) {
                ApplicationModule module = entry.getKey();
                switch (module.getPermissionType().getPermissionTypeName()) {
                    case "ModuleLevelPermission": {
                        addModuleLevelJson(modulePermissionsJson, module, entry.getValue().get(0));
                        break;
                    }
                    case "TenantLevelPermission": {
                        addTenantLevelJson(modulePermissionsJson, module, entry.getValue());
                        break;
                    }
                    case "EnvironmentLevelPermission": {
                        addEnvironmentLevelJson(modulePermissionsJson, module, entry.getValue());
                        break;
                    }
                }
            }
        }

        return userPermissionsJson;
    }

    /**
     * Add module level permission JSON to the given JSON array.
     *
     * @param modulePermissionsJson The JSON array to add to.
     * @param module The application module.
     * @param moduleLevelPermissions The module level permissions to add.
     */
    private void addModuleLevelJson(ArrayNode modulePermissionsJson, ApplicationModule module,
                                    UserPermissions moduleLevelPermissions) {
        ObjectNode moduleLevelPermissionsJson = modulePermissionsJson.addObject();

        moduleLevelPermissionsJson.put("permissionType", module.getPermissionType().getPermissionTypeName());
        moduleLevelPermissionsJson.put("moduleName", module.getApplicationModuleName());

        ArrayNode permissionsJson = moduleLevelPermissionsJson.putArray("permissions");
        addPermissionsJson(permissionsJson,
                moduleLevelPermissions.getPermissionSettings().getPermissionSettingsValues());
    }

    /**
     * Add tenant level permission JSON to the given JSON array.
     *
     * @param modulePermissionsJson The JSON array to add to.
     * @param module The application module.
     * @param tenantLevelPermissions The tenant level permissions to add.
     */
    private void addTenantLevelJson(ArrayNode modulePermissionsJson, ApplicationModule module,
                                    List<UserPermissions> tenantLevelPermissions) {
        ObjectNode tenantLevelPermissionsJson = modulePermissionsJson.addObject();

        tenantLevelPermissionsJson.put("permissionType", module.getPermissionType().getPermissionTypeName());
        tenantLevelPermissionsJson.put("moduleName", module.getApplicationModuleName());

        ArrayNode previewerPermissionsJson = tenantLevelPermissionsJson.putArray("previewerPermissions");
        Map<Integer, List<UserPermissions>> partitionedPermissions = tenantLevelPermissions.stream()
                .collect(Collectors.groupingBy(UserPermissions::getPreviewerId));
        for (Map.Entry<Integer, List<UserPermissions>> entry : partitionedPermissions.entrySet()) {
            ObjectNode permissionsJsonForSinglePreviewer = previewerPermissionsJson.addObject();
            permissionsJsonForSinglePreviewer.put("previewerId", entry.getKey());

            ArrayNode tenantPermissionsJson = permissionsJsonForSinglePreviewer.putArray("tenantPermissions");
            for (UserPermissions userPermissions : entry.getValue()) {
                ObjectNode permissionsJsonForSingleTenant = tenantPermissionsJson.addObject();
                if (userPermissions.getIsWildcard()) {
                    permissionsJsonForSingleTenant.put("tenantId", "*");
                } else {
                    permissionsJsonForSingleTenant.put("tenantId", userPermissions.getTenantId());
                }

                ArrayNode permissionsJson = permissionsJsonForSingleTenant.putArray("permissions");
                addPermissionsJson(permissionsJson,
                        userPermissions.getPermissionSettings().getPermissionSettingsValues());
            }
        }
    }

    /**
     * Add environment level permission JSON to the given JSON array.
     *
     * @param modulePermissionsJson The JSON array to add to.
     * @param module The application module.
     * @param environmentLevelPermissions The environment level permissions to add.
     */
    private void addEnvironmentLevelJson(ArrayNode modulePermissionsJson, ApplicationModule module,
                                         List<UserPermissions> environmentLevelPermissions) {
        ObjectNode environmentLevelPermissionsJson = modulePermissionsJson.addObject();

        environmentLevelPermissionsJson.put("permissionType", module.getPermissionType().getPermissionTypeName());
        environmentLevelPermissionsJson.put("moduleName", module.getApplicationModuleName());

        ArrayNode environmentPermissionsJson = environmentLevelPermissionsJson.putArray("environmentPermissions");
        for (UserPermissions userPermissions : environmentLevelPermissions) {
            ObjectNode permissionsJsonForSingleEnvironment = environmentPermissionsJson.addObject();
            if (userPermissions.getIsWildcard()) {
                permissionsJsonForSingleEnvironment.put("environmentId", "*");
            } else {
                permissionsJsonForSingleEnvironment.put("environmentId", userPermissions.getEnvironmentId());
            }

            ArrayNode permissionsJson = permissionsJsonForSingleEnvironment.putArray("permissions");
            addPermissionsJson(permissionsJson,
                    userPermissions.getPermissionSettings().getPermissionSettingsValues());
        }
    }

    /**
     * Add JSON for the given permissions to the given ArrayNode.
     *
     * @param permissionsJson The ArrayNode to add the permissions to.
     * @param permissionSettingsValues The permissions.
     */
    private void addPermissionsJson(ArrayNode permissionsJson, Set<PermissionSettingsValue> permissionSettingsValues) {
        for (PermissionSettingsValue settingsValue: permissionSettingsValues) {
            ObjectNode featurePermissions = permissionsJson.addObject();
            featurePermissions.put("name", settingsValue.getPermissionOption().getName());
            featurePermissions.put("createAccess", settingsValue.getCreateAccess());
            featurePermissions.put("readAccess", settingsValue.getReadAccess());
            featurePermissions.put("updateAccess", settingsValue.getUpdateAccess());
            featurePermissions.put("deleteAccess", settingsValue.getDeleteAccess());
        }
    }

    /**
     * Get module level permissions for the given module. Returns an empty Optional if the module does not exist, if
     * the user does not have any permissions for the module, or if the permissions of the module are not at the module
     * level.
     *
     * @param moduleName The module name.
     * @param jpaApi An instance of JPAApi.
     * @param permissionSettingsValueService An instance of PermissionSettingsValueService.
     * @return An Optional containing module level permission data.
     */
    public Optional<ModuleLevelPermissions> getModuleLevelPermissions(String moduleName, JPAApi jpaApi,
                                                    PermissionSettingsValueService permissionSettingsValueService) {
        Optional<ModuleLevelPermissionData> permissionDataOptional = modulePermissions.stream()
                .filter(mpd -> mpd.getModuleName().equals(moduleName))
                .filter(mpd -> mpd instanceof ModuleLevelPermissionData)
                .map(mpd -> (ModuleLevelPermissionData) mpd)
                .findFirst();

        if (permissionDataOptional.isPresent()) {
            return Optional.of(
                    new ModuleLevelPermissions(permissionDataOptional.get(), admin, jpaApi,
                            permissionSettingsValueService)
            );
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get tenant level permissions for the given module. Returns an empty Optional if the module does not exist, if
     * the user does not have any permissions for the module, or if the permissions of the module are not at the tenant
     * level.
     *
     * @param moduleName The module name.
     * @param jpaApi An instance of JPAApi.
     * @param permissionSettingsValueService An instance of PermissionSettingsValueService.
     * @return An Optional containing tenant level permission data.
     */
    public Optional<TenantLevelPermissions> getTenantLevelPermissions(String moduleName, JPAApi jpaApi,
                                                      PermissionSettingsValueService permissionSettingsValueService) {
        Optional<TenantLevelPermissionData> permissionDataOptional = modulePermissions.stream()
                .filter(mpd -> mpd.getModuleName().equals(moduleName))
                .filter(mpd -> mpd instanceof TenantLevelPermissionData)
                .map(mpd -> (TenantLevelPermissionData) mpd)
                .findFirst();

        if (permissionDataOptional.isPresent()) {
            return Optional.of(
                    new TenantLevelPermissions(permissionDataOptional.get(), admin, jpaApi,
                            permissionSettingsValueService)
            );
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get environment level permissions for the given module. Returns an empty Optional if the module does not exist,
     * if the user does not have any permissions for the module, or if the permissions of the module are not at the
     * environment level.
     *
     * @param moduleName The module name.
     * @param jpaApi An instance of JPAApi.
     * @param permissionSettingsValueService An instance of PermissionSettingsValueService.
     * @return An Optional containing environment level permission data.
     */
    public Optional<EnvironmentLevelPermissions> getEnvironmentLevelPermissions(String moduleName, JPAApi jpaApi,
                                                   PermissionSettingsValueService permissionSettingsValueService) {
        Optional<EnvironmentLevelPermissionData> permissionDataOptional = modulePermissions.stream()
                .filter(mpd -> mpd.getModuleName().equals(moduleName))
                .filter(mpd -> mpd instanceof EnvironmentLevelPermissionData)
                .map(mpd -> (EnvironmentLevelPermissionData) mpd)
                .findFirst();

        if (permissionDataOptional.isPresent()) {
            return Optional.of(
                    new EnvironmentLevelPermissions(permissionDataOptional.get(), admin, jpaApi,
                            permissionSettingsValueService)
            );
        } else {
            return Optional.empty();
        }
    }
}
