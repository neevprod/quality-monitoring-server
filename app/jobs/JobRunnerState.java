package jobs;

import java.util.*;

public class JobRunnerState {
    private Map<String, Queue<JobStarter>> jobQueues;
    private Set<String> queuedJobs;
    private Map<String, JobStarter> runningJobs;

    JobRunnerState() {
        jobQueues = new HashMap<>();
        queuedJobs = new HashSet<>();
        runningJobs = new HashMap<>();
    }

    /**
     * Add a job to the collection of running jobs.
     *
     * @param jobStarter The job to add.
     */
    public void addRunningJob(JobStarter jobStarter) {
        runningJobs.put(jobStarter.getJobId(), jobStarter);
    }

    /**
     * Remove a job from the collection of running jobs.
     *
     * @param jobId The ID of the job to remove.
     */
    public void removeRunningJob(String jobId) {
        runningJobs.remove(jobId);
    }

    /**
     * Get a running job by its ID.
     *
     * @param jobId The ID of the job to get.
     * @return The running job.
     */
    public Optional<JobStarter> getRunningJob(String jobId) {
        JobStarter jobStarter = runningJobs.get(jobId);
        if (jobStarter != null) {
            return Optional.of(jobStarter);
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get the number of currently running jobs for the specified job type.
     *
     * @param jobType The type of job to check.
     * @return The number of currently running jobs for the specified job type.
     */
    public long getRunningJobCount(String jobType) {
        return runningJobs.values().stream()
                .filter(j -> j.getJobType().equals(jobType))
                .count();
    }

    /**
     * Get the set of IDs representing the currently running jobs.
     *
     * @return The set of IDs representing the currently running jobs.
     */
    public Set<String> getRunningJobIds() {
        return runningJobs.keySet();
    }

    /**
     * Add a job to the queue. There is a separate queue for each job type.
     *
     * @param jobStarter The job to add to the queue.
     */
    public void queueJob(JobStarter jobStarter) {
        String jobType = jobStarter.getJobType();

        if (!jobQueues.containsKey(jobType)) {
            jobQueues.put(jobType, new PriorityQueue<>());
        }

        jobQueues.get(jobType).add(jobStarter);
        queuedJobs.add(jobStarter.getJobId());
    }

    /**
     * Remove a job from the queue of the specified job type, and get the removed job. If no jobs of the given type
     * are queued, Optional.empty will be returned.
     *
     * @param jobType The type of job to dequeue.
     * @return The dequeued job.
     */
    public Optional<JobStarter> removeQueuedJob(String jobType) {
        if (jobQueues.get(jobType) != null && !jobQueues.get(jobType).isEmpty()) {
            JobStarter queuedJobStarter = jobQueues.get(jobType).remove();
            queuedJobs.remove(queuedJobStarter.getJobId());
            return Optional.of(queuedJobStarter);
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get the number of currently queued jobs for the specified job type.
     *
     * @param jobType The type of job to check.
     * @return The number of currently queued jobs for the specified job type.
     */
    public int getQueuedJobCount(String jobType) {
        if (jobQueues.get(jobType) != null) {
            return jobQueues.get(jobType).size();
        } else {
            return 0;
        }
    }

    /**
     * Get the set of IDs representing the currently queued jobs.
     *
     * @return The set of IDs representing the currently queued jobs.
     */
    public Set<String> getQueuedJobIds() {
        return queuedJobs;
    }

    /**
     * Check whether a specific job is either queued or running.
     *
     * @param jobId The ID of the job to check.
     * @return Whether the job is currently queued or running.
     */
    public boolean jobIsQueuedOrRunning(String jobId) {
        return queuedJobs.contains(jobId) || runningJobs.containsKey(jobId);
    }
}
