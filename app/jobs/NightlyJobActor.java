package jobs;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;
import jobs.qtivalidation.QtiScoringValidationJobStarter;
import models.ApplicationSetting;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.akka.InjectedActorSupport;
import services.ApplicationSettingService;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

public class NightlyJobActor extends UntypedAbstractActor implements InjectedActorSupport {
    private static String NIGHTLY_JOB_ENABLED_SETTING = "nightly_job_enabled";

    private final JPAApi jpaApi;
    private final ApplicationSettingService applicationSettingService;
    private final QtiScoringValidationJobStarter.Factory qtiScvJobStarterFactory;

    private ActorRef sender;
    private Map<JobStarter, Set<String>> jobsToRun;
    private String jobId;

    public static final class RunMessage {
        private final String jobId;

        public RunMessage(String jobId) {
            this.jobId = jobId;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        public String getJobId() {
            return jobId;
        }
    }

    public interface Factory {
        Actor create();
    }

    @Inject
    NightlyJobActor(JPAApi jpaApi, ApplicationSettingService applicationSettingService,
            QtiScoringValidationJobStarter.Factory qtiScvJobStarterFactory) {
        this.jpaApi = jpaApi;
        this.applicationSettingService = applicationSettingService;
        this.qtiScvJobStarterFactory = qtiScvJobStarterFactory;
    }

    /**
     * Process an incoming message.
     *
     * @param message The message sent.
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof RunMessage) {
            jobId = ((RunMessage) message).getJobId();
            processRunMessage();
        } else if (message instanceof JobProtocol.JobFinishMessage) {
            String finishedJobId = ((JobProtocol.JobFinishMessage) message).getJobId();
            processFinishMessage(finishedJobId);
        }
    }

    /**
     * Start a run of the nightly job.
     */
    private void processRunMessage() {
        sender = getSender();

        if (nightlyJobIsEnabled()) {
            initializeJobsList();
            getJobsAvailableToRun().forEach(this::startJob);
        } else {
            Logger.info("event=\"Did not execute nightly job, since it is disabled.\"");
            finishNightlyJob();
        }
    }

    /**
     * Check to see if the nightly job is enabled.
     *
     * @return A boolean indicating whether the nightly job is enabled.
     */
    private boolean nightlyJobIsEnabled() {
        return jpaApi.withTransaction(() -> {
            boolean enabled = false;

            Optional<ApplicationSetting> applicationSettingOptional = applicationSettingService
                    .findByApplicationSettingName(NIGHTLY_JOB_ENABLED_SETTING);
            if (applicationSettingOptional.isPresent()) {
                try {
                    int value = Integer.parseInt(applicationSettingOptional.get().getApplicationSettingValue());
                    if (value == 1) {
                        enabled = true;
                    }
                } catch (NumberFormatException e) {
                    Logger.error("event=\"Application setting value is in the wrong format.\", "
                            + "applicationSettingName=\"{}\"", NIGHTLY_JOB_ENABLED_SETTING);
                }
            }

            return enabled;
        });
    }

    /**
     * Update the jobs to run, check if the nightly job is finished, and start any jobs that now have their
     * dependencies met.
     *
     * @param finishedJobId The ID of the job that just finished.
     */
    private void processFinishMessage(String finishedJobId) {
        Iterator<JobStarter> iterator = jobsToRun.keySet().iterator();
        while (iterator.hasNext()) {
            JobStarter jobStarter = iterator.next();
            if (jobStarter.getJobId().equals(finishedJobId)) {
                iterator.remove();
            }
        }

        if (!jobsToRun.isEmpty()) {
            jobsToRun.values().stream()
                    .forEach(dependencies -> dependencies.remove(finishedJobId));
            getJobsAvailableToRun().forEach(this::startJob);
        } else {
            finishNightlyJob();
        }
    }

    /**
     * Set up the collection of jobs to run, along with their dependencies.
     */
    private void initializeJobsList() {
        jobsToRun = new HashMap<>();

        Set<String> qtiScoringValidationDependencies = new HashSet<>();
        JobStarter qtiScoringValidationJobStarter = qtiScvJobStarterFactory.create();
        qtiScoringValidationJobStarter.setParentJobId(jobId);
        jobsToRun.put(qtiScoringValidationJobStarter, qtiScoringValidationDependencies);
    }

    /**
     * Get a set containing the jobs that have no dependencies remaining and are ready to run.
     *
     * @return The jobs that are ready to run.
     */
    private Set<JobStarter> getJobsAvailableToRun() {
        return jobsToRun.entrySet().stream()
                .filter(entry -> entry.getValue().isEmpty())
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    /**
     * Start a job using the given job starter.
     *
     * @param jobStarter The job starter.
     */
    private void startJob(JobStarter jobStarter) {
        ActorRef jobActor = injectedChild(jobStarter::createJobActor,
                jobStarter.getJobId() + "_nightly_" + System.currentTimeMillis());
        jobStarter.startJob(jobActor, getSelf());
    }

    /**
     * Send a finish message back to the sender and shut down the actor.
     */
    private void finishNightlyJob() {
        JobProtocol.JobFinishMessage finishMessage = new JobProtocol.JobFinishMessage(jobId, true);
        if (sender != null) {
            sender.tell(finishMessage, getSelf());
        }
        getContext().stop(getSelf());
    }

}
