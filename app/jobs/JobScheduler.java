package jobs;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import models.ApplicationSetting;
import play.Logger;
import play.db.jpa.JPAApi;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import services.ApplicationSettingService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.time.Clock;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Singleton
public class JobScheduler {
    private static String NIGHTLY_JOB_START_HOUR_SETTING = "nightly_job_start_hour";
    private static String NIGHTLY_JOB_START_MINUTE_SETTING = "nightly_job_start_minute";

    private final Config configuration;
    private final ActorSystem actorSystem;
    private final ActorRef jobRunnerActor;
    private final NightlyJobStarter nightlyJobStarter;
    private final UpdateTenantsJobStarter updateTenantsJobStarter;
    private final Clock clock;
    private final JPAApi jpaApi;
    private final ApplicationSettingService applicationSettingService;

    @Inject
    JobScheduler(Config configuration, ActorSystem actorSystem, @Named("jobRunnerActor") ActorRef jobRunnerActor,
                 NightlyJobStarter nightlyJobStarter, UpdateTenantsJobStarter updateTenantsJobStarter, Clock clock,
                 JPAApi jpaApi, ApplicationSettingService applicationSettingService) {
        this.configuration = configuration;
        this.actorSystem = actorSystem;
        this.jobRunnerActor = jobRunnerActor;
        this.nightlyJobStarter = nightlyJobStarter;
        this.updateTenantsJobStarter = updateTenantsJobStarter;
        this.clock = clock;
        this.jpaApi = jpaApi;
        this.applicationSettingService = applicationSettingService;

//        scheduleNightlyJob();
//        scheduleUpdateTenantsJob();
        Logger.info("event=\"Scheduled jobs have been started.\"");
    }

    /**
     * Schedule the nightly job.
     */
    private void scheduleNightlyJob() {
        int nightlyJobStartHour = getApplicationSettingValue(NIGHTLY_JOB_START_HOUR_SETTING);
        int nightlyJobStartMinute = getApplicationSettingValue(NIGHTLY_JOB_START_MINUTE_SETTING);
        ZoneId developerTimeZone = ZoneId.of(configuration.getString("application.developerTimeZone"));
        long millisecondsUntilNextRun = getMillisecondsUntilTime(nightlyJobStartHour, nightlyJobStartMinute,
                developerTimeZone, clock);

        FiniteDuration initialDelay = Duration.create(millisecondsUntilNextRun, TimeUnit.MILLISECONDS);
        FiniteDuration interval = Duration.create(1, TimeUnit.DAYS);

        actorSystem.scheduler().schedule(
                initialDelay,
                interval,
                jobRunnerActor,
                new JobProtocol.JobStartMessage(nightlyJobStarter),
                actorSystem.dispatcher(),
                null
        );

        Logger.info("event=\"Job scheduled.\", jobType=\"nightlyJob\", initialDelay=\"{} h\", interval=\"{} h\"",
                initialDelay.toMillis() / 3600000f, interval.toMillis() / 3600000f);
    }

    /**
     * Schedule the update tenants job.
     */
    private void scheduleUpdateTenantsJob() {
        FiniteDuration initialDelay = Duration.create(0, TimeUnit.MILLISECONDS);
        FiniteDuration interval = Duration.create(1, TimeUnit.HOURS);

        actorSystem.scheduler().schedule(
                initialDelay,
                interval,
                jobRunnerActor,
                new JobProtocol.JobStartMessage(updateTenantsJobStarter),
                actorSystem.dispatcher(),
                null
        );

        Logger.info("event=\"Job scheduled.\", jobType=\"updateTenants\", initialDelay=\"{} h\", interval=\"{} h\"",
                initialDelay.toMillis() / 3600000f, interval.toMillis() / 3600000f);
    }

    /**
     * Get the application setting value with the given name.
     *
     * @param applicationSettingName The application setting name.
     * @return The application setting value.
     */
    private int getApplicationSettingValue(String applicationSettingName) {
        return jpaApi.withTransaction(() -> {
            Optional<ApplicationSetting> applicationSettingOptional = applicationSettingService
                    .findByApplicationSettingName(applicationSettingName);
            if (applicationSettingOptional.isPresent()) {
                return Integer.parseInt(applicationSettingOptional.get().getApplicationSettingValue());
            } else {
                throw new RuntimeException("Nightly job start time is not defined properly.");
            }
        });
    }

    /**
     * Get the number of milliseconds until the given hour and minute occurs in the given time zone.
     *
     * @param hour     The hour (0 to 23).
     * @param minute   The minute (0 to 59).
     * @param timeZone The time zone.
     * @return The number of milliseconds until the time occurs.
     */
    private long getMillisecondsUntilTime(int hour, int minute, ZoneId timeZone, Clock clock) {
        ZonedDateTime targetDateTimeInDeveloperTimeZone = ZonedDateTime.ofInstant(clock.instant(), timeZone)
                .withHour(hour)
                .withMinute(minute)
                .withSecond(0)
                .withNano(0);

        ZonedDateTime targetDateTime = targetDateTimeInDeveloperTimeZone
                .withZoneSameInstant(clock.getZone());

        if (targetDateTime.isBefore(ZonedDateTime.ofInstant(clock.instant(), clock.getZone()))) {
            targetDateTime = targetDateTime.plusDays(1L);
        }

        return java.time.Duration.between(
                ZonedDateTime.ofInstant(clock.instant(), clock.getZone()),
                targetDateTime
        ).toMillis();
    }

}
