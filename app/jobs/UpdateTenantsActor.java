package jobs;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;
import com.typesafe.config.Config;
import models.qtivalidation.Previewer;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.akka.InjectedActorSupport;
import services.qtivalidation.PreviewerService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class UpdateTenantsActor extends UntypedAbstractActor implements InjectedActorSupport {
    public static final String RUN_MESSAGE = "Run";
    public static final String FINISHED_MESSAGE = "Finished";
    public static final String FAILED_MESSAGE = "Failed";

    private final JPAApi jpaApi;
    private final PreviewerService previewerService;
    private final UpdateTenantsChildActor.Factory updateTenantsChildActorFactory;
    private final int numberOfChildren;

    ActorRef sender;
    private int childrenRunning;

    public interface Factory {
        Actor create();
    }

    @Inject
    UpdateTenantsActor(JPAApi jpaApi, PreviewerService previewerService,
                       UpdateTenantsChildActor.Factory updateTenantsChildActorFactory, Config configuration) {
        this.jpaApi = jpaApi;
        this.previewerService = previewerService;
        this.updateTenantsChildActorFactory = updateTenantsChildActorFactory;
        this.numberOfChildren = configuration.getInt("application.jobs.updateTenants.numberOfChildWorkers");
    }

    public void onReceive(Object message) throws Exception {
        if (message instanceof String) {
            if (RUN_MESSAGE.equals(message)) {
                sender = getSender();
                startChildren();
            } else if (FINISHED_MESSAGE.equals(message)) {
                childrenRunning--;
                if (childrenRunning == 0) {
                    sender.tell(new JobProtocol.JobFinishMessage("updateTenants", true), getSelf());
                    getContext().stop(getSelf());
                }
            } else if (FAILED_MESSAGE.equals(message)) {
                Logger.error("event=\"Update tenants job failed.\"");

                sender.tell(new JobProtocol.JobFinishMessage("updateTenants", false,
                        JobProtocol.getJobResultJson("An unexpected error occurred while running the job.")),
                        getSelf());
                getContext().stop(getSelf());
            }
        }
    }

    /**
     * Divide the work up between the child actors.
     */
    private void startChildren() {
        childrenRunning = numberOfChildren;
        long currentTime = System.currentTimeMillis();

        Set<Integer> previewerIds;
        try {
            previewerIds = jpaApi.withTransaction((Supplier<List<Previewer>>) previewerService::findAll)
                    .stream()
                    .map(Previewer::getPreviewerId)
                    .collect(Collectors.toSet());
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }

        for (int i = 0; i < numberOfChildren; i++) {
            ActorRef updateTenantsActorChild = injectedChild(updateTenantsChildActorFactory::create,
                    "updateTenantsChildActor_" + currentTime + "_" + i);
            updateTenantsActorChild.tell(new UpdateTenantsChildActor.UpdateMessage(getChildSet(previewerIds, i)), getSelf());
        }
    }

    /**
     * Given a set of previewer IDs and the index of a child actor, return the subset of previewer IDs that the child
     * actor should be responsible for.
     *
     * @param previewerIds the set of previewer IDs
     * @param childIndex the index of the child actor to get the set for
     * @return a subset of previewer IDs
     */
    private Set<Integer> getChildSet(Set<Integer> previewerIds, int childIndex) {
        List<Integer> previewerIdList = new ArrayList<>(previewerIds);
        int elementsPerChild = previewerIdList.size() / numberOfChildren;
        int remainder = previewerIdList.size() % numberOfChildren;

        int start = childIndex * elementsPerChild;
        start += Math.min(childIndex, remainder);
        int end = start + elementsPerChild;
        if (childIndex + 1 <= remainder) {
            end += 1;
        }

        return new HashSet<>(previewerIdList.subList(start, end));
    }

}
