package jobs;

import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.JobExecution;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;
import play.libs.akka.InjectedActorSupport;
import services.JobExecutionService;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

public class JobRunnerActor extends UntypedAbstractActor implements InjectedActorSupport {
    public static final String GET_RUNNING_JOBS_MESSAGE = "Get Running Jobs";

    private final JPAApi jpaApi;
    private final JobExecutionService jobExecutionService;
    private final JobRunnerState state;

    @Inject
    JobRunnerActor(JPAApi jpaApi, JobExecutionService jobExecutionService, JobRunnerState state) {
        this.jpaApi = jpaApi;
        this.jobExecutionService = jobExecutionService;
        this.state = state;
    }

    /**
     * Process an incoming message.
     *
     * @param message
     *            The message sent.
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof JobProtocol.JobStartMessage) {
            startOrQueueJob(((JobProtocol.JobStartMessage) message).getJobStarter());
        } else if (message instanceof JobProtocol.JobFinishMessage) {
            JobProtocol.JobFinishMessage finishMessage = (JobProtocol.JobFinishMessage) message;
            finishJob(finishMessage.getJobId(), finishMessage.isSuccess(), finishMessage.getResultJson());
        } else if (message instanceof JobProtocol.JobCheckMessage) {
            checkJobStatus(((JobProtocol.JobCheckMessage) message).getJobId());
        } else if (message instanceof String && GET_RUNNING_JOBS_MESSAGE.equals(message)) {
            ObjectNode returnData = Json.newObject();
            returnData.set("runningJobs", Json.toJson(state.getRunningJobIds()));
            returnData.set("queuedJobs", Json.toJson(state.getQueuedJobIds()));
            getSender().tell(returnData, getSelf());
        }
    }

    /**
     * Either start or queue the job defined by the given job starter, depending on whether the max concurrent jobs of
     * this type are running.
     *
     * @param jobStarter The job starter responsible for starting the job.
     */
    private void startOrQueueJob(JobStarter jobStarter) {
        String jobId = jobStarter.getJobId();
        if (!state.jobIsQueuedOrRunning(jobId)) {
            if (state.getRunningJobCount(jobStarter.getJobType()) >= jobStarter.getMaxConcurrentInstances()) {
                queueJob(jobStarter);
            } else {
                startJob(jobStarter);
            }
        }

        getSender().tell(jobId, getSelf());
    }

    /**
     * Queue the job defined by the given job starter.
     *
     * @param jobStarter The job starter responsible for starting the job.
     */
    private void queueJob(JobStarter jobStarter) {
        state.queueJob(jobStarter);
        Logger.info("event=\"Job queued.\", jobType=\"{}\", jobId=\"{}\", queueSize=\"{}\"", jobStarter.getJobType(),
                jobStarter.getJobId(), state.getQueuedJobCount(jobStarter.getJobType()));
    }

    /**
     * Start a job using the given job starter.
     *
     * @param jobStarter The job starter responsible for starting the job.
     */
    private void startJob(JobStarter jobStarter) {
        String jobId = jobStarter.getJobId();
        ActorRef jobActor = injectedChild(jobStarter::createJobActor, jobId + "_" + System.currentTimeMillis());
        jobStarter.setJobStartTime(Instant.now());
        jobStarter.startJob(jobActor, getSelf());
        state.addRunningJob(jobStarter);

        Logger.info("event=\"Job started.\", jobType=\"{}\", jobId=\"{}\"", jobStarter.getJobType(),
                jobStarter.getJobId());
    }

    /**
     * Mark a finished job as no longer currently running, and also save the job execution details. If there are any
     * queued jobs of the same type, start a job from the queue.
     *
     * @param jobId The ID of the finished job.
     * @param success A boolean indicating whether the finished job was successful.
     */
    private void finishJob(String jobId, boolean success, JsonNode resultJson) {
        Optional<JobStarter> jobStarterOptional = state.getRunningJob(jobId);

        if (jobStarterOptional.isPresent()) {
            JobStarter jobStarter = jobStarterOptional.get();

            if (jobStarter.getJobStartTime().isPresent()) {
                Logger.info("event=\"Job finished.\", jobType=\"{}\", jobId=\"{}\", success=\"{}\", duration=\"{} ms\"",
                        jobStarter.getJobType(), jobStarter.getJobId(), success,
                        Duration.between(jobStarter.getJobStartTime().get(), Instant.now()).toMillis());
            }

            jpaApi.withTransaction(() -> createJobExecution(jobId, success, resultJson));
            state.removeRunningJob(jobId);
            state.removeQueuedJob(jobStarter.getJobType()).ifPresent(this::startJob);
        }
    }

    /**
     * Creates a new job execution in the database for the completed job with the given information.
     *
     * @param jobId The ID of the finished job.
     * @param success A boolean indicating whether the finished job was successful.
     */
    private void createJobExecution(String jobId, boolean success, JsonNode resultJson) {
        Optional<JobExecution> jobExecutionOptional = jobExecutionService.findLatest(jobId);
        Integer runNumber;
        if (jobExecutionOptional.isPresent()) {
            runNumber = jobExecutionOptional.get().getRunNumber() + 1;
        } else {
            runNumber = 1;
        }

        String resultJsonString = "";
        if (resultJson != null) {
            resultJsonString = Json.stringify(resultJson);
        }

        JobExecution newJobExecution =
                new JobExecution(jobId, runNumber, success, resultJsonString, Timestamp.from(Instant.now()));
        jobExecutionService.create(newJobExecution);
    }

    /**
     * Get the status of the given job.
     *
     * @param jobId The ID of the job to check.
     */
    private void checkJobStatus(String jobId) {
        ObjectNode returnJson = Json.newObject();

        if (state.jobIsQueuedOrRunning(jobId)) {
            returnJson.put("isRunning", true);
        } else {
            Optional<JobExecution> jobExecutionOptional;
            try {
                jobExecutionOptional = jpaApi.withTransaction(() -> jobExecutionService.findLatest(jobId));
            } catch (Throwable t) {
                Logger.error("event=\"Exception occurred while retrieving job execution.\"", t);
                jobExecutionOptional = Optional.empty();
            }

            if (jobExecutionOptional.isPresent()) {
                JobExecution jobExecution = jobExecutionOptional.get();

                returnJson.put("isRunning", false);
                returnJson.put("jobSuccessful", jobExecution.getSuccess());
                if (jobExecution.getResultJson() != null && !"".equals(jobExecution.getResultJson())) {
                    JsonNode jobResultJson = Json.parse(jobExecution.getResultJson());
                    JsonNode jobData = jobResultJson.get("jobData");
                    JsonNode jobErrors = jobResultJson.get("jobErrors");
                    if (jobData != null) {
                        returnJson.set("jobData", jobData);
                    }
                    if (jobErrors != null) {
                        returnJson.set("jobErrors", jobErrors);
                    }
                }
            }
        }

        if (returnJson.size() > 0) {
            getSender().tell(returnJson, getSelf());
        } else {
            getSender().tell(false, getSelf());
        }
    }
}
