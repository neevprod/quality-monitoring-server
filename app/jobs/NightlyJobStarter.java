package jobs;

import akka.actor.Actor;
import akka.actor.ActorRef;
import com.typesafe.config.Config;

import javax.inject.Inject;
import java.time.Clock;

public class NightlyJobStarter extends JobStarter {
    private final int maxConcurrentInstances;
    private final NightlyJobActor.Factory nightlyJobActorFactory;

    @Inject
    NightlyJobStarter(Clock clock, NightlyJobActor.Factory nightlyJobActorFactory, Config configuration) {
        super(clock);

        this.nightlyJobActorFactory = nightlyJobActorFactory;
        this.maxConcurrentInstances = configuration.getInt("application.jobs.nightlyJob.maxConcurrent");
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    @Override
    public String getJobType() {
        return "nightlyJob";
    }

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    @Override
    public int getMaxConcurrentInstances() {
        return maxConcurrentInstances;
    }

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    @Override
    public String getJobId() {
        return "nightlyJob";
    }

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    @Override
    public Actor createJobActor() {
        return nightlyJobActorFactory.create();
    }

    /**
     * Start the job.
     *
     * @param jobActor A reference to the job actor.
     * @param sender A reference to the message sender.
     */
    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        jobActor.tell(new NightlyJobActor.RunMessage(getJobId()), sender);
    }
}
