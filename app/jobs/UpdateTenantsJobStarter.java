package jobs;

import akka.actor.Actor;
import akka.actor.ActorRef;
import com.typesafe.config.Config;

import javax.inject.Inject;
import java.time.Clock;

public class UpdateTenantsJobStarter extends JobStarter {
    private final int maxConcurrentInstances;
    private final UpdateTenantsActor.Factory updateTenantsActorFactory;

    @Inject
    UpdateTenantsJobStarter(Clock clock, UpdateTenantsActor.Factory updateTenantsActorFactory,
                            Config configuration) {
        super(clock);

        this.updateTenantsActorFactory = updateTenantsActorFactory;
        this.maxConcurrentInstances = configuration.getInt("application.jobs.updateTenants.maxConcurrent");
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    @Override
    public String getJobType() {
        return "updateTenants";
    }

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    @Override
    public int getMaxConcurrentInstances() {
        return maxConcurrentInstances;
    }

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    @Override
    public String getJobId() {
        return "updateTenants";
    }

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    @Override
    public Actor createJobActor() {
        return updateTenantsActorFactory.create();
    }

    /**
     * Start the job.
     *
     * @param jobActor A reference to the job actor.
     * @param sender A reference to the message sender.
     */
    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        jobActor.tell(UpdateTenantsActor.RUN_MESSAGE, sender);
    }
}
