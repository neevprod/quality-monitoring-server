package jobs;

import akka.actor.Actor;
import akka.actor.ActorRef;

import javax.inject.Inject;
import java.time.Clock;
import java.time.Instant;
import java.util.Optional;

public abstract class JobStarter implements Comparable<JobStarter> {

    private final Instant initializationTime;
    private Instant jobStartTime;
    private int priority;
    private String parentJobId;

    @Inject
    protected JobStarter(Clock clock) {
        initializationTime = clock.instant();
        priority = 1;
    }

    /**
     * Compare this with another instance of JobStarter to find which has the higher priority of being started.
     *
     * @param other Another instance of JobStarter.
     * @return An integer indicating which instance of JobStarter has the higher priority.
     */
    @Override
    public int compareTo(JobStarter other) {
        if (this.priority == other.getPriority()) {
            return Long.signum(this.initializationTime.toEpochMilli() - other.getInitializationTime().toEpochMilli());
        } else {
            return this.priority - other.getPriority();
        }
    }

    /**
     * Get the initialization time.
     *
     * @return The initialization time.
     */
    public Instant getInitializationTime() {
        return initializationTime;
    }

    public Optional<Instant> getJobStartTime() {
        if (jobStartTime == null) {
            return Optional.empty();
        } else {
            return Optional.of(jobStartTime);
        }
    }

    public void setJobStartTime(Instant jobStartTime) {
        this.jobStartTime = jobStartTime;
    }

    /**
     * Get the priority.
     *
     * @return The priority.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Set the priority.
     *
     * @param priority The priority.
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * Get the parent job ID.
     *
     * @return The parent job ID.
     */
    public Optional<String> getParentJobId() {
        if (parentJobId == null) {
            return Optional.empty();
        } else {
            return Optional.of(parentJobId);
        }
    }

    /**
     * Set the parent job ID.
     *
     * @param parentJobId The parent job ID.
     */
    public void setParentJobId(String parentJobId) {
        this.parentJobId = parentJobId;
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    public abstract String getJobType();

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    public abstract int getMaxConcurrentInstances();

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    public abstract String getJobId();

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    public abstract Actor createJobActor();

    /**
     * Start the job.
     *
     * @param jobActor A reference to the job actor.
     * @param sender A reference to the message sender.
     */
    public abstract void startJob(ActorRef jobActor, ActorRef sender);

}
