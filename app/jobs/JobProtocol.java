package jobs;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

import java.util.ArrayList;
import java.util.List;

/**
 * This class defines the protocol that the jobs use to communicate with the JobRunnerActor and the NightlyJobActor.
 */
public class JobProtocol {

    /**
     * A message telling the Job Runner actor to start running a job.
     */
    public static final class JobStartMessage {
        private final JobStarter jobStarter;

        public JobStartMessage(JobStarter jobStarter) {
            this.jobStarter = jobStarter;
        }

        /**
         * Get the job starter.
         *
         * @return The job starter.
         */
        public JobStarter getJobStarter() {
            return jobStarter;
        }
    }

    /**
     * A message telling the Job Runner actor that a job has finished running.
     */
    public static final class JobFinishMessage {
        private final String jobId;
        private final boolean success;
        private final JsonNode resultJson;

        /**
         * Create a new job finish message.
         *
         * @param jobId The ID of the job that finished.
         * @param success A boolean indicating whether the job was successful.
         */
        public JobFinishMessage(String jobId, boolean success) {
            this.jobId = jobId;
            this.success = success;
            this.resultJson = null;
        }

        /**
         * Create a new job finish message with result data.
         *
         * @param jobId The ID of the job that finished.
         * @param success A boolean indicating whether the job was successful.
         * @param resultJson A JSON node containing the finished job's result data.
         */
        public JobFinishMessage(String jobId, boolean success, JsonNode resultJson) {
            this.jobId = jobId;
            this.success = success;
            this.resultJson = resultJson;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        public String getJobId() {
            return jobId;
        }

        /**
         * Find out whether the job was successful.
         *
         * @return A boolean indicating whether the job was successful.
         */
        public boolean isSuccess() {
            return success;
        }

        /**
         * Get the JSON containing the job's results.
         *
         * @return The result JSON.
         */
        public JsonNode getResultJson() {
            return resultJson;
        }
    }

    /**
     * A message telling the Job Runner actor to check the status of a job.
     */
    public static final class JobCheckMessage {
        private final String jobId;

        /**
         * Create a new job check message.
         *
         * @param jobId The ID of the job to check.
         */
        public JobCheckMessage(String jobId) {
            this.jobId = jobId;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        public String getJobId() {
            return jobId;
        }
    }

    /**
     * Construct a JSON object that contains information about the result of a job.
     *
     * @param jobData Data about the execution of the job.
     * @return A JSON object.
     */
    public static JsonNode getJobResultJson(JsonNode jobData) {
        ObjectNode resultJson = Json.newObject();
        resultJson.set("jobData", jobData);
        return resultJson;
    }

    /**
     * Construct a JSON object that contains information about the result of a job.
     *
     * @param jobData Data about the execution of the job.
     * @param jobError An error message.
     * @return A JSON object.
     */
    public static JsonNode getJobResultJson(JsonNode jobData, String jobError) {
        ObjectNode resultJson = Json.newObject();
        resultJson.set("jobData", jobData);

        List<String> messages = new ArrayList<>();
        messages.add(jobError);
        resultJson.set("jobErrors", constructErrorJson(messages));

        return resultJson;
    }

    /**
     * Construct a JSON object that contains information about the result of a job.
     *
     * @param jobData Data about the execution of the job.
     * @param jobErrors A list of error messages.
     * @return A JSON object.
     */
    public static JsonNode getJobResultJson(JsonNode jobData, List<String> jobErrors) {
        ObjectNode resultJson = Json.newObject();
        resultJson.set("jobData", jobData);
        resultJson.set("jobErrors", constructErrorJson(jobErrors));
        return resultJson;
    }

    /**
     * Construct a JSON object that contains information about the result of a job.
     *
     * @param jobError An error message.
     * @return A JSON object.
     */
    public static JsonNode getJobResultJson(String jobError) {
        ObjectNode resultJson = Json.newObject();

        List<String> messages = new ArrayList<>();
        messages.add(jobError);
        resultJson.set("jobErrors", constructErrorJson(messages));

        return resultJson;
    }

    /**
     * Construct a JSON object that contains information about the result of a job.
     *
     * @param jobErrors A list of error messages.
     * @return A JSON object.
     */
    public static JsonNode getJobResultJson(List<String> jobErrors) {
        ObjectNode resultJson = Json.newObject();

        resultJson.set("jobErrors", constructErrorJson(jobErrors));

        return resultJson;
    }

    /**
     * Construct a JSON array representing the given error messages.
     *
     * @param messages The error messages.
     * @return A JSON array.
     */
    private static JsonNode constructErrorJson(List<String> messages) {
        ArrayNode errors = Json.newArray();
        for (String message : messages) {
            ObjectNode error = Json.newObject();
            error.put("message", message);
            errors.add(error);
        }
        return errors;
    }

}
