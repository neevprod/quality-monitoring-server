package jobs.qtivalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import jobs.JobProtocol;
import models.qtivalidation.KtTestCase;
import play.Logger;
import play.db.jpa.JPAApi;
import services.qtivalidation.KtTestCaseService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class KtTestCaseDownloadActor extends UntypedAbstractActor {
    private static final String ITEM_IDENTIFIER = "itemIdentifier";
    private static final String RESPONSE_IDENTIFIER = "responseIdentifier";

    private final KtTestCaseService ktTestCaseService;
    private final JPAApi jpaApi;

    @Inject
    public KtTestCaseDownloadActor(KtTestCaseService ktTestCaseService, JPAApi jpaApi) {
        this.ktTestCaseService = ktTestCaseService;
        this.jpaApi = jpaApi;
    }

    /**
     * process the incoming message.
     *
     * @param message The message sent
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        RunMessage msg = (RunMessage) message;
        String jobId = msg.getJobId();
        List<String> testCasesInError = new ArrayList<>();
        JsonArray ktItems = msg.getJsonElement().getAsJsonArray();
        Set<String> deletedItemIdentifiers = new HashSet<>();
        for (JsonElement ktItem : ktItems) {
            JsonObject ktItemObject = ktItem.getAsJsonObject();
            if (isValidKtItem(ktItemObject)) {
                String itemIdentifier = ktItemObject.get(ITEM_IDENTIFIER).getAsString();
                if (!deletedItemIdentifiers.contains(itemIdentifier)) {
                    jpaApi.withTransaction(() -> ktTestCaseService.deleteByIdentifiers(itemIdentifier));
                    deletedItemIdentifiers.add(itemIdentifier);
                }
                processKtItemTestCases(ktItemObject, testCasesInError);
            } else {
                testCasesInError.add("Item level data should contain the following fields: '_id', " +
                        "'includeInBuild', 'itemIdentifier', 'responseIdentifier', 'data'.");
            }
        }

        JobProtocol.JobFinishMessage finishMessage;
        if (testCasesInError.isEmpty()) {
            finishMessage = new JobProtocol.JobFinishMessage(jobId, true);
        } else {
            ArrayNode array = getJsonArrayNode(testCasesInError);
            Logger.info("event=\"Test case download job Failed.\", jobId=\"{}\"", jobId, array);
            finishMessage = new JobProtocol.JobFinishMessage(jobId, false, array);
        }
        getSender().tell(finishMessage, getSelf());
    }

    /**
     * Validates the KT responses. Deletes the old responses from QMA database and adds new ones.
     *
     * @param ktItemObject     The kt item as JSON
     * @param testCasesInError The List of errors to collect.
     */
    private void processKtItemTestCases(JsonObject ktItemObject, List<String> testCasesInError) {
        JsonArray ktItemTestCases = ktItemObject.get("data").getAsJsonArray();

        String ktId = ktItemObject.get("_id").getAsString();
        boolean ktIncludeInBuild = ktItemObject.get("includeInBuild").getAsBoolean();
        String itemIdentifier = ktItemObject.get(ITEM_IDENTIFIER).getAsString();
        String responseIdentifier = ktItemObject.get(RESPONSE_IDENTIFIER).getAsString();

        for (JsonElement ktItemTestCase : ktItemTestCases) {
            JsonObject ktItemTestCaseObject = ktItemTestCase.getAsJsonObject();

            if (!(ktItemTestCaseObject.has("candidateResponse") && ktItemTestCaseObject.has("responseId")
                    && ktItemTestCaseObject.has("savedScore") && ktItemTestCaseObject.has("savedScoreState"))) {
                String errorMessage = "KT Test case level data should contain the following fields: " +
                        "'candidateResponse', 'responseId', 'savedScore', 'savedScoreState'.";
                testCasesInError.add("ktId=" + ktId + ": " + errorMessage);
                Logger.error("event=\"{}\", ktId=\"{}\"", errorMessage, ktId);
            } else {
                String ktCandidateResponse = ktItemTestCaseObject.get("candidateResponse").toString();
                String ktResponseId = ktItemTestCaseObject.get("responseId").getAsString();
                String savedScore = ktItemTestCaseObject.get("savedScore").getAsString();
                String savedScoreState = ktItemTestCaseObject.get("savedScoreState").getAsString();

                KtTestCase ktTestCase = KtTestCase.createKtTestCaseObject(ktId, ktCandidateResponse, ktResponseId,
                        savedScore, savedScoreState, ktIncludeInBuild, itemIdentifier, responseIdentifier);
                jpaApi.withTransaction(() -> ktTestCaseService.create(ktTestCase));
            }
        }
    }

    private boolean isValidKtItem(JsonObject ktItemObject) {
        return ktItemObject.has("_id")
                && ktItemObject.has("includeInBuild")
                && ktItemObject.has(ITEM_IDENTIFIER)
                && ktItemObject.has(RESPONSE_IDENTIFIER)
                && ktItemObject.has("data");


    }

    private ArrayNode getJsonArrayNode(List<String> testCasesInError) {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode array = mapper.createArrayNode();
        for (String error : testCasesInError) {
            array.add(error);
        }
        return array;
    }

    public interface Factory {
        Actor create();
    }

    /**
     * A message telling the actor to run a KT testcase download job.
     */
    public static final class RunMessage {
        private final String jobId;
        private final JsonElement jsonElement;

        /**
         * @param jobId       The job id
         * @param jsonElement The KT test cases in JSON format.
         */
        public RunMessage(String jobId, JsonElement jsonElement) {
            this.jobId = jobId;
            this.jsonElement = jsonElement;
        }

        public String getJobId() {
            return jobId;
        }

        public JsonElement getJsonElement() {
            return jsonElement;
        }
    }
}
