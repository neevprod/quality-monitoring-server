package jobs.qtivalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pearson.itautomation.qtiscv.ItemResponse;
import com.pearson.itautomation.qtiscv.ItemResponseBean;
import com.pearson.itautomation.qtiscv.TestCaseLoader;
import com.pearson.itautomation.tn8.previewer.api.GetTenantItemsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import global.exceptions.QaApplicationException;
import jobs.JobProtocol;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;
import util.qtirespgen.QtiResponseGenerator;
import util.qtivalidation.TestCaseLoaderFactory;
import util.qtivalidation.TestNav8APIFactory;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

public class TestCaseUploadActor extends UntypedAbstractActor {
    // 8000 is the maximum length of URL allowed. 7000 is randomly picked up for url parameter.
    private static final int MAX_ALLOWED_URL_LENGTH = 7000;
    //Comma representation in URI (%2C)
    private static final int COMMA_HEX_CODE_LENGTH = 3;
    private final JPAApi jpaApi;
    private final TestNav8APIFactory apiFactory;
    private final QtiResponseGenerator qtiResponseGenerator;
    private final TestCaseLoaderFactory testCaseLoaderFactory;

    private List<String> errors;

    /**
     * A message telling the actor to run a test case upload job.
     */
    public static final class RunMessage {
        private final int previewerId;
        private final long tenantId;
        private final long userId;
        private final JsonNode json;
        private final Set<Integer> itemIds;
        private final int maxCorrect;
        private final int maxIncorrect;
        private final String jobId;

        /**
         * Create a RunMessage with JSON that specifies the test cases to upload.
         *
         * @param previewerId The ID of the previewer the tenant is in.
         * @param tenantId    The ID of the tenant.
         * @param userId      The ID of the user.
         * @param json        The JSON containing the test cases to upload.
         * @param jobId       The job ID.
         */
        public RunMessage(int previewerId, long tenantId, long userId, JsonNode json, String jobId) {
            this.previewerId = previewerId;
            this.tenantId = tenantId;
            this.userId = userId;
            this.json = json;
            this.itemIds = null;
            this.maxCorrect = 0;
            this.maxIncorrect = 0;
            this.jobId = jobId;
        }

        /**
         * Create a RunMessage with a set of items to generate test cases for.
         *
         * @param previewerId  The ID of the previewer the tenant is in.
         * @param tenantId     The ID of the tenant.
         * @param userId       The ID of the user.
         * @param itemIds      The set of item IDs to generate test cases for.
         * @param maxCorrect   The maximum number of correct responses to generate.
         * @param maxIncorrect The maximum number of incorrect responses to generate.
         * @param jobId        The job ID.
         */
        public RunMessage(int previewerId, long tenantId, long userId, Set<Integer> itemIds, int maxCorrect,
                          int maxIncorrect, String jobId) {
            this.previewerId = previewerId;
            this.tenantId = tenantId;
            this.userId = userId;
            this.json = null;
            this.itemIds = Collections.unmodifiableSet(itemIds);
            this.maxCorrect = maxCorrect;
            this.maxIncorrect = maxIncorrect;
            this.jobId = jobId;
        }

        /**
         * Get the previewer ID.
         *
         * @return The previewer ID.
         */
        public int getPreviewerId() {
            return previewerId;
        }

        /**
         * Get the tenant ID.
         *
         * @return The tenant ID.
         */
        public long getTenantId() {
            return tenantId;
        }

        /**
         * Get the user ID.
         *
         * @return The user ID.
         */
        public long getUserId() {
            return userId;
        }

        /**
         * Get the JSON Data.
         *
         * @return JsonNode containing the test case data.
         */
        public JsonNode getJson() {
            return json;
        }

        /**
         * Get the set of item IDs.
         *
         * @return The set of item IDs.
         */
        public Set<Integer> getItemIds() {
            return itemIds;
        }

        /**
         * Get the max correct value.
         *
         * @return The max correct value.
         */
        public int getMaxCorrect() {
            return maxCorrect;
        }

        /**
         * Get the max incorrect value.
         *
         * @return The max incorrect value.
         */
        public int getMaxIncorrect() {
            return maxIncorrect;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        public String getJobId() {
            return jobId;
        }
    }

    public interface Factory {
        Actor create();
    }

    @Inject
    TestCaseUploadActor(JPAApi jpaApi, TestNav8APIFactory apiFactory, QtiResponseGenerator qtiResponseGenerator,
                        TestCaseLoaderFactory testCaseLoaderFactory) {
        this.jpaApi = jpaApi;
        this.apiFactory = apiFactory;
        this.qtiResponseGenerator = qtiResponseGenerator;
        this.testCaseLoaderFactory = testCaseLoaderFactory;

        this.errors = new ArrayList<>();
    }

    /**
     * Process an incoming message.
     *
     * @param message The message sent.
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        RunMessage runMessage = (RunMessage) message;

        int previewerId = runMessage.getPreviewerId();
        long tenantId = runMessage.getTenantId();
        String jobId = runMessage.getJobId();

        Logger.info("event=\"Running Test Case Upload job.\", previewerId=\"{}\", tenantId=\"{}\", jobId=\"{}\"",
                previewerId, tenantId, jobId);

        try {
            Map<String, Long> uploadResults;
            if (runMessage.getJson() == null) {
                uploadResults = generateAndUploadTestCases(runMessage.getPreviewerId(), runMessage.getTenantId(),
                        runMessage.getItemIds(), runMessage.getMaxCorrect(), runMessage.getMaxIncorrect(),
                        runMessage.getUserId());
            } else {
                uploadResults = uploadTestCases(runMessage.getPreviewerId(), runMessage.getTenantId(),
                        runMessage.getUserId(), runMessage.getJson());
            }

            JobProtocol.JobFinishMessage finishMessage;
            if (errors.isEmpty()) {
                finishMessage = new JobProtocol.JobFinishMessage(jobId, true,
                        JobProtocol.getJobResultJson(Json.toJson(uploadResults)));
            } else {
                finishMessage = new JobProtocol.JobFinishMessage(jobId, true,
                        JobProtocol.getJobResultJson(Json.toJson(uploadResults), errors));
            }
            getSender().tell(finishMessage, getSelf());
        } catch (QaApplicationException e) {
            Logger.error("event=\"Test case upload job failed. No test cases could be generated.\", " +
                    "previewerId=\"{}\", tenantId=\"{}\", jobId=\"{}\"", previewerId, tenantId, jobId);

            JobProtocol.JobFinishMessage failMessage;
            if (errors.isEmpty()) {
                failMessage = new JobProtocol.JobFinishMessage(jobId, false,
                        JobProtocol.getJobResultJson("Generation of test cases failed."));
            } else {
                failMessage = new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson(errors));
            }
            getSender().tell(failMessage, getSelf());
        } catch (Throwable e) {
            Logger.error("event=\"Exception occurred while running Test Case Upload job.\", previewerId=\"{}\", " +
                    "tenantId=\"{}\", jobId=\"{}\"", previewerId, tenantId, jobId, e);

            JobProtocol.JobFinishMessage failMessage = new JobProtocol.JobFinishMessage(jobId, false,
                    JobProtocol.getJobResultJson("An unexpected error occurred while running the job."));
            getSender().tell(failMessage, getSelf());
        } finally {
            getContext().stop(getSelf());
        }
    }

    /**
     * Generate and upload test cases for the given item IDs.
     *
     * @param previewerId The ID of the previewer the tenant is in.
     * @param tenantId    The ID of the tenant.
     * @param itemIds     The set of item IDs to generate test cases for.
     * @return A map containing the results of the upload.
     */
    private Map<String, Long> generateAndUploadTestCases(int previewerId, long tenantId, Set<Integer> itemIds, int maxCorrect,
                                                         int maxIncorrect, long userId) throws QaApplicationException {
        ArrayNode generatedTestCases = Json.newArray();
        Optional<TestNav8API> tn8ApiOptional = jpaApi.withTransaction(() -> apiFactory.create(previewerId));
        if (tn8ApiOptional.isPresent()) {
            TestNav8API tn8Api = tn8ApiOptional.get();
            Map<Integer, String> itemIdsToIdentifiers = getItemsIdToIdentifierMap(itemIds, tn8Api, tenantId);

            for (Integer itemId : itemIds) {
                String itemXml = tn8Api.getItemXMLByIdentifier((int) tenantId, itemIdsToIdentifiers.get(itemId)).getXML();

                try {

                    JsonNode response = qtiResponseGenerator.generateResponses(itemXml, previewerId, tenantId, maxCorrect,
                            maxIncorrect, 0, true, itemIdsToIdentifiers.get(itemId));

                    JsonNode itemTestCases = response.get("data");
                    if (itemTestCases != null) {
                        generatedTestCases.add(itemTestCases);
                    } else {
                        Logger.warn(
                                "event=\"Could not generate test cases for item.\", "
                                        + "previewerId=\"{}\", tenantId=\"{}\", itemIdentifier=\"{}\"",
                                previewerId, tenantId, itemIdsToIdentifiers.get(itemId));
                    }

                    JsonNode responseGenErrors = response.get("errors");
                    if (responseGenErrors != null) {
                        for (JsonNode responseGenError : responseGenErrors) {
                            errors.add("Item " + itemIdsToIdentifiers.get(itemId) + " (ID " + itemId + "): " +
                                    responseGenError.get("message").asText());
                        }
                    }

                } catch (Throwable e) {

                    Logger.warn(
                            "event=\"Could not generate test cases for item.\", "
                                    + "previewerId=\"{}\", tenantId=\"{}\", itemIdentifier=\"{}\"",
                            previewerId, tenantId, itemIdsToIdentifiers.get(itemId));
                    errors.add("Item " + itemIdsToIdentifiers.get(itemId) + " (ID " + itemId + "): an exception occurred while generating test cases for this item");
                }
            }

            if (generatedTestCases.size() > 0) {
                return uploadTestCases(previewerId, tenantId, userId, generatedTestCases);
            } else {
                throw new QaApplicationException("No test cases could be generated for the given items.");
            }
        } else {
            throw new QaApplicationException(
                    "No test cases could be generated because the given previewer does not " + "exist.");
        }
    }

    /**
     * Get a map of item IDs to identifiers for the given set of items in the given tenant.
     *
     * @param itemIds  The set of items to put in the map.
     * @param tn8Api   The instance of TestNav8Api to get the item information from.
     * @param tenantId The ID of the tenant the items are in.
     * @return The map of item IDs to identifiers.
     */
    private Map<Integer, String> getItemsIdToIdentifierMap(Set<Integer> itemIds, TestNav8API tn8Api, long tenantId) {

        Map<Integer, String> result = new HashMap<>();
        int maxItemIdLength = itemIds.stream().max(Comparator.comparing(itemId -> itemId))
                .map(itemId -> String.valueOf(itemId).length()).orElse(0);

        List<Integer> tempItemIds = new ArrayList<>(itemIds);
        //Calculates the length of comma encoded URI parameters using itemIds.
        int calculatedUrlLength = (maxItemIdLength * tempItemIds.size()) + ((tempItemIds.size() - 1) *
                COMMA_HEX_CODE_LENGTH);

        //Calculates the maximum number of items per request
        Integer maxItemIdsPerRequest = (MAX_ALLOWED_URL_LENGTH + COMMA_HEX_CODE_LENGTH) / (maxItemIdLength +
                COMMA_HEX_CODE_LENGTH);
        int firstPosition = 0;
        int lastPosition = maxItemIdsPerRequest < tempItemIds.size() ? maxItemIdsPerRequest
                : tempItemIds.size();

        while (calculatedUrlLength > MAX_ALLOWED_URL_LENGTH) {
            List<Integer> subList = tempItemIds.subList(firstPosition,
                    (lastPosition < tempItemIds.size()) ? lastPosition : tempItemIds.size());

            String itemIdList = subList.stream().map(String::valueOf).collect(Collectors.joining(","));
            Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = new HashMap<>();
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS, itemIdList);
            result.putAll(tn8Api.getTenantItems((int) tenantId, paramMap).getItems().stream().collect(
                    Collectors.toMap(GetTenantItemsResult.Item::getId, GetTenantItemsResult.Item::getIdentifier)));

            tempItemIds.removeAll(subList);
            calculatedUrlLength = (maxItemIdLength * tempItemIds.size()) + ((tempItemIds.size() - 1) * COMMA_HEX_CODE_LENGTH);

        }
        if (!tempItemIds.isEmpty()) {
            String itemIdList = tempItemIds.stream().map(String::valueOf).collect(Collectors.joining(","));
            Map<GetTenantItemsResult.GetTenantItemsParam, String> paramMap = new HashMap<>();
            paramMap.put(GetTenantItemsResult.GetTenantItemsParam.ITEM_IDS, itemIdList);

            result.putAll(tn8Api.getTenantItems((int) tenantId, paramMap).getItems().stream().collect(
                    Collectors.toMap(GetTenantItemsResult.Item::getId, GetTenantItemsResult.Item::getIdentifier)));
        }
        return result;
    }

    /**
     * Upload the test cases contained in the given JSON.
     *
     * @param previewerId The ID of the previewer the tenant is in.
     * @param tenantId    The ID of the tenant.
     * @param userId      The ID of the user.
     * @param json        The JSON containing the test cases to upload.
     * @return A map containing the results of the upload.
     */
    private Map<String, Long> uploadTestCases(int previewerId, long tenantId, long userId, JsonNode json)
            throws QaApplicationException {
        List<ItemResponse> itemResponses = getItemResponses(json);
        Optional<TestCaseLoader> loaderOptional = jpaApi
                .withTransaction(() -> testCaseLoaderFactory.create(previewerId));
        if (loaderOptional.isPresent()) {
            return loaderOptional.get().generateAndLoadTestCases(itemResponses, (int) tenantId, userId);
        } else {
            throw new QaApplicationException(
                    "No test cases could be uploaded because the given previewer does not " + "exist.");
        }
    }

    /**
     * Converts the test case JSON data into a list of ItemResponses.
     *
     * @param json JsonNode containing the test case data.
     * @return List of ItemResponses
     */
    private List<ItemResponse> getItemResponses(JsonNode json) {
        String jsonString = json.toString();
        List<ItemResponse> responses = new ArrayList<>();
        JsonArray jsonArray = new JsonParser().parse(jsonString).getAsJsonArray();

        for (JsonElement jsonElem : jsonArray) {
            JsonObject jsonData = jsonElem.getAsJsonObject();
            String uin = jsonData.get("itemId").getAsString();
            String identifierToInteraction =
                    createIdentifierToInteraction(jsonData.get("identifiersToInteractions").getAsJsonArray());

            JsonArray responseArray = jsonData.getAsJsonArray("responses");

            for (JsonElement response : responseArray) {
                JsonObject responseObj = response.getAsJsonObject();

                double points = 0d;
                JsonArray outcomes = responseObj.get("outcomes").getAsJsonArray();

                for (int i = 0; i < outcomes.size(); i++) {
                    JsonObject outcome = outcomes.get(i).getAsJsonObject();
                    if (outcome.get("identifier").getAsString().equals("SCORE")) {
                        points = outcome.get("value").getAsDouble();
                        break;
                    }
                }
                boolean attempted = responseObj.get("attempted").getAsBoolean();
                String mods = responseObj.get("mods").toString();
                ItemResponse itemResponse =
                        new ItemResponseBean(uin, points, attempted, identifierToInteraction, mods, null);

                responses.add(itemResponse);
            }
        }
        return responses;
    }

    /**
     * Create an IdentifierToInteraction string from a JSON array.
     *
     * @param jsonArray The JSON array to create the string from.
     * @return The IdentifierToInteraction string.
     */
    private String createIdentifierToInteraction(final JsonArray jsonArray) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject element = jsonArray.get(i).getAsJsonObject();
            result.append(element.get("identifier"));
            result.append(":");
            result.append(element.get("interaction"));
            if (i < jsonArray.size() - 1) {
                result.append(";");
            }
        }
        return result.toString();
    }
}
