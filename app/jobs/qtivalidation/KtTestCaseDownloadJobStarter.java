package jobs.qtivalidation;

import akka.actor.Actor;
import akka.actor.ActorRef;
import com.google.gson.JsonElement;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.typesafe.config.Config;
import jobs.JobStarter;

import java.time.Clock;

public class KtTestCaseDownloadJobStarter extends JobStarter {
    private final int maxConcurrentInstances;
    private final KtTestCaseDownloadActor.Factory ktTestCaseDownloadActorFactory;
    private final long userId;
    private final JsonElement json;
    private final long startTime;

    public interface Factory {

        KtTestCaseDownloadJobStarter create(@Assisted("userId") long userId, JsonElement json);
    }

    @AssistedInject
    KtTestCaseDownloadJobStarter(Clock clock, KtTestCaseDownloadActor.Factory ktTestCaseDownloadActorFactory,
                                 @Assisted("userId") long userId, @Assisted JsonElement json, Config configuration) {
        super(clock);
        this.ktTestCaseDownloadActorFactory = ktTestCaseDownloadActorFactory;
        this.userId = userId;
        this.json = json;
        this.startTime = System.currentTimeMillis();
        this.maxConcurrentInstances = configuration.getInt("application.jobs.testCaseUpload.maxConcurrent");
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    @Override
    public String getJobType() {
        return "KTTestCaseDownload";
    }

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    @Override
    public int getMaxConcurrentInstances() {
        return maxConcurrentInstances;
    }

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    @Override
    public String getJobId() {
        return "ktTestCaseDownload_" + userId + "_" + startTime;
    }

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    @Override
    public Actor createJobActor() {
        return ktTestCaseDownloadActorFactory.create();
    }

    /**
     * Start the job.
     *
     * @param jobActor A reference to the job actor.
     * @param sender   A reference to the message sender.
     */
    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        jobActor.tell(new KtTestCaseDownloadActor.RunMessage(getJobId(), json), sender);

    }
}
