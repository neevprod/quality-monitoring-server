package jobs.qtivalidation;

import akka.actor.Actor;
import akka.actor.ActorRef;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.typesafe.config.Config;
import jobs.JobStarter;

import java.time.Clock;
import java.util.Set;

public class TestCaseUploadJobStarter extends JobStarter {
    private final int maxConcurrentInstances;
    private final TestCaseUploadActor.Factory testCaseUploadActorFactory;
    private final int previewerId;
    private final long tenantId;
    private final long userId;
    private final JsonNode json;
    private final Set<Integer> itemIds;
    private final int maxCorrect;
    private final int maxIncorrect;
    private final long startTime;

    public interface Factory {
        /**
         * Create an instance of TestCaseUploadJobStarter for uploading the given test cases.
         *
         * @param previewerId The ID of the previewer.
         * @param tenantId The ID of the tenant.
         * @param userId The ID of the user doing the upload.
         * @param json The test cases to upload.
         * @return An instance of TestCaseUploadJobStarter.
         */
        TestCaseUploadJobStarter create(int previewerId, @Assisted("tenantId") long tenantId,
                                        @Assisted("userId") long userId, JsonNode json);

        /**
         * Create an instance of TestCaseUploadJobStarter for generating and uploading test cases.
         *
         * @param previewerId The ID of the previewer.
         * @param tenantId The ID of the tenant.
         * @param userId The ID of the user doing the upload.
         * @param itemIds The set of items to generate test cases for.
         * @param maxCorrect The maximum number of correct responses to generate for each item.
         * @param maxIncorrect The maximum number of incorrect responses to generate for each item.
         * @return An instance of TestCaseUploadJobStarter.
         */
        TestCaseUploadJobStarter create(int previewerId, @Assisted("tenantId") long tenantId,
                                        @Assisted("userId") long userId, Set<Integer> itemIds,
                                        @Assisted("maxCorrect") int maxCorrect,
                                        @Assisted("maxIncorrect") int maxIncorrect);
    }

    /**
     * Create an instance of TestCaseUploadJobStarter for uploading the given test cases.
     *
     * @param testCaseUploadActorFactory The TestCaseUploadActor factory.
     * @param previewerId The ID of the previewer.
     * @param tenantId The ID of the tenant.
     * @param userId The ID of the user doing the upload.
     * @param json The test cases to upload.
     */
    @AssistedInject
    TestCaseUploadJobStarter(Clock clock, TestCaseUploadActor.Factory testCaseUploadActorFactory,
                             @Assisted int previewerId, @Assisted("tenantId") long tenantId,
                             @Assisted("userId") long userId, @Assisted JsonNode json, Config configuration) {
        super(clock);

        this.testCaseUploadActorFactory = testCaseUploadActorFactory;
        this.previewerId = previewerId;
        this.tenantId = tenantId;
        this.userId = userId;
        this.json = json;
        this.itemIds = null;
        this.maxCorrect = 0;
        this.maxIncorrect = 0;
        this.startTime = System.currentTimeMillis();
        this.maxConcurrentInstances = configuration.getInt("application.jobs.testCaseUpload.maxConcurrent");
    }

    /**
     * Create an instance of TestCaseUploadJobStarter for generating and uploading test cases.
     *
     * @param testCaseUploadActorFactory The TestCaseUploadActor factory.
     * @param previewerId The ID of the previewer.
     * @param tenantId The ID of the tenant.
     * @param userId The ID of the user doing the upload.
     * @param itemIds The set of items to generate test cases for.
     * @param maxCorrect The maximum number of correct responses to generate for each item.
     * @param maxIncorrect The maximum number of incorrect responses to generate for each item.
     */
    @AssistedInject
    TestCaseUploadJobStarter(Clock clock, TestCaseUploadActor.Factory testCaseUploadActorFactory,
                             @Assisted int previewerId, @Assisted("tenantId") long tenantId,
                             @Assisted("userId") long userId, @Assisted Set<Integer> itemIds,
                             @Assisted("maxCorrect") int maxCorrect, @Assisted("maxIncorrect") int maxIncorrect,
                             Config configuration) {
        super(clock);

        this.testCaseUploadActorFactory = testCaseUploadActorFactory;
        this.previewerId = previewerId;
        this.tenantId = tenantId;
        this.userId = userId;
        this.json = null;
        this.itemIds = itemIds;
        this.maxCorrect = maxCorrect;
        this.maxIncorrect = maxIncorrect;
        this.startTime = System.currentTimeMillis();
        this.maxConcurrentInstances = configuration.getInt("application.jobs.testCaseUpload.maxConcurrent");
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    @Override
    public String getJobType() {
        return "testCaseUpload";
    }

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    @Override
    public int getMaxConcurrentInstances() {
        return maxConcurrentInstances;
    }

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    @Override
    public String getJobId() {
        return "testCaseUpload_" + userId + "_" + startTime;
    }

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    @Override
    public Actor createJobActor() {
        return testCaseUploadActorFactory.create();
    }

    /**
     * Start the job.
     *
     * @param jobActor A reference to the job actor.
     * @param sender   A reference to the message sender.
     */
    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        if (json != null) {
            // Upload the test cases given in the JSON.
            jobActor.tell(new TestCaseUploadActor.RunMessage(previewerId, tenantId, userId, json, getJobId()), sender);
        } else {
            // Generate and upload test cases for the given item IDs.
            jobActor.tell(new TestCaseUploadActor.RunMessage(previewerId, tenantId, userId, itemIds, maxCorrect,
                    maxIncorrect, getJobId()), sender);
        }
    }
}
