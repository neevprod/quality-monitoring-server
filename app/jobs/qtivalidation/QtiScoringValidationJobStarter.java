package jobs.qtivalidation;

import akka.actor.Actor;
import akka.actor.ActorRef;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.typesafe.config.Config;
import jobs.JobStarter;

import java.time.Clock;

public class QtiScoringValidationJobStarter extends JobStarter {
    private final int maxConcurrentInstances;
    private final QtiScoringValidationActor.Factory qtiScoringValidationActorFactory;
    private final Integer previewerId;
    private final Long tenantId;

    public interface Factory {
        QtiScoringValidationJobStarter create();
        QtiScoringValidationJobStarter create(int previewerId, long tenantId);
    }

    /**
     * Create an instance of QtiScoringValidationJobStarter that will start the job for all subscribed tenants.
     *
     * @param qtiScoringValidationActorFactory The QtiScoringValidationActorFactory.
     */
    @AssistedInject
    QtiScoringValidationJobStarter(Clock clock, QtiScoringValidationActor.Factory qtiScoringValidationActorFactory,
                                   Config configuration) {
        super(clock);

        this.qtiScoringValidationActorFactory = qtiScoringValidationActorFactory;
        this.previewerId = null;
        this.tenantId = null;
        this.maxConcurrentInstances = configuration.getInt("application.jobs.qtiScoringValidation.maxConcurrent");
    }

    /**
     * Create an instance of QtiScoringValidationJobStarter that will start the job for a specific tenant.
     *
     * @param qtiScoringValidationActorFactory The QtiScoringValidationActorFactory.
     */
    @AssistedInject
    QtiScoringValidationJobStarter(Clock clock, QtiScoringValidationActor.Factory qtiScoringValidationActorFactory,
                                   @Assisted int previewerId, @Assisted long tenantId, Config configuration) {
        super(clock);

        this.qtiScoringValidationActorFactory = qtiScoringValidationActorFactory;
        this.previewerId = previewerId;
        this.tenantId = tenantId;
        this.maxConcurrentInstances = configuration.getInt("application.jobs.qtiScoringValidation.maxConcurrent");
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    @Override
    public String getJobType() {
        return "qtiScoringValidation";
    }

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    @Override
    public int getMaxConcurrentInstances() {
        return maxConcurrentInstances;
    }

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    @Override
    public String getJobId() {
        StringBuilder jobId = new StringBuilder();

        if (getParentJobId().isPresent()) {
            jobId.append(getParentJobId().get()).append("_");
        }

        if (previewerId == null || tenantId == null) {
            jobId.append("qtiScoringValidation");
        } else {
            jobId.append("qtiScoringValidation_").append(previewerId).append("_").append(tenantId);
        }

        return jobId.toString();
    }

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    @Override
    public Actor createJobActor() {
        return qtiScoringValidationActorFactory.create();
    }

    /**
     * Start the job.
     *
     * @param jobActor A reference to the job actor.
     * @param sender A reference to the message sender.
     */
    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        if (previewerId == null || tenantId == null) {
            jobActor.tell(new QtiScoringValidationActor.RunAllMessage(getJobId()), sender);
        } else {
            jobActor.tell(new QtiScoringValidationActor.RunMessage(getJobId(), previewerId, tenantId), sender);
        }
    }
}
