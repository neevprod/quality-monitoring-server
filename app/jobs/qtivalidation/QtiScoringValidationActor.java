package jobs.qtivalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pearson.itautomation.qtiscv.QtiScv;
import com.typesafe.config.Config;
import jobs.JobProtocol;
import models.qtivalidation.HistTenant;
import models.qtivalidation.UserTenantJobSubscription;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import services.qtivalidation.HistTenantService;
import services.qtivalidation.UserTenantJobSubscriptionService;

import javax.inject.Inject;
import javax.inject.Provider;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class QtiScoringValidationActor extends UntypedAbstractActor {
    private final QtiScv qtiScv;
    private final JPAApi jpaApi;
    private final HistTenantService histTenantService;
    private final UserTenantJobSubscriptionService userTenantJobSubscriptionService;
    private final Provider<Email> emailProvider;
    private final MailerClient mailerClient;
    private final String fromAddress;
    private final ZoneId developerTimeZone;

    /**
     * A message telling the actor to run the job for all tenants with subscribers.
     */
    public static final class RunAllMessage {
        private final String jobId;

        public RunAllMessage(String jobId) {
            this.jobId = jobId;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        public String getJobId() {
            return jobId;
        }
    }

    /**
     * A message telling the actor to run the job for the given tenant in the given previewer.
     */
    public static final class RunMessage {
        private final String jobId;
        private final int previewerId;
        private final long tenantId;

        public RunMessage(String jobId, int previewerId, long tenantId) {
            this.jobId = jobId;
            this.previewerId = previewerId;
            this.tenantId = tenantId;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        public String getJobId() {
            return jobId;
        }

        /**
         * Get the previewer ID.
         *
         * @return The previewer ID.
         */
        public int getPreviewerId() {
            return previewerId;
        }

        /**
         * Get the tenant ID.
         *
         * @return The tenant ID.
         */
        public long getTenantId() {
            return tenantId;
        }
    }

    public interface Factory {
        Actor create();
    }

    @Inject
    QtiScoringValidationActor(QtiScv qtiScv, JPAApi jpaApi, HistTenantService histTenantService,
                              UserTenantJobSubscriptionService userTenantJobSubscriptionService, Provider<Email> emailProvider,
                              MailerClient mailerClient, Config configuration) {
        this.qtiScv = qtiScv;
        this.jpaApi = jpaApi;
        this.histTenantService = histTenantService;
        this.userTenantJobSubscriptionService = userTenantJobSubscriptionService;
        this.emailProvider = emailProvider;
        this.mailerClient = mailerClient;
        this.fromAddress = configuration.getString("application.email.fromAddress");
        this.developerTimeZone = ZoneId.of(configuration.getString("application.developerTimeZone"));
    }

    /**
     * Process an incoming message.
     *
     * @param message The message sent.
     * @throws Exception
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof RunAllMessage) {
            runJobForAllSubscribedTenants(((RunAllMessage) message).getJobId());
        } else if (message instanceof RunMessage) {
            RunMessage runMessage = (RunMessage) message;
            runJobForSingleTenant(runMessage.getJobId(), runMessage.getPreviewerId(), runMessage.getTenantId());
        }
    }

    /**
     * Run the QTI Scoring Validation job for all tenants with subscribers.
     */
    private void runJobForAllSubscribedTenants(String jobId) {
        Logger.info("event=\"Running QTI-SCV job for all tenants with subscriptions.\", jobId=\"{}\"", jobId);

        try {
            List<Integer> histExecIds = qtiScv.execute();
            List<HistTenant> histTenants = getHistTenants(histExecIds);
            Map<Long, Set<String>> previewerTenantToEmails = getPreviewerTenantToSubscribersMap();

            for (HistTenant histTenant : histTenants) {
                sendEmailToSubscribers(histTenant,
                        previewerTenantToEmails.get(histTenant.getPreviewerTenant().getPreviewerTenantId()));
            }

            ObjectNode returnJson = Json.newObject();
            ArrayNode histExecIdsJson = returnJson.putArray("histExecIds");
            for (Integer histExecId : histExecIds) {
                histExecIdsJson.add(histExecId);
            }

            JobProtocol.JobFinishMessage finishMessage = new JobProtocol.JobFinishMessage(jobId, true,
                    JobProtocol.getJobResultJson(returnJson));
            getSender().tell(finishMessage, getSelf());
        } catch (Exception e) {
            Logger.error("event=\"Exception occurred while running QTI-SCV job.\", jobId=\"{}\"", jobId, e);

            JobProtocol.JobFinishMessage finishMessage = new JobProtocol.JobFinishMessage(jobId, false,
                    JobProtocol.getJobResultJson("An unexpected error occurred while running the job."));
            getSender().tell(finishMessage, getSelf());
        } finally {
            getContext().stop(getSelf());
        }
    }

    /**
     * Run the QTI Scoring Validation job for a single tenant.
     *
     * @param previewerId The ID of the previewer the tenant is in.
     * @param tenantId    The ID of the tenant.
     */
    private void runJobForSingleTenant(String jobId, int previewerId, long tenantId) {
        Logger.info("event=\"Running QTI-SCV job for a single tenant.\", previewerId=\"{}\", tenantId=\"{}\", "
                + "jobId=\"{}\"", previewerId, tenantId, jobId);

        try {
            int histExecId = qtiScv.executeForTenant(previewerId, (int) tenantId);

            ObjectNode returnJson = Json.newObject();
            returnJson.put("histExecId", histExecId);

            JobProtocol.JobFinishMessage finishMessage = new JobProtocol.JobFinishMessage(jobId, true,
                    JobProtocol.getJobResultJson(returnJson));
            getSender().tell(finishMessage, getSelf());
        } catch (Exception e) {
            Logger.error("event=\"Exception occurred while running QTI-SCV job.\", previewerId=\"{}\", "
                    + "tenantId=\"{}\", jobId=\"{}\"", previewerId, tenantId, jobId, e);

            JobProtocol.JobFinishMessage finishMessage = new JobProtocol.JobFinishMessage(jobId, false,
                    JobProtocol.getJobResultJson("An unexpected error occurred while running the job."));
            getSender().tell(finishMessage, getSelf());
        } finally {
            getContext().stop(getSelf());
        }
    }

    /**
     * Get a list of HistTenants from the given list of HistExec IDs.
     *
     * @param histExecIds The list of HistExec IDs.
     * @return A list of HistTenants.
     */
    private List<HistTenant> getHistTenants(List<Integer> histExecIds) {
        List<HistTenant> histTenants;
        Set<Integer> histExecIdSet = histExecIds.stream().collect(Collectors.toSet());

        try {
            histTenants = jpaApi.withTransaction(() -> histTenantService.findAllByHistExecIds(histExecIdSet));
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }

        return histTenants;
    }

    /**
     * Get a map of PreviewerTenant IDs to sets of subscriber emails.
     *
     * @return A map of PreviewerTenant IDs to sets of subscriber emails.
     */
    private Map<Long, Set<String>> getPreviewerTenantToSubscribersMap() {
        String jobTypeName = "Score Consistency Validation";
        List<UserTenantJobSubscription> userTenantJobSubscriptions;

        try {
            userTenantJobSubscriptions = jpaApi
                    .withTransaction(() -> userTenantJobSubscriptionService.fetchByJobTypeName(jobTypeName));
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }

        return userTenantJobSubscriptions.stream().collect(Collectors.groupingBy(
                UserTenantJobSubscription::getPreviewerTenantId,
                Collectors.mapping(subscription -> subscription.getHumanUser().getEmail(), Collectors.toSet())));
    }

    /**
     * Send an email to all subscribers for the given HistTenant.
     *
     * @param histTenant The HistTenant to send the email about.
     * @param emails     The email addresses to send to.
     */
    private void sendEmailToSubscribers(HistTenant histTenant, Set<String> emails) {
        try {
            Email email = emailProvider.get();

            if (histTenant.getItemsWithFailedTcs() == 0 && histTenant.getItemExceptions() == 0) {
                email.setSubject(histTenant.getPreviewerTenant().getPreviewer().getName() + " SCV Job for Tenant "
                        + histTenant.getPreviewerTenant().getTenantId() + " Completed with No Failures");
            } else {
                email.setSubject(histTenant.getPreviewerTenant().getPreviewer().getName() + " SCV Job for Tenant "
                        + histTenant.getPreviewerTenant().getTenantId() + " Completed with Failures");
            }
            email.setFrom(fromAddress);
            emails.stream().forEach(email::addBcc);

            String eol = System.getProperty("line.separator");
            String body = "Previewer Name: " + histTenant.getPreviewerTenant().getPreviewer().getName() + eol
                    + "Tenant Id: " + histTenant.getPreviewerTenant().getTenantId() + eol + "Tenant Name: "
                    + histTenant.getPreviewerTenant().getName() + eol + "Execution Date: "
                    + getLocalStartDate(histTenant) + eol + "Execution Duration: " + getDuration(histTenant) + eol
                    + "Number of Items Validated: " + histTenant.getItemsValidated() + eol
                    + "Number of Items with Failed Test Cases: " + histTenant.getItemsWithFailedTcs() + eol
                    + "Number of Items with Pending Test Cases: " + histTenant.getItemsWithPendingTcs() + eol
                    + "Number of Deactivated Items: " + histTenant.getDeactivatedItems() + eol
                    + "Number of Item Exceptions: " + histTenant.getItemExceptions() + eol + "Execution Completed: "
                    + histTenant.getExecCompleted();
            email.setBodyText(body);
            mailerClient.send(email);
        } catch (Exception e) {
            Logger.error(
                    "event=\"Exception occurred while attempting to send email to tenant Subscribers.\", previewerId=\"{}\", "
                            + "tenantId=\"{}\"",
                    histTenant.getPreviewerTenant().getTenantId(), histTenant.getPreviewerTenant().getTenantId(), e);
        }
    }

    /**
     * Get the start date from a HistTenant in the local time zone.
     *
     * @param histTenant The HistTenant.
     * @return The start date.
     */
    private String getLocalStartDate(HistTenant histTenant) {
        ZonedDateTime serverStartTime = ZonedDateTime.of(histTenant.getExecStart().toLocalDateTime(),
                ZoneId.systemDefault());
        ZonedDateTime localStartTime = serverStartTime.withZoneSameInstant(developerTimeZone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        return localStartTime.format(formatter);
    }

    /**
     * Get the duration of a HistTenant in HH:MM:SS format.
     *
     * @param histTenant The HistTenant.
     * @return The duration.
     */
    private String getDuration(HistTenant histTenant) {
        LocalDateTime startTime = histTenant.getExecStart().toLocalDateTime();
        LocalDateTime endTime = histTenant.getExecStop().toLocalDateTime();
        Duration duration = Duration.between(startTime, endTime);

        long hours = duration.toHours();
        long minutes = duration.toMinutes() % 60;
        long seconds = duration.getSeconds() % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

}
