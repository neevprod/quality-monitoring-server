package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.google.gson.JsonParser;
import com.pearson.itautomation.ValidationError;
import com.pearson.itautomation.bevalidation.BackendValidator;
import com.pearson.itautomation.bevalidation.database.BackendDbConfigProperty;
import com.pearson.itautomation.bevalidation.exception.BackendDatabaseException;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import models.systemvalidation.DbConnection;
import models.systemvalidation.StudentReservation;
import models.systemvalidation.StudentScenario;
import play.Logger;
import play.db.jpa.JPAApi;
import services.systemvalidation.StudentReservationService;
import util.ConnectionPoolManager;
import util.systemvalidation.BackendData;

import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Nand Joshi
 */
public class BackendValidationActor extends UntypedAbstractActor {
    private final BackendData.Factory backendDataFactory;
    private final JPAApi jpaApi;
    private final StudentReservationService studentReservationService;

    @Inject
    BackendValidationActor(ConnectionPoolManager connectionPoolManager, BackendData.Factory backendDataFactory,
                           JPAApi jpaApi, StudentReservationService studentReservationService) {
        this.backendDataFactory = backendDataFactory;
        this.jpaApi = jpaApi;
        this.studentReservationService = studentReservationService;
    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof BackendInputMessage) {
            BackendInputMessage inputMessage = (BackendInputMessage) message;
            StudentScenario studentScenario = inputMessage.getStudentScenario();
            Long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();
            Long studentScenarioId = studentScenario.getStudentScenarioId();
            String stateName = studentScenario.getState().getStateName();

            try {
                Logger.info("event=\"Started studentScenario execution.\"," +
                        " divJobExecId= \"{}\", " +
                        "studentScenarioId= \"{}\", " +
                        "state= \"{}\"", divJobExecId, studentScenarioId, stateName);

                Set<BackendDbConfigProperty> beWorkflowDbProperties = getBackendWorkflowDbProperties(studentScenario);
                BackendValidator validator = new BackendValidator(beWorkflowDbProperties);

                final String validationResult = validator.execute(studentScenario.getDivJobExec().getScopeTreePath(),
                        studentScenario.getUuid(), inputMessage.validateEpenPath());

                final String success = new JsonParser().parse(validationResult).getAsJsonObject().get("success")
                        .getAsString();

                String resultMessage = DataIntegrityValidationActor.FINISHED_MESSAGE;
                if ("No".equalsIgnoreCase(success)) {
                    resultMessage = DataIntegrityValidationActor.FAILED_MESSAGE;
                } else {
                    jpaApi.withTransaction(() -> {
                        Optional<StudentReservation> studentReservation = studentReservationService
                                .findByUUID(studentScenario.getUuid());
                        studentReservation.ifPresent(studentReservationService::delete);
                    });
                }

                FinishMessage finishMessage = generateFinishMessage(validationResult, studentScenario, resultMessage);

                // Sends back the finish message to the sender actor
                getSender().tell(finishMessage, getSelf());
            } catch (BackendDatabaseException ex) {
                FinishMessage finishMessage = generateFinishMessage(ex.getMessage(), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);
                getSender().tell(finishMessage, getSelf());
            } catch (Throwable ex) {
                final String validationResult = "Exception occurred while calling BackEndPath Validator for StudentScenarioId "
                        + studentScenarioId;

                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error(
                        "event=\"Exception occurred while calling BackEndPath Validator for StudentScenarioId {}.\"",
                        studentScenarioId, ex);
                getSender().tell(finishMessage, getSelf());
            } finally {
                getContext().stop(getSelf());
            }
        }

    }

    /**
     * Creates an instance of {@link FinishMessage} to send back to the sender actor.
     *
     * @param validationResult The Optional of JsonString of {@link ValidationError}
     * @param studentScenario  The {@link StudentScenario}
     * @param finishMessage    The finished message
     * @return The {@link FinishMessage}
     */
    private FinishMessage generateFinishMessage(final String validationResult, final StudentScenario studentScenario,
                                                final String finishMessage) {
        return new DataIntegrityValidationActor.FinishMessage(studentScenario,
                studentScenario.getDivJobExec().getDivJobExecId(), finishMessage, validationResult);
    }

    /**
     * Given a StudentScenario, get the details of the Backend workflow DBs.
     *
     * @param studentScenario The StudentScenario.
     * @return A Set of BackendDbConfigProperty objects.
     */
    private Set<BackendDbConfigProperty> getBackendWorkflowDbProperties(StudentScenario studentScenario) throws BackendDatabaseException {
        DbConnection backendDbConnection = studentScenario.getDivJobExec().getEnvironment().getBeMysqlConnection();
        BackendData backendData = backendDataFactory.create(backendDbConnection);
        Set<BackendData.BackendWorkflowDetails> workflowDetails = backendData.getWorkflows();
        return workflowDetails.stream()
                .map(wf -> new BackendDbConfigProperty(wf.getHost(), wf.getDatabase(), wf.getDataSource()))
                .collect(Collectors.toSet());
    }

    public interface Factory {
        Actor create();
    }

    public static class BackendInputMessage {
        private final boolean validateEpenPath;
        private final StudentScenario studentScenario;

        /**
         * @param validateEpenPath If true, validate the ePEN path. If false, validate the primary path.
         * @param studentScenario  The {@link StudentScenario}
         */
        public BackendInputMessage(boolean validateEpenPath, StudentScenario studentScenario) {
            this.validateEpenPath = validateEpenPath;
            this.studentScenario = studentScenario;
        }

        /**
         * @return the validateEpenPath
         */
        public final boolean validateEpenPath() {
            return validateEpenPath;
        }

        /**
         * @return the studentScenario
         */
        public final StudentScenario getStudentScenario() {
            return studentScenario;
        }
    }
}
