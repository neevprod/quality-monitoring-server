package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.google.gson.*;
import com.mongodb.MongoClient;
import com.pearson.epen2.bulkscoring.util.ApplicationConfig;
import com.pearson.epen2.bulkscoring.util.ConnectionDetails;
import com.pearson.epen2.bulkscoring.util.Queries;
import com.pearson.itautomation.ValidationError;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import controllers.systemvalidation.EpenScenarioCreator;
import itautomation.epen2.scoring.Epen2Scoring;
import itautomation.epen2.scoring.Epen2ScoringFactory;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import models.systemvalidation.*;
import play.Logger;
import play.db.jpa.JPAApi;
import services.systemvalidation.EpenUserReservationService;
import util.ConnectionPoolManager;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nand Joshi
 */
public class Epen2ValidationActor extends UntypedAbstractActor {
    private final static int WF_DB_PORT = 3306;
    private final static String WF_DB_USERNAME = "dba_admin";
    private final static String WF_DB_PASSWORD = "4p8n66U6rh43XiS";
    private String epenUserPrefix;
    private final JPAApi jpaApi;
    private final EpenUserReservationService epenUserReservationService;
    private Integer reservedEpenUserId;

    private final ConnectionPoolManager connectionPoolManager;
    private final Provider<EpenScenarioCreator> epenScenarioCreatorProvider;

    @Inject
    Epen2ValidationActor(ConnectionPoolManager connectionPoolManager,
                         Provider<EpenScenarioCreator> epenScenarioCreatorProvider, JPAApi jpaApi,
                         EpenUserReservationService epenUserReservationService, Config configuration) {
        this.connectionPoolManager = connectionPoolManager;
        this.epenScenarioCreatorProvider = epenScenarioCreatorProvider;
        this.jpaApi = jpaApi;
        this.epenUserReservationService = epenUserReservationService;
        try {
            this.epenUserPrefix = configuration.getString("application.epenUserPrefix");
        } catch (ConfigException.Missing e) {
            this.epenUserPrefix = "";
            Logger.warn(e.getMessage());
        }

    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof StudentScenario) {
            final StudentScenario studentScenario = (StudentScenario) message;
            Long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();
            Long studentScenarioId = studentScenario.getStudentScenarioId();
            String stateName = studentScenario.getState().getStateName();
            try {
                MongoClient epenMongoClient = getEpenMongoClient(
                        studentScenario.getDivJobExec().getEnvironment().getEpen2MongoConnection());

                JsonObject epenScenarioJsonObj = new JsonParser().parse(studentScenario.getEpenScenario().getScenario()).getAsJsonObject();
                if (epenScenarioJsonObj.get("data").getAsJsonArray().size() == 0) {

                    Gson gson = new Gson();
                    JsonObject validationResultObject = new JsonObject();

                    validationResultObject.addProperty("success", "no");
                    validationResultObject.addProperty("errorMessage", "No ePEN scoring eligible item statuses found in the PANext Backend Path.");
                    String validationResult = gson.toJson(validationResultObject);
                    String resultMessage = DataIntegrityValidationActor.FAILED_MESSAGE;

                    FinishMessage finishMessage = generateFinishMessage(validationResult, studentScenario, resultMessage);
                    getSender().tell(finishMessage, getSelf());
                } else {

                    Logger.info("event=\"Started studentScenario execution.\"," +
                            " divJobExecId= \"{}\"," +
                            " studentScenarioId= \"{}\"," +
                            " state= \"{}\"", divJobExecId, studentScenarioId, stateName);

                    ConnectionDetails epenMasterDbConnectionDetails = getEpenMasterDbDetails(
                            studentScenario.getDivJobExec().getEnvironment().getEpen2MysqlConnection());
                    List<ConnectionDetails> epenWorkflowDbConnectionDetails = getEpenWorkflowDbDetails(
                            epenMasterDbConnectionDetails.getDataSource());

                    Epen2Scoring epen2Scoring = Epen2ScoringFactory.getScoringInstance(new ApplicationConfig());
                    epen2Scoring.setTimeOut(20); // Time out in minutes
                    String validationResult = epen2Scoring.scoreResponses(setUpEpen2Scenario(studentScenario), epenMasterDbConnectionDetails,
                            epenWorkflowDbConnectionDetails, epenMongoClient);

                    String success = new JsonParser().parse(validationResult).getAsJsonObject().get("success")
                            .getAsString();

                    if ("yes".equalsIgnoreCase(success)) {
                        JsonArray inputDataArray = new JsonParser().parse(validationResult).getAsJsonObject().get("inputData").getAsJsonArray();
                        if (inputDataArray.size() <= 0 || inputDataArray.get(0).getAsJsonObject().get("data").getAsJsonArray().size() <= 0) {
                            success = "no";
                            Gson gson = new Gson();
                            JsonObject validationResultObject = gson.fromJson(validationResult, JsonElement.class).getAsJsonObject();
                            validationResultObject.addProperty("success", "no");
                            validationResultObject.addProperty("errorMessage", "ePEN2 bulkScorer failed to score the ePEN items. Please resume the job to try again.");
                            validationResult = gson.toJson(validationResultObject);
                        }
                    }

                    String resultMessage = DataIntegrityValidationActor.FINISHED_MESSAGE;

                    jpaApi.withTransaction(() -> epenUserReservationService.unlock(reservedEpenUserId));

                    if ("no".equalsIgnoreCase(success)) {
                        resultMessage = DataIntegrityValidationActor.FAILED_MESSAGE;
                    }

                    FinishMessage finishMessage = generateFinishMessage(validationResult, studentScenario, resultMessage);

                    // Sends back the finish message to the sender actor
                    getSender().tell(finishMessage, getSelf());
                }
            } catch (SQLException ex) {
                final String validationResult = "Exception occurred while calling ePEN2 Validator for studentScenarioId "
                        + studentScenarioId + ". An error occurred while connecting to the ePEN MySQL DB.";

                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"Exception occurred while calling ePEN2 Validator.\", studentScenarioId=\"{}\"",
                        studentScenarioId, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (Throwable ex) {
                final String validationResult = "Exception occurred while calling ePEN2 Validator for studentScenarioId "
                        + studentScenarioId + ".";

                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"Exception occurred while calling ePEN2 Validator.\", studentScenarioId=\"{}\"",
                        studentScenarioId, ex);

                getSender().tell(finishMessage, getSelf());
            } finally {
                getContext().stop(getSelf());
            }
        }
    }

    /**
     * Construct a ConnectionDetails object with ePEN Master DB details to send to ePEN Bulk Scorer.
     *
     * @param epenMasterDbConnection The ePEN Master DB Connection.
     * @return A ConnectionDetails object.
     */
    private ConnectionDetails getEpenMasterDbDetails(DbConnection epenMasterDbConnection) {
        ConnectionDetails connectionDetails = new ConnectionDetails();
        connectionDetails.setHostName(epenMasterDbConnection.getHost());
        connectionDetails.setPortNumber(epenMasterDbConnection.getPort().toString());
        connectionDetails.setSchemaName(epenMasterDbConnection.getDbName());
        connectionDetails.setUserName(epenMasterDbConnection.getDbUser());
        connectionDetails.setPassword(epenMasterDbConnection.getDbPass());

        DataSource dataSource = connectionPoolManager.getMysqlConnectionPool(epenMasterDbConnection.getHost(),
                epenMasterDbConnection.getDbName(), epenMasterDbConnection.getDbUser(),
                epenMasterDbConnection.getDbPass(), epenMasterDbConnection.getPort());
        connectionDetails.setDataSource(dataSource);

        return connectionDetails;
    }

    /**
     * Construct a List of ConnectionDetails with ePEN Workflow DB details to send to ePEN Bulk Scorer.
     *
     * @param epenMasterDbDataSource A DataSource for connecting to the ePEN Master DB.
     * @return A List of ConnectionDetails.
     */
    private List<ConnectionDetails> getEpenWorkflowDbDetails(DataSource epenMasterDbDataSource) throws SQLException {
        List<ConnectionDetails> workflowDbDetails = new ArrayList<>();

        try (Connection connection = epenMasterDbDataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(Queries.QUERY_WF_INSTANCES_DETAILS)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    ConnectionDetails workflowConnectionDetails = new ConnectionDetails();
                    workflowConnectionDetails.setInstanceId(resultSet.getInt(1));
                    workflowConnectionDetails.setInstanceName(resultSet.getString(2));
                    workflowConnectionDetails.setHostName(resultSet.getString(3));
                    workflowConnectionDetails.setSchemaName(resultSet.getString(4));

                    DataSource dataSource = connectionPoolManager.getMysqlConnectionPool(
                            workflowConnectionDetails.getHostName(), workflowConnectionDetails.getSchemaName(),
                            WF_DB_USERNAME, WF_DB_PASSWORD, WF_DB_PORT);
                    workflowConnectionDetails.setDataSource(dataSource);

                    workflowDbDetails.add(workflowConnectionDetails);
                }
            }
        }

        return workflowDbDetails;
    }

    /**
     * Get a MongoClient for the ePEN Mongo DB to send to ePEN Bulk Scorer.
     *
     * @param epenMongoDbConnection The ePEN Mongo DB connection.
     * @return A MongoClient
     */
    private MongoClient getEpenMongoClient(DbConnection epenMongoDbConnection) {
        if ("n/a".equalsIgnoreCase(epenMongoDbConnection.getDbName())) {
            return connectionPoolManager.getMongoConnectionPool(epenMongoDbConnection.getHost(),
                    epenMongoDbConnection.getPort());
        } else {
            return connectionPoolManager.getMongoConnectionPool(epenMongoDbConnection.getHost(),
                    epenMongoDbConnection.getDbName(), epenMongoDbConnection.getDbUser(),
                    epenMongoDbConnection.getDbPass(), epenMongoDbConnection.getPort());
        }
    }

    /**
     * Add the environment specific information to the ePEN2 scenario in the given student scenario.
     *
     * @param studentScenario The {@link StudentScenario}
     * @return The ePEN2 Scenario JSON string.
     */
    private String setUpEpen2Scenario(StudentScenario studentScenario) {

        final Environment environment = studentScenario.getDivJobExec().getEnvironment();
        final Scenario epenScenarioDetail = studentScenario.getEpenScenario();
        final List<String> scenarioClasses = new ArrayList<>();

        //Using the MAXPOINTS scenario class to retrieve administration, accountCode and epenApplicationlUrl
        // as for certain programs condition codes with a BLANK score type are not present.
        scenarioClasses.add("MAXPOINTS");

        EpenScenarioCreator epenScenarioCreator = epenScenarioCreatorProvider.get();

        List<EpenScenarioCreator.EpenScenarioResult> epenScenarios = epenScenarioCreator.readEpen2Scenario(environment,
                scenarioClasses, studentScenario.getDivJobExec().getScopeTreePath(), epenScenarioDetail.getTestCode(),
                epenScenarioDetail.getFormCode());

        if (epenScenarios.isEmpty()) {
            throw new RuntimeException(
                    "Failed to read administration, accountCode, and epenApplicationUrl to update epenScenario.");
        }
        EpenScenarioCreator.EpenScenarioResult epenScenarioResult = epenScenarios.get(0);
        String administration = epenScenarioResult.getAdministration();
        String accountCode = epenScenarioResult.getAccountCode();
        String epenApplicationUrl = epenScenarioResult.getEpenApplicationUrl();

        final DbConnection con = environment.getEpen2MysqlConnection();

        JsonObject result = new JsonObject();

        JsonObject environments = new JsonObject();

        environments.addProperty("administration", administration);
        environments.addProperty("accountCode", accountCode);
        environments.addProperty("epenApplicationUrl", epenApplicationUrl);

        environments.addProperty("epenopsdbHost", con.getHost());
        environments.addProperty("epenopsdbPort", con.getPort().toString());
        environments.addProperty("epenopsdbName", con.getDbName());
        environments.addProperty("epenopsdbUser", con.getDbUser());
        environments.addProperty("epenopsdbPassword", con.getDbPass());

        environments.addProperty("wfdbHost", "");
        environments.addProperty("wfdbPort", String.valueOf(WF_DB_PORT));
        environments.addProperty("wfdbUser", WF_DB_USERNAME);
        environments.addProperty("wfdbPassword", WF_DB_PASSWORD);

        environments.addProperty("threadCount", "1");

        environments.addProperty("scoreUserRole", "Role_Project_Manager");
        environments.addProperty("reliabilityUserRole", "Role_Project_Manager");
        environments.addProperty("resolutionUserRole", "Role_Project_Manager");
        environments.addProperty("adjudicationUserRole", "Role_Project_Manager");

        DbConnection epen2MongoDatabaseCon = environment.getEpen2MongoConnection();
        environments.addProperty("mongoHost", epen2MongoDatabaseCon.getHost());
        environments.addProperty("mongoPort", epen2MongoDatabaseCon.getPort().toString());

        result.add("environmentDetails", environments);

        final Long studentScenarioId = studentScenario.getStudentScenarioId();

        jpaApi.withTransaction(() -> {
            String reservedEpenUserIdString = studentScenarioId.toString();
            Optional<EpenUserReservation> reservedEpenUser = epenUserReservationService.findNextAvailable();

            if (reservedEpenUser.isPresent()) {
                reservedEpenUserId = reservedEpenUser.get().getId();
                reservedEpenUserIdString = reservedEpenUserId.toString();
            }

            JsonObject score = new JsonObject();
            score.addProperty("scoreType", "SCORE");
            score.addProperty("account", epenUserPrefix + "sco" + reservedEpenUserIdString);

            JsonObject reliability = new JsonObject();
            reliability.addProperty("scoreType", "RELIABILITY");
            reliability.addProperty("account", epenUserPrefix + "rel" + reservedEpenUserIdString);

            JsonObject resolution = new JsonObject();
            resolution.addProperty("scoreType", "RESOLUTION");
            resolution.addProperty("account", epenUserPrefix + "res" + reservedEpenUserIdString);

            JsonObject adjudication = new JsonObject();
            adjudication.addProperty("scoreType", "ADJUDICATION");
            adjudication.addProperty("account", epenUserPrefix + "adj" + reservedEpenUserIdString);

            JsonArray accounts = new JsonArray();

            accounts.add(score);
            accounts.add(reliability);
            accounts.add(resolution);
            accounts.add(adjudication);

            result.add("scorerAccounts", accounts);
        });

        final String epenScenario = studentScenario.getEpenScenario().getScenario();

        JsonObject epenScenarioJsonObj = new JsonParser().parse(epenScenario).getAsJsonObject();

        epenScenarioJsonObj.addProperty("uuid", studentScenario.getUuid());
        JsonArray inputDataArray = new JsonArray();
        inputDataArray.add(epenScenarioJsonObj);

        result.add("inputData", inputDataArray);
        return result.toString();
    }

    /**
     * Creates an instance of {@link FinishMessage} to send back to the sender actor.
     *
     * @param validationResult The Optional of JsonString of {@link ValidationError}
     * @param studentScenario  The {@link StudentScenario}
     * @param finishMessage    The finished message
     * @return The {@link FinishMessage}
     */
    private FinishMessage generateFinishMessage(final String validationResult, final StudentScenario studentScenario,
                                                final String finishMessage) {
        return new DataIntegrityValidationActor.FinishMessage(studentScenario,
                studentScenario.getDivJobExec().getDivJobExecId(), finishMessage, validationResult);
    }

    public interface Factory {
        Actor create();
    }
}