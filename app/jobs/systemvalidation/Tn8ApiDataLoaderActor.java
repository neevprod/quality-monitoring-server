package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.*;
import com.pearson.itautomation.AppSettings;
import com.pearson.itautomation.TN8ApiDataLoaderInput;
import com.pearson.itautomation.Tn8ApiDataLoader;
import com.pearson.itautomation.ValidationError;
import com.pearson.itautomation.http.Tn8HttpClient;
import com.pearson.itautomation.scenario.Scenario;
import com.pearson.itautomation.scenario.ScenarioItem;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import models.systemvalidation.Environment;
import models.systemvalidation.StudentScenario;
import models.systemvalidation.StudentScenarioResult;
import models.systemvalidation.scenario.*;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;
import services.systemvalidation.StudentScenarioResultService;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nand Joshi
 */
public class Tn8ApiDataLoaderActor extends UntypedAbstractActor {
    private final JPAApi jpaApi;
    private final StudentScenarioResultService studentScenarioResultService;

    @Inject
    Tn8ApiDataLoaderActor(JPAApi jpaApi, StudentScenarioResultService studentScenarioResultService) {
        this.jpaApi = jpaApi;
        this.studentScenarioResultService = studentScenarioResultService;
    }

    @Override
    public void onReceive(Object message) {

        if (message instanceof StudentScenario) {

            StudentScenario studentScenario = (StudentScenario) message;
            Long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();
            Long studentScenarioId = studentScenario.getStudentScenarioId();
            String stateName = studentScenario.getState().getStateName();

            try {
                Logger.info("event=\"Started studentScenario execution.\"," +
                        " divJobExecId= \"{}\", " +
                        "studentScenarioId= \"{}\", " +
                        "state= \"{}\"", divJobExecId, studentScenarioId, stateName);

                Tn8ApiDataLoaderResult validationResult = callTestnavApiDataLoader(studentScenario);

                JsonObject finalResult = new JsonObject();
                finalResult.addProperty("contentKey", validationResult.getContentKey());
                finalResult.addProperty("wireKey", validationResult.getWireKey());
                finalResult.addProperty("urlGetTestDef", validationResult.getUrlGetTestDef());

                String resultMessage = DataIntegrityValidationActor.FINISHED_MESSAGE;

                // If a validationError is found, set the Finished message to failure.
                if (validationResult.getErrorMessage() != null) {
                    resultMessage = DataIntegrityValidationActor.FAILED_MESSAGE;
                    finalResult.addProperty("success", "no");
                    finalResult.add("validationError",
                            new JsonParser().parse(validationResult.getErrorMessage()).getAsJsonArray());
                } else {
                    finalResult.addProperty("success", "yes");
                    finalResult.add("validationError", new JsonArray());
                }
                FinishMessage finishMessage = generateFinishMessage(finalResult.toString(), studentScenario,
                        resultMessage);

                // Send the finish message back to the sender actor
                getSender().tell(finishMessage, getSelf());
            } catch (Throwable e) {

                Logger.error("event=\"Exception occurred while calling TestnavApiDataLoader for studentScenario {}.\"",
                        studentScenarioId, e);

                final String validationResult = "Exception occurred while calling TestnavApiDataLoader job for studentScenarioId "
                        + studentScenarioId;

                JsonObject errorMessage = new JsonParser()
                        .parse(DataIntegrityValidationActor.getErrorJson(validationResult)).getAsJsonObject();

                FinishMessage finishMessage = generateFinishMessage(errorMessage.toString(), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                getSender().tell(finishMessage, getSelf());

            } finally {
                getContext().stop(getSelf());
            }

        } else {
            Logger.warn("event=\"Message is not Instance of StudentScenario.\"");
            getContext().stop(getSelf());
        }
    }

    /**
     * Converts JsonScenario to TN8ApiDataLoader scenario.<br>
     * Creates TN8ApiDataLoader application setting records.<br>
     * Calls TN8ApiDataLoader application.<br>
     * If TN8ApiDataLoader application founds any validationError then returns the result as JSON string.
     *
     * @param studentScenario The {@link StudentScenario}
     * @return The instance of {@link Tn8ApiDataLoaderResult}
     */
    private Tn8ApiDataLoaderResult callTestnavApiDataLoader(final StudentScenario studentScenario) {
        String validationResult = null;
        final String jsonScenario = studentScenario.getTestnavScenario().getScenario();
        if (jsonScenario == null || jsonScenario.isEmpty()) {
            Logger.error("event=\"Can not find scenario for scenarioId {}.\"",
                    studentScenario.getTestnavScenario().getScenarioId());
            String errorMsg = "[\"Can not find scenario for studentScenarioId " + studentScenario.getStudentScenarioId()
                    + "\"]";
            return new Tn8ApiDataLoaderResult(errorMsg);
        }
        Scenario scenarioResult = getTestnavScenarioFromJsonScenario(jsonScenario);
        if (scenarioResult == null) {
            String errorMsg = "[\"Failed to convert JsonScenario to Tn8ApiDataloaderScenario!\"]";
            return new Tn8ApiDataLoaderResult(errorMsg);
        }
        AppSettings appSettings = createAppSettings(studentScenario);
        Environment environment = studentScenario.getDivJobExec().getEnvironment();

        Tn8ApiDataLoader tn8ApiDataLoader = new Tn8ApiDataLoader(appSettings);
        tn8ApiDataLoader.execute(getTn8ApiDataLoaderInput(studentScenario, scenarioResult, environment));
        List<ValidationError> errors = tn8ApiDataLoader.getErrorForSingleLogin();
        if (!errors.isEmpty()) {
            validationResult = getJsonError(errors);
        }

        Tn8HttpClient tn8HttpClient = tn8ApiDataLoader.getTestNav8HttpClient();
        return new Tn8ApiDataLoaderResult(validationResult, tn8HttpClient.getContentKey(), tn8HttpClient.getWireKey(),
                tn8HttpClient.getUrlGetTestDef());
    }

    /**
     * Converts List of {@link ValidationError} to JSON String.
     *
     * @param errors The List of {@link ValidationError}
     * @return The JSON representation of <code>errors</code>
     */
    private String getJsonError(final List<ValidationError> errors) {
        return new GsonBuilder().serializeNulls().create().toJson(errors);
    }

    /**
     * Creates instance of {@link TN8ApiDataLoaderInput}.
     *
     * @param studentScenario The {@link StudentScenario}
     * @param scenario        The {@link Scenario}
     * @return The instance of {@link TN8ApiDataLoaderInput}
     */
    private TN8ApiDataLoaderInput getTn8ApiDataLoaderInput(final StudentScenario studentScenario,
                                                           final Scenario scenario, final Environment environment) {
        TN8ApiDataLoaderInput tn8ApiDataLoaderInput = new TN8ApiDataLoaderInput();
        tn8ApiDataLoaderInput.setExportType(studentScenario.getTestSession().getScopeCode());
        tn8ApiDataLoaderInput.setExternalLoginKey(studentScenario.getUuid());
        tn8ApiDataLoaderInput.setFormNumber(studentScenario.getTestnavScenario().getFormCode());
        tn8ApiDataLoaderInput.setLoginName(studentScenario.getTestnavLoginName());
        tn8ApiDataLoaderInput.setPassword(studentScenario.getTestnavLoginPassword());
        tn8ApiDataLoaderInput.setPanFeUrl(environment.getFeUrl());

        if (!(studentScenario.getStudentTestSessionAssignId() == null)) {
            tn8ApiDataLoaderInput.setStudentTestSessionAssignId(studentScenario.getStudentTestSessionAssignId());
        }

        tn8ApiDataLoaderInput.setTestScenario(scenario);
        return tn8ApiDataLoaderInput;
    }

    /**
     * @param studentScenario The {@link StudentScenario}
     * @return The instance of {@link AppSettings}
     */
    private AppSettings createAppSettings(StudentScenario studentScenario) {

        String testnavApplicationUrl = studentScenario.getTestSession().getTestnavApplicationUrl();
        String testnavWebserviceUrl = studentScenario.getTestSession().getTestnavWebserviceUrl();

        testnavApplicationUrl = testnavApplicationUrl.replaceAll("http://", "");
        testnavWebserviceUrl = testnavWebserviceUrl.replaceAll("http://", "");

        if (!(testnavApplicationUrl.startsWith("https://"))) {
            testnavApplicationUrl = "https://" + testnavApplicationUrl;
        }

        if (!(testnavWebserviceUrl.startsWith("https://"))) {
            testnavWebserviceUrl = "https://" + testnavWebserviceUrl;
        }

        if (testnavWebserviceUrl.endsWith("/")) {
            testnavWebserviceUrl = testnavWebserviceUrl.substring(0, testnavWebserviceUrl.length() - 1);
        }

        if (testnavApplicationUrl.endsWith("/")) {
            testnavApplicationUrl = testnavApplicationUrl.substring(0, testnavApplicationUrl.length() - 1);
        }

        // Retrieving the previous execution result to get content key and wire key.
        List<StudentScenarioResult> studentScenarioResults = jpaApi.withTransaction(() -> studentScenarioResultService
                .findByStudentScenarioIdAndStateName(studentScenario.getStudentScenarioId(),
                        DataIntegrityValidationActor.DivStateName.TN8_API_DATA_LOADER.name()));

        String contentKey = null;
        String wireKey = null;
        String urlGetTestDef = null;
        for (StudentScenarioResult studentScenarioResult : studentScenarioResults) {
            ObjectNode resultJson = (ObjectNode) Json.parse(studentScenarioResult.getResultDetails());
            if (resultJson.has("contentKey")) {
                contentKey = resultJson.get("contentKey").asText();
                wireKey = resultJson.get("wireKey").asText();
                urlGetTestDef = resultJson.get("urlGetTestDef").asText();
            }
        }
        return new AppSettings.Builder(testnavApplicationUrl, testnavWebserviceUrl,
                studentScenario.getTestSession().getTestnavCustomerCode())
                .clientIdentifier(studentScenario.getTestSession().getTestnavClientIdentifier())
                .clientSecret(studentScenario.getTestSession().getTestnavClientSecret()).contentKey(contentKey)
                .wireKey(wireKey).urlGetTestDef(urlGetTestDef).build();
    }

    /**
     * Creates an instance of {@link FinishMessage} to send back to the sender actor.
     *
     * @param validationResult The Optional of JsonString of {@link ValidationError}
     * @param studentScenario  The {@link StudentScenario}
     * @param finishMessage    The finished message
     * @return The {@link FinishMessage}
     */
    private FinishMessage generateFinishMessage(final String validationResult, final StudentScenario studentScenario,
                                                final String finishMessage) {
        return new DataIntegrityValidationActor.FinishMessage(studentScenario,
                studentScenario.getDivJobExec().getDivJobExecId(), finishMessage, validationResult);
    }

    public interface Factory {
        Actor create();
    }

    /**
     * Takes Scenario JSON String and converts into {@link Scenario} which is input for {@link Tn8ApiDataLoader}.
     *
     * @param jsonScenarioString The Scenario JSON String.
     * @return The instance of {@link Scenario}
     */
    private Scenario getTestnavScenarioFromJsonScenario(final String jsonScenarioString) {
        Scenario result = null;
        if (jsonScenarioString == null || jsonScenarioString.isEmpty()) {
            return result;
        }
        try {
            Gson gson = new Gson();
            JsonScenario jsonScenario = gson.fromJson(jsonScenarioString, JsonScenario.class);
            final List<ScenarioItem> scenarioItems = new ArrayList<>();
            Gson g = new Gson();
            for (ItemResponse itemResponse : jsonScenario.getItemResponses()) {

                String uin = itemResponse.getUin();
                List<IdentifiersToInteraction> identifierToInteraction = itemResponse.getIdentifiersToInteractions();
                boolean answered = true;

                if ("NONATTEMPTED".equalsIgnoreCase(jsonScenario.getScenarioName())) {
                    answered = false;
                }

                for (int idx = 0; idx < itemResponse.getOutcomes().size(); idx++) {
                    Outcome outcome = itemResponse.getOutcomes().get(idx);
                    String outcomeVersion = outcome.getVersion();
                    BigDecimal point = new BigDecimal(outcome.getSCORE());

                    @SuppressWarnings("rawtypes")
                    List<Mod> mods = itemResponse.getMods();
                    List<Mod> updatedMods = new ArrayList<>();

                    for (Mod mod : mods) {
                        Mod updatedMod = new Mod();

                        updatedMod.setDid(mod.getDid());
                        updatedMod.setIt(mod.getIt());

                        if (mod.getResponses() != null && mod.getResponses().size() >= idx + 1) {
                            List<Response> responses = mod.getResponses();
                            for (Response response : responses) {
                                String version = response.getVersion();
                                if (version.equalsIgnoreCase(outcomeVersion)) {
                                    updatedMod.setR(response.getR());
                                    break;
                                }
                            }
                        } else {
                            updatedMod.setR(mod.getR());
                        }

                        updatedMod.setId(mod.getId());
                        updatedMod.setSid(mod.getSid());
                        updatedMods.add(updatedMod);
                    }
                    ScenarioItem scenarioItem = new ScenarioItem(uin, point, answered, g.toJson(identifierToInteraction), g.toJson(updatedMods));
                    scenarioItems.add(scenarioItem);
                }
            }

            result = new Scenario(scenarioItems);
        } catch (JsonSyntaxException ex) {
            Logger.error("Event=\"Failed to convert Json Scenario to TestnavAPIDataLoader Scenario\", Scenario={}",
                    jsonScenarioString, ex);
        }
        return result;
    }

    /**
     * Represents the result of Tn8ApiDataLoader contains the information error message, content key, wire key and
     * testDef url.
     */
    private static class Tn8ApiDataLoaderResult {
        private final String errorMessage;
        private final String contentKey;
        private final String wireKey;
        private final String urlGetTestDef;

        public Tn8ApiDataLoaderResult(String errorMessage) {
            this.errorMessage = errorMessage;
            this.contentKey = "";
            this.wireKey = "";
            this.urlGetTestDef = "";
        }

        public Tn8ApiDataLoaderResult(String errorMessage, String contentKey, String wireKey, String urlGetTestDef) {
            this.errorMessage = errorMessage;
            this.contentKey = contentKey;
            this.wireKey = wireKey;
            this.urlGetTestDef = urlGetTestDef;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public String getContentKey() {
            return contentKey;
        }

        public String getWireKey() {
            return wireKey;
        }

        public String getUrlGetTestDef() {
            return urlGetTestDef;
        }
    }
}