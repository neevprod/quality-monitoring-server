package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.mongodb.MongoClient;
import com.pearson.itautomation.bevalidation.exception.BackendDatabaseException;
import com.pearson.itautomation.stats.service.MongoInfo;
import com.pearson.itautomation.testmaps.models.enums.ItemStatus;
import com.typesafe.config.Config;
import jobs.JobProtocol;
import jobs.systemvalidation.BackendValidationActor.BackendInputMessage;
import jobs.systemvalidation.StatsValidationActor.StatsInputMessage;
import models.HumanUser;
import models.systemvalidation.*;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.akka.InjectedActorSupport;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import services.systemvalidation.*;
import services.usermanagement.HumanUserService;
import util.ConnectionPoolManager;
import util.systemvalidation.BackendData;
import util.systemvalidation.DivJobSetupUtil;
import util.systemvalidation.DivJobSetupUtil.StatusNames;
import util.systemvalidation.T3TestMapCache;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.sql.DataSource;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static jobs.systemvalidation.DataIntegrityValidationActor.DivStateName.*;

/**
 * @author Nand Joshi
 */
public class DataIntegrityValidationActor extends UntypedAbstractActor implements InjectedActorSupport {
    public static final String FINISHED_MESSAGE = "Finished";
    static final String FAILED_MESSAGE = "Failed";
    private final Tn8ApiDataLoaderActor.Factory tn8ApiDataLoaderActorFactory;
    private final IrisValidationActor.Factory irisValidationFactory;
    private final BackendValidationActor.Factory beValidationFactory;
    private final Epen2ValidationActor.Factory epen2ValidationFactory;
    private final StatsValidationActor.Factory statsValidationFactory;
    private final BatteryStatsValidationActor.Factory batteryValidationFactory;
    private final Provider<Email> emailProvider;
    private final MailerClient mailerClient;
    private final String fromAddress;
    private final int numberOfChildren;
    private final String baseUrl;
    private final JPAApi jpaApi;
    private final EnvironmentService environmentService;
    private final StudentScenarioService studentScenarioService;
    private final StudentScenarioResultService studentScenarioResultService;
    private final HumanUserService humanUserService;
    private final DivJobExecService divJobExecService;
    private final DivJobExecStatusService divJobExecStatusService;
    private final StudentScenarioStatusService studentScenarioStatusService;
    private final ScenarioService scenarioService;
    /**
     * Currently Running Student.
     */
    private final List<StudentScenario> runningStudentScenarios;
    /**
     * Student successfully completed.
     */
    private final List<StudentScenario> completedStudentScenarios;

    /**
     * Student completed with error.
     */
    private final Set<StudentScenario> errorStudentScenarios;
    private final List<StudentScenario> pendingStudentScenarios;
    private final Map<Integer, State> pathStateOrderAndStates;
    private final BackendData.Factory backendDataFactory;
    private final ConnectionPoolManager connectionPoolManager;
    private final Set<String> studentsInBatteryStatsState;
    private Instant startTime;
    private ActorRef sender;
    private long divJobExecId;
    private int environmentId;
    private String environmentName;
    private String userEmailAddress;
    private int totalStudentScenarios;
    private int totalPreviouslyCompletedStudentScenarios;
    private Map<String, List<StudentScenario>> batteryStudentScenarios;
    private String jobId;
    private Boolean isBatteryEmail = false;
    private Integer finalPathStateOrder;

    /**
     * @param jpaApi                       The instance of play JPAApi
     * @param divJobExecService            The {@link DivJobExecService}
     * @param studentScenarioService       The {@link StudentScenarioService}
     * @param studentScenarioResultService The {@link StudentScenarioResultService}
     * @param tn8ApiDataLoaderActorFactory The Factory for Tn8ApiDataLoaderActor
     * @param irisValidationFactory        The Factory for IrisValidationActor
     */
    @Inject
    public DataIntegrityValidationActor(final JPAApi jpaApi, final DivJobExecService divJobExecService,
                                        final EnvironmentService environmentService, final StudentScenarioService studentScenarioService,
                                        final StudentScenarioResultService studentScenarioResultService, final HumanUserService humanUserService,
                                        final DivJobExecStatusService divJobExecStatusService,
                                        final StudentScenarioStatusService studentScenarioStatusService,
                                        final ScenarioService scenarioService,
                                        final Tn8ApiDataLoaderActor.Factory tn8ApiDataLoaderActorFactory,
                                        final IrisValidationActor.Factory irisValidationFactory,
                                        final BackendValidationActor.Factory beValidationFactory,
                                        final Epen2ValidationActor.Factory epen2ValidationFactory,
                                        final StatsValidationActor.Factory statsValidationFactory,
                                        final BatteryStatsValidationActor.Factory batteryValidationFactory,
                                        final BackendData.Factory backendDataFactory, Provider<Email> emailProvider, MailerClient mailerClient,
                                        final ConnectionPoolManager connectionPoolManager, final Config configuration) {

        this.jpaApi = jpaApi;
        this.divJobExecService = divJobExecService;
        this.environmentService = environmentService;
        this.studentScenarioService = studentScenarioService;
        this.studentScenarioResultService = studentScenarioResultService;
        this.humanUserService = humanUserService;
        this.divJobExecStatusService = divJobExecStatusService;
        this.studentScenarioStatusService = studentScenarioStatusService;
        this.scenarioService = scenarioService;
        this.tn8ApiDataLoaderActorFactory = tn8ApiDataLoaderActorFactory;
        this.irisValidationFactory = irisValidationFactory;
        this.beValidationFactory = beValidationFactory;
        this.epen2ValidationFactory = epen2ValidationFactory;
        this.statsValidationFactory = statsValidationFactory;

        this.batteryValidationFactory = batteryValidationFactory;

        this.backendDataFactory = backendDataFactory;

        this.emailProvider = emailProvider;
        this.mailerClient = mailerClient;
        this.connectionPoolManager = connectionPoolManager;
        runningStudentScenarios = new ArrayList<>();
        completedStudentScenarios = new ArrayList<>();
        errorStudentScenarios = new HashSet<>();
        pendingStudentScenarios = new ArrayList<>();
        numberOfChildren = configuration.getInt("application.jobs.dataIntegrityValidation.maxConcurrentStudents");
        baseUrl = configuration.getString("application.baseUrl");
        fromAddress = configuration.getString("application.email.fromAddress");
        studentsInBatteryStatsState = new HashSet<>();
        pathStateOrderAndStates = new HashMap<>();
    }

    static String getErrorJson(final String errorMessage) {
        JsonObject errorMsg = new JsonObject();
        errorMsg.addProperty("success", "no");
        errorMsg.addProperty("message", errorMessage);
        return errorMsg.toString();
    }

    @Override
    public void onReceive(Object message) {
        try {
            if (message instanceof RunMessage) {
                sender = getSender();

                divJobExecId = ((RunMessage) message).getDivJobExecId();
                jobId = ((RunMessage) message).getJobId();
                Optional<DivJobExec> divJobExecResult = jpaApi
                        .withTransaction(() -> divJobExecService.findByDivJobExecId(divJobExecId));
                if (divJobExecResult.isPresent()) {
                    final DivJobExec divJobExec = divJobExecResult.get();
                    environmentId = divJobExec.getEnvironment().getEnvironmentId();
                    environmentName = divJobExec.getEnvironment().getName();
                    userEmailAddress = jpaApi
                            .withTransaction(() -> humanUserService.findByUserId(divJobExec.getSubmitUserId()))
                            .map(HumanUser::getEmail).orElse(null);

                    jpaApi.withTransaction(
                            () -> divJobExecStatusService.findByDivJobExecStatusName(StatusNames.IN_PROGRESS.getName())
                                    .ifPresent(status -> updateJobStatus(divJobExec.getDivJobExecId(),
                                            status.getDivJobExecStatusId(), false)));

                    // Collects the pathStateOrderAndStates from divJobStates
                    pathStateOrderAndStates.putAll(divJobExec.getPath().getPathStates().stream()
                            .sorted(Comparator.comparing(PathState::getPathStateOrder))
                            .collect(Collectors.toMap(PathState::getPathStateOrder, PathState::getState)));

                    finalPathStateOrder = pathStateOrderAndStates.keySet().stream().mapToInt(v -> v).max().getAsInt();

                    batteryStudentScenarios = new HashMap<>();
                    for (StudentScenario studentScenario : divJobExec.getStudentScenarios()) {
                        if (studentScenario.getBatteryStudentScenario() == null) {
                            break;
                        }

                        if (finalPathStateOrder == getPathStateOrder(studentScenario.getLastCompletedStateId())) {
                            changeInProgressStudentScenarioStatusToComplete(studentScenario);
                            continue;
                        }
                        String batteryUuid = studentScenario.getBatteryStudentScenario().getBatteryUuid();
                        if (batteryStudentScenarios.containsKey(batteryUuid)) {
                            batteryStudentScenarios.get(batteryUuid).add(studentScenario);
                        } else {
                            List<StudentScenario> studentScenarios = new ArrayList<>();
                            studentScenarios.add(studentScenario);
                            batteryStudentScenarios.put(batteryUuid, studentScenarios);
                        }
                    }

                    if (batteryStudentScenarios.isEmpty()) {
                        totalStudentScenarios = divJobExec.getNumStudentScenarios();
                        pendingStudentScenarios.addAll(divJobExec.getStudentScenarios().stream()
                                .filter(ss -> !(finalPathStateOrder == getPathStateOrder(ss.getLastCompletedStateId())))
                                .collect(Collectors.toList()));
                        totalPreviouslyCompletedStudentScenarios = totalStudentScenarios
                                - pendingStudentScenarios.size();
                        runningStudentScenarios.addAll(getChildList(pendingStudentScenarios));
                    } else {
                        isBatteryEmail = true;
                        List<StudentScenario> notStartedUnitsFormAllBattery = new ArrayList<>();
                        for (Map.Entry<String, List<StudentScenario>> entry : batteryStudentScenarios.entrySet()) {
                            totalStudentScenarios += entry.getValue().size();

                            List<StudentScenario> startedUnits = entry.getValue().stream()
                                    .filter(ss -> ss.getLastCompletedStateId() != null).collect(Collectors.toList());
                            // If Battery is already started and passed TADL state then add all units as
                            // pendingStudentScenarios.
                            pendingStudentScenarios.addAll(startedUnits);

                            if (startedUnits.size() != entry.getValue().size()) {
                                // If All units are not submitted then select the last unit that has
                                // lastCompletedStateId is null (i.e TADL state is not passed) then add that to
                                // pendingStudentScenarios
                                List<StudentScenario> notStartedUnits = entry.getValue().stream()
                                        .filter(ss -> ss.getLastCompletedStateId() == null)
                                        .collect(Collectors.toList());
                                if (!notStartedUnits.isEmpty()) {
                                    pendingStudentScenarios.add(notStartedUnits.remove(notStartedUnits.size() - 1));
                                    notStartedUnitsFormAllBattery.addAll(notStartedUnits);
                                }
                            }
                        }
                        totalPreviouslyCompletedStudentScenarios = totalStudentScenarios
                                - (pendingStudentScenarios.size() + notStartedUnitsFormAllBattery.size());
                        List<StudentScenario> studentScenarioSubListToRun = getChildList(pendingStudentScenarios);
                        pendingStudentScenarios.removeAll(studentScenarioSubListToRun);
                        runningStudentScenarios.addAll(studentScenarioSubListToRun);
                    }

                    startTime = Instant.now();

                    if (runningStudentScenarios.isEmpty()) {
                        //If a failed job is resumed and all studentScenarios are completed
                        jpaApi.withTransaction(() -> divJobExecStatusService
                                .findByDivJobExecStatusName(StatusNames.COMPLETED.getName()).ifPresent(
                                        status -> updateJobStatus(divJobExecId, status.getDivJobExecStatusId(), true)));

                        T3TestMapCache.removeT3TestMapFromCache(divJobExecId);
                        sendCompletionEmail(true);
                        sender.tell(new JobProtocol.JobFinishMessage(jobId, true), getSelf());
                        getContext().stop(getSelf());
                    }
                    do {
                        beginStudentScenariosExecution();
                        if (runningStudentScenarios.isEmpty()) {
                            List<StudentScenario> studentScenarioSubListToRun = getChildList(pendingStudentScenarios);
                            pendingStudentScenarios.removeAll(studentScenarioSubListToRun);
                            runningStudentScenarios.addAll(studentScenarioSubListToRun);
                        } else {
                            break;
                        }
                    } while (!runningStudentScenarios.isEmpty());
                } else {
                    Logger.error("event=\"Cannot find DIV job exec with the given ID.\", divJobExecId=\"{}\", jobId=\"{}\"", divJobExecId, jobId);
                    sender.tell(new JobProtocol.JobFinishMessage(jobId, false), getSelf());
                    getContext().stop(getSelf());
                }

            } else if (message instanceof FinishMessage) {
                FinishMessage finishMsg = (FinishMessage) message;
                processFinishMessage(finishMsg);
            }
        } catch (Throwable t) {
            try {
                Logger.error("event=\"Error occurred while executing DIV job.\", jobId={}", jobId, t);
                jpaApi.withTransaction(() -> divJobExecStatusService
                        .findByDivJobExecStatusName(StatusNames.FAILED.getName())
                        .ifPresent(status -> updateJobStatus(divJobExecId, status.getDivJobExecStatusId(), false)));
                T3TestMapCache.removeT3TestMapFromCache(divJobExecId);
                sendCompletionEmail(false);

            } finally {
                T3TestMapCache.removeT3TestMapFromCache(divJobExecId);
                sender.tell(new JobProtocol.JobFinishMessage(jobId, false), getSelf());
                getContext().stop(getSelf());
            }
        }
    }

    private void beginStudentScenariosExecution() {
        List<StudentScenario> studentsNeedToStop = new ArrayList<>();
        for (StudentScenario studentScenario : runningStudentScenarios) {
            jpaApi.withTransaction(() -> DivJobSetupUtil.initializeLazyFetchedData(studentScenarioService, studentScenario));
            State currentState = pathStateOrderAndStates.get(pathStateOrderAndStates.keySet().stream().mapToInt(v -> v).min().getAsInt());

            if (studentScenario.getLastCompletedStateId() != null) {

                currentState = getNextState(studentScenario.getLastCompletedStateId()).orElse(null);
                boolean epenLastState = false;
                if (currentState != null && studentScenario.getEpenScenario() == null) {
                    while (BE_EPEN_PATH_VALIDATOR.name().equals(currentState.getStateName())
                            || EPEN2_AUTOMATION.name().equals(currentState.getStateName())) {
                        Optional<State> temp = getNextState(currentState.getStateId());
                        if (temp.isPresent()) {
                            currentState = temp.get();
                        } else {
                            epenLastState = true;
                            break;
                        }
                    }
                    if (epenLastState) {
                        continue;
                    }
                }
                studentScenario.setState(currentState);
            }
            if (studentScenario.getState() == null) {
                State firstStateInPath = studentScenario.getDivJobExec().getPath().getPathStates().get(0).getState();
                studentScenario.setState(firstStateInPath);
            }
            if (DivStateName.BATTERY_STATS.name().equals(studentScenario.getState().getStateName())) {
                String batteryUuid = studentScenario.getBatteryStudentScenario().getBatteryUuid();
                if (!studentsInBatteryStatsState.contains(batteryUuid)) {
                    StudentScenarioResult studentScenarioResult = new StudentScenarioResult();
                    List<StudentScenario> batteryUnits = jpaApi.withTransaction(() -> studentScenarioService.findByDivJobExecIdAndBatteryStudentScenarioId(divJobExecId, studentScenario.getBatteryStudentScenario().getBatteryStudentScenarioId(), environmentId));

                    if (!updateStudentScenarioResult(batteryUnits, studentScenarioResult)) {
                        studentScenarioResult.setStudentScenario(studentScenario);
                    }

                    if (studentScenarioResult.getStudentScenario() != null
                            && studentScenarioResult.getStudentScenario().getStudentScenarioId()
                            .equals(studentScenario.getStudentScenarioId())) {
                        studentsInBatteryStatsState.add(batteryUuid);

                        jpaApi.withTransaction(() -> studentScenarioStatusService
                                .findByStudentScenarioStatusName(StatusNames.IN_PROGRESS.getName())
                                .ifPresent(status -> updateStudentScenarioStatus(studentScenario,
                                        status.getStudentScenarioStatusId())));

                        startChildren(currentState, studentScenario);
                    } else {
                        changeInProgressStudentScenarioStatusToComplete(studentScenario);
                        studentsNeedToStop.add(studentScenario);
                    }
                } else {
                    changeInProgressStudentScenarioStatusToComplete(studentScenario);
                    studentsNeedToStop.add(studentScenario);
                }
            } else {
                jpaApi.withTransaction(() -> studentScenarioStatusService
                        .findByStudentScenarioStatusName(StatusNames.IN_PROGRESS.getName())
                        .ifPresent(status -> updateStudentScenarioStatus(studentScenario,
                                status.getStudentScenarioStatusId())));
                startChildren(currentState, studentScenario);
            }
        }
        runningStudentScenarios.removeAll(studentsNeedToStop);
    }


    private void changeInProgressStudentScenarioStatusToComplete(StudentScenario studentScenario) {
        if (StatusNames.IN_PROGRESS.getName().equals(studentScenario.getStudentScenarioStatus().getStatus())) {
            jpaApi.withTransaction(() -> {
                Long studentScenarioId = studentScenario.getStudentScenarioId();
                List<StudentScenarioResult> res = studentScenarioResultService.findByStudentScenarioIdAndStateId(studentScenarioId, studentScenario.getLastCompletedStateId(), environmentId);
                String success = new JsonParser().parse(res.stream().max(Comparator.comparing(StudentScenarioResult::getVersion)).get().getResultDetails()).getAsJsonObject().get("success").getAsString();
                String statusName = StatusNames.FAILED.getName();
                if ("yes".equals(success)) {
                    statusName = StatusNames.COMPLETED.getName();

                }
                Logger.info("event= \"Updating student scenario status\", studentScenarioId= \"{}\", status= \"{}\"", studentScenarioId, statusName);

                studentScenarioStatusService.findByStudentScenarioStatusName(statusName)
                        .ifPresent(status -> updateStudentScenarioStatus(studentScenario, status.getStudentScenarioStatusId()));
            });
        }
    }

    /**
     * @param finishMsg The instance of {@link FinishMessage}
     * @throws Throwable If any error occurred while executing job
     */
    private void processFinishMessage(FinishMessage finishMsg) throws Throwable {
        saveMessageToDatabase(finishMsg);
        Duration runDuration = Duration.between(startTime, Instant.now());
        StudentScenario studentScenario = finishMsg.getStudentScenario();
        Long studentScenarioId = studentScenario.getStudentScenarioId();
        State currentState = studentScenario.getState();
        String stateName = currentState.getStateName();
        if (finishMsg.getFinishMessage().equals(FINISHED_MESSAGE)) {
            Logger.info("event=\"Finished step in DIV job.\", divJobExecId= \"{}\", studentScenarioId=\"{}\", state=\"{}\", jobId=\"{}\",  duration=\"{} ms\"",
                    divJobExecId, studentScenarioId, stateName, jobId, runDuration.toMillis());

            if (!batteryStudentScenarios.isEmpty() && TN8_API_DATA_LOADER.name().equals(stateName)) {
                submitNextUnit(studentScenario);
            }

            // To update lastCompletedState
            updateLastCompletedState(studentScenario);

            Optional<State> nextState = getNextState(currentState.getStateId());

            if (studentScenario.getEpenScenario() == null && nextState.isPresent()) {
                State tempState = nextState.get();
                boolean hasNextState = true;
                while (hasNextState && (BE_EPEN_PATH_VALIDATOR.name().equals(tempState.getStateName())
                        || EPEN2_AUTOMATION.name().equals(tempState.getStateName()))) {
                    nextState = getNextState(tempState.getStateId());
                    if (nextState.isPresent()) {
                        tempState = nextState.get();
                    } else {
                        hasNextState = false;
                    }
                }
            }

            if (nextState.isPresent()) {
                if (DivStateName.BATTERY_STATS.name().equals(nextState.get().getStateName())) {
                    executeBatteryState(studentScenario, finishMsg.getFinishMessage());
                } else {
                    startChildren(nextState.get(), studentScenario);
                }
            } else {
                completedStudentScenarios.add(studentScenario);
                updateStatusAndStartNewStudent(finishMsg);
            }
        } else if (finishMsg.getFinishMessage().equals(FAILED_MESSAGE)) {
            Logger.error("event=\"Failed step in DIV job.\", divJobExecId= \"{}\", studentScenarioId=\"{}\", state=\"{}\", jobId=\"{}\", duration=\"{} ms\"",
                    divJobExecId, studentScenarioId, stateName, jobId, runDuration.toMillis());
            errorStudentScenarios.add(studentScenario);
            Optional<State> nextState = getNextState(currentState.getStateId());
            if (nextState.isPresent() && DivStateName.BATTERY_STATS.name().equals(nextState.get().getStateName())) {
                executeBatteryState(studentScenario, finishMsg.getFinishMessage());
            } else {
                if (!batteryStudentScenarios.isEmpty()
                        && TN8_API_DATA_LOADER.name().equals(stateName)) {
                    submitNextUnit(studentScenario);
                }
                updateStatusAndStartNewStudent(finishMsg);
            }
        }
    }

    /**
     * For the battery test, it will execute next unit within a battery.
     *
     * @param studentScenario The instance of {@link StudentScenario}
     */
    private void submitNextUnit(final StudentScenario studentScenario) {
        List<StudentScenario> totalUnits = batteryStudentScenarios
                .get(studentScenario.getBatteryStudentScenario().getBatteryUuid());

        StudentScenario attemptedUnit = totalUnits.stream().filter(ss -> ss.getUuid().equals(studentScenario.getUuid()))
                .findFirst().orElse(null);

        totalUnits.remove(attemptedUnit);

        if (totalUnits.isEmpty()) {
            batteryStudentScenarios.remove(studentScenario.getBatteryStudentScenario().getBatteryUuid());
        } else {
            StudentScenario nextUnit = totalUnits.iterator().next();
            // If numbers of students reached to maximum limit or a test attempt is already done through TADL 
            // then add next unit to queue otherwise start execution.
            if (runningStudentScenarios.size() >= numberOfChildren || nextUnit.getLastCompletedStateId() != null) {
                pendingStudentScenarios.add(nextUnit);
            } else {
                jpaApi.withTransaction(() -> {
                    studentScenarioStatusService
                            .findByStudentScenarioStatusName(StatusNames.IN_PROGRESS.getName()).ifPresent(
                            status -> updateStudentScenarioStatus(nextUnit, status.getStudentScenarioStatusId()));
                    DivJobSetupUtil.initializeLazyFetchedData(studentScenarioService, nextUnit);
                });

                startChildren(studentScenario.getState(), nextUnit);
                runningStudentScenarios.add(nextUnit);
            }
        }
    }

    /**
     * This method is executed only for battery test. This method performs the followings:
     * <ul>
     * <li>Executes BATTERY_STATS for a battery if responses of all units are in backend</li>
     * <li>Executes BATTERY_STATS for only one unit from a battery</li>
     * <li>If a BATTERY_STATS state if failed and while job is resumed it will execute the same unit BATTERY_STATS</li>
     * </ul>
     *
     * @param studentScenario The studentScenario
     * @param finishMsg       The finish message (Finished or Failed)
     */
    private void executeBatteryState(final StudentScenario studentScenario, String finishMsg) {

        BatteryStudentScenario batteryStudentScenario = studentScenario.getBatteryStudentScenario();
        List<StudentScenario> batteryUnits = new ArrayList<>();
        StudentScenarioResult studentScenarioResult = new StudentScenarioResult();
        jpaApi.withTransaction(() -> {
            batteryUnits.addAll(studentScenarioService.findByDivJobExecIdAndBatteryStudentScenarioId(divJobExecId,
                    batteryStudentScenario.getBatteryStudentScenarioId(), environmentId));
            updateStudentScenarioResult(batteryUnits, studentScenarioResult);
        });

        int pathStateOrder;
        if (studentScenario.getEpenScenario() != null) {
            pathStateOrder = getPathStateOrderForBatteryWaitingState(DivStateName.BE_EPEN_PATH_VALIDATOR.name());
        } else {
            pathStateOrder = getPathStateOrderForBatteryWaitingState(DivStateName.BE_PRIMARY_PATH_VALIDATOR.name());
        }

        long beSuccessUnits = batteryUnits.stream().filter(ss -> stateIsUpToGivenPoint(ss, pathStateOrder))
                .count();
        String batteryUuid = batteryStudentScenario.getBatteryUuid();
        StudentScenario studentScenarioFailedInBatteryState = studentScenarioResult.getStudentScenario();

        if (batteryUnits.size() == beSuccessUnits && !studentsInBatteryStatsState.contains(batteryUuid)) {
            // If a unit is already failed in BATTERY_STATS then always run BATTERY_STATS for that
            // studentScenario in a battery.
            if (studentScenarioFailedInBatteryState == null || studentScenarioFailedInBatteryState
                    .getStudentScenarioId().equals(studentScenario.getStudentScenarioId())) {
                studentsInBatteryStatsState.add(studentScenario.getBatteryStudentScenario().getBatteryUuid());
                startChildren(getNextState(studentScenario.getState().getStateId()).get(), studentScenario);
            } else {
                if (FINISHED_MESSAGE.equals(finishMsg)) {
                    jpaApi.withTransaction(() -> studentScenarioStatusService
                            .findByStudentScenarioStatusName(StatusNames.COMPLETED.getName())
                            .ifPresent(status -> updateStudentScenarioStatus(studentScenario,
                                    status.getStudentScenarioStatusId())));
                } else {
                    jpaApi.withTransaction(() -> studentScenarioStatusService
                            .findByStudentScenarioStatusName(StatusNames.FAILED.getName())
                            .ifPresent(status -> updateStudentScenarioStatus(studentScenario,
                                    status.getStudentScenarioStatusId())));
                }
                updateStatusAndStartNewStudent(new FinishMessage(studentScenario,
                        studentScenario.getDivJobExec().getDivJobExecId(), finishMsg, ""));
                completedStudentScenarios.add(studentScenario);
            }
        } else {
            if (FINISHED_MESSAGE.equals(finishMsg)) {
                jpaApi.withTransaction(() -> studentScenarioStatusService
                        .findByStudentScenarioStatusName(StatusNames.COMPLETED.getName())
                        .ifPresent(status -> updateStudentScenarioStatus(studentScenario,
                                status.getStudentScenarioStatusId())));
            } else {
                jpaApi.withTransaction(() -> studentScenarioStatusService
                        .findByStudentScenarioStatusName(StatusNames.FAILED.getName())
                        .ifPresent(status -> updateStudentScenarioStatus(studentScenario,
                                status.getStudentScenarioStatusId())));
            }
            updateStatusAndStartNewStudent(new FinishMessage(studentScenario,
                    studentScenario.getDivJobExec().getDivJobExecId(), finishMsg, ""));
            completedStudentScenarios.add(studentScenario);
            if (runningStudentScenarios.isEmpty()) {
                T3TestMapCache.removeT3TestMapFromCache(divJobExecId);
                stopDivJob(errorStudentScenarios.isEmpty(), true);
            }
        }
    }

    private int getPathStateOrderForBatteryWaitingState(String batteryWaitingStateName) {
        return pathStateOrderAndStates.entrySet().stream()
                .filter(map -> batteryWaitingStateName.equals(map.getValue().getStateName()))
                .mapToInt(Map.Entry::getKey).findFirst()
                .orElse(-1);
    }

    private boolean updateStudentScenarioResult(List<StudentScenario> batteryUnits, StudentScenarioResult studentScenarioResult) {
        boolean foundStudentScenarioResult = false;
        for (StudentScenario ss : batteryUnits) {
            List<StudentScenarioResult> studentScenarioResults = jpaApi.withTransaction(() -> studentScenarioResultService
                    .findByStudentScenarioIdAndStateName(ss.getStudentScenarioId(),
                            DivStateName.BATTERY_STATS.name()));

            Optional<StudentScenarioResult> maxVersionSSR = studentScenarioResults.stream()
                    .max(Comparator.comparing(StudentScenarioResult::getVersion));

            if (maxVersionSSR.isPresent()) {
                foundStudentScenarioResult = true;
                studentScenarioResult.setSuccess(maxVersionSSR.get().isSuccess());
                studentScenarioResult.setStudentScenario(maxVersionSSR.get().getStudentScenario());
                break;
            }
        }
        return foundStudentScenarioResult;
    }

    /**
     * Executes the given state for the given student.
     *
     * @param state           The new {@link State} for the <code>studentScenario</code> The {@link State} to start for the given
     *                        {@link StudentScenario}
     * @param studentScenario The current running student scenario The {@link StudentScenario}
     */
    private void startChildren(final State state, final StudentScenario studentScenario) {
        Long studentScenarioId = studentScenario.getStudentScenarioId();
        studentScenario.setState(state);
        if (DivStateName.TN8_API_DATA_LOADER.name().equals(state.getStateName())) {
            ActorRef tn8ApiDataLoaderActor = injectedChild(tn8ApiDataLoaderActorFactory::create,
                    "tniApiDataLoaderActor_" + System.currentTimeMillis() + "_"
                            + studentScenarioId + "_" + studentScenario.getUuid());
            tn8ApiDataLoaderActor.tell(studentScenario, getSelf());

        } else if (DivStateName.IRIS_PROCESSING_VALIDATOR.name().equals(state.getStateName())) {
            ActorRef irisValidationActor = injectedChild(irisValidationFactory::create, "irisValidationActor_"
                    + System.currentTimeMillis() + "_" + studentScenarioId);
            irisValidationActor.tell(studentScenario, getSelf());
        } else if (studentScenario.getEpenScenario() != null
                && BE_EPEN_PATH_VALIDATOR.name().equals(state.getStateName())) {
            BackendInputMessage inputMessage = new BackendInputMessage(true, studentScenario);

            ActorRef beValidationActor = injectedChild(beValidationFactory::create, "beValidationActor_"
                    + System.currentTimeMillis() + "_" + studentScenarioId);
            beValidationActor.tell(inputMessage, getSelf());
        } else if (studentScenario.getEpenScenario() != null
                && DivStateName.EPEN2_AUTOMATION.name().equals(state.getStateName())) {

            String epenScenarioWithoutItemStatus = selectScoreEligibleItems(studentScenario);

            studentScenario.getEpenScenario().setScenario(epenScenarioWithoutItemStatus);


            ActorRef epenValidationActor = injectedChild(epen2ValidationFactory::create, "epen2ValidationActor_"
                    + System.currentTimeMillis() + "_" + studentScenarioId);
            epenValidationActor.tell(studentScenario, getSelf());
        } else if (DivStateName.BE_PRIMARY_PATH_VALIDATOR.name().equals(state.getStateName())) {
            BackendInputMessage inputMessage = new BackendInputMessage(false, studentScenario);

            ActorRef beValidationActor = injectedChild(beValidationFactory::create, "beValidationActor_"
                    + System.currentTimeMillis() + "_" + studentScenarioId);
            beValidationActor.tell(inputMessage, getSelf());
        } else if (DivStateName.STATS.name().equals(state.getStateName())) {
            StatsInputMessage inputMessage;
            loadLazyFetchTestMapDetail(studentScenario);

            try {
                inputMessage = getStatsInputMessage(studentScenario);
            } catch (BackendDatabaseException e) {
                inputMessage = new StatsInputMessage(studentScenario, null, null, null, null, null, null);
                inputMessage.setFailedMessage(e.getMessage());
            }


            ActorRef statsValidationActor = injectedChild(statsValidationFactory::create, "statsValidationActor_"
                    + System.currentTimeMillis() + "_" + studentScenarioId);
            statsValidationActor.tell(inputMessage, getSelf());

        } else if (DivStateName.BATTERY_STATS.name().equals(state.getStateName())) {
            StatsInputMessage inputMessage;
            try {
                inputMessage = getBatteryStatsInputMessage(studentScenario);
            } catch (BackendDatabaseException e) {
                inputMessage = new StatsInputMessage(studentScenario, null, null, null, null, null, null);
                inputMessage.setFailedMessage(e.getMessage());
            }
            ActorRef batteryStatsValidationActor = injectedChild(batteryValidationFactory::create,
                    "batteryStatsValidationActor_" + System.currentTimeMillis() + "_"
                            + studentScenarioId);
            batteryStatsValidationActor.tell(inputMessage, getSelf());
        }
    }

    private void loadLazyFetchTestMapDetail(StudentScenario studentScenario) {
        Optional<Scenario> testNavScenario = jpaApi.withTransaction(
                () -> scenarioService.findByScenarioId(
                        studentScenario.getTestnavScenario().getScenarioId()));
        testNavScenario.ifPresent(studentScenario::setTestnavScenario);
    }

    private String selectScoreEligibleItems(StudentScenario studentScenario) {
        String scopeTreePath = studentScenario.getDivJobExec().getScopeTreePath();
        Environment environment = getEnvironment().get();
        DbConnection beDbConnection = environment.getBeMysqlConnection();
        BackendData backendData = backendDataFactory.create(beDbConnection);
        Set<ItemStatus> scoreEligibleItemStatuses = backendData.getItemStatusesEligibleForEpenScoring(scopeTreePath);
        String epenScenario = studentScenario.getEpenScenario().getScenario();
        DocumentContext context = JsonPath.parse(epenScenario);

        if (scoreEligibleItemStatuses.size() > 0) {
            StringBuilder predicate = new StringBuilder("$.data[?(");
            Iterator<ItemStatus> iterator = scoreEligibleItemStatuses.iterator();
            while (iterator.hasNext()) {
                ItemStatus is = iterator.next();
                predicate.append("@.itemStatus == '");
                predicate.append(is.name());
                predicate.append("'");
                if (iterator.hasNext()) {
                    predicate.append(" || ");
                }
            }
            predicate.append(")]");
            List<String> filteredData = context.read(predicate.toString());
            context.set("$.data", filteredData);
            context.delete("$.data[*].itemStatus");
            return context.jsonString();
        } else {
            context.delete("$.data[*]");
            return context.jsonString();
        }
    }

    /**
     * Saves the result into the database.
     *
     * @param finishMsg The instance of {@link FinishMessage}
     */
    private void saveMessageToDatabase(final FinishMessage finishMsg) {
        StudentScenarioResult scenarioResult = new StudentScenarioResult();
        final String message = finishMsg.getErrorMessage();
        final StudentScenario studentScenario = finishMsg.getStudentScenario();
        final String success = new JsonParser().parse(message).getAsJsonObject().get("success").getAsString();

        // Setting the value of success in StudentScenarioResult table.
        boolean successFlag = true;
        if ("no".equalsIgnoreCase(success)) {
            successFlag = false;
        }
        scenarioResult.setSuccess(successFlag);

        // Setting the value of version in StudentScenarioResult table.
        jpaApi.withTransaction(() -> {
            Optional<Integer> maxVersion = studentScenarioResultService.findMaxVersionByStudentScenarioIdAndStateId(
                    studentScenario.getStudentScenarioId(), studentScenario.getState().getStateId());
            if (maxVersion.isPresent()) {
                scenarioResult.setVersion(maxVersion.get() + 1);
            } else {
                scenarioResult.setVersion(1);
            }

        });

        scenarioResult.setResultDetails(finishMsg.getErrorMessage());
        scenarioResult.setStateId(finishMsg.getStudentScenario().getState().getStateId());
        scenarioResult.setStudentScenarioId(finishMsg.getStudentScenario().getStudentScenarioId());
        scenarioResult.setTimestamp(Timestamp.from(Instant.now()));

        jpaApi.withTransaction(() -> studentScenarioResultService.create(scenarioResult));

    }

    /**
     * Marks the current studentScenario as completed and removes it from the list of runningStudentScenarios.<br>
     * If the list of pendingStudentScenarios is not empty then starts the execution of first studentScenario from the
     * pendingStudentScenario and adds this to runningStudentScenario.<br>
     * If runningStudentScenario is empty then stops the DIV job execution.
     *
     * @param finishMsg The instance of {@link FinishMessage}
     */
    private void updateStatusAndStartNewStudent(final FinishMessage finishMsg) {
        final StudentScenario currentStudentScenario = finishMsg.getStudentScenario();

        updateStudentScenario(finishMsg.getFinishMessage(), currentStudentScenario);

        // Removing completed StudentScenario from running list
        runningStudentScenarios.remove(currentStudentScenario);
        if (!pendingStudentScenarios.isEmpty() && runningStudentScenarios.size() < numberOfChildren) {
            final StudentScenario newStudentScenario = pendingStudentScenarios.remove(0);

            // Adding pending StudentScenario to running list
            runningStudentScenarios.add(newStudentScenario);

            jpaApi.withTransaction(() -> studentScenarioStatusService
                    .findByStudentScenarioStatusName(StatusNames.IN_PROGRESS.getName())
                    .ifPresent(status -> updateStudentScenarioStatus(newStudentScenario,
                            status.getStudentScenarioStatusId())));

            State currentState = pathStateOrderAndStates.get(pathStateOrderAndStates.keySet().stream().mapToInt(v -> v).min().getAsInt());
            if (newStudentScenario.getLastCompletedStateId() != null) {
                currentState = getNextState(newStudentScenario.getLastCompletedStateId()).orElse(null);
                if (newStudentScenario.getEpenScenario() == null) {
                    boolean hasNextState = true;
                    while (hasNextState && (BE_EPEN_PATH_VALIDATOR.name().equals(currentState.getStateName())
                            || EPEN2_AUTOMATION.name().equals(currentState.getStateName()))) {
                        Optional<State> temp = getNextState(currentState.getStateId());
                        if (temp.isPresent()) {
                            currentState = temp.get();
                        } else {
                            hasNextState = false;
                        }
                    }
                }
            }
            jpaApi.withTransaction(() -> DivJobSetupUtil.initializeLazyFetchedData(studentScenarioService, newStudentScenario));
            startChildren(currentState, newStudentScenario);
        } else if (runningStudentScenarios.isEmpty()) {
            // To clear all studentsInBatteryStatsState
            if (currentStudentScenario.getBatteryStudentScenario() != null) {
                Set<String> batteryUuids = currentStudentScenario.getDivJobExec().getStudentScenarios().stream()
                        .map(ss -> ss.getBatteryStudentScenario().getBatteryUuid()).distinct()
                        .collect(Collectors.toSet());
                studentsInBatteryStatsState.removeAll(batteryUuids);
            }
            T3TestMapCache.removeT3TestMapFromCache(divJobExecId);
            stopDivJob(errorStudentScenarios.isEmpty(), true);
        }
    }

    /**
     * Updates the status of the given student scenario based on the last STATS result.
     * *
     *
     * @param finishMessage   The finish message
     * @param studentScenario The student scenario to update
     */
    private void updateStudentScenario(String finishMessage, StudentScenario studentScenario) {
        jpaApi.withTransaction(() -> {
                    if (FINISHED_MESSAGE.equals(finishMessage)) {
                        List<String> lastStatsResult = studentScenarioService.getLastStatsResultDetail(studentScenario.getStudentScenarioId());
                        if (lastStatsResult.isEmpty()) {
                            findAndUpdateStudentScenarioStatusByName(StatusNames.COMPLETED.getName(), studentScenario);
                        } else {
                            String success = new JsonParser().parse(lastStatsResult.get(0)).getAsJsonObject().get("success").getAsString();
                            if (success.startsWith("yes") && success.contains("OE Excluded")) {
                                findAndUpdateStudentScenarioStatusByName(StatusNames.COMPLETED_OE_EXCLUDED.getName(), studentScenario);
                            } else if (success.startsWith("yes") && success.contains("OE-OP Excluded")) {
                                findAndUpdateStudentScenarioStatusByName(StatusNames.COMPLETED_OE_OP_EXCLUDED.getName(), studentScenario);
                            } else if (success.startsWith("yes") && success.contains("OE-FT Excluded")) {
                                findAndUpdateStudentScenarioStatusByName(StatusNames.COMPLETED_OE_FT_EXCLUDED.getName(), studentScenario);
                            } else {
                                findAndUpdateStudentScenarioStatusByName(StatusNames.COMPLETED.getName(), studentScenario);
                            }
                        }
                    } else {
                        findAndUpdateStudentScenarioStatusByName(StatusNames.FAILED.getName(), studentScenario);
                    }
                }
        );
    }

    /**
     * Updates the given student scenario status using the provided status name
     * *
     *
     * @param statusName      The status name
     * @param studentScenario The student scenario to update
     */
    private void findAndUpdateStudentScenarioStatusByName(String statusName, StudentScenario studentScenario) {
        jpaApi.withTransaction(() ->
                studentScenarioStatusService
                        .findByStudentScenarioStatusName(statusName)
                        .ifPresent(status -> updateStudentScenarioStatus(studentScenario, status.getStudentScenarioStatusId()))
        );
    }

    /**
     * Updates DivJobExecId and stops the current job execution.
     *
     * @param divJobSuccess Whether the DIV job was successful (true/false)
     * @param jobCompleted  The flag for job complete (true/false)
     */
    private void stopDivJob(final boolean divJobSuccess, final boolean jobCompleted) {
        if (runningStudentScenarios.isEmpty()) {
            sendCompletionEmail(divJobSuccess);

            if (divJobSuccess) {
                jpaApi.withTransaction(() -> divJobExecStatusService
                        .findByDivJobExecStatusName(StatusNames.COMPLETED.getName()).ifPresent(
                                status -> updateJobStatus(divJobExecId, status.getDivJobExecStatusId(), jobCompleted)));
            } else {
                jpaApi.withTransaction(() -> divJobExecStatusService
                        .findByDivJobExecStatusName(StatusNames.FAILED.getName()).ifPresent(
                                status -> updateJobStatus(divJobExecId, status.getDivJobExecStatusId(), jobCompleted)));
            }
            if (divJobSuccess) {
                Logger.info("event=\"DIV Job completed.\", divJobExecId=\"{}\", jobId=\"{}\"", divJobExecId, jobId);
            } else {
                Logger.info("event=\"DIV Job failed.\", divJobExecId=\"{}\", jobId=\"{}\"", divJobExecId, jobId);
            }

            // sends the response back to sender and stops the current actor
            sender.tell(new JobProtocol.JobFinishMessage(jobId, divJobSuccess), getSelf());
            getContext().stop(getSelf());
        }
    }

    /**
     * Send an email to the user who created the DIV job with a summary of its execution.
     *
     * @param successful Whether the DIV job was successful.
     */
    private void sendCompletionEmail(final boolean successful) {
        try {
            if (!StringUtils.isEmpty(userEmailAddress)) {
                Email email = emailProvider.get();

                if (successful) {
                    email.setSubject("DIV Job " + divJobExecId + " in " + environmentName + " completed successfully");
                } else {
                    email.setSubject("DIV Job " + divJobExecId + " in " + environmentName + " completed with failures");
                }

                email.setFrom(fromAddress);
                email.addTo(userEmailAddress);
                String body;

                if (isBatteryEmail) {
                    body = "<p>" + "<a href=\"" + baseUrl + "/systemValidation/environments/" + environmentId
                            + "/divJobs/" + divJobExecId + "/batteries\">View Results</a>" + "</p>" + "<p>"
                            + "<b>Summary:</b><br/>" + "Total Student Scenarios: " + totalStudentScenarios + "<br/>"
                            + "Completed Student Scenarios: " + (totalStudentScenarios - errorStudentScenarios.size())
                            + "<br/>" + "Failed Student Scenarios: " + errorStudentScenarios.size() + "<br/>" + "</p>";
                } else {
                    body = "<p>" + "<a href=\"" + baseUrl + "/systemValidation/environments/" + environmentId
                            + "/divJobs/" + divJobExecId + "/studentScenarios\">View Results</a>" + "</p>" + "<p>"
                            + "<b>Summary:</b><br/>" + "Total Student Scenarios: " + totalStudentScenarios + "<br/>"
                            + "Completed Student Scenarios: "
                            + (totalPreviouslyCompletedStudentScenarios + completedStudentScenarios.size()) + "<br/>"
                            + "Failed Student Scenarios: " + errorStudentScenarios.size() + "<br/>" + "</p>";
                }

                email.setBodyHtml(body);
                mailerClient.send(email);
            } else {
                Logger.error(
                        "event=\"User email address was found to be null or empty while attempting to send DIV job completion email.\", environmentName=\"{}\", "
                                + "divJobExecId=\"{}\"",
                        environmentName, divJobExecId);
            }
        } catch (Exception e) {
            Logger.error(
                    "event=\"Exception occurred while attempting to send DIV job completion email.\", environmentName=\"{}\", "
                            + "divJobExecId=\"{}\"",
                    environmentName, divJobExecId, e);
        }
    }

    /**
     * Retrieves the next state from the path state with the help of current state.
     *
     * @param lastCompletedStateId The Last completed state id
     * @return The Optional of {@link State}
     */
    private Optional<State> getNextState(Integer lastCompletedStateId) {
        Optional<State> nextState = Optional.empty();
        Integer lastCompletedPathStateOrder = pathStateOrderAndStates.entrySet().stream()
                .filter(state -> state.getValue().getStateId().equals(lastCompletedStateId))
                .map(Map.Entry::getKey)
                .findFirst()
                .get();

        if (lastCompletedPathStateOrder != -1 && lastCompletedPathStateOrder < finalPathStateOrder) {
            nextState = Optional.of(pathStateOrderAndStates.get(lastCompletedPathStateOrder + 1));
        }
        return nextState;
    }

    /**
     * Returns the index of given state id form the map of pathStateOrder and States for a path.
     *
     * @param stateId The stateId
     * @return The index of given state id. If the given state id is not found in the map of pathStateOrder and States within a path then return
     * -1.
     */
    private int getPathStateOrder(Integer stateId) {

        for (Map.Entry<Integer, State> entry : pathStateOrderAndStates.entrySet()) {
            if (entry.getValue().getStateId().equals(stateId)) {
                return entry.getKey();
            }
        }
        return -1;
    }

    /**
     * Returns true if the state of the given student scenario is at or above the given state index within the path.
     *
     * @param studentScenario      The student scenario to check.
     * @param targetPathStateOrder The pathStateOrder to check.
     * @return A boolean indicating whether the student scenario state is up to or past the given index.
     */
    private boolean stateIsUpToGivenPoint(StudentScenario studentScenario, int targetPathStateOrder) {
        State studentScenarioState = studentScenario.getState();
        return studentScenarioState != null && getPathStateOrder(studentScenarioState.getStateId()) >= targetPathStateOrder;
    }

    /**
     * Updates the last completed state of a student in the database.
     *
     * @param studentScenario The instance of {@link StudentScenario}
     */
    private void updateLastCompletedState(final StudentScenario studentScenario) {
        State studentScenarioState = studentScenario.getState();
        if (studentScenarioState != null) {
            jpaApi.withTransaction(() -> {
                Long studentScenarioId = studentScenario.getStudentScenarioId();
                boolean changeLastCompleteState = true;
                if (DivStateName.BATTERY_STATS.name().equals(studentScenario.getState().getStateName())) {
                    Optional<StudentScenarioResult> resultOptional = getLatestStudentScenarioResultForStats(
                            studentScenarioId);
                    if (resultOptional.isPresent() && !resultOptional.get().isSuccess()) {
                        changeLastCompleteState = false;

                    }
                }
                Optional<StudentScenario> stdScenario = studentScenarioService
                        .findByStudentScenarioId(studentScenarioId);
                if (stdScenario.isPresent() && changeLastCompleteState) {
                    StudentScenario sc = stdScenario.get();
                    sc.setLastCompletedStateId(studentScenario.getState().getStateId());
                    studentScenario.setLastCompletedStateId(studentScenario.getState().getStateId());
                    studentScenario.setState(studentScenario.getState());
                }
            });
        }
    }

    /**
     * Retrieves the StudentScenarioResult for the STATS state for the given <code>studentScenarioId</code>. This
     * studentScenarioResult contains the highest version number.
     *
     * @param studentScenarioId The studentScenarioId
     * @return The Optional of {@link StudentScenarioResult} which has the highest version number.
     */
    private Optional<StudentScenarioResult> getLatestStudentScenarioResultForStats(final Long studentScenarioId) {
        List<StudentScenarioResult> studentScenarioResults = studentScenarioResultService
                .findByStudentScenarioIdAndStateName(studentScenarioId, DivStateName.STATS.name());
        return studentScenarioResults.stream().max(Comparator.comparing(StudentScenarioResult::getVersion));
    }

    /**
     * Updates the studentScenario status id and saves this status to database.
     *
     * @param studentScenario The instance of {@link StudentScenario}
     * @param statusId        The student scenario status id
     */
    private void updateStudentScenarioStatus(final StudentScenario studentScenario, final Integer statusId) {
        Integer studentScenarioStatusId = statusId;
        Long studentScenarioId = studentScenario.getStudentScenarioId();
        if (studentScenario.getState() != null
                && DivStateName.BATTERY_STATS.name().equals(studentScenario.getState().getStateName())) {
            Optional<StudentScenarioResult> resultOptional = getLatestStudentScenarioResultForStats(
                    studentScenarioId);
            if (resultOptional.isPresent() && !resultOptional.get().isSuccess()) {
                Optional<StudentScenarioStatus> status = studentScenarioStatusService
                        .findByStudentScenarioStatusName(StatusNames.FAILED.getName());
                if (status.isPresent()) {
                    studentScenarioStatusId = status.get().getStudentScenarioStatusId();
                }

            }
        }
        Optional<StudentScenario> stdScenario = studentScenarioService
                .findByStudentScenarioId(studentScenarioId);
        if (stdScenario.isPresent()) {
            StudentScenario sc = stdScenario.get();
            sc.setStudentScenarioStatusId(studentScenarioStatusId);
        }
    }

    /**
     * Updates JobStatus by updating the following attributes:
     * <ul>
     * <li>completed</li>
     * <li>completeTimeStamp</li>
     * <li>divJobExecStatusId</li>
     * </ul>
     *
     * @param divJobExecId The Long value of divJobExecId
     * @param jobStatusId  The Job status Id (1 -> Not Started, 2 -> In Progress, 3 -> Failed, 4 -> Completed)
     * @param completed    true if the job is completed successfully otherwise false.
     */
    private void updateJobStatus(final Long divJobExecId, final Integer jobStatusId, final boolean completed) {
        jpaApi.withTransaction(() -> {

            Optional<DivJobExec> divJobExec = divJobExecService.findByDivJobExecId(divJobExecId);
            divJobExec.ifPresent(divJobExec1 -> {
                DivJobExec divJob = divJobExec1;
                divJob.setCompleted(completed);
                divJob.setDivJobExecStatusId(jobStatusId);

                if (completed) {
                    divJob.setCompleteTimeStamp(Timestamp.from(Instant.now()));
                }

            });
        });
    }

    /**
     * @param pendingStudentScenarios The List of {@link StudentScenario}
     * @return The sublist of studentScenarios based on numberOfChildren
     */
    private List<StudentScenario> getChildList(final List<StudentScenario> pendingStudentScenarios) {
        final List<StudentScenario> subList = new ArrayList<>();
        if (pendingStudentScenarios.isEmpty() || pendingStudentScenarios.size() < numberOfChildren) {
            subList.addAll(pendingStudentScenarios);
            pendingStudentScenarios.clear();

        } else {
            subList.addAll(pendingStudentScenarios.subList(0, numberOfChildren));
            pendingStudentScenarios.subList(0, numberOfChildren).clear();
        }
        return subList;
    }

    private StatsInputMessage getStatsInputMessage(final StudentScenario studentScenario) throws
            BackendDatabaseException {
        DbConnection mongoDb = studentScenario.getDivJobExec().getEnvironment().getDataWarehouseMongoConnection();
        String host = mongoDb.getHost();
        int port = mongoDb.getPort();
        String adminDbName = mongoDb.getDbName();
        String dbUser = mongoDb.getDbUser();
        String dbPass = mongoDb.getDbPass();
        String scopeTreePath = studentScenario.getDivJobExec().getScopeTreePath();
        List<StudentScenario> batteryUnitStudentScenarios = new ArrayList<>();

        DivJobSetupUtil.ScopeTreePath stp = new DivJobSetupUtil.ScopeTreePath(scopeTreePath);
        String adminCode = stp.getAdminCode();
        String mongoDbName = stp.getAccountCode();

        MongoClient mongoClient = connectionPoolManager.getMongoConnectionPool(host, adminDbName, dbUser, dbPass, port);
        MongoInfo mongoInfo = new MongoInfo(mongoClient, mongoDbName);

        Environment environment = getEnvironment().get();

        DbConnection beDbConnection = environment.getBeMysqlConnection();
        BackendData backendData = backendDataFactory.create(beDbConnection);

        Set<DataSource> backendWfDataSources = backendData.getWorkflows().stream().map(BackendData.BackendWorkflowDetails::getDataSource)
                .collect(Collectors.toSet());

        Set<ItemStatus> eligibleItemStatuses = backendData.getItemStatusesEligibleForEpenScoring(scopeTreePath);

        return new StatsInputMessage(studentScenario, batteryUnitStudentScenarios, backendWfDataSources, mongoInfo,
                eligibleItemStatuses, adminCode, stp.getAccountCode());
    }

    private Optional<Environment> getEnvironment() {
        return jpaApi.withTransaction(() -> environmentService.findById(environmentId));

    }

    private StatsInputMessage getBatteryStatsInputMessage(final StudentScenario studentScenario) throws
            BackendDatabaseException {
        DbConnection mongoDb = studentScenario.getDivJobExec().getEnvironment().getDataWarehouseMongoConnection();
        String host = mongoDb.getHost();
        int port = mongoDb.getPort();
        String adminDbName = mongoDb.getDbName();
        String dbUser = mongoDb.getDbUser();
        String dbPass = mongoDb.getDbPass();
        String scopeTreePath = studentScenario.getDivJobExec().getScopeTreePath();
        List<StudentScenario> batteryUnitStudentScenarios = new ArrayList<>();

        DivJobSetupUtil.ScopeTreePath stp = new DivJobSetupUtil.ScopeTreePath(scopeTreePath);
        String adminCode = stp.getAdminCode();
        String mongoDbName = stp.getAccountCode();

        MongoClient mongoClient = connectionPoolManager.getMongoConnectionPool(host, adminDbName, dbUser, dbPass, port);
        MongoInfo mongoInfo = new MongoInfo(mongoClient, mongoDbName);

        Environment environment = getEnvironment().get();

        DbConnection beDbConnection = environment.getBeMysqlConnection();
        BackendData backendData = backendDataFactory.create(beDbConnection);

        Set<DataSource> backendWfDataSources = backendData.getWorkflows().stream().map(BackendData.BackendWorkflowDetails::getDataSource)
                .collect(Collectors.toSet());

        Set<ItemStatus> eligibleItemStatuses = backendData.getItemStatusesEligibleForEpenScoring(scopeTreePath);

        if (studentScenario.getBatteryStudentScenario() != null) {
            Long batteryStudentScenarioId = studentScenario.getBatteryStudentScenario().getBatteryStudentScenarioId();
            batteryUnitStudentScenarios = jpaApi.withTransaction(() -> {
                List<StudentScenario> units = studentScenarioService.findByDivJobExecIdAndBatteryStudentScenarioId(
                        divJobExecId, batteryStudentScenarioId, environmentId);
                units.forEach(ss -> {
                    ss.getTestnavScenario().getTestmapDetail().getTestmapName();
                    ss.getTestSession().getScopeCode();
                });
                return units;
            });
        }
        return new StatsInputMessage(studentScenario, batteryUnitStudentScenarios, backendWfDataSources, mongoInfo,
                eligibleItemStatuses, adminCode, stp.getAccountCode());
    }

    public enum DivStateName {
        TN8_API_DATA_LOADER,
        IRIS_PROCESSING_VALIDATOR,
        BE_PRIMARY_PATH_VALIDATOR,
        EPEN2_AUTOMATION,
        BE_EPEN_PATH_VALIDATOR,
        STATS,
        BATTERY_STATS
    }

    public interface Factory {
        Actor create();
    }

    /**
     * A message telling the DIV job to run.
     */
    public static final class RunMessage {
        private final String jobId;
        private final long divJobExecId;

        public RunMessage(String jobId, long divJobExecId) {
            this.jobId = jobId;
            this.divJobExecId = divJobExecId;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        String getJobId() {
            return jobId;
        }

        /**
         * Get the DIV job exec ID.
         *
         * @return The DIV job exec ID.
         */
        long getDivJobExecId() {
            return divJobExecId;
        }
    }

    /**
     * A Response message from the child actor.
     */
    public static final class FinishMessage {
        private final StudentScenario studentScenario;
        private final long divJobExecId;
        private final String finishMessage;
        private final String errorMessage;

        /**
         * @param studentScenario The studentScenario
         * @param divJobExecId    The divJobExecId
         * @param finishMessage   The finish message for the job
         * @param errorMessage    The Optional value of error message
         */
        public FinishMessage(final StudentScenario studentScenario, final long divJobExecId, final String finishMessage,
                             final String errorMessage) {
            this.studentScenario = studentScenario;
            this.divJobExecId = divJobExecId;
            this.finishMessage = finishMessage;
            this.errorMessage = errorMessage;
        }

        /**
         * @return the studentScenarioId
         */
        final StudentScenario getStudentScenario() {
            return studentScenario;
        }

        /**
         * @return the divJobExecId
         */
        public final long getDivJobExecId() {
            return divJobExecId;
        }

        /**
         * @return the finishMessage
         */
        public final String getFinishMessage() {
            return finishMessage;
        }

        /**
         * @return the errorMessage
         */
        final String getErrorMessage() {
            return errorMessage;
        }

    }
}