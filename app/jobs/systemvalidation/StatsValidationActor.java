package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.google.gson.*;
import com.mongodb.MongoException;
import com.pearson.itautomation.ValidationError;
import com.pearson.itautomation.comparator.XmlComparer;
import com.pearson.itautomation.exceptions.ParseTestmapException;
import com.pearson.itautomation.panext.datafetcher.DataFetcherException;
import com.pearson.itautomation.stats.service.MongoInfo;
import com.pearson.itautomation.stats.service.StatsInput;
import com.pearson.itautomation.stats.service.StatsServiceCall;
import com.pearson.itautomation.testmaps.models.Item;
import com.pearson.itautomation.testmaps.models.Testmap;
import com.pearson.itautomation.testmaps.models.TestmapForm;
import com.pearson.itautomation.testmaps.models.enums.ItemStatus;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapJsonResult;
import com.pearson.itautomation.tn8.previewer.api.GetTestMapsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import com.typesafe.config.Config;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import models.qtivalidation.PreviewerTenant;
import models.systemvalidation.StudentScenario;
import models.systemvalidation.TestmapDetail;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;
import play.Logger;
import util.EncryptorDecryptor;
import util.systemvalidation.StatsFactory;
import util.systemvalidation.T3TestMapCache;
import util.t3.T3TestMapException;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Nand Joshi
 */
public class StatsValidationActor extends UntypedAbstractActor {
    private static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss.S").disableHtmlEscaping().create();
    private final StatsFactory statsFactory;
    private final String token;
    private final String secret;

    @Inject
    public StatsValidationActor(StatsFactory statsFactory, Config config) {
        this.statsFactory = statsFactory;
        this.token = config.getString("application.previewerApiToken");
        this.secret = EncryptorDecryptor.getDecryptedConfigurationString(config, "application.previewerApiSecret");
    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof StatsInputMessage) {
            StatsInputMessage inputMsg = (StatsInputMessage) message;
            final StudentScenario studentScenario = inputMsg.getStudentScenario();
            Long studentScenarioId = studentScenario.getStudentScenarioId();
            Long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();
            String stateName = studentScenario.getState().getStateName();
            if (inputMsg.getFailedMessage() != null) {
                FinishMessage finishMessage = generateFinishMessage(inputMsg.getFailedMessage(), studentScenario, DataIntegrityValidationActor.FAILED_MESSAGE);
                getSender().tell(finishMessage, getSelf());
                return;
            }
            try {
                Logger.info("event=\"Started studentScenario execution.\"," +
                        " divJobExecId= \"{}\"," +
                        " studentScenarioId= \"{}\"," +
                        " state= \"{}\"", divJobExecId, studentScenarioId, stateName);


                Testmap t3TestMap = getT3TestMap(inputMsg, studentScenario);

                StatsInput statsInput = statsFactory.getStatsInput(inputMsg);
                statsInput.setT3TestMap(Collections.singletonList(t3TestMap));
                StatsServiceCall sCall = statsFactory.getStatsServiceCall(statsInput);
                Logger.info("event=\"STATS - execution complete.\", studentScenarioId=\"{}\", state=\"{}\"", studentScenarioId, stateName);

                Logger.info("event=\"STATS - runComparison started.\", studentScenarioId=\"{}\", state=\"{}\"", studentScenarioId, stateName);
                String validationResult = sCall.runComparison(false);
                Logger.info("event=\"STATS - runComparison completed.\", studentScenarioId=\"{}\", state=\"{}\"", studentScenarioId, stateName);

                JsonObject resultJson = new JsonParser().parse(validationResult).getAsJsonObject();
                final String success = resultJson.get("success").getAsString();

                Logger.info("event=\"STATS - success result={}.\", studentScenarioId=\"{}\", " + "state=\"{}\"", success, studentScenarioId, stateName);

                String resultMessage = DataIntegrityValidationActor.FINISHED_MESSAGE;
                if ("no".equalsIgnoreCase(success)) {
                    resultMessage = DataIntegrityValidationActor.FAILED_MESSAGE;

                    TestmapDetail scenarioTestMapDetail = studentScenario.getTestnavScenario().getTestmapDetail();

                    testmapVersionComparision(resultJson, scenarioTestMapDetail, t3TestMap);

                    Logger.info("event=\"STATS - t3AndPreviewerItemXmlComparison started.\", studentScenarioId=\"{}\", state=\"{}\"", studentScenarioId, stateName);
                    List<String> mismatchItemIdentifiers = getMismatchItemIdentifiersFromResultJson(resultJson);

                    Optional<TestmapForm> t3TestmapFormResult = t3TestMap.getForms()
                            .stream()
                            .filter(form -> form.getFormId().equals(studentScenario.getTestnavScenario().getFormCode()))
                            .findFirst();
                    Map<String, JsonArray> itemXmlMismatch = new HashMap<>();
                    if (t3TestmapFormResult.isPresent()) {
                        TestmapForm t3TestmapForm = t3TestmapFormResult.get();
                        Map<String, String> t3TestMapItems = t3TestmapForm.getItems().stream().collect(Collectors.toMap(Item::getUin, Item::getItemXML));
                        String formId = t3TestmapForm.getFormId();


                        // Reading item XML from previewer
                        PreviewerTenant previewerTenant = studentScenario.getTestnavScenario().getPreviewerTenant();
                        TestNav8API testNav8API = connectToPreviewer(previewerTenant);
                        int tenantId = Integer.parseInt(String.valueOf(previewerTenant.getTenantId()));

                        int previewerTestmapId = getPreviewerTestmapId(formId, testNav8API, tenantId);
                        if (previewerTestmapId > 0) {
                            GetTestMapJsonResult result = testNav8API.getTestMapJson(tenantId, previewerTestmapId);
                            Set<String> previewerItemIdentifiers = new HashSet<>(result.getItemIdentifierList());
                            JsonArray missingItems = findMissingItems(previewerItemIdentifiers, t3TestMapItems.keySet());

                            if (!missingItems.isJsonNull() && missingItems.size() > 0) {
                                itemXmlMismatch.put("MissingItems", missingItems);
                            }
                        } else {
                            Logger.error("event=\"Testmap is not found in previewer.\", tenantId=\"{}\", testmapId: \"{}\"", tenantId, previewerTestmapId);
                        }
                        Map<String, String> previewerItemIdentifierToXmlMap = getItemXmlFromPreviewer(mismatchItemIdentifiers, testNav8API, tenantId);

                        itemXmlMismatch.putAll(t3AndPreviewerItemXmlComparison(previewerItemIdentifierToXmlMap, t3TestMapItems));
                    }
                    Logger.info("event=\"STATS - t3AndPreviewerItemXmlComparison completed.\", studentScenarioId=\"{}\", state=\"{}\"", studentScenarioId, stateName);

                    if (!itemXmlMismatch.isEmpty()) {
                        resultJson.add("XML_Comparison", new JsonParser().parse(GSON.toJson(itemXmlMismatch)));
                    }
                }

                FinishMessage finishMessage = generateFinishMessage(resultJson.toString(), studentScenario, resultMessage);

                // Sends back the finish message to the sender actor
                getSender().tell(finishMessage, getSelf());
            } catch (MongoException ex) {
                final String validationResult = "Exception occurred during execution of Stats Validator. Could not connect to Data Warehouse Mongo DB.";
                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"Exception occurred during execution of Stats Validator.\", studentScenarioId=\"{}\"", studentScenarioId, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (DataFetcherException ex) {
                final String validationResult = "Exception occurred during execution of Stats Validator."
                        + " Could not find database '" + inputMsg.getMongoInfo().getDatabase()
                        + "' in the Mongo instance.";
                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"Exception occurred during execution of Stats Validator.\", studentScenarioId=\"{}\"", studentScenarioId, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (SQLException ex) {
                final String validationResult = "Exception occurred during execution of Stats Validator."
                        + " Could not connect to one or more MySQL WF DBs.";
                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"Exception occurred during execution of Stats Validator.\", studentScenarioId=\"{}\"", studentScenarioId, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (ParseTestmapException ex) {
                final String validationResult = "Exception occurred during execution of Stats Validator. "
                        + ex.getMessage();
                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"Exception occurred during execution of Stats Validator.\", studentScenarioId=\"{}\"", studentScenarioId, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (Throwable ex) {
                final String validationResult = "Exception occurred during execution of Stats Validator.";

                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"Exception occurred during execution of Stats Validator.\", studentScenarioId=\"{}\"", studentScenarioId, ex);

                getSender().tell(finishMessage, getSelf());
            } finally {
                Logger.info("event=\"STATS - reached finally statement. \", studentScenarioId=\"{}\", " + "state=\"{}\"", studentScenarioId, stateName);
                getContext().stop(getSelf());
            }
        }

    }

    private Testmap getT3TestMap(StatsInputMessage inputMsg, StudentScenario studentScenario) throws T3TestMapException {
        String testMapName = studentScenario.getTestnavScenario().getTestmapDetail().getTestmapName();
        String accountCode = inputMsg.getAccountCode();
        String adminCode = inputMsg.getAdminCode();
        String testCode = studentScenario.getTestnavScenario().getTestCode();
        String formCode = studentScenario.getTestnavScenario().getFormCode();
        long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();

        Testmap t3TestMap = T3TestMapCache.getT3TestMapFromCache(divJobExecId, testMapName);
        if (t3TestMap == null) {
            t3TestMap = T3TestMapCache.downloadTestMapFromT3(divJobExecId, accountCode, adminCode, testCode, formCode);
        }
        return t3TestMap;
    }

    private Integer getPreviewerTestmapId(String formId, TestNav8API testNav8API, int tenantId) {
        List<GetTestMapsResult.TestMap> previewerTestMaps = testNav8API.getTestMaps(tenantId).getTestMaps();
        return previewerTestMaps.stream()
                .filter(tm -> tm.getFormId().equals(formId)).findFirst()
                .map(GetTestMapsResult.TestMap::getId).orElse(0);
    }

    private Map<String, String> getItemXmlFromPreviewer(List<String> mismatchItemIdentifiers, TestNav8API testNav8API, int tenantId) {
        Map<String, String> previewerItemIdentifierToXmlMap = new HashMap<>();
        for (String itemIdentifier : mismatchItemIdentifiers) {
            String xml = testNav8API.getItemXMLByIdentifier(tenantId, itemIdentifier).getXML();
            previewerItemIdentifierToXmlMap.put(itemIdentifier, xml);
        }
        return previewerItemIdentifierToXmlMap;
    }

    private void testmapVersionComparision(JsonObject resultJson, TestmapDetail scenarioTestMapDetail, Testmap t3Testmap) {
        int initialTestMapVersion = scenarioTestMapDetail.getTestmapVersion();
        int currentTestMapVersion = Integer.parseInt(t3Testmap.getVersion());

        if (initialTestMapVersion != currentTestMapVersion) {
            JsonElement temp = resultJson.remove("studentTestComparisons");
            resultJson.add("testMapVersionComparison", getTestmapVersionResultJson(false, initialTestMapVersion, currentTestMapVersion));
            resultJson.add("studentTestComparisons", temp);
        } else {
            JsonElement temp = resultJson.remove("studentTestComparisons");
            resultJson.add("testMapVersionComparison", getTestmapVersionResultJson(true, initialTestMapVersion, currentTestMapVersion));
            resultJson.add("studentTestComparisons", temp);
        }
    }

    private TestNav8API connectToPreviewer(PreviewerTenant previewerTenant) {
        String previewerUrl = previewerTenant.getPreviewer().getApiUrl();
        String protocol = previewerTenant.getPreviewer().getApiProtocol();
        return TestNav8API.connectToPreviewer(protocol + previewerUrl, token.toCharArray(), secret.toCharArray());
    }

    private List<String> getMismatchItemIdentifiersFromResultJson(JsonObject resultJson) {
        if (resultJson == null || resultJson.isJsonNull()) {
            return new ArrayList<>();
        }

        List<String> mismatchItemIdentifiers = new ArrayList<>();
        JsonElement studentTestComparisons = resultJson.get("studentTestComparisons");
        if (studentTestComparisons != null && studentTestComparisons.isJsonArray()) {
            JsonObject comparisionResult = studentTestComparisons.getAsJsonArray().get(0).getAsJsonObject();
            JsonElement fieldComparisionResult = comparisionResult.get("mismatchedFields");
            if (fieldComparisionResult != null && !fieldComparisionResult.isJsonNull() && fieldComparisionResult.isJsonArray()) {
                for (JsonElement mismatchElement : fieldComparisionResult.getAsJsonArray()) {
                    JsonObject mismatchField = mismatchElement.getAsJsonObject();
                    String fieldName = mismatchField.get("fieldName").getAsString();
                    if (fieldName.startsWith("Item")) {
                        mismatchItemIdentifiers.add(fieldName.substring(fieldName.indexOf("Item ") + "Item ".length()));
                    }
                }
            }
        }

        return mismatchItemIdentifiers;
    }

    private Map<String, JsonArray> t3AndPreviewerItemXmlComparison(Map<String, String> previewerItemIdentifierToXmlMap, Map<String, String> t3TestMapItems)
            throws ParserConfigurationException, SAXException, IOException {
        final Map<String, JsonArray> itemXmlMismatch = new LinkedHashMap<>();

        for (Map.Entry<String, String> entry : previewerItemIdentifierToXmlMap.entrySet()) {
            String itemIdentifier = entry.getKey();
            String previewerItemXml = StringUtils.normalizeSpace(entry.getValue());
            String t3ItemXml = StringUtils.normalizeSpace(t3TestMapItems.get(itemIdentifier));
            // comparing item XML
            List<String> xmlMisMatchErrors = XmlComparer.similar(previewerItemXml, t3ItemXml);
            if (!xmlMisMatchErrors.isEmpty()) {
                itemXmlMismatch.put(itemIdentifier, new JsonParser().parse(GSON.toJson(xmlMisMatchErrors)).getAsJsonArray());
            }
        }
        return itemXmlMismatch;
    }

    /**
     * @param versionMatched        The boolean value for version matched or not matched
     * @param initialTestMapVersion The testmap version captured during scenario creation
     * @param currentTestMapVersion The testmap version captured during the execution of STATS
     * @return The JsonObject containing the testMapVersionComparison result
     */
    private JsonObject getTestmapVersionResultJson(boolean versionMatched, int initialTestMapVersion,
                                                   int currentTestMapVersion) {
        JsonObject testMapVersionComparison = new JsonObject();
        testMapVersionComparison.addProperty("matched", versionMatched);
        if (!versionMatched) {
            testMapVersionComparison.addProperty("message",
                    "The current test map version does not match the captured during DIV Job setup.");
        }
        testMapVersionComparison.addProperty("initialTestMapVersion", initialTestMapVersion);
        testMapVersionComparison.addProperty("currentTestMapVersion", currentTestMapVersion);
        return testMapVersionComparison;

    }

    /**
     * Creates an instance of {@link FinishMessage} to send back to the sender actor.
     *
     * @param validationResult The Optional of JsonString of {@link ValidationError}
     * @param studentScenario  The {@link StudentScenario}
     * @param finishMessage    The finished message
     * @return The {@link FinishMessage}
     */
    private FinishMessage generateFinishMessage(final String validationResult, final StudentScenario studentScenario,
                                                final String finishMessage) {
        return new DataIntegrityValidationActor.FinishMessage(studentScenario,
                studentScenario.getDivJobExec().getDivJobExecId(), finishMessage, validationResult);
    }

    private JsonArray findMissingItems(final Set<String> previewerItemXmls, final Set<String> t3ItemXmls) {
        JsonArray result = new JsonArray();

        Set<String> differenceItemIds = t3ItemXmls.stream().filter(item -> !previewerItemXmls.contains(item))
                .collect(Collectors.toSet());
        differenceItemIds.forEach(differenceItemId -> result.add(getMissingItem(differenceItemId, false, true)));

        differenceItemIds = previewerItemXmls.stream().filter(item -> !t3ItemXmls.contains(item))
                .collect(Collectors.toSet());
        differenceItemIds.forEach(differenceItemId -> result.add(getMissingItem(differenceItemId, true, false)));
        return result;

    }

    private JsonObject getMissingItem(String differenceItemId, boolean existsInPreviewer, boolean existsInT3) {
        JsonObject obj = new JsonObject();
        obj.addProperty("uin", differenceItemId);
        obj.add("existsInPreviewer", new JsonPrimitive(existsInPreviewer));
        obj.add("existsInT3", new JsonPrimitive(existsInT3));
        return obj;
    }

    public interface Factory {
        Actor create();
    }

    /**
     * This is a wrapper class that is used to send information to the StatsValidationActor.<br>
     * Contains the following information: {@link StudentScenario}, Backend workflow DataSources, {@link MongoInfo}, account code, administration code, etc
     *
     * @author Nand Joshi
     */
    public static class StatsInputMessage {
        private final StudentScenario studentScenario;
        private final List<StudentScenario> batteryUnitStudentScenarios;
        private final Set<DataSource> backendWfDataSources;
        private final MongoInfo mongoInfo;
        private final Set<ItemStatus> eligibleItemStatuses;
        private String failedMessage;
        private final String adminCode;
        private final String accountCode;

        /**
         * @param studentScenario             The instance of {@link StudentScenario}
         * @param batteryUnitStudentScenarios A List of {@link StudentScenario} - for battery tests. Null if non-battery.
         * @param backendWfDataSources        The set of DataSources for the backend workflow databases
         * @param mongoInfo                   The instance of {@link MongoInfo}
         * @param eligibleItemStatuses        The set of {@link ItemStatus}
         * @param adminCode                   The administration code
         * @param accountCode                 The account
         */
        public StatsInputMessage(StudentScenario studentScenario, List<StudentScenario> batteryUnitStudentScenarios,
                                 Set<DataSource> backendWfDataSources, MongoInfo mongoInfo,
                                 Set<ItemStatus> eligibleItemStatuses, String adminCode, String accountCode) {
            this.studentScenario = studentScenario;
            this.batteryUnitStudentScenarios = batteryUnitStudentScenarios;
            this.backendWfDataSources = backendWfDataSources;
            this.mongoInfo = mongoInfo;
            this.eligibleItemStatuses = eligibleItemStatuses;
            this.adminCode = adminCode;
            this.accountCode = accountCode;
        }

        /**
         * Get the student scenario.
         *
         * @return the studentScenario
         */
        public StudentScenario getStudentScenario() {
            return studentScenario;
        }

        public List<StudentScenario> getBatteryUnitStudentScenarios() {
            return batteryUnitStudentScenarios;
        }

        /**
         * Get the backend workflow DataSources.
         *
         * @return the backend workflow DataSources
         */
        public Set<DataSource> getBackendWfDataSources() {
            return backendWfDataSources;
        }

        /**
         * Get the Mongo info.
         *
         * @return the mongoInfo
         */
        public MongoInfo getMongoInfo() {
            return mongoInfo;
        }

        /**
         * Get the eligible item statuses.
         *
         * @return the eligible item statuses
         */
        public Set<ItemStatus> getEligibleItemStatuses() {
            return eligibleItemStatuses;
        }


        public String getFailedMessage() {
            return failedMessage;
        }

        public void setFailedMessage(String failedMessage) {
            this.failedMessage = failedMessage;
        }

        public String getAdminCode() {
            return adminCode;
        }

        public String getAccountCode() {
            return accountCode;
        }
    }
}
