package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.google.gson.Gson;
import com.pearson.itautomation.ValidationError;
import com.pearson.itautomation.irisvalidation.IrisValidator;
import com.pearson.itautomation.irisvalidation.database.IrisDbConfigProperty;
import com.pearson.itautomation.irisvalidation.exception.IrisDatabaseException;
import com.pearson.itautomation.irisvalidation.model.json.IrisValidationOutput;
import com.typesafe.config.Config;
import com.zaxxer.hikari.pool.HikariPool;
import jobs.systemvalidation.DataIntegrityValidationActor.FinishMessage;
import models.systemvalidation.DbConnection;
import models.systemvalidation.StudentScenario;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import util.ConnectionPoolManager;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.Random;

/**
 * @author Nand Joshi
 */
public class IrisValidationActor extends UntypedAbstractActor {
    private final ConnectionPoolManager connectionPoolManager;
    private int numberOfReconnectTries;
    private final int maxNumberOfRetries;

    @Inject
    IrisValidationActor(ConnectionPoolManager connectionPoolManager, Config configuration) {
        this.connectionPoolManager = connectionPoolManager;
        int numberOfConcurrentStudents = configuration.getInt("application.jobs.dataIntegrityValidation.maxConcurrentStudents");
        int numberOfConcurrentJobs = configuration.getInt("application.jobs.dataIntegrityValidation.maxConcurrent");
        maxNumberOfRetries = numberOfConcurrentJobs * numberOfConcurrentStudents;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof StudentScenario) {
            StudentScenario studentScenario = (StudentScenario) message;
            Long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();
            Long studentScenarioId = studentScenario.getStudentScenarioId();
            String stateName = studentScenario.getState().getStateName();
            try {
                Logger.info("event=\"Started studentScenario execution.\", divJobExecId= \"{}\", studentScenarioId= \"{}\", state= \"{}\"",
                        divJobExecId, studentScenarioId, stateName);

                IrisDbConfigProperty irisDbConfigProperty = readIrisDbConfigProperty(studentScenario);
                IrisValidator validator = new IrisValidator(irisDbConfigProperty);

                boolean requiresEpen = true;
                if (studentScenario.getEpenScenario() == null
                        || studentScenario.getEpenScenario().getScenario() == null) {
                    requiresEpen = false;
                }
                String validationResult = "";
                boolean needToReExecute = false;
                do {
                    try {
                        if (needToReExecute) {
                            Logger.debug("event=\"Re-executing studentScenario.\", divJobExecId= \"{}\", studentScenarioId= \"{}\", state= \"{}\"",
                                    divJobExecId, studentScenarioId, stateName);
                        }
                        validationResult = validator.execute(studentScenario.getDivJobExec().getScopeTreePath(),
                                requiresEpen, studentScenario.getUuid());
                        needToReExecute = false;
                    } catch (IrisDatabaseException ex) {
                        if ("SQLTransientConnectionException".equals(ex.getExceptionName()) && numberOfReconnectTries <= maxNumberOfRetries) {
                            numberOfReconnectTries++;
                            int waitTime = new Random().nextInt(1001) + 1000;
                            Logger.debug("event=\"Connection is not available. Waiting for {} milliseconds.\", divJobExecId= \"{}\", studentScenarioId= \"{}\", state= \"{}\"",
                                    waitTime, divJobExecId, studentScenarioId, stateName);
                            needToReExecute = true;
                            Thread.sleep(waitTime);
                        } else {
                            Logger.error("event= \"Connection is not available. Execution stopped.\", divJobExecId= \"{}\", studentScenarioId= \"{}\", state= \"{}\"",
                                    divJobExecId, studentScenarioId, stateName);
                            FinishMessage finishMessage = generateFinishMessage(ex.getMessage(), studentScenario,
                                    DataIntegrityValidationActor.FAILED_MESSAGE);
                            getSender().tell(finishMessage, getSelf());
                            return;
                        }
                    }
                } while (needToReExecute);

                String resultMessage = DataIntegrityValidationActor.FINISHED_MESSAGE;
                IrisValidationOutput output = new Gson().fromJson(validationResult, IrisValidationOutput.class);

                // sets the Finished message as failure if success attribute of response JSON contains value "NO".
                if ("No".equalsIgnoreCase(output.getSuccess())) {
                    resultMessage = DataIntegrityValidationActor.FAILED_MESSAGE;
                }

                FinishMessage finishMessage = generateFinishMessage(validationResult, studentScenario, resultMessage);

                // Sends back the finish message to the sender actor
                getSender().tell(finishMessage, getSelf());

            } catch (HikariPool.PoolInitializationException ex) {
                String errorMessage = "An error occurred while establishing a connection to the database. Please verify the IRIS database connection details.";
                Logger.error("event= \"{}\", divJobExecId= \"{}\", StudentScenarioId= \"{}\", state= \"{}\"",
                        errorMessage, divJobExecId, studentScenarioId, stateName);
                FinishMessage finishMessage = generateFinishMessage(DataIntegrityValidationActor.getErrorJson(errorMessage), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);
                getSender().tell(finishMessage, getSelf());
            } catch (Throwable t) {
                String validationResult = "An exception occurred while calling IRIS Validator for StudentScenarioId " + studentScenarioId;
                if (!StringUtils.isEmpty(t.getMessage())) {
                    validationResult += " ErrorMessage: " + t.getMessage();
                }

                FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error("event=\"An exception occurred while calling IRIS Validator.\", divJobExecId=\"{}\", studentScenarioId= \"{}\", state= \"{}\"",
                        divJobExecId, studentScenarioId, stateName, t);

                getSender().tell(finishMessage, getSelf());
            } finally {
                getContext().stop(getSelf());
            }

        }

    }

    /**
     * @param studentScenario The instance of {@link StudentScenario}
     * @return The instance of {@link IrisDbConfigProperty}
     */
    private IrisDbConfigProperty readIrisDbConfigProperty(StudentScenario studentScenario) {
        DbConnection dbConnection = studentScenario.getDivJobExec().getEnvironment().getIrisMysqlConnection();
        DataSource dataSource = connectionPoolManager.getMysqlConnectionPool(dbConnection.getHost(),
                dbConnection.getDbName(), dbConnection.getDbUser(), dbConnection.getDbPass(), dbConnection.getPort());
        return new IrisDbConfigProperty(dbConnection.getHost(), dbConnection.getDbName(), dataSource);
    }

    /**
     * Creates an instance of {@link FinishMessage} to send back to the sender actor.
     *
     * @param validationResult The Optional of JsonString of {@link ValidationError}
     * @param studentScenario  The {@link StudentScenario}
     * @param finishMessage    The finished message
     * @return The {@link FinishMessage}
     */
    private FinishMessage generateFinishMessage(final String validationResult, final StudentScenario studentScenario,
                                                final String finishMessage) {
        return new DataIntegrityValidationActor.FinishMessage(studentScenario,
                studentScenario.getDivJobExec().getDivJobExecId(), finishMessage, validationResult);
    }

    public interface Factory {
        Actor create();
    }
}
