package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.ActorRef;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.typesafe.config.Config;
import jobs.JobStarter;

import java.time.Clock;

public class DivJobSetupJobStarter extends JobStarter {
    private final int maxConcurrentInstances;
    private final DivJobSetupActor.Factory divJobSetupFactory;
    private final DivJobSetupActor.DivJobSetupType divJobSetupType;
    private final long startTime;
    private final long userId;

    private final int environmentId;
    private final String testType;
    private final String scopeTreePath;
    private final String sessionIds;
    private final String testCode;
    private final JsonNode json;

    public interface Factory {
        /**
         * Create an instance of DivJobSetupStarter for getting forms.
         *
         * @param environmentId
         *            The ID of the environment to get the forms for.
         * @param testType
         *            The test type.
         * @param scopeTreePath
         *            The scope tree path to get the forms for.
         * @param sessionIds
         *            A comma separated list of session IDs to get forms for.
         * @param testCode
         *            The test code to get forms for.
         * @param userId
         *            The user ID.
         * @return An instance of DivJobSetupStarter.
         */
        DivJobSetupJobStarter create(@Assisted("environmentId") int environmentId,
                @Assisted("testType") String testType, @Assisted("scopeTreePath") String scopeTreePath,
                @Assisted("sessionIds") String sessionIds, @Assisted("testCode") String testCode,
                @Assisted("userId") long userId);

        /**
         * Create an instance of DivJobSetupStarter for getting forms.
         *
         * @param environmentId
         *            The ID of the environment to get the forms for.
         * @param testType
         *            The test type.
         * @param scopeTreePath
         *            The scope tree path to get the forms for.
         * @param json
         *            JSON containing selected scenario information.
         * @param userId
         *            The user ID.
         * @return An instance of DivJobSetupStarter.
         */
        DivJobSetupJobStarter create(@Assisted("environmentId") int environmentId,
                @Assisted("testType") String testType, @Assisted("scopeTreePath") String scopeTreePath,
                @Assisted("json") JsonNode json, @Assisted("userId") long userId);
    }

    @AssistedInject
    DivJobSetupJobStarter(Clock clock, DivJobSetupActor.Factory divJobSetupFactory, Config configuration,
            @Assisted("environmentId") int environmentId, @Assisted("testType") String testType,
            @Assisted("scopeTreePath") String scopeTreePath, @Assisted("sessionIds") String sessionIds,
            @Assisted("testCode") String testCode, @Assisted("userId") long userId) {
        super(clock);

        this.divJobSetupFactory = divJobSetupFactory;
        this.maxConcurrentInstances = configuration.getInt("application.jobs.divJobSetup.maxConcurrent");
        this.startTime = System.currentTimeMillis();
        this.divJobSetupType = DivJobSetupActor.DivJobSetupType.GET_FORMS;

        this.environmentId = environmentId;
        this.testType = testType;
        this.scopeTreePath = scopeTreePath;
        this.sessionIds = sessionIds;
        this.testCode = testCode;
        this.userId = userId;

        this.json = null;
    }

    @AssistedInject
    DivJobSetupJobStarter(Clock clock, DivJobSetupActor.Factory divJobSetupFactory, Config configuration,
            @Assisted("environmentId") int environmentId, @Assisted("testType") String testType,
            @Assisted("scopeTreePath") String scopeTreePath, @Assisted("json") JsonNode json,
            @Assisted("userId") long userId) {
        super(clock);

        this.divJobSetupFactory = divJobSetupFactory;
        this.maxConcurrentInstances = configuration.getInt("application.jobs.divJobSetup.maxConcurrent");
        this.startTime = System.currentTimeMillis();
        this.divJobSetupType = DivJobSetupActor.DivJobSetupType.GET_STUDENT_SCENARIOS;

        this.environmentId = environmentId;
        this.testType = testType;
        this.scopeTreePath = scopeTreePath;
        this.json = json;
        this.userId = userId;

        this.sessionIds = null;
        this.testCode = null;
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    @Override
    public String getJobType() {
        return "divJobSetup";
    }

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    @Override
    public int getMaxConcurrentInstances() {
        return maxConcurrentInstances;
    }

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    @Override
    public String getJobId() {
        StringBuilder jobId = new StringBuilder();

        if (getParentJobId().isPresent()) {
            jobId.append(getParentJobId().get()).append("_");
        }

        if (DivJobSetupActor.DivJobSetupType.GET_FORMS == divJobSetupType) {
            jobId.append("divJobSetup_getForms_").append(userId).append("_").append(startTime);
        } else {
            jobId.append("divJobSetup_getStudentScenarios_").append(userId).append("_").append(startTime);
        }

        return jobId.toString();
    }

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    @Override
    public Actor createJobActor() {
        return divJobSetupFactory.create();
    }

    /**
     * Start the job.
     *
     * @param jobActor
     *            A reference to the job actor.
     * @param sender
     *            A reference to the message sender.
     */
    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        if (DivJobSetupActor.DivJobSetupType.GET_FORMS == divJobSetupType) {
            jobActor.tell(new DivJobSetupActor.RunMessage(environmentId, testType, scopeTreePath, sessionIds, testCode,
                    getJobId()), sender);
        } else {
            jobActor.tell(new DivJobSetupActor.RunMessage(environmentId, testType, scopeTreePath, json, getJobId()),
                    sender);
        }
    }
}
