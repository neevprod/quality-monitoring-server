package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Sets;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.pearson.itautomation.models.AccessTokenResp;
import com.pearson.itautomation.panext.studentsession.SessionInfo;
import com.pearson.itautomation.panext.studentsession.StudentTestSession;
import com.pearson.itautomation.panext.studentsession.exception.FrontendDatabaseException;
import com.pearson.itautomation.panext.studentsession.util.StudentTestSessionExtractor;
import com.pearson.itautomation.qtiitem.QtiItem;
import com.pearson.itautomation.qtiscv.ItemResponse;
import com.pearson.itautomation.qtiscv.ItemResponseBean;
import com.pearson.itautomation.qtiscv.ItemResponsePool;
import com.pearson.itautomation.qtiscv.ItemTestCaseCoverage;
import com.pearson.itautomation.testmaps.models.Item;
import com.pearson.itautomation.testmaps.models.Testmap;
import com.pearson.itautomation.testmaps.models.TestmapForm;
import com.pearson.itautomation.tn8.previewer.api.GetXMLByIdentifierResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import com.typesafe.config.Config;
import jobs.JobProtocol;
import models.qtivalidation.KtTestCase;
import models.systemvalidation.*;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import play.Logger;
import play.db.jpa.JPAApi;
import play.libs.Json;
import services.qtivalidation.KtTestCaseService;
import services.systemvalidation.*;
import util.EncryptorDecryptor;
import util.systemvalidation.DivJobSetupUtil;
import util.systemvalidation.DivJobSetupUtil.BatteryTest;
import util.systemvalidation.DivJobSetupUtil.TestDetail;
import util.systemvalidation.ScenarioCreatorUtil;

import javax.inject.Inject;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public class DivJobSetupActor extends UntypedAbstractActor {

    private static final String USER_AGENT_STRING = "IRIS-Validator";
    private static final int MAX_CONN_PER_ROUTE = 15;
    private static final int MAX_CONN_TOTAL = 15;
    private static final Type LIST_TYPE = new TypeToken<List<TestDetail>>() {
    }.getType();
    private final Gson GSON;
    private final DivJobSetupUtil divJobSetupUtil;
    private final JPAApi jpaApi;
    private final EnvironmentService environmentService;
    private final ScenarioClassService scenarioClassService;
    private final ScenarioService scenarioService;
    private final ScenarioTypeService scenarioTypeService;
    private final TestmapDetailService testmapDetailService;
    private final StudentReservationService studentReservationService;
    private final KtTestCaseService ktTestCaseService;
    private final long studentReservationTimeOut;
    private final String token;
    private final String secret;
    private String jobId;

    @Inject
    DivJobSetupActor(DivJobSetupUtil divJobSetupUtil, JPAApi jpaApi, EnvironmentService environmentService,
                     ScenarioClassService scenarioClassService, ScenarioService scenarioService,
                     ScenarioTypeService scenarioTypeService, TestmapDetailService testmapDetailService,
                     StudentReservationService studentReservationService, KtTestCaseService ktTestCaseService, Config config) {
        this.divJobSetupUtil = divJobSetupUtil;
        this.jpaApi = jpaApi;
        this.environmentService = environmentService;
        this.scenarioService = scenarioService;
        this.scenarioClassService = scenarioClassService;
        this.scenarioTypeService = scenarioTypeService;
        this.testmapDetailService = testmapDetailService;
        this.studentReservationService = studentReservationService;
        this.ktTestCaseService = ktTestCaseService;
        this.studentReservationTimeOut = config.getDuration("application.studentReservationTimeOut").getSeconds();
        this.token = config.getString("application.previewerApiToken");
        this.secret = EncryptorDecryptor.getDecryptedConfigurationString(config, "application.previewerApiSecret");
        GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss.S").registerTypeAdapter(Timestamp.class, new DivJobSetupUtil.TimestampDeserializer()).create();
    }

    /**
     * Process an incoming message.
     *
     * @param message The message sent.
     */
    @Override
    public void onReceive(Object message) {
        RunMessage runMessage = (RunMessage) message;
        jobId = runMessage.getJobId();
        JobProtocol.JobFinishMessage finishMessage;

        try {
            if (DivJobSetupType.GET_FORMS == runMessage.divJobSetupType) {
                int environmentId = runMessage.getEnvironmentId();
                String testType = runMessage.getTestType();
                String scopeTreePath = runMessage.getScopeTreePath();
                String testCode = runMessage.getTestCode();
                String sessionIds = runMessage.getSessionIds();

                Logger.info(
                        "event=\"Running DIV job setup job for getting forms.\", environmentId=\"{}\", "
                                + "scopeTreePath=\"{}\", testCode=\"{}\", sessionIds=\"{}\", jobId=\"{}\"",
                        environmentId, scopeTreePath, testCode, sessionIds, jobId);
                if ("Battery".equals(testType)) {
                    finishMessage = getBatteryTestForms(environmentId, scopeTreePath, sessionIds, testCode);
                } else {
                    finishMessage = getStandardTestForms(environmentId, scopeTreePath, sessionIds, testCode);
                }
            } else {
                int environmentId = runMessage.getEnvironmentId();
                String scopeTreePath = runMessage.getScopeTreePath();
                JsonNode json = runMessage.getJson();

                Logger.info(
                        "event=\"Running DIV job setup job for getting student scenarios.\", "
                                + "environmentId=\"{}\", scopeTreePath=\"{}\", jobId=\"{}\"",
                        environmentId, scopeTreePath, jobId);

                if ("Battery".equals(runMessage.getTestType())) {
                    finishMessage = getBatteryStudentScenarios(environmentId, json);
                } else {
                    finishMessage = getStandardStudentScenarios(environmentId, json);
                }

            }

            getSender().tell(finishMessage, getSelf());
            Logger.info("event=\"Finished running DIV job setup job.\", jobId=\"{}\"", jobId);
        } catch (JsonSyntaxException ex) {
            Logger.error("event=\"Exception occurred while running DIV job setup job.\", jobId=\"{}\"", jobId, ex);
            JobProtocol.JobFinishMessage failMessage = new JobProtocol.JobFinishMessage(jobId, false,
                    JobProtocol.getJobResultJson(ex.getMessage()));
            getSender().tell(failMessage, getSelf());
        } catch (Throwable t) {
            Logger.error("event=\"Exception occurred while running DIV job setup job.\", jobId=\"{}\"", jobId, t);
            JobProtocol.JobFinishMessage failMessage = new JobProtocol.JobFinishMessage(jobId, false,
                    JobProtocol.getJobResultJson("An unexpected error occurred while running the job."));
            getSender().tell(failMessage, getSelf());
        } finally {
            getContext().stop(getSelf());
        }
    }

    private JobProtocol.JobFinishMessage getStandardTestForms(final int environmentId, final String scopeTreePath, final String sessionIds, final String testCodeString) {
        return jpaApi.withTransaction(() -> {
            List<String> selectedTestCodes = Arrays.asList(testCodeString.split(","));

            List<SessionInfo> selectedTestSession = new ArrayList<>();
            for (String sessionId : sessionIds.split(",")) {
                SessionInfo sessionInfo = new SessionInfo();
                sessionInfo.setSessionId(Integer.parseInt(sessionId));
                selectedTestSession.add(sessionInfo);
            }

            final String decodedScopeTreePath = StringUtils.replace(scopeTreePath, "|", "/");
            // Getting adminCode and accountCode from scopeTreePath
            DivJobSetupUtil.ScopeTreePath stp;
            try {
                stp = new DivJobSetupUtil.ScopeTreePath(decodedScopeTreePath);
            } catch (IllegalArgumentException ex) {
                Logger.error("event= \"Invalid ScopeTreePath.\", scopeTreePath= \"{}\"", decodedScopeTreePath);
                return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson("Invalid ScopeTreePath found: " + decodedScopeTreePath));
            }

            final Optional<Environment> environment = environmentService.findById(environmentId);

            if (!environment.isPresent()) {
                Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
                return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson("Can not find Environment for the environmentId " + environmentId));
            }

            List<ScenarioClass> scenarioClasses = scenarioClassService.findAllEnabled();
            List<ScenarioClass> tn8ScenarioClasses = scenarioClasses.stream().filter(ScenarioClass::isEnabledForTn8).collect(Collectors.toList());
            List<ScenarioClass> epenScenarioClasses = scenarioClasses.stream().filter(ScenarioClass::isEnabledForEpen).collect(Collectors.toList());

            StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());

            List<String> studentNotFoundForms = new ArrayList<>();
            List<TestDetail> testDetails = new ArrayList<>();
            try {
                Map<String, List<String>> testCodeToFormCode = stse.getFormCodesBySessionIds(Arrays.asList(sessionIds.split(",")));
                for (String testCode : testCodeToFormCode.keySet()) {
                    if (selectedTestCodes.contains(testCode)) {
                        List<String> formCodes = testCodeToFormCode.get(testCode);
                        for (String formCode : formCodes) {
                            List<StudentTestSession> studentTestSessions = stse.getStudTestSessionDetails(decodedScopeTreePath, testCode, formCode)
                                    .stream().filter(sts -> sessionIds.contains(sts.getSessionId())).collect(Collectors.toList());
                            if (!studentTestSessions.isEmpty()) {
                                StudentTestSession studentTestSession = studentTestSessions.get(0);
                                TestDetail testDetail = divJobSetupUtil.createTestDetailObject(testCode, formCode);
                                testDetail.setFormName(studentTestSession.getFormName());
                                testDetail.setTestNavCustomerCode(studentTestSession.getTn8CustomerCode());
                                testDetail.setTestnavWebServiceUrl(studentTestSession.getTestnavWebServiceURL());
                                testDetail.setTestnavClientIdentifier(studentTestSession.getTestnavClientIdentifier());
                                testDetail.setTestnavClientSecret(studentTestSession.getTestnavClientSecret());

                                testDetail.setTestSessions(selectedTestSession);
                                List<String> reservedStudents = studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut);
                                long totalNumberOfStudents = studentTestSessions.stream()
                                        .filter(sts -> !reservedStudents.contains(sts.getStudentTestUuid()))
                                        .filter(sts -> sts.getFirstName() != null && sts.getLastName() != null)
                                        .count();
                                testDetail.setRemainingStudents(Integer.parseInt(String.valueOf(totalNumberOfStudents)));
                                testDetails.add(testDetail);
                            } else {
                                Logger.warn("event= \"Can not find Students\", scopeTreePath= \"{}\", testCode= \"{}\", formCode= \"{}\"", decodedScopeTreePath, testCode, formCode);
                                studentNotFoundForms.add(formCode);
                            }
                        }
                    }
                }
            } catch (FrontendDatabaseException e) {
                return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson("Unable to retrieve student test session details from PANext Frontend Database."));
            }
            List<String> errorForms = new ArrayList<>();
            Iterator<TestDetail> itr = testDetails.listIterator();
            while (itr.hasNext()) {
                boolean removedFromIterator = true;
                TestDetail testDetail = itr.next();
                boolean disableEpenScenario = true;
                String form;
                try {
                    form = getTestFormsFromTestNav(testDetail);
                } catch (JsonSyntaxException ex) {
                    throw new JsonSyntaxException("Invalid response received while retrieving forms from TestNav. TestNav may be down: ;;" + testDetail.getTestnavWebServiceUrl());
                }
                if (form == null || !"yes".equalsIgnoreCase(new JsonParser().parse(form).getAsJsonObject().get("success").getAsString())) {
                    Logger.error("TestNav Publish API call failed for Customer Code ({}), Test Number ({}) and apiurlBase({})",
                            testDetail.getTestNavCustomerCode(), testDetail.getTestCode(), testDetail.getTestnavWebServiceUrl());
                    errorForms.add(testDetail.getFormCode());
                    itr.remove();
                    continue;
                }

                final JsonObject formObject = new JsonParser().parse(form).getAsJsonObject();
                final JsonArray formArray = formObject.get("msg").getAsJsonArray();
                for (JsonElement formElement : formArray) {
                    String currentFormNumber = formElement.getAsJsonObject().get("form_number").getAsString();
                    if (testDetail.getFormCode().equals(currentFormNumber)) {
                        String includesAdaptiveIndicator = formElement.getAsJsonObject().get("includes_adaptive_ind").getAsString();
                        String publisherInfo = formElement.getAsJsonObject().get("publisher_info").getAsString();
                        JsonObject publisherInfoObject = new JsonParser().parse(publisherInfo).getAsJsonObject();
                        String previewerUrl = publisherInfoObject.get("homeURL").getAsString();
                        int tenant = publisherInfoObject.get("tenant").getAsInt();
                        testDetail.setIncludesAdaptiveIndicator(includesAdaptiveIndicator);
                        testDetail.setPreviewerUrl(previewerUrl);
                        testDetail.setTenant(tenant);

                        List<Testmap> testMaps = new ArrayList<>();
                        try {
                            testMaps.addAll(divJobSetupUtil.getT3Testmap(stp, testDetail, testmapDetailService));
                        } catch (Exception e) {
                            Logger.error(e.getMessage());
                        }
                        if (testMaps.isEmpty()) {
                            Logger.error("event=\"Unable to process testmap.\", adminCode= \"{}\", accountCode= \"{}\", testCode= \"{}\", formCode= \"{}\"",
                                    stp.getAdminCode(), stp.getAccountCode(), testDetail.getTestCode(), testDetail.getFormCode());
                            errorForms.add(testDetail.getFormName());
                            if (removedFromIterator) {
                                removedFromIterator = false;
                                itr.remove();
                            }
                            continue;
                        }

                        Optional<TestmapForm> optionalTestmapForm = testMaps.stream()
                                .flatMap(testmap -> testmap.getForms().stream())
                                .filter(testmapForm -> testDetail.getFormCode().equals(testmapForm.getFormId()))
                                .findAny();

                        List<Item> epenItems = new ArrayList<>();
                        if (optionalTestmapForm.isPresent()) {

                            TestmapForm t3TestMap = optionalTestmapForm.get();
                            List<Item> t3Items = t3TestMap.getItems().stream().filter(item -> item.getUin() != null && !item.getUin().isEmpty()).collect(Collectors.toList());
                            epenItems = t3Items.stream().filter(divJobSetupUtil::containsEpenItem).collect(Collectors.toList());

                            Map<String, String> itemToMaxScoreMap = t3Items.stream().collect(Collectors.toMap(Item::getUin, Item::getMaxPoints));

                            //Adding TestCase Coverage logic
                            StudentTestSession studentTestSession = divJobSetupUtil.buildStudentTestSession(testDetail);
                            String testCode = testDetail.getTestCode();
                            String formCode = testDetail.getFormCode();
                            ScenarioCreatorUtil sc = new ScenarioCreatorUtil(scopeTreePath, stp.getAdminCode(),
                                    stp.getAccountCode(), studentTestSession, token, secret, previewerUrl,
                                    tenant, includesAdaptiveIndicator, null, testMaps);

                            Optional<ScenarioCreatorUtil.PreviewerDetail> previewerDetailVal = sc.getPreviewerItemDetails().stream()
                                    .filter(pid -> pid.getFormId().equals(formCode)).findAny();
                            if (!previewerDetailVal.isPresent()) {
                                Logger.error(
                                        "event=\"Unable to process test map.\", admin=\"{}\", account=\"{}\", testCode=\"{}\", formCode=\"{}\"",
                                        stp.getAdminCode(), stp.getAccountCode(), testCode, formCode);
                                return new JobProtocol.JobFinishMessage(jobId, false,
                                        JobProtocol.getJobResultJson("Unable to process test map with admin code "
                                                + stp.getAdminCode() + ", account code " + stp.getAccountCode() + ", test code " + testCode + ", and form code " + formCode));
                            }
                            ScenarioCreatorUtil.PreviewerDetail previewerDetail = previewerDetailVal.get();

                            List<String> itemIdentifiers = new ArrayList<>(previewerDetail.getItemIdentifiers());
                            TestNav8API testNav8Api = TestNav8API.connectToPreviewer("http://" + previewerUrl, token.toCharArray(), secret.toCharArray());
                            ItemResponsePool itemResponsePool = new ItemResponsePool(testNav8Api);
                            Map<String, List<ItemResponse>> itemResponseMap = itemResponsePool.getItemsItemResponsePool(tenant, itemIdentifiers)
                                    .parallelStream()
                                    .collect(Collectors.groupingBy(ItemResponse::getUIN));
                            List<ItemTestCaseCoverage.ItemCoverage> itemCoverageResult = getItemCoverages(tenant,
                                    itemToMaxScoreMap, itemIdentifiers, testNav8Api, itemResponseMap);

                            Set<String> sampleItems = sc.getSampleItems();
                            Set<String> surveyItems = sc.getSurveyItems();

                            List<Item> ktItems = sc.getKtItems(testCode, formCode);
                            itemCoverageResult = itemCoverageResult.stream()
                                    .filter(ic -> !sampleItems.contains(ic.getItemIdentifier()))
                                    .filter(ic -> !surveyItems.contains(ic.getItemIdentifier()))
                                    .filter(ic -> ktItems.contains(ic.getItemIdentifier()) && !ic.isCoveredMaxPoint())
                                    .collect(Collectors.toList());

                            List<ItemTestCaseCoverage.ItemCoverage> ktItemCoverageResult = getTestCaseCoverageForKtItems(testNav8Api, tenant, ktItems, itemResponseMap, itemToMaxScoreMap);
                            if (!itemCoverageResult.isEmpty() || !ktItemCoverageResult.isEmpty()) {
                                StringWriter outputWriter = new StringWriter();
                                if (!itemCoverageResult.isEmpty()) {
                                    new ItemTestCaseCoverage(null).itemCoverageListToCsv(itemCoverageResult, outputWriter);
                                }
                                if (!ktItemCoverageResult.isEmpty()) {
                                    outputWriter.append("\n\nItem Coverage Result for KT Items:\n");
                                    new ItemTestCaseCoverage(null).itemCoverageListToCsv(ktItemCoverageResult, outputWriter);
                                }
                                testDetail.setTestCaseCoverage(outputWriter.toString());
                            } else {
                                updateItemResponsePool(testMaps, itemResponseMap);
                            }
                        }
                        if (!epenItems.isEmpty()) {
                            disableEpenScenario = false;
                        }
                        break;
                    }
                }
                testDetail.setEpenScenarioDisable(disableEpenScenario);
            }

            List<String> messages = new ArrayList<>();
            if (!studentNotFoundForms.isEmpty()) {
                messages.add("No available student sessions found for the following forms: " + StringUtils.join(studentNotFoundForms, ", "));
            }
            if (!errorForms.isEmpty()) {
                messages.add("Unable to process the following forms: " + StringUtils.join(errorForms, ", "));
            }

            if (testDetails.isEmpty()) {
                if (messages.isEmpty()) {
                    messages.add("Unable to retrieve form information.");
                }
                return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson(messages));
            } else {
                ObjectNode jobData = Json.newObject();
                ObjectNode formsJson = jobData.putObject("forms");
                ArrayNode testDetailsJson = formsJson.putArray("testDetails");
                formsJson.set("testnavScenarioClasses", Json.toJson(tn8ScenarioClasses));
                formsJson.set("epenScenarioClasses", Json.toJson(epenScenarioClasses));
                for (TestDetail testDetail : testDetails) {
                    ObjectNode testDetailJson = (ObjectNode) Json.toJson(testDetail);
                    String testCaseCoverage = testDetail.getTestCaseCoverage();
                    if (testCaseCoverage != null && !testCaseCoverage.isEmpty()) {
                        testDetailJson.put("alertSuccess", false);
                        testDetailJson.put("alertMessage", "Insufficient Item Test Case Coverage found.");
                    }
                    testDetailsJson.add(testDetailJson);
                }

                if (messages.isEmpty()) {
                    return new JobProtocol.JobFinishMessage(jobId, true, JobProtocol.getJobResultJson(jobData));
                } else {
                    return new JobProtocol.JobFinishMessage(jobId, true, JobProtocol.getJobResultJson(jobData, messages));
                }
            }
        });

    }

    private JobProtocol.JobFinishMessage getStandardStudentScenarios(int environmentId, JsonNode json) {
        final Optional<Environment> environmentResult = jpaApi.withTransaction(() -> environmentService.findById(environmentId));

        if (!environmentResult.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol
                    .getJobResultJson("Can not find Environment for the environmentId " + environmentId));
        }
        List<TestDetail> testDetails = GSON.fromJson(json.toString(), LIST_TYPE);
        Optional<ScenarioType> testNavScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("TN8"));
        if (!testNavScenarioType.isPresent()) {
            Logger.error("event= \"Can not find TN8 ScenarioType in database\", \"scenarioType\" = \"TN8\"");
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson("Can not find TN8 ScenarioType in database. Failed to create TestNav Scenario"));
        }

        Optional<ScenarioType> epenScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("EPEN"));
        if (!epenScenarioType.isPresent()) {
            Logger.error("event= \"Can not find EPEN ScenarioType in database\", \"epenScenario\" \"EPEN\"");
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson("Can not find EPEN scenario type in database. Failed to create EPEN scenario."));
        }


        ArrayNode studentScenarios = Json.newArray();
        for (TestDetail testDetail : testDetails) {
            if (testDetail.getSelectedScenarios() == null || testDetail.getSelectedScenarios().isEmpty()) {
                continue;
            }

            final String testCode = testDetail.getTestCode();
            final String formCode = testDetail.getFormCode();


            Integer testNavScenarioTypeId = testNavScenarioType.get().getScenarioTypeId();
            Integer epenScenarioTypeId = epenScenarioType.get().getScenarioTypeId();

            for (DivJobSetupUtil.ScenarioDetail selectedScenario : testDetail.getSelectedScenarios()) {
                Integer testNavScenarioClassId = jpaApi.withTransaction(() -> {
                    String scenarioClassName = selectedScenario.getTestNavScenarioClassName();
                    if (scenarioClassName.startsWith("percentCorrect")) {
                        scenarioClassName = "percentCorrect";
                    }
                    return scenarioClassService.findByScenarioClass(scenarioClassName);
                }).map(ScenarioClass::getScenarioClassId).orElse(null);

                List<Scenario> testNavScenarios = jpaApi.withTransaction(() -> scenarioService.getScenarios(testCode, formCode, testNavScenarioTypeId, Collections.singletonList(testNavScenarioClassId), selectedScenario.getTestNavScenarioNames()));

                List<Scenario> epenScenarios = new ArrayList<>();
                if (selectedScenario.getEpenScenarioClassName() != null) {
                    jpaApi.withTransaction(() -> {
                        String scenarioClassName = selectedScenario.getEpenScenarioClassName();
                        if (scenarioClassName.startsWith("percentCorrect")) {
                            scenarioClassName = "percentCorrect";
                        }
                        return scenarioClassService.findByScenarioClass(scenarioClassName);
                    }).map(ScenarioClass::getScenarioClassId)
                            .ifPresent(epenScenarioClassId -> epenScenarios.addAll(
                                    jpaApi.withTransaction(() ->
                                            scenarioService.getScenarios(testCode, formCode, epenScenarioTypeId,
                                                    Collections.singletonList(epenScenarioClassId), selectedScenario.getEpenScenarioNames()))));
                }

                int longerListSize;

                if (testNavScenarios.size() > epenScenarios.size()) {
                    longerListSize = testNavScenarios.size();
                } else {
                    longerListSize = epenScenarios.size();
                }

                Map<String, String> testSessionToUuidMap = selectedScenario.getUuids();
                Iterator<Map.Entry<String, String>> itr = testSessionToUuidMap.entrySet().iterator();
                for (int i = 0; i < longerListSize; i++) {

                    Scenario testNavScenario;
                    if (i >= testNavScenarios.size()) {
                        testNavScenario = testNavScenarios.get(testNavScenarios.size() - 1);
                    } else {
                        testNavScenario = testNavScenarios.get(i);
                    }


                    double totalPoints = Json.parse(testNavScenario.getScenario()).get("totalPoints").asDouble();
                    testNavScenario.setPoints(totalPoints);
                    StudentScenario studentScenario = new StudentScenario();
                    Map.Entry<String, String> uuidToTestSession = null;
                    if (itr.hasNext()) {
                        uuidToTestSession = itr.next();
                        studentScenario.setUuid(uuidToTestSession.getKey());
                    }
                    studentScenario.setTestnavScenario(testNavScenario);
                    if (!epenScenarios.isEmpty()) {
                        Scenario epenScenario;
                        if (i >= epenScenarios.size()) {
                            epenScenario = epenScenarios.get(epenScenarios.size() - 1);
                        } else {
                            epenScenario = epenScenarios.get(i);
                        }
                        studentScenario.setEpenScenario(epenScenario);
                    }

                    TestSession testSession = new TestSession();
                    if (uuidToTestSession != null) {
                        testSession.setSessionName(uuidToTestSession.getValue());
                    }
                    studentScenario.setTestSession(testSession);
                    ObjectNode studentScenarioJson = (ObjectNode) Json.toJson(studentScenario);
                    studentScenarioJson.put("numberOfStudents", selectedScenario.getUuids().size());
                    studentScenarioJson.put("numberOfScenarios", longerListSize);
                    studentScenarios.add(studentScenarioJson);
                }
            }

        }
        ObjectNode jobData = Json.newObject();
        jobData.set("studentScenarios", Json.toJson(studentScenarios));
        return new JobProtocol.JobFinishMessage(jobId, true, JobProtocol.getJobResultJson(jobData));
    }

    private JobProtocol.JobFinishMessage getBatteryTestForms(final int environmentId,
                                                             final String scopeTreePath, final String sessionIds, final String testCodeString) {
        List<String> selectedTestCodes = Arrays.asList(testCodeString.split(","));

        List<SessionInfo> selectedTestSession = new ArrayList<>();
        List<String> selectedTestSessions = Arrays.asList(sessionIds.split(","));
        for (String sessionId : selectedTestSessions) {
            SessionInfo sessionInfo = new SessionInfo();
            sessionInfo.setSessionId(Integer.parseInt(sessionId));
            selectedTestSession.add(sessionInfo);
        }

        final String decodedScopeTreePath = StringUtils.replace(scopeTreePath, "|", "/");
        // Getting adminCode and accountCode from scopeTreePath
        DivJobSetupUtil.ScopeTreePath stp;
        try {
            stp = new DivJobSetupUtil.ScopeTreePath(decodedScopeTreePath);
        } catch (IllegalArgumentException ex) {
            return new JobProtocol.JobFinishMessage(jobId, false,
                    JobProtocol.getJobResultJson("Invalid ScopeTreePath found: " + decodedScopeTreePath));
        }

        final Optional<Environment> environment = jpaApi.withTransaction(() -> environmentService.findById(environmentId));

        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol
                    .getJobResultJson("Can not find Environment for the environmentId " + environmentId));
        }

        StudentTestSessionExtractor stse = divJobSetupUtil.getStudentTestSessionExtractor(environment.get().getFeMysqlConnection());

        List<Integer> sessions = selectedTestSession.stream().map(SessionInfo::getSessionId).collect(Collectors.toList());
        Map<String, List<StudentTestSession>> testSessionDetailMap = stse.getBatteryStudTestSessionDetails(decodedScopeTreePath, selectedTestCodes, sessions);
        int studentsInReadyState = testSessionDetailMap.size();
        //Excluding the battery student which already attempted test.
        Iterator<Map.Entry<String, List<StudentTestSession>>> iterator = testSessionDetailMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<StudentTestSession>> entry = iterator.next();
            List<String> uuids = entry.getValue().stream().map(StudentTestSession::getStudentTestUuid).collect(Collectors.toList());
            List<String> reservedUuids = jpaApi.withTransaction(() -> studentReservationService.findReservedStudentUUIDs(studentReservationTimeOut));
            // checking whether the students of all forms are reserved or not
            if (!ListUtils.intersection(reservedUuids, uuids).isEmpty()) {
                iterator.remove();
            }
        }

        if (testSessionDetailMap.isEmpty()) {
            String message = "No students found.Number of students in \"Ready\" status:" + studentsInReadyState
                    + ". Number of students reserved for other DIV jobs: " + studentsInReadyState;

            Logger.error("event= \"{}\", scopeTreePath= \"{}\",sessions= \"{}\" ", message, decodedScopeTreePath, Json.toJson(sessions));
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson(message));
        }

        //Finding battery forms from the list of batteryTestSession
        Map<String, List<StudentTestSession>> batteryForms = getBatteryForms(selectedTestCodes, selectedTestSessions, testSessionDetailMap);

        List<BatteryTest> batteryTests = new LinkedList<>();
        for (String batteryUUID : batteryForms.keySet()) {

            List<StudentTestSession> unitForms = batteryForms.get(batteryUUID);
            List<TestDetail> units = new ArrayList<>();

            //Adding unit level information to the battery
            String batteryTestCode = addBatteryUnits(unitForms, units);

            BatteryTest batteryTest = divJobSetupUtil.createBatteryTestObject(batteryTestCode, units);
            batteryTest.setRemainingStudents(getBatteryStudentCount(testSessionDetailMap, batteryUUID));
            batteryTest.setTestSessions(selectedTestSession);
            batteryTests.add(batteryTest);
        }

        List<String> errorForms = new ArrayList<>();
        Iterator<BatteryTest> itr = batteryTests.listIterator();

        while (itr.hasNext()) {
            Map<String, Set<ItemTestCaseCoverage.ItemCoverage>> itemCoverageFinalResult = new HashMap<>();
            Map<String, Set<ItemTestCaseCoverage.ItemCoverage>> ktItemCoverageFinalResult = new HashMap<>();
            boolean removedFromIterator = true;
            BatteryTest battery = itr.next();
            for (TestDetail unit : battery.getUnits()) {
                boolean disableEpenScenario = true;
                String form;
                try {
                    form = getTestFormsFromTestNav(unit);
                } catch (JsonSyntaxException ex) {
                    throw new JsonSyntaxException("Invalid response received while retrieving forms from TestNav. TestNav may be down: ;;" + unit.getTestnavWebServiceUrl());
                }
                if (form == null || !"yes".equalsIgnoreCase(new JsonParser().parse(form).getAsJsonObject().get("success").getAsString())) {
                    Logger.error("event= \"TestNav Publish API call failed.\", Customer Code= \"{}\", Test Number= \"{}\", apiurlBase= \"{}\".",
                            unit.getTestNavCustomerCode(), unit.getTestCode(), unit.getTestnavWebServiceUrl());

                    errorForms.add(unit.getFormName());
                    if (removedFromIterator) {
                        removedFromIterator = false;
                        itr.remove();
                    }
                    continue;
                }

                final JsonObject formObject = new JsonParser().parse(form).getAsJsonObject();
                final JsonArray formArray = formObject.get("msg").getAsJsonArray();
                for (JsonElement formElement : formArray) {
                    String currentFormNumber = formElement.getAsJsonObject().get("form_number").getAsString();
                    String formCode = unit.getFormCode();
                    if (unit.getFormCode().equals(currentFormNumber)) {
                        String includesAdaptiveIndicator = formElement.getAsJsonObject().get("includes_adaptive_ind").getAsString();
                        String publisherInfo = formElement.getAsJsonObject().get("publisher_info").getAsString();
                        JsonObject publisherInfoObject = new JsonParser().parse(publisherInfo).getAsJsonObject();
                        String previewerUrl = publisherInfoObject.get("homeURL").getAsString();
                        int tenant = publisherInfoObject.get("tenant").getAsInt();

                        unit.setIncludesAdaptiveIndicator(includesAdaptiveIndicator);
                        unit.setPreviewerUrl(previewerUrl);
                        unit.setTenant(tenant);

                        List<Testmap> testMaps = jpaApi.withTransaction(() -> divJobSetupUtil.getT3Testmap(stp, unit, testmapDetailService));
                        if (testMaps.isEmpty()) {
                            Logger.error("event=\"Unable to process testmap.\", adminCode= \"{}\", accountCode= \"{}\", testCode= \"{}\", formCode= \"{}\"",
                                    stp.getAdminCode(), stp.getAccountCode(), unit.getTestCode(), formCode);
                            errorForms.add(unit.getFormName());
                            if (removedFromIterator) {
                                removedFromIterator = false;
                                itr.remove();
                            }
                            continue;
                        }

                        Optional<TestmapForm> optionalTestmapForm = testMaps.stream()
                                .flatMap(testmap -> testmap.getForms().stream())
                                .filter(testmapForm -> formCode.equals(testmapForm.getFormId()))
                                .findAny();

                        List<Item> epenItems = new ArrayList<>();
                        if (optionalTestmapForm.isPresent()) {
                            TestmapForm t3TestMap = optionalTestmapForm.get();
                            List<Item> t3Items = t3TestMap.getItems().stream().filter(item -> item.getUin() != null && !item.getUin().isEmpty()).collect(Collectors.toList());
                            epenItems = t3Items.stream().filter(divJobSetupUtil::containsEpenItem).collect(Collectors.toList());

                            Map<String, String> itemToMaxScoreMap = t3Items.stream().collect(Collectors.toMap(Item::getUin, Item::getMaxPoints));

                            String testCode = unit.getTestCode();
                            StudentTestSession studentTestSession = divJobSetupUtil.buildStudentTestSession(unit);
                            ScenarioCreatorUtil sc = new ScenarioCreatorUtil(scopeTreePath, stp.getAdminCode(),
                                    stp.getAccountCode(), studentTestSession, token, secret, previewerUrl,
                                    tenant, includesAdaptiveIndicator, null, testMaps);

                            Optional<ScenarioCreatorUtil.PreviewerDetail> previewerDetailVal = sc.getPreviewerItemDetails().stream()
                                    .filter(pid -> pid.getFormId().equals(formCode)).findAny();
                            if (!previewerDetailVal.isPresent()) {
                                Logger.error(
                                        "event=\"Unable to process test map.\", admin=\"{}\", account=\"{}\", testCode=\"{}\", formCode=\"{}\"",
                                        stp.getAdminCode(), stp.getAccountCode(), testCode, formCode);
                                return new JobProtocol.JobFinishMessage(jobId, false,
                                        JobProtocol.getJobResultJson("Unable to process test map with admin code '"
                                                + stp.getAdminCode() + "', account code '" + stp.getAccountCode() + "', test code '" + testCode + "', and form code '" + formCode + "'"));
                            }
                            List<String> itemIdentifiers = previewerDetailVal.get().getItemIdentifiers();

                            TestNav8API testNav8Api = TestNav8API.connectToPreviewer("http://" + previewerUrl, token.toCharArray(), secret.toCharArray());
                            ItemResponsePool itemResponsePool = new ItemResponsePool(testNav8Api);

                            Map<String, List<ItemResponse>> itemResponseMap = itemResponsePool.getItemsItemResponsePool(tenant, itemIdentifiers)
                                    .parallelStream()
                                    .collect(Collectors.groupingBy(ItemResponse::getUIN));
                            List<ItemTestCaseCoverage.ItemCoverage> itemCoverageResult = getItemCoverages(tenant,
                                    itemToMaxScoreMap, itemIdentifiers, testNav8Api, itemResponseMap);

                            //Ignoring testCase coverage error report for sample and survey items
                            Set<String> sampleItems = sc.getSampleItems();
                            Set<String> surveyItems = sc.getSurveyItems();
                            List<Item> ktItems = sc.getKtItems(testCode, formCode);
                            itemCoverageResult = itemCoverageResult.stream()
                                    .filter(ic -> !sampleItems.contains(ic.getItemIdentifier()))
                                    .filter(ic -> !surveyItems.contains(ic.getItemIdentifier()))
                                    .filter(ic -> ktItems.contains(ic.getItemIdentifier()) && !ic.isCoveredMaxPoint())
                                    .collect(Collectors.toList());

                            List<ItemTestCaseCoverage.ItemCoverage> ktItemCoverageResult = jpaApi.withTransaction(
                                    () -> getTestCaseCoverageForKtItems(testNav8Api, tenant, ktItems, itemResponseMap, itemToMaxScoreMap));

                            if (!itemCoverageResult.isEmpty()) {
                                itemCoverageFinalResult.put(formCode, new HashSet<>(itemCoverageResult));
                            }
                            if (!ktItemCoverageResult.isEmpty()) {
                                ktItemCoverageFinalResult.put(formCode, new HashSet<>(ktItemCoverageResult));
                            } else {
                                jpaApi.withTransaction(() -> updateItemResponsePool(testMaps, itemResponseMap));
                            }
                        }
                        if (!epenItems.isEmpty()) {
                            disableEpenScenario = false;
                        }
                        break;
                    }
                }
                unit.setEpenScenarioDisable(disableEpenScenario);
            }
            StringWriter outputWriter = new StringWriter();
            if (!itemCoverageFinalResult.isEmpty()) {
                List<ItemTestCaseCoverage.ItemCoverage> list = new ArrayList<>();
                for (Set<ItemTestCaseCoverage.ItemCoverage> ics : itemCoverageFinalResult.values()) {
                    list.addAll(ics);
                }
                new ItemTestCaseCoverage(null).itemCoverageListToCsv(list, outputWriter);
                battery.setTestCaseCoverage(outputWriter.toString());
                battery.setErrorFormCodes(itemCoverageFinalResult.keySet());
            }
            if (!ktItemCoverageFinalResult.isEmpty()) {
                List<ItemTestCaseCoverage.ItemCoverage> list = new ArrayList<>();
                for (Set<ItemTestCaseCoverage.ItemCoverage> ics : ktItemCoverageFinalResult.values()) {
                    list.addAll(ics);
                }
                outputWriter.append("\n\nItem Coverage Result for KT Items:\n");
                new ItemTestCaseCoverage(null).itemCoverageListToCsv(list, outputWriter);
                battery.setTestCaseCoverage(outputWriter.toString());
                battery.setErrorFormCodes(ktItemCoverageFinalResult.keySet());
            }
        }

        List<String> messages = new ArrayList<>();
        if (!errorForms.isEmpty()) {
            messages.add("Unable to process the following forms: " + StringUtils.join(errorForms, ", "));
        }

        if (batteryTests.isEmpty() && messages.isEmpty()) {
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson("Unable to retrieve form information."));
        } else {
            ObjectNode jobData = generateBatteryTestFormsToDisplay(batteryTests);

            if (messages.isEmpty()) {
                return new JobProtocol.JobFinishMessage(jobId, true, JobProtocol.getJobResultJson(jobData));
            } else {
                return new JobProtocol.JobFinishMessage(jobId, true, JobProtocol.getJobResultJson(jobData, messages));
            }
        }
    }

    private ObjectNode generateBatteryTestFormsToDisplay(List<BatteryTest> batteryTests) {
        List<ScenarioClass> scenarioClasses = jpaApi.withTransaction(scenarioClassService::findAllEnabled);
        List<ScenarioClass> tn8ScenarioClasses = scenarioClasses.stream().filter(ScenarioClass::isEnabledForTn8).collect(Collectors.toList());
        List<ScenarioClass> epenScenarioClasses = scenarioClasses.stream().filter(ScenarioClass::isEnabledForEpen).collect(Collectors.toList());
        ObjectNode jobData = Json.newObject();
        ObjectNode formsJson = jobData.putObject("forms");
        ArrayNode testDetailsJson = formsJson.putArray("testDetails");
        formsJson.set("testnavScenarioClasses", Json.toJson(tn8ScenarioClasses));
        formsJson.set("epenScenarioClasses", Json.toJson(epenScenarioClasses));

        for (BatteryTest testDetail : batteryTests) {
            ObjectNode testDetailJson = (ObjectNode) Json.toJson(testDetail);
            testDetailsJson.add(testDetailJson);
            if (testDetail.getTestCaseCoverage() != null && !testDetail.getErrorFormCodes().isEmpty()) {
                testDetailJson.put("alertSuccess", false);
                testDetailJson.put("alertMessage", "Insufficient Item Test Case Coverage found.");
                testDetailJson.put("testCaseCoverage", testDetail.getTestCaseCoverage());
            }
        }
        return jobData;
    }

    private List<ItemTestCaseCoverage.ItemCoverage> getItemCoverages(int tenant, Map<String, String> itemToMaxScoreMap,
                                                                     List<String> itemIdentifiers,
                                                                     TestNav8API testNav8Api,
                                                                     Map<String, List<ItemResponse>> itemResponseMap) {
        List<ItemTestCaseCoverage.ItemCoverage> itemCoverageResult = new ArrayList<>();
        for (String itemIdentifier : itemIdentifiers) {
            String maxScoreString = itemToMaxScoreMap.get(itemIdentifier);
            double maxPoint = (maxScoreString != null) ? Double.parseDouble(maxScoreString) : 0.0;
            List<ItemResponse> irs = itemResponseMap.get(itemIdentifier);
            ItemTestCaseCoverage.ItemCoverage itemCoverage = getItemCoverageByItem(tenant, itemIdentifier, irs, maxPoint, testNav8Api);
            if (itemCoverage != null && !itemCoverage.isCovered()) {
                itemCoverageResult.add(itemCoverage);
            }
        }
        return itemCoverageResult;
    }

    private void updateItemResponsePool
            (List<Testmap> t3Testmaps, Map<String, List<ItemResponse>> itemResponseMap) {
        Testmap testmap = t3Testmaps.get(0);
        int testMapId = Integer.parseInt(testmap.getId());
        int testMapVersion = Integer.parseInt(testmap.getVersion());
        Optional<TestmapDetail> testMapDetailResult = testmapDetailService.findByTestmapIdNameAndVersion(testMapId, testmap.getName(), testMapVersion);
        if (testMapDetailResult.isPresent()) {
            List<com.pearson.itautomation.testscenariocreator.inputs.ItemResponse> result = new ArrayList<>();
            for (Map.Entry<String, List<ItemResponse>> entry : itemResponseMap.entrySet()) {
                List<com.pearson.itautomation.testscenariocreator.inputs.ItemResponse> itemResponses =
                        entry.getValue().stream().map(response -> new com.pearson.itautomation.testscenariocreator.inputs.ItemResponse(
                                response.getUIN(),
                                response.getPoints(),
                                response.isAttempted(),
                                response.getIdentifierToInteraction(),
                                response.getMods())).collect(Collectors.toList());
                result.addAll(itemResponses);
            }
            com.pearson.itautomation.testscenariocreator.inputs.ItemResponsePool pool =
                    new com.pearson.itautomation.testscenariocreator.inputs.ItemResponsePool(result);
            String responsePool = DivJobSetupUtil.GSON.toJson(pool);
            testMapDetailResult.get().setTempResponsePool(responsePool);
        }
    }

    private List<ItemTestCaseCoverage.ItemCoverage> getTestCaseCoverageForKtItems(TestNav8API testNav8API,
                                                                                  int tenant, List<Item> ktItems,
                                                                                  Map<String, List<ItemResponse>> itemResponseMap, Map<String, String> itemToMaxScoreMap) {
        List<ItemTestCaseCoverage.ItemCoverage> itemCoverages = new ArrayList<>();
        for (Item item : ktItems) {
            String uin = item.getUin();
            try {
                QtiItem qtiItem = new QtiItem(item.getItemXML());
                double maxPoint = Double.parseDouble(itemToMaxScoreMap.get(uin));
                List<ItemResponse> ktItemResponses = getKtItemResponses(qtiItem, itemResponseMap.remove(qtiItem.getItemIdentifier()));
                itemResponseMap.put(qtiItem.getItemIdentifier(), ktItemResponses);
                ItemTestCaseCoverage.ItemCoverage itemCoverage = new ItemTestCaseCoverage(testNav8API)
                        .new ItemCoverage(tenant, qtiItem, ktItemResponses, maxPoint);

                if (!itemCoverage.isCovered()) {
                    itemCoverages.add(itemCoverage);
                }
            } catch (Exception e) {
                Logger.error("event=\"Exception occurred getting item coverage details\", tenant=\"{}\", previewer=\"{}\", uin=\"{}\"",
                        tenant, testNav8API.getPreviewerUrl(), uin, e);
            }
        }
        return itemCoverages;
    }

    private List<ItemResponse> getKtItemResponses(QtiItem qtiItem, List<ItemResponse> itemResponses) {
        Map<String, Set<String>> identifierToDefaultResponseSetMap = qtiItem.getIdentifierToDefaultResponseMap();
        List<ItemResponse> ktItemResponses = new ArrayList<>();
        List<KtTestCase> ktTestCases = ktTestCaseService.findByIdentifiers(qtiItem.getItemIdentifier());

        if (ktTestCases.isEmpty()) {
            return new ArrayList<>();
        }

        Map<String, Map<Double, KtTestCase>> responseIdToKtUniqueScoreTestCase = new TreeMap<>();

        for (KtTestCase testCase : ktTestCases) {
            String ktResponseIdentifier = testCase.getResponseIdentifier();
            double score = Double.parseDouble(testCase.getKtSavedScore());
            if (responseIdToKtUniqueScoreTestCase.containsKey(ktResponseIdentifier)) {
                responseIdToKtUniqueScoreTestCase.get(ktResponseIdentifier).put(score, testCase);
            } else {
                Map<Double, KtTestCase> tempMap = new TreeMap(Collections.reverseOrder());
                tempMap.put(score, testCase);
                responseIdToKtUniqueScoreTestCase.put(ktResponseIdentifier, tempMap);
            }
        }

        List<Set<Double>> scoresForAllResponseIds = new LinkedList<>();

        for (Map.Entry<String, Map<Double, KtTestCase>> responseIdToScoreEntry : responseIdToKtUniqueScoreTestCase.entrySet()) {
            scoresForAllResponseIds.add(responseIdToScoreEntry.getValue().keySet());
        }

        Set<List<Double>> allScoreCombinations = Sets.cartesianProduct(scoresForAllResponseIds);
        Set<Double> tempSet = allScoreCombinations.stream()
                .map(scoreCombination -> scoreCombination.stream().mapToDouble(i -> i).sum())
                .collect(Collectors.toSet());

        for (ItemResponse iResp : itemResponses) {
            List<Double> tempUniqueKtItemScorePoints = new LinkedList<>(tempSet);

            int ktResponseIdCount = responseIdToKtUniqueScoreTestCase.size();
            if (iResp.isAttempted()) {
                while (tempUniqueKtItemScorePoints.size() > 0) {
                    boolean testCaseAdded = false;
                    double totalPoints = tempUniqueKtItemScorePoints.get(0);
                    double totalPointsFinal = totalPoints;
                    Set<String> updatedKtResponseIds = new HashSet<>();
                    String modsString = iResp.getMods();
                    JsonArray modsArray = new JsonParser().parse(modsString).getAsJsonArray();
                    for (JsonElement modElement : modsArray) {
                        if (testCaseAdded) {
                            break;
                        }
                        JsonObject mod = modElement.getAsJsonObject();
                        for (Map.Entry<String, Map<Double, KtTestCase>> tempMapResponseIdEntry : responseIdToKtUniqueScoreTestCase.entrySet()) {
                            if (testCaseAdded) {
                                break;
                            }
                            Map<Double, KtTestCase> tempScoreTestCase = tempMapResponseIdEntry.getValue();
                            if (tempMapResponseIdEntry.getKey().equals(mod.get("did").getAsString()) || identifierToDefaultResponseSetMap.size() == 1) {
                                for (Map.Entry<Double, KtTestCase> tempScoreTestCaseEntry : tempScoreTestCase.entrySet()) {
                                    if (testCaseAdded || updatedKtResponseIds.contains(tempMapResponseIdEntry.getKey())) {
                                        break;
                                    }
                                    double testCaseScore = tempScoreTestCaseEntry.getKey();
                                    if ((responseIdToKtUniqueScoreTestCase.size() > 1 && totalPoints - testCaseScore >= 0)
                                            || (responseIdToKtUniqueScoreTestCase.size() == 1 && testCaseScore == tempUniqueKtItemScorePoints.get(0))) {
                                        mod.add("r", new JsonParser().parse(tempScoreTestCaseEntry.getValue().getKtCandidateResponse()));
                                        totalPoints -= testCaseScore;
                                        updatedKtResponseIds.add(tempMapResponseIdEntry.getKey());
                                        if (updatedKtResponseIds.size() == ktResponseIdCount && totalPoints == 0) {
                                            ItemResponse iResponse = new ItemResponseBean(iResp.getUIN(), totalPointsFinal + iResp.getPoints(), true, iResp.getIdentifierToInteraction(), modsArray.toString(), null);
                                            ktItemResponses.add(iResponse);
                                            updatedKtResponseIds.clear();
                                            tempUniqueKtItemScorePoints.remove(0);
                                            testCaseAdded = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                ktItemResponses.add(iResp);
            }
        }
        return ktItemResponses;
    }

    /**
     * Returns an ItemCoverage object for the specified item in the specified tenant.
     *
     * @param tenantId       The id of the previewer tenant.
     * @param itemIdentifier The item identifier.
     * @param itemResponses  The list of ItemResponse objects for the item.
     * @param maxPoint       The maximum score point from T3 Testmap.
     * @return An ItemCoverage object for the specified item in the specified tenant.
     */
    private ItemTestCaseCoverage.ItemCoverage getItemCoverageByItem(final int tenantId,
                                                                    final String itemIdentifier,
                                                                    final List<ItemResponse> itemResponses,
                                                                    final double maxPoint,
                                                                    final TestNav8API tn8Api) {
        ItemTestCaseCoverage.ItemCoverage result = null;
        String msgDetail = "getting item coverage details for item '" + itemIdentifier + "' in tenant '" + tenantId
                + "' of previewer '" + tn8Api.getPreviewerUrl() + "'";

        try {
            GetXMLByIdentifierResult xmlByIdentifierResult = tn8Api.getItemXMLByIdentifier(tenantId, itemIdentifier);
            result = new ItemTestCaseCoverage(tn8Api)
                    .new ItemCoverage(tenantId, new QtiItem(xmlByIdentifierResult.getXML()), itemResponses, maxPoint);
        } catch (Exception e) {
            Logger.error("Exception occurred " + msgDetail, e);
        }
        return result;
    }

    private String addBatteryUnits(List<StudentTestSession> unitForms, List<TestDetail> units) {
        String batteryTestCode = null;
        for (StudentTestSession unitForm : unitForms) {
            batteryTestCode = unitForm.getBatteryTestCode();
            String unitTestCode = unitForm.getTestCode();
            String unitFormCode = unitForm.getFormCode();
            TestDetail unit = divJobSetupUtil.createTestDetailObject(unitTestCode, unitFormCode);
            unit.setFormName(unitForm.getFormName());
            unit.setTestNavCustomerCode(unitForm.getTn8CustomerCode());
            unit.setTestnavWebServiceUrl(unitForm.getTestnavWebServiceURL());
            unit.setTestnavClientIdentifier(unitForm.getTestnavClientIdentifier());
            unit.setTestnavClientSecret(unitForm.getTestnavClientSecret());
            units.add(unit);

        }
        return batteryTestCode;
    }

    private int getBatteryStudentCount(Map<String, List<StudentTestSession>> testSessionDetailMap, String
            batteryUuid) {
        Map<String, List<String>> batteryFormToUuidMapping = new HashMap<>();

        for (String batteryUUID : testSessionDetailMap.keySet()) {
            List<StudentTestSession> testSessions = testSessionDetailMap.get(batteryUUID);
            String batteryCodes = DivJobSetupUtil.getBatteryUnitFormCodes(testSessions);
            List<String> batteryUuids = new ArrayList<>();
            if (batteryFormToUuidMapping.containsKey(batteryCodes)) {
                batteryUuids.addAll(batteryFormToUuidMapping.get(batteryCodes));
            }
            batteryUuids.add(batteryUUID);
            batteryFormToUuidMapping.put(batteryCodes, batteryUuids);
        }
        return batteryFormToUuidMapping.entrySet()
                .stream()
                .filter(entry -> entry.getValue().contains(batteryUuid))
                .findFirst()
                .map(entry -> entry.getValue().size())
                .orElse(0);
    }

    private Map<String, List<StudentTestSession>> getBatteryForms
            (List<String> selectedTestCodes, List<String> selectedTestSessions, Map<String, List<StudentTestSession>> testSessionDetailMap) {
        Map<String, List<StudentTestSession>> batteryForms = new HashMap<>();
        List<List<String>> batteryTestFormCodes = new ArrayList<>();
        for (String batteryUUID : testSessionDetailMap.keySet()) {
            List<StudentTestSession> testSessions = testSessionDetailMap.get(batteryUUID).stream()
                    .filter(testSession -> selectedTestCodes.contains(testSession.getBatteryTestCode()))
                    .filter(ts -> selectedTestSessions.contains(ts.getSessionId()))
                    .collect(Collectors.toList());
            List<String> currentCodes = new ArrayList<>();
            for (StudentTestSession studentTestSession : testSessions) {
                String batteryTestCode = studentTestSession.getBatteryTestCode();
                String unitTestCode = studentTestSession.getTestCode();
                String unitFormCode = studentTestSession.getFormCode();
                String batteryCodes = batteryTestCode + unitTestCode + unitFormCode;
                currentCodes.add(batteryCodes);
            }

            if (!currentCodes.isEmpty() && !batteryTestFormCodes.contains(currentCodes)) {
                batteryForms.put(batteryUUID, testSessions);
                batteryTestFormCodes.add(currentCodes);
            }
        }
        return batteryForms;
    }

    private JobProtocol.JobFinishMessage getBatteryStudentScenarios(int environmentId,
                                                                    JsonNode json) {
        final Optional<Environment> environment = jpaApi.withTransaction(() -> environmentService.findById(environmentId));

        if (!environment.isPresent()) {
            Logger.error("event= \"Can not find Environment\", environmentId= \"{}\"", environmentId);
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol
                    .getJobResultJson("Can not find Environment for the environmentId " + environmentId));
        }

        Optional<ScenarioType> testNavScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("TN8"));
        if (!testNavScenarioType.isPresent()) {
            Logger.error("event= \"Can not find TN8 ScenarioType in database\", \"scenarioType\" = \"TN8\"");
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson(
                    "Can not find TN8 ScenarioType in database. Failed to create TestNav Scenario"));
        }
        Integer testNavScenarioTypeId = testNavScenarioType.get().getScenarioTypeId();
        Optional<ScenarioType> epenScenarioType = jpaApi.withTransaction(() -> scenarioTypeService.findByScenarioType("EPEN"));
        if (!epenScenarioType.isPresent()) {
            Logger.error("event= \"Can not find EPEN ScenarioType in database\", \"epenScenario\" \"EPEN\"");
            return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson(
                    "Can not find EPEN scenario type in database. Failed to create EPEN scenario."));
        }
        Integer epenScenarioTypeId = epenScenarioType.get().getScenarioTypeId();

        ArrayNode batteryStudentScenarios = Json.newArray();
        ArrayNode formDetails = (ArrayNode) json;
        for (JsonNode formDetail : formDetails) {
            String batteryTestCode = formDetail.get("batteryTestCode").asText();
            ArrayNode selectedScenarios = (ArrayNode) formDetail.get("selectedScenarios");
            for (JsonNode selectedScenario : selectedScenarios) {
                String testNavScenarioClass = selectedScenario.get("testNav").asText();
                ArrayNode units = (ArrayNode) selectedScenario.get("units");
                //batteryUuid as key and units as value
                Map<String, List<StudentScenario>> studentScenariosMap = new LinkedHashMap<>();
                int requiredStudentScenarios = getRequiredStudents(units);
                for (JsonNode unit : units) {
                    String testCode = unit.get("testCode").asText();
                    String formCode = unit.get("formCode").asText();
                    List<String> testNavScenarioNames = Json.fromJson(unit.get("testNavScenarioNames"), List.class);
                    if (testNavScenarioNames.isEmpty()) {
                        Logger.error("event= \"Can not find TestNav scenario names in the request body.\", \"requestBody\" \"{}\"", json.toString());
                        return new JobProtocol.JobFinishMessage(jobId, false, JobProtocol.getJobResultJson(
                                "Can not find TestNav scenario names in the request body."));
                    }
                    List<String> epenNavScenarioNames = Json.fromJson(unit.get("epenScenarioNames"), List.class);

                    Integer testNavScenarioClassId = jpaApi.withTransaction(() -> {
                        String scenarioClassName = testNavScenarioClass;
                        if (scenarioClassName.startsWith("percentCorrect")) {
                            scenarioClassName = "percentCorrect";
                        }
                        return scenarioClassService.findByScenarioClass(scenarioClassName);
                    }).map(ScenarioClass::getScenarioClassId).orElse(null);
                    List<Scenario> testNavScenarios = new ArrayList<>();
                    if (testNavScenarioClassId != null) {
                        testNavScenarios.addAll(jpaApi.withTransaction(() -> scenarioService.getScenarios(testCode, formCode, testNavScenarioTypeId, Collections.singletonList(testNavScenarioClassId), testNavScenarioNames)));
                    }
                    List<Scenario> epenScenarios = new ArrayList<>();
                    if (epenNavScenarioNames != null && !epenNavScenarioNames.isEmpty()) {
                        Integer epenScenarioClassId = jpaApi.withTransaction(() -> {
                            String scenarioClassName = selectedScenario.get("epen").asText();
                            if (scenarioClassName.startsWith("percentCorrect")) {
                                scenarioClassName = "percentCorrect";
                            }
                            return scenarioClassService.findByScenarioClass(scenarioClassName);
                        }).map(ScenarioClass::getScenarioClassId).orElse(null);

                        if (epenScenarioClassId != null) {
                            epenScenarios = jpaApi.withTransaction(() -> scenarioService.getScenarios(testCode, formCode, epenScenarioTypeId, Collections.singletonList(epenScenarioClassId), epenNavScenarioNames));
                        }
                    }

                    ArrayNode uuids = (ArrayNode) unit.get("uuids");
                    for (int i = 0; i < requiredStudentScenarios; i++) {
                        Scenario testNavScenario;
                        if (i >= testNavScenarios.size()) {
                            testNavScenario = testNavScenarios.get(testNavScenarios.size() - 1);
                        } else {
                            testNavScenario = testNavScenarios.get(i);
                        }

                        double totalPoints = Json.parse(testNavScenario.getScenario()).get("totalPoints").asDouble();
                        testNavScenario.setPoints(totalPoints);

                        StudentScenario studentScenario = new StudentScenario();
                        studentScenario.setTestnavScenario(testNavScenario);
                        Scenario epenScenario;
                        if (!epenScenarios.isEmpty()) {
                            if (i >= epenScenarios.size()) {
                                epenScenario = epenScenarios.get(epenScenarios.size() - 1);
                            } else {
                                epenScenario = epenScenarios.get(i);
                            }
                            studentScenario.setEpenScenario(epenScenario);
                        }
                        TestSession testSession = new TestSession();
                        JsonNode uuidNode = uuids.get(i);
                        String batteryUuid = null;
                        if (uuidNode != null) {
                            studentScenario.setUuid(uuidNode.get("uuid").asText());
                            testSession.setSessionName(uuidNode.get("testSessionName").asText());
                            testSession.setPanTestSessionId(uuidNode.get("testSessionId").asText());
                            batteryUuid = uuidNode.get("batteryUuid").asText();
                        }
                        studentScenario.setTestSession(testSession);
                        if (batteryUuid == null) {
                            batteryUuid = String.valueOf(i);
                        }
                        List<StudentScenario> studentScenarios = studentScenariosMap.get(batteryUuid);
                        if (studentScenarios != null) {
                            studentScenarios.add(studentScenario);
                        } else {
                            studentScenarios = new ArrayList<>();
                            studentScenarios.add(studentScenario);
                            studentScenariosMap.put(batteryUuid, studentScenarios);

                        }
                    }
                }

                for (Map.Entry<String, List<StudentScenario>> entry : studentScenariosMap.entrySet()) {
                    BatteryStudentScenario bss = new BatteryStudentScenario();
                    bss.setBatteryTestCode(batteryTestCode);
                    if (entry.getKey().length() > 10) {
                        bss.setBatteryUuid(entry.getKey());
                    }
                    bss.setStudentScenarios(entry.getValue());
                    ObjectNode bssJson = (ObjectNode) Json.toJson(bss);
                    bssJson.put("numberOfStudents", units.get(0).get("uuids").size());
                    bssJson.put("numberOfScenarios", requiredStudentScenarios);
                    batteryStudentScenarios.add(bssJson);
                }
            }
        }
        ObjectNode jobData = Json.newObject();
        jobData.set("studentScenarios", batteryStudentScenarios);
        return new JobProtocol.JobFinishMessage(jobId, true, JobProtocol.getJobResultJson(jobData));
    }

    private int getRequiredStudents(ArrayNode units) {
        int maxStudentScenarios = 0;
        for (JsonNode unit : units) {
            int numberOfTestnavScenario = unit.get("testNavScenarioNames").size();
            int numberOfEpenScenario = unit.get("epenScenarioNames").size();
            int numberOfStudents = (numberOfTestnavScenario > numberOfEpenScenario) ? numberOfTestnavScenario : numberOfEpenScenario;
            if (maxStudentScenarios < numberOfStudents) {
                maxStudentScenarios = numberOfStudents;
            }
        }
        return maxStudentScenarios;
    }

    /**
     * Makes TN8 API call and return a form as JSON String. Returns null if API call fails.
     *
     * @param testDetail The instance of {@link TestDetail}
     * @return The form as String if API call is success otherwise null
     */
    private String getTestFormsFromTestNav(final TestDetail testDetail) {
        try {
            final List<Header> headers = new ArrayList<>();
            headers.add(new BasicHeader("User-Agent", USER_AGENT_STRING));
            final CloseableHttpClient httpClient = HttpClients.custom().setMaxConnPerRoute(MAX_CONN_PER_ROUTE)
                    .setMaxConnTotal(MAX_CONN_TOTAL).setDefaultHeaders(headers).build();

            String newApiUrlBase = testDetail.getTestnavWebServiceUrl();

            if (newApiUrlBase != null && !newApiUrlBase.startsWith("https://")) {
                if (newApiUrlBase.startsWith("http://")) {
                    newApiUrlBase = newApiUrlBase.replaceFirst("http://", "https://");
                } else {
                    newApiUrlBase = "https://" + newApiUrlBase;
                }
            }

            URI accessTokenUri = new URI(
                    newApiUrlBase + "/oauth2/" + testDetail.getTestNavCustomerCode() + "/token");
            HttpPost httpPost = new HttpPost(accessTokenUri);
            httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
            List<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair("grant_type", "client_credentials"));
            nvps.add(new BasicNameValuePair("client_id", testDetail.getTestnavClientIdentifier()));
            nvps.add(new BasicNameValuePair("client_secret", testDetail.getTestnavClientSecret()));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps));

            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpResponseEntity = httpResponse.getEntity();
            String responseEntity = EntityUtils.toString(httpResponseEntity, StandardCharsets.UTF_8);

            Gson gson = new Gson();
            AccessTokenResp accessTokenResp = gson.fromJson(responseEntity, AccessTokenResp.class);

            if (accessTokenResp == null || accessTokenResp.getError() != null) {
                return null;
            }

            String accessToken = accessTokenResp.getAccessToken();

            String requestUrl = newApiUrlBase + "/publish/" + testDetail.getTestNavCustomerCode() + "/test/number/"
                    + testDetail.getTestCode() + "/forms?resultsPerPage=10000";
            URI uri = new URI(requestUrl);

            HttpGet httpGet = new HttpGet(uri);
            httpGet.addHeader("Authorization", "bearer " + accessToken);

            httpResponse = httpClient.execute(httpGet);
            httpResponseEntity = httpResponse.getEntity();
            return EntityUtils.toString(httpResponseEntity, StandardCharsets.UTF_8);
        } catch (IOException | URISyntaxException ex) {
            Logger.error("Failed to send post request! ", ex);
        }
        return null;
    }


    public enum DivJobSetupType {
        GET_FORMS,
        GET_STUDENT_SCENARIOS
    }

    public interface Factory {
        Actor create();
    }

    /**
     * A message telling the actor to run a DIV job setup job.
     */
    public static final class RunMessage {
        private final DivJobSetupType divJobSetupType;
        private final int environmentId;
        private final String testType;
        private final String scopeTreePath;
        private final String sessionIds;
        private final String testCode;
        private final JsonNode json;
        private final String jobId;

        /**
         * Create a RunMessage for getting test forms.
         *
         * @param environmentId The ID of the environment to get the forms for.
         * @param testType      The test type
         * @param scopeTreePath The scope tree path to get the forms for.
         * @param sessionIds    A comma separated list of session IDs to get forms for.
         * @param testCode      The test code to get forms for.
         * @param jobId         The job ID.
         */
        public RunMessage(int environmentId, String testType, String scopeTreePath, String sessionIds, String testCode,
                          String jobId) {
            this.divJobSetupType = DivJobSetupType.GET_FORMS;

            this.environmentId = environmentId;
            this.testType = testType;
            this.scopeTreePath = scopeTreePath;
            this.sessionIds = sessionIds;
            this.testCode = testCode;
            this.jobId = jobId;

            this.json = null;
        }

        /**
         * Create a RunMessage for getting student scenarios.
         *
         * @param environmentId The ID of the environment to get the forms for.
         * @param testType      The test type
         * @param scopeTreePath The scope tree path to get the forms for.
         * @param json          JSON containing selected scenario information.
         * @param jobId         The job ID.
         */
        public RunMessage(int environmentId, String testType, String scopeTreePath, JsonNode json, String jobId) {
            this.divJobSetupType = DivJobSetupType.GET_STUDENT_SCENARIOS;

            this.environmentId = environmentId;
            this.testType = testType;
            this.scopeTreePath = scopeTreePath;
            this.json = json;
            this.jobId = jobId;

            this.sessionIds = null;
            this.testCode = null;
        }

        /**
         * Get the environment ID.
         *
         * @return The environment ID.
         */
        int getEnvironmentId() {
            return environmentId;
        }

        /**
         * Get the test type.
         *
         * @return The test type.
         */
        String getTestType() {
            return testType;
        }

        /**
         * Get the scope tree path.
         *
         * @return The scope tree path.
         */
        String getScopeTreePath() {
            return scopeTreePath;
        }

        /**
         * Get the session IDs.
         *
         * @return The session IDs.
         */
        String getSessionIds() {
            return sessionIds;
        }

        /**
         * Get the test code.
         *
         * @return The test code.
         */
        String getTestCode() {
            return testCode;
        }

        /**
         * Get the JSON.
         *
         * @return The JSON.
         */
        JsonNode getJson() {
            return json;
        }

        /**
         * Get the job ID.
         *
         * @return The job ID.
         */
        String getJobId() {
            return jobId;
        }
    }
}