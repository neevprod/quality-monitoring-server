package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.MongoException;
import com.pearson.itautomation.ValidationError;
import com.pearson.itautomation.exceptions.ParseTestmapException;
import com.pearson.itautomation.panext.datafetcher.DataFetcherException;
import com.pearson.itautomation.stats.service.StatsInput;
import com.pearson.itautomation.stats.service.StatsServiceCall;
import com.pearson.itautomation.testmaps.models.Testmap;
import models.systemvalidation.StudentScenario;
import play.Logger;
import util.systemvalidation.StatsFactory;
import util.systemvalidation.T3TestMapCache;
import util.t3.T3TestMapException;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SELIGE on 4/11/2017.
 */
public class BatteryStatsValidationActor extends UntypedAbstractActor {
    private StatsFactory statsFactory;

    @Inject
    public BatteryStatsValidationActor(StatsFactory statsFactory) {
        this.statsFactory = statsFactory;
    }

    @Override
    public void onReceive(Object message) {
        if (message instanceof StatsValidationActor.StatsInputMessage) {
            StatsValidationActor.StatsInputMessage inputMsg = (StatsValidationActor.StatsInputMessage) message;
            final StudentScenario studentScenario = inputMsg.getStudentScenario();
            final long divJobExecId = studentScenario.getDivJobExec().getDivJobExecId();
            long batteryStudentScenarioId = studentScenario.getBatteryStudentScenario().getBatteryStudentScenarioId();
            long studentScenarioId = studentScenario.getStudentScenarioId();
            String stateName = studentScenario.getState().getStateName();
            if (inputMsg.getFailedMessage() != null) {
                final String validationResult = inputMsg.getFailedMessage();
                DataIntegrityValidationActor.FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);
                getSender().tell(finishMessage, getSelf());
                return;
            }
            try {
                Logger.info("event=\"BATTERY_STATS - execution started.\", divJobExecId=\"{}\", studentScenarioId=\"{}\", " +
                        "state=\"{}\"", divJobExecId, studentScenarioId, stateName);


                List<Testmap> t3TestMaps = getBatteryUnitsT3TestMap(inputMsg);


                StatsInput statsInput = statsFactory.getBatteryStatsInput(inputMsg);

                statsInput.setT3TestMap(t3TestMaps);

                StatsServiceCall sCall = statsFactory.getStatsServiceCall(statsInput);
                String validationResult = sCall.runComparison(true);

                String resultMessage = DataIntegrityValidationActor.FINISHED_MESSAGE;

                JsonObject resultJson = new JsonParser().parse(validationResult).getAsJsonObject();

                final String success = resultJson.get("success").getAsString();
                Logger.info("event=\"BATTERY_STATS - success result={}.\", divJobExecId=\"{}\", studentScenarioId=\"{}\", state=\"{}\"",
                        success, divJobExecId, studentScenarioId, stateName);

                if ("no".equalsIgnoreCase(success)) {
                    resultMessage = DataIntegrityValidationActor.FAILED_MESSAGE;
                }

                DataIntegrityValidationActor.FinishMessage finishMessage = generateFinishMessage(resultJson.toString(),
                        studentScenario, resultMessage);

                // Sends back the finish message to the sender actor
                getSender().tell(finishMessage, getSelf());

            } catch (MongoException ex) {
                final String validationResult = "Exception occurred during execution of Battery Stats Validator."
                        + " Could not connect to Data Warehouse Mongo DB.";
                DataIntegrityValidationActor.FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error(
                        "event=\"Exception occurred.\", divJobExecId=\"{}\", batteryStudentScenarioId=\"{}\", studentScenarioId=\"{}\", state=\"{}\"",
                        divJobExecId, batteryStudentScenarioId, studentScenarioId, stateName, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (DataFetcherException ex) {
                final String validationResult = "Exception occurred during execution of Battery Stats Validator."
                        + " Could not find database '" + inputMsg.getMongoInfo().getDatabase()
                        + "' in the Mongo instance.";
                DataIntegrityValidationActor.FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error(
                        "event=\"Exception occurred.\", divJobExecId=\"{}\", studentScenarioId=\"{}\", state=\"{}\"",
                        divJobExecId, studentScenarioId, stateName, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (SQLException ex) {
                final String validationResult = "Exception occurred during execution of Battery Stats Validator."
                        + " Could not connect to one or more MySQL WF DBs.";
                DataIntegrityValidationActor.FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error(
                        "event=\"Exception occurred.\", divJobExecId=\"{}\", studentScenarioId=\"{}\", state=\"{}\"",
                        divJobExecId, studentScenarioId, stateName, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (ParseTestmapException ex) {
                final String validationResult = "Exception occurred during execution of Battery Stats Validator. "
                        + ex.getMessage();
                DataIntegrityValidationActor.FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error(
                        "event=\"Exception occurred.\", divJobExecId=\"{}\", studentScenarioId=\"{}\", state=\"{}\"",
                        divJobExecId, studentScenarioId, stateName, ex);

                getSender().tell(finishMessage, getSelf());
            } catch (Throwable ex) {
                final String validationResult = "Exception occurred during execution of Battery Stats Validator.";

                DataIntegrityValidationActor.FinishMessage finishMessage = generateFinishMessage(
                        DataIntegrityValidationActor.getErrorJson(validationResult), studentScenario,
                        DataIntegrityValidationActor.FAILED_MESSAGE);

                Logger.error(
                        "event=\"Exception occurred.\", divJobExecId=\"{}\", studentScenarioId=\"{}\", state=\"{}\"",
                        divJobExecId, studentScenarioId, stateName, ex);

                getSender().tell(finishMessage, getSelf());
            } finally {
                getContext().stop(getSelf());
            }
        }

    }

    private List<Testmap> getBatteryUnitsT3TestMap(StatsValidationActor.StatsInputMessage inputMsg) throws T3TestMapException {
        List<Testmap> t3TestMaps = new ArrayList<>();
        for (StudentScenario ss : inputMsg.getBatteryUnitStudentScenarios()) {
            String testMapName = ss.getTestnavScenario().getTestmapDetail().getTestmapName();
            String accountCode = inputMsg.getAccountCode();
            String adminCode = inputMsg.getAdminCode();
            String testCode = ss.getTestnavScenario().getTestCode();
            String formCode = ss.getTestnavScenario().getFormCode();
            long divJobExecId = ss.getDivJobExec().getDivJobExecId();

            Testmap t3TestMap = T3TestMapCache.getT3TestMapFromCache(divJobExecId, testMapName);
            if (t3TestMap == null) {
                t3TestMap = T3TestMapCache.downloadTestMapFromT3(divJobExecId, accountCode, adminCode, testCode, formCode);
            }
            t3TestMaps.add(t3TestMap);
        }
        return t3TestMaps;
    }

    /**
     * Creates an instance of {@link DataIntegrityValidationActor.FinishMessage} to send back to the sender actor.
     *
     * @param validationResult The Optional of JsonString of {@link ValidationError}
     * @param studentScenario  The {@link StudentScenario}
     * @param finishMessage    The finished message
     * @return The {@link DataIntegrityValidationActor.FinishMessage}
     */
    private DataIntegrityValidationActor.FinishMessage generateFinishMessage(final String validationResult,
                                                                             final StudentScenario studentScenario, final String finishMessage) {
        return new DataIntegrityValidationActor.FinishMessage(studentScenario,
                studentScenario.getDivJobExec().getDivJobExecId(), finishMessage, validationResult);
    }

    public interface Factory {
        Actor create();
    }
}
