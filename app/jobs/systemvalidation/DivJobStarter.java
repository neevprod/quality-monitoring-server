package jobs.systemvalidation;

import akka.actor.Actor;
import akka.actor.ActorRef;
import com.google.inject.assistedinject.Assisted;
import com.typesafe.config.Config;
import jobs.JobStarter;

import javax.inject.Inject;
import java.time.Clock;

public class DivJobStarter extends JobStarter {
    private final int maxConcurrentInstances;
    private final DataIntegrityValidationActor.Factory divJobFactory;
    private final long divJobExecId;

    public interface Factory {
        DivJobStarter create(long divJobExecId);
    }

    @Inject
    DivJobStarter(Clock clock, DataIntegrityValidationActor.Factory divJobFactory, @Assisted long divJobExecId,
                  Config configuration) {
        super(clock);

        this.divJobFactory = divJobFactory;
        this.divJobExecId = divJobExecId;
        this.maxConcurrentInstances = configuration.getInt("application.jobs.dataIntegrityValidation.maxConcurrent");
    }

    /**
     * Get the job type.
     *
     * @return The job type.
     */
    @Override
    public String getJobType() {
        return "dataIntegrityValidation";
    }

    /**
     * Get the maximum number of instances of this job type that can be run concurrently.
     *
     * @return The maximum number of instances of this job type that can be run concurrently.
     */
    @Override
    public int getMaxConcurrentInstances() {
        return maxConcurrentInstances;
    }

    /**
     * Get the ID of the job.
     *
     * @return The job ID.
     */
    @Override
    public String getJobId() {
        StringBuilder jobId = new StringBuilder();

        if (getParentJobId().isPresent()) {
            jobId.append(getParentJobId().get()).append("_");
        }

        jobId.append("dataIntegrityValidation_").append(divJobExecId);

        return jobId.toString();
    }

    /**
     * Create an instance of the actor responsible for performing this job.
     *
     * @return An instance of the job actor.
     */
    @Override
    public Actor createJobActor() {
        return divJobFactory.create();
    }

    /**
     * Start the job.
     *
     * @param jobActor A reference to the job actor.
     * @param sender   A reference to the message sender.
     */
    @Override
    public void startJob(ActorRef jobActor, ActorRef sender) {
        jobActor.tell(new DataIntegrityValidationActor.RunMessage(getJobId(), divJobExecId), sender);
    }
}
