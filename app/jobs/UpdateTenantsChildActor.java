package jobs;

import akka.actor.Actor;
import akka.actor.UntypedAbstractActor;
import com.pearson.itautomation.tn8.previewer.api.GetTenantsResult;
import com.pearson.itautomation.tn8.previewer.api.TestNav8API;
import models.qtivalidation.Previewer;
import models.qtivalidation.PreviewerTenant;
import play.Logger;
import play.db.jpa.JPAApi;
import services.qtivalidation.PreviewerService;
import services.qtivalidation.PreviewerTenantService;
import util.qtivalidation.TestNav8APIFactory;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

public class UpdateTenantsChildActor extends UntypedAbstractActor {
    private final JPAApi jpaApi;
    private final TestNav8APIFactory apiFactory;
    private final PreviewerService previewerService;
    private final PreviewerTenantService previewerTenantService;

    /**
     * This class represents a message telling this child actor to update all tenants under a list of previewers.
     */
    public static final class UpdateMessage {
        private final Set<Integer> previewerIds;

        public UpdateMessage(Set<Integer> previewerIds) {
            this.previewerIds = Collections.unmodifiableSet(previewerIds);
        }

        /**
         * Get the list of previewer IDs.
         *
         * @return an unmodifiable list of previewer IDs
         */
        public Set<Integer> getPreviewerIds() {
            return previewerIds;
        }
    }

    public interface Factory {
        Actor create();
    }

    @Inject
    UpdateTenantsChildActor(JPAApi jpaApi, TestNav8APIFactory apiFactory,
                            PreviewerService previewerService, PreviewerTenantService previewerTenantService) {
        this.jpaApi = jpaApi;
        this.apiFactory = apiFactory;
        this.previewerService = previewerService;
        this.previewerTenantService = previewerTenantService;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof UpdateMessage) {
            Set<Integer> previewerIds = ((UpdateMessage) message).getPreviewerIds();
            if (!previewerIds.isEmpty()) {
                try {
                    updateTenants(previewerIds);

                    getSender().tell(UpdateTenantsActor.FINISHED_MESSAGE, getSelf());
                } catch (Exception e) {
                    Logger.error("event=\"Exception occurred while updating tenants in update tenants job.\"", e);

                    getSender().tell(UpdateTenantsActor.FAILED_MESSAGE, getSelf());
                } finally {
                    getContext().stop(getSelf());
                }
            }
        }
    }

    /**
     * Update the tenants under each previewer indicated in the given list.
     *
     * @param previewerIds a list of previewer IDs representing the previewers whose tenants should be updated
     */
    private void updateTenants(Set<Integer> previewerIds) {
        jpaApi.withTransaction(() -> {
            List<Previewer> previewers = previewerService.findAll(previewerIds);
            previewers.stream().forEach(this::updateTenantsInPreviewer);
        });
    }

    /**
     * Update the tenant table for the given previewer.
     *
     * @param previewer the previewer whose tenants should be updated
     */
    private void updateTenantsInPreviewer(Previewer previewer) {
        Map<Integer, GetTenantsResult.Tenant> apiTenants = getTenantsFromPreviewerApi(previewer);
        Map<Long, PreviewerTenant> dbTenants = getTenantsFromDb(previewer.getPreviewerId());
        makeNecessaryTenantModifications(previewer, apiTenants, dbTenants);
    }

    /**
     * Using the previewer API, get a map of the tenants belonging to the given previewer.
     *
     * @param previewer the previewer to filter by
     * @return a map of all tenants in the previewer
     */
    private Map<Integer, GetTenantsResult.Tenant> getTenantsFromPreviewerApi(Previewer previewer) {
        Optional<TestNav8API> testNav8ApiOptional = apiFactory.create(previewer.getPreviewerId());
        if (!testNav8ApiOptional.isPresent()) {
            throw new RuntimeException("Unable to find a previewer with the given ID.");
        } else {
            List<GetTenantsResult.Tenant> tenants = testNav8ApiOptional.get().getTenants().getTenants();
            return tenants.stream().collect(Collectors.toMap(GetTenantsResult.Tenant::getId, tenant -> tenant));
        }
    }

    /**
     * Using the database, get a map of the tenants belonging to the given previewer.
     *
     * @param previewerId the previewer to filter by
     * @return a map of all tenants in the previewer
     */
    private Map<Long, PreviewerTenant> getTenantsFromDb(int previewerId) {
        List<PreviewerTenant> previewerTenants = previewerTenantService.findAll(previewerId);
        return previewerTenants.stream().collect(Collectors.toMap(PreviewerTenant::getTenantId, tenant -> tenant));
    }

    /**
     * Based on the differences between the given API tenant list and the given database tenant list, make updates to
     * the tenant table. Possible changes are:
     *
     * 1. Added tenants
     * 2. Deactivated tenants
     * 3. Reactivated tenants
     * 4. Tenant name changes
     *
     * @param apiTenants a map containing tenants from the previewer API
     * @param dbTenants a map containing tenants from the database
     */
    private void makeNecessaryTenantModifications(Previewer previewer, Map<Integer, GetTenantsResult.Tenant> apiTenants,
                                                  Map<Long, PreviewerTenant> dbTenants) {
        for (Integer tenantId: apiTenants.keySet()) {
            GetTenantsResult.Tenant apiTenant = apiTenants.get(tenantId);
            PreviewerTenant dbTenant = dbTenants.get(tenantId.longValue());

            if (dbTenant == null) {
                Logger.info("event=\"Creating new tenant.\", previewerId=\"{}\", tenantId=\"{}\", tenantName=\"{}\"",
                        previewer.getPreviewerId(), apiTenant.getId(), apiTenant.getName());
                PreviewerTenant newTenant = new PreviewerTenant(previewer, (long) apiTenant.getId(),
                        apiTenant.getName(), true);
                previewerTenantService.create(newTenant);
            } else {
                if (!dbTenant.getActive()) {
                    Logger.info("event=\"Reactivating tenant.\", previewerId=\"{}\", tenantId=\"{}\", tenantName=\"{}\"",
                            previewer.getPreviewerId(), apiTenant.getId(), apiTenant.getName());
                    dbTenant.setActive(true);
                }
                if (!dbTenant.getName().equals(apiTenant.getName())) {
                    Logger.info("event=\"Renaming tenant.\", previewerId=\"{}\", tenantId=\"{}\", tenantName=\"{}\"",
                            previewer.getPreviewerId(), apiTenant.getId(), apiTenant.getName());
                    dbTenant.setName(apiTenant.getName());
                }
            }
        }

        dbTenants.keySet()
                .stream()
                .filter(key -> dbTenants.get(key).getActive())
                .forEach(tenantId -> {
                    PreviewerTenant dbTenant = dbTenants.get(tenantId);
                    if (tenantId < Integer.MIN_VALUE || tenantId > Integer.MAX_VALUE) {
                        throw new IllegalArgumentException("The number " + tenantId + " cannot be cast to an int because it is " +
                                "either too large or too small.");
                    }
                    GetTenantsResult.Tenant apiTenant = apiTenants.get(tenantId.intValue());

                    if (apiTenant == null) {
                        Logger.info("event=\"Deactivating tenant.\", previewerId=\"{}\", tenantId=\"{}\", tenantName=\"{}\"",
                                previewer.getPreviewerId(), dbTenant.getTenantId(), dbTenant.getName());
                        dbTenant.setActive(false);
                    }
                });
    }

}
