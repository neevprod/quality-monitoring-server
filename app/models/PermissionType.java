package models;

import javax.persistence.*;

@Entity
@Table(name = "permission_type")
public class PermissionType {
    private Integer permissionTypeId;
    private String permissionTypeName;

    @Id
    @Column(name = "permission_type_id")
    public Integer getPermissionTypeId() {
        return permissionTypeId;
    }

    public void setPermissionTypeId(Integer permissionTypeId) {
        this.permissionTypeId = permissionTypeId;
    }

    @Basic
    @Column(name = "permission_type_name")
    public String getPermissionTypeName() {
        return permissionTypeName;
    }

    public void setPermissionTypeName(String permissionTypeName) {
        this.permissionTypeName = permissionTypeName;
    }
}
