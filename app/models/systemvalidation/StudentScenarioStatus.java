package models.systemvalidation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the student_scenario_status database table.
 * 
 */
@Entity
@Table(name = "student_scenario_status")
@NamedQuery(name = "StudentScenarioStatus.findAll", query = "SELECT s FROM StudentScenarioStatus s")
public class StudentScenarioStatus implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_scenario_status_id", unique = true, nullable = false)
    private Integer studentScenarioStatusId;

    @Column(nullable = false, length = 50)
    private String status;

    public StudentScenarioStatus() {
    }

    public Integer getStudentScenarioStatusId() {
        return this.studentScenarioStatusId;
    }

    public void setStudentScenarioStatusId(Integer studentScenarioStatusId) {
        this.studentScenarioStatusId = studentScenarioStatusId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}