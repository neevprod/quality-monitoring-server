package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the external_defect_repository database table.
 * 
 */
@Entity
@Table(name = "external_defect_repository")
@NamedQuery(name = "ExternalDefectRepository.findAll", query = "SELECT e FROM ExternalDefectRepository e")
public class ExternalDefectRepository implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "external_defect_repository_id", unique = true, nullable = false)
    private Integer externalDefectRepositoryId;

    @Column(nullable = false, length = 50)
    private String name;

    public ExternalDefectRepository() {
    }

    public Integer getExternalDefectRepositoryId() {
        return this.externalDefectRepositoryId;
    }

    public void setExternalDefectRepositoryId(Integer externalDefectRepositoryId) {
        this.externalDefectRepositoryId = externalDefectRepositoryId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}