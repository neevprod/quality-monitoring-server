package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the scenario_param database table.
 * 
 */
@Entity
@Table(name = "scenario_param")
@NamedQuery(name = "ScenarioParam.findAll", query = "SELECT s FROM ScenarioParam s")
public class ScenarioParam implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "scenario_param_id", unique = true, nullable = false)
    private Long scenarioParamId;

    @Column(name = "scenario_param_key", nullable = false, length = 50)
    private String scenarioParamKey;

    @Column(name = "scenario_param_value", nullable = false, length = 50)
    private String scenarioParamValue;

    // bi-directional many-to-one association to Scenario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "scenario_id", nullable = false)
    private Scenario scenario;

    public ScenarioParam() {
    }

    public Long getScenarioParamId() {
        return this.scenarioParamId;
    }

    public void setScenarioParamId(Long scenarioParamId) {
        this.scenarioParamId = scenarioParamId;
    }

    public String getScenarioParamKey() {
        return this.scenarioParamKey;
    }

    public void setScenarioParamKey(String scenarioParamKey) {
        this.scenarioParamKey = scenarioParamKey;
    }

    public String getScenarioParamValue() {
        return this.scenarioParamValue;
    }

    public void setScenarioParamValue(String scenarioParamValue) {
        this.scenarioParamValue = scenarioParamValue;
    }

    public Scenario getScenario() {
        return this.scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

}