package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the testmap_detail database table.
 */
@Entity
@Table(name = "testmap_detail")
@NamedQuery(name = "TestmapDetail.findAll", query = "SELECT t FROM TestmapDetail t")
public class TestmapDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "testmap_detail_id", unique = true, nullable = false)
    private Integer testmapDetailId;

    @Column(name = "testmap_id", nullable = false)
    private Integer testmapId;

    @Column(name = "testmap_name", nullable = false, length = 255)
    private String testmapName;

    @Column(name = "testmap_publish_format", nullable = false, length = 1)
    private String testmapPublishFormat;

    @Column(name = "testmap_version", nullable = false)
    private Integer testmapVersion;

    // bi-directional many-to-one association to Scenario
    @OneToMany(mappedBy = "testmapDetail", fetch = FetchType.LAZY)
    private List<Scenario> scenarios;

    @Column(name = "testmap_xml", columnDefinition = "MEDIUMTEXT")
    private String testmapXml;

    @Column(name = "temp_response_pool", columnDefinition = "MEDIUMTEXT")
    private String tempResponsePool;

    @Column(name = "previewer_testmap_id")
    private Integer previewerTestmapId;

    public Integer getTestmapDetailId() {
        return this.testmapDetailId;
    }

    public void setTestmapDetailId(Integer testmapDetailId) {
        this.testmapDetailId = testmapDetailId;
    }

    public Integer getTestmapId() {
        return this.testmapId;
    }

    public void setTestmapId(Integer testmapId) {
        this.testmapId = testmapId;
    }

    public String getTestmapName() {
        return this.testmapName;
    }

    public void setTestmapName(String testmapName) {
        this.testmapName = testmapName;
    }

    public String getTestmapPublishFormat() {
        return this.testmapPublishFormat;
    }

    public void setTestmapPublishFormat(String testmapPublishFormat) {
        this.testmapPublishFormat = testmapPublishFormat;
    }

    public Integer getTestmapVersion() {
        return this.testmapVersion;
    }

    public void setTestmapVersion(Integer testmapVersion) {
        this.testmapVersion = testmapVersion;
    }

    public List<Scenario> getScenarios() {
        return this.scenarios;
    }

    public void setScenarios(List<Scenario> scenarios) {
        this.scenarios = scenarios;
    }

    public Scenario addScenario(Scenario scenario) {
        getScenarios().add(scenario);
        scenario.setTestmapDetail(this);

        return scenario;
    }

    public Scenario removeScenario(Scenario scenario) {
        getScenarios().remove(scenario);
        scenario.setTestmapDetail(null);

        return scenario;
    }

    public String getTestmapXml() {
        return testmapXml;
    }

    public void setTestmapXml(String testmapXml) {
        this.testmapXml = testmapXml;
    }

    public String getTempResponsePool() {
        return tempResponsePool;
    }

    public void setTempResponsePool(String tempResponsePool) {
        this.tempResponsePool = tempResponsePool;
    }

    public Integer getPreviewerTestmapId() {
        return previewerTestmapId;
    }

    public void setPreviewerTestmapId(Integer previewerTestmapId) {
        this.previewerTestmapId = previewerTestmapId;
    }
}