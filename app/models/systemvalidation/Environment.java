package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the environment database table.
 */
@Entity
@Table(name = "environment")
@NamedQuery(name = "Environment.findAll", query = "SELECT e FROM Environment e")
public class Environment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "environment_id", unique = true, nullable = false)
    private Integer environmentId;

    @Column(name = "fe_url", nullable = false, length = 140)

    private String feUrl;

    @Column(name = "epen_url", nullable = false, length = 140)

    private String epenUrl;

    @Column(nullable = false, unique = true, length = 50)
    private String name;

    @Column(name = "archived")
    private boolean archived;

    // bi-directional many-to-one association to ApiConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "be_api_connection_id", nullable = true)

    private ApiConnection beApiConnection;

    // bi-directional many-to-one association to ApiConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "data_warehouse_api_connection_id", nullable = true)

    private ApiConnection dataWarehouseApiConnection;

    // bi-directional many-to-one association to ApiConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "epen2_api_connection_id", nullable = true)

    private ApiConnection epen2ApiConnection;

    // bi-directional many-to-one association to ApiConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fe_api_connection_id", nullable = true)

    private ApiConnection feApiConnection;

    // bi-directional many-to-one association to ApiConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ice_bridge_api_connection_id", nullable = true)

    private ApiConnection iceBridgeApiConnection;

    // bi-directional many-to-one association to ApiConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "iris_api_connection_id", nullable = true)

    private ApiConnection irisApiConnection;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "be_mysql_connection_id", nullable = false)

    private DbConnection beMysqlConnection;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "data_warehouse_mongo_connection_id", nullable = false)

    private DbConnection dataWarehouseMongoConnection;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "epen2_mysql_connection_id", nullable = false)

    private DbConnection epen2MysqlConnection;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fe_mysql_connection_id", nullable = false)

    private DbConnection feMysqlConnection;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ice_bridge_oracle_connection_id", nullable = true)

    private DbConnection iceBridgeOracleConnection;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "iris_mysql_connection_id", nullable = false)

    private DbConnection irisMysqlConnection;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "epen2_mongo_connection_id", nullable = false)
    private DbConnection epen2MongoConnection;

    public Environment() {
    }

    /**
     * @return the environmentId
     */
    public final Integer getEnvironmentId() {
        return environmentId;
    }

    /**
     * @param environmentId the environmentId to set
     */
    public final void setEnvironmentId(Integer environmentId) {
        this.environmentId = environmentId;
    }

    /**
     * @return the feUrl
     */
    public String getFeUrl() {
        return feUrl;
    }

    /**
     * @param feUrl the feUrl to set
     */
    public void setFeUrl(String feUrl) {
        this.feUrl = feUrl;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    public boolean getArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    /**
     * @return the beApiConnection
     */
    public ApiConnection getBeApiConnection() {
        return beApiConnection;
    }

    /**
     * @param beApiConnection the beApiConnection to set
     */
    public void setBeApiConnection(ApiConnection beApiConnection) {
        this.beApiConnection = beApiConnection;
    }

    /**
     * @return the dataWarehouseApiConnection
     */
    public ApiConnection getDataWarehouseApiConnection() {
        return dataWarehouseApiConnection;
    }

    /**
     * @param dataWarehouseApiConnection the dataWarehouseApiConnection to set
     */
    public void setDataWarehouseApiConnection(ApiConnection dataWarehouseApiConnection) {
        this.dataWarehouseApiConnection = dataWarehouseApiConnection;
    }

    /**
     * @return the epen2ApiConnection
     */
    public ApiConnection getEpen2ApiConnection() {
        return epen2ApiConnection;
    }

    /**
     * @param epen2ApiConnection the epen2ApiConnection to set
     */
    public void setEpen2ApiConnection(ApiConnection epen2ApiConnection) {
        this.epen2ApiConnection = epen2ApiConnection;
    }

    /**
     * @return the feApiConnection
     */
    public ApiConnection getFeApiConnection() {
        return feApiConnection;
    }

    /**
     * @param feApiConnection the feApiConnection to set
     */
    public void setFeApiConnection(ApiConnection feApiConnection) {
        this.feApiConnection = feApiConnection;
    }

    /**
     * @return the iceBridgeApiConnection
     */
    public ApiConnection getIceBridgeApiConnection() {
        return iceBridgeApiConnection;
    }

    /**
     * @param iceBridgeApiConnection the iceBridgeApiConnection to set
     */
    public void setIceBridgeApiConnection(ApiConnection iceBridgeApiConnection) {
        this.iceBridgeApiConnection = iceBridgeApiConnection;
    }

    /**
     * @return the irisApiConnection
     */
    public ApiConnection getIrisApiConnection() {
        return irisApiConnection;
    }

    /**
     * @param irisApiConnection the irisApiConnection to set
     */
    public void setIrisApiConnection(ApiConnection irisApiConnection) {
        this.irisApiConnection = irisApiConnection;
    }

    /**
     * @return the beMysqlConnection
     */
    public DbConnection getBeMysqlConnection() {
        return beMysqlConnection;
    }

    /**
     * @param beMysqlConnection the beMysqlConnection to set
     */
    public void setBeMysqlConnection(DbConnection beMysqlConnection) {
        this.beMysqlConnection = beMysqlConnection;
    }

    /**
     * @return the dataWarehouseMongoConnection
     */
    public DbConnection getDataWarehouseMongoConnection() {
        return dataWarehouseMongoConnection;
    }

    /**
     * @param dataWarehouseMongoConnection the dataWarehouseMongoConnection to set
     */
    public void setDataWarehouseMongoConnection(DbConnection dataWarehouseMongoConnection) {
        this.dataWarehouseMongoConnection = dataWarehouseMongoConnection;
    }

    /**
     * @return the epen2MysqlConnection
     */
    public DbConnection getEpen2MysqlConnection() {
        return epen2MysqlConnection;
    }

    /**
     * @param epen2MysqlConnection the epen2MysqlConnection to set
     */
    public void setEpen2MysqlConnection(DbConnection epen2MysqlConnection) {
        this.epen2MysqlConnection = epen2MysqlConnection;
    }

    /**
     * @return the feMysqlConnection
     */
    public DbConnection getFeMysqlConnection() {
        return feMysqlConnection;
    }

    /**
     * @param feMysqlConnection the feMysqlConnection to set
     */
    public void setFeMysqlConnection(DbConnection feMysqlConnection) {
        this.feMysqlConnection = feMysqlConnection;
    }

    /**
     * @return the iceBridgeOracleConnection
     */
    public DbConnection getIceBridgeOracleConnection() {
        return iceBridgeOracleConnection;
    }

    /**
     * @param iceBridgeOracleConnection the iceBridgeOracleConnection to set
     */
    public void setIceBridgeOracleConnection(DbConnection iceBridgeOracleConnection) {
        this.iceBridgeOracleConnection = iceBridgeOracleConnection;
    }

    /**
     * @return the irisMysqlConnection
     */
    public DbConnection getIrisMysqlConnection() {
        return irisMysqlConnection;
    }

    /**
     * @param irisMysqlConnection the irisMysqlConnection to set
     */
    public void setIrisMysqlConnection(DbConnection irisMysqlConnection) {
        this.irisMysqlConnection = irisMysqlConnection;
    }

    /**
     * @return the serialversionuid
     */
    public static final long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * @return the epenUrl
     */
    public String getEpenUrl() {
        return epenUrl;
    }

    /**
     * @param epenUrl the epenUrl to set
     */
    public void setEpenUrl(String epenUrl) {
        this.epenUrl = epenUrl;
    }

    /**
     * @return the epen2MongoConnection
     */
    public DbConnection getEpen2MongoConnection() {
        return epen2MongoConnection;
    }

    /**
     * @param epen2MongoConnection the epen2MongoConnection to set
     */
    public void setEpen2MongoConnection(DbConnection epen2MongoConnection) {
        this.epen2MongoConnection = epen2MongoConnection;
    }
}
