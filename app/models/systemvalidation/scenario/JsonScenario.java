
package models.systemvalidation.scenario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JsonScenario {

    @SerializedName("scenarioName")
    @Expose
    private String scenarioName;
    @SerializedName("scenarioType")
    @Expose
    private String scenarioType;
    @SerializedName("testNumber")
    @Expose
    private String testNumber;
    @SerializedName("formNumber")
    @Expose
    private String formNumber;
    @SerializedName("itemResponses")
    @Expose
    private List<ItemResponse> itemResponses = new ArrayList<ItemResponse>();

    /**
     * @return The scenarioName
     */
    public String getScenarioName() {
        return scenarioName;
    }

    /**
     * @param scenarioName The scenarioName
     */
    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    /**
     * @return The scenarioType
     */
    public String getScenarioType() {
        return scenarioType;
    }

    /**
     * @param scenarioType The scenarioType
     */
    public void setScenarioType(String scenarioType) {
        this.scenarioType = scenarioType;
    }

    /**
     * @return The testNumber
     */
    public String getTestNumber() {
        return testNumber;
    }

    /**
     * @param testNumber The testNumber
     */
    public void setTestNumber(String testNumber) {
        this.testNumber = testNumber;
    }

    /**
     * @return The formNumber
     */
    public String getFormNumber() {
        return formNumber;
    }

    /**
     * @param formNumber The formNumber
     */
    public void setFormNumber(String formNumber) {
        this.formNumber = formNumber;
    }

    /**
     * @return The itemResponses
     */
    public List<ItemResponse> getItemResponses() {
        return itemResponses;
    }

    /**
     * @param itemResponses The itemResponses
     */
    public void setItemResponses(List<ItemResponse> itemResponses) {
        this.itemResponses = itemResponses;
    }
}