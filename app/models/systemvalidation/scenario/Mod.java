
package models.systemvalidation.scenario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Mod<T> {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("sid")
    @Expose
    private String sid;

    @SerializedName("r")
    @Expose
    private T r;

    @SerializedName("responses")
    @Expose
    private List<Response> responses;

    @SerializedName("did")
    @Expose
    private String did;

    @SerializedName("it")
    @Expose
    private String it;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The sid
     */
    public String getSid() {
        return sid;
    }

    /**
     * @param sid The sid
     */
    public void setSid(String sid) {
        this.sid = sid;
    }

    /**
     * @return The r
     */
    public T getR() {
        return r;
    }

    /**
     * @param r The r
     */
    public void setR(T r) {
        this.r = r;
    }

    /**
     * @return The did
     */
    public String getDid() {
        return did;
    }

    /**
     * @param did The did
     */
    public void setDid(String did) {
        this.did = did;
    }

    public String getIt() {
        return it;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }
}