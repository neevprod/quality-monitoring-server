package models.systemvalidation.scenario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response<T> {

    @SerializedName("r")
    @Expose
    private T r;
    private String version;

    public T getR() {
        return r;
    }

    public void setR(T r) {
        this.r = r;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}