
package models.systemvalidation.scenario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdentifiersToInteraction {

    @SerializedName("identifier")
    @Expose
    private String identifier;
    @SerializedName("interactionType")
    @Expose
    private String interactionType;

    /**
     * 
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * 
     * @param identifier
     *            The identifier
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * 
     * @return The interactionType
     */
    public String getInteractionType() {
        return interactionType;
    }

    /**
     * 
     * @param interactionType
     *            The interactionType
     */
    public void setInteractionType(String interactionType) {
        this.interactionType = interactionType;
    }

}
