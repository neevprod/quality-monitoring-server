
package models.systemvalidation.scenario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Outcome {

    @SerializedName("SCORE")
    @Expose
    private String SCORE;

    @SerializedName("version")
    @Expose
    private String version;

    /**
     * @return The SCORE
     */
    public String getSCORE() {
        return SCORE;
    }

    /**
     * @param SCORE The SCORE
     */
    public void setSCORE(String SCORE) {
        this.SCORE = SCORE;
    }

    /*
     * @return The VERSION
     */
    public String getVersion() {
        return version;
    }

    /*
     * @param VERSION
     * The VERSION
     */
    public void setVersion(String version) {
        this.version = version;
    }
}
