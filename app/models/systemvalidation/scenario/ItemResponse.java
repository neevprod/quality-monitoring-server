
package models.systemvalidation.scenario;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ItemResponse {

    @SerializedName("uin")
    @Expose
    private String uin;

    @SerializedName("outcomes")
    @Expose
    private List<Outcome> outcomes = new ArrayList<Outcome>();

    @SerializedName("identifiersToInteractions")
    @Expose
    private List<IdentifiersToInteraction> identifiersToInteractions = new ArrayList<IdentifiersToInteraction>();

    @SerializedName("mods")
    @Expose
    private List<Mod> mods;

    @SerializedName("paperResponseStr")
    @Expose
    private String paperResponseStr;

    /**
     * @return The uin
     */
    public String getUin() {
        return uin;
    }

    /**
     * @param uin The uin
     */
    public void setUin(String uin) {
        this.uin = uin;
    }

    /**
     * @return The outcomes
     */
    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    /**
     * @param outcomes The outcomes
     */
    public void setOutcomes(List<Outcome> outcomes) {
        this.outcomes = outcomes;
    }

    /**
     * @return The identifiersToInteractions
     */
    public List<IdentifiersToInteraction> getIdentifiersToInteractions() {
        return identifiersToInteractions;
    }

    /**
     * @param identifiersToInteractions The identifiersToInteractions
     */
    public void setIdentifiersToInteractions(List<IdentifiersToInteraction> identifiersToInteractions) {
        this.identifiersToInteractions = identifiersToInteractions;
    }

    /**
     * @return The mods
     */
    public List<Mod> getMods() {
        return mods;
    }

    /**
     * @param mods The mods
     */
    public void setMods(List<Mod> mods) {
        this.mods = mods;
    }

    /**
     * @return The paperResponseStr
     */
    public String getPaperResponseStr() {
        return paperResponseStr;
    }

    /**
     * @param paperResponseStr The paperResponseStr
     */
    public void setPaperResponseStr(String paperResponseStr) {
        this.paperResponseStr = paperResponseStr;
    }
}