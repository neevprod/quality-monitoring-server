package models.systemvalidation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import util.EncryptorDecryptor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the api_connection database table.
 * 
 */
@Entity
@Table(name = "api_connection")
@NamedQuery(name = "ApiConnection.findAll", query = "SELECT a FROM ApiConnection a")
public class ApiConnection implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "api_connection_id", unique = true, nullable = false)
    private Integer apiConnectionId;

    @Column(name = "api_url", nullable = false, length = 140)
    private String apiUrl;

    @Column(nullable = false, length = 50)
    @Convert(converter = EncryptorDecryptor.class)
    @JsonIgnore
    private String secret;

    @Column(nullable = false, length = 50)
    private String token;

    public ApiConnection() {
    }

    public Integer getApiConnectionId() {
        return this.apiConnectionId;
    }

    public void setApiConnectionId(Integer apiConnectionId) {
        this.apiConnectionId = apiConnectionId;
    }

    public String getApiUrl() {
        return this.apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}