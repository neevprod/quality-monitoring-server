package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the path database table.
 * 
 */
@Entity
@Table(name = "path")
public class Path implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "path_id", unique = true, nullable = false)
    private Integer pathId;

    @Column(name = "path_description", length = 255)
    private String pathDescription;

    @Column(name = "path_name", nullable = false, length = 50)
    private String pathName;

    // bi-directional many-to-one association to PathState
    @OneToMany(mappedBy = "path", fetch = FetchType.EAGER)
    private List<PathState> pathStates;

    public Path() {
    }

    public Integer getPathId() {
        return this.pathId;
    }

    public void setPathId(Integer pathId) {
        this.pathId = pathId;
    }

    public String getPathDescription() {
        return this.pathDescription;
    }

    public void setPathDescription(String pathDescription) {
        this.pathDescription = pathDescription;
    }

    public String getPathName() {
        return this.pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public List<PathState> getPathStates() {
        return this.pathStates;
    }

    public void setPathStates(List<PathState> pathStates) {
        this.pathStates = pathStates;
    }
}