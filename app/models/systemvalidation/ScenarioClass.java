package models.systemvalidation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the scenario_class database table.
 * 
 */
@Entity
@Table(name = "scenario_class")
@NamedQuery(name = "ScenarioClass.findAll", query = "SELECT s FROM ScenarioClass s")
public class ScenarioClass implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "scenario_class_id", unique = true, nullable = false)
    private Integer scenarioClassId;

    @Column(name = "scenario_class", nullable = false, length = 50)
    private String scenarioClass;

    @Column(name = "enabled_for_tn8")
    @JsonIgnore
    private boolean enabledForTn8;

    @Column(name = "enabled_for_epen")
    @JsonIgnore
    private boolean enabledForEpen;

    public ScenarioClass() {
    }

    public Integer getScenarioClassId() {
        return this.scenarioClassId;
    }

    public void setScenarioClassId(Integer scenarioClassId) {
        this.scenarioClassId = scenarioClassId;
    }

    public String getScenarioClass() {
        return this.scenarioClass;
    }

    public void setScenarioClass(String scenarioClass) {
        this.scenarioClass = scenarioClass;
    }

    public boolean isEnabledForTn8() {
        return enabledForTn8;
    }

    public void setEnabledForTn8(boolean enabledForTn8) {
        this.enabledForTn8 = enabledForTn8;
    }

    public boolean isEnabledForEpen() {
        return enabledForEpen;
    }

    public void setEnabledForEpen(boolean enabledForEpen) {
        this.enabledForEpen = enabledForEpen;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((scenarioClass == null) ? 0 : scenarioClass.hashCode());
        result = prime * result + ((scenarioClassId == null) ? 0 : scenarioClassId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ScenarioClass other = (ScenarioClass) obj;
        if (scenarioClass == null) {
            if (other.scenarioClass != null)
                return false;
        } else if (!scenarioClass.equals(other.scenarioClass))
            return false;
        if (scenarioClassId == null) {
            if (other.scenarioClassId != null)
                return false;
        } else if (!scenarioClassId.equals(other.scenarioClassId))
            return false;
        return true;
    }

}