package models.systemvalidation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import models.User;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the div_job_exec database table.
 */
@Entity
@Table(name = "div_job_exec")
@NamedQuery(name = "DivJobExec.findAll", query = "SELECT d FROM DivJobExec d")
public class DivJobExec implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "div_job_exec_id", unique = true, nullable = false)
    private Long divJobExecId;

    @Column(name = "complete_time_stamp", insertable = false)
    private Timestamp completeTimeStamp;

    @Column(name = "completed", nullable = false, insertable = false)
    private Boolean completed;

    @Column(name = "priority", nullable = false)
    @JsonIgnore
    private Integer priority;

    @Column(name = "submit_time_stamp", nullable = true, insertable = false, updatable = false)
    private Timestamp submitTimeStamp;

    @Column(name = "submit_user_id", nullable = false)
    @JsonIgnore
    private Long submitUserId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submit_user_id", insertable = false, updatable = false)
    private User submitUser;

    // bi-directional many-to-one association to DivJobExecStatus
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "div_job_exec_status_id", insertable = false, updatable = false)
    private DivJobExecStatus divJobExecStatus;

    @Column(name = "div_job_exec_status_id", nullable = false)
    @JsonIgnore
    private Integer divJobExecStatusId;

    // bi-directional many-to-one association to DivJobExecType
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "div_job_exec_type_id", insertable = false, updatable = false)
    private DivJobExecType divJobExecType;

    @Column(name = "div_job_exec_type_id", nullable = false)
    private Integer divJobExecTypeId;

    // bi-directional many-to-one association to Environment
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "environment_id", nullable = false)
    private Environment environment;

    // bi-directional many-to-one association to Path
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "path_id", nullable = false)
    private Path path;

    @Column(name = "scope_tree_path", nullable = false, length = 2000)
    private String scopeTreePath;

    // bi-directional many-to-one association to StudentScenario
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "divJobExec", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<StudentScenario> studentScenarios;

    @Transient
    private Integer numStudentScenarios;

    @Transient
    private Integer numCompleted;

    @Transient
    private Integer numInProgress;

    @Transient
    private Integer numFailed;

    @Transient
    private Integer numNotStarted;

    @Transient
    private String testType;

    @Column(name = "test_type_id", nullable = false)
    private Integer testTypeId;

    @Transient
    private boolean hasStatsState;

    public DivJobExec() {
        studentScenarios = new ArrayList<>();
    }

    public Long getDivJobExecId() {
        return this.divJobExecId;
    }

    public void setDivJobExecId(Long divJobExecId) {
        this.divJobExecId = divJobExecId;
    }

    public Timestamp getCompleteTimeStamp() {
        return this.completeTimeStamp;
    }

    public void setCompleteTimeStamp(Timestamp completeTimeStamp) {
        this.completeTimeStamp = completeTimeStamp;
    }

    public Boolean isCompleted() {
        return this.completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Integer getPriority() {
        return this.priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Timestamp getSubmitTimeStamp() {
        return this.submitTimeStamp;
    }

    public void setSubmitTimeStamp(Timestamp submitTimeStamp) {
        this.submitTimeStamp = submitTimeStamp;
    }

    public Long getSubmitUserId() {
        return this.submitUserId;
    }

    public void setSubmitUserId(Long submitUserId) {
        this.submitUserId = submitUserId;
    }

    public User getSubmitUser() {
        return submitUser;
    }

    public void setSubmitUser(User submitUser) {
        this.submitUser = submitUser;
    }

    public DivJobExecStatus getDivJobExecStatus() {
        return this.divJobExecStatus;
    }

    public void setDivJobExecStatus(DivJobExecStatus divJobExecStatus) {
        this.divJobExecStatus = divJobExecStatus;
    }

    public Integer getDivJobExecStatusId() {
        return divJobExecStatusId;
    }

    public void setDivJobExecStatusId(Integer divJobExecStatusId) {
        this.divJobExecStatusId = divJobExecStatusId;
    }

    public DivJobExecType getDivJobExecType() {
        return this.divJobExecType;
    }

    public void setDivJobExecType(DivJobExecType divJobExecType) {
        this.divJobExecType = divJobExecType;
    }

    public Integer getDivJobExecTypeId() {
        return divJobExecTypeId;
    }

    public void setDivJobExecTypeId(Integer divJobExecTypeId) {
        this.divJobExecTypeId = divJobExecTypeId;
    }

    public Environment getEnvironment() {
        return this.environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public Path getPath() {
        return this.path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public List<StudentScenario> getStudentScenarios() {
        return this.studentScenarios;
    }

    public void setStudentScenarios(List<StudentScenario> studentScenarios) {
        this.studentScenarios = studentScenarios;
    }

    public StudentScenario addStudentScenario(StudentScenario studentScenario) {
        getStudentScenarios().add(studentScenario);
        studentScenario.setDivJobExec(this);

        return studentScenario;
    }

    public StudentScenario removeStudentScenario(StudentScenario studentScenario) {
        getStudentScenarios().remove(studentScenario);
        studentScenario.setDivJobExec(null);

        return studentScenario;
    }

    public Integer getNumStudentScenarios() {
        if (numStudentScenarios == null) {
            return studentScenarios.size();
        } else {
            return numStudentScenarios;
        }
    }

    public void setNumStudentScenarios(Integer numStudentScenarios) {
        this.numStudentScenarios = numStudentScenarios;
    }

    /**
     * @return the numCompleted
     */
    public final Integer getNumCompleted() {
        return numCompleted;
    }

    /**
     * @param numCompleted the numCompleted to set
     */
    public final void setNumCompleted(Integer numCompleted) {
        this.numCompleted = numCompleted;
    }

    /**
     * @return the numInProgress
     */
    public final Integer getNumInProgress() {
        return numInProgress;
    }

    /**
     * @param numInProgress the numInProgress to set
     */
    public final void setNumInProgress(Integer numInProgress) {
        this.numInProgress = numInProgress;
    }

    /**
     * @return the numFailed
     */
    public final Integer getNumFailed() {
        return numFailed;
    }

    /**
     * @param numFailed the numFailed to set
     */
    public final void setNumFailed(Integer numFailed) {
        this.numFailed = numFailed;
    }

    /**
     * @return the numNotStarted
     */
    public final Integer getNumNotStarted() {
        return numNotStarted;
    }

    /**
     * @param numNotStarted the numNotStarted to set
     */
    public final void setNumNotStarted(Integer numNotStarted) {
        this.numNotStarted = numNotStarted;
    }

    public String getScopeTreePath() {
        return this.scopeTreePath;
    }

    public void setScopeTreePath(String scopeTreePath) {
        this.scopeTreePath = scopeTreePath;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public boolean isHasStatsState() {
        return hasStatsState;
    }

    public void setHasStatsState(boolean hasStatsState) {
        this.hasStatsState = hasStatsState;
    }

    public Integer getTestTypeId() {
        return testTypeId;
    }

    public void setTestTypeId(Integer testTypeId) {
        this.testTypeId = testTypeId;
    }
}