package models.systemvalidation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the state database table.
 * 
 */
@Entity
@Table(name = "state")
@NamedQuery(name = "State.findAll", query = "SELECT s FROM State s")
public class State implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "state_id", unique = true, nullable = false)
    private Integer stateId;

    @Column(name = "auto_retries", nullable = false)
    private Integer autoRetries;

    @Column(name = "state_description", length = 255)
    private String stateDescription;

    @Column(name = "state_name", nullable = false, length = 50)
    private String stateName;

    public State() {
    }

    public Integer getStateId() {
        return this.stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getAutoRetries() {
        return this.autoRetries;
    }

    public void setAutoRetries(Integer autoRetries) {
        this.autoRetries = autoRetries;
    }

    public String getStateDescription() {
        return this.stateDescription;
    }

    public void setStateDescription(String stateDescription) {
        this.stateDescription = stateDescription;
    }

    public String getStateName() {
        return this.stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}