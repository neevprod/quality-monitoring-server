package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the student_scenario_result database table.
 */
@Entity
@Table(name = "student_scenario_result")
@NamedQuery(name = "StudentScenarioResult.findAll", query = "SELECT s FROM StudentScenarioResult s")
public class StudentScenarioResult implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_scenario_result_id", unique = true, nullable = false)
    private Long studentScenarioResultId;

    @Lob
    @Column(name = "result_details", nullable = false)
    private String resultDetails;

    @Column(nullable = false)
    private Timestamp timestamp;

    // bi-directional many-to-one association to State
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "state_id", insertable = false, updatable = false)
    private State state;

    @Column(name = "state_id", unique = true, nullable = false)
    private Integer stateId;

    // bi-directional many-to-one association to StudentScenario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_scenario_id", insertable = false, updatable = false)
    private StudentScenario studentScenario;

    @Column(name = "student_scenario_id", nullable = false)
    private Long studentScenarioId;

    // bi-directional many-to-one association to StudentScenarioResultComment
    @OneToMany(mappedBy = "studentScenarioResult", fetch = FetchType.LAZY)
    private List<StudentScenarioResultComment> studentScenarioResultComments;

    @Column(name = "success")
    private boolean success;

    @Column(name = "version")
    private Integer version;

    public StudentScenarioResult() {
    }

    public Long getStudentScenarioResultId() {
        return this.studentScenarioResultId;
    }

    public void setStudentScenarioResultId(Long studentScenarioResultId) {
        this.studentScenarioResultId = studentScenarioResultId;
    }

    public String getResultDetails() {
        return this.resultDetails;
    }

    public void setResultDetails(String resultDetails) {
        this.resultDetails = resultDetails;
    }

    public Timestamp getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public StudentScenario getStudentScenario() {
        return this.studentScenario;
    }

    public void setStudentScenario(StudentScenario studentScenario) {
        this.studentScenario = studentScenario;
    }

    public List<StudentScenarioResultComment> getStudentScenarioResultComments() {
        return this.studentScenarioResultComments;
    }

    public void setStudentScenarioResultComments(List<StudentScenarioResultComment> studentScenarioResultComments) {
        this.studentScenarioResultComments = studentScenarioResultComments;
    }

    public StudentScenarioResultComment addStudentScenarioResultComment(
            StudentScenarioResultComment studentScenarioResultComment) {
        getStudentScenarioResultComments().add(studentScenarioResultComment);

        return studentScenarioResultComment;
    }

    public StudentScenarioResultComment removeStudentScenarioResultComment(
            StudentScenarioResultComment studentScenarioResultComment) {
        getStudentScenarioResultComments().remove(studentScenarioResultComment);

        return studentScenarioResultComment;
    }

    /**
     * @return the stateId
     */
    public Integer getStateId() {
        return stateId;
    }

    /**
     * @param stateId the stateId to set
     */
    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    /**
     * @return the studentScenarioId
     */
    public Long getStudentScenarioId() {
        return studentScenarioId;
    }

    /**
     * @param studentScenarioId the studentScenarioId to set
     */
    public void setStudentScenarioId(Long studentScenarioId) {
        this.studentScenarioId = studentScenarioId;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * @return the version
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(final Integer version) {
        this.version = version;
    }

}