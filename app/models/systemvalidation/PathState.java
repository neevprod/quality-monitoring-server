package models.systemvalidation;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the path_state database table.
 * 
 */
@Entity
@Table(name = "path_state")
public class PathState implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "path_state_id", unique = true, nullable = false)
    private Integer pathStateId;

    // bi-directional many-to-one association to State
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "state_id", nullable = false)
    private State state;

    // bi-directional many-to-one association to Path
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "path_id", nullable = false)
    @JsonIgnore
    private Path path;

    @Basic
    @Column(name = "path_state_order", nullable = false)
    private Integer pathStateOrder;

    public PathState() {
    }

    public Integer getPathStateId() {
        return this.pathStateId;
    }

    public void setPathStateId(Integer pathStateId) {
        this.pathStateId = pathStateId;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Path getPath() {
        return this.path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public Integer getPathStateOrder() {
        return pathStateOrder;
    }

    public void setPathStateOrder(Integer pathStateOrder) {
        this.pathStateOrder = pathStateOrder;
    }

}