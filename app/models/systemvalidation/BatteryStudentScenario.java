package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the battery_student_scenario database table.
 */
@Entity
@Table(name = "battery_student_scenario")
public class BatteryStudentScenario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "battery_student_scenario_id", unique = true, nullable = false)
    private Long batteryStudentScenarioId;

    @Column(name = "battery_uuid", nullable = false)
    private String batteryUuid;

    @Column(name = "battery_test_code", nullable = false)
    private String batteryTestCode;

    // bi-directional many-to-one association to StudentScenario
    @OneToMany(mappedBy = "batteryStudentScenario", fetch = FetchType.LAZY)
    private List<StudentScenario> studentScenarios;

    public BatteryStudentScenario() {
        studentScenarios = new ArrayList<>();
    }

    public Long getBatteryStudentScenarioId() {
        return batteryStudentScenarioId;
    }

    public void setBatteryStudentScenarioId(Long batteryStudentScenarioId) {
        this.batteryStudentScenarioId = batteryStudentScenarioId;
    }

    public String getBatteryUuid() {
        return batteryUuid;
    }

    public void setBatteryUuid(String batteryUuid) {
        this.batteryUuid = batteryUuid;
    }

    public String getBatteryTestCode() {
        return batteryTestCode;
    }

    public void setBatteryTestCode(String batteryTestCode) {
        this.batteryTestCode = batteryTestCode;
    }

    public List<StudentScenario> getStudentScenarios() {
        return studentScenarios;
    }

    public void setStudentScenarios(List<StudentScenario> studentScenarios) {
        this.studentScenarios = studentScenarios;
    }
}
