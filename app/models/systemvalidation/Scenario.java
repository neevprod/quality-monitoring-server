package models.systemvalidation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import models.qtivalidation.PreviewerTenant;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the scenario database table.
 */
@Entity
@Table(name = "scenario")
@NamedQuery(name = "Scenario.findAll", query = "SELECT s FROM Scenario s")
public class Scenario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "scenario_id", unique = true, nullable = false)
    private int scenarioId;

    @Lob
    @Column(name = "scenario", nullable = false)
    @JsonIgnore
    private String scenario;

    // bi-directional many-to-one association to ScenarioClass
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "scenario_class_id", nullable = false)
    private ScenarioClass scenarioClass;

    // bi-directional many-to-one association to ScenarioType
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "scenario_type_id", nullable = false)
    @JsonIgnore
    private ScenarioType scenarioType;

    // bi-directional many-to-one association to ScenarioParam
    @OneToMany(mappedBy = "scenario", fetch = FetchType.LAZY)
    private List<ScenarioParam> scenarioParams;

    @Column(name = "form_code", nullable = false, length = 255)
    private String formCode;

    @Column(name = "scenario_name", nullable = false, length = 255)
    private String scenarioName;

    @Column(name = "test_code", nullable = false, length = 100)
    private String testCode;

    // bi-directional many-to-one association to TestmapDetail
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "testmap_detail_id")
    @JsonIgnore
    private TestmapDetail testmapDetail;

    @Column(name = "previewer_tenant_id")
    private Long previewerTenantId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previewer_tenant_id", insertable = false, updatable = false)
    @JsonIgnore
    private PreviewerTenant previewerTenant;

    @Transient
    private Double points;

    public int getScenarioId() {
        return this.scenarioId;
    }

    public void setScenarioId(int scenarioId) {
        this.scenarioId = scenarioId;
    }

    public String getFormCode() {
        return this.formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getScenario() {
        return this.scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getScenarioName() {
        return this.scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public String getTestCode() {
        return this.testCode;
    }

    public void setTestCode(String testCode) {
        this.testCode = testCode;
    }

    public ScenarioClass getScenarioClass() {
        return this.scenarioClass;
    }

    public void setScenarioClass(ScenarioClass scenarioClass) {
        this.scenarioClass = scenarioClass;
    }

    public ScenarioType getScenarioType() {
        return this.scenarioType;
    }

    public void setScenarioType(ScenarioType scenarioType) {
        this.scenarioType = scenarioType;
    }

    public TestmapDetail getTestmapDetail() {
        return this.testmapDetail;
    }

    public void setTestmapDetail(TestmapDetail testmapDetail) {
        this.testmapDetail = testmapDetail;
    }

    public List<ScenarioParam> getScenarioParams() {
        return this.scenarioParams;
    }

    public void setScenarioParams(List<ScenarioParam> scenarioParams) {
        this.scenarioParams = scenarioParams;
    }

    public ScenarioParam addScenarioParam(ScenarioParam scenarioParam) {
        getScenarioParams().add(scenarioParam);
        scenarioParam.setScenario(this);

        return scenarioParam;
    }

    public ScenarioParam removeScenarioParam(ScenarioParam scenarioParam) {
        getScenarioParams().remove(scenarioParam);
        scenarioParam.setScenario(null);

        return scenarioParam;
    }

    /**
     * @return the previewerTenantId
     */
    public Long getPreviewerTenantId() {
        return previewerTenantId;
    }

    /**
     * @param previewerTenantId the previewerTenantId to set
     */
    public void setPreviewerTenantId(Long previewerTenantId) {
        this.previewerTenantId = previewerTenantId;
    }

    /**
     * @return the previewerTenant
     */
    public PreviewerTenant getPreviewerTenant() {
        return previewerTenant;
    }

    /**
     * @param previewerTenant the previewerTenant to set
     */
    public void setPreviewerTenant(PreviewerTenant previewerTenant) {
        this.previewerTenant = previewerTenant;
    }

    /**
     * @return the points
     */
    public final Double getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public final void setPoints(Double points) {
        this.points = points;
    }

}