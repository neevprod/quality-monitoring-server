package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Nand Joshi
 */
@Entity
@Table(name = "student_reservation")
public class StudentReservation implements Serializable {
    private static final long serialVersionUID = 6544217505732403238L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_reservation_id", unique = true, nullable = false)
    private Long studentReservationId;

    @Column(name = "uuid", unique = true, nullable = false)
    private String uuid;

    @Column(name = "start_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false,
            updatable = false, nullable = false)
    private Timestamp startTime;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_scenario_id", insertable = false, updatable = false)
    private StudentScenario studentScenario;

    @Column(name = "student_scenario_id", unique = true)
    private Long studentScenarioId;

    /**
     * @return the studentReservationId
     */
    public final Long getStudentReservationId() {
        return studentReservationId;
    }

    /**
     * @param studentReservationId the studentReservationId to set
     */
    public final void setStudentReservationId(Long studentReservationId) {
        this.studentReservationId = studentReservationId;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the startTime
     */
    public Timestamp getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the studentScenario
     */
    public StudentScenario getStudentScenario() {
        return studentScenario;
    }

    /**
     * @param studentScenario the studentScenario to set
     */
    public void setStudentScenario(StudentScenario studentScenario) {
        this.studentScenario = studentScenario;
    }

    /**
     * @return the studentScenarioId
     */
    public Long getStudentScenarioId() {
        return studentScenarioId;
    }

    /**
     * @param studentScenarioId the studentScenarioId to set
     */
    public void setStudentScenarioId(Long studentScenarioId) {
        this.studentScenarioId = studentScenarioId;
    }

}
