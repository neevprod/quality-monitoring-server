package models.systemvalidation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import models.User;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The persistent class for the student_scenario_result_comment database table.
 */
@Entity
@Table(name = "student_scenario_result_comment")
@NamedQuery(name = "StudentScenarioResultComment.findAll", query = "SELECT s FROM StudentScenarioResultComment s")
public class StudentScenarioResultComment implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_scenario_result_comment_id", unique = true, nullable = false)
    private Long studentScenarioResultCommentId;

    @Column(name = "external_defect_id", length = 50)
    private String externalDefectId;

    @Column(name = "result_comment", nullable = false, length = 512)
    private String resultComment;

    @Column(name = "timestamp", nullable = false, insertable = false)
    private Timestamp timestamp;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    // bi-directional many-to-one association to User
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false, insertable = false, updatable = false)
    private User user;

    // bi-directional many-to-one association to ExternalDefectRepository
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "external_defect_repository_id")
    private ExternalDefectRepository externalDefectRepository;

    // bi-directional many-to-one association to ResultCommentCategory
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_scenario_result_comment_category_id", nullable = false)
    private StudentScenarioResultCommentCategory studentScenarioResultCommentCategory;

    // bi-directional many-to-one association to StudentScenarioResult
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_scenario_result_id", nullable = false, insertable = false, updatable = false)
    private StudentScenarioResult studentScenarioResult;

    @Column(name = "student_scenario_result_id")
    private Long studentScenarioResultId;

    /**
     * @return the studentScenarioResultId
     */
    public Long getStudentScenarioResultId() {
        return studentScenarioResultId;
    }

    /**
     * @param studentScenarioResultId the studentScenarioResultId to set
     */
    public void setStudentScenarioResultId(Long studentScenarioResultId) {
        this.studentScenarioResultId = studentScenarioResultId;
    }

    @Transient
    private String stateName;

    public StudentScenarioResultComment() {
    }

    public Long getResultCommentId() {
        return this.studentScenarioResultCommentId;
    }

    public void setResultCommentId(Long resultCommentId) {
        this.studentScenarioResultCommentId = resultCommentId;
    }

    public String getExternalDefectId() {
        return this.externalDefectId;
    }

    public void setExternalDefectId(String externalDefectId) {
        this.externalDefectId = externalDefectId;
    }

    public String getResultComment() {
        return this.resultComment;
    }

    public void setResultComment(String resultComment) {
        this.resultComment = resultComment;
    }

    public Timestamp getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public ExternalDefectRepository getExternalDefectRepository() {
        return this.externalDefectRepository;
    }

    public void setExternalDefectRepository(ExternalDefectRepository externalDefectRepository) {
        this.externalDefectRepository = externalDefectRepository;
    }

    /**
     * @return the studentScenarioResultCommentCategory
     */
    public StudentScenarioResultCommentCategory getStudentScenarioResultCommentCategory() {
        return studentScenarioResultCommentCategory;
    }

    /**
     * @param studentScenarioResultCommentCategory the studentScenarioResultCommentCategory to set
     */
    public void setStudentScenarioResultCommentCategory(
            StudentScenarioResultCommentCategory studentScenarioResultCommentCategory) {
        this.studentScenarioResultCommentCategory = studentScenarioResultCommentCategory;
    }

    public StudentScenarioResult getStudentScenarioResult() {
        return this.studentScenarioResult;
    }

    public void setStudentScenarioResult(StudentScenarioResult studentScenarioResult) {
        this.studentScenarioResult = studentScenarioResult;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the stateName
     */
    public final String getStateName() {
        return stateName;
    }

    /**
     * @param stateName the stateName to set
     */
    public final void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @PostLoad
    private void onLoad() {
        if (studentScenarioResult != null && studentScenarioResult.getState() != null) {
            setStateName(studentScenarioResult.getState().getStateName());
        }

    }
}