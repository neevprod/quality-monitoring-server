package models.systemvalidation;

public class DbConnectionDto {
    private Integer dbConnectionId;
    private String dbName;
    private String dbPass;
    private String dbUser;
    private String host;
    private Integer port;

    /**
     * @return the dbConnectionId
     */
    public final Integer getDbConnectionId() {
        return dbConnectionId;
    }

    /**
     * @param dbConnectionId
     *            the dbConnectionId to set
     */
    public final void setDbConnectionId(Integer dbConnectionId) {
        this.dbConnectionId = dbConnectionId;
    }

    /**
     * @return the dbName
     */
    public final String getDbName() {
        return dbName;
    }

    /**
     * @param dbName
     *            the dbName to set
     */
    public final void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * @return the dbPass
     */
    public final String getDbPass() {
        return dbPass;
    }

    /**
     * @param dbPass
     *            the dbPass to set
     */
    public final void setDbPass(String dbPass) {
        this.dbPass = dbPass;
    }

    /**
     * @return the dbUser
     */
    public final String getDbUser() {
        return dbUser;
    }

    /**
     * @param dbUser
     *            the dbUser to set
     */
    public final void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    /**
     * @return the host
     */
    public final String getHost() {
        return host;
    }

    /**
     * @param host
     *            the host to set
     */
    public final void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the port
     */
    public final Integer getPort() {
        return port;
    }

    /**
     * @param port
     *            the port to set
     */
    public final void setPort(Integer port) {
        this.port = port;
    }

}