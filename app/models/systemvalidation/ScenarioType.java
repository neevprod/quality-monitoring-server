package models.systemvalidation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the scenario_type database table.
 * 
 */
@Entity
@Table(name = "scenario_type")
@NamedQuery(name = "ScenarioType.findAll", query = "SELECT s FROM ScenarioType s")
public class ScenarioType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "scenario_type_id", unique = true, nullable = false)
    private Integer scenarioTypeId;

    @Column(name = "scenario_type", nullable = false, length = 50)
    private String scenarioType;

    public ScenarioType() {
    }

    public Integer getScenarioTypeId() {
        return this.scenarioTypeId;
    }

    public void setScenarioTypeId(Integer scenarioTypeId) {
        this.scenarioTypeId = scenarioTypeId;
    }

    public String getScenarioType() {
        return this.scenarioType;
    }

    public void setScenarioType(String scenarioType) {
        this.scenarioType = scenarioType;
    }
}