package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the div_job_exec_type database table.
 * 
 */
@Entity
@Table(name = "div_job_exec_type")
@NamedQuery(name = "DivJobExecType.findAll", query = "SELECT d FROM DivJobExecType d")
public class DivJobExecType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "div_job_exec_type_id", unique = true, nullable = false)
    private Integer divJobExecTypeId;

    @Column(name = "div_job_exec_type", nullable = false, length = 50)
    private String divJobExecType;

    // bi-directional many-to-one association to DivJobExec
    @OneToMany(mappedBy = "divJobExecType", fetch = FetchType.LAZY)
    private List<DivJobExec> divJobExecs;

    public DivJobExecType() {
    }

    public Integer getDivJobExecTypeId() {
        return this.divJobExecTypeId;
    }

    public void setDivJobExecTypeId(Integer divJobExecTypeId) {
        this.divJobExecTypeId = divJobExecTypeId;
    }

    public String getDivJobExecType() {
        return this.divJobExecType;
    }

    public void setDivJobExecType(String divJobExecType) {
        this.divJobExecType = divJobExecType;
    }

    public List<DivJobExec> getDivJobExecs() {
        return this.divJobExecs;
    }

    public void setDivJobExecs(List<DivJobExec> divJobExecs) {
        this.divJobExecs = divJobExecs;
    }

    public DivJobExec addDivJobExec(DivJobExec divJobExec) {
        getDivJobExecs().add(divJobExec);
        divJobExec.setDivJobExecType(this);

        return divJobExec;
    }

    public DivJobExec removeDivJobExec(DivJobExec divJobExec) {
        getDivJobExecs().remove(divJobExec);
        divJobExec.setDivJobExecType(null);

        return divJobExec;
    }

}