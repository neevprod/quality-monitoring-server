package models.systemvalidation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import util.EncryptorDecryptor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the test_session database table.
 * 
 */
@Entity
@Table(name = "test_session")
@NamedQuery(name = "TestSession.findAll", query = "SELECT t FROM TestSession t")
public class TestSession implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "test_session_id", unique = true, nullable = false)
    private String testSessionId;

    @Temporal(TemporalType.DATE)
    @Column(name = "schedule_end_date")
    private Date scheduleEndDate;

    @Column(name = "schedule_end_time")
    private Time scheduleEndTime;

    @Temporal(TemporalType.DATE)
    @Column(name = "schedule_start_date")
    private Date scheduleStartDate;

    @Column(name = "schedule_start_time")
    private Time scheduleStartTime;

    @Column(name = "scope_code", nullable = false, length = 50)
    private String scopeCode;

    @Column(name = "session_available_friday")
    private Boolean sessionAvailableFriday;

    @Column(name = "session_available_monday")
    private Boolean sessionAvailableMonday;

    @Column(name = "session_available_saturday")
    private Boolean sessionAvailableSaturday;

    @Column(name = "session_available_sunday")
    private Boolean sessionAvailableSunday;

    @Column(name = "session_available_thursday")
    private Boolean sessionAvailableThursday;

    @Column(name = "session_available_tuesday")
    private Boolean sessionAvailableTuesday;

    @Column(name = "session_available_wednesday")
    private Boolean sessionAvailableWednesday;

    @Column(name = "session_name", length = 50)
    private String sessionName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "session_scheduled_start_date")
    private Date sessionScheduledStartDate;

    @Column(name = "session_seal_codes", length = 255)
    private String sessionSealCodes;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "session_start_date")
    private Date sessionStartDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "session_stop_date")
    private Date sessionStopDate;

    @Column(name = "testnav_application_url", nullable = false, length = 500)
    private String testnavApplicationUrl;

    @JsonIgnore
    @Column(name = "testnav_client_identifier", length = 200)
    private String testnavClientIdentifier;

    @JsonIgnore
    @Column(name = "testnav_client_secret", length = 200)
    @Convert(converter = EncryptorDecryptor.class)
    private String testnavClientSecret;

    @Column(name = "testnav_customer_code", nullable = false, length = 200)
    private String testnavCustomerCode;

    @Column(name = "testnav_version", nullable = false, length = 50)
    private String testnavVersion;

    @Column(name = "testnav_webservice_url", nullable = false, length = 500)
    private String testnavWebserviceUrl;

    @Column(name = "pan_test_session_id", nullable = false, length = 16)
    private String panTestSessionId;

    // bi-directional many-to-one association to Environment
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "environment_id", nullable = false)
    private Environment environment;

    // bi-directional many-to-one association to StudentScenario
    @OneToMany(mappedBy = "testSession", fetch = FetchType.LAZY)
    private List<StudentScenario> studentScenarios;

    public TestSession() {
        studentScenarios = new ArrayList<>();
    }

    public String getTestSessionId() {
        return this.testSessionId;
    }

    public void setTestSessionId(String testSessionId) {
        this.testSessionId = testSessionId;
    }

    public Date getScheduleEndDate() {
        return this.scheduleEndDate;
    }

    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    public Time getScheduleEndTime() {
        return this.scheduleEndTime;
    }

    public void setScheduleEndTime(Time scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public Date getScheduleStartDate() {
        return this.scheduleStartDate;
    }

    public void setScheduleStartDate(Date scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public Time getScheduleStartTime() {
        return this.scheduleStartTime;
    }

    public void setScheduleStartTime(Time scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public String getScopeCode() {
        return this.scopeCode;
    }

    public void setScopeCode(String scopeCode) {
        this.scopeCode = scopeCode;
    }

    public Boolean getSessionAvailableFriday() {
        return this.sessionAvailableFriday;
    }

    public void setSessionAvailableFriday(Boolean sessionAvailableFriday) {
        this.sessionAvailableFriday = sessionAvailableFriday;
    }

    public Boolean getSessionAvailableMonday() {
        return this.sessionAvailableMonday;
    }

    public void setSessionAvailableMonday(Boolean sessionAvailableMonday) {
        this.sessionAvailableMonday = sessionAvailableMonday;
    }

    public Boolean getSessionAvailableSaturday() {
        return this.sessionAvailableSaturday;
    }

    public void setSessionAvailableSaturday(Boolean sessionAvailableSaturday) {
        this.sessionAvailableSaturday = sessionAvailableSaturday;
    }

    public Boolean getSessionAvailableSunday() {
        return this.sessionAvailableSunday;
    }

    public void setSessionAvailableSunday(Boolean sessionAvailableSunday) {
        this.sessionAvailableSunday = sessionAvailableSunday;
    }

    public Boolean getSessionAvailableThursday() {
        return this.sessionAvailableThursday;
    }

    public void setSessionAvailableThursday(Boolean sessionAvailableThursday) {
        this.sessionAvailableThursday = sessionAvailableThursday;
    }

    public Boolean getSessionAvailableTuesday() {
        return this.sessionAvailableTuesday;
    }

    public void setSessionAvailableTuesday(Boolean sessionAvailableTuesday) {
        this.sessionAvailableTuesday = sessionAvailableTuesday;
    }

    public Boolean getSessionAvailableWednesday() {
        return this.sessionAvailableWednesday;
    }

    public void setSessionAvailableWednesday(Boolean sessionAvailableWednesday) {
        this.sessionAvailableWednesday = sessionAvailableWednesday;
    }

    public String getSessionName() {
        return this.sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public Date getSessionScheduledStartDate() {
        return this.sessionScheduledStartDate;
    }

    public void setSessionScheduledStartDate(Date sessionScheduledStartDate) {
        this.sessionScheduledStartDate = sessionScheduledStartDate;
    }

    public String getSessionSealCodes() {
        return this.sessionSealCodes;
    }

    public void setSessionSealCodes(String sessionSealCodes) {
        this.sessionSealCodes = sessionSealCodes;
    }

    public Date getSessionStartDate() {
        return this.sessionStartDate;
    }

    public void setSessionStartDate(Date sessionStartDate) {
        this.sessionStartDate = sessionStartDate;
    }

    public Date getSessionStopDate() {
        return this.sessionStopDate;
    }

    public void setSessionStopDate(Date sessionStopDate) {
        this.sessionStopDate = sessionStopDate;
    }

    public String getTestnavApplicationUrl() {
        return this.testnavApplicationUrl;
    }

    public void setTestnavApplicationUrl(String testnavApplicationUrl) {
        this.testnavApplicationUrl = testnavApplicationUrl;
    }

    public String getTestnavClientIdentifier() {
        return this.testnavClientIdentifier;
    }

    public void setTestnavClientIdentifier(String testnavClientIdentifier) {
        this.testnavClientIdentifier = testnavClientIdentifier;
    }

    public String getTestnavClientSecret() {
        return this.testnavClientSecret;
    }

    public void setTestnavClientSecret(String testnavClientSecret) {
        this.testnavClientSecret = testnavClientSecret;
    }

    public String getTestnavCustomerCode() {
        return this.testnavCustomerCode;
    }

    public void setTestnavCustomerCode(String testnavCustomerCode) {
        this.testnavCustomerCode = testnavCustomerCode;
    }

    public String getTestnavVersion() {
        return this.testnavVersion;
    }

    public void setTestnavVersion(String testnavVersion) {
        this.testnavVersion = testnavVersion;
    }

    public String getTestnavWebserviceUrl() {
        return this.testnavWebserviceUrl;
    }

    public void setTestnavWebserviceUrl(String testnavWebserviceUrl) {
        this.testnavWebserviceUrl = testnavWebserviceUrl;
    }

    public String getPanTestSessionId() {
        return this.panTestSessionId;
    }

    public void setPanTestSessionId(String panTestSessionId) {
        this.panTestSessionId = panTestSessionId;
    }

    public Environment getEnvironment() {
        return this.environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public List<StudentScenario> getStudentScenarios() {
        return this.studentScenarios;
    }

    public void setStudentScenarios(List<StudentScenario> studentScenarios) {
        this.studentScenarios = studentScenarios;
    }

    public StudentScenario addStudentScenario(StudentScenario studentScenario) {
        getStudentScenarios().add(studentScenario);
        studentScenario.setTestSession(this);

        return studentScenario;
    }

    public StudentScenario removeStudentScenario(StudentScenario studentScenario) {
        getStudentScenarios().remove(studentScenario);
        studentScenario.setTestSession(null);

        return studentScenario;
    }

}