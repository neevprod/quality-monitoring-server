package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the div_job_exec_status database table.
 * 
 */
@Entity
@Table(name = "div_job_exec_status")
@NamedQuery(name = "DivJobExecStatus.findAll", query = "SELECT d FROM DivJobExecStatus d")
public class DivJobExecStatus implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "div_job_exec_status_id", unique = true, nullable = false)
    private Integer divJobExecStatusId;

    @Column(nullable = false, length = 50)
    private String status;

    public DivJobExecStatus() {
    }

    public Integer getDivJobExecStatusId() {
        return this.divJobExecStatusId;
    }

    public void setDivJobExecStatusId(Integer divJobExecStatusId) {
        this.divJobExecStatusId = divJobExecStatusId;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}