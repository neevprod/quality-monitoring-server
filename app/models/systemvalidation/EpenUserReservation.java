package models.systemvalidation;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by SELIGE on 6/6/2017.
 */
@Entity
@Table(name = "epen_user_reservation")
public class EpenUserReservation implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "locked", nullable = false)
    private Boolean locked;

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @Basic
    @Column(name = "locked_timestamp")
    private Timestamp lockedTimestamp;

    public Timestamp getLockedTimestamp() {
        return lockedTimestamp;
    }

    public void setLockedTimestamp(Timestamp lockedTimestamp) {
        this.lockedTimestamp = lockedTimestamp;
    }
}