package models.systemvalidation;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the student_scenario database table.
 */
@Entity
@Table(name = "student_scenario")
@NamedQuery(name = "StudentScenario.findAll", query = "SELECT s FROM StudentScenario s")
public class StudentScenario implements Serializable {
    private static final long serialVersionUID = 1875396455711056129L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_scenario_id", unique = true, nullable = false)
    private Long studentScenarioId;

    @Column(name = "student_code", length = 50)
    private String studentCode;

    @Column(name = "first_name", length = 100)
    private String firstName;

    @Column(name = "last_name", length = 100)
    private String lastName;

    @Column(name = "student_group_name", length = 100)
    private String studentGroupName;

    @JsonIgnore
    @Column(name = "testnav_login_name", nullable = false, length = 90)
    private String testnavLoginName;

    @JsonIgnore
    @Column(name = "testnav_login_password", nullable = false, length = 40)
    private String testnavLoginPassword;

    @Column(name = "uuid", nullable = false, length = 80)
    private String uuid;

    @Column(name = "student_test_status", length = 50)
    private String studentTestStatus;

    @Column(name = "session_assign_status", length = 50)
    private String sessionAssignStatus;

    // bi-directional many-to-one association to DivJobExec
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "div_job_exec_id", nullable = false)
    private DivJobExec divJobExec;

    // bi-directional many-to-one association to Scenario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "testnav_scenario_id")
    private Scenario testnavScenario;

    // bi-directional many-to-one association to Scenario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "epen_scenario_id")
    private Scenario epenScenario;

    // bi-directional many-to-one association to State
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "last_completed_state_id", insertable = false, updatable = false)
    private State state;

    @Column(name = "last_completed_state_id", nullable = true)
    private Integer lastCompletedStateId;

    // bi-directional many-to-one association to StudentScenarioStatus
    @ManyToOne
    @JoinColumn(name = "student_scenario_status_id", insertable = false, updatable = false)
    private StudentScenarioStatus studentScenarioStatus;

    @Column(name = "student_scenario_status_id", nullable = false, columnDefinition = "TINYINT")
    private Integer studentScenarioStatusId;

    @Column(name = "student_test_session_assign_id", nullable = true)
    private Integer studentTestSessionAssignId;

    // bi-directional many-to-one association to StudentScenarioResult
    @OneToMany(mappedBy = "studentScenario", fetch = FetchType.LAZY)
    private List<StudentScenarioResult> studentScenarioResults;

    @Column(name = "export_type", length = 20)
    private String exportType;

    // bi-directional many-to-one association to TestSession
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "test_session_id", nullable = false)
    private TestSession testSession;

    // bi-directional many-to-one association to DbConnection
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wf_db_connection_id")
    private DbConnection wfDbConnection;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "battery_student_scenario_id")
    private BatteryStudentScenario batteryStudentScenario;

    public StudentScenario() {
    }

    public Long getStudentScenarioId() {
        return studentScenarioId;
    }

    public void setStudentScenarioId(Long studentScenarioId) {
        this.studentScenarioId = studentScenarioId;
    }

    public String getStudentCode() {
        return this.studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getStudentGroupName() {
        return this.studentGroupName;
    }

    public void setStudentGroupName(String studentGroupName) {
        this.studentGroupName = studentGroupName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTestnavLoginName() {
        return this.testnavLoginName;
    }

    public void setTestnavLoginName(String testnavLoginName) {
        this.testnavLoginName = testnavLoginName;
    }

    public String getTestnavLoginPassword() {
        return this.testnavLoginPassword;
    }

    public void setTestnavLoginPassword(String testnavLoginPassword) {
        this.testnavLoginPassword = testnavLoginPassword;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public DivJobExec getDivJobExec() {
        return this.divJobExec;
    }

    public void setDivJobExec(DivJobExec divJobExec) {
        this.divJobExec = divJobExec;
    }

    public Scenario getTestnavScenario() {
        return this.testnavScenario;
    }

    public void setTestnavScenario(Scenario testnavScenario) {
        this.testnavScenario = testnavScenario;
    }

    public Scenario getEpenScenario() {
        return this.epenScenario;
    }

    public void setEpenScenario(Scenario epenScenario) {
        this.epenScenario = epenScenario;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getSessionAssignStatus() {
        return this.sessionAssignStatus;
    }

    public void setSessionAssignStatus(String sessionAssignStatus) {
        this.sessionAssignStatus = sessionAssignStatus;
    }

    public StudentScenarioStatus getStudentScenarioStatus() {
        return this.studentScenarioStatus;
    }

    public void setStudentScenarioStatus(StudentScenarioStatus studentScenarioStatus) {
        this.studentScenarioStatus = studentScenarioStatus;
    }

    public List<StudentScenarioResult> getStudentScenarioResults() {
        return this.studentScenarioResults;
    }

    public void setStudentScenarioResults(List<StudentScenarioResult> studentScenarioResults) {
        this.studentScenarioResults = studentScenarioResults;
    }

    public StudentScenarioResult addStudentScenarioResult(StudentScenarioResult studentScenarioResult) {
        getStudentScenarioResults().add(studentScenarioResult);
        studentScenarioResult.setStudentScenario(this);

        return studentScenarioResult;
    }

    public StudentScenarioResult removeStudentScenarioResult(StudentScenarioResult studentScenarioResult) {
        getStudentScenarioResults().remove(studentScenarioResult);
        studentScenarioResult.setStudentScenario(null);

        return studentScenarioResult;
    }

    public Integer getStudentScenarioStatusId() {
        return studentScenarioStatusId;
    }

    public void setStudentScenarioStatusId(Integer studentScenarioStatusId) {
        this.studentScenarioStatusId = studentScenarioStatusId;
    }

    public Integer getStudentTestSessionAssignId() {
        return studentTestSessionAssignId;
    }

    public void setStudentTestSessionAssignId(Integer studentTestSessionAssignId) {
        this.studentTestSessionAssignId = studentTestSessionAssignId;
    }

    public Integer getLastCompletedStateId() {
        return lastCompletedStateId;
    }

    public void setLastCompletedStateId(Integer lastCompletedStateId) {
        this.lastCompletedStateId = lastCompletedStateId;
    }

    public String getExportType() {
        return this.exportType;
    }

    public void setExportType(String exportType) {
        this.exportType = exportType;
    }

    public TestSession getTestSession() {
        return this.testSession;
    }

    public void setTestSession(TestSession testSession) {
        this.testSession = testSession;
    }

    public String getStudentTestStatus() {
        return this.studentTestStatus;
    }

    public void setStudentTestStatus(String studentTestStatus) {
        this.studentTestStatus = studentTestStatus;
    }

    public DbConnection getWfDbConnection() {
        return this.wfDbConnection;
    }

    public void setWfDbConnection(DbConnection wfDbConnection) {
        this.wfDbConnection = wfDbConnection;
    }

    public BatteryStudentScenario getBatteryStudentScenario() {
        return batteryStudentScenario;
    }

    public void setBatteryStudentScenario(BatteryStudentScenario batteryStudentScenario) {
        this.batteryStudentScenario = batteryStudentScenario;
    }
}