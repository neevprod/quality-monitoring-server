package models.systemvalidation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import util.EncryptorDecryptor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the db_connection database table.
 * 
 */
@Entity
@Table(name = "db_connection")
@NamedQuery(name = "DbConnection.findAll", query = "SELECT d FROM DbConnection d")
public class DbConnection implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "db_connection_id", unique = true, nullable = false)
    private Integer dbConnectionId;

    @Column(name = "db_name", nullable = false, length = 50)
    private String dbName;

    @Column(name = "db_pass", nullable = false, length = 50)
    @Convert(converter = EncryptorDecryptor.class)
    @JsonIgnore
    private String dbPass;

    @Column(name = "db_user", nullable = false, length = 50)
    @JsonIgnore
    private String dbUser;

    @Column(nullable = false, length = 140)
    private String host;

    @Column(nullable = false)
    private Integer port;

    public DbConnection() {
    }

    public Integer getDbConnectionId() {
        return this.dbConnectionId;
    }

    public void setDbConnectionId(Integer dbConnectionId) {
        this.dbConnectionId = dbConnectionId;
    }

    public String getDbName() {
        return this.dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbPass() {
        return this.dbPass;
    }

    public void setDbPass(String dbPass) {
        this.dbPass = dbPass;
    }

    public String getDbUser() {
        return this.dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}