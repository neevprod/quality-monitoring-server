package models.systemvalidation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the result_comment_category database table.
 * 
 */
@Entity
@Table(name = "student_scenario_result_comment_category")
@NamedQuery(name = "StudentScenarioResultCommentCategory.findAll", query = "SELECT f FROM StudentScenarioResultCommentCategory f")
public class StudentScenarioResultCommentCategory implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_scenario_result_comment_category_id", unique = true, nullable = false)
    private Integer studentScenarioResultCommentCategoryId;

    @Column(length = 256)
    private String description;

    @Column(nullable = false, length = 50)
    private String name;

    public StudentScenarioResultCommentCategory() {
    }

    public Integer getStudentScenarioResultCommentCategoryId() {
        return this.studentScenarioResultCommentCategoryId;
    }

    public void setStudentScenarioResultCommentCategoryId(Integer studentScenarioResultCommentCategoryId) {
        this.studentScenarioResultCommentCategoryId = studentScenarioResultCommentCategoryId;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}