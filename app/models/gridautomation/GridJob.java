package models.gridautomation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import models.User;
import models.systemvalidation.DivJobExecStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The persistent class for the grid_jobs database table.
 */
@Entity
@Table(name = "grid_job")

public class GridJob implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", unique = true, nullable = false)
    private Long gridJobId;

    @Column(name = "run_type",  length = 20)
    private String runType;

    @Column(name = "grid_contract_id", nullable = false, length = 10)
    @JsonIgnore
    private Integer gridContractId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grid_contract_id", insertable = false, updatable = false)
    private GridContract gridContract;

    @Column(name = "grid_program_id", nullable = false, length = 10)
    private Integer gridProgramId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grid_program_id", insertable = false, updatable = false)
    private GridProgram gridProgram;

    @Column(name = "grid_admin_id", nullable = false, length = 10)
    private Integer gridAdminId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grid_admin_id", insertable = false, updatable = false)
    private GridAdmin gridAdmin;

    @Column(name = "grid_season_id", nullable = false, length = 10)
    private Integer gridSeasonId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grid_season_id", insertable = false, updatable = false)
    private GridSeason gridSeason;
    
    @Column(name = "file_name", nullable = false, length = 30)
    private String fileName;

	@Column(name = "pgmssn",  length = 6)
    private String pgmssn;

    @Column(name = "doc_code",  length = 6)
    private String docCode;

    @Column(name = "state_code",  length = 2)
    private String stateCode;

    @Column(name = "litho_start", nullable = true)
    private String lithoStart;

    @Column(name = "delivery_date", nullable = false)
    private Timestamp deliveryDate;

    @Column(name = "qual_group", nullable = false, length = 3)
    private String qualGroup;

    @Column(name = "comments",  length = 200)
    private String comments;
    
    @Column(name = "is6by8",  length = 1)
    private Boolean is6by8;
    
    @Column(name = "autorelease",  length = 1)
    private Boolean autoRelease;

    @Column(name = "user_id", nullable = false)
    @JsonIgnore
    private Long userId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User submitUser;

    @Column(name = "submit_time_stamp",  insertable = false, updatable = false)
    private Timestamp submitTimestamp;

    @Column(name = "complete_time_stamp", insertable = false)
    private Timestamp completeTimeStamp;

    @Column(name = "status_id", nullable = false)
    @JsonIgnore
    private Integer jobStatusId;
    // bi-directional many-to-one association to DivJobExecStatus
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id", insertable = false, updatable = false)
    private DivJobExecStatus jobStatus;

    public Long getGridJobId() {
        return gridJobId;
    }

    public void setGridJobId(Long gridJobId) {
        this.gridJobId = gridJobId;
    }

    public String getRunType() {
        return runType;
    }

    public void setRunType(String runType) {
        this.runType = runType;
    }

    public Integer getGridContractId() {
        return gridContractId;
    }

    public void setGridContractId(Integer gridContractId) {
        this.gridContractId = gridContractId;
    }

    public GridContract getGridContract() {
        return gridContract;
    }

    public void setGridContract(GridContract gridContract) {
        this.gridContract = gridContract;
    }

    public Integer getGridProgramId() {
        return gridProgramId;
    }

    public void setGridProgramId(Integer gridProgramId) {
        this.gridProgramId = gridProgramId;
    }

    public GridProgram getGridProgram() {
        return gridProgram;
    }

    public void setGridProgram(GridProgram gridProgram) {
        this.gridProgram = gridProgram;
    }

    public Integer getGridAdminId() {
        return gridAdminId;
    }

    public void setGridAdminId(Integer gridAdminId) {
        this.gridAdminId = gridAdminId;
    }

    public GridAdmin getGridAdmin() {
        return gridAdmin;
    }

    public void setGridAdmin(GridAdmin gridAdmin) {
        this.gridAdmin = gridAdmin;
    }

    public Integer getGridSeasonId() {
        return gridSeasonId;
    }

    public void setGridSeasonId(Integer gridSeasonId) {
        this.gridSeasonId = gridSeasonId;
    }

    public GridSeason getGridSeason() {
        return gridSeason;
    }

    public void setGridSeason(GridSeason gridSeason) {
        this.gridSeason = gridSeason;
    }

    public String getPgmssn() {
        return pgmssn;
    }

    public void setPgmssn(String pgmssn) {
        this.pgmssn = pgmssn;
    }

    public String getDocCode() {
        return docCode;
    }

    public void setDocCode(String docCode) {
        this.docCode = docCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getLithoStart() {
        return lithoStart;
    }

    public void setLithoStart(String lithoStart) {
        this.lithoStart = lithoStart;
    }

    public Timestamp getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Timestamp deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getQualGroup() {
        return qualGroup;
    }

    public void setQualGroup(String qualGroup) {
        this.qualGroup = qualGroup;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    
    public Boolean getAutoRelease() {
        return autoRelease;
    }

    public void setAutoRelease(Boolean autoRelease) {
        this.autoRelease = autoRelease;
    }
    
    public Boolean getIs6By8() {
        return is6by8;
    }

    public void setIs6By8(Boolean is6by8) {
        this.is6by8 = is6by8;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public User getSubmitUser() {
        return submitUser;
    }

    public void setSubmitUser(User submitUser) {
        this.submitUser = submitUser;
    }

    public Timestamp getSubmitTimestamp() {
        return submitTimestamp;
    }

    public void setSubmitTimestamp(Timestamp submitTimestamp) {
        this.submitTimestamp = submitTimestamp;
    }

    public Timestamp getCompleteTimeStamp() {
        return completeTimeStamp;
    }

    public void setCompleteTimeStamp(Timestamp completeTimeStamp) {
        this.completeTimeStamp = completeTimeStamp;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public DivJobExecStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(DivJobExecStatus jobStatus) {
        this.jobStatus = jobStatus;
    }
    
    public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}