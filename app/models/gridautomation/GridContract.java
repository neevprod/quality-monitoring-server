package models.gridautomation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the grid_contracts database table.
 * 
 */
@Entity
@Table(name = "grid_contracts")

public class GridContract implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "grid_contract_id", unique = true, nullable = false)
    private Integer gridContractId;

    @Column(name = "contract_abb", nullable = false, length = 2)
    private String contractAbb;

    @Column(name = "contract_name", nullable = false, length = 30)
    private String contractName;

    @Column(name = "active", nullable = false)
    private Boolean active;

    public Boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getGridContractId() {
        return gridContractId;
    }

    public void setGridContractId(int gridContractId) {
        this.gridContractId = gridContractId;
    }

    public String getContractAbb() {
        return contractAbb;
    }

    public void setContractAbb(String contractAbb) {
        this.contractAbb = contractAbb;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }
}