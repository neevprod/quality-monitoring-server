package models.gridautomation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the grid_programs database table.
 * 
 */
@Entity
@Table(name = "grid_programs")

public class GridProgram implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "program_id", unique = true, nullable = false)
    private Integer programId;

    @Column(name = "program_abb", nullable = false, length = 4)
    private String programAbb;

    @Column(name = "program_name", nullable = false, length = 35)
    private String programName;

    @Column(name = "active", nullable = false)
    private Boolean active;


    public Boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getProgramAbb() {
        return programAbb;
    }

    public void setProgramAbb(String programAbb) {
        this.programAbb = programAbb;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }
}