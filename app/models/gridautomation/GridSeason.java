package models.gridautomation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the grid_seasons database table.
 * 
 */
@Entity
@Table(name = "grid_seasons")

public class GridSeason implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "season_id", unique = true, nullable = false)
    private Integer seasonId;

    @Column(name = "season_abb", nullable = false, length = 3)
    private String seasonAbb;

    @Column(name = "season_name", nullable = false, length = 20)
    private String seasonName;

    @Column(name = "active", nullable = false)
    private Boolean active;


    public Boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(int seasonId) {
        this.seasonId = seasonId;
    }

    public String getSeasonAbb() {
        return seasonAbb;
    }

    public void setSeasonAbb(String seasonAbb) {
        this.seasonAbb = seasonAbb;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }
}