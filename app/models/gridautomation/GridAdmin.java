package models.gridautomation;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistent class for the grid_admins database table.
 * 
 */
@Entity
@Table(name = "grid_admins")

public class GridAdmin implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "admin_id", unique = true, nullable = false)
    private Integer adminId;

    @Column(name = "admin_abb", nullable = false, length = 3)
    private String adminAbb;

    @Column(name = "admin_name", nullable = false, length = 40)
    private String adminName;

    @Column(name = "active", nullable = false)
    private Boolean active;

    public Boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getAdminAbb() {
        return adminAbb;
    }

    public void setAdminAbb(String adminAbb) {
        this.adminAbb = adminAbb;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }
}