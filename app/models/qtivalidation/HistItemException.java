package models.qtivalidation;

import javax.persistence.*;

@Entity
@Table(name = "hist_item_exception")
public class HistItemException {
    private Integer histItemExceptionId;
    private Long itemId;
    private String itemIdentifier;
    private String exception;
    private HistTenant histTenant;

    @Id
    @GeneratedValue
    @Column(name = "hist_item_exception_id")
    public Integer getHistItemExceptionId() {
        return histItemExceptionId;
    }

    public void setHistItemExceptionId(Integer histItemExceptionId) {
        this.histItemExceptionId = histItemExceptionId;
    }

    @Basic
    @Column(name = "item_id")
    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @Basic
    @Column(name = "item_identifier")
    public String getItemIdentifier() {
        return itemIdentifier;
    }

    public void setItemIdentifier(String itemIdentifier) {
        this.itemIdentifier = itemIdentifier;
    }

    @Basic
    @Column(name = "exception")
    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hist_tenant_id", referencedColumnName = "hist_tenant_id", nullable = false)
    public HistTenant getHistTenant() {
        return histTenant;
    }

    public void setHistTenant(HistTenant histTenant) {
        this.histTenant = histTenant;
    }

}
