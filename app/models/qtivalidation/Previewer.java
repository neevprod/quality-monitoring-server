package models.qtivalidation;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Previewer {
    private Integer previewerId;
    private String name;
    private String url;
    private String apiUrl;
    private String protocol;
    private String apiProtocol;
    @Transient private Timestamp lastUpdate;

    @Id
    @GeneratedValue
    @Column(name = "previewer_id")
    public Integer getPreviewerId() {
        return previewerId;
    }

    public void setPreviewerId(Integer previewerId) {
        this.previewerId = previewerId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "api_url")
    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    @Basic
    @Column(name = "protocol")
    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    @Basic
    @Column(name = "api_protocol")
    public String getApiProtocol() {
        return apiProtocol;
    }

    public void setApiProtocol(String apiProtocol) {
        this.apiProtocol = apiProtocol;
    }

    @Basic
    @Column(name = "last_update")
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
