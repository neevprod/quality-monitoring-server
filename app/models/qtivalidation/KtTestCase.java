package models.qtivalidation;

import javax.persistence.*;

@Entity
@Table(name = "kt_test_case")
public class KtTestCase {
    private Integer ktTestCaseId;
    private String ktId;
    private String ktCandidateResponse;
    private String ktResponseId;
    private String ktSavedScore;
    private String ktSavedScoreState;
    private Boolean ktIncludeInBuild;
    private String itemIdentifier;
    private String responseIdentifier;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "kt_test_case_id")
    public Integer getKtTestCaseId() {
        return ktTestCaseId;
    }

    public void setKtTestCaseId(Integer ktTestCaseId) {
        this.ktTestCaseId = ktTestCaseId;
    }

    @Column(name = "kt_id")
    public String getKtId() {
        return ktId;
    }

    public void setKtId(String ktId) {
        this.ktId = ktId;
    }

    @Column(name = "kt_candidate_response")
    public String getKtCandidateResponse() {
        return ktCandidateResponse;
    }

    public void setKtCandidateResponse(String ktCandidateResponse) {
        this.ktCandidateResponse = ktCandidateResponse;
    }

    @Column(name = "kt_response_id")
    public String getKtResponseId() {
        return ktResponseId;
    }

    public void setKtResponseId(String ktResponseId) {
        this.ktResponseId = ktResponseId;
    }

    @Column(name = "kt_saved_score")
    public String getKtSavedScore() {
        return ktSavedScore;
    }

    public void setKtSavedScore(String ktSavedScore) {
        this.ktSavedScore = ktSavedScore;
    }

    @Column(name = "kt_saved_score_state")
    public String getKtSavedScoreState() {
        return ktSavedScoreState;
    }

    public void setKtSavedScoreState(String ktSavedScoreState) {
        this.ktSavedScoreState = ktSavedScoreState;
    }

    @Column(name = "kt_include_in_build")
    public Boolean getKtIncludeInBuild() {
        return ktIncludeInBuild;
    }

    public void setKtIncludeInBuild(Boolean ktIncludeInBuild) {
        this.ktIncludeInBuild = ktIncludeInBuild;
    }

    @Column(name = "item_identifier")
    public String getItemIdentifier() {
        return itemIdentifier;
    }

    public void setItemIdentifier(String itemIdentifier) {
        this.itemIdentifier = itemIdentifier;
    }

    @Column(name = "response_identifier")
    public String getResponseIdentifier() {
        return responseIdentifier;
    }

    public void setResponseIdentifier(String responseIdentifier) {
        this.responseIdentifier = responseIdentifier;
    }

    public static KtTestCase createKtTestCaseObject(String ktId,
                                                    String ktCandidateResponse,
                                                    String ktResponseId,
                                                    String ktSavedScore,
                                                    String ktSavedScoreState,
                                                    Boolean ktIncludeInBuild,
                                                    String itemIdentifier,
                                                    String responseIdentifier) {

        KtTestCase ktTestcase = new KtTestCase();
        ktTestcase.setKtCandidateResponse(ktCandidateResponse);
        ktTestcase.setResponseIdentifier(responseIdentifier);
        ktTestcase.setKtId(ktId);
        ktTestcase.setKtIncludeInBuild(ktIncludeInBuild);
        ktTestcase.setItemIdentifier(itemIdentifier);
        ktTestcase.setKtResponseId(ktResponseId);
        ktTestcase.setKtSavedScore(ktSavedScore);
        ktTestcase.setKtSavedScoreState(ktSavedScoreState);
        return ktTestcase;
    }
}
