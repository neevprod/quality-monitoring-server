package models.qtivalidation;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "hist_tenant")
public class HistTenant {
    private Integer histTenantId;
    private Timestamp execStart;
    private Timestamp execStop;
    private Boolean execCompleted;
    private Integer itemsValidated;
    private Integer itemsWithFailedTcs;
    private Integer itemsWithPendingTcs;
    private Integer deactivatedItems;
    private Integer itemExceptions;
    private HistExec histExec;
    private PreviewerTenant previewerTenant;

    @Id
    @GeneratedValue
    @Column(name = "hist_tenant_id")
    public Integer getHistTenantId() {
        return histTenantId;
    }

    public void setHistTenantId(Integer histTenantId) {
        this.histTenantId = histTenantId;
    }

    @Basic
    @Column(name = "exec_start")
    public Timestamp getExecStart() {
        return execStart;
    }

    public void setExecStart(Timestamp execStart) {
        this.execStart = execStart;
    }

    @Basic
    @Column(name = "exec_stop")
    public Timestamp getExecStop() {
        return execStop;
    }

    public void setExecStop(Timestamp execStop) {
        this.execStop = execStop;
    }

    @Basic
    @Column(name = "exec_completed")
    public Boolean getExecCompleted() {
        return execCompleted;
    }

    public void setExecCompleted(Boolean execCompleted) {
        this.execCompleted = execCompleted;
    }

    @Basic
    @Column(name = "items_validated")
    public Integer getItemsValidated() {
        return itemsValidated;
    }

    public void setItemsValidated(Integer itemsValidated) {
        this.itemsValidated = itemsValidated;
    }

    @Basic
    @Column(name = "items_with_failed_tcs")
    public Integer getItemsWithFailedTcs() {
        return itemsWithFailedTcs;
    }

    public void setItemsWithFailedTcs(Integer itemsWithFailedTcs) {
        this.itemsWithFailedTcs = itemsWithFailedTcs;
    }

    @Basic
    @Column(name = "items_with_pending_tcs")
    public Integer getItemsWithPendingTcs() {
        return itemsWithPendingTcs;
    }

    public void setItemsWithPendingTcs(Integer itemsWithPendingTcs) {
        this.itemsWithPendingTcs = itemsWithPendingTcs;
    }

    @Basic
    @Column(name = "deactivated_items")
    public Integer getDeactivatedItems() {
        return deactivatedItems;
    }

    public void setDeactivatedItems(Integer deactivatedItems) {
        this.deactivatedItems = deactivatedItems;
    }

    @Basic
    @Column(name = "item_exceptions")
    public Integer getItemExceptions() {
        return itemExceptions;
    }

    public void setItemExceptions(Integer itemExceptions) {
        this.itemExceptions = itemExceptions;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hist_exec_id", referencedColumnName = "hist_exec_id", nullable = false)
    public HistExec getHistExec() {
        return histExec;
    }

    public void setHistExec(HistExec histExec) {
        this.histExec = histExec;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previewer_tenant_id", referencedColumnName = "previewer_tenant_id", nullable = false)
    public PreviewerTenant getPreviewerTenant() {
        return previewerTenant;
    }

    public void setPreviewerTenant(PreviewerTenant previewerTenant) {
        this.previewerTenant = previewerTenant;
    }

}
