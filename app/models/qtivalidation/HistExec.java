package models.qtivalidation;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "hist_exec")
public class HistExec {
    private Integer histExecId;
    private Timestamp execStart;
    private Timestamp execStop;
    private Boolean execCompleted;
    private Boolean tenantLevelExec;
    private Previewer previewer;

    @Id
    @GeneratedValue
    @Column(name = "hist_exec_id")
    public Integer getHistExecId() {
        return histExecId;
    }

    public void setHistExecId(Integer histExecId) {
        this.histExecId = histExecId;
    }

    @Basic
    @Column(name = "exec_start")
    public Timestamp getExecStart() {
        return execStart;
    }

    public void setExecStart(Timestamp execStart) {
        this.execStart = execStart;
    }

    @Basic
    @Column(name = "exec_stop")
    public Timestamp getExecStop() {
        return execStop;
    }

    public void setExecStop(Timestamp execStop) {
        this.execStop = execStop;
    }

    @Basic
    @Column(name = "exec_completed")
    public Boolean getExecCompleted() {
        return execCompleted;
    }

    public void setExecCompleted(Boolean execCompleted) {
        this.execCompleted = execCompleted;
    }

    @Basic
    @Column(name = "tenant_level_exec")
    public Boolean getTenantLevelExec() {
        return tenantLevelExec;
    }

    public void setTenantLevelExec(Boolean tenantLevelExec) {
        this.tenantLevelExec = tenantLevelExec;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previewer_id", referencedColumnName = "previewer_id", nullable = false)
    public Previewer getPreviewer() {
        return previewer;
    }

    public void setPreviewer(Previewer previewer) {
        this.previewer = previewer;
    }

}
