package models.qtivalidation;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "previewer_tenant")
public class PreviewerTenant {
    private Long previewerTenantId;
    private Long tenantId;
    private String name;
    private boolean active;
    @Transient
    private Timestamp lastUpdate;
    private Previewer previewer;
    private Integer previewerId;
    private String subscribed;

    public PreviewerTenant() {
    }

    public PreviewerTenant(Previewer previewer, Long tenantId, String name, boolean active) {
        this.previewer = previewer;
        this.tenantId = tenantId;
        this.name = name;
        this.active = active;
    }

    @Id
    @GeneratedValue
    @Column(name = "previewer_tenant_id")
    public Long getPreviewerTenantId() {
        return previewerTenantId;
    }

    public void setPreviewerTenantId(Long previewerTenantId) {
        this.previewerTenantId = previewerTenantId;
    }

    @Basic
    @Column(name = "tenant_id")
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "active")
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "last_update")
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previewer_id", referencedColumnName = "previewer_id", nullable = false)
    public Previewer getPreviewer() {
        return previewer;
    }

    public void setPreviewer(Previewer previewer) {
        this.previewer = previewer;
    }

    @Basic
    @Column(name = "previewer_id", insertable = false, updatable = false)
    public Integer getPreviewerId() {
        return previewerId;
    }

    public void setPreviewerId(Integer previewerId) {
        this.previewerId = previewerId;
    }

    @Transient
    public String getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(String subscribed) {
        this.subscribed = subscribed;
    }
}
