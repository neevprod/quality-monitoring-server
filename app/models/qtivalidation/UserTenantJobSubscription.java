package models.qtivalidation;

import models.HumanUser;
import models.JobType;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "user_tenant_job_subscription")
public class UserTenantJobSubscription {
    private Long userTenantJobSubscriptionId;
    private Timestamp lastUpdate;
    private Long previewerTenantId;
    private PreviewerTenant previewerTenant;
    private HumanUser humanUser;
    private JobType jobType;

    public UserTenantJobSubscription() {
    }

    public UserTenantJobSubscription(HumanUser humanUser, long previewerTenantId, JobType jobType) {
        this.humanUser = humanUser;
        this.previewerTenantId = previewerTenantId;
        this.jobType = jobType;
    }

    @Id
    @GeneratedValue
    @Column(name = "user_tenant_job_subscription_id")
    public Long getUserTenantJobSubscriptionId() {
        return userTenantJobSubscriptionId;
    }

    public void setUserTenantJobSubscriptionId(Long userTenantJobSubscriptionId) {
        this.userTenantJobSubscriptionId = userTenantJobSubscriptionId;
    }

    @Basic
    @Column(name = "last_update", insertable = false, updatable = false)
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Basic
    @Column(name = "previewer_tenant_id")
    public Long getPreviewerTenantId() {
        return previewerTenantId;
    }

    public void setPreviewerTenantId(Long previewerTenantId) {
        this.previewerTenantId = previewerTenantId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previewer_tenant_id", insertable = false, updatable = false)
    public PreviewerTenant getPreviewerTenant() {
        return previewerTenant;
    }

    public void setPreviewerTenant(PreviewerTenant previewerTenant) {
        this.previewerTenant = previewerTenant;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "human_user_id", referencedColumnName = "human_user_id", nullable = false)
    public HumanUser getHumanUser() {
        return humanUser;
    }

    public void setHumanUser(HumanUser humanUser) {
        this.humanUser = humanUser;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_type_id", referencedColumnName = "job_type_id", nullable = false)
    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

}
