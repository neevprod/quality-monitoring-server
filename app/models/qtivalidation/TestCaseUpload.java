package models.qtivalidation;

import models.User;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "test_case_uploads")
public class TestCaseUpload {
    private Long testCaseUploadsId;
    private Integer previewerId;
    private Long tenantId;
    private User user;
    private Integer itemsCount;
    private Integer testCasesCount;
    private Integer failedTestCasesCount;
    private Integer mismatchedTestCasesCount;
    private String uploadJsonResult;
    private Timestamp processedDate;

    @Id
    @GeneratedValue
    @Column(name = "test_case_uploads_id")
    public Long getTestCaseUploadsId() {
        return testCaseUploadsId;
    }

    public void setTestCaseUploadsId(Long uploadId) {
        this.testCaseUploadsId = uploadId;
    }

    @Column(name = "previewer_id")
    public Integer getPreviewerId() {
        return previewerId;
    }

    public void setPreviewerId(Integer previewerId) {
        this.previewerId = previewerId;
    }

    @Column(name = "tenant_id")
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Basic
    @Column(name = "items_count")
    public Integer getItemsCount() {
        return itemsCount;
    }

    public void setItemsCount(Integer itemsCount) {
        this.itemsCount = itemsCount;
    }

    @Basic
    @Column(name = "test_cases_count")
    public Integer getTestCasesCount() {
        return testCasesCount;
    }

    public void setTestCasesCount(Integer testCasesCount) {
        this.testCasesCount = testCasesCount;
    }

    @Basic
    @Column(name = "failed_test_cases_count")
    public Integer getFailedTestCasesCount() {
        return failedTestCasesCount;
    }

    public void setFailedTestCasesCount(Integer failedTestCasesCount) {
        this.failedTestCasesCount = failedTestCasesCount;
    }

    @Basic
    @Column(name = "mismatched_test_cases_count")
    public Integer getMismatchedTestCasesCount() {
        return mismatchedTestCasesCount;
    }

    public void setMismatchedTestCasesCount(Integer mismatchedTestCasesCount) {
        this.mismatchedTestCasesCount = mismatchedTestCasesCount;
    }

    @Basic
    @Column(name = "upload_json_result")
    public String getUploadJsonResult() {
        return uploadJsonResult;
    }

    public void setUploadJsonResult(String uploadJsonResult) {
        this.uploadJsonResult = uploadJsonResult;
    }

    @Basic
    @Column(name = "processed_date")
    public Timestamp getProcessedDate() {
        return processedDate;
    }

    public void setProcessedDate(Timestamp processedDate) {
        this.processedDate = processedDate;
    }
}
