package models.qtivalidation;

import javax.persistence.*;

@Entity
@Table(name = "previewer_tenant_item")
public class PreviewerTenantItem {
    private Long previewerTenantItemId;
    private Long itemId;
    private String itemIdentifier;
    private Byte active;

    @Id
    @GeneratedValue
    @Column(name = "previewer_tenant_item_id")
    public Long getPreviewerTenantItemId() {
        return previewerTenantItemId;
    }

    public void setPreviewerTenantItemId(Long previewerTenantItemId) {
        this.previewerTenantItemId = previewerTenantItemId;
    }

    @Basic
    @Column(name = "item_id")
    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @Basic
    @Column(name = "item_identifier")
    public String getItemIdentifier() {
        return itemIdentifier;
    }

    public void setItemIdentifier(String itemIdentifier) {
        this.itemIdentifier = itemIdentifier;
    }

    @Basic
    @Column(name = "active")
    public Byte getActive() {
        return active;
    }

    public void setActive(Byte active) {
        this.active = active;
    }

}
