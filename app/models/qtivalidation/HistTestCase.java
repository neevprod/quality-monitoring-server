package models.qtivalidation;

import javax.persistence.*;

@Entity
@Table(name = "hist_test_case")
public class HistTestCase {
    private Integer histTestCaseId;
    private String failedTestCaseExpected;
    private String failedTestCaseActual;
    private Integer tcIdFromPreviewer;
    private String mods;
    private String failedFingerprintExpected;
    private String failedFingerprintActual;
    private HistItem histItem;

    @Id
    @GeneratedValue
    @Column(name = "hist_test_case_id")
    public Integer getHistTestCaseId() {
        return histTestCaseId;
    }

    public void setHistTestCaseId(Integer histTestCaseId) {
        this.histTestCaseId = histTestCaseId;
    }

    @Basic
    @Column(name = "failed_test_case_expected")
    public String getFailedTestCaseExpected() {
        return failedTestCaseExpected;
    }

    public void setFailedTestCaseExpected(String failedTestCaseExpected) {
        this.failedTestCaseExpected = failedTestCaseExpected;
    }

    @Basic
    @Column(name = "failed_test_case_actual")
    public String getFailedTestCaseActual() {
        return failedTestCaseActual;
    }

    public void setFailedTestCaseActual(String failedTestCaseActual) {
        this.failedTestCaseActual = failedTestCaseActual;
    }

    @Basic
    @Column(name = "tc_id_from_previewer")
    public Integer getTcIdFromPreviewer() {
        return tcIdFromPreviewer;
    }

    public void setTcIdFromPreviewer(Integer tcIdFromPreviewer) {
        this.tcIdFromPreviewer = tcIdFromPreviewer;
    }

    @Basic
    @Column(name = "mods")
    public String getMods() {
        return mods;
    }

    public void setMods(String mods) {
        this.mods = mods;
    }

    @Basic
    @Column(name = "failed_fingerprint_expected")
    public String getFailedFingerprintExpected() {
        return failedFingerprintExpected;
    }

    public void setFailedFingerprintExpected(String failedFingerprintExpected) {
        this.failedFingerprintExpected = failedFingerprintExpected;
    }

    @Basic
    @Column(name = "failed_fingerprint_actual")
    public String getFailedFingerprintActual() {
        return failedFingerprintActual;
    }

    public void setFailedFingerprintActual(String failedFingerprintActual) {
        this.failedFingerprintActual = failedFingerprintActual;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hist_item_id", referencedColumnName = "hist_item_id", nullable = false)
    public HistItem getHistItem() {
        return histItem;
    }

    public void setHistItem(HistItem histItem) {
        this.histItem = histItem;
    }

}
