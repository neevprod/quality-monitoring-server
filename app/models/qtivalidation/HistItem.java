package models.qtivalidation;

import javax.persistence.*;

@Entity
@Table(name = "hist_item")
public class HistItem {
    private Integer histItemId;
    private Integer testCases;
    private Integer failedTestCases;
    private Integer pendingTestCases;
    private HistTenant histTenant;
    private PreviewerTenantItem previewerTenantItem;

    @Id
    @GeneratedValue
    @Column(name = "hist_item_id")
    public Integer getHistItemId() {
        return histItemId;
    }

    public void setHistItemId(Integer histItemId) {
        this.histItemId = histItemId;
    }

    @Basic
    @Column(name = "test_cases")
    public Integer getTestCases() {
        return testCases;
    }

    public void setTestCases(Integer testCases) {
        this.testCases = testCases;
    }

    @Basic
    @Column(name = "failed_test_cases")
    public Integer getFailedTestCases() {
        return failedTestCases;
    }

    public void setFailedTestCases(Integer failedTestCases) {
        this.failedTestCases = failedTestCases;
    }

    @Basic
    @Column(name = "pending_test_cases")
    public Integer getPendingTestCases() {
        return pendingTestCases;
    }

    public void setPendingTestCases(Integer pendingTestCases) {
        this.pendingTestCases = pendingTestCases;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hist_tenant_id", referencedColumnName = "hist_tenant_id", nullable = false)
    public HistTenant getHistTenant() {
        return histTenant;
    }

    public void setHistTenant(HistTenant histTenant) {
        this.histTenant = histTenant;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previewer_tenant_item_id", referencedColumnName = "previewer_tenant_item_id", nullable = false)
    public PreviewerTenantItem getPreviewerTenantItem() {
        return previewerTenantItem;
    }

    public void setPreviewerTenantItem(PreviewerTenantItem previewerTenantItem) {
        this.previewerTenantItem = previewerTenantItem;
    }

}
