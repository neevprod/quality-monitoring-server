package models;

import javax.persistence.*;

@Entity
@Table(name = "application_module")
public class ApplicationModule {
    private Integer applicationModuleId;
    private String applicationModuleName;
    private PermissionType permissionType;

    @Id
    @Column(name = "application_module_id")
    public Integer getApplicationModuleId() {
        return applicationModuleId;
    }

    public void setApplicationModuleId(Integer applicationModuleId) {
        this.applicationModuleId = applicationModuleId;
    }

    @Basic
    @Column(name = "application_module_name")
    public String getApplicationModuleName() {
        return applicationModuleName;
    }

    public void setApplicationModuleName(String applicationModuleName) {
        this.applicationModuleName = applicationModuleName;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permission_type_id", referencedColumnName = "permission_type_id", nullable = false)
    public PermissionType getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(PermissionType permissionType) {
        this.permissionType = permissionType;
    }
}
