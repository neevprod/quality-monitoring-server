package models;

import play.data.validation.Constraints;

/**
 * @author Gert Selis
 *
 */
public class LoginUser {
    @Constraints.Required
    private String password;

    @Constraints.Required
    private String networkId;

    public LoginUser() {
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the networkId
     */
    public String getNetworkId() {
        return networkId;
    }

    /**
     * @param networkId
     *            the networkId to set
     */
    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }
}
