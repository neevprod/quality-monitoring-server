package models;

import models.qtivalidation.Previewer;
import models.systemvalidation.Environment;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "user_permissions")
public class UserPermissions {
    private Long userPermissionId;
    private Timestamp lastUpdate;
    private PermissionSettings permissionSettings;
    private User user;
    private Long userId;
    private ApplicationModule applicationModule;
    private Boolean isWildcard;
    private Integer previewerId;
    private Previewer previewer;
    private Long tenantId;
    private Integer environmentId;
    private Environment environment;
    private Long permissionSettingsId;

    public UserPermissions() {
    }

    public UserPermissions(Integer previewerId, Long tenantId, Long permissionSettingsId) {
        this.previewerId = previewerId;
        this.tenantId = tenantId;
        this.permissionSettingsId = permissionSettingsId;
    }

    @Id
    @GeneratedValue
    @Column(name = "user_permission_id")
    public Long getUserPermissionId() {
        return userPermissionId;
    }

    public void setUserPermissionId(Long userPermissionId) {
        this.userPermissionId = userPermissionId;
    }

    @Basic
    @Column(name = "last_update")
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permission_settings_id", referencedColumnName = "permission_settings_id", nullable = false)
    public PermissionSettings getPermissionSettings() {
        return permissionSettings;
    }

    public void setPermissionSettings(PermissionSettings permissionSettings) {
        this.permissionSettings = permissionSettings;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Basic
    @Column(name = "user_id", insertable = false, updatable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_module_id", referencedColumnName = "application_module_id", nullable = false)
    public ApplicationModule getApplicationModule() {
        return applicationModule;
    }

    public void setApplicationModule(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;
    }

    @Basic
    @Column(name = "is_wildcard")
    public Boolean getIsWildcard() {
        return isWildcard;
    }

    public void setIsWildcard(Boolean isWildcard) {
        this.isWildcard = isWildcard;
    }

    @Basic
    @Column(name = "previewer_id")
    public Integer getPreviewerId() {
        return previewerId;
    }

    public void setPreviewerId(Integer previewerId) {
        this.previewerId = previewerId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "previewer_id", referencedColumnName = "previewer_id", insertable = false, updatable = false)
    public Previewer getPreviewer() {
        return previewer;
    }

    public void setPreviewer(Previewer previewer) {
        this.previewer = previewer;
    }

    @Basic
    @Column(name = "tenant_id")
    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "environment_id", referencedColumnName = "environment_id", insertable = false, updatable = false)
    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Basic
    @Column(name = "environment_id")
    public Integer getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(Integer environmentId) {
        this.environmentId = environmentId;
    }



    @Basic
    @Column(name = "permission_settings_id", insertable = false, updatable = false)
    public Long getPermissionSettingsId() {
        return permissionSettingsId;
    }

    public void setPermissionSettingsId(Long permissionSettingsId) {
        this.permissionSettingsId = permissionSettingsId;
    }
}
