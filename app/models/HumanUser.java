package models;

import play.data.validation.Constraints;

import javax.persistence.*;

@Entity
@Table(name = "human_user")
public class HumanUser {
    private Long humanUserId;
    private Long userId;

    @Constraints.Email
    private String email;

    @Constraints.Required
    private String networkId;

    private User user;

    @Id
    @GeneratedValue
    @Column(name = "human_user_id")
    public Long getHumanUserId() {
        return humanUserId;
    }

    public void setHumanUserId(Long humanUserId) {
        this.humanUserId = humanUserId;
    }

    @Basic
    @Column(name = "user_id", insertable = false, updatable = false)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "network_id")
    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {
        this.networkId = networkId;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
