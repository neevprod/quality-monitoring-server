package models;

import javax.persistence.*;

@Entity
@Table(name = "job_type")
public class JobType {
    private Integer jobTypeId;
    private String code;
    private String name;

    @Id
    @GeneratedValue
    @Column(name = "job_type_id")
    public Integer getJobTypeId() {
        return jobTypeId;
    }

    public void setJobTypeId(Integer jobTypeId) {
        this.jobTypeId = jobTypeId;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
