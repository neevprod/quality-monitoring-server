package models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

    private Long id;
    private String fullname;
    private boolean isAdmin;
    private boolean isApiUser;

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fullname")
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Basic
    @Column(name = "isAdmin")
    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Basic
    @Column(name = "is_api_user")
    @JsonIgnore
    public boolean getIsApiUser() {
        return isApiUser;
    }

    public void setIsApiUser(boolean isApiUser) {
        this.isApiUser = isApiUser;
    }

}
