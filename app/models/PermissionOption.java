package models;

import javax.persistence.*;

@Entity
@Table(name = "permission_option")
public class PermissionOption {
    private Long permissionOptionId;
    private Long applicationModuleId;
    private ApplicationModule applicationModule;
    private String name;
    private boolean createAccess;
    private boolean readAccess;
    private boolean updateAccess;
    private boolean deleteAccess;

    @Id
    @GeneratedValue
    @Column(name = "permission_option_id")
    public Long getPermissionOptionId() {
        return permissionOptionId;
    }

    public void setPermissionOptionId(Long permissionOptionId) {
        this.permissionOptionId = permissionOptionId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "application_module_id")
    public Long getApplicationModuleId() {
        return this.applicationModuleId;
    }

    public void setApplicationModuleId(Long applicationModuleId) {
        this.applicationModuleId = applicationModuleId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_module_id", referencedColumnName = "application_module_id", insertable = false, updatable = false)
    public ApplicationModule getApplicationModule() {
        return applicationModule;
    }

    public void setApplicationModule(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;
    }

    @Basic
    @Column(name = "create_access")
    public boolean getCreateAccess() {
        return createAccess;
    }

    public void setCreateAccess(boolean createAccess) {
        this.createAccess = createAccess;
    }

    @Basic
    @Column(name = "read_access")
    public boolean getReadAccess() {
        return readAccess;
    }

    public void setReadAccess(boolean readAccess) {
        this.readAccess = readAccess;
    }

    @Basic
    @Column(name = "update_access")
    public boolean getUpdateAccess() {
        return updateAccess;
    }

    public void setUpdateAccess(boolean updateAccess) {
        this.updateAccess = updateAccess;
    }

    @Basic
    @Column(name = "delete_access")
    public boolean getDeleteAccess() {
        return deleteAccess;
    }

    public void setDeleteAccess(boolean deleteAccess) {
        this.deleteAccess = deleteAccess;
    }
}
