package models;

import play.data.validation.Constraints;
import util.EncryptorDecryptor;

import javax.persistence.*;

@Entity
@Table(name = "api_user")
public class ApiUser {
    private Long apiUserId;

    @Constraints.Required
    private String apiKeyName;

    @Constraints.Required
    private String apiKey;

    private User user;

    @Id
    @GeneratedValue
    @Column(name = "api_user_id")
    public Long getApiUserId() {
        return apiUserId;
    }

    public void setApiUserId(Long apiUserId) {
        this.apiUserId = apiUserId;
    }

    @Basic
    @Column(name = "api_key_name")
    public String getApiKeyName() {
        return apiKeyName;
    }

    public void setApiKeyName(String apiKeyName) {
        this.apiKeyName = apiKeyName;
    }

    @Basic
    @Column(name = "api_key")
    @Convert(converter = EncryptorDecryptor.class)
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
