package models;

import javax.persistence.*;

@Entity
@Table(name = "application_setting")
public class ApplicationSetting {
    private Integer applicationSettingId;
    private String applicationSettingName;
    private String applicationSettingValue;

    @Id
    @Column(name = "application_setting_id")
    public Integer getApplicationSettingId() {
        return applicationSettingId;
    }

    public void setApplicationSettingId(Integer applicationSettingId) {
        this.applicationSettingId = applicationSettingId;
    }

    @Basic
    @Column(name = "application_setting_name")
    public String getApplicationSettingName() {
        return applicationSettingName;
    }

    public void setApplicationSettingName(String applicationSettingName) {
        this.applicationSettingName = applicationSettingName;
    }

    @Basic
    @Column(name = "application_setting_value")
    public String getApplicationSettingValue() {
        return applicationSettingValue;
    }

    public void setApplicationSettingValue(String applicationSettingValue) {
        this.applicationSettingValue = applicationSettingValue;
    }
}
