package models;

import javax.persistence.*;

@Entity
@Table(name = "permission_settings_value")
public class PermissionSettingsValue {
    private Long permissionSettingsValueId;
    private boolean createAccess;
    private boolean readAccess;
    private boolean updateAccess;
    private boolean deleteAccess;
    private PermissionOption permissionOption;
    private PermissionSettings permissionSettings;

    @Id
    @GeneratedValue
    @Column(name = "permission_settings_value_id")
    public Long getPermissionSettingsValueId() {
        return permissionSettingsValueId;
    }

    public void setPermissionSettingsValueId(Long permissionSettingsValueId) {
        this.permissionSettingsValueId = permissionSettingsValueId;
    }

    @Basic
    @Column(name = "create_access")
    public boolean getCreateAccess() {
        return createAccess;
    }

    public void setCreateAccess(boolean createAccess) {
        this.createAccess = createAccess;
    }

    @Basic
    @Column(name = "read_access")
    public boolean getReadAccess() {
        return readAccess;
    }

    public void setReadAccess(boolean readAccess) {
        this.readAccess = readAccess;
    }

    @Basic
    @Column(name = "update_access")
    public boolean getUpdateAccess() {
        return updateAccess;
    }

    public void setUpdateAccess(boolean updateAccess) {
        this.updateAccess = updateAccess;
    }

    @Basic
    @Column(name = "delete_access")
    public boolean getDeleteAccess() {
        return deleteAccess;
    }

    public void setDeleteAccess(boolean deleteAccess) {
        this.deleteAccess = deleteAccess;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permission_option_id", referencedColumnName = "permission_option_id", nullable = false)
    public PermissionOption getPermissionOption() {
        return permissionOption;
    }

    public void setPermissionOption(PermissionOption permissionOption) {
        this.permissionOption = permissionOption;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "permission_settings_id", referencedColumnName = "permission_settings_id", nullable = false)
    public PermissionSettings getPermissionSettings() {
        return permissionSettings;
    }

    public void setPermissionSettings(PermissionSettings permissionSettings) {
        this.permissionSettings = permissionSettings;
    }
}
