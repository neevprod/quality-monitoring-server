package models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "permission_settings")
public class PermissionSettings {
    private Long permissionSettingsId;
    private String name;
    private Timestamp lastUpdate;
    private Integer applicationModuleId;
    private ApplicationModule applicationModule;
    private Set<PermissionSettingsValue> permissionSettingsValues;

    @Id
    @GeneratedValue
    @Column(name = "permission_settings_id")
    public Long getPermissionSettingsId() {
        return permissionSettingsId;
    }

    public void setPermissionSettingsId(Long permissionSettingsId) {
        this.permissionSettingsId = permissionSettingsId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "last_update")
    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Basic
    @Column(name = "application_module_id")
    public Integer getApplicationModuleId() {
        return applicationModuleId;
    }

    public void setApplicationModuleId(Integer applicationModuleId) {
        this.applicationModuleId = applicationModuleId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_module_id", referencedColumnName = "application_module_id",insertable=false, updatable=false)
    public ApplicationModule getApplicationModule() {
        return applicationModule;
    }

    public void setApplicationModule(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;
    }

    @OneToMany(mappedBy = "permissionSettings", orphanRemoval = true, fetch = FetchType.LAZY)
    public Set<PermissionSettingsValue> getPermissionSettingsValues() {
        return permissionSettingsValues;
    }

    public void setPermissionSettingsValues(Set<PermissionSettingsValue> permissionSettingsValues) {
        this.permissionSettingsValues = permissionSettingsValues;
    }
}
