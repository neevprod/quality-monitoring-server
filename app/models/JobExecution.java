package models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "job_execution")
public class JobExecution {
    private Long jobExecutionId;
    private String jobId;
    private Integer runNumber;
    private Boolean success;
    private String resultJson;
    private Timestamp completionTime;

    public JobExecution() {
    }

    public JobExecution(String jobId, Integer runNumber, Boolean success, String resultJson, Timestamp completionTime) {
        this.jobId = jobId;
        this.runNumber = runNumber;
        this.success = success;
        this.resultJson = resultJson;
        this.completionTime = completionTime;
    }

    @Id
    @GeneratedValue
    @Column(name = "job_execution_id")
    public Long getJobExecutionId() {
        return jobExecutionId;
    }

    public void setJobExecutionId(Long jobExecutionId) {
        this.jobExecutionId = jobExecutionId;
    }

    @Basic
    @Column(name = "job_id")
    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "run_number")
    public Integer getRunNumber() {
        return runNumber;
    }

    public void setRunNumber(Integer runNumber) {
        this.runNumber = runNumber;
    }

    @Basic
    @Column(name = "success")
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @Basic
    @Column(name = "result_json")
    public String getResultJson() {
        return resultJson;
    }

    public void setResultJson(String resultJson) {
        this.resultJson = resultJson;
    }

    @Basic
    @Column(name = "completion_time")
    public Timestamp getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(Timestamp completionTime) {
        this.completionTime = completionTime;
    }
}
