package services;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import models.ApplicationSetting;
import play.Logger;
import play.db.jpa.JPAApi;

public class ApplicationSettingService {
    private final JPAApi jpaApi;

    @Inject
    ApplicationSettingService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Find an application setting by name.
     * 
     * @param applicationSettingName
     *            The application setting name.
     * @return The application setting.
     */
    public Optional<ApplicationSetting> findByApplicationSettingName(String applicationSettingName) {
        Logger.debug("event=\"Retrieving application setting.\", applicationSettingName=\"{}\"",
                applicationSettingName);
        List<ApplicationSetting> applicationSettingList = jpaApi.em()
                .createQuery("select s from ApplicationSetting s "
                        + "where s.applicationSettingName = :applicationSettingName", ApplicationSetting.class)
                .setParameter("applicationSettingName", applicationSettingName)
                .getResultList();
        if (!applicationSettingList.isEmpty()) {
            return Optional.of(applicationSettingList.get(0));
        } else {
            return Optional.empty();
        }
    }
}
