package services;

import models.ApiUser;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class ApiUserService {
    private final JPAApi jpaApi;

    @Inject
    ApiUserService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Search for an API user by API key name. The associated user is eagerly fetched.
     *
     * @param apiKeyName The API user's API key name.
     * @return An Optional containing the API user identified by the API key name.
     */
    public Optional<ApiUser> findByApiKeyName(String apiKeyName) {
        Logger.info("event=\"Retrieving API user from DB by API key name.\"");
        List<ApiUser> apiUserList = jpaApi.em().createQuery("select au " +
                "from ApiUser au " +
                "join fetch au.user " +
                "where au.apiKeyName = :apiKeyName", ApiUser.class)
                .setParameter("apiKeyName", apiKeyName)
                .getResultList();
        if (!apiUserList.isEmpty()) {
            return Optional.of(apiUserList.get(0));
        } else {
            return Optional.empty();
        }
    }

}
