package services.gridautomation;

import models.gridautomation.*;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

public class GridAutomationService {
    private final JPAApi jpaApi;

    @Inject
    GridAutomationService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Retrieves the list of grid contracts.
     *
     * @return The List of {@link GridContract}
     */
    public List<GridContract> findAllContracts() {
        Logger.debug("event=\"Retrieving all grid contracts from DB\"");
        return jpaApi.em().createQuery("select gc from GridContract gc ", GridContract.class).getResultList();

    }

    public List<GridContract> findActiveContracts() {
        Logger.debug("event=\"Retrieving active grid contracts from DB\"");
        return jpaApi.em().createQuery("select gc from GridContract gc where gc.active = true", GridContract.class)
                .getResultList();

    }

    public List<GridProgram> findAllPrograms() {
        Logger.debug("event=\"Retrieving all grid programs from DB\"");
        return jpaApi.em().createQuery("select gp from GridProgram gp ", GridProgram.class).getResultList();

    }

    public List<GridProgram> findActivePrograms() {
        Logger.debug("event=\"Retrieving active grid programs from DB\"");
        return jpaApi.em().createQuery("select gp from GridProgram gp where gp.active = true", GridProgram.class)
                .getResultList();

    }

    public List<GridAdmin> findAllAdmins() {
        Logger.debug("event=\"Retrieving all grid admins from DB\"");
        return jpaApi.em().createQuery("select ga from GridAdmin ga ", GridAdmin.class).getResultList();

    }

    public List<GridAdmin> findActiveAdmins() {
        Logger.debug("event=\"Retrieving active grid admins from DB\"");
        // note, Gridcontract below is supposed to be underlined red
        return jpaApi.em().createQuery("select ga from GridAdmin ga where ga.active = true", GridAdmin.class)
                .getResultList();

    }

    public List<GridSeason> findAllSeasons() {
        Logger.debug("event=\"Retrieving all grid seasons from DB\"");
        // note, Gridcontract below is supposed to be underlined red
        return jpaApi.em().createQuery("select gs from GridSeason gs ", GridSeason.class).getResultList();

    }

    public List<GridSeason> findActiveSeasons() {
        Logger.debug("event=\"Retrieving active grid seasons from DB\"");
        return jpaApi.em().createQuery("select gs from GridSeason gs where gs.active = true", GridSeason.class)
                .getResultList();

    }

    /**
     * Retrieves the list of grid jobs.
     *
     * @return The List of {@link GridJob}
     */
    public List<GridJob> findAllGridJobs() {
        Logger.debug("event=\"Retrieving all grid jobs from DB\"");
        return jpaApi.em().createQuery("select gj from GridJob gj " +
                "join fetch gj.gridContract " +
                "join fetch  gj.jobStatus " +
                "join fetch gj.gridProgram " +
                "join fetch gj.gridSeason " +
                "join fetch gj.gridAdmin " +
                "join fetch gj.submitUser", GridJob.class).getResultList();

    }

    /**
     * Inserts a <code>gridJob</code> into the database.
     *
     * @param gridJob The instance of {@link GridJob}
     */
    public void saveJob(GridJob gridJob) {
        jpaApi.em().persist(gridJob);
    }

}