package services;

import models.JobType;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class JobTypeService {
    private final JPAApi jpaApi;

    @Inject
    JobTypeService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Fetch a JobType by its name.
     *
     * @param name The name of the JobType.
     * @return An Optional containing a JobType.
     */
    public Optional<JobType> findByName(String name) {
        Logger.info("event=\"Retrieving job type by name.\", name=\"{}\"", name);
        List<JobType> jobTypeList = jpaApi.em().createQuery("select jt from JobType jt where jt.name = :name",
                    JobType.class)
                .setParameter("name", name)
                .getResultList();
        if (!jobTypeList.isEmpty()) {
            return Optional.of(jobTypeList.get(0));
        } else {
            return Optional.empty();
        }
    }
}
