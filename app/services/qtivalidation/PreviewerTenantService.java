package services.qtivalidation;

import models.qtivalidation.PreviewerTenant;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class PreviewerTenantService {
    private final JPAApi jpaApi;

    @Inject
    PreviewerTenantService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Add a new tenant to the database.
     *
     * @param tenant
     *            the tenant to add
     */
    public void create(PreviewerTenant tenant) {
        jpaApi.em().persist(tenant);
    }

    /**
     * Fetch all tenants under the previewer with the given ID.
     *
     * @param previewerId
     *            the ID of the previewer to filter by
     * @return the list of tenants
     */
    public List<PreviewerTenant> findAll(int previewerId) {
        Logger.info("event=\"Retrieving all tenants under previewer.\", previewerId=\"{}\"", previewerId);
        return jpaApi.em().createQuery("select t from PreviewerTenant t where t.previewer.previewerId = :previewerId " +
                "order by t.tenantId", PreviewerTenant.class)
                .setParameter("previewerId", previewerId)
                .getResultList();
    }

    /**
     * Fetch all active tenants under the previewer with the given ID.
     *
     * @param previewerId
     *            the ID of the previewer to filter by
     * @return the list of tenants
     */
    public List<PreviewerTenant> findAllActive(int previewerId) {
        Logger.info("event=\"Retrieving all active tenants under previewer.\", previewerId=\"{}\"", previewerId);
        return jpaApi.em().createQuery("select t from PreviewerTenant t where t.previewer.previewerId = :previewerId " +
                "and t.active = true order by t.tenantId", PreviewerTenant.class)
                .setParameter("previewerId", previewerId)
                .getResultList();
    }

    /**
     * Fetch all active tenants under the previewer with the given ID, but only return the tenants whose tenantIds
     * appear in the given list.
     *
     * @param previewerId
     *            the ID of the previewer to filter by
     * @param allowedTenantIds
     *            the set of allowed tenant IDs to filter by
     * @return the list of tenants
     */
    public List<PreviewerTenant> findAllActive(int previewerId, Set<Long> allowedTenantIds) {
        Logger.info("event=\"Retrieving all active tenants under previewer.\", previewerId=\"{}\"", previewerId);
        if (!allowedTenantIds.isEmpty()) {
            return jpaApi.em()
                    .createQuery("select t from PreviewerTenant t where t.previewer.previewerId = :previewerId " +
                            "and t.active = true and t.tenantId in :allowedTenantIds " +
                            "order by t.tenantId", PreviewerTenant.class)
                    .setParameter("previewerId", previewerId)
                    .setParameter("allowedTenantIds", allowedTenantIds)
                    .getResultList();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Fetch the active tenant under the previewer with the given ID and which are not subscribed to by the given user for the given job type.
     *
     * @param previewerId
     *            The ID of the previewer.
     * @param tenantId
     *            The Id of tenant.
     * @param humanUserId
     *            The human user ID.
     * @param jobTypeId
     *            The job type ID.
     * @return The optional of {@link PreviewerTenant}
     */
    public Optional<PreviewerTenant> findActiveWithNoJobSubscription(int previewerId, Long tenantId,
            long humanUserId, int jobTypeId) {
        Logger.info("event=\"Retrieving active tenants under previewer without a subscription to the given job.\", " +
                "previewerId=\"{}\", jobTypeId=\"{}\"", previewerId, jobTypeId);

            List<PreviewerTenant> previewerTenants= jpaApi.em().createQuery("select t from PreviewerTenant t " +
                    "where t.previewer.previewerId = :previewerId " +
                    "and t.active = true and t.tenantId = :tenantId " +
                    "and t.previewerTenantId not in (" +
                    "select s.previewerTenantId from UserTenantJobSubscription s " +
                    "where s.humanUser.humanUserId = :humanUserId and s.jobType.jobTypeId = :jobTypeId) " +
                    "order by t.tenantId", PreviewerTenant.class)
                    .setParameter("previewerId", previewerId)
                    .setParameter("tenantId", tenantId)
                    .setParameter("humanUserId", humanUserId)
                    .setParameter("jobTypeId", jobTypeId)
                    .getResultList();
            if(previewerTenants.isEmpty()){
                return Optional.empty();
            }
            return Optional.of(previewerTenants.get(0));
    }

    /**
     * Fetch an active tenant by the previewer ID and tenant ID.
     *
     * @param previewerId
     *            the previewer ID to fetch by
     * @param tenantId
     *            the tenant ID to fetch by
     * @return an Optional containing the tenant
     */
    public Optional<PreviewerTenant> findActive(int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving tenant.\", previewerId=\"{}\", tenantId=\"{}\"", previewerId, tenantId);
        List<PreviewerTenant> tenantList = jpaApi.em().createQuery("select t from PreviewerTenant t " +
                "where t.previewer.previewerId = :previewerId and t.tenantId = :tenantId " +
                "and t.active = true", PreviewerTenant.class)
                .setParameter("previewerId", previewerId)
                .setParameter("tenantId", tenantId)
                .getResultList();
        if (!tenantList.isEmpty()) {
            return Optional.of(tenantList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * @param tenant
     *            The tenant ID
     * @param previewerUrl
     *            The previewer URL without protocol
     * @return An optional containing the tenant
     */
    public Optional<PreviewerTenant> findByTenantAndPreviewerUrl(final long tenant, final String previewerUrl) {
        Logger.debug("event=\"Retrieving tenant.\", tenant=\"{}\", previewerUrl=\"{}\"", tenant, previewerUrl);
        List<PreviewerTenant> previewerTenants = jpaApi.em().createQuery("select pt from PreviewerTenant pt "
                + "where pt.tenantId = :tenantId and pt.previewer.apiUrl = :previewerUrl", PreviewerTenant.class)
                .setParameter("tenantId", tenant)
                .setParameter("previewerUrl", previewerUrl).getResultList();
        if (!previewerTenants.isEmpty()) {
            return Optional.of(previewerTenants.get(0));
        } else {
            return Optional.empty();
        }
    }
}
