package services.qtivalidation;

import models.qtivalidation.Previewer;
import play.Logger;
import play.db.jpa.JPAApi;
import util.ResponseGeneratorVersionReader;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class PreviewerService {
    private final JPAApi jpaApi;

    @Inject
    PreviewerService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Fetch all previewers.
     *
     * @return the list of previewers
     */
    public List<Previewer> findAll() {
        Logger.info("event=\"Retrieving all previewers.\"");
        return jpaApi.em()
                .createQuery("select p from Previewer p order by p.previewerId", Previewer.class).getResultList();
    }

    /**
     * Fetch all previewers in the given set.
     *
     * @param previewerIds the set of previewerIds to filter by
     * @return the list of previewers
     */
    public List<Previewer> findAll(Set<Integer> previewerIds) {
        Logger.info("event=\"Retrieving all previewers in the given set.\"");
        if (!previewerIds.isEmpty()) {
            return jpaApi.em().createQuery("select p from Previewer p where p.previewerId in :previewerIds " +
                    "order by p.previewerId", Previewer.class)
                    .setParameter("previewerIds", previewerIds)
                    .getResultList();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Fetch a previewer by the given ID.
     *
     * @param previewerId the ID of the previewer to fetch
     * @return an Optional containing the previewer
     */
    public Optional<Previewer> find(int previewerId) {
        Logger.debug("event=\"Retrieving previewer.\", previewerId=\"{}\"", previewerId);
        List<Previewer> previewerList = jpaApi.em()
                .createQuery("select p from Previewer p where p.previewerId = :previewerId",
                        Previewer.class)
                .setParameter("previewerId", previewerId)
                .getResultList();
        if (!previewerList.isEmpty()) {
            return Optional.of(previewerList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetch all versions for the given artifactID.
     *
     * @return list of versionsId
     */
    public List<String> findAllRespGenVersions(String url) {
        return ResponseGeneratorVersionReader.getAllVersions(url);
    }

    public String downloadSelectedRespGenVersion(String versionId, String url, String path) {
        return ResponseGeneratorVersionReader.getSelectedVersionId(versionId, url, path);
    }

}
