package services.qtivalidation;

import models.qtivalidation.KtTestCase;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

public class KtTestCaseService {
    private final JPAApi jpaApi;

    @Inject
    KtTestCaseService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Retrieves a list of <code>KtTestCase</code> from the database for the given itemIdentifier.
     *
     * @param itemIdentifier The itemIdentifier
     */
    public List<KtTestCase> findByIdentifiers(final String itemIdentifier) {
        Logger.info("event=\"Retrieving KT Test Cases by item identifier.\", itemIdentifier=\"{}\"", itemIdentifier);

        return jpaApi.em().createQuery("select k from KtTestCase k " +
                "where k.itemIdentifier = :itemIdentifier ", KtTestCase.class)
                .setParameter("itemIdentifier", itemIdentifier)
                .getResultList();
    }

    /**
     * Deletes one or more <code>KtTestCase</code> from the database that match the given itemIdentifier.
     *
     * @param itemIdentifier The itemIdentifier
     */
    public void deleteByIdentifiers(final String itemIdentifier) {
        Logger.debug("event=\"Deleting KT Test Case data for item identifier. \", itemIdentifier=\"{}\"", itemIdentifier);
        jpaApi.em().createQuery("DELETE KtTestCase k where k.itemIdentifier = :itemIdentifier ")
                .setParameter("itemIdentifier", itemIdentifier)
                .executeUpdate();
    }

    /**
     * Adds a new KtTestCase to the kt_test_case table
     *
     * @param ktTestCase The {@link KtTestCase}
     */
    public void create(final KtTestCase ktTestCase) {
        jpaApi.em().persist(ktTestCase);
    }
}
