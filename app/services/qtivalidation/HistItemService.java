package services.qtivalidation;

import models.qtivalidation.HistItem;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

/**
 * This class contains methods that will query the database to find HistItems.
 */
public class HistItemService {
    private final JPAApi jpaApi;

    @Inject
    HistItemService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Get a list of HistItems by HistTenant ID. The corresponding PreviewerTenantItems will be eagerly fetched.
     *
     * @param histTenantId The ID of the HistTenant to fetch by.
     * @return A list of HistItems.
     */
    public List<HistItem> findAllByHistTenantId(int histTenantId) {
        Logger.info("event=\"Retrieving HistItems by HistTenant ID.\", histTenantId=\"{}\"", histTenantId);

        return jpaApi.em().createQuery("select h " +
                "from HistItem h " +
                "join fetch h.previewerTenantItem " +
                "where h.histTenant.histTenantId = :histTenantId " +
                "order by h.histItemId", HistItem.class)
                .setParameter("histTenantId", histTenantId)
                .getResultList();
    }

    /**
     * Get a list of HistItems by HistTenant ID, filtering by the given tenant. The corresponding PreviewerTenantItems
     * will be eagerly fetched.
     *
     * @param histTenantId The ID of the HistTenant to fetch by.
     * @param previewerId The ID of the previewer containing the tenants.
     * @param tenantId The ID of the tenant to filter by.
     * @return A list of HistItems.
     */
    public List<HistItem> findAllByHistTenantId(int histTenantId, int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving HistItems by HistTenant ID and filtering by the given tenant.\", " +
                "histTenantId=\"{}\", previewerId=\"{}\", tenantId=\"{}\"", histTenantId, previewerId, tenantId);

        return jpaApi.em().createQuery("select h " +
                "from HistItem h " +
                "join fetch h.previewerTenantItem " +
                "where h.histTenant.histTenantId = :histTenantId " +
                "and h.histTenant.previewerTenant.previewer.previewerId = :previewerId " +
                "and h.histTenant.previewerTenant.tenantId = :tenantId " +
                "order by h.histItemId", HistItem.class)
                .setParameter("histTenantId", histTenantId)
                .setParameter("previewerId", previewerId)
                .setParameter("tenantId", tenantId)
                .getResultList();
    }

}
