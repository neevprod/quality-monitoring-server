package services.qtivalidation;

import models.qtivalidation.HistTestCase;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

public class HistTestCaseService {
    private final JPAApi jpaApi;

    @Inject
    HistTestCaseService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Get a list of HistTestCases by HistItem ID, filtering by the given tenant.
     *
     * @param histItemId The ID of the HistItem to fetch by.
     * @param previewerId The ID of the previewer the tenant belongs to.
     * @param tenantId The ID of the tenant to filter by.
     * @return A list of HistTestCases.
     */
    public List<HistTestCase> findAllByHistItemId(int histItemId, int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving HistTestCases by HistItem ID and filtering by the given tenant.\", " +
                "histItemId=\"{}\", previewerId=\"{}\", tenantId=\"{}\"", histItemId, previewerId, tenantId);

        return jpaApi.em().createQuery("select htc " +
                "from HistTestCase htc " +
                "where htc.histItem.histItemId = :histItemId " +
                "and htc.histItem.histTenant.previewerTenant.previewerId = :previewerId " +
                "and htc.histItem.histTenant.previewerTenant.tenantId = :tenantId " +
                "order by htc.histTestCaseId", HistTestCase.class)
                .setParameter("histItemId", histItemId)
                .setParameter("previewerId", previewerId)
                .setParameter("tenantId", tenantId)
                .getResultList();
    }

}
