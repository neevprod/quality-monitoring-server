package services.qtivalidation;

import models.qtivalidation.HistExec;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * This class contains methods that will query the database to find HistExecs.
 */
public class HistExecService {
    private final JPAApi jpaApi;

    @Inject
    HistExecService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Get a list of HistExecs by previewer ID. Only HistExecs not run at the tenant level will be returned.
     *
     * @param previewerId The previewer ID.
     * @return A list of HistExecs.
     */
    public List<HistExec> findAllByPreviewerId(int previewerId) {
        Logger.info("event=\"Retrieving HistExecs by previewer ID.\", previewerId=\"{}\"", previewerId);

        return jpaApi.em().createQuery("select he " +
                "from HistExec he " +
                "where he.previewer.previewerId = :previewerId and he.tenantLevelExec = false " +
                "order by he.histExecId desc", HistExec.class)
                .setParameter("previewerId", previewerId)
                .getResultList();
    }

    /**
     * Get a list of HistExecs by previewer ID. Only HistExecs not run at the tenant level will be returned, and only
     * HistExecs that contain a HistTenant with a tenant ID in the given set will be returned.
     *
     * @param previewerId The previewer ID.
     * @param tenantIds The set of tenant IDs to filter by.
     * @return A list of HistExecs.
     */
    public List<HistExec> findAllByPreviewerId(int previewerId, Set<Long> tenantIds) {
        Logger.info("event=\"Retrieving HistExecs by previewer ID and filtering by tenant IDs in the given set.\", " +
                "previewerId=\"{}\"", previewerId);

        if (!tenantIds.isEmpty()) {
            return jpaApi.em().createQuery("select he " +
                    "from HistExec he " +
                    "where he.previewer.previewerId = :previewerId and he.tenantLevelExec = false " +
                    "and he.histExecId in " +
                    "(select distinct ht.histExec.histExecId " +
                    "from HistTenant ht " +
                    "where ht.previewerTenant.previewerId = :previewerId and ht.previewerTenant.tenantId in (:tenantIds)) " +
                    "order by he.histExecId desc", HistExec.class)
                    .setParameter("previewerId", previewerId)
                    .setParameter("tenantIds", tenantIds)
                    .getResultList();
        } else {
            return new ArrayList<>();
        }
    }
}
