package services.qtivalidation;

import models.qtivalidation.HistItemException;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

public class HistItemExceptionService {
    private final JPAApi jpaApi;

    @Inject
    HistItemExceptionService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Get a list of HistItemExceptions by HistTenant ID, filtering by the the given previewer.
     *
     * @param histTenantId The HistTenant ID.
     * @param previewerId The ID of the previewer that the tenant belongs to.
     * @param tenantId The tenant ID.
     * @return A list of HistItemExceptions.
     */
    public List<HistItemException> findAllByHistTenantId(int histTenantId, int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving HistItemExceptions by HistTenant ID and filtering by the given tenant.\", " +
                "histTenantId=\"{}\", previewerId=\"{}\", tenantId=\"{}\"", histTenantId, previewerId, tenantId);

        return jpaApi.em().createQuery("select hie " +
                "from HistItemException hie " +
                "where hie.histTenant.histTenantId = :histTenantId " +
                "and hie.histTenant.previewerTenant.previewer.previewerId = :previewerId " +
                "and hie.histTenant.previewerTenant.tenantId = :tenantId " +
                "order by hie.histItemExceptionId", HistItemException.class)
                .setParameter("histTenantId", histTenantId)
                .setParameter("previewerId", previewerId)
                .setParameter("tenantId", tenantId)
                .getResultList();
    }
}
