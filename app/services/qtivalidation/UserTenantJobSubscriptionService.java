package services.qtivalidation;

import models.qtivalidation.UserTenantJobSubscription;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class UserTenantJobSubscriptionService {
    private final JPAApi jpaApi;

    @Inject
    UserTenantJobSubscriptionService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Add a new UserTenantJobSubscription to the database.
     *
     * @param userTenantJobSubscription
     *            The UserTenantJobSubscription to create.
     */
    public void create(UserTenantJobSubscription userTenantJobSubscription) {
        Logger.info("event=\"Creating new UserTenantJobSubscription.\", previewerTenantId=\"{}\"",
                userTenantJobSubscription.getPreviewerTenantId());
        jpaApi.em().persist(userTenantJobSubscription);
    }

    /**
     * Deletes the <code>userTenantJobSubscription</code> from database.
     *
     * @param userTenantJobSubscription
     *            The {@link UserTenantJobSubscription} to be deleted
     */
    public void delete(final UserTenantJobSubscription userTenantJobSubscription) {
        Logger.debug("event=\"Deleteting userTenantJobSubscription record \", userTenantJobSubscriptionId=\"{}\"",
                userTenantJobSubscription.getUserTenantJobSubscriptionId());
        jpaApi.em().remove(userTenantJobSubscription);
    }

    /**
     * Get a list of UserTenantJobSubscriptions by JobType name. The corresponding Users will be eagerly fetched.
     *
     * @param jobTypeName
     *            The JobType name.
     * @return A list of UserTenantJobSubscriptions.
     */
    public List<UserTenantJobSubscription> fetchByJobTypeName(String jobTypeName) {
        Logger.info("event=\"Retrieving UserTenantJobSubscriptions by JobType name.\", jobTypeName=\"{}\"",
                jobTypeName);

        return jpaApi.em()
                .createQuery(
                        "select u " + "from UserTenantJobSubscription u " + "join fetch u.humanUser "
                                + "where u.jobType.name = :jobTypeName " + "order by u.userTenantJobSubscriptionId",
                        UserTenantJobSubscription.class)
                .setParameter("jobTypeName", jobTypeName).getResultList();
    }

    /**
     * Get a list of {@link UserTenantJobSubscription} for given user id and previewerId.
     * 
     * @param userId
     *            The user id
     * @param previewerId
     *            The ID of previewer
     * @return The list of UserTenantJobSubscriptions
     */
    public List<UserTenantJobSubscription> findByUserIdAndPreviewerId(final Long userId, final Integer previewerId) {
        Logger.info("event=\"Retrieving UserTenantJobSubscriptions by User ID.\", UserId=\"{}\"", userId);

        return jpaApi.em().createQuery("select u from UserTenantJobSubscription u " + "join fetch  u.humanUser hu "
                + "join fetch u.previewerTenant pt " + "where hu.userId = :userId and pt.previewerId = :previewerId",
                UserTenantJobSubscription.class).setParameter("userId", userId).setParameter("previewerId", previewerId)
                .getResultList();
    }

    /**
     * Retrieves the Optional of {@link UserTenantJobSubscription} for the given previewerId, tenantId, userId and
     * jobType.
     * 
     * @param previewerId
     *            The ID of previewer
     * @param tenantId
     *            The ID of tenant
     * @param userId
     *            The user id
     * @param jobTypeId
     *            The ID of job type
     * @return The Optional of {@link UserTenantJobSubscription}
     */
    public Optional<UserTenantJobSubscription> findByPreviewerIdTenantIdUserIdAndJobTypeId(final int previewerId,
            final Long tenantId, final Long userId, final int jobTypeId) {
        List<UserTenantJobSubscription> result = jpaApi.em()
                .createQuery(
                        "select s from UserTenantJobSubscription s "
                                + "join fetch s.previewerTenant pt join fetch s.humanUser u join fetch s.jobType jt "
                                + "where pt.previewerId = :previewerId and pt.tenantId = :tenantId "
                                + "and u.userId = :userId and jt.jobTypeId = :jobTypeId",
                        UserTenantJobSubscription.class)
                .setParameter("previewerId", previewerId).setParameter("tenantId", tenantId)
                .setParameter("userId", userId).setParameter("jobTypeId", jobTypeId).getResultList();
        if (result.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(result.get(0));
    }

}
