package services.qtivalidation;

import models.qtivalidation.TestCaseUpload;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

public class TestCaseUploadService {
    private final JPAApi jpaApi;

    @Inject
    TestCaseUploadService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public List<TestCaseUpload> findAllByTenantId(int previewerId, long tenantId) {
        Logger.info("event=\"Retrieving Test Case Uploads by Tenant ID.\", previewerId=\"{}\", tenantId=\"{}\"",
                previewerId, tenantId);

        return jpaApi.em().createQuery("select t from TestCaseUpload t " +
                    "join fetch t.user " +
                    "where t.previewerId = :previewerId and t.tenantId = :tenantId " +
                    "order by t.testCaseUploadsId desc", TestCaseUpload.class)
                .setParameter("previewerId", previewerId)
                .setParameter("tenantId", tenantId).getResultList();
    }
}