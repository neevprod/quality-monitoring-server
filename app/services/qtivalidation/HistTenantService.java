package services.qtivalidation;

import models.qtivalidation.HistTenant;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class HistTenantService {
    private final JPAApi jpaApi;

    @Inject
    HistTenantService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Get a HistTenant by its ID, but only if it is associated with the given HistExec. The corresponding
     * PreviewerTenant will be eagerly fetched.
     *
     * @param histTenantId The ID of the HistTenant.
     * @param histExecId The ID of the HistExec.
     * @return An Optional containing a HistTenant.
     */
    public Optional<HistTenant> find(int histTenantId, int histExecId) {
        Logger.info("event=\"Retrieving HistTenant.\", histTenantId=\"{}\", histExecId=\"{}\"", histTenantId,
                histExecId);

        List<HistTenant> histTenants = jpaApi.em().createQuery("select h from HistTenant h " +
                "join fetch h.previewerTenant " +
                "where h.histTenantId = :histTenantId " +
                "and h.histExec.histExecId = :histExecId ", HistTenant.class)
                .setParameter("histTenantId", histTenantId)
                .setParameter("histExecId", histExecId)
                .getResultList();
        if (!histTenants.isEmpty()) {
            return Optional.of(histTenants.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get a HistTenant by its ID, but only if it is associated with the given HistExec and one of the Tenants in the
     * given set. The corresponding PreviewerTenant will be eagerly fetched.
     *
     * @param histTenantId The ID of the HistTenant.
     * @param histExecId The ID of the HistExec.
     * @param previewerId The ID of the previewer the given tenant belongs to.
     * @param tenantIds The set of tenant IDs.
     * @return An Optional containing a HistTenant.
     */
    public Optional<HistTenant> find(int histTenantId, int histExecId, int previewerId, Set<Long> tenantIds) {
        Logger.info("event=\"Retrieving HistTenant.\", histTenantId=\"{}\", histExecId=\"{}\", previewerId=\"{}\"",
                histTenantId, histExecId, previewerId);

        List<HistTenant> histTenants = jpaApi.em().createQuery("select h from HistTenant h " +
                "join fetch h.previewerTenant " +
                "where h.histTenantId = :histTenantId " +
                "and h.histExec.histExecId = :histExecId " +
                "and h.previewerTenant.previewerId = :previewerId " +
                "and h.previewerTenant.tenantId in :tenantIds", HistTenant.class)
                .setParameter("histTenantId", histTenantId)
                .setParameter("histExecId", histExecId)
                .setParameter("previewerId", previewerId)
                .setParameter("tenantIds", tenantIds)
                .getResultList();
        if (!histTenants.isEmpty()) {
            return Optional.of(histTenants.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Get a list of completed HistTenants by the set of HistExec IDs given. The corresponding PreviewerTenants will be
     * eagerly fetched.
     *
     * @param histExecId The HistExec ID to fetch with.
     * @return A list of HistTenants.
     */
    public List<HistTenant> findAllByHistExecId(int histExecId) {
        Logger.info("event=\"Retrieving HistTenants by HistExec ID.\", histExecId=\"{}\"", histExecId);

        return jpaApi.em().createQuery("select h " +
                "from HistTenant h " +
                "join fetch h.previewerTenant " +
                "where h.histExec.histExecId = :histExecId " +
                "and h.execCompleted = true " +
                "order by h.histTenantId", HistTenant.class)
                .setParameter("histExecId", histExecId)
                .getResultList();
    }

    /**
     * Get a list of completed HistTenants by the set of HistExec IDs given. The corresponding PreviewerTenants will be
     * eagerly fetched. Any entries not associated with the given tenants will be filtered out.
     *
     * @param histExecId The HistExec ID to fetch with.
     * @param previewerId The ID of the previewer the given tenants belong to.
     * @param tenantIds The tenant IDs to filter by.
     * @return A list of HistTenants.
     */
    public List<HistTenant> findAllByHistExecId(int histExecId, int previewerId, Set<Long> tenantIds) {
        Logger.info("event=\"Retrieving HistTenants by HistExec ID and filtering by tenant IDs in the given set.\", " +
                "histExecId=\"{}\", previewerId=\"{}\"", histExecId, previewerId);

        if (!tenantIds.isEmpty()) {
            return jpaApi.em().createQuery("select h " +
                    "from HistTenant h " +
                    "join fetch h.previewerTenant " +
                    "where h.histExec.histExecId = :histExecId " +
                    "and h.previewerTenant.previewerId = :previewerId and h.previewerTenant.tenantId in :tenantIds " +
                    "and h.execCompleted = true " +
                    "order by h.histTenantId", HistTenant.class)
                    .setParameter("histExecId", histExecId)
                    .setParameter("previewerId", previewerId)
                    .setParameter("tenantIds", tenantIds)
                    .getResultList();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Get a list of completed HistTenants by the set of HistExec IDs given. The corresponding PreviewerTenants and
     * Previewers will be eagerly fetched.
     *
     * @param histExecIds The set of HistExec IDs to fetch with.
     * @return A list of HistTenants.
     */
    public List<HistTenant> findAllByHistExecIds(Set<Integer> histExecIds) {
        Logger.info("event=\"Retrieving HistTenants by HistExec ID.\"");

        if (!histExecIds.isEmpty()) {
            return jpaApi.em().createQuery("select h " +
                    "from HistTenant h " +
                    "join fetch h.previewerTenant pt " +
                    "join fetch pt.previewer " +
                    "where h.histExec.histExecId in :histExecIds " +
                    "and h.execCompleted = true " +
                    "order by h.histTenantId", HistTenant.class)
                    .setParameter("histExecIds", histExecIds)
                    .getResultList();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Get a list of completed HistTenants for the provided previewer ID and tenant ID.
     * Results also contain join to HistExec table.
     *
     * @param previewerId The previewer ID.
     * @param tenantId The tenant ID.
     * @return A list of HistTenants.
     */
    public List<HistTenant> findAllByTenantId(int previewerId, Long tenantId){
        Logger.info("event=\"Retrieving HistTenant data for Scoring Validation Jobs.\"");
        return jpaApi.em().createQuery("select h " +
                "from HistTenant h " +
                "join fetch h.histExec " +
                "where h.previewerTenant.tenantId = :tenantId " +
                "and h.previewerTenant.previewerId = :previewerId " +
                "and h.execCompleted = true " +
                "order by h.histTenantId desc", HistTenant.class)
                .setParameter("previewerId", previewerId)
                .setParameter("tenantId", tenantId)
                .getResultList();
    }
}
