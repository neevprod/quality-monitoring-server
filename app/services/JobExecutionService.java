package services;

import models.JobExecution;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class JobExecutionService {
    private final JPAApi jpaApi;

    @Inject
    JobExecutionService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Add a new JobExecution to the database.
     *
     * @param jobExecution The JobExecution to add.
     */
    public void create(JobExecution jobExecution) {
        Logger.info("event=\"Saving new job execution to the DB.\", jobId=\"{}\"", jobExecution.getJobId());
        jpaApi.em().persist(jobExecution);
    }

    /**
     * Retrieve the latest completed execution of the given job ID.
     *
     * @param jobId The job ID.
     * @return An Optional containing a JobExecution.
     */
    public Optional<JobExecution> findLatest(String jobId) {
        Logger.info("event=\"Retrieving latest execution of job from the DB.\", jobId=\"{}\"", jobId);
        List<JobExecution> jobTypeList =
                jpaApi.em().createQuery("select je from JobExecution je where je.jobId LIKE :jobId " +
                        "order by je.jobExecutionId desc", JobExecution.class)
                        .setParameter("jobId", jobId)
                        .getResultList();
        if (!jobTypeList.isEmpty()) {
            return Optional.of(jobTypeList.get(0));
        } else {
            return Optional.empty();
        }
    }
}
