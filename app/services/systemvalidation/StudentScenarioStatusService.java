package services.systemvalidation;

import models.systemvalidation.StudentScenarioStatus;
import play.Logger;
import play.cache.SyncCacheApi;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by VJOSHNA on 4/21/2017.
 */
public class StudentScenarioStatusService {
    private final JPAApi jpaApi;
    private final SyncCacheApi cache;

    @Inject
    StudentScenarioStatusService(JPAApi jpaApi, SyncCacheApi cache) {
        this.jpaApi = jpaApi;
        this.cache = cache;
    }

    /**
     * Fetches the {@link StudentScenarioStatus} for the given studentScenarioStatusName.
     * 
     * @param studentScenarioStatusName
     *            The studentScenarioStatusName
     * @return The Optional of {@link StudentScenarioStatus}
     */
    public Optional<StudentScenarioStatus> findByStudentScenarioStatusName(final String studentScenarioStatusName) {
        Logger.debug("event=\"Retrieving student scenario status from cache.\", studentScenarioStatusName=\"{}\"",
                studentScenarioStatusName);
        return cache.getOrElseUpdate("studentScenarioStatus." + studentScenarioStatusName,
                () -> retrieveFromDbByStudentScenarioStatusName(studentScenarioStatusName));
    }

    private Optional<StudentScenarioStatus> retrieveFromDbByStudentScenarioStatusName(
            final String studentScenarioStatusName) {
        Logger.debug("event=\"Retrieving student scenario status from DB.\", studentScenarioStatusName=\"{}\"",
                studentScenarioStatusName);

        List<StudentScenarioStatus> studentScenarioStatuses = jpaApi.em()
                .createQuery(
                        "select sss from StudentScenarioStatus sss " + "where sss.status = :studentScenarioStatusName",
                        StudentScenarioStatus.class)
                .setParameter("studentScenarioStatusName", studentScenarioStatusName).getResultList();
        if (studentScenarioStatuses.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(studentScenarioStatuses.get(0));
    }

}
