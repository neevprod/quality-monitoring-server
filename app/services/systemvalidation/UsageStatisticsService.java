package services.systemvalidation;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import play.Logger;
import play.db.jpa.JPAApi;

/**
 * @author Tarun Gupta
 *
 */
public class UsageStatisticsService {
    private final JPAApi jpaApi;

    @Inject
    UsageStatisticsService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Fetch all test attempts.
     *
     * @return Count of test attempts.
     */
    public int findAllTestAttempts(Integer environmentId, String scopeTreePath, Timestamp startTime,
            Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Test Attempts.\"");

        String query = "SELECT COUNT(DISTINCT ss.student_scenario_id)" + " FROM div_job_exec dje"
                + " JOIN student_scenario ss ON ss.div_job_exec_id = dje.div_job_exec_id"
                + " JOIN student_scenario_result ssr" + " ON ssr.student_scenario_id = ss.student_scenario_id"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }

        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }

        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        List<BigInteger> testAttemptsList = createQuery.getResultList();
        if (testAttemptsList != null && !testAttemptsList.isEmpty()) {
            return testAttemptsList.get(0).intValue();
        }
        return 0;

    }

    /**
     * Fetch all passed test attempts.
     *
     * @return Count of passed test attempts.
     */
    public int findAllPassedTestAttempts(Integer environmentId, String scopeTreePath, Timestamp startTime,
            Timestamp endTime) {
        int passedAttemptsCount = 0;
        Logger.debug("event=\"Retrieving all Passes Test Attempts.\"");
        String query = "SELECT (case when SUM(b.success) is null then  0 else SUM(b.success) end) "
                + "FROM (SELECT (case when SUM(a.success - 1) = 0 then  0 else 1 end) success"
                + " FROM (SELECT ss.student_scenario_id," + " ssr.student_scenario_result_id," + " ssr.state_id,"
                + " ssr.success" + " FROM div_job_exec dje" + "  JOIN student_scenario ss"
                + " ON ss.div_job_exec_id = dje.div_job_exec_id" + " JOIN student_scenario_result ssr"
                + " ON ss.student_scenario_id = ssr.student_scenario_id" + " JOIN"
                + " (SELECT MAX(ssr.student_scenario_result_id)" + " studentScenarioResultId"
                + " FROM student_scenario_result ssr" + " GROUP BY ssr.student_scenario_id, ssr.state_id) max"
                + "  ON ssr.student_scenario_result_id =" + " max.studentScenarioResultId"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }

        query = query + " )a GROUP BY a.student_scenario_id) b";
        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        List<Number> passedAttemptsList = createQuery.getResultList();

        if (!passedAttemptsList.isEmpty()) {
            passedAttemptsCount = passedAttemptsList.get(0).intValue();
        }
        return passedAttemptsCount;

    }

    /**
     * Fetch all failed resumed test attempts.
     *
     * @return Count of failed resumed test attempts.
     */
    public int findAllFailedResumedTestAttempts(Integer environmentId, String scopeTreePath, Timestamp startTime,
            Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Failed Resumed Test Attempts.\"");
        int failedResumedAttemptsCount = 0;
        String query = "SELECT (case when SUM(b.success) is null then  0 else SUM(b.success) end)"
                + " FROM(SELECT (case when SUM(a.success - 1) = 0 then  0 else 1 end) success "
                + " FROM (SELECT ss.student_scenario_id," + " ssr.student_scenario_result_id,"
                + " ssr.state_id,ssr.success FROM div_job_exec dje" + " JOIN student_scenario ss"
                + " ON ss.div_job_exec_id = dje.div_job_exec_id" + " JOIN student_scenario_result ssr"
                + " ON ss.student_scenario_id = ssr.student_scenario_id" + " JOIN"
                + " (SELECT MAX(ssr.student_scenario_result_id)" + " studentScenarioResultId "
                + " FROM student_scenario_result ssr  " + " GROUP BY ssr.student_scenario_id, ssr.state_id) max "
                + " ON ssr.student_scenario_result_id =  " + " max.studentScenarioResultId "
                + " JOIN (SELECT DISTINCT (ssr.student_scenario_id) " + "  FROM student_scenario_result ssr "
                + " WHERE ssr.version > 1) resumedStudentScenarios "
                + "  ON resumedStudentScenarios.student_scenario_id =  " + " ssr.student_scenario_id"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }

        query = query + " )a GROUP BY a.student_scenario_id) b";
        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        List<Number> failedResumedAttemptsList = createQuery.getResultList();

        if (!failedResumedAttemptsList.isEmpty()) {
            failedResumedAttemptsCount = failedResumedAttemptsList.get(0).intValue();
        }
        return failedResumedAttemptsCount;
    }

    /**
     * Fetch all unique users.
     *
     * @return Count of users.
     */
    public int findAllUniqueUsers(Integer environmentId, String scopeTreePath, Timestamp startTime, Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Unique Users.\"");
        String query = "SELECT COUNT(DISTINCT dje.submit_user_id)" + " FROM div_job_exec dje"
                + " JOIN student_scenario ss ON ss.div_job_exec_id = dje.div_job_exec_id"
                + " JOIN student_scenario_result ssr" + "  ON ssr.student_scenario_id = ss.student_scenario_id"
                + " JOIN user u ON u.id = dje.submit_user_id" + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }

        Query createQuery = jpaApi.em().createNativeQuery(query);

        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        List<Number> uniqueUsers = createQuery.getResultList();

        if (null != uniqueUsers && !uniqueUsers.isEmpty()) {
            return uniqueUsers.get(0).intValue();
        }
        return 0;
    }

    /**
     * Fetch all unique forms.
     *
     * @return Count of forms.
     */
    public int findAllUniqueForms(Integer environmentId, String scopeTreePath, Timestamp startTime, Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Unique Forms.\"");
        String query = "SELECT COUNT(DISTINCT s.form_code)" + "FROM div_job_exec dje"
                + " JOIN student_scenario ss ON ss.div_job_exec_id = dje.div_job_exec_id"
                + " JOIN student_scenario_result ssr" + " ON ssr.student_scenario_id = ss.student_scenario_id"
                + " JOIN scenario s ON s.scenario_id = ss.testnav_scenario_id"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }
        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        List<Number> uniqueForms = createQuery.getResultList();

        if (null != uniqueForms && !uniqueForms.isEmpty()) {
            return uniqueForms.get(0).intValue();
        }
        return 0;
    }

    /**
     * Fetch all passed resumed test attempts.
     *
     * @return Count of passed resumed test attempts.
     */
    public int findAllPassedResumedTestAttempts(Integer environmentId, String scopeTreePath, Timestamp startTime,
            Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Passes Resumed Test Attempts.\"");
        int passedResumedAttemptsCount = 0;
        String query = "SELECT (case when SUM(b.success) is null then  0 else SUM(b.success) end)"
                + "	FROM (SELECT (case when SUM(a.success - 1) = 0 then  0 else 1 end) success"
                + "	FROM (SELECT ss.student_scenario_id, ssr.student_scenario_result_id,"
                + " ssr.state_id,ssr.success" + " FROM div_job_exec dje " + " JOIN student_scenario ss"
                + " ON ss.div_job_exec_id = dje.div_job_exec_id	" + " JOIN student_scenario_result ssr"
                + "	 ON ss.student_scenario_id = ssr.student_scenario_id"
                + "	JOIN (SELECT MAX(ssr.student_scenario_result_id)" + "	studentScenarioResultId	"
                + " FROM student_scenario_result ssr" + " GROUP BY ssr.student_scenario_id, ssr.state_id) max"
                + " ON ssr.student_scenario_result_id =" + "	max.studentScenarioResultId"
                + " JOIN (SELECT DISTINCT (ssr.student_scenario_id)" + "	FROM student_scenario_result ssr"
                + "	WHERE ssr.version > 1) resumedStudentScenarios"
                + "	ON resumedStudentScenarios.student_scenario_id =" + " ssr.student_scenario_id	"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }
        query = query + " )a GROUP BY a.student_scenario_id) b";
        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);

        List<Number> passedResumedAttemptsList = createQuery.getResultList();

        if (!passedResumedAttemptsList.isEmpty()) {
            passedResumedAttemptsCount = passedResumedAttemptsList.get(0).intValue();
        }
        return passedResumedAttemptsCount;
    }

    /**
     * Fetch all Failed Test Attempts.
     *
     * @return Count of Failed Test Attempts.
     */
    public int findAllFailedTestAttempts(Integer environmentId, String scopeTreePath, Timestamp startTime,
            Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Failed Test Attempts.\"");
        int allFailedTestAttemptsCount = 0;
        String query = "SELECT (case when SUM(b.success) is null then  0 else SUM(b.success) end)"
                + "	FROM (SELECT (case when SUM(a.success - 1) = 0 then  0 else 1 end) success"
                + "	FROM (SELECT ss.student_scenario_id," + " ssr.student_scenario_result_id," + " ssr.state_id,"
                + " ssr.success" + " FROM div_job_exec dje" + " JOIN student_scenario ss"
                + " ON ss.div_job_exec_id = dje.div_job_exec_id" + " JOIN student_scenario_result ssr"
                + " ON ss.student_scenario_id = ssr.student_scenario_id"
                + "	JOIN (SELECT MAX(ssr.student_scenario_result_id)" + " studentScenarioResultId"
                + " FROM student_scenario_result ssr" + "	GROUP BY ssr.student_scenario_id, ssr.state_id) max"
                + "	ON ssr.student_scenario_result_id =" + " max.studentScenarioResultId"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }
        query = query + " )a GROUP BY a.student_scenario_id) b";

        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        List<Number> failedAttemptsList = createQuery.getResultList();

        if (!failedAttemptsList.isEmpty()) {
            allFailedTestAttemptsCount = failedAttemptsList.get(0).intValue();
        }
        return allFailedTestAttemptsCount;
    }

    /**
     * Fetch all failed test attempts for state.
     *
     * @return Count of failed test attempts for state.
     */
    public List<Object[]> findFailedTestAttemptsForState(Integer environmentId, String scopeTreePath,
            Timestamp startTime, Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Failed Test Attempts for State.\"");
        String query = "SELECT s.state_name State," + " COUNT(ssr.student_scenario_id)" + " FROM div_job_exec dje"
                + " JOIN student_scenario ss ON ss.div_job_exec_id = dje.div_job_exec_id"
                + " JOIN student_scenario_result ssr" + " ON ssr.student_scenario_id = ss.student_scenario_id"
                + " JOIN state s ON s.state_id = ssr.state_id"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }
        query = query + " AND ssr.success = 0" + " AND ssr.version = 1" + " GROUP BY s.state_name"
                + " ORDER BY s.state_name";
        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        return createQuery.getResultList();

    }

    /**
     * Fetch all test attempts for form.
     *
     * @return Count of test attempts for form.
     */
    public List<Object[]> findTestAttemptsForForm(Integer environmentId, String scopeTreePath, Timestamp startTime,
            Timestamp endTime) {
        Logger.debug("event=\"Retrieving all Test Attempts for Form.\"");
        String query = "SELECT s.form_code," + " COUNT(DISTINCT ss.student_scenario_id)" + " FROM div_job_exec dje"
                + " JOIN student_scenario ss ON ss.div_job_exec_id = dje.div_job_exec_id"
                + " JOIN student_scenario_result ssr" + "   ON ssr.student_scenario_id = ss.student_scenario_id"
                + " JOIN scenario s ON s.scenario_id = ss.testnav_scenario_id"
                + " WHERE ssr.timestamp BETWEEN :startDate AND :endDate ";
        if (environmentId != 0) {
            query = query + " AND dje.environment_id LIKE :environmentId";
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            query = query + " AND dje.scope_tree_path LIKE :scopeTreePath";
        }
        query = query + " GROUP BY s.form_code" + " ORDER BY s.form_code ASC";
        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        if (!scopeTreePath.equalsIgnoreCase("ALL")) {
            createQuery.setParameter("scopeTreePath", scopeTreePath);
        }
        createQuery.setParameter("startDate", startTime).setParameter("endDate", endTime);
        return createQuery.getResultList();
    }

    /**
     * Fetch scopeTreePaths for environmentId.
     *
     * @return scopeTreePaths for environmentId.
     */
    public List<Object[]> findAllScopeTreePath(Integer environmentId) {
        Logger.debug("event=\"Retrieving all scopeTreePaths for the given environmentId.\"" + environmentId);
        String query = "SELECT distinct dje.scope_tree_path" + " FROM div_job_exec dje";
        if (environmentId != 0) {
            query = query + " where dje.environment_Id = :environmentId";
        }
        query = query + " ORDER BY dje.scope_tree_path";
        Query createQuery = jpaApi.em().createNativeQuery(query);
        if (environmentId != 0) {
            createQuery.setParameter("environmentId", environmentId);
        }
        return createQuery.getResultList();
    }

}
