package services.systemvalidation;

import models.systemvalidation.ApiConnection;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * @author Nand Joshi
 *
 */
public class ApiConnectionService {
    private final JPAApi jpaApi;

    @Inject
    ApiConnectionService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Inserts an <code>apiConnection</code> into database.
     * 
     * @param apiConnection
     *            The instance of {@link ApiConnection}
     */
    public void save(final ApiConnection apiConnection) {
        jpaApi.em().persist(apiConnection);
    }

    /**
     * Fetch all API connection information.
     * 
     * @return The List of {@link ApiConnection}
     */
    public List<ApiConnection> findAll() {
        Logger.debug("event=\"Retrieving API Connection.\"");
        return jpaApi.em().createQuery("select ac from ApiConnection ac", ApiConnection.class).getResultList();
    }

    /**
     * Fetch an API connection by its ID.
     * 
     * @param apiConnectionId
     *            The API Connection Id
     * @return The Optional of {@link ApiConnection}
     */
    public Optional<ApiConnection> findByApiConnectionId(final Integer apiConnectionId) {
        Logger.debug("event=\"Retrieving API Connection.\", dbConnectionId=\"{}\"" + apiConnectionId);
        List<ApiConnection> dbConnections = jpaApi.em()
                .createQuery("select ac from ApiConnection ac where ac.apiConnectionId = :apiConnectionId",
                        ApiConnection.class)
                .setParameter("apiConnectionId", apiConnectionId).getResultList();
        if (!dbConnections.isEmpty()) {
            return Optional.of(dbConnections.get(0));
        } else {
            return Optional.empty();
        }
    }
}
