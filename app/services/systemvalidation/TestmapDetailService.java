package services.systemvalidation;

import models.systemvalidation.TestmapDetail;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * @author Nand Joshi
 */
public class TestmapDetailService {
    public final JPAApi jpaApi;

    @Inject
    TestmapDetailService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Saves new testmapDetail into database.
     *
     * @param testmapDetail The instance of {@link TestmapDetail}
     */
    public final void create(TestmapDetail testmapDetail) {
        Logger.debug("event=\"Creating new TestmapDetail\".");
        jpaApi.em().persist(testmapDetail);
    }

    /**
     * Fetch an TestmapDetail by <code>testmapId</code> and <code>testmapVersion</code>.
     *
     * @param testmapId      The Testmap Id
     * @param testmapVersion The Testmap Version
     * @return The Optional of {@link TestmapDetail}
     */
    public Optional<TestmapDetail> findByTestmapIdAndVersion(int testmapId, int testmapVersion) {
        Logger.debug("event=\"Retrieving TestmapDetail by ID.\", ID=\"{}\"", testmapId);
        List<TestmapDetail> testmapDetails = jpaApi.em()
                .createQuery(
                        "select tmd from TestmapDetail tmd where tmd.testmapId = :testmapId and tmd.testmapVersion = :testmapVersion",
                        TestmapDetail.class)
                .setParameter("testmapId", testmapId)
                .setParameter("testmapVersion", testmapVersion).getResultList();
        if (!testmapDetails.isEmpty()) {
            return Optional.of(testmapDetails.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetch an TestmapDetail by <code>testmapId</code>, <code>testmapName</code>and <code>testmapVersion</code>.
     *
     * @param testmapId      The Testmap Id
     * @param testmapVersion The Testmap Version
     * @param testmapName    The Testmap Name
     * @return The Optional of {@link TestmapDetail}
     */
    public Optional<TestmapDetail> findByTestmapIdNameAndVersion(int testmapId, final String testmapName, int testmapVersion) {
        Logger.debug("event=\"Retrieving TestmapDetail by testmap ID, Name and version.\", ID=\"{}\", Name=\"{}\", Version=\"{}\"", testmapId, testmapName, testmapVersion);
        List<TestmapDetail> testmapDetails = jpaApi.em()
                .createQuery("select tmd from TestmapDetail tmd " +
                                "where tmd.testmapId = :testmapId " +
                                "and tmd.testmapName = :testmapName " +
                                "and tmd.testmapVersion = :testmapVersion",
                        TestmapDetail.class)
                .setParameter("testmapId", testmapId)
                .setParameter("testmapName", testmapName)
                .setParameter("testmapVersion", testmapVersion).getResultList();
        if (!testmapDetails.isEmpty()) {
            return Optional.of(testmapDetails.get(0));
        } else {
            return Optional.empty();
        }
    }
}
