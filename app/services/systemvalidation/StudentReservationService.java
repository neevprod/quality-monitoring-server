package services.systemvalidation;

import models.systemvalidation.StudentReservation;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

/**
 * @author Nand Joshi
 */
public class StudentReservationService {
    private final JPAApi jpaApi;

    @Inject
    StudentReservationService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public void create(StudentReservation studentReservation) {
        StudentReservation managedEntity = jpaApi.em().merge(studentReservation);
        jpaApi.em().persist(managedEntity);
    }

    /**
     * Fetch the student reservation by given <code>uuid</code>.
     *
     * @param uuid The student UUID
     * @return The Optional of {@link StudentReservation}
     */
    public Optional<StudentReservation> findByUUID(final String uuid) {
        Logger.debug("event=\"Retrieving Student Reservation record by UUID.\", UUID=\"{}\"", uuid);
        final List<StudentReservation> studentReservations = jpaApi.em()
                .createQuery("select sr from StudentReservation sr where sr.uuid = :uuid", StudentReservation.class)
                .setParameter("uuid", uuid).getResultList();
        if (!studentReservations.isEmpty()) {
            return Optional.of(studentReservations.get(0));
        } else {
            return Optional.empty();
        }

    }

    /**
     * Deletes the <code>studentReservation</code> from database.
     *
     * @param studentReservation The {@link StudentReservation} to be deleted
     */
    public void delete(final StudentReservation studentReservation) {
        Logger.debug("event=\"Deleting student reservation record \", UUID=\"{}\"", studentReservation.getUuid());
        jpaApi.em().remove(studentReservation);
    }

    /**
     * Deletes the <code>studentReservation</code> from database for the given studentUUIDs.
     *
     * @param uuids The List of studentUuid
     */
    public void deleteByUuids(final List<String> uuids) {
        Logger.debug("event=\"Deleting student reservation records. \", UUIDs=\"{}\"", StringUtils.join(uuids, ", "));
        jpaApi.em().createQuery("DELETE StudentReservation sr where sr.uuid in (:uuids)")
                .setParameter("uuids", uuids)
                .executeUpdate();
    }

    /**
     * Gets a list of reserved student UUIDs.
     *
     * @param reservationExpireTime The {@link Long} reservation expiration time
     */
    public List<String> findReservedStudentUUIDs(final Long reservationExpireTime) {

        Timestamp cutOffTime = Timestamp.from(Instant.now().minus(reservationExpireTime, ChronoUnit.SECONDS));
        Timestamp sevenDayWindow = Timestamp.from(Instant.now().minus(7, ChronoUnit.DAYS));

        jpaApi.em()
                .createQuery(
                        "delete from StudentReservation sr where (sr.startTime < :cutOffTime and sr.studentScenarioId IS NULL) "
                                + "OR sr.studentScenarioId in (select ss.studentScenarioId from StudentScenario ss " +
                                "where ss.studentScenarioId = sr.studentScenarioId " +
                                "and ss.studentScenarioStatusId = (select sss.studentScenarioStatusId from StudentScenarioStatus sss where sss.status ='Completed')) " +
                                "OR sr.startTime < :sevenDayWindow")
                .setParameter("cutOffTime", cutOffTime)
                .setParameter("sevenDayWindow", sevenDayWindow).executeUpdate();

        return jpaApi.em().createQuery("select sr.uuid from StudentReservation sr", String.class).getResultList();
    }

    /**
     * Verifies if the reservation records for the provided list of student UUIDs have expired.
     *
     * @param uuids                     The List of student UUIDs to check.
     * @param studentReservationTimeOut The {@link Long} student reservation timeout.
     */
    public boolean isExpired(final List<String> uuids, final Long studentReservationTimeOut) {
        boolean expire = false;
        final List<StudentReservation> studentReservations = jpaApi.em()
                .createQuery("select sr from StudentReservation sr where sr.uuid in :uuids", StudentReservation.class)
                .setParameter("uuids", uuids).getResultList();

        if (studentReservations.isEmpty()) {
            expire = true;
        }
        for (StudentReservation sr : studentReservations) {
            Instant cutOff = sr.getStartTime().toInstant().plus(studentReservationTimeOut, ChronoUnit.MINUTES);
            if (cutOff.isBefore(Instant.now())) {
                expire = true;
            }
        }

        return expire;
    }
}
