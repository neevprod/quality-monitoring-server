/**
 * 
 */
package services.systemvalidation;

import java.util.List;
import java.util.Optional;

import models.systemvalidation.Environment;
import models.systemvalidation.TestSession;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;

/**
 * @author Gert Selis
 *
 */
public class TestSessionService {
    private final JPAApi jpaApi;

    @Inject
    TestSessionService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * @param testSession
     *            The TestSession.
     */
    public final void create(final TestSession testSession) {
        jpaApi.em().persist(testSession);
    }

    /**
     * @param testSessionId
     *            The TestSession Id.
     * @return The TestSession
     */
    public final Optional<TestSession> findByTestSessionId(final Long testSessionId) {
        Logger.info("event=\"Retrieving TestSession by ID.\", ID=\"{}\"", testSessionId);
        List<TestSession> testSessionList = jpaApi.em()
                .createQuery("select ts from test_session ts where ts.test_session_id = :testSessionId",
                        TestSession.class)
                .setParameter("testSessionId", testSessionId).getResultList();
        if (!testSessionList.isEmpty()) {
            return Optional.of(testSessionList.get(0));
        } else {
            return Optional.empty();
        }
    }


    /**
     *
     * @param environment The instance of {@link Environment}.
     * @param panTestSessionId The PA next session id
     * @return The optional of {@link TestSession}
     */
    public final Optional<TestSession> findByEnvironmentIdAndPANTestSessionId(final Environment environment,
            final String panTestSessionId) {
        Logger.info(
                "event=\"Retrieving TestSession by Environment and PA Next Test Session ID.\", Environment ID=\"{}\", PA Next Test Session ID=\"{}\"",
                environment.getEnvironmentId(), panTestSessionId);
        List<TestSession> testSessionList = jpaApi.em()
                .createQuery(
                        "select ts from TestSession ts where ts.environment = :environment and ts.panTestSessionId = :panTestSessionId",
                        TestSession.class)
                .setParameter("environment", environment).setParameter("panTestSessionId", panTestSessionId)
                .getResultList();
        if (!testSessionList.isEmpty()) {
            return Optional.of(testSessionList.get(0));
        } else {
            return Optional.empty();
        }
    }

}