/**
 * 
 */
package services.systemvalidation;

import java.util.List;

import javax.inject.Inject;

import models.systemvalidation.ApiConnection;
import models.systemvalidation.StudentScenarioResultCommentCategory;
import play.Logger;
import play.db.jpa.JPAApi;

/**
 * @author Gert Selis
 *
 */
public class StudentScenarioResultCommentCategoryService {
    private final JPAApi jpaApi;

    @Inject
    StudentScenarioResultCommentCategoryService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }
    /**
     * Fetch all API connection information.
     * 
     * @return The List of {@link ApiConnection}
     */
    public List<StudentScenarioResultCommentCategory> findAll() {
        Logger.debug("event=\"Retrieving Student Scenario Result Comment Categories.\"");
        return jpaApi.em().createQuery("select ssrcc from StudentScenarioResultCommentCategory ssrcc", StudentScenarioResultCommentCategory.class).getResultList();
    }
    
}
