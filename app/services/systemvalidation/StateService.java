package services.systemvalidation;

import com.google.inject.Inject;
import models.systemvalidation.State;
import play.db.jpa.JPAApi;

import java.util.List;
import java.util.Optional;

/**
 * Created by VJOSHNA on 4/21/2017.
 */
public class StateService {
    private final JPAApi jpaApi;

    @Inject
    public StateService(final JPAApi japApi) {
        this.jpaApi = japApi;
    }

    /**
     * Fetches the {@link State} filtered by stateId.
     * 
     * @param stateId
     *            The state ID
     * @return The Optional of {@link State}
     */
    public Optional<State> findByStateId(final int stateId) {
        List<State> states = jpaApi.em().createQuery("select s from State s where s.stateId = :stateId", State.class)
                .setParameter("stateId", stateId).getResultList();
        if (states.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(states.get(0));
    }

    /**
     * Fetches the {@link State} filtered by stateName.
     *
     * @param stateName
     *            The state name
     * @return The Optional of {@link State}
     */
    public Optional<State> findByStateName(final String stateName) {
        List<State> states = jpaApi.em()
                .createQuery("select s from State s where s.stateName = :stateName", State.class)
                .setParameter("stateName", stateName).getResultList();
        if (states.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(states.get(0));
    }
}
