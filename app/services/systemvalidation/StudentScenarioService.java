package services.systemvalidation;

import models.systemvalidation.StudentScenario;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Performs CRUD operations on student_scenario table.<br>
 * It contains the following methods:
 * <ul>
 * <li>{@link #create(StudentScenario)}</li>
 * <li>{@link #findAll()}</li>
 * <li>{@link #findByStudentScenarioId(Long)}</li>
 * </ul>
 *
 * @author Nand Joshi
 */
public class StudentScenarioService {
    private final JPAApi jpaApi;

    @Inject
    StudentScenarioService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Adds a new student scenario to the database.
     *
     * @param studentScenario The {@link StudentScenario}
     */
    public final void create(final StudentScenario studentScenario) {
        jpaApi.em().persist(studentScenario);
    }

    /**
     * Fetch all student scenario.
     *
     * @return The List of {@link StudentScenario}
     */
    public final List<StudentScenario> findAll() {
        Logger.info("event=\"Retrieving all studentScenarios.\"");
        return jpaApi.em()
                .createQuery("select s from StudentScenario s order by s.studentScenarioId", StudentScenario.class)
                .getResultList();
    }

    /**
     * Fetches the {@link StudentScenario} filtered by provided <code>studentScenarioId</code>.
     *
     * @param studentScenarioId The Long value of StudentScenarioId
     * @return The Optional of {@link StudentScenario}
     */
    public Optional<StudentScenario> findByStudentScenarioId(final Long studentScenarioId) {
        List<StudentScenario> studentScenarios = jpaApi.em()
                .createQuery("select s from StudentScenario s where s.studentScenarioId = :studentScenarioId",
                        StudentScenario.class)
                .setParameter("studentScenarioId", studentScenarioId).getResultList();
        if (studentScenarios.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(studentScenarios.get(0));
    }

    /**
     * Fetches the {@link StudentScenario} filtered by provided <code>studentScenarioId</code>,
     * <code>environmentId</code>, and <code>divJobExecId</code>.
     *
     * @param studentScenarioId The Long value of StudentScenarioId.
     * @param environmentId     The environment ID.
     * @param divJobExecId      The DIV job execution ID.
     * @return The Optional of {@link StudentScenario}
     */
    public final Optional<StudentScenario> findByStudentScenarioId(final Long studentScenarioId,
                                                                   final Integer environmentId, final Long divJobExecId) {
        List<StudentScenario> studentScenarios = jpaApi.em()
                .createQuery("select s from StudentScenario s "
                        + "where s.studentScenarioId = :studentScenarioId "
                        + "and s.divJobExec.environment.environmentId = :environmentId "
                        + "and s.divJobExec.divJobExecId = :divJobExecId", StudentScenario.class)
                .setParameter("studentScenarioId", studentScenarioId).setParameter("environmentId", environmentId)
                .setParameter("divJobExecId", divJobExecId).getResultList();
        if (studentScenarios.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(studentScenarios.get(0));
    }

    /**
     * Fetches the list of {@link StudentScenario} filtered by provided <code>divJobExecId</code> and
     * <code>environmentId</code>.
     *
     * @param divJobExecId  The Long value of divJobExecId.
     * @param environmentId The environment ID.
     * @return The List of {@link StudentScenario}.
     */
    public List<StudentScenario> findByDivJobExecId(Long divJobExecId, Integer environmentId) {
        return jpaApi.em()
                .createQuery(
                        "select s from StudentScenario s "
                                + "join fetch s.studentScenarioStatus sss "
                                + "left join fetch s.batteryStudentScenario bss "
                                + "join fetch s.divJobExec dje "
                                + "join fetch dje.path p "
                                + "left join fetch s.testnavScenario ts "
                                + "left join fetch ts.previewerTenant pt "
                                + "left join fetch pt.previewer "
                                + "where s.divJobExec.divJobExecId = :divJobExecId "
                                + "and s.divJobExec.environment.environmentId = :environmentId order by s.studentScenarioId",
                        StudentScenario.class)
                .setParameter("divJobExecId", divJobExecId).setParameter("environmentId", environmentId)
                .getResultList();

    }

    /**
     * Fetches the list of {@link StudentScenario} filtered by provided <code>divJobExecId</code>,
     * <code>batteryStudentScenarioId</code> and <code>environmentId</code>
     *
     * @param divJobExecId             The DIV job id
     * @param batteryStudentScenarioId The battery student scenario id
     * @param environmentId            The environment id
     * @return The List of {@link StudentScenario}
     */
    public List<StudentScenario> findByDivJobExecIdAndBatteryStudentScenarioId(final Long divJobExecId,
                                                                               final Long batteryStudentScenarioId, final int environmentId) {
        return jpaApi.em()
                .createQuery("select s from StudentScenario s "
                        + "left join fetch s.batteryStudentScenario bss "
                        + "join fetch s.divJobExec dje "
                        + "join fetch dje.path p "
                        + "join fetch s.studentScenarioStatus sst "
                        + "left join fetch s.testnavScenario ts "
                        + "left join fetch ts.previewerTenant pt "
                        + "left join fetch pt.previewer "
                        + "where s.divJobExec.divJobExecId = :divJobExecId "
                        + "and s.divJobExec.environment.environmentId = :environmentId "
                        + "and bss.batteryStudentScenarioId = :batteryStudentScenarioId", StudentScenario.class)
                .setParameter("divJobExecId", divJobExecId).setParameter("environmentId", environmentId)
                .setParameter("batteryStudentScenarioId", batteryStudentScenarioId).getResultList();
    }

    /**
     * Retrieves the most current STATS result detail, if available
     *
     * @param studentScenarioResultId The student scenario id
     * @return The stats result details
     */
    public List<String> getLastStatsResultDetail(final Long studentScenarioResultId) {
        return jpaApi.em().createNativeQuery("SELECT ssr.result_details " +
                "FROM student_scenario ss " +
                "     JOIN student_scenario_result ssr " +
                "        ON ssr.student_scenario_id = ss.student_scenario_id " +
                "     JOIN state s ON s.state_id = ssr.state_id " +
                "WHERE ss.student_scenario_id = :studentScenarioResultId " +
                "AND s.state_name = 'STATS' " +
                "ORDER BY ssr.version DESC " +
                "LIMIT 1")
                .setParameter("studentScenarioResultId", studentScenarioResultId)
                .getResultList();
    }
}