package services.systemvalidation;

import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Query;
import java.util.List;

public class ViewStatsFailureService {
    private final JPAApi jpaApi;

    @Inject
    ViewStatsFailureService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public List<Object[]> findStatsFailureReport(List<String> list) {

        Logger.debug("event=\"Retrieving Failed Stats Report for selected Div job ids.\"");

        String query = "SELECT a.DivJobId, " +
                "       a.BatteryId, " +
                "       a.StudentScenarioId, " +
                "       a.FormCode, " +
                "       a.Uuid, " +
                "       a.ResultDetails, " +
                "       max(a.StudentScenarioResultId) as StudentScenarioResultId " +
                "FROM (SELECT dje.div_job_exec_id AS DivJobId, " +
                "             ss.battery_student_scenario_id AS BatteryId, " +
                "             ss.student_scenario_id AS StudentScenarioId, " +
                "             sc.form_code AS FormCode, " +
                "             ss.uuid AS Uuid, " +
                "             ssr.result_details as ResultDetails, " +
                "             ssr.student_scenario_result_id as StudentScenarioResultId, " +
                "             ssr.state_id " +
                "      FROM div_job_exec dje " +
                "           JOIN student_scenario ss ON ss.div_job_exec_id = dje.div_job_exec_id " +
                "           JOIN student_scenario_result ssr ON ssr.student_scenario_id = ss.student_scenario_id " +
                "           JOIN state s ON s.state_id = ssr.state_id " +
                "           JOIN scenario sc ON sc.scenario_id = ss.testnav_scenario_id " +
                "      WHERE dje.div_job_exec_id IN (:div_job_exec_ids) " +
                "            AND s.state_name = 'STATS' " +
                "            AND ssr.success = 0) a " +
                "GROUP BY a.StudentScenarioId;";

        Query createQuery = jpaApi.em().createNativeQuery(query);

        createQuery.setParameter("div_job_exec_ids", list);
        return createQuery.getResultList();

    }
}
