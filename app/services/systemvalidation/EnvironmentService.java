package services.systemvalidation;

import models.systemvalidation.Environment;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @author Gert Selis
 */
public class EnvironmentService {
    private final JPAApi jpaApi;

    @Inject
    EnvironmentService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Saves the <code>environment</code> into database.
     *
     * @param environment The new instance of {@link Environment}
     */
    public void save(Environment environment) throws PersistenceException {
        jpaApi.em().persist(environment);
    }

    /**
     * Fetch an environment by its ID.
     *
     * @param environmentId The environment ID.
     * @return An Optional containing an environment.
     */
    public Optional<Environment> findById(int environmentId) {
        Logger.debug("event=\"Retrieving Environment Details by ID.\", ID=\"{}\"", environmentId);
        List<Environment> environmentList = new ArrayList<>();
        List<Environment> environments = jpaApi.em()
                .createQuery("select env from Environment env " + "left join fetch env.beApiConnection "
                        + "left join fetch env.dataWarehouseApiConnection " + "left join fetch env.feApiConnection "
                        + "left join fetch env.iceBridgeApiConnection " + "left join fetch env.irisApiConnection "
                        + "join fetch env.beMysqlConnection " + "join fetch env.dataWarehouseMongoConnection "
                        + "join fetch env.epen2MysqlConnection " + "left join fetch env.epen2ApiConnection "
                        + "join fetch env.feMysqlConnection " + "left join fetch env.iceBridgeOracleConnection "
                        + "join fetch env.irisMysqlConnection " + "join fetch env.epen2MongoConnection "
                        + "where env.environmentId = :environmentId", Environment.class)
                .setParameter("environmentId", environmentId).getResultList();
        environmentList.addAll(environments);
        if (!environmentList.isEmpty()) {
            return Optional.of(environmentList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetch all environments.
     *
     * @return A list of environments.
     */
    public List<Environment> findAll() {
        Logger.debug("event=\"Retrieving all environments.\"");

        return jpaApi.em()
                .createQuery("select e from Environment e " + "left join fetch e.beApiConnection "
                        + "left join fetch e.dataWarehouseApiConnection " + "left join fetch e.feApiConnection "
                        + "left join fetch e.iceBridgeApiConnection " + "left join fetch e.irisApiConnection "
                        + "join fetch e.beMysqlConnection " + "join fetch e.dataWarehouseMongoConnection "
                        + "join fetch e.epen2MysqlConnection " + "left join fetch e.epen2ApiConnection "
                        + "join fetch e.feMysqlConnection " + "left join fetch e.iceBridgeOracleConnection "
                        + "join fetch e.irisMysqlConnection " + "join fetch e.epen2MongoConnection "
                        + "order by e.name", Environment.class)
                .getResultList();
    }

    /**
     * Fetch all environments in the given set.
     *
     * @param environmentIds The set of environment IDs to filter by.
     * @return A list of environments.
     */
    public List<Environment> findAll(Set<Integer> environmentIds) {
        Logger.debug("event=\"Retrieving all environments in the given set.\"");

        if (!environmentIds.isEmpty()) {
            return jpaApi.em()
                    .createQuery("select e from Environment e " + "left join fetch e.beApiConnection "
                            + "left join fetch e.dataWarehouseApiConnection " + "left join fetch e.feApiConnection "
                            + "left join fetch e.iceBridgeApiConnection " + "left join fetch e.irisApiConnection "
                            + "join fetch e.beMysqlConnection " + "join fetch e.dataWarehouseMongoConnection "
                            + "join fetch e.epen2MysqlConnection " + "left join fetch e.epen2ApiConnection "
                            + "join fetch e.feMysqlConnection " + "left join fetch e.iceBridgeOracleConnection "
                            + "join fetch e.irisMysqlConnection " + "join fetch e.epen2MongoConnection "
                            + "where e.environmentId in :environmentIds " + "order by e.name", Environment.class)
                    .setParameter("environmentIds", environmentIds).getResultList();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Fetch all environments.
     *
     * @return A list of environments.
     */
    public List<Environment> findAllEnvironmentsWithDivJob() {
        Logger.debug("event=\"Retrieving all environments With Div Jobs Executions .\"");

        return jpaApi.em()
                .createQuery("select e from Environment e "
                        + " where e.archived = false and e.environmentId in (select distinct(dje.environment.environmentId) from DivJobExec dje)"
                        + " order by e.name", Environment.class)
                .getResultList();
    }

    /**
     * Fetch an environment by its ID.
     *
     * @param environmentId The environment ID.
     * @return An Optional containing an environment.
     */
    public Optional<Environment> findByIdWithDivJob(int environmentId) {
        Logger.debug("event=\"Retrieving Environment Details by ID With Div Jobs Executions.\", ID=\"{}\"",
                environmentId);

        List<Environment> EnvironmentList = jpaApi.em()
                .createQuery(
                        "select e from Environment e "
                                + " where e.environmentId in (select distinct(dje.environment.environmentId) from DivJobExec dje) and  e.environmentId=:environmentId",
                        Environment.class)
                .setParameter("environmentId", environmentId).getResultList();
        if (!EnvironmentList.isEmpty()) {
            return Optional.of(EnvironmentList.get(0));
        } else {
            return Optional.empty();
        }
    }

    public List<Environment> findAllByDbConnectionId(int dbConnectionId) {
        Logger.debug("event=\"Retrieving environments detail by dbConnectionId.\", dbConnectionId= \"{}\"", dbConnectionId);
        return jpaApi.em()
                .createQuery("select e from Environment e " +
                        "where e.feMysqlConnection.dbConnectionId = :dbConnectionId " +
                        "or e.beMysqlConnection.dbConnectionId = :dbConnectionId " +
                        "or e.irisMysqlConnection.dbConnectionId = :dbConnectionId " +
                        "or e.dataWarehouseMongoConnection.dbConnectionId = :dbConnectionId " +
                        "or e.epen2MysqlConnection.dbConnectionId = :dbConnectionId " +
                        "or e.epen2MongoConnection.dbConnectionId = :dbConnectionId ", Environment.class)
                .setParameter("dbConnectionId", dbConnectionId).getResultList();
    }
}
