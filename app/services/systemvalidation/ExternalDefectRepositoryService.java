/**
 * 
 */
package services.systemvalidation;

import java.util.List;

import javax.inject.Inject;

import models.systemvalidation.ApiConnection;
import models.systemvalidation.ExternalDefectRepository;
import play.Logger;
import play.db.jpa.JPAApi;

/**
 * @author Gert Selis
 *
 */
public class ExternalDefectRepositoryService {
    private final JPAApi jpaApi;

    @Inject
    ExternalDefectRepositoryService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Fetch all API connection information.
     * 
     * @return The List of {@link ApiConnection}
     */
    public List<ExternalDefectRepository> findAll() {
        Logger.debug("event=\"Retrieving External Defect Repositories.\"");
        return jpaApi.em().createQuery("select edr from ExternalDefectRepository edr", ExternalDefectRepository.class)
                .getResultList();
    }
}
