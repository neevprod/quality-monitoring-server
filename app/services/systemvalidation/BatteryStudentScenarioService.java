package services.systemvalidation;

import com.google.inject.Inject;
import models.systemvalidation.BatteryStudentScenario;
import play.db.jpa.JPAApi;

/**
 * Performs CRUD operations on battery_student_scenario database table.
 */
public class BatteryStudentScenarioService {
    private final JPAApi jpaApi;

    @Inject
    BatteryStudentScenarioService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Adds a new battery student scenario to the database.
     * 
     * @param batteryStudentScenario
     *            The new instance of {@link BatteryStudentScenario}
     */
    public void create(final BatteryStudentScenario batteryStudentScenario) {
        jpaApi.em().persist(batteryStudentScenario);
    }
}
