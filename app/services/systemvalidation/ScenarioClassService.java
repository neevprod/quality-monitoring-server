package services.systemvalidation;

import models.systemvalidation.ScenarioClass;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Performs CRUD operations on scenario_class table.<br>
 * It contains the following methods:
 * <ul>
 * <li>{@link #findAll()}</li>
 * </ul>
 *
 * @author Nand Joshi
 */
public class ScenarioClassService {
    private final JPAApi jpaApi;

    @Inject
    ScenarioClassService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Fetches the List of {@link ScenarioClass}.
     *
     * @return The List of {@link ScenarioClass}
     */
    public final List<ScenarioClass> findAll() {
        Logger.debug("event=\"Retrieving all scenario_class.\"");
        return jpaApi.em().createQuery("SELECT sc FROM ScenarioClass sc order by sc.scenarioClass", ScenarioClass.class).getResultList();
    }

    /**
     * Fetch all the scenario classes that are enabled either for TN8 or for ePEN.
     *
     * @return The List of {@link ScenarioClass}
     */
    public final List<ScenarioClass> findAllEnabled() {
        Logger.debug("event=\"Retrieving all scenario classes that are enabled for TN8 or ePEN.\"");
        return jpaApi.em().createQuery(
                "select sc from ScenarioClass sc " +
                        "where sc.enabledForTn8 = true or sc.enabledForEpen = true " +
                        " order by sc.scenarioClass",
                ScenarioClass.class)
                .getResultList();
    }

    public final Optional<ScenarioClass> findByScenarioClass(final String scenarioClass) {
        List<ScenarioClass> result = jpaApi.em()
                .createQuery("SELECT sc FROM ScenarioClass sc WHERE sc.scenarioClass = :scenarioClass",
                        ScenarioClass.class)
                .setParameter("scenarioClass", scenarioClass).getResultList();
        if (!result.isEmpty()) {
            return Optional.of(result.get(0));
        }
        return Optional.empty();
    }
}
