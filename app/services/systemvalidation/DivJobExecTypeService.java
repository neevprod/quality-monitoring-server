/**
 * 
 */
package services.systemvalidation;

import models.systemvalidation.DivJobExecType;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * @author Gert Selis
 *
 */
public class DivJobExecTypeService {
    private final JPAApi jpaApi;

    @Inject
    DivJobExecTypeService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * @param divJobExecTypeId
     *            The DivJobExecType Id.
     * @return The DivJobExecType
     */
    public final Optional<DivJobExecType> findByDivJobExecTypeId(final int divJobExecTypeId) {
        Logger.info("event=\"Retrieving DivJobExecType by divJobExecTypeId: \", divJobExecTypeId=\"{}\"",
                divJobExecTypeId);
        List<DivJobExecType> divJobExecTypeList = jpaApi.em()
                .createQuery("select djet from DivJobExecType djet where djet.divJobExecTypeId = :divJobExecTypeId",
                        DivJobExecType.class)
                .setParameter("divJobExecTypeId", divJobExecTypeId).getResultList();

        if (!divJobExecTypeList.isEmpty()) {
            return Optional.of(divJobExecTypeList.get(0));
        } else {
            return Optional.empty();
        }
    }
}