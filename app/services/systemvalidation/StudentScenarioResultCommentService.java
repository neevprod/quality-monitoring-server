package services.systemvalidation;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import models.systemvalidation.StudentScenarioResult;
import models.systemvalidation.StudentScenarioResultComment;
import play.Logger;
import play.db.jpa.JPAApi;

/**
 * @author Gert Selis
 *
 */
public class StudentScenarioResultCommentService {
    private final JPAApi jpaApi;

    @Inject
    StudentScenarioResultCommentService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Adds a new comment to the student_scenario_result_comment table
     * 
     * @param studentScenarioResultComment
     *            The {@link StudentScenarioResultComment}
     */
    public final void create(final StudentScenarioResultComment studentScenarioResultComment) {
        jpaApi.em().persist(studentScenarioResultComment);
    }

    /**
     * Fetch all comments for a given studentScenarioResultId
     * 
     * @return The List of {@link StudentScenarioResultComment}
     */
    public List<StudentScenarioResultComment> findAll(final Integer environmentId, final Long studentScenarioResultId) {
        Logger.debug("event=\"Retrieving Database Connections.\"");
        return jpaApi.em()
                .createQuery(
                        "select ssrc from StudentScenarioResultComment ssrc where ssrc.studentScenarioResult.studentScenarioResultId = :studentScenarioResultId and ssrc.studentScenarioResult.studentScenario.divJobExec.environment.environmentId = :environmentId",
                        StudentScenarioResultComment.class)
                .setParameter("studentScenarioResultId", studentScenarioResultId)
                .setParameter("environmentId", environmentId).getResultList();
    }

    /**
     * Fetch a StudentScenarioResultComment by its ID.
     * 
     * @param studentScenarioResultCommentId
     *            The studentScenarioResultComment id
     * @return The Optional of {@link StudentScenarioResultComment}
     */
    public Optional<StudentScenarioResultComment> findByStudentScenarioResultCommentId(final Integer environmentId,
            final Long studentScenarioResultCommentId) {
        Logger.debug("event=\"Retrieving StudentScenarioResultComment.\", studentScenarioResultCommentId=\"{}\"",
                studentScenarioResultCommentId);
        List<StudentScenarioResultComment> studentScenarioResultComments = jpaApi.em()
                .createQuery(
                        "select ssrc from StudentScenarioResultComment ssrc where ssrc.studentScenarioResultCommentId = :studentScenarioResultCommentId and ssrc.studentScenarioResult.studentScenario.divJobExec.environment.environmentId = :environmentId",
                        StudentScenarioResultComment.class)
                .setParameter("studentScenarioResultCommentId", studentScenarioResultCommentId)
                .setParameter("environmentId", environmentId).getResultList();
        if (!studentScenarioResultComments.isEmpty()) {
            return Optional.of(studentScenarioResultComments.get(0));
        } else {
            return Optional.empty();
        }
    }

}