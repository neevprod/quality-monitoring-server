/**
 * 
 */
package services.systemvalidation;

import models.systemvalidation.DivJobExecStatus;
import play.Logger;
import play.cache.SyncCacheApi;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * @author Gert Selis
 *
 */
public class DivJobExecStatusService {
    private final JPAApi jpaApi;
    private final SyncCacheApi cache;

    @Inject
    DivJobExecStatusService(JPAApi jpaApi, SyncCacheApi cache) {
        this.jpaApi = jpaApi;
        this.cache = cache;
    }

    /**
     * @param divJobExecStatusId
     *            The DivJobExecStatus Id.
     * @return The DivJobExecStatus
     */
    public Optional<DivJobExecStatus> findByDivJobExecStatusId(final int divJobExecStatusId) {
        Logger.info("event=\"Retrieving DivJobExecStatus by divJobExecStatusId: \", divJobExecStatusId=\"{}\"",
                divJobExecStatusId);
        List<DivJobExecStatus> divJobExecStatusList = jpaApi.em()
                .createQuery(
                        "select djes from DivJobExecStatus djes where djes.divJobExecStatusId = :divJobExecStatusId",
                        DivJobExecStatus.class)
                .setParameter("divJobExecStatusId", divJobExecStatusId).getResultList();

        if (!divJobExecStatusList.isEmpty()) {
            return Optional.of(divJobExecStatusList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * @param divJobExecStatusName
     *            The divJobExecStatus name.
     * @return The Optional of divJobExecStatus
     */
    public Optional<DivJobExecStatus> findByDivJobExecStatusName(final String divJobExecStatusName) {
        Logger.debug("event=\"Retrieving DivJobExecStatus by divJobExecStatusName: \", divJobExecStatusName=\"{}\"",
                divJobExecStatusName);
        return cache.getOrElseUpdate("divJobExecStatus." + divJobExecStatusName,
                () -> retrieveFromDbByDivJobExecStatusName(divJobExecStatusName));
    }

    private Optional<DivJobExecStatus> retrieveFromDbByDivJobExecStatusName(final String divJobExecStatusName) {
        Logger.info("event=\"Retrieving DivJobExecStatus from DB.\", divJobExecStatusName=\"{}\"",
                divJobExecStatusName);

        List<DivJobExecStatus> divJobExecStatusList = jpaApi.em()
                .createQuery("select djes from DivJobExecStatus djes where djes.status = :divJobExecStatusName",
                        DivJobExecStatus.class)
                .setParameter("divJobExecStatusName", divJobExecStatusName).getResultList();

        if (divJobExecStatusList.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(divJobExecStatusList.get(0));
    }
}