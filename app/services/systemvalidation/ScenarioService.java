package services.systemvalidation;

import models.systemvalidation.Scenario;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Gert Selis
 */
public class ScenarioService {
    private JPAApi jpaApi;

    @Inject
    ScenarioService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Creates a new scenario into database.
     *
     * @param scenario An instance of {@link Scenario}
     */
    public final void create(final Scenario scenario) {
        Logger.debug("event=\"Creating new Scenario\", \"ScenarioType =\"{}\", \"ScenarioClass =\"{}\"",
                scenario.getScenarioType().getScenarioType(), scenario.getScenarioClass().getScenarioClass());
        jpaApi.em().persist(scenario);
    }

    /**
     * Fetch the {@link Scenario} by <code>scenarioId</code>.
     *
     * @param scenarioId The Scenario Id.
     * @return The Optional of {@link Scenario}
     */

    public Optional<Scenario> findByScenarioId(final Integer scenarioId) {
        Logger.debug("event=\"Retrieving Scenario by ID.\", ID=\"{}\"", scenarioId);
        List<Scenario> scenarioList = jpaApi.em()
                .createQuery("select s from Scenario s " +
                        "where s.scenarioId = :scenarioId", Scenario.class)
                .setParameter("scenarioId", scenarioId).getResultList();
        if (!scenarioList.isEmpty()) {
            Scenario scenario = scenarioList.get(0);
            scenario.getTestmapDetail().getTestmapXml();
            scenario.getPreviewerTenant().getPreviewer().getApiUrl();
            return Optional.of(scenario);
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetch the TestNav scenario with the given student scenario ID, DIV job exec ID, and environment ID.
     *
     * @param environmentId     The environment ID.
     * @param divJobExecId      The DIV job exec ID.
     * @param studentScenarioId The student scenario ID.
     * @return An Optional containing the TestNav scenario.
     */
    public Optional<Scenario> findTestNavScenarioByStudentScenarioId(final int environmentId, final long divJobExecId,
                                                                     final long studentScenarioId) {
        Logger.debug(
                "event=\"Retrieving TestNav scenario by student scenario ID.\", environmentId=\"{}\", "
                        + "divJobExecId=\"{}\", studentScenarioId=\"{}\"",
                environmentId, divJobExecId, studentScenarioId);

        List<Scenario> scenarioList = jpaApi.em()
                .createQuery("select s from Scenario s, StudentScenario ss, DivJobExec dje "
                        + "left join fetch s.previewerTenant pt "
                        + "left join fetch pt.previewer "
                        + "left join fetch s.testmapDetail "
                        + "where s.scenarioId = ss.testnavScenario.scenarioId "
                        + "and ss.divJobExec.divJobExecId = dje.divJobExecId "
                        + "and dje.environment.environmentId = :environmentId "
                        + "and dje.divJobExecId = :divJobExecId "
                        + "and ss.studentScenarioId = :studentScenarioId", Scenario.class)
                .setParameter("environmentId", environmentId)
                .setParameter("divJobExecId", divJobExecId)
                .setParameter("studentScenarioId", studentScenarioId)
                .getResultList();
        if (!scenarioList.isEmpty()) {
            return Optional.of(scenarioList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetch the ePEN scenario with the given student scenario ID, DIV job exec ID, and environment ID.
     *
     * @param environmentId     The environment ID.
     * @param divJobExecId      The DIV job exec ID.
     * @param studentScenarioId The student scenario ID.
     * @return An Optional containing the ePEN scenario.
     */
    public Optional<Scenario> findEpenScenarioByStudentScenarioId(final int environmentId, final long divJobExecId,
                                                                  final long studentScenarioId) {
        Logger.debug(
                "event=\"Retrieving ePEN scenario by student scenario ID.\", environmentId=\"{}\", "
                        + "divJobExecId=\"{}\", studentScenarioId=\"{}\"",
                environmentId, divJobExecId, studentScenarioId);

        List<Scenario> scenarioList = jpaApi.em()
                .createQuery("select s from Scenario s, StudentScenario ss, DivJobExec dje "
                        + "where s.scenarioId = ss.epenScenario.scenarioId "
                        + "and ss.divJobExec.divJobExecId = dje.divJobExecId "
                        + "and dje.environment.environmentId = :environmentId "
                        + "and dje.divJobExecId = :divJobExecId "
                        + "and ss.studentScenarioId = :studentScenarioId", Scenario.class)
                .setParameter("environmentId", environmentId)
                .setParameter("divJobExecId", divJobExecId)
                .setParameter("studentScenarioId", studentScenarioId).getResultList();
        if (!scenarioList.isEmpty()) {
            return Optional.of(scenarioList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetch the List of {@link Scenario} by <code>testCode</code>, <code>formCode</code>, <code>scenarioTypeId</code>,
     * and the list of <code>scenarioClassIds</code>.
     *
     * @param testCode         The Test Code
     * @param formCode         The Form Code
     * @param scenarioTypeId   The Scenario Type Id
     * @param scenarioClassIds The List of Scenario Class Ids
     * @param scenarioNames    The List of scenario names
     * @return The List of {@link Scenario}
     */
    public List<Scenario> getScenarios(final String testCode, final String formCode, final int scenarioTypeId,
                                       final List<Integer> scenarioClassIds, List<String> scenarioNames) {
        Logger.debug(
                "event= \"Retrieving Scenario by testCode.\", testCode=\"{}\", formCode=\"{}\", "
                        + "scenarioTypeId=\"{}\", scenarioClassIds=\"{}\"",
                testCode, formCode, scenarioTypeId, scenarioClassIds.toString());

        List<Integer> scenarioIds = jpaApi.em()
                .createQuery(
                        "select max(sc.scenarioId) from Scenario sc "
                                + "where sc.scenarioType.scenarioTypeId = :scenarioTypeId "
                                + "and sc.scenarioClass.scenarioClassId IN (:scenarioClassId) "
                                + "and sc.scenarioName IN (:scenarioNames) "
                                + "and sc.testCode = :testCode "
                                + "and sc.formCode = :formCode group by sc.scenarioClass.scenarioClassId, sc.scenarioName", Integer.class)
                .setParameter("scenarioTypeId", scenarioTypeId)
                .setParameter("scenarioClassId", scenarioClassIds)
                .setParameter("scenarioNames", scenarioNames)
                .setParameter("testCode", testCode)
                .setParameter("formCode", formCode).getResultList();

        if (scenarioIds.isEmpty()) {
            scenarioIds = jpaApi.em().createQuery("select max(sc.scenarioId) from Scenario sc "
                            + "where sc.scenarioType.scenarioTypeId = :scenarioTypeId "
                            + "and sc.scenarioClass.scenarioClassId IN (:scenarioClassId) " + "and sc.testCode = :testCode "
                            + "and sc.formCode = :formCode group by sc.scenarioClass.scenarioClassId, sc.scenarioName",
                    Integer.class).setParameter("scenarioTypeId", scenarioTypeId)
                    .setParameter("scenarioClassId", scenarioClassIds).setParameter("testCode", testCode)
                    .setParameter("formCode", formCode).getResultList();
        }

        String sql = "select s from Scenario s " + "where s.scenarioType.scenarioTypeId = :scenarioTypeId "
                + "and s.scenarioClass.scenarioClassId IN (:scenarioClassId) " + "and s.testCode = :testCode "
                + "and s.formCode = :formCode ";
        if (!scenarioIds.isEmpty()) {
            sql += "and s.scenarioId in :maxScenarioId";
        }

        TypedQuery<Scenario> query = jpaApi.em().createQuery(sql, Scenario.class)
                .setParameter("scenarioTypeId", scenarioTypeId).setParameter("scenarioClassId", scenarioClassIds)
                .setParameter("testCode", testCode).setParameter("formCode", formCode);
        if (!scenarioIds.isEmpty()) {
            query.setParameter("maxScenarioId", scenarioIds);
        }
        List<Scenario> result = new ArrayList<>(query.getResultList());

        return result;

    }

    /**
     * Returns true if the Scenario is already created for given testCode, formCode, scenarioTypeId, scenarioClassId,
     * scenarioName, testmapId and testmapVersion otherwise false.
     *
     * @param testCode          The Test Code
     * @param formCode          The Form Code
     * @param scenarioTypeId    The Scenario Type Id
     * @param scenarioClassId   The Scenario Class Id
     * @param scenarioName      The Scenario Name
     * @param testmapId         The Testmap Id
     * @param testmapVersion    The Testmap Version
     * @param previewerTenantId The previewer tenant Id
     * @return true if the scenario exists in database otherwise false.
     */
    public boolean isActiveTestNavScenario(final String testCode, final String formCode, final int scenarioTypeId,
                                           final int scenarioClassId, final String scenarioName, final Integer testmapId, final int testmapVersion,
                                           final String scenario, final Long previewerTenantId) {
        Long count = jpaApi.em().createQuery("select count(s) from Scenario s "
                + "where s.scenario = :scenario "
                + "and s.previewerTenantId = :previewerTenantId "
                + "and s.testmapDetail.testmapId = :testmapId "
                + "and s.testmapDetail.testmapVersion = :testmapVersion "
                + "and s.scenarioId = ("
                + "select max(s.scenarioId) from Scenario s "
                + "where s.scenarioType.scenarioTypeId = :scenarioTypeId "
                + "and s.scenarioClass.scenarioClassId = :scenarioClassId "
                + "and s.scenarioName = :scenarioName "
                + "and s.testCode = :testCode "
                + "and s.formCode = :formCode "
                + ")", Long.class)
                .setParameter("scenario", scenario)
                .setParameter("previewerTenantId", previewerTenantId)
                .setParameter("testmapId", testmapId)
                .setParameter("testmapVersion", testmapVersion)
                .setParameter("scenarioTypeId", scenarioTypeId)
                .setParameter("scenarioClassId", scenarioClassId)
                .setParameter("scenarioName", scenarioName)
                .setParameter("testCode", testCode)
                .setParameter("formCode", formCode)
                .getResultList().get(0);

        return count > 0;

    }

    public Optional<Scenario> getActiveScenario(final String testCode, final String formCode, final int scenarioTypeId,
                                                final int scenarioClassId, final String scenarioName, final Integer testmapId, final int testmapVersion,
                                                final String scenario, final Long previewerTenantId) {
        List<Scenario> scenarios = jpaApi.em().createQuery("select s from Scenario s "
                + "where s.scenario = :scenario "
                + "and s.previewerTenantId = :previewerTenantId "
                + "and s.testmapDetail.testmapId = :testmapId "
                + "and s.testmapDetail.testmapVersion = :testmapVersion "
                + "and s.scenarioId = ("
                + "select max(s.scenarioId) from Scenario s "
                + "where s.scenarioType.scenarioTypeId = :scenarioTypeId "
                + "and s.scenarioClass.scenarioClassId = :scenarioClassId "
                + "and s.scenarioName = :scenarioName "
                + "and s.testCode = :testCode "
                + "and s.formCode = :formCode "
                + ")", Scenario.class)
                .setParameter("scenario", scenario).setParameter("previewerTenantId", previewerTenantId)
                .setParameter("testmapId", testmapId).setParameter("testmapVersion", testmapVersion)
                .setParameter("scenarioTypeId", scenarioTypeId).setParameter("scenarioClassId", scenarioClassId)
                .setParameter("scenarioName", scenarioName).setParameter("testCode", testCode)
                .setParameter("formCode", formCode).getResultList();

        if (scenarios.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(scenarios.get(0));

    }

    public boolean isActiveScenario(final String testCode, final String formCode, final int scenarioTypeId,
                                    final int scenarioClassId, final String scenarioName, final String scenario) {
        Long count = jpaApi.em()
                .createQuery("select count(s) from Scenario s "
                        + "where s.scenario = :scenario "
                        + "and s.scenarioId = ("
                        + "select max(s.scenarioId) from Scenario s "
                        + "where s.scenarioType.scenarioTypeId = :scenarioTypeId "
                        + "and s.scenarioClass.scenarioClassId = :scenarioClassId "
                        + "and s.testCode = :testCode "
                        + "and s.formCode = :formCode "
                        + "and s.scenarioName = :scenarioName"
                        + ")", Long.class)
                .setParameter("scenario", scenario)
                .setParameter("scenarioTypeId", scenarioTypeId)
                .setParameter("scenarioClassId", scenarioClassId)
                .setParameter("testCode", testCode)
                .setParameter("formCode", formCode)
                .setParameter("scenarioName", scenarioName)
                .getResultList().get(0);

        return count > 0;
    }

    public Optional<Scenario> getEpenActiveScenario(final String testCode, final String formCode, final int scenarioTypeId,
                                                    final int scenarioClassId, final String scenarioName, final String scenario) {
        List<Scenario> result = jpaApi.em()
                .createQuery("select s from Scenario s "
                        + "where s.scenario = :scenario "
                        + "and s.scenarioId = ("
                        + "select max(s.scenarioId) from Scenario s "
                        + "where s.scenarioType.scenarioTypeId = :scenarioTypeId "
                        + "and s.scenarioClass.scenarioClassId = :scenarioClassId "
                        + "and s.testCode = :testCode "
                        + "and s.formCode = :formCode "
                        + "and s.scenarioName = :scenarioName"
                        + ")", Scenario.class)
                .setParameter("scenario", scenario)
                .setParameter("scenarioTypeId", scenarioTypeId)
                .setParameter("scenarioClassId", scenarioClassId).setParameter("testCode", testCode)
                .setParameter("formCode", formCode).setParameter("scenarioName", scenarioName).getResultList();

        if (result.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(result.get(0));
    }
}