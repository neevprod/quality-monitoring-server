package services.systemvalidation;

import models.systemvalidation.ScenarioType;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * @author Nand Joshi
 *
 */
public class ScenarioTypeService {
    private final JPAApi jpaApi;

    @Inject
    ScenarioTypeService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Gets an instance of {@link ScenarioType} by scenarioType.
     * 
     * @param scenarioType
     *            The ScenarioType
     * @return The optional of {@link ScenarioType}
     */
    public final Optional<ScenarioType> findByScenarioType(final String scenarioType) {
        Logger.debug("event=\"Retrieving Scenario type by scenarioType.\", ID=\"{}\"", scenarioType);
        List<ScenarioType> scenarioTypes = jpaApi.em()
                .createQuery(
                        "select st from ScenarioType st where st.scenarioType = :scenarioType",
                        ScenarioType.class)
                .setParameter("scenarioType", scenarioType).getResultList();
        if (!scenarioTypes.isEmpty()) {
            return Optional.of(scenarioTypes.get(0));
        } else {
            return Optional.empty();
        }
    }
}
