package services.systemvalidation;

import models.systemvalidation.Path;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * @author Tarun Gupta
 */
public class PathService {
    private final JPAApi jpaApi;

    @Inject
    PathService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * @param pathId The Path Id.
     * @return The Optional of Path
     */
    public final Optional<Path> findByPathId(final int pathId) {
        Logger.info("event=\"Retrieving path. \", pathId=\"{}\"", pathId);
        List<Path> pathList = jpaApi.em().createQuery("select p from Path p where p.pathId = :pathId", Path.class)
                .setParameter("pathId", pathId).getResultList();

        if (pathList.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(pathList.get(0));
    }

    public int findByTestTypeName(final String testType) {
        Logger.debug("event= \"Retrieving path.\", testType= \"{}\"", testType);
        List<Integer> testTypeIds = jpaApi.em()
                .createNativeQuery("SELECT  tt.test_type_id FROM test_type tt WHERE tt.name = :testType")
                .setParameter("testType", testType).getResultList();
        if (null != testTypeIds && !testTypeIds.isEmpty()) {
            return testTypeIds.get(0);
        }
        return 0;
    }


    public final int findPathId(final List<Integer> selectedStates, final List<Integer> notSelectedStates) {
        Logger.debug("event=\"Retrieving Path Id from the DB.\", selectedStates=\"{}\", notSelectedStates=\"{}\"",
                selectedStates, notSelectedStates);

        String query = "SELECT y.path_id"
                + " FROM (SELECT COUNT(x.path_id) selectedStates, x.path_id"
                + " FROM (SELECT ps.path_id FROM path_state ps WHERE ps.state_id IN (:selectedStates) AND ps.path_id NOT IN (SELECT ps.path_id FROM path_state ps"
                + " WHERE ps.state_id IN (:notSelectedStates))) x " + " GROUP BY path_id) y"
                + " WHERE y.selectedStates =:count";

        Query createQuery = jpaApi.em().createNativeQuery(query);

        createQuery.setParameter("selectedStates", selectedStates).setParameter("notSelectedStates", notSelectedStates)
                .setParameter("count", selectedStates.size());

        List<Integer> pathIds = createQuery.getResultList();
        if (pathIds != null && !pathIds.isEmpty()) {
            return pathIds.get(0);
        }
        return 0;
    }

    public List<Object[]> findByTestTypeId(int testTypeId) {
        Logger.debug("event= \"Retrieving path state for the given testTypeId\", testTypeId= \"{}\"", testTypeId);
        return jpaApi
                .em()
                .createNativeQuery(
                        "SELECT s.state_Id, s.state_name FROM State s "
                                + "JOIN Path_State ps ON ps.state_Id= s.state_Id AND ps.path_Id=:testTypeId ORDER BY ps.path_State_Order")
                .setParameter("testTypeId", testTypeId).getResultList();

    }
}