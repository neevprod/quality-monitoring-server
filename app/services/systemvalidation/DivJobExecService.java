package services.systemvalidation;

import models.systemvalidation.DivJobExec;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;

/**
 * @author Gert Selis
 */
public class DivJobExecService {
    private JPAApi jpaApi;

    @Inject
    DivJobExecService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public void createDivJobExec(final DivJobExec divJobExec) {
        jpaApi.em().persist(divJobExec);
    }

    /**
     * Fetch all DIV job executions. The environment, submitUser, and divJobExecStatus are eagerly fetched.
     *
     * @return The list of DivJobExecs.
     */
    public List<DivJobExec> findAll() {
        Logger.debug("event=\"Retrieving all DIV job executions from the DB.\"");
        return jpaApi.em()
                .createQuery("select dje from DivJobExec dje " +
                                "join fetch dje.environment " +
                                "join fetch dje.submitUser " +
                                "join fetch dje.divJobExecStatus " +
                                "order by dje.divJobExecId",
                        DivJobExec.class)
                .getResultList();
    }

    /**
     * Fetch a DIV job execution by its ID. The studentScenarios, path, and environment are eagerly fetched.
     *
     * @param divJobExecId The DivJobExec Id.
     * @return The DivJobExec
     */
    public Optional<DivJobExec> findByDivJobExecId(final Long divJobExecId) {
        Logger.debug("event=\"Retrieving Data Integrity Validation Job Execution by ID.\", ID=\"{}\"", divJobExecId);

        List<DivJobExec> divJobExecList = jpaApi.em()
                .createQuery("select dje from DivJobExec dje " +
                                "join fetch dje.divJobExecStatus status  " +
                                "join fetch dje.studentScenarios ss  " +
                                "left join fetch ss.batteryStudentScenario " +
                                "join fetch dje.path " +
                                "join fetch dje.environment env " +
                                "left join fetch env.beApiConnection " +
                                "left join fetch env.dataWarehouseApiConnection " +
                                "left join fetch env.feApiConnection " +
                                "left join fetch env.iceBridgeApiConnection " +
                                "left join fetch env.irisApiConnection " +
                                "join fetch env.beMysqlConnection " +
                                "join fetch env.dataWarehouseMongoConnection " +
                                "join fetch env.epen2MysqlConnection " +
                                "join fetch env.feMysqlConnection " +
                                "left join fetch env.iceBridgeOracleConnection " +
                                "join fetch env.irisMysqlConnection " +
                                "join fetch env.epen2MongoConnection " +
                                "left join fetch env.epen2ApiConnection " +
                                "where dje.divJobExecId = :divJobExecId",
                        DivJobExec.class)
                .setParameter("divJobExecId", divJobExecId).getResultList();
        if (!divJobExecList.isEmpty()) {
            return Optional.of(divJobExecList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetches the list of {@link DivJobExec} filtered by <code>environmentId</code>
     *
     * @param environmentId The environment Id
     * @return The List of {@link DivJobExec}
     */
    public List<DivJobExec> findByEnvironmentId(final Integer environmentId) {
        Logger.debug("event=\"Retrieving all DIV job executions from the DB.\",\"environmentId\"=\"{}\"",
                environmentId);
        return jpaApi.em()
                .createQuery("select dje from DivJobExec dje " +
                                "join fetch dje.environment " +
                                "join fetch dje.submitUser " +
                                "join fetch dje.divJobExecStatus "
                                + "where dje.environment.environmentId = :environmentId " +
                                "order by dje.divJobExecId",
                        DivJobExec.class)
                .setParameter("environmentId", environmentId)
                .getResultList();
    }

    /**
     * Fetch a DIV job execution by the given environment ID and DIV job exec ID.
     *
     * @param environmentId The environment ID.
     * @param divJobExecId  The DIV job exec ID.
     * @return An Optional containing a DIV job execution.
     */
    public Optional<DivJobExec> find(final Integer environmentId, final Long divJobExecId) {
        Logger.debug("event=\"Retrieving DIV job execution from the DB.\", environmentId=\"{}\", divJobExecId=\"{}\"",
                environmentId, divJobExecId);

        List<DivJobExec> divJobExecutions = jpaApi.em()
                .createQuery("select dje from DivJobExec dje " +
                                "where dje.environment.environmentId = :environmentId " +
                                "and dje.divJobExecId = :divJobExecId",
                        DivJobExec.class)
                .setParameter("environmentId", environmentId)
                .setParameter("divJobExecId", divJobExecId)
                .getResultList();
        if (!divJobExecutions.isEmpty()) {
            return Optional.of(divJobExecutions.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Fetch a DIV job execution status by its ID.
     *
     * @param divJobExecId The DivJobExec Id.
     * @return The DivJobExec
     */
    public Optional<DivJobExec> findStatusByDivJobExecId(final Long divJobExecId) {
        Logger.debug("event=\"Retrieving DIV job execution status\", ID=\"{}\"", divJobExecId);

        List<DivJobExec> divJobExecList = jpaApi
                .em()
                .createQuery(
                        "select dje from DivJobExec dje " + "join fetch dje.environment env "
                                + "join fetch dje.divJobExecStatus " + "where dje.divJobExecId = :divJobExecId",
                        DivJobExec.class).setParameter("divJobExecId", divJobExecId).getResultList();
        if (!divJobExecList.isEmpty()) {
            return Optional.of(divJobExecList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Retrieve student scenario counts by status for a given list of DivJobExec ids.
     *
     * @param divJobExecIds The list of DivJobExec Ids.
     * @return List of Tuples containing the test type, student scenario counts by status and DivJobExec status
     */
    public List<Tuple> getDivJobStudentScenarioStatusCounts(final List<Long> divJobExecIds) {
        return jpaApi.em().createNativeQuery("SELECT x.div_job_exec_id, " +
                "       x.name AS testType, " +
                "       SUM(x.completedOeExcluded) AS completedOeExcluded, " +
                "       SUM(x.completedOeOpExcluded) AS completedOeOpExcluded, " +
                "       SUM(x.completedOeFtExcluded) AS completedOeFtExcluded, " +
                "       SUM(x.completed) AS completed, " +
                "       SUM(x.failed) AS failed, " +
                "       SUM(x.inProgress) AS inProgress, " +
                "       SUM(x.notStarted) AS notStarted, " +
                "       x.status AS divJobStatus " +
                "FROM (SELECT ss.div_job_exec_id, " +
                "             t.name, " +
                "             sum(CASE WHEN sss.status = 'Completed (OE Excluded)' THEN 1 ELSE 0 END) AS completedOeExcluded, " +
                "             sum(CASE WHEN sss.status = 'Completed (OE-OP Excluded)' THEN 1 ELSE 0 END) AS completedOeOpExcluded," +
                "             sum(CASE WHEN sss.status = 'Completed (OE-FT Excluded)' THEN 1 ELSE 0 END) AS completedOeFtExcluded," +
                "             sum(CASE WHEN sss.status = 'Completed' THEN 1 ELSE 0 END) AS completed, " +
                "             sum(CASE WHEN sss.status = 'Failed' THEN 1 ELSE 0 END) AS failed, " +
                "             sum(CASE WHEN sss.status = 'In Progress' THEN 1 ELSE 0 END) AS inProgress, " +
                "             sum(CASE WHEN sss.status = 'Not Started' THEN 1 ELSE 0 END) AS notStarted, " +
                "             ds.status " +
                "      FROM student_scenario ss " +
                "           JOIN student_scenario_status sss " +
                "              ON ss.student_scenario_status_id = " +
                "                 sss.student_scenario_status_id " +
                "           JOIN div_job_exec d ON d.div_job_exec_id = ss.div_job_exec_id " +
                "           JOIN test_type t ON t.test_type_id = d.test_type_id " +
                "           JOIN div_job_exec_status ds " +
                "              ON ds.div_job_exec_status_id = d.div_job_exec_status_id " +
                "      WHERE ss.div_job_exec_id IN :divJobExecIds " +
                "      GROUP BY ss.div_job_exec_id, sss.status) x " +
                "GROUP BY x.div_job_exec_id", Tuple.class)
                .setParameter("divJobExecIds", divJobExecIds)
                .getResultList();
    }
}