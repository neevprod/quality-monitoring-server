package services.systemvalidation;

import models.systemvalidation.DbConnection;
import models.systemvalidation.DbConnectionDto;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Nand Joshi
 */
public class DbConnectionService {
    private final JPAApi jpaApi;

    @Inject
    DbConnectionService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Inserts a <code>dbConnection</code> into database.
     *
     * @param dbConnection The instance of {@link DbConnection}
     */
    public void save(DbConnection dbConnection) {
        jpaApi.em().persist(dbConnection);

    }

    /**
     * Fetch all Database connection information.
     *
     * @return The List of {@link DbConnectionDto}
     */
    public List<DbConnectionDto> findAll() {
        Logger.debug("event=\"Retrieving Database Connections.\"");
        List<DbConnection> dbConnections = jpaApi.em()
                .createQuery("select dbc from DbConnection dbc order by dbc.host", DbConnection.class).getResultList();
        return dbConnections.stream().map(dbc -> getDbConnectionDto(dbc)).collect(Collectors.toList());

    }

    private DbConnectionDto getDbConnectionDto(DbConnection dbc) {
        final DbConnectionDto dto = new DbConnectionDto();
        dto.setDbConnectionId(dbc.getDbConnectionId());
        dto.setDbName(dbc.getDbName());
        dto.setDbPass(dbc.getDbPass());
        dto.setDbUser(dbc.getDbUser());
        dto.setHost(dbc.getHost());
        dto.setPort(dbc.getPort());
        return dto;
    }

    /**
     * Fetch a database connection by its ID.
     *
     * @param dbConnectionId The database connection id
     * @return The Optional of {@link DbConnection}
     */
    public Optional<DbConnection> findByDbConnectionId(final Integer dbConnectionId) {
        Logger.debug("event=\"Retrieving Database Connection.\", dbConnectionId=\"{}\"", dbConnectionId);
        List<DbConnection> dbConnections = jpaApi.em()
                .createQuery("select dbc from DbConnection dbc where dbc.dbConnectionId = :dbConnectionId",
                        DbConnection.class)
                .setParameter("dbConnectionId", dbConnectionId).getResultList();
        if (!dbConnections.isEmpty()) {
            return Optional.of(dbConnections.get(0));
        } else {
            return Optional.empty();
        }
    }
}