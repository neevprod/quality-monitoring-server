package services.systemvalidation;

import models.systemvalidation.Environment;
import models.systemvalidation.StudentScenarioResult;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Executes CRUD operations for student_scenario_result table.<br>
 * It contains the following methods:
 * <ul>
 * <li>{@link #create(StudentScenarioResult)}</li>
 * </ul>
 *
 * @author Nand Joshi
 */
public class StudentScenarioResultService {
    private final JPAApi jpaApi;

    @Inject
    StudentScenarioResultService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Adds a new student scenario to the student_scenario_result table
     *
     * @param studentScenarioResult The {@link StudentScenarioResult}
     */
    public void create(final StudentScenarioResult studentScenarioResult) {
        jpaApi.em().persist(studentScenarioResult);
    }

    public Optional<Integer> findMaxVersionByStudentScenarioIdAndStateId(final Long studentScenarioId,
                                                                         final int stateId) {
        Logger.debug(
                "event=\"Retrieving StudentScenarioResult by studentScenarioId  and stateId: \", studentScenarioId=\"{}\", stateId=\"{}\"",
                studentScenarioId, stateId);

        List<Integer> studentScenarioResults = jpaApi.em()
                .createQuery(
                        "select max(sr.version) from StudentScenarioResult sr where sr.studentScenarioId = :studentScenarioId and sr.stateId = :stateId",
                        Integer.class)
                .setParameter("studentScenarioId", studentScenarioId).setParameter("stateId", stateId).getResultList();

        if (!studentScenarioResults.isEmpty()) {
            return Optional.ofNullable(studentScenarioResults.get(0));
        } else {
            return Optional.empty();
        }

    }

    /**
     * Retrieves the list of {@link StudentScenarioResult} filtered by <code>studentScenarioId</code>.
     *
     * @param studentScenarioId The studentScenarioId
     * @return The List of {@link StudentScenarioResult}
     */
    public final List<StudentScenarioResult> findByStudentScenarioId(final Long studentScenarioId) {
        Logger.debug("event=\"Retrieving StudentScenarioResult by studentScenarioId.\", \"StudentScenarioId={}\"",
                studentScenarioId);
        jpaApi.em().createQuery("select e from Environment e", Environment.class).getResultList();
        return jpaApi.em()
                .createQuery(
                        "select ssr from StudentScenarioResult ssr " + "join fetch ssr.studentScenario "
                                + "join fetch ssr.state s " + "left join fetch ssr.studentScenarioResultComments ssrc "
                                + "where ssr.studentScenario.studentScenarioId = :studentScenarioId",
                        StudentScenarioResult.class)
                .setParameter("studentScenarioId", studentScenarioId).getResultList();
    }

    /**
     * Retrieves the list of {@link StudentScenarioResult} filtered by <code>studentScenarioId</code>,
     * <code>stateId</code>, and <code>environmentId</code>.
     *
     * @param studentScenarioId The studentScenarioId.
     * @param stateId           The stateId.
     * @param environmentId     The environment ID.
     * @return The List of {@link StudentScenarioResult}
     */
    public List<StudentScenarioResult> findByStudentScenarioIdAndStateId(Long studentScenarioId, Integer stateId,
                                                                         Integer environmentId) {
        Logger.debug("event=\"Retrieving Student Scenario Result.\", \"StudentScenarioId={}\", \"stateId={}\"",
                studentScenarioId, stateId);
        jpaApi.em().createQuery("select e from Environment e", Environment.class).getResultList();
        return jpaApi.em()
                .createQuery("select distinct ssr from StudentScenarioResult ssr " + "join fetch ssr.studentScenario "
                        + "join fetch ssr.state s " + "left join fetch ssr.studentScenarioResultComments ssrc "
                        + "where ssr.studentScenario.studentScenarioId = :studentScenarioId "
                        + "and ssr.stateId = :stateId "
                        + "and ssr.studentScenario.divJobExec.environment.environmentId = :environmentId "
                        + "order by ssr.version", StudentScenarioResult.class)
                .setParameter("studentScenarioId", studentScenarioId).setParameter("stateId", stateId)
                .setParameter("environmentId", environmentId).getResultList();
    }

    /**
     * Retrieves the list of {@link StudentScenarioResult} filtered by <code>studentScenarioId</code> and
     * <code>stateName</code>.
     *
     * @param studentScenarioId The studentScenarioId.
     * @param stateName         The stateName.
     * @return The List of {@link StudentScenarioResult}
     */
    public List<StudentScenarioResult> findByStudentScenarioIdAndStateName(Long studentScenarioId, String stateName) {
        Logger.debug("event=\"Retrieving Student Scenario Result.\", \"StudentScenarioId={}\", \"stateName={}\"",
                studentScenarioId, stateName);
        jpaApi.em().createQuery("select e from Environment e", Environment.class).getResultList();
        return jpaApi.em()
                .createQuery(
                        "select ssr from StudentScenarioResult ssr "
                                + "join fetch ssr.studentScenario ss "
                                + "join fetch ssr.state s "
                                + "where ssr.studentScenario.studentScenarioId = :studentScenarioId "
                                + "and ssr.state.stateName = :stateName " + "order by ssr.version",
                        StudentScenarioResult.class)
                .setParameter("studentScenarioId", studentScenarioId).setParameter("stateName", stateName)
                .getResultList();
    }

    /**
     * Retrieves the list of {@link StudentScenarioResult} for the BATTERY_STATE for the given studentScenario filtered
     * by <code>studentScenarioId</code>, <code>stateId</code>, and <code>environmentId</code>.
     *
     * @param studentScenarioId The studentScenarioId.
     * @param stateId           The stateId.
     * @param environmentId     The environment ID.
     * @return The List of {@link StudentScenarioResult}
     */
    public List<StudentScenarioResult> findByStudentScenarioIdAndStateIdFromBattery(Long studentScenarioId,
                                                                                    Integer stateId, Integer environmentId) {
        Logger.debug("event=\"Retrieving Student Scenario Result.\", \"StudentScenarioId={}\", \"stateId={}\"",
                studentScenarioId, stateId);
        jpaApi.em().createQuery("select e from Environment e", Environment.class).getResultList();
        return jpaApi.em()
                .createQuery("select distinct ssr from StudentScenarioResult ssr "
                        + "join fetch ssr.studentScenario ss " + "left join fetch ss.batteryStudentScenario bss "
                        + "join fetch ssr.state s " + "left join fetch ssr.studentScenarioResultComments ssrc "
                        + "where bss.batteryStudentScenarioId in "
                        + "(select stdScenario.batteryStudentScenario.batteryStudentScenarioId from StudentScenario  stdScenario where "
                        + "stdScenario.studentScenarioId =" + " :studentScenarioId) " + "and ssr.stateId = :stateId "
                        + "and ssr.studentScenario.divJobExec.environment.environmentId = :environmentId "
                        + "order by ssr.version ", StudentScenarioResult.class)
                .setParameter("studentScenarioId", studentScenarioId).setParameter("stateId", stateId)
                .setParameter("environmentId", environmentId).getResultList();
    }
}
