package services.systemvalidation;

import java.util.List;

import javax.inject.Inject;

import models.systemvalidation.PathState;
import play.Logger;
import play.db.jpa.JPAApi;

/**
 * Performs CRUD operations on path_state table.<br>
 * It contains the following methods:
 * <ul>
 * <li>{@link #findByPathId(int)}</li>
 * </ul>
 * 
 * @author Nand Joshi
 *
 */
public class PathStateService {
    private final JPAApi jpaApi;

    @Inject
    PathStateService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Fetches the List of {@link PathState} for the given <code>pathId</code>.
     * 
     * @param pathId
     *            The Path Id.
     * @return The List of {@link PathState}
     */
    public final List<PathState> findByPathId(final int pathId) {
        Logger.info("event=\"Retrieving all PathStates.\", \"pathId={}\"", pathId);
        return jpaApi.em()
                .createQuery(
                        "SELECT ps FROM PathState ps "
                                + "JOIN fetch ps.state s WHERE ps.path.pathId = :pathId ORDER BY ps.pathStateOrder ASC",
                        PathState.class)
                .setParameter("pathId", pathId).getResultList();
    }
}
