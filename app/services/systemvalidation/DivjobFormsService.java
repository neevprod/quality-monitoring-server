package services.systemvalidation;

import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Query;
import java.util.List;

/**
 * @author Gert Selis
 */
public class DivjobFormsService {
    private final JPAApi jpaApi;

    @Inject
    DivjobFormsService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Retrieve forms for all DIV jobs in the specified environment
     *
     * @return List of DIV job forms
     */
    public List<Object[]> getDivjobForms(Integer environmentId) {
        Logger.debug("event=\"Retrieving all Test Attempts for Form.\"");
        String query = "SELECT sub.div_job_exec_id,"
                + " sub.test_code,"
                + " sub.form_code,"
                + " GROUP_CONCAT(sub.scenario_class) scenarios"
                + " FROM (SELECT DISTINCT dje.div_job_exec_id, s.test_code, s.form_code, sc.scenario_class"
                + " FROM environment e"
                + " JOIN div_job_exec dje ON dje.environment_id = e.environment_id"
                + " JOIN student_scenario ss"
                + " ON ss.div_job_exec_id = dje.div_job_exec_id"
                + " JOIN scenario s ON s.scenario_id = ss.testnav_scenario_id"
                + " JOIN scenario_class sc"
                + " ON sc.scenario_class_id = s.scenario_class_id"
                + " WHERE e.environment_id = :environmentId"
                + " ORDER BY dje.div_job_exec_id, s.form_code) sub"
                + " GROUP BY sub.div_job_exec_id, sub.form_code"
                + " ORDER BY sub.div_job_exec_id, sub.form_code";
        Query createQuery = jpaApi.em().createNativeQuery(query);
        createQuery.setParameter("environmentId", environmentId);
        return createQuery.getResultList();
    }
}