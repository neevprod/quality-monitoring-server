package services.systemvalidation;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import models.systemvalidation.EpenUserReservation;
import play.Logger;
import play.db.jpa.JPAApi;

/**
 * Created by SELIGE on 6/6/2017.
 */
public class EpenUserReservationService {
    private final JPAApi jpaApi;

    @Inject
    EpenUserReservationService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    private EpenUserReservation create() {
        EpenUserReservation epenUserReservation = new EpenUserReservation();
        epenUserReservation.setLockedTimestamp(Timestamp.from(Instant.now()));
        epenUserReservation.setLocked(true);
        jpaApi.em().persist(epenUserReservation);
        jpaApi.em().flush();
        return epenUserReservation;
    }

    public Optional<EpenUserReservation> findNextAvailable() {
        Logger.debug("event=\"Retrieving next available ePEN User.\"");

        final Long lockExpirationTime = 1440L;
        Timestamp lockExpirationTimeStamp = Timestamp.from(Instant.now().minus(lockExpirationTime, ChronoUnit.MINUTES));

        final List<EpenUserReservation> ePenUserReservations = jpaApi.em()
                .createQuery(
                        "select eur from EpenUserReservation eur where eur.locked = false or eur.lockedTimestamp <= :lockExpirationTimeStamp",
                        EpenUserReservation.class)
                .setParameter("lockExpirationTimeStamp", lockExpirationTimeStamp).getResultList();

        if (ePenUserReservations.isEmpty()) {
            return Optional.of(create());
        } else {
            ePenUserReservations.get(0).setLocked(true);
            ePenUserReservations.get(0).setLockedTimestamp(Timestamp.from(Instant.now()));
            return Optional.of(ePenUserReservations.get(0));
        }
    }

    public void unlock(int epenUserReservationId) {
        Optional<EpenUserReservation> ePenUserReservation = getByEpenUserReservationId(epenUserReservationId);
        if (ePenUserReservation.isPresent()) {
            ePenUserReservation.get().setLocked(false);
        }
    }

    private Optional<EpenUserReservation> getByEpenUserReservationId(int epenUserReservationId) {
        final List<EpenUserReservation> ePenUserReservations = jpaApi.em()
                .createQuery("select eur from EpenUserReservation eur where eur.id = :epenUserReservationId",
                        EpenUserReservation.class)
                .setParameter("epenUserReservationId", epenUserReservationId).getResultList();
        return Optional.of(ePenUserReservations.get(0));
    }
}