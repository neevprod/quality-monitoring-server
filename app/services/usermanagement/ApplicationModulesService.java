package services.usermanagement;

import models.ApplicationModule;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by VJOSHNA on 11/3/2016.
 */
public class ApplicationModulesService {
    private final JPAApi jpaApi;

    @Inject
    public ApplicationModulesService(final JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Retrieves the list of all application modules.
     * 
     * @return The list of {@link ApplicationModule}
     */
    public List<ApplicationModule> findAll() {
        Logger.debug("event=\"Retrieving ApplicationModules from database\"");
        return jpaApi.em().createQuery("select am from ApplicationModule am join fetch am.permissionType",
                ApplicationModule.class).getResultList();
    }

    /**
     * Retrieves the list of application modules which has at least one role is assigned
     *
     * @return The List of {@link ApplicationModule}
     */

    public List<ApplicationModule> findApplicationModules() {
        Logger.debug("event=\"Retrieving all application modules which has permission settings.\"");
        return jpaApi.em()
                .createQuery(
                        "select distinct ap from PermissionSettings  ps join ps.applicationModule ap join fetch  ap.permissionType")
                .getResultList();
    }

    /**
     * The list of application modules that has at least one permission option defined.
     * 
     * @return The list of {@link ApplicationModule}
     */
    public List<ApplicationModule> findPermissionOptionAssignedApplicationModules() {
        return jpaApi.em().createQuery("select distinct am from PermissionOption po join po.applicationModule am",
                ApplicationModule.class).getResultList();
    }

    /**
     * Retrieves the instance of ApplicationModule by applicationModuleId.
     *
     * @param applicationModuleId
     *            The application module id
     * @return The Optional value of ApplicationModule.
     */
    public Optional<ApplicationModule> findById(final Integer applicationModuleId) {
        Optional<ApplicationModule> result = Optional.empty();
        List<ApplicationModule> applicationModules = jpaApi.em()
                .createQuery("select ap from ApplicationModule ap where ap.applicationModuleId= :applicationModuleId",
                        ApplicationModule.class)
                .setParameter("applicationModuleId", applicationModuleId).getResultList();
        if (!applicationModules.isEmpty()) {
            result = Optional.of(applicationModules.get(0));
        }
        return result;
    }
}
