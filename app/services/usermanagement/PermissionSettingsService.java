package services.usermanagement;

import models.PermissionSettings;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by VJOSHNA on 10/19/2016.
 */
public class PermissionSettingsService {
    private final JPAApi jpaApi;

    @Inject
    public PermissionSettingsService(final JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Saves the permissionSettings into database.
     *
     * @param permissionSettings
     *            the instance of {@link PermissionSettings}
     */
    public void save(final PermissionSettings permissionSettings) {
        jpaApi.em().persist(permissionSettings);
    }

    /**
     * Retrieves the list of all permission settings from Database.
     *
     * @return The List of {@link PermissionSettings}
     */
    public List<PermissionSettings> findAll() {
        Logger.debug("event=\"Retrieving all permission settings from DB\"");
        return jpaApi.em()
                .createQuery(
                        "select ps from PermissionSettings ps join fetch ps.applicationModule order by ps.applicationModule.applicationModuleName",
                        PermissionSettings.class)
                .getResultList();
    }

    /**
     * Retrieves the instance of PermissionSettings by permissionSettingsId.
     *
     * @param permissionSettingsId
     * @return
     */
    public Optional<PermissionSettings> findByPermissionSettingsId(final Long permissionSettingsId) {
        Optional<PermissionSettings> result = Optional.empty();
        List<PermissionSettings> permissionSettingsList = jpaApi.em()
                .createQuery(
                        "select ps from PermissionSettings ps where ps.permissionSettingsId = :permissionSettingsId",
                        PermissionSettings.class)
                .setParameter("permissionSettingsId", permissionSettingsId).getResultList();
        if (!permissionSettingsList.isEmpty()) {
            result = Optional.of(permissionSettingsList.get(0));
        }
        return result;
    }

    /**
     * Retrieves the list of all permission settings from Database for given <code>applicationModuleId</code>.
     *
     * @return The List of {@link PermissionSettings}
     */
    public List<PermissionSettings> findByApplicationModuleId(final Integer applicationModuleId) {
        Logger.debug("event=\"Retrieving all permission settings from DB\"");
        return jpaApi.em()
                .createQuery(
                        "select ps from PermissionSettings ps " + "where ps.applicationModuleId = :applicationModuleId",
                        PermissionSettings.class)
                .setParameter("applicationModuleId", applicationModuleId).getResultList();
    }

    /**
     * Deletes the given role from database.
     *
     * @param permissionSettings
     *            The {@link PermissionSettings} to be deleted
     */
    public void delete(final PermissionSettings permissionSettings) {

        jpaApi.em().remove(permissionSettings);
    }

}