package services.usermanagement;

import models.HumanUser;
import models.User;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

public class HumanUserService {
    private final JPAApi jpaApi;

    @Inject
    HumanUserService(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Retrieves the list of human users.
     * 
     * @return The List of {@link HumanUser}
     */
    public List<HumanUser> findAll() {
        Logger.debug("event=\"Retrieving all human users from DB\"");
        return jpaApi.em().createQuery("select hu from HumanUser hu "
                + "join fetch hu.user ", HumanUser.class).getResultList();

    }

    /**
     * Search for a human user by email address. The associated user is eagerly fetched.
     *
     * @param email
     *            The user's email address.
     * @return An Optional containing the human user identified by the email address.
     */
    public Optional<HumanUser> findByEmail(String email) {
        Logger.debug("event=\"Retrieving human user from DB by email address.\"");
        List<HumanUser> humanUserList = jpaApi.em()
                .createQuery("select hu from HumanUser hu join fetch hu.user where hu.email = :email",
                        HumanUser.class)
                .setParameter("email", email).getResultList();
        if (!humanUserList.isEmpty()) {
            return Optional.of(humanUserList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Search for a human user by the ID of the user it is associated with.
     *
     * @param userId
     *            The user ID.
     * @return An Optional containing the human user identified by the user ID.
     */
    public Optional<HumanUser> findByUserId(long userId) {
        Logger.debug("event=\"Retrieving human user from DB by user ID.\", \"user ID\"=\"" + userId + "\"");
        List<HumanUser> humanUserList = jpaApi.em()
                .createQuery("select hu from HumanUser hu join fetch hu.user "
                        + "where hu.user.id = :userId",
                        HumanUser.class)
                .setParameter("userId", userId).getResultList();
        if (!humanUserList.isEmpty()) {
            return Optional.of(humanUserList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Search for a human user by the ID of the human user it is associated with.
     *
     * @param humanUserId
     *            The human user ID.
     * @return An Optional containing the human user identified by the human user ID.
     */
    public Optional<HumanUser> findByHumanUserId(long humanUserId) {
        Logger.debug("event=\"Retrieving human user from DB by human user ID.\", \"human user ID\"=\"" + humanUserId + "\"");
        List<HumanUser> humanUserList = jpaApi.em()
                .createQuery("select hu from HumanUser hu join fetch hu.user "
                                + "where hu.id = :humanUserId",
                        HumanUser.class)
                .setParameter("humanUserId", humanUserId).getResultList();
        if (!humanUserList.isEmpty()) {
            return Optional.of(humanUserList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Search for a human user by the network ID of the user it is associated with.
     *
     * @param networkId
     *            The network ID.
     * @return An Optional containing the human user identified by the network ID.
     */
    public Optional<HumanUser> findByNetworkId(String networkId) {
        Logger.info("event=\"Retrieving human user from DB by network ID.\"");
        List<HumanUser> humanUserList = jpaApi.em()
                .createQuery("select hu from HumanUser hu join fetch hu.user where hu.networkId = :networkId",
                        HumanUser.class)
                .setParameter("networkId", networkId).getResultList();
        if (!humanUserList.isEmpty()) {
            return Optional.of(humanUserList.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Inserts a <code>humanUser</code> into the database.
     * 
     * @param humanUser
     *            The instance of {@link HumanUser}
     */
    public void save(HumanUser humanUser) {
        User user = humanUser.getUser();
        jpaApi.em().persist(user);
        humanUser.setUser(user);
        jpaApi.em().persist(humanUser);
    }
}