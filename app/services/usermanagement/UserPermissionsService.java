package services.usermanagement;

import models.PermissionOption;
import models.UserPermissions;
import play.Logger;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by vjoshna on 10/31/2016.
 */
public class UserPermissionsService {
    private final JPAApi jpaApi;

    @Inject
    public UserPermissionsService(final JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Saves user permissions into database.
     * 
     * @param userPermissions
     *            the instance of {@link UserPermissions}
     */
    public void save(final UserPermissions userPermissions) {
        jpaApi.em().persist(userPermissions);
    }

    /**
     * Retrieves the instance of {@link UserPermissions} from database.
     * 
     * @param userPermissionId
     *            The userPermissionId
     * @return The Optional of {@link UserPermissions}
     */
    public Optional<UserPermissions> findByUserPermissionId(final Long userPermissionId) {
        Optional<UserPermissions> result = Optional.empty();
        List<UserPermissions> userPermissionses = jpaApi.em()
                .createQuery("select up from UserPermissions up where up.userPermissionId= :userPermissionId",
                        UserPermissions.class)
                .setParameter("userPermissionId", userPermissionId).getResultList();
        if (!userPermissionses.isEmpty()) {
            result = Optional.of(userPermissionses.get(0));
        }
        return result;
    }

    /**
     * Retrieves the user permissions for given userId
     * 
     * @param userId
     *            The user id
     * @return The List of {@link UserPermissions}
     */
    public List<UserPermissions> findByUserId(final Long userId) {
        return jpaApi.em()
                .createQuery(
                        "select up from UserPermissions  up " + "join fetch  up.applicationModule "
                                + "join  fetch up.permissionSettings " + "left join fetch up.environment "
                                + "left join fetch  up.previewer " + "where up.userId= :userId "
                                + "order by up.applicationModule.applicationModuleName, up.previewer.name,up.tenantId,up.environment.name",
                        UserPermissions.class)
                .setParameter("userId", userId).getResultList();
    }

    /**
     * Retrieves the list of environment id which are assigned to the given user.
     * 
     * @param userId
     *            The user id
     * @param permissionSettingsId
     *            The role id
     * @return The list of environment ids
     */
    public List<Integer> findEnvironmentsByUserId(final Long userId, final Long permissionSettingsId) {
        return jpaApi.em()
                .createQuery("select up.environmentId from UserPermissions up "
                        + "where up.userId= :userId and up.permissionSettingsId = :permissionSettingsId "
                        + "order by  up.environment.name", Integer.class)
                .setParameter("userId", userId).setParameter("permissionSettingsId", permissionSettingsId)
                .getResultList();
    }

    /**
     * Retrieves the list of tenant ids from database which are assigned to the given user and role.
     * 
     * @param userId
     *            The user id
     * @param permissionSettingId
     *            The permissionSettings id
     * @return The list of tenant ids.
     */
    public List<Long> findTenantByUserIdAndPermissionSettingId(final Long userId, final Long permissionSettingId) {
        return jpaApi.em()
                .createQuery("select up.tenantId from UserPermissions  up where up.userId= :userId "
                        + "and up.permissionSettingsId = :permissionSettingId", Long.class)
                .setParameter("userId", userId).setParameter("permissionSettingId", permissionSettingId)
                .getResultList();
    }

    /**
     * Retrieves the list of UserPermissions from database for given <code>userId</code>,
     * <code>applicationModuleId</code> and <code>permissionSettingId</code>.
     *
     * @param userId
     *            The user id
     * @param applicationModuleId
     *            The application module id
     * @param permissionSetttingsId
     *            The permissionSettings id
     * @return The Optional of {@link UserPermissions}
     */
    public Optional<UserPermissions> find(final Long userId, final Integer applicationModuleId,
            final Long permissionSetttingsId, final Integer previewerId) {
        Optional<UserPermissions> result = Optional.empty();
        List<UserPermissions> userPermissions = jpaApi.em()
                .createQuery(
                        "select up from UserPermissions up join fetch up.permissionSettings ps "
                                + "where up.userId= :userId "
                                + "and up.applicationModule.applicationModuleId= :applicationModuleId "
                                + "and up.permissionSettingsId= :permissionSetttingsId "
                                + "and up.previewerId= :previewerId " + "and up.isWildcard = true",
                        UserPermissions.class)
                .setParameter("userId", userId).setParameter("applicationModuleId", applicationModuleId)
                .setParameter("permissionSetttingsId", permissionSetttingsId).setParameter("previewerId", previewerId)
                .getResultList();
        if (!userPermissions.isEmpty()) {
            result = Optional.of(userPermissions.get(0));
        }
        return result;
    }

    /**
     * Retrieves the list of UserPermissions from database for given <code>userId</code>,
     * <code>applicationModuleId</code> and <code>permissionSettingId</code>.
     *
     * @param userId
     *            The user id
     * @param applicationModuleId
     *            The application module id
     * @param permissionSetttingsId
     *            The permissionSettings id
     * @return The Optional of {@link UserPermissions}
     */
    public Optional<UserPermissions> find(final Long userId, final Integer applicationModuleId,
            final Long permissionSetttingsId) {
        Optional<UserPermissions> result = Optional.empty();
        List<UserPermissions> userPermissions = jpaApi.em().createQuery(
                "select up from UserPermissions up join fetch up.permissionSettings ps " + "where up.userId= :userId "
                        + "and up.applicationModule.applicationModuleId= :applicationModuleId "
                        + "and up.permissionSettingsId= :permissionSetttingsId " + "and up.isWildcard = true",
                UserPermissions.class).setParameter("userId", userId)
                .setParameter("applicationModuleId", applicationModuleId)
                .setParameter("permissionSetttingsId", permissionSetttingsId).getResultList();
        if (!userPermissions.isEmpty()) {
            result = Optional.of(userPermissions.get(0));
        }
        return result;
    }

    /**
     * Retrieves the list of UserPermissions from database which contains the given role.
     *
     * @param permissionSettingId
     *            The permissionSettings id
     * @return The list of {@link UserPermissions}
     */
    public List<UserPermissions> findByPermissionSettingId(final Long permissionSettingId) {
        return jpaApi.em()
                .createQuery("select up from UserPermissions  up where up.permissionSettingsId = :permissionSettingId",
                        UserPermissions.class)
                .setParameter("permissionSettingId", permissionSettingId).getResultList();
    }

    /**
     * Removes the given user permissions from database.
     * 
     * @param userPermissions
     *            The instance of {@link UserPermissions} to be deleted
     */
    public void delete(final UserPermissions userPermissions) {
        jpaApi.em().remove(userPermissions);
    }

    public List<UserPermissions> findAllByUserId(long userId) {
        Logger.debug("event=\"Retrieving user permissions by user ID {}.\"", userId);

        // Fetch PermissionOptions so that they won't be lazy loaded and cause multiple DB hits later on.
        jpaApi.em()
                .createQuery(
                        "select o from PermissionOption o where o.permissionOptionId in "
                                + "(select sv.permissionOption.permissionOptionId "
                                + "from PermissionSettingsValue sv, PermissionSettings s, UserPermissions u "
                                + "where u.userId = :userId and s.permissionSettingsId = u.permissionSettingsId "
                                + "and sv.permissionSettings.permissionSettingsId = s.permissionSettingsId)",
                        PermissionOption.class)
                .setParameter("userId", userId).getResultList();

        return jpaApi.em()
                .createQuery("select distinct u " + "from UserPermissions u " + "join fetch u.permissionSettings s "
                        + "join fetch s.permissionSettingsValues " + "join fetch u.applicationModule a "
                        + "join fetch a.permissionType " + "where u.userId = :userId order by u.userPermissionId",
                        UserPermissions.class)
                .setParameter("userId", userId).getResultList();
    }

    public List<UserPermissions> findByApplicationModuleIdAndUserId(final Integer applicationModuleId,
            final Long userId) {
        return jpaApi.em()
                .createQuery("select up from UserPermissions up where up.userId= :userId and up" + ".applicationModule"
                        + ".applicationModuleId= :applicationModuleId", UserPermissions.class)
                .setParameter("userId", userId).setParameter("applicationModuleId", applicationModuleId)
                .getResultList();
    }
}
