package services.usermanagement;

import models.PermissionOption;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by VJOSHNA on 11/3/2016.
 */
public class PermissionOptionService {
    private JPAApi jpaApi;

    @Inject
    public PermissionOptionService(final JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Retrieves the list of available permission options available for given <code>applicationModuleId</code>
     * 
     * @param applicationModuleId
     *            The application module ID.
     * @return The list of {@link PermissionOption}
     */
    public List<PermissionOption> findByApplicationModuleId(final Long applicationModuleId) {
        return jpaApi.em()
                .createQuery("select po from PermissionOption po join fetch po.applicationModule am where po.applicationModuleId= :applicationModuleId",
                        PermissionOption.class)
                .setParameter("applicationModuleId", applicationModuleId).getResultList();
    }
}
