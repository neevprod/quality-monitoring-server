package services;

import models.PermissionSettingsValue;
import play.Logger;
import play.cache.SyncCacheApi;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class PermissionSettingsValueService {
    private final JPAApi jpaApi;
    private final SyncCacheApi cache;

    @Inject
    PermissionSettingsValueService(JPAApi jpaApi, SyncCacheApi cache) {
        this.jpaApi = jpaApi;
        this.cache = cache;
    }

    /**
     * Inserts a <code>permissionSettingsValue</code> into database.
     *
     * @param permissionSettingsValue The instance of {@link PermissionSettingsValue}
     */
    public void create(final PermissionSettingsValue permissionSettingsValue) {
        Logger.debug("event=\"Saving new permissionSetting value to database.\"");
        jpaApi.em().persist(permissionSettingsValue);
    }

    /**
     * Deletes a <code>permissionSettingsValue</code> from database.
     *
     * @param permissionSettingsValue The instance of {@link PermissionSettingsValue}
     */
    public void delete(final PermissionSettingsValue permissionSettingsValue) {
        Logger.debug("event=\"Deleting permissionSettingValue from database.\",permissionSettingsValueId= \"{}\"",
                permissionSettingsValue.getPermissionSettingsValueId());
        jpaApi.em().remove(permissionSettingsValue);
    }

    /**
     * Fetch a permissionSettingsValue by its permissionSettingsValueId.
     *
     * @param permissionSettingsValueId The permissionSettingsValueId
     * @return The Optional of {@link PermissionSettingsValue}
     */
    public Optional<PermissionSettingsValue> findPermissionSettingValueById(final Long permissionSettingsValueId) {
        Logger.debug("event=\"Retrieving permissionSettingValue.\", permissionSettingsValueId=\"{}\"",
                permissionSettingsValueId);
        List<PermissionSettingsValue> permissionSettingsValues = jpaApi.em()
                .createQuery(
                        "select psv from PermissionSettingsValue psv where psv.permissionSettingsValueId= :permissionSettingsValueId",
                        PermissionSettingsValue.class)
                .setParameter("permissionSettingsValueId", permissionSettingsValueId).getResultList();
        if (!permissionSettingsValues.isEmpty()) {
            return Optional.of(permissionSettingsValues.get(0));
        } else {
            return Optional.empty();
        }
    }

    public List<PermissionSettingsValue> findByPermissionSettingsId(final Long applicationModuleId,
                                                                    final Long permissionSettingsId) {
        return jpaApi.em()
                .createQuery(
                        "select psv from PermissionSettingsValue  psv join fetch psv.permissionOption po join fetch psv.permissionSettings ps "
                                + "where po.applicationModuleId= :applicationModuleId "
                                + "and psv.permissionSettings.permissionSettingsId= :permissionSettingsId",
                        PermissionSettingsValue.class)
                .setParameter("permissionSettingsId", permissionSettingsId)
                .setParameter("applicationModuleId", applicationModuleId).getResultList();
    }

    public Map<String, PermissionSettingsValue> findAllByPermissionSettingsId(long permissionSettingsId) {
        Logger.info("event=\"Retrieving permission setting values from cache.\", permissionSettingsId=\"{}\"",
                permissionSettingsId);

        return cache.getOrElseUpdate("permissionSettings." + permissionSettingsId,
                () -> retrieveFromDbById(permissionSettingsId));
    }

    private Map<String, PermissionSettingsValue> retrieveFromDbById(long permissionSettingsId) {
        Logger.info("event=\"Retrieving permission setting values from DB.\", permissionSettingsId=\"{}\"",
                permissionSettingsId);

        List<PermissionSettingsValue> valueList = jpaApi.em()
                .createQuery(
                        "select sv from PermissionSettingsValue sv "
                                + "where sv.permissionSettings.permissionSettingsId = :permissionSettingsId",
                        PermissionSettingsValue.class)
                .setParameter("permissionSettingsId", permissionSettingsId).getResultList();

        Map<String, PermissionSettingsValue> valueMap = new HashMap<>();
        for (PermissionSettingsValue value : valueList) {
            valueMap.put(value.getPermissionOption().getName(), value);
        }

        return valueMap;
    }
}
