# Quality Monitoring API

## Overview

### Guidelines

- In order for requests to be authenticated, a valid token needs to be sent in the "X-Auth-Token" header. A valid token
can be obtained by sending a Retrieve Token request with valid credentials (see below for details).
- For requests that require a body, the body must be in JSON format. This requires sending a "Content-Type" header with
a value of "application/json".

### Response Formats

Responses will be returned in a JSON format. If the request was successful, the "success" property will be `true`, and
any relevant data will be returned in the "data" property. Here is an example of the format of a successful request:

```javascript
{
  "success": true,
  "data": { <returned data> }
}
```

If the request was not successful, the "success" property will be `false`, and there will be one or more error
messages in an array called "errors". Here is an example of the format of an unsuccessful request:

```javascript
{
  "success": false,
  "errors": [
    {
      "message": "<error message>"
    }
  ]
}
```

## Request Types

### Retrieve Token

This allows a token to be retrieved, given a valid API key name and API key. The token is valid for 2 hours.

**Request:**  
`POST https://quality-monitoring.pearsondev.com/api/core/v1/token`  

**Body:**
```javascript
{
    "apiKeyName": "<apiKeyName>",
    "apiKey": "<apiKey>"
}
```

**Response:**
```javascript
{
  "success": true,
  "data": {
    "token": "<token>"
  }
}
```

### Retrieve Previewers

This retrieves a list of the previewers available in the system. When making other requests that require a previewer
ID, this is how you can find the ID of the previewer you are working with.

**Request:**  
`GET https://quality-monitoring.pearsondev.com/api/qtiValidation/v1/previewers`

**Response:**
```javascript
{
  "success": true,
  "data": {
    "previewers": [
      {
        "previewerId": <previewerId>,
        "name": "<previewerName>",
        "url": "<url>",
        "apiUrl": "<apiUrl>"
      }
    ]
  }
}
```

### Generate QTI Responses

This will generate QTI responses for the given item XML that can be used as test cases. In addition to the item XML, it
also takes in number of max correct and max incorrect responses to generate. If these parameters are left out, they
both default to 1.

*Note: In the item XML that is placed in the body, double quotes should be escaped with a `\`, but single quotes
should be left unescaped.*

**Request:**  
`POST https://quality-monitoring.pearsondev.com/api/qtiRespGen/v1/responses`  

**Body:**
```javascript
{
  "itemXml": "<itemXml>",
  "maxCorrect": <maxCorrect>,
  "maxIncorrect": <maxIncorrect>
}
```

**Response:**
```javascript
{
  "success": true,
  "data": {
    "itemId": "<itemId>",
    "identifiersToInteractions": [
      {
        "identifier": "<identifier>",
        "interaction": "<interactionType>"
      }
    ],
    "responses": [<responses>]
  }
}
```

### Upload Test Cases

This will start a job to upload the given QTI responses as test cases to the given item. The body takes the same form as the data
returned from the Generate QTI Responses request, so generated responses can be directly used to upload test cases. In the response, the ID of the job is returned. This can be used in a separate request to check the status of the job.

**Request:**  
`POST https://quality-monitoring.pearsondev.com/api/qtiValidation/v1/previewers/<previewerId>/tenants/<tenantId>/items/<itemId>/testCases`  

**Parameters:**  

- previewerId: The ID of the previewer the item resides in.
- tenantId: The ID of the tenant the item resides in.
- itemId: The numeric ID of the item.

**Body:**
```javascript
{
  {
    "itemId": "<itemIdentifier>",
    "identifiersToInteractions": [
      {
        "identifier": "<identifier>",
        "interaction": "<interactionType>"
      }
    ],
    "responses": [<responses>]
  }
}
```

**Response:**
```javascript
{
  "success": true,
  "data": {
    "jobId": "<jobId>"
  }
}
```

### Generate and Upload Test Cases

This will start a job to generate test cases for the given items or test maps and then upload them. This is basically a combination of the "Generate QTI Responses" request and the "Upload Test Cases" request. Instead of making two separate requests to generate responses and then to upload them as test cases, this will allow you to both generate and upload in one request. The ID of the test case upload job is returned in this request's response. This can be used in a separate request to check the status of the job.

To generate and upload test cases for a list of items, include the item IDs in an array called "itemIds" in the request body. To generate and upload test cases for all items in a list of test maps, include the test map IDs in an array called "testMapIds" in the request body. You can also specify the max correct and max incorrect responses to generate for each item; these both default to 1 if they are not specified.

**Request:**  
`POST https://quality-monitoring.pearsondev.com/api/qtiValidation/v1/previewers/<previewerId>/tenants/<tenantId>/testCases`  

**Parameters:**  

- previewerId: The ID of the previewer.
- tenantId: The ID of the tenant.

**Body:**
```javascript
{
  {
    "itemIds": "[<itemId1>, <itemId2>, ... ]",
    "maxCorrect": <maxCorrect>,
    "maxIncorrect": <maxIncorrect>
  }
}
```
or
```javascript
{
  {
    "testMapIds": "[<testMapId1>, <testMapId2>, ... ]",
    "maxCorrect": <maxCorrect>,
    "maxIncorrect": <maxIncorrect>
  }
}
```

**Response:**
```javascript
{
  "success": true,
  "data": {
    "jobId": "<jobId>"
  }
}
```

### Get Test Case Upload Job Status

This will retrieve the status of a test case upload job by its job ID to check its status. It will indicate whether the
job is still running, and if not, it will indicate whether the job was successful. If the job is finished, data and/or
errors about the job's execution will also be returned.

**Request**  
`GET https://quality-monitoring.pearsondev.com/api/qtiValidation/v1/testCaseUploadJobs/<jobId>`

**Parameters:**  

- jobId: The ID of the job to check.

**Response:**
```javascript
{
  "success": true,
  "data": {
    "isRunning": true
  }
}
```
or
```javascript
{
  "success": true,
  "data": {
    "isRunning": false,
    "jobSuccessful": <true|false>,
    "jobData": {<jobData>},
    "jobErrors": [<jobErrors>]
  }
}
```

### Get Response Outcomes

Given item XML and a list of responses, this will return a corresponding list of outcomes.

In the request body, "responses" contains the list of responses to get outcomes for. "initializedOutcomes" is optional, and it is used for any outcomes that need to be initialized to a specific value (such as for a human-scored response).

In the response, there will be a corresponding entry in "responseOutcomes" for each entry in the request's "responses" array (in the same order). For each response outcome, "scorable" will be set to true if the response was able to be scored, and false if it was not able to be scored. In the case where it was not able to be scored, there will also be one or more errors in the "errors" array.

*Note: In the item XML that is placed in the body, double quotes should be escaped with a `\`, but single quotes
should be left unescaped.*

**Request:**  
`POST https://quality-monitoring.pearsondev.com/api/qtiRespGen/v1/outcomes`  

**Body:**
```javascript
{
  "itemXml": "<itemXml>",
  "responses": [
    {
      "mods": [
        {
          "did": "<responseId>",
          "r": "<response>"
        }
      ],
      "initializedOutcomes": [
        {
          "identifier": "<identifier>",
          "value": <value>
        }
      ]
    }
  ]
}
```

**Response:**
```javascript
{
  "success": true,
  "data": {
    "responseOutcomes": [
      {
        "scorable": true,
        "errors": [
          {
            "message": "<errorMessage>"
          }
        ],
        "outcomes": [
          {
            "identifier": "<identifier>",
            "value": "<value>"
          }
        ]
      }
    ]
  }
}
```
### Load KT Testcase Manifest

This loads the test case data from the specified KT test case manifest from the "product-validation-math-scoring-manifest" S3 bucket.

**Request:**  
`GET https://quality-monitoring.pearsondev.com/api/qtiValidation/v1/manifests/<manifestName>/ktTestCases`

**Response:**
```javascript
{
   "success": true,
   "data": {
       "jobId": "ktTestCaseDownload_3_1552424604152"
   }
}
```